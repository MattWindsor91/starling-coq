(** WIP soundness proof for GStarling. *)


From Coq Require Import
     Arith
     Classes.DecidableClass
     Classes.SetoidClass
     Classes.SetoidDec
     Lists.List
     Lists.SetoidList
     Lists.SetoidPermutation
     Omega
     Program.Basics
     Program.Utils
     RelationPairs
     Sets.Constructive_sets
     Sorting.Permutation
.

From Starling Require Import
     Logics.GStarling.Core
     Logics.GStarling.Lowered
     Logics.GStarling.Instance
     Utils.Ensemble.Facts
     Utils.List.Facts
     Utils.List.MapPrf
     Utils.List.Setoid
     Utils.Multiset.LMultiset
     Utils.Monads
     Views.Classes
     Views.Expr
     Views.Instances.LMultiset
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.Core
     Views.Frameworks.LVF.Instances
     Views.Frameworks.LVF.Signatures
     Views.Transformers.Function
     Views.Transformers.Subtype
     ProgramProof
     ProofRules
     Backend.AbstractExpr
     Backend.Alpha
     Backend.Classes
     Backend.Transformers.Function
     Frontend.Common
     Frontend.APred
.

Set Implicit Arguments.

Section gstarling_soundness.
  Variables
    Ep Ev
  : Set -> Set.

  Variables
    Var Val Symb VBop EBop VMop EMop C Tag
    : Set.

  Variables
    GCtx
    LCtx
    : Type.

  Context
    {T_Tag : Tagger Tag}
    {S_Val : Setoid Val}
    {D_Val : EqDec S_Val}
    {E_Ep  : EqPredEx GCtx unit Ep (Val := Val)}
    {V_Ev  : ValEx Ev (Val := Val)}
    {AEC   : AbstractExprContext Symb VBop EBop VMop EMop (AP_Ep := E_Ep)}.

  Existing Instance EVal_Setoid.

  Variable
    gs : GStarling Ep Ev Tag Var Val Symb VBop EBop VMop EMop C.

  Variables
    (gctx_witness : GCtx).

  Import ListNotations.

  Definition gs_sem_define (gc : GCtx) (v : LowView gs) :
    option (Ensemble (GSSStP gs)) :=
    match flat_map (gs_define_lowered_single v) gs.(gs_definer) with
    | [] => None
    | xs => Some (interp_def gc (iconj xs (P_Ep := (PredEx_Regular gs))))
    end.

  Definition gs_define_lowered_to_expr
             (v : LowView gs) :
    AbstractExpr Symb VBop EBop VMop EMop (Ev (SVar gs)) :=
    gd_conj_over_definer
      (SVar gs)
      Ev Val GCtx unit Symb VBop EBop VMop EMop Tag
      (gs.(gs_protos))
      (gs_define_lowered_single v)
      (gs.(gs_definer)).

  Definition gs_v_define (gc : GCtx) : LowView gs -> Ensemble (GSSStP gs) :=
    interp_def gc ∘ gs_define_lowered_to_expr.

  (** Rephrasing [gs_v_define] as [gs_sem_define] doesn't change the
      semantics. *)

  Lemma gs_v_sem_define_same (gc : GCtx) (v : LowView gs) :
    Same_set _
             (dfn_lift _ _ (gs_sem_define gc) v)
             (gs_v_define gc v).
  Proof.
    unfold dfn_lift, gs_sem_define, gs_v_define, gs_define_lowered_to_expr,
    gd_conj_over_definer, compose.
    destruct (flat_map (gs_define_lowered_single v) (gs_definer gs)); intuition.
    symmetry.
    apply pe_true_unit.
  Qed.

  Definition lower_goal
             (gc : GCtx)
             (d : {x : GSAPredDef gs | List.In x gs.(gs_definer) })
             (lst lst' : GSLSt gs) (gst : GoalState Ev) :
    LNormVExpr (LowAtom gs) :=
    (lvmap (lower_goal_atom gc lst lst' gst) (inst_gdef gc d (S_Val := S_Val))).

  Fixpoint shadow_goal (g : list (LowAtom gs)) : GoalState Ev :=
    match g with
    | [] => const (ve_bot _)
    | x::xs =>
      fun i =>
        if length (` (ap_argv x)) <=? i
        then shadow_goal xs (i - length (` (ap_argv x)))
        else nth i (` (ap_argv x)) (ve_bot _)
    end.

  Lemma shadow_goal_nth (xs : list (LowAtom gs)) (i : nat) :
    shadow_goal xs i =
    nth i (flat_map (fun x => ` (ap_argv x)) xs) (ve_bot _).
  Proof.
    revert i.
    induction xs.
    - now destruct i.
    - intro i.
      cbn.
      destruct (length (lap_argv (` a)) <=? i) eqn:Hlen.
      + apply leb_complete in Hlen.
        now rewrite app_nth2.
      + apply leb_complete_conv in Hlen.
        now rewrite app_nth1.
  Qed.

  Definition interpret_low : VExpr (LowAtom gs) -> LowView gs :=
    interpret_sm
      (al_inject _ _
                 (AtomLanguage :=
                    AtomLanguage_APred_LMultiset_Direct Datatypes.Empty_set Ev gs.(gs_protos)))
      (SV := LowView_Setoid gs).

  Lemma unlistify_inner_interpret (xs : list (LowAtom gs)) :
    interpret_low (unlistify_inner xs)
    = LBag xs.
  Proof.
    induction xs; try easy.
    cbn in *.
    now rewrite IHxs.
  Qed.

  Corollary unlistify_lnorm_interpret (xs : list (LowAtom gs)) :
    interpret_low
      (forget_lnorm (unlistify_lnorm xs))
    = LBag xs.
  Proof.
    rewrite unlistify_inner_unlistify_lnorm.
    apply unlistify_inner_interpret.
  Qed.

  (** For some reason, Coq doesn't want to find this by itself. *)

  Instance equiv_by_ve_interp_Equivalence : Equivalence (equiv_by_ve_interp Datatypes.Empty_set) := {}.

  Lemma has_exact_match_is_lowering
        (gc : GCtx) (l l' : GSLSt gs) (a g : (LowView gs))
        (df : { x : GSAPredDef gs | List.In x (gs_definer gs) }) :
    is_exact_match a g (apd_tag (`df)) ->
    exists g',
      lmeq g g' /\
      eqbagA g' a /\
      eqbagA g'
             (interpret_low
                (forget_lnorm (lower_goal gc df l l' (shadow_goal (unbag g'))))).
  Proof.
    intros Hin_a%dedupeA_inclA_general.
    2: intuition.
    2: apply eqbagbA_eqbagA_reflect.
    apply InA_alt in Hin_a.
    destruct Hin_a as (wit & Ha_wit & Hwit_in).
    unfold pattern_matches_exact_lowered in Hwit_in.
    (* Now get at [wit]'s underlying list. *)
    destruct g as (lg).
    apply in_map_iff in Hwit_in.
    destruct Hwit_in as (wit' & <- & Hin).
    (* We can show that that [lg] is a permutation of [wit'].  *)
    pose proof (Forall_forall_in _ _ _ (pattern_matches_exact_permut _ _ _) Hin) as
        Hwit_perm.
    (* [wit'] is precisely the body of the atom we intended to witness
       in the lemma. *)
    exists (LBag wit').
    repeat split.
    - apply PermutationA_lmeq, Permutation_PermutationA; intuition.
    - now symmetry.
    - (* Show that the tags are indeed in the right places. *)
      pose proof (Forall_forall_in _ _ _ (pattern_matches_exact_tags _ _ _) Hin) as
          Hwit_tags.
      unfold eqbagA, "@@", unbag.
      cbn.
      unfold compose, inst_gdef, proj1_sig.
      rewrite <- lvmap_fmap, lvmap_map, unlistify_listify_lnorm, unlistify_lnorm_interpret.
      (* We now use [lower_shadow_equiv] to show that the witness is equivalent
       to the instantiation-and-lowering we do on the rhs. *)
      destruct df as (df & Hdf).
      cbn in *.
      apply lower_shadow_equiv.
      (* Now we just have to show that the tags line up. *)
      apply unflatten_tags_inner_proj', Hwit_tags.
  Qed.

  Corollary has_def_is_lowering (gc : GCtx) (l l' : GSLSt) (g : (LowView gs)) :
    has_def (LowView gs) GSSStP (gs_sem_define gc) g ->
    exists (gst : GSGSt) (d : {x : GSAPredDef gs | List.In x gs.(gs_definer) }),
      lmeq g (interpret_low (forget_lnorm (lower_goal gc d l l' gst))).
  Proof.
    intros (d & Hd).
    unfold gs_sem_define, gs_define_lowered_single in Hd.
    destruct (flat_map
                (fun d : APredDef Symb VBop EBop VMop EMop (Ev (DefVar (SVar gs)))
                                (flattened_protos Tag (gs_protos gs)) =>
                   map (inst_lowered_match d)
                       (dedupeA eqbagbA (pattern_matches_exact_lowered (` (apd_tag d)) g)))
                       (gs_definer gs)) as [|x xs] eqn:Hfmap; try easy.
    clear Hd.
    (* Try to work out where [x] comes from in the flat map. *)
    pose proof (in_eq x xs) as Hin_fmap.
    rewrite <- Hfmap in Hin_fmap.
    apply in_flat_map in Hin_fmap.
    destruct Hin_fmap as (df & Hin_df & Hlowered_x).
    apply in_map_iff in Hlowered_x.
    destruct Hlowered_x as (a & <- & Hin_a).
    apply In_InA with (eqA := eqbagA) in Hin_a; intuition.
    destruct (has_exact_match_is_lowering gc l l' (exist _ df Hin_df) Hin_a) as (g' & Hmeq & Heqb).
    exists (shadow_goal (unbag g')).
    exists (exist _ df Hin_df).
    rewrite Hmeq.
    now apply eqbagA_equiv_sub.
  Qed.

  (** If two sets over normal shared states are the same, then
      so are the same sets lifted over proof-irrelevant shared states. *)
  Lemma Same_set_remove_prfirr f g :
    Same_set GSSSt f g ->
    Same_set GSSStP (f ∘ extract_pfun) (g ∘ extract_pfun).
  Proof.
    intros (Hinc_fg & Hinc_gf).
    split; intros ((s & Hs)) Hin; unfold Included, In in *; intuition.
  Qed.

  (** A goal state [g] is 'capped' with regards to [max] if,
      for every goal variable index [n] that is outside [max], [g n]
      is the bottom value expression. *)

  Definition goal_state_well_formed
             (g : GSGSt)
             (max : nat) :=
    forall (n : nat), max <= n -> g n = (ve_bot _ (ValEx := V_Ev)).

  (** Small map rearrangement lemmas used below. *)

  Lemma reduce_map
        (l l' : GSLSt)
        (g : GSGSt)
        (xs : list (GSGAPred gs (DMLVar gs))) :
    map (fun x => map (eval_to_valex (SVar gs))
                   (map (mbind (erase_DLVar_goal l l' g))
                        (lap_argv (` (g_item x)))))
        xs =
    map ((map (eval_to_valex (SVar gs) ∘ mbind (erase_DLVar_goal l l' g)))
           ∘ fun x : GSGAPred gs (DMLVar gs) => (lap_argv (` (g_item x))))
        xs.
  Proof.
    apply map_ext.
    intro a.
    apply map_map.
  Qed.

  Lemma reduce_map_2
        (l l' : GSLSt)
        (g : GSGSt)
        (xs : list (GSGAPred gs (DMLVar gs))) :
    map (fun x => map (eval_to_valex (SVar gs))
                   (map (mbind (erase_DLVar_goal l l' g))
                        (lap_argv (` (g_item x)))))
        xs =
    map ((map (eval_to_valex (SVar gs) ∘ mbind (erase_DLVar_goal l l' g))))
        (map (fun x : GSGAPred gs (DMLVar gs) => (lap_argv (` (g_item x)))) xs).
  Proof.
    rewrite reduce_map.
    apply symmetry, map_map.
  Qed.

  (** Iterative core for instantiating a list of non-guarded goal atoms. *)

  Fixpoint inst_argvs_iter
           (base : nat)
           (tags : list (ATag gs.(gs_protos)))
    : list (list (Ev (DMLVar gs))) :=
    match tags with
    | [] => []
    | t::ts =>
      (inst_argv gs base (get_argc t))
        :: (inst_argvs_iter (base + get_argc t) ts)
    end.

  (** We can model the extraction of argvs from instantiated guarded atoms
      using [inst_argvs_iter]. *)

  Lemma inst_gatoms_iter_argv_strip
        (gc : GCtx)
        (tags : list (ATag (gs_protos gs)))
        (base : nat) :
    map (fun x : GSGAPred gs (DMLVar gs) => (lap_argv (` (g_item x))))
        (inst_gatoms_iter gc gs base tags)
    = inst_argvs_iter base tags.
  Proof.
    revert base.
    induction tags; cbn; eauto.
    intro base.
    now f_equal.
  Qed.

  Corollary inst_gatoms_argv_strip
        (gc : GCtx)
        (tags : list (ATag (gs_protos gs))) :
    map (fun x : GSGAPred gs (DMLVar gs) => (lap_argv (` (g_item x))))
        (inst_gatoms gc gs tags)
    = inst_argvs_iter 0 tags.
  Proof.
    apply inst_gatoms_iter_argv_strip.
  Qed.

  Fixpoint inst_argvs_from_argcs_iter
           (base : nat)
           (argcs : list nat)
    : list (list (Ev (DMLVar gs))) :=
    match argcs with
    | [] => []
    | t::ts =>
      (inst_argv gs base t)
        :: (inst_argvs_from_argcs_iter (base + t) ts)
    end.

  (** Each instantiated argv has length equal to its argc. *)

  Lemma inst_argvs_from_argcs_length
        (base : nat)
        (argcs : list nat) :
    (map (length (A:=Ev (DMLVar gs)))
         (inst_argvs_from_argcs_iter base argcs))
    = argcs.
  Proof.
    revert base.
    induction argcs; cbn; intuition.
    f_equal.
    - apply inst_argv_length.
    - apply IHargcs.
  Qed.

  Lemma iter_argv_from_argc_strip
        (tags : list (ATag (gs_protos gs)))
        (base : nat) :
    inst_argvs_iter base tags
    = inst_argvs_from_argcs_iter base (map (@get_argc _ _) tags).
  Proof.
    revert base.
    induction tags; cbn; eauto.
    intro base.
    now f_equal.
  Qed.

  Definition capped {A : Type} (f : nat -> A) (default : A) (cap n : nat) : A :=
    if (n <? cap) then f n else default.

  Definition capped_goal_state (g : GSGSt) (cap : nat) : GSGSt :=
    capped g (ve_bot _) cap.

  (** If [n] is in range of an [inst_argv], then taking the [n]th element of an
      [inst_argv] and erasing it against a goal state is equivalent to just
      taking the [n]th value from the goal state directly. *)
  Lemma inst_erase_equiv
        (l l' : GSLSt)
        (g : GSGSt)
        (n base max : nat)
        (default : Ev (DMLVar gs)) :
    n < max ->
    eval_equiv
    (erase_DLVar_goal l l' g =<< nth n (inst_argv gs base max) default)
    (g (base + n)).
  Proof.
    intro Hrange.
    rewrite (inst_argv_nth gs base default Hrange).
    unfold inst_arg.
    unfold eval_equiv, "@@".
    rewrite ve_state_mbind, ve_interp_mreturn.
    unfold compose.
    unfold mvars_in_lift_ref, erase_DLVar_goal, lift_dlvar, remove_empty_varlist.
    rewrite ve_state_fmap.
    unfold compose.
    unfold erase_locals_and_goal, flip.
    rewrite ve_state_mbind.
    cbn.
    rewrite ve_interp_mreturn.
    unfold erase_goals, compose.
    cbn.
    rewrite eval_lift_to_valex.
    apply elim_empty_ve_interp_equiv.
  Qed.

  (** Inner lemma used in [goal_vars_equiv_cap]. *)

  Lemma goal_vars_equiv_cap_inner_base
        (l l' : GSLSt)
        (g : GSGSt)
        (argcs : list nat)
        (s : SVar gs -> Val)
        (n base : nat) :
    ve_interp s
              (nth n
                   (concat
                      (map (map (eval_to_valex (SVar gs) ∘ mbind (erase_DLVar_goal l l' g)))
                           (inst_argvs_from_argcs_iter base argcs))) (ve_bot (SVar gs))) ==
    eval_lift (capped_goal_state g (sum argcs + base)) (base + n).
    revert n base.
    induction argcs; intros n base; cbn.
    - (* Base case *)
      unfold capped_goal_state, capped, eval_lift, compose; simpl.
      replace (base + n <? base) with false.
      + destruct n; apply ve_bot_same.
      + apply symmetry, Nat.ltb_ge.
        omega.
    - (* Inductive case *)

      (* This helps us reduce the question of which side of the [++] [n] is indexing into a
         question of whether [n < a]. *)
      assert
        (length
           (map (eval_to_valex (SVar gs) ∘ mbind (erase_DLVar_goal l l' g)) (inst_argv gs base a))
         = a) as Hlen.
      {
        rewrite map_length.
        apply inst_argv_length.
      }

      unfold capped_goal_state, capped, eval_lift, compose in *.
      (* This is a fairly large and repeatedly used expression, so
         shorten it.  (TODO(@MattWindsor91): give it a proper name?) *)
      remember (fun x : Ev (DMLVar gs) => eval_to_valex (SVar gs) (erase_DLVar_goal l l' g =<< x)) as erase.
      destruct (n <? a) eqn:Hna.
      + rewrite app_nth1.
        2: {
          replace (length (map erase (inst_argv gs base a))) with a.
          now apply Nat.ltb_lt.
        }
        replace (base + n <? a + sum argcs + base) with true.
        2: {
          symmetry.
          apply Nat.ltb_lt.
          apply Nat.ltb_lt in Hna.
          omega.
        }
        replace (ve_bot (SVar gs)) with (erase (ve_bot _)).
        2: {
          subst.
          unfold eval_to_valex.
          now rewrite ve_bot_mbind, ve_bot_fmap.
        }
        rewrite map_nth.
        subst.
        (* Get the two interpretations over the same state. *)
        rewrite eval_lift_to_valex, elim_empty_ve_interp_equiv.
        now apply inst_erase_equiv, Nat.ltb_lt.
      + (* n >= a *)
        apply Nat.ltb_ge in Hna.
        rewrite app_nth2; intuition.
        rewrite Hlen, IHargcs.
        rewrite Nat.add_assoc.
        replace (base + a + (n - a)) with (base + n) by omega.
        now replace (sum argcs + base + a) with (a + sum argcs + base) by omega.
  Qed.

  Corollary goal_vars_equiv_cap_inner
        (l l' : GSLSt)
        (g : GSGSt)
        (argcs : list nat)
        (s : SVar gs -> Val)
        (n : nat) :
    ve_interp s
              (nth n
                   (concat
                      (map (map (eval_to_valex (SVar gs) ∘ mbind (erase_DLVar_goal l l' g)))
                           (inst_argvs_from_argcs_iter 0 argcs))) (ve_bot (SVar gs))) ==
    eval_lift (capped_goal_state g (sum argcs)) n.
  Proof.
    now rewrite goal_vars_equiv_cap_inner_base, Nat.add_0_l, Nat.add_0_r.
  Qed.

  (** If we know that [n] is in-bounds, we can get rid of the capping. *)

  Corollary goal_vars_equiv_inbounds_inner
        (l l' : GSLSt)
        (g : GSGSt)
        (argcs : list nat)
        (s : SVar gs -> Val)
        (n : nat) :
    n < sum argcs ->
    ve_interp s
              (nth n
                   (concat
                      (map (map (eval_to_valex (SVar gs) ∘ mbind (erase_DLVar_goal l l' g)))
                           (inst_argvs_from_argcs_iter 0 argcs))) (ve_bot (SVar gs))) ==
    eval_lift g n.
  Proof.
    rewrite goal_vars_equiv_cap_inner.
    intro Hn.
    unfold capped_goal_state, capped, eval_lift, compose.
    apply Nat.ltb_lt in Hn.
    now rewrite Hn.
  Qed.

  (** Simplifying rewrite used to clean up a large swathe of plumbing
      used in the goal-instantiation lemmata. *)

  Lemma simplify_nth_of_goal
        (gc : GCtx)
        (l l' : GSLSt)
        (g : GSGSt)
        (def : {x : GSAPredDef gs | List.In x (gs_definer gs) }) :
      (lap_argv
         (`
            (flatten_lowered_view
               (interpret_low
                  (forget_lnorm (lower_goal gc def l l' g))))))
      =
      (concat
         (map (map (eval_to_valex (SVar gs) ∘ mbind (erase_DLVar_goal l l' g)))
              (inst_argvs_from_argcs_iter
                 0
                 (map (get_argc (pfun:=gs_protos gs)) (unflatten_tags Tag (gs_protos gs) (apd_tag (`def))))))).
  Proof.
    cbn.
    rewrite <- lvmap_fmap, lvmap_map, unlistify_inner_unlistify_lnorm, unlistify_inner_interpret.
    simpl.
    (* Lots and lots of map reorganisation. *)
    rewrite map_map.
    simpl.
    rewrite flat_map_concat_map, map_map, concat_map, map_map.
    simpl.
    rewrite inst_gdef_listify.
    unfold inst_gdef_list.
    (* We've finally made the definition's proof irrelevant. *)
    destruct def as ((dt & dd) & Hdef).
    unfold inst_gatoms.
    cbn in *.
    clear Hdef dd.
    now rewrite reduce_map_2, inst_gatoms_iter_argv_strip, iter_argv_from_argc_strip.
  Qed.

  (** The nth in-bounds variable in the simplified
      instantiated form of the goal over [def] is just the nth
      variable in the goal state. *)

  Lemma goal_vars_equiv_inbounds
        (l l' : GSLSt)
        (g : GSGSt)
        (dt : _)
        (s : SVar gs -> Val)
        (n : nat) :
    n < (get_argc dt) ->
    ve_interp s
              (nth n
                   (concat
                      (map (map (eval_to_valex (SVar gs) ∘ mbind (erase_DLVar_goal l l' g)))
                           (inst_argvs_from_argcs_iter 0
                                                       (map (get_argc (pfun:=gs_protos gs)) (unflatten_tags Tag (gs_protos gs) dt)))))
                   (ve_bot (SVar gs))) == eval_lift g n.
  Proof.
    intro Hinbounds.
    rewrite (unflatten_tags_argc Tag _ dt) in *.
    now apply goal_vars_equiv_inbounds_inner.
  Qed.

  (** Shadowing goal states are always well-formed with respect to the flattened
      form of the goal's tags. *)

  Lemma shadow_goal_well_formed
        (goal : list (LowAtom gs)) :
    goal_state_well_formed
      (shadow_goal goal)
      (sum (map ((@get_argc _ gs.(gs_protos)) ∘ (@ap_tag _ _ gs.(gs_protos))) goal)).
  Proof.
    intros n Hoob.
    rewrite shadow_goal_nth.
    rewrite nth_overflow; intuition.
    etransitivity.
    2: apply Hoob.
    apply Nat.eq_le_incl.
    rewrite flat_map_concat_map, concat_length.
    clear Hoob.
    rewrite ! map_map.
    induction goal; intuition.
    unfold ap_argv, proj1_sig at 1.
    rewrite map_cons, valid_argv_matches_argc, ! map_cons, sum_cons.
    unfold ap_argv, proj1_sig at 1 in IHgoal.
    unfold (LowAtom gs). (* This is implicit, and used for IHgoal. *)
    rewrite IHgoal.
    now try apply same_tag_same_argc.
  Qed.

  Global Instance Measure_Proj1 {A : Type} {P : A -> Prop} : Measure (@proj1_sig A P).

  (** If a list of tags lines up with the tags of a list of atoms, so does
    an unflattening with the same underlying list. *)

  Lemma unflatten_tags_eq_atom_tags (xs : list Tag) (ys : list (LowAtom gs))
        (Hxs : Forall (has_TagProto Tag gs.(gs_protos)) xs) :
    Forall2 (fun (t : Tag) (a : (LowAtom gs)) => t = lap_tag (` a)) xs ys ->
    Forall2 (fun t a => `t = lap_tag (` a))
            (unflatten_tags_inner _ gs.(gs_protos) xs Hxs)
            ys.
  Proof.
    intro Hf2.
    revert Hxs.
    apply (fun a b c => Forall2_ind a b c Hf2); cbn; intuition.
  Qed.

  (** If there is an exact match for [tag] against [goal], then [goal]'s tags are a
      permutation modulo validity proof of the unflattened form of [tag]. *)

  Lemma exact_match_tags_permut
        (wit goal : (LowView gs))
        (tag : _) :
    is_exact_match wit goal tag ->
    PermutationA
      (eq @@ proj1_sig (P:=fun x0 : Tag => gs_protos gs x0 <> None))
      (map (ap_tag (pfun:=gs_protos gs)) (unbag wit)) (unflatten_tags Tag (gs_protos gs) tag).
  Proof.
    intros Hin%dedupeA_equivlistA_general; intuition.
    2: apply eqbagbA_eqbagA_reflect.
    apply InA_alt in Hin.
    destruct Hin as (y & Heqy & Hiny).
    destruct wit as (wit), goal as (goal).
    simpl in *.
    apply in_map_iff in Hiny.
    destruct Hiny as (x & <- & Hinx).
    pose proof (Forall_forall_in _ _ _ (pattern_matches_exact_permut _ _ _) Hinx) as Hgoal_permut.
    pose proof (Forall_forall_in _ _ _ (pattern_matches_exact_tags _ _ _) Hinx) as Htags_permut.
    transitivity (map (ap_tag (pfun:=gs_protos gs)) goal).
    {
      apply map_PermutationA_Proper.
      - intros a b Hab.
        apply Hab.
      - transitivity x.
        + now apply eqlistA_PermutationA.
        + apply symmetry, Permutation_PermutationA; intuition.
          apply equiv_subtype_Equivalence.
    }
    clear Heqy Hinx.
    revert tag Htags_permut.
    induction (Hgoal_permut).
    - (* nil *) destruct tag as (tag & Htag); now destruct tag.
    - (* cons *)
      intros (tag & Htag).
      cbn.
      inversion 1; subst.
      constructor; intuition.
      destruct (proj2 (flatten_cons_legal _ gs.(gs_protos) (lap_tag (`x)) l0) Htag) as (_ & Htag').
      rewrite (H (exist _ l0 Htag') H3).
      apply eqlistA_PermutationA, unflatten_tags_inner_prfirr.
    - (* exchange *)
      intros (tag & Htag).
      transitivity (ap_tag x :: ap_tag y :: map (ap_tag (pfun := gs.(gs_protos))) l).
      { now constructor. }
      cbn in *.
      inversion Htags_permut; subst.
      inversion H3; subst.
      constructor; intuition.
      constructor; intuition.
      symmetry.
      apply eqlistA_PermutationA, eqlistA_altdef.
      eapply Forall2_map_r.
      2: {
        intros a b.
        unfold "@@".
        intro Hq.
        apply Hq.
      }
      now apply unflatten_tags_eq_atom_tags.
    - (* transitivity *)
      intros tag Hf2.
      transitivity (map (ap_tag (pfun := gs.(gs_protos))) l').
      + refine (map_PermutationA_Proper (RB := eq @@ (@proj1_sig _ _)) (RA := eq) _ _ _ _).
        2: apply Permutation_PermutationA; intuition.
        now intros x y ->.
      + now apply IHp2.
  Qed.

  Corollary shadow_goal_well_formed_match_wit
            (wit goal : (LowView gs))
            (tag : _) :
    is_exact_match wit goal tag ->
    goal_state_well_formed (shadow_goal (unbag wit)) (get_argc tag).
  Proof.
    intro Hwf.
    replace (get_argc tag) with (sum (map (@get_argc Tag gs.(gs_protos) ∘ (@ap_tag _ _ gs.(gs_protos))) (unbag wit))).
    - apply shadow_goal_well_formed.
    - rewrite unflatten_tags_argc.
      apply sum_Permutation.
      unfold compose.
      rewrite <- map_map.
      apply PermutationA_Permutation.
      refine (map_PermutationA_Proper (A := ATag gs.(gs_protos)) (RA := eq @@ (@proj1_sig _ _)) (B := nat) (RB := eq) (get_argc (pfun := gs.(gs_protos))) _ _ _).
      + intros a b.
        apply same_tag_same_argc.
      + now apply exact_match_tags_permut with (goal := goal).
  Qed.

  (** The direct interpretation of a goal definition [def] is
      equivalent to the interpretation of a lowering of a pattern
      match over [def]. *)
  Lemma interp_cond_interp_def
        (gc : GCtx)
        (l l' : GSLStP)
        (g : GSGSt)
        (def : {x : GSAPredDef gs | List.In x (gs_definer gs) }) :
    goal_state_well_formed g (get_argc (apd_tag (`def))) ->
    Same_set GSSStP
      (interp_cond
        gc
        l l' g
        (gs_define_goal def))
      (interp_def
         gc
         (inst_lowered_match
            (`def)
            (interpret_low (forget_lnorm (lower_goal gc def
                                                     (extract_pfun l)
                                                     (extract_pfun l') g))))).
  Proof.
    intro Hwf.
    destruct def as ((dt & dd) & Hdef_in), l as ((l & Hl)), l' as ((l' & Hl')).
    (* We don't need proof irrelevance here. *)
    apply
      (@Same_set_remove_prfirr
         (fun s =>
            (pe_interp gc tt ∘ interp_abstract (Val:=Val))
              (aemap (fmap (defvar_stoc (gs:=gs))) dd)
              (ve_interp
                 (make_condvar_state_npi (eval_lift (mark_states (Ev Datatypes.Empty_set) l l'))
                                         s (eval_lift g))))
         (fun s =>
            (pe_interp gc tt ∘ interp_abstract (Val:=Val))
              (aemap
                 (mbind
                    (inst_DefVar (SVar gs) _ _ (list Tag) (flattened_protos _ gs.(gs_protos))
                                 (flatten_lowered_view
                                    (interpret_low
                                       (forget_lnorm (lower_goal gc
                                                           (exist (fun x : GSAPredDef gs => List.In x (gs_definer gs))
                                                                  {| apd_tag := dt; apd_def := dd |} Hdef_in) l l' g)))))) dd)
           (ve_interp s))).
    unfold inst_gdef, inst_gatoms in *.
    (* Try to move the [aemaps] out into [fmaps]. *)
    transitivity
      (fun s =>
         pe_interp gc tt
                   (interp_abstract dd)
                   (ve_interp (make_condvar_state_npi (eval_lift (mark_states _ l l'))
                                                      s
                                                      (eval_lift g))
                              ∘ (fmap (defvar_stoc (gs:=gs))))
                   (PredEx := ape_PredEx _ (EqPredEx := E_Ep))
).
    {
      destruct (aemap_fmap (fmap (defvar_stoc (gs := gs))) dd (Ctx := AEC) gc tt) as (Hin1 & Hin2).
      split; intros s Hins.
      - apply ape_state_fmap, Hin1, Hins.
      - apply Hin2, ape_state_fmap, Hins.
    }
    symmetry.
    transitivity
      (fun s =>
         pe_interp gc tt (interp_abstract dd)
                   (ve_interp s ∘
                              (mbind ((inst_DefVar (SVar gs) _ _ (list Tag) (flattened_protos _ gs.(gs_protos))
                                                   (flatten_lowered_view
                   (interpret_low
                      (forget_lnorm
                         (lower_goal gc
                                     (exist (fun x : GSAPredDef gs => List.In x (gs_definer gs))
                                            {| apd_tag := dt; apd_def := dd |} Hdef_in) l l' g))))))))
                   (PredEx := ape_PredEx _ (EqPredEx := E_Ep))
).
    {
      split; intros s Hins; unfold Included, In in *.
      - apply ape_state_fmap, aemap_fmap, Hins.
      - apply aemap_fmap, ape_state_fmap, Hins.
    }
    apply pe_interp_compose_same_set.
    intros s expv.
    unfold compose.
    rewrite ve_state_mbind, ve_state_fmap.
    apply ve_interp_equiv.
    intro v.
    unfold compose.
    unfold make_condvar_state_npi.
    destruct v as [(v & Hv) | n].
    - (* Shared state references *)
      cbn.
      apply ve_interp_mreturn.
    - (* Goal variable references *)
      unfold inst_DefVar, ap_argv, proj1_sig at 1.
      rewrite (simplify_nth_of_goal gc l l').
      cbn.
      unfold eval_lift, compose.
      unfold goal_state_well_formed in Hwf.
      specialize (Hwf n).
      destruct (get_argc dt <=? n) eqn:Hinboundsb.
      + (* Out of bounds. *)
        apply Nat.leb_le in Hinboundsb.
        rewrite (Hwf Hinboundsb), nth_overflow.
        * apply ve_bot_same.
        * rewrite <- concat_map, map_length, concat_length, inst_argvs_from_argcs_length.
          now rewrite <- unflatten_tags_argc.
      + (* In bounds. *)
        now apply goal_vars_equiv_inbounds, Nat.ltb_ge.
  Qed.

  (** Building the weakest-precondition view in the GStarling way weakens the
      defining-views equivalent. *)
  Lemma gs_wpre_weaken (gc : GCtx)
        (g g' : LMultiset (LowAtom gs))
        (def : { x : GSAPredDef gs | List.In x (gs_definer gs) })
        (op oq : GSVExpr gs (LVar gs))
        (l l' : GSLStP)
        (s : GSSStP):
  lmeq g g' ->
  is_exact_match g' g (apd_tag (` def)) ->
  goal_state_well_formed (shadow_goal (unbag g')) (get_argc (apd_tag (` def))) ->
  (dreify (LowView gs) GSSStP (gs_sem_define gc)
          (lmunion (view_interp op l) (lmminus g (view_interp oq l'))) s) ->
  pe_interp gc (l, l', (shadow_goal (unbag g')))
    (gs_reify_wpre gctx_witness gs
       (build_wp gctx_witness op oq def))
    s.
  Proof.
    intros Hlmeq Hmatch Hgswf Hdviews.
    (* Get rid of the lifting we have on pe_interp first. *)
    red.
    unfold PredEx_Cond_PrfIrr.
    apply iconj_flat_map, iconj_Forall, Forall_forall.
    unfold definer_lift_vars, gd_reify_single.
    (* There are lots of mappings and quantifiers that we need to
       unravel before we get to a view. *)
    intros x (y & <- & (defn & <- & Hdefn)%in_map_iff)%in_map_iff.
    apply iconj_Forall, Forall_forall.
    intros x (v & <- & Hv)%in_map_iff.
    (** The goal now reduces to an implication on the guard, at which
        point we can assume that the guard is true. *)
    unfold inst_gmatch, inst_GAPreds, inst_GAPred.
    apply (pe_impl_cond (ImplPredEx := ImplPredEx_AbstractExpr (Val := Val))).
    intro Hguard_true.
    unfold In in *.
    simpl in *.
    (* Show that we can strip out the various maps applied to [v].
       First, we get rid of the preprocessing, giving us a view that
       has symbol-free guards. *)
    pose proof (Forall_forall_in
                  _ _ _
                  (g_pattern_matches_map_back Tag Val (DMLVar gs) (CondVar gs) Ev Ev
                                              (GSGuardExpr VBop EBop VMop EMop)
                                              (AbstractExpr Symb VBop EBop VMop EMop)
                                              gs.(gs_protos) _ _ _ _)
                  Hv

               ) as (v_g & Hv_g & ->)%Exists_exists.

    
assert (
     forall
    (e : GSGuardExpr VBop EBop VMop EMop
           (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs)))
    (s0 : @DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs -> EVal Ev),
  {@pe_interp
     (GSGuardExpr VBop EBop VMop EMop (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs)))
     unit unit (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs -> EVal Ev)
     (@fun_Setoid (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs) 
        (EVal Ev) (@Setoid_EVal Val Ev S_Val V_Ev))
     (@PredEx_EVal
        (GSGuardExpr VBop EBop VMop EMop
           (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs))) unit unit
        (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs) Val Ev S_Val V_Ev
        (@PredEx_Domain_Chained
           (GSGuardExpr VBop EBop VMop EMop
              (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs))) unit unit
           (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs) Val Ev S_Val V_Ev
           (@ape_PredEx Val unit unit (GSGuardExpr VBop EBop VMop EMop) S_Val 
                        (guard_ctxless_EqPredEx_Val gctx_witness)
              (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs))))) tt tt e s0} +
  {~
   @pe_interp
     (GSGuardExpr VBop EBop VMop EMop (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs)))
     unit unit (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs -> EVal Ev)
     (@fun_Setoid (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs) 
        (EVal Ev) (@Setoid_EVal Val Ev S_Val V_Ev))
     (@PredEx_EVal
        (GSGuardExpr VBop EBop VMop EMop
           (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs))) unit unit
        (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs) Val Ev S_Val V_Ev
        (@PredEx_Domain_Chained
           (GSGuardExpr VBop EBop VMop EMop
              (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs))) unit unit
           (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs) Val Ev S_Val V_Ev
           (@ape_PredEx Val unit unit (GSGuardExpr VBop EBop VMop EMop) S_Val 
                        (guard_ctxless_EqPredEx_Val gctx_witness)
              (Ev (@DMLVar Tag Var Ep Ev Val Symb VBop EBop VMop EMop C gs))))) tt tt e s0})
      as Hgs_pred_dec_alt.
{
  intros e s'.
  unfold pe_interp.
  cbn.
  unfold compose in *.
  apply Hgs_pred_dec.
}

Set Printing Implicit.
Set Printing Universes.

    (* Now to show that [v] is a merged subview of the high-level
       weakest precondition. *)
epose (g_pattern_matches_subview _ _ (DMLVar gs) (GSGuardExpr VBop EBop VMop EMop) _ gs.(gs_protos) (V_Ev := V_Ev) (T_Tag := T_Tag) Hgs_pred_dec_alt (` (apd_tag defn)) (build_wp gctx_witness op oq def (T_Tag := T_Tag))) as blep.
epose proof (proj1 (Forall_forall _ _) blep _ Hv_g).
    epose (Forall_forall_in _ v_g _ (g_pattern_matches_subview _ _ (DMLVar gs) (GSGuardExpr VBop EBop VMop EMop) _ gs.(gs_protos) (V_Ev := V_Ev) (T_Tag := T_Tag) Hgs_pred_dec_alt (` (apd_tag defn)) (build_wp gctx_witness op oq def (T_Tag := T_Tag))) Hv_g
          ).


    unfold g_pattern_matches in Hv.
    apply in_map_iff in Hv.
    destruct Hv as (vxs & <- & Hin).
    unfold preprocess_wpre in Hin.
    rewrite lvmap_map_listify in Hin.
    assert
      (forall x : GSGAPred gs (DMLVar gs),
          tag_of x = tag_of (preprocess_apred gctx_witness gs x)
      )
      as Htag_of by intuition.
    pose proof (Forall_forall_in _ _ _ (pattern_matches_map_back _ _ Htag_of _ _) Hin)
         as (vxs'' & Hin_vxs'' & ->)%Exists_exists.
    clear Hin.
    (* [vxs] is now known to be a lowered pattern match of the weakest precondition. *)


    unfold pe_interp in f.
    

    specialize (Hdviews v).
    simpl.

    rewrite flat_map_concat_map, map_map.

  (** We now get to the main part of the soundness proof: showing that the
      GStarling template strengthens the rearranged version of the definer. *)

  Lemma gs_strengthens_defining (gc : GCtx) :
    strengthens _ _ _
                (gs_sig gc)
                (gs_template gc)
                (defining_views_template_ni
                   _ _ _
                   (gs_sem_define gc)).
  Proof.
    intros ax (ax' & Hax' & Hin) (g & Hg_def) s' (s & Hs & Htrans).
    cbn in *.
    unfold Included, gs_backend_decomp, gs_build_vcs in Hin.

    (* Simplify the definer on the RHS. *)
    apply gs_v_sem_define_same.
    (* Work out what the lowered axiom actually is.*)
    destruct ax as [lp [|((lc & l) & l')] lq], ax' as [op oc oq]; intuition.
    destruct Hax' as (pp & qq & <- & <- & Hax'%Singleton_inv).
    (* We've finally revealed that the lowered axiom is a
       [gs_views_decomp_upper] of the original axiom! *)
    inversion Hax'; subst; clear Hax'.
    unfold gs_v_define, interp_def, compose, gs_define_lowered_to_expr.
    (* The backend conditions each target a single definition of the goal view
       [g], so we split the lowered goal accordingly. *)
    apply (proj2 (iconj_Forall _ _ _ s' (P_Ep := PredEx_Regular_PrfIrr))).
    apply Forall_forall.
    intros gp (gd & Hin_gd & Hin_gp)%in_flat_map.
    (* As in LoStarling, we now build a witnessing backend condition. *)
    remember
      (gs_build_vcs_single gctx_witness
                           (<| op |> lc <| oq |>)
                           (exist _ gd Hin_gd)) as wit.
    assert (In _ (gs_backend_decomp <| op |> lc <| oq |>) wit) as Hwit.
    {
      subst.
      apply in_map_prf, gs_build_vcs_single_prfirr.
    }
    (* This then means that we can use the covering property to work out that
       [bvhoare] holds for [wit]. *)
    specialize (Hin wit Hwit s s').
    clear Hwit.
    subst.
    (* We now need a goal state.  To get it, we first find a view [g'] which
       corresponds to an exact match of [g], has all of the tags in the correct
       place, and can be shadowed. *)
    unfold gs_define_lowered_single in Hin_gp.
    apply in_map_iff in Hin_gp.
    destruct Hin_gp as (a & <- & Hin_gp).
    apply In_InA with (eqA := eqbagA) in Hin_gp.
    2: apply eqbagA_Equivalence.
    destruct (has_exact_match_is_lowering gc (extract_pfun l) (extract_pfun l') (exist _ gd Hin_gd) Hin_gp) as (g' & Hg_eq & Ha_eq & Hg_lowering).
    (* Show that the shadowing state, [gst], is well-formed with
       regards to the goal's definition [gd]. *)
    pose (shadow_goal (unbag g')) as gst.
    symmetry in Ha_eq.
    pose proof (is_exact_match_eqbag Hin_gp Ha_eq) as Hin_gp'.
    pose proof (shadow_goal_well_formed_match_wit Hin_gp') as Hgst.
    (* As the view we're instantiating in the goal is equivalent to
       the shadowed construction, so is its interpretation. *)
    apply (inst_lowered_match_Proper gd (symmetry Ha_eq)).
    apply (inst_lowered_match_Proper gd (symmetry Hg_lowering)).
    clear Ha_eq.
    clear Hg_lowering.
    (** Because the shadowing state is well-formed, we can
        convert the instantiation of the match for [gd] into
        a direct instantiation of the goal. *)
    apply (interp_cond_interp_def gc l l' (exist _ gd Hin_gd) (g := (shadow_goal (unbag g'))) Hgst).
    (** We can now apply [Hin]. *)
    apply (Hin (l, l', (shadow_goal (unbag g')))); clear Hin.
    2:{  (* State transformation *)
      destruct lc.
      - apply re_id_id_MarkedPredEx.
        apply Singleton_inv in Htrans.
        injection Htrans.
        now intros -> ->.
      - apply Htrans.
      }
    (* Weakest precondition *)
    unfold In in *.
    simpl.
  Abort.
End gstarling_soundness.