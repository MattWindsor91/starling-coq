(** * Product instance transformers for PredEx and RelEx *)

From Coq Require Import
     Classes.RelationClasses
     Classes.Morphisms
     Sets.Ensembles
     Sets.Constructive_sets
     Program.Basics.

From Starling Require Import
     Utils.Product
     Backend.Classes.

Set Implicit Arguments.

Section transformers_product.

  Variables
    GCtxA GCtxB (* Global contexts *)
    LCtxA LCtxB (* Local contexts *)
  : Type.
  
  Variable
    SStA SStB (* Shared states *)
    : Set.

  (** [PredEx_Prod_Disjoint] lets us combine two predicates on disjoint
      state models into a single predicate. *)
  
  Global Instance PredEx_Prod_Disjoint (EpA EpB : Type)
         {P_EpA : PredEx EpA GCtxA LCtxA SStA}
         {P_EpB : PredEx EpB GCtxB LCtxB SStB}         
    : PredEx (EpA * EpB) (GCtxA * GCtxB) (LCtxA * LCtxB) (SStA * SStB) :=
    {|
      pe_true := (pe_true, pe_true);
      pe_conj := prodmap2 pe_conj pe_conj;
      pe_impl := prodmap2 pe_impl pe_impl;
      pe_interp '((ga, gb) : GCtxA * GCtxB) '((la, lb) : LCtxA * LCtxB) '((ea, eb) : EpA * EpB) :=
        fun '(sa, sb) => In _ (pe_interp ga la ea) sa /\ In _ (pe_interp gb lb eb) sb
    |}.
  Proof.
    - (* pe_true_unit *)
      intros (ga & gb) (la & lb) (sa & sb).
      split; apply pe_true_unit.
    - (* pe_conj_distr *)
      intros (ga & gb) (la & lb) (ep1a & ep1b) (ep2a & ep2b).
      split; intros (sa & sb).
      unfold In in *.
      + intros ((Hsa1&Hsa2)%pe_conj_distr%Intersection_inv & (Hsb1&Hsb2)%pe_conj_distr%Intersection_inv).
        repeat split; assumption.
      + intros ((Hsa1&Hsb1)&(Hsa2&Hsb2))%Intersection_inv.
        split; apply pe_conj_distr; split; assumption.
    - (* modus ponens *)
      intros (ga & gb) (la & lb) (ep1a & ep1b) (ep2a & ep2b) (sa & sb)
             (Hsa%pe_impl_modus_ponens & Hsb%pe_impl_modus_ponens).
      split; assumption.
    - (* conditional proof *)
      intros (ga & gb) (la & lb) (ep1a & ep1b) (ep2a & ep2b) (sa & sb) Hcond.
      simpl.
      split.
      + apply pe_impl_cond.
        intros Hsa.
        apply Hcond.
        split.

      split; assumption.      
      intros gc (l & le) e1 e2.
      apply pe_impl_cond.
  Defined.

  Global Instance RelEx_Fun (Er : Type) {R_Er : RelEx Er GCtx LCtx_Ex SSt}
    : RelEx (LCtx -> Er) GCtx (LCtx * LCtx_Ex) SSt :=
    {|
      re_id := const re_id;
      re_empty := const re_empty;

      re_interp (g : GCtx) '((l, le) : LCtx * LCtx_Ex) (f : LCtx -> Er) :=
        re_interp g le (f l)
    |}.
  Proof.
    - (* re_id_id *)
      intros gc (l & le).
      apply re_id_id.
    - (* re_empty_empty *)
      intros gc (l & le).
      apply re_empty_empty.      
  Defined.

  Section transformers_function_erase.
    (** ** Erasing functions in backend conditions

        If the predicate and relation expressions in a backend condition are
        functions, we can lift out the functions by moving the quantifications
        into the condition sets.  In general, this doesn't give us a finite
        condition set! *)

    Variables
      Ep (* Predicate expressions *)
      Er (* Relation expressions *)
    : Type.

    Context
      {P_Ep : PredEx Ep GCtx LCtx_Ex SSt}
      {R_Er : RelEx Er GCtx LCtx_Ex SSt}.      
    
    Definition fun_erase
               (xs: Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er))) :
      Ensemble (BVCond Ep Er) :=
      (fun (x : BVCond Ep Er) =>
         exists (x' : BVCond (LCtx -> Ep) (LCtx -> Er)) (l : LCtx),
           Ensembles.In _ xs x' /\
           x.(bv_w) = x'.(bv_w) l /\
           x.(bv_c) = x'.(bv_c) l /\
           x.(bv_g) = x'.(bv_g) l).

    (** The erasure of the empty set is the empty set. *)
    
    Lemma fun_erase_Empty: Same_set _ (fun_erase (Empty_set _)) (Empty_set _).
    Proof with contradiction.
      split; intros s.
      - intros (x & _ & Hin & _)...
      - intro Hoops...
    Qed.

    (** Local erasure distributes across [Union]. *)
    
    Lemma fun_erase_Union (xs ys: Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er))):
      Same_set _
               (fun_erase (Union _ xs ys))
               (Union _ (fun_erase xs) (fun_erase ys)).
    Proof.
      split; intros x.
      - intros (x' & l & [Hin|Hin]%Union_inv & Hw & Hc & Hg).
        + left.
          exists x'; exists l.
          repeat split; assumption.
        + right.
          exists x'; exists l.
          repeat split; assumption.
      - intros [(x' & l & Hin & Hw & Hc & Hg)|
                (x' & l & Hin & Hw & Hc & Hg)]%Union_inv;
          exists x'; exists l; repeat split; try assumption.
        + left.
          exact Hin.
        + right.
          exact Hin.
    Qed.
    
    Lemma fun_erase_Proper: Proper (Same_set _ ==> Same_set _) fun_erase.
      intros xs ys Hsame.
      split;
        intros s (x & l & Hin & Hw & Hc & Hg);
        exists x; exists l;
          repeat split;
          try assumption;
          apply Hsame, Hin.
    Qed.

    Lemma bvhoare_fun_erase (gc : GCtx) (xs : Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er))) :
      Included _ xs             (bvhoare gc) <->
      Included _ (fun_erase xs) (bvhoare gc).      
    Proof.
      split.
      - intros Hloc x (x' & l & Hin & Hw & Hc & Hg) s s' le.
        specialize (Hloc x' Hin s s' (l, le)).
        simpl in Hloc.
        rewrite <- Hw, <- Hc, <- Hg in Hloc.
        apply Hloc.
      - intros Herased x Hin_x s s' (l & le).
        apply (Herased
                 {| bv_w := x.(bv_w) l;
                    bv_c := x.(bv_c) l;
                    bv_g := x.(bv_g) l |}).
        exists x, l.
        repeat split.
        exact Hin_x.
    Qed.
    
    Corollary ideal_solve_fun_erase (xs: Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er))) :
      ideal_solve xs <->
      ideal_solve (fun_erase xs).
    Proof.
      split.
      - intros (a & Hloc).
        exists a.
        apply bvhoare_fun_erase.
        intros x.
        apply Hloc.
      - intros (a & Herased).
        exists a.
        apply bvhoare_fun_erase.
        intros x.
        apply Herased.
    Qed.
  End transformers_function_erase.  
End transformers_function.