From Coq Require Import
     Classes.SetoidClass.

From Views Require Import
     Framework
     Language
     Monoids
     Setof.

From Starling Require Import
     Views.Classes
     Views.Frameworks.Common     
     Views.Frameworks.CVF.Core
     ProofRules
     ProgramProof.

(** * Compatibility with the Views framework Coq development

    This chapter maps each [SoundViewsInstance] to an axiom-sound
    instance in the original Coq development. *)

Module ViewsCoq_Compat.
  Parameters
    V   (* Views *)
  : Type.
  
  Parameters
    C   (* Atomic action *)
    S   (* State *)
    : Set.

  Declare Instance SV: Setoid V.
  Declare Instance VV: ViewsSemigroup V.
  
  Parameter IV: ViewsInstance C S.
  
  Instance SS: Setoid S := { equiv := eq }.

  Definition st: C -> S -> S -> Prop := sig_sem (proj1_sig IV).

  Lemma compat_st_morph (c: C): Proper (eq ==> eq ==> iff) (st c).
  Proof.
    intros a b -> x y ->.
    reflexivity.
  Qed.

  Definition compat_interp (c: C): ST :=
      {| st_trans := st c;
         st_morph := compat_st_morph c |}.  
  
  Module CompatAtomics <: Language.Atomics.
    Definition Atomic := C.
  End CompatAtomics.

  Module CompatSyntax <: Language.BasicSyntaxT CompatAtomics :=
    Language.BasicSyntax CompatAtomics.

  Module CompatSyntaxTransformers <: StateTransformers CompatAtomics.
    Definition State := S.
    Definition state_setoid := SS.
    Definition interp := compat_interp.
  End CompatSyntaxTransformers.

  Instance Compat_SemiGroup: SemiGroup dot :=
    { op_assoc := dot_assoc }.

  Instance Compat_Commutative: Commutative dot :=
    { op_commut := dot_comm }.

  Definition Compat_erase (v: V) : Setof :=
    mkSetof (proj1_sig (proj1_sig IV).(sig_reifier) v).

  Lemma Compat_erase_morphism
    : Proper (SetoidClass.equiv ==> SetoidClass.equiv) Compat_erase.
  Proof.
    intros x y Hxy s.
    simpl.
    split.
    - intros (b & Hr & Hb).
      exists b.
      split.
      + apply (proj2_sig (sig_reifier (proj1_sig IV)) x y Hxy), Hr.
      + exact Hb.
    - intros (b & Hr & Hb).
      exists b.
      split.
      + apply (proj2_sig (sig_reifier (proj1_sig IV)) y x (symmetry Hxy)), Hr.
      + exact Hb.
  Qed.
  
  Module CompatSafetyParams <: SafetyParams CompatAtomics CompatSyntaxTransformers.
    Definition V := V.
    Definition V_setoid := SV.
    Definition op := dot.
    Definition op_monoid := Compat_SemiGroup.
    Definition op_comm := Compat_Commutative.
    Definition erase := Compat_erase.
    Definition erase_morphism := Compat_erase_morphism.
  End CompatSafetyParams.
  
  Module CompatSafety :=
    Safety CompatAtomics CompatSyntax CompatSyntaxTransformers CompatSafetyParams.

  Import CompatSafety.

  (* Axiom soundness in the Starling version of Views implies axiom
     soundness in the original. *)

  Lemma compat_action_judgement_id (a: ViewsAxiom V C):
    cmd a = Id _ ->
    s_axiom_judgement (v_sig (proj1_sig IV)) a ->
    aimpl_wm (pre a) (post a).
  Proof.
    intros Hid (Hpq & Hpqv).
    rewrite Hid in *.
    split.
    - intros s (_ & Hs_p & <-).
      exists s.
      split.
      + apply Hpq.
        exists s.
        split.
        * apply Hs_p.
        * reflexivity.
      + reflexivity.
    - intros r s (_ & Hs_p & <-).
      exists s.
      split.
      + apply Hpqv.
        exists s.
        split.
        * apply Hs_p.
        * reflexivity.
      + reflexivity.
  Qed.

  Lemma compat_action_judgement_cmd (a: ViewsAxiom V C) (c: C):
    cmd a = Cmd c ->
    s_axiom_judgement (v_sig (proj1_sig IV)) a ->
    asat_wm (pre a) (compat_interp c) (post a).
  Proof.
    intros HCmd (Hpq & Hpqv).
    rewrite HCmd in *.
    split.
    - intros s' (s & (_ & Hs_p & <-) & Hs_s').
      exists s'.
      split.
      + apply Hpq.
        exists s.
        split; assumption.
      + reflexivity.
    - intros r s' (s & (_ & Hs_p & <-) & Hs_s').
      exists s'.
      split.
      + apply Hpqv.
        exists s.
        split; assumption.
      + reflexivity.
  Qed.
  
  Theorem compat_axiom_sound
          (a: {x: ViewsAxiom V C | Ensembles.In _ (v_axioms (proj1_sig IV)) x}):
      match (cmd (proj1_sig a)) with
      | Id _ =>
        aimpl_wm (pre (proj1_sig a))
                 (post (proj1_sig a))
      | Cmd c =>
        asat_wm (pre (proj1_sig a))
                (compat_interp c)
                (post (proj1_sig a))
      end.
  Proof.
    destruct (cmd (proj1_sig a)) eqn:Hc.
    - apply compat_action_judgement_id.
      + exact Hc.
      + apply (proj2_sig IV).
    - apply compat_action_judgement_cmd.
      + exact Hc.
      + apply (proj2_sig IV).
  Qed.

  Import CompatSyntax.
  
  Fixpoint syntax_map (o: Outline V C): Syntax :=
    match o with
    | OSkip _ _ => skip
    | OAtomic _ c _ => atom c
    | OFrame _ _ c _ => syntax_map c
    | OIter _ c _ => loop (syntax_map c)
    | OBin _ c d BPar _ => parallel (syntax_map c) (syntax_map d)
    | OBin _ c d BSeq _ => sequence (syntax_map c) (syntax_map d)
    | OBin _ c d BNdt _ => choice (syntax_map c) (syntax_map d)
    end.

  Local Open Scope outline_scope.
  Local Open Scope views_scope.  

  Lemma outline_sound_impl (o: Outline V C) (p q: V):
    (proj1_sig IV) |- o -> in_vcs o <|p|> (Id _) <|q|> -> aimpl_wm p q.
  Proof.
    intros Hsound Hin_vcs.
    refine (compat_axiom_sound (exist _ <| p |> (Id _) <| q |> _)).
    apply Hsound, Hin_vcs.
  Qed.
  
  Corollary compat_safe:
    forall o: Outline V C,
    (proj1_sig IV) |- o ->
    Safe (outpre o) (syntax_map o) (outpost o).
  Proof.
    intros o Haxsound.
    induction o.
    - (* Skip *)
      apply Safe_raimpl with (q' := p).
      + apply Safe_skip.
      + refine (compat_axiom_sound (exist _ (mk_ViewsAxiom p (Id _) q) _)).
        apply Haxsound, skip_axiom.
    - (* Atomic *)
      apply Safe_asat_wm.
      refine (compat_axiom_sound (exist _ (mk_ViewsAxiom p (Cmd c) q) _)).
      apply Haxsound, atomic_axiom.
    - (* Iteration *)
      apply Safe_laimpl with (p' := outpre o).
      + refine (compat_axiom_sound (exist _ (mk_ViewsAxiom p (Id _) _) _)).
        apply Haxsound, List.in_or_app.
        right.
        left.
        reflexivity.
      + apply Safe_raimpl with (q' := outpre o).
        * apply Safe_loop.
          apply Safe_raimpl with (q' := outpost o).
          -- apply IHo, (outline_sound_iter Haxsound).
          -- refine (outline_sound_impl (OIter p o q) _ _ Haxsound _).
             apply List.in_or_app.
             right.
             right.
             right.
             left.
             reflexivity.
        * refine (outline_sound_impl (OIter p o q) _ _ Haxsound _).
          apply List.in_or_app.
          right.
          right.
          left.
          reflexivity.
    - (* Frame rule *)
      refine (Safe_aimpl ((outpre o * f)%views) p _ _ _ (reflexivity _) (outpost o * f)%views q _ _).
      + refine (compat_axiom_sound (exist _ (mk_ViewsAxiom _ (Id _) _) _)).
        apply Haxsound, List.in_or_app.
        right.
        left.
        reflexivity.        
      + refine (compat_axiom_sound (exist _ (mk_ViewsAxiom _ (Id _) _) _)).
        apply Haxsound, List.in_or_app.
        right.
        right.
        left.
        reflexivity.
      + apply Safe_frame, IHo, (outline_sound_frame Haxsound).
    - (* Binary operations *)
      destruct b; simpl.
      + (* Parallel *)
        refine (Safe_aimpl (outpre o1 * outpre o2) p _
                           _ _ (reflexivity _)
                           (outpost o1 * outpost o2) q _ _)%views.
        * refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
          apply List.in_or_app.
          right.
          left.
          reflexivity.
        * refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
          apply List.in_or_app.
          right.
          right.
          left.
          reflexivity.
        * apply Safe_para.
          -- apply IHo1, (proj1 (outline_sound_bin Haxsound)).
          -- apply IHo2, (proj2 (outline_sound_bin Haxsound)).      
      + (* Sequential *)
        refine (Safe_aimpl (outpre o1) p _
                           _ _ (reflexivity _)
                           (outpost o2) q _ _)%views.
        * refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
          apply List.in_or_app.
          right.
          left.
          reflexivity.
        * refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
          apply List.in_or_app.
          right.
          right.
          right.
          left.
          reflexivity.
        * apply Safe_seq with (r := (outpost o1)).
          -- apply IHo1, (proj1 (outline_sound_bin Haxsound)).
          -- apply Safe_laimpl with (p' := (outpre o2)).
             ++ refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
                apply List.in_or_app.
                right.
                right.
                left.
                reflexivity.
             ++ apply IHo2, (proj2 (outline_sound_bin Haxsound)).
      + (* Nondeterministic choice *)
        apply Safe_choice.
        * refine (Safe_aimpl (outpre o1) p _
                             _ _ (reflexivity _)
                             (outpost o1) q _ _)%views.
          -- refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
             apply List.in_or_app.
             right.
             left.
             reflexivity.
          -- refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
             apply List.in_or_app.
             right.
             right.
             right.
             left.
             reflexivity.
          -- apply IHo1, (proj1 (outline_sound_bin Haxsound)).
        * refine (Safe_aimpl (outpre o2) p _
                             _ _ (reflexivity _)
                             (outpost o2) q _ _)%views.
          -- refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
             apply List.in_or_app.
             right.
             right.
             left.
             reflexivity.
          -- refine (outline_sound_impl (OBin p o1 o2 _ q) _ _ Haxsound _).
             apply List.in_or_app.
             right.
             right.
             right.
             right.
             left.
             reflexivity.             
          -- apply IHo2, (proj2 (outline_sound_bin Haxsound)).
  Qed.
End ViewsCoq_Compat.
