(** Common inference rules

    When we have a compositional backend interface and sound frontend, we can
    express many of the Views program logic inference rules. *)

From Coq Require Import
     Classes.SetoidClass
     Lists.List
     Program.Basics
     Sets.Ensembles.

From Starling Require Import
     ProgramProof
     Backend.Classes
     Frontend.Common
     Utils.List.Facts
     Views.Classes
     Views.Frameworks.Common     
     Views.Frameworks.CVF.Core.

Local Open Scope frontend_scope.
Local Open Scope outline_scope.
Local Open Scope views_scope.  

Section starling_inference_rules.  
  Variables
    (Va Vf Ep Er GCtx LCtx : Type)
    (Ca Cf SSt             : Set)
    (T                     : Type -> Type).

  Context
    {SVa   : Setoid Va}
    {VVa   : ViewsSemigroup Va}
    {SVf   : Setoid Vf}
    {VVf   : ViewsSemigroup Vf}    
    {T_Slk : Setlike T}
    {B : Backend Ep Er GCtx LCtx SSt}
    {F : Frontend Va Vf Ep Er LCtx GCtx Ca Cf SSt T}
    {CB: CompBackend}.

  Lemma bi_solve_flat_decomp (xs ys: list (ViewsAxiom Va Ca)):
    bi_solve (flat_map_to_set fr_backend_decomp_set (xs ++ ys))
    <->
    bi_solve (Union _
                    (flat_map_to_set fr_backend_decomp_set xs)
                    (flat_map_to_set fr_backend_decomp_set ys)).
  Proof.
    split.
    - eapply bi_solve_same.
      apply flat_map_to_set_app.    
    - eapply bi_solve_same.
      symmetry.
      apply flat_map_to_set_app.
  Qed.

  Lemma bi_solve_flat_decomp_cons (x: ViewsAxiom Va Ca) (xs: list (ViewsAxiom Va Ca)):
    bi_solve (flat_map_to_set fr_backend_decomp_set (x::xs))
    <->
    bi_solve (Union _
                    (fr_backend_decomp_set x)
                    (flat_map_to_set fr_backend_decomp_set xs)).
  Proof.
    split.
    - eapply bi_solve_same.
      apply flat_map_to_set_cons.    
    - eapply bi_solve_same.
      symmetry.
      apply flat_map_to_set_cons.
  Qed.  

  Lemma bi_solve_flat_decomp_nil :
    bi_solve (flat_map_to_set fr_backend_decomp_set (nil : list (ViewsAxiom Va Ca))).
  Proof.
    apply bi_solve_same with (xs := Empty_set _).
    - split; intros s Hin.
      + contradiction.
      + apply Exists_nil in Hin.
        contradiction.
    - apply cbi_solve_empty.
  Qed.  
  
  Lemma lg_bin (b : BinOp) (c1 c2 : Outline Va Ca) (p q : Va) :
    bi_solve (flat_map_to_set fr_backend_decomp_set (map to_id_vc (bin_side_vcs b p q c1 c2))) ->
    |~ c1 ->
    |~ c2 ->
    |~ (OBin p c1 c2 b q).
  Proof.
    intros Hside_ok Hmc1 Hmc2.
    unfold lg_safe, compose, vcs, side_vcs.
    fold (side_vcs c1).
    fold (side_vcs c2).
    rewrite ! map_app.    
    repeat (apply bi_solve_flat_decomp, cbi_solve_union).
    - apply bi_solve_flat_decomp, cbi_solve_noinu in Hmc1.
      apply Hmc1.
    - apply bi_solve_flat_decomp, cbi_solve_noinu in Hmc2.
      apply Hmc2.
    - exact Hside_ok.
    - apply bi_solve_flat_decomp, cbi_solve_noinu in Hmc1.
      apply Hmc1.
    - apply bi_solve_flat_decomp, cbi_solve_noinu in Hmc2.
      apply Hmc2.
  Qed.

  (** Skip rule. *)

  Theorem lg_skip (p q : Va) :
    >>~ <|p|> (Id Ca) <|q|> ->
    |~ {{p}} skip {{q}}.
  Proof.
    intros Hskip.
    apply bi_solve_flat_decomp, cbi_solve_union.
    - apply bi_solve_flat_decomp_cons, cbi_solve_union.
      + exact Hskip.
      + apply bi_solve_flat_decomp_nil.
    - apply bi_solve_flat_decomp_nil.
  Qed.

  (** Atomic (and skip) rule. *)

  Theorem lg_atom (p q : Va) (c : Ca) :
    >>~ <|p|> (Cmd c) <|q|> ->
    |~ {{p}} <c> {{q}}.
  Proof.
    intros Hatom.
    apply bi_solve_flat_decomp, cbi_solve_union.
    - apply bi_solve_flat_decomp_cons, cbi_solve_union.
      + exact Hatom.
      + apply bi_solve_flat_decomp_nil.
    - apply bi_solve_flat_decomp_nil.
  Qed.

  (** Frame rule. *)

  Theorem lg_frame (c : Outline Va Ca) (p q f : Va) :
    >>~                <|p|> (Id _) <|c.(outpre) * f|> ->
    >>~  <|c.(outpost) * f|> (Id _) <|q|>              ->    
    |~ c -> |~ {{p}} frame f in c {{q}}.
  Proof.
    intros Hin Hout Hc.
    unfold "|~", compose, vcs, side_vcs in *.
    fold (side_vcs c) in *.
    rewrite bi_solve_flat_decomp in Hc.
    destruct (cbi_solve_noinu _ _ Hc) as (Hca & Hcs).
    rewrite ! map_app.
    repeat (apply bi_solve_flat_decomp, cbi_solve_union); try assumption.
    repeat (apply bi_solve_flat_decomp_cons, cbi_solve_union); try assumption.
    apply bi_solve_flat_decomp_nil.
  Qed.  
  
  (** Nondeterministic choice rule. *)
  
  Theorem lg_ndt (c1 c2 : Outline Va Ca) (p q : Va) :
    >>~            <|p|> (Id _) <|c1.(outpre)|> ->
    >>~            <|p|> (Id _) <|c2.(outpre)|> ->    
    >>~ <|c1.(outpost)|> (Id _) <|q|>           ->
    >>~ <|c2.(outpost)|> (Id _) <|q|>           ->    
    |~ c1 -> |~ c2 -> |~ {{p}} c1 + c2 {{q}}.
  Proof.
    intros Hc1_p Hc2_p Hc1_q Hc2_q.
    apply lg_bin.
    unfold bin_side_vcs.
    rewrite ! map_cons.
    repeat (apply bi_solve_flat_decomp_cons, cbi_solve_union); try assumption.
    apply bi_solve_flat_decomp_nil.
  Qed.

  (** Parallel composition rule. *)
  
  Theorem lg_par (c1 c2 : Outline Va Ca) (p q : Va) :
    >>~                           <|p|> (Id _) <|c1.(outpre) * c2.(outpre) |> ->
    >>~ <|c1.(outpost) * c2.(outpost)|> (Id _) <|q|>                          ->
    |~ c1 -> |~ c2 -> |~ {{p}} c1 || c2 {{q}}.
  Proof.
    intros Hp Hq.
    apply lg_bin.
    unfold bin_side_vcs.
    rewrite ! map_cons.
    repeat (apply bi_solve_flat_decomp_cons, cbi_solve_union); try assumption.
    apply bi_solve_flat_decomp_nil.
  Qed.

  (** Sequential composition rule. *)
  
  Theorem lg_seq (c1 c2 : Outline Va Ca) (p q : Va) :
    >>~            <|p|> (Id _) <|c1.(outpre)|> ->
    >>~ <|c1.(outpost)|> (Id _) <|c2.(outpre)|> ->
    >>~ <|c2.(outpost)|> (Id _) <|q|>           ->
    |~ c1 -> |~ c2 -> |~ {{p}} c1; c2 {{q}}.
  Proof.
    intros Hp Hr Hq.
    apply lg_bin.
    unfold bin_side_vcs.
    rewrite ! map_cons.
    repeat (apply bi_solve_flat_decomp_cons, cbi_solve_union); try assumption.
    apply bi_solve_flat_decomp_nil.
  Qed.

  (** Iteration rule. *)

  Theorem ms_iter (c : Outline Va Ca) (p q : Va) :
    >>~            <|p|> (Id _) <|c.(outpre)|> ->
    >>~   <|c.(outpre)|> (Id _) <|q|>          ->
    >>~  <|c.(outpost)|> (Id _) <|c.(outpre)|> ->
    |~ c -> |~ {{p}} c* {{q}}.
  Proof.
    intros Hin Habort Hloop Hc.
    unfold "|~", compose, vcs, side_vcs in *.
    fold (side_vcs c) in *.
    rewrite bi_solve_flat_decomp in Hc.
    destruct (cbi_solve_noinu _ _ Hc) as (Hca & Hcs).
    rewrite ! map_app.
    repeat (apply bi_solve_flat_decomp, cbi_solve_union); try assumption.
    repeat (apply bi_solve_flat_decomp_cons, cbi_solve_union); try assumption.
    apply bi_solve_flat_decomp_nil.
  Qed.
End starling_inference_rules.
