(** * LoStarling frontend and resulting logic *)

From Coq Require Import
     Classes.DecidableClass
     Classes.SetoidClass
     Lists.List
     Program.Basics
     Sets.Constructive_sets.

From Starling Require Import
     Utils.List.Facts
     Views.Classes
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core
     Views.Frameworks.LVF
     ProgramProof
     ProofRules
     Backend.Classes
     Backend.Transformers.Function
     Frontend.Common
     Frontend.SynDefiner.

Set Implicit Arguments.

Record LoStarling (V Ep Er : Type) (C LSt : Set) :=
  mk_LoStarling
    {
      ls_svd  : SynDefiner V Ep;
      ls_avd  : C -> (LSt * LSt) -> Er;
    }.

Local Open Scope backend_scope.
Local Open Scope program_scope.
Local Open Scope views_scope.

Section lostarling_outline_decompose.

  (** ** Decomposing proof outlines into backend verification conditions *)

  Variables
    (V Ep Er GCtx LCtx : Type)
    (C LSt SSt : Set).

  Context
    {SV : Setoid                   V}
    {VV : ViewsSemigroup           V}
    {OV : OrderedViewsSemigroup    V}
    {DV : DecOrderedViewsSemigroup V}
    {AV : AdjView                  V}
    {DC : forall l l' : LSt, Decidable (l = l')}
    `{BE : Backend Ep Er GCtx unit SSt}.

  Variable
    (ls : LoStarling V Ep Er C LSt).

  (** [lifted_id] lifts [re_id] correctly over local states by checking that
      the local states are equal.

      Since this must be done at the program level, we need said equality to
      be decidable.  This is a big restriction of LoStarling! *)

  Definition lifted_id '((l, l') : LSt * LSt) : Er :=
    if (decide (l = l'))
    then re_id (RelEx := bi_relex)
    else re_empty (RelEx := bi_relex).

  Lemma lifted_id_same (l : LSt) :
    lifted_id (l, l) = re_id (RelEx := bi_relex).
  Proof.
    unfold lifted_id.
    now decide (l = l).
  Qed.

  (** [ls_reify] is shorthand for [sd_syn_reify_expr]. *)

  Definition ls_reify (v : V) : Ep :=
    sd_syn_reify_expr v ls.(ls_svd).

  (** [ls_label_avd] defines a label using a LoStarling definer. *)

  Definition ls_label_avd (c : LabelC C) : (LSt * LSt) -> Er :=
    label_maybe lifted_id ls.(ls_avd) c.

  (** [ls_build_vc_ni] converts a wpre/goal Hoare triple to a [BVCond]. *)

  Definition ls_build_vc_ni (w : (LSt * LSt) -> V) (g : V) (c: LabelC C) :
    BVCond ((LSt * LSt) -> Ep) ((LSt * LSt) -> Er) :=
    << ls_reify ∘ apply w >>
       (ls_label_avd c)
    << const ((sd_syn_define_expr g ls.(ls_svd)) : Ep) >>.

  (** [ls_build_vc_pc] converts a partial-correctness Hoare triple to a
      [BVCond]. *)

  Definition ls_build_vc_pc (p q : LSt -> V) (c: LabelC C) :
    BVCond ((LSt * LSt) -> Ep) ((LSt * LSt) -> Er) :=
    << ls_reify ∘ apply p ∘ fst >> (ls_label_avd c) << ls_reify ∘ apply q ∘ snd >>.

  Definition ls_decomp_ni_single '(<| p |> c <| q |>) (g : V) :
    BVCond ((LSt * LSt) -> Ep) ((LSt * LSt) -> Er) :=
    (* Assign the pre-state to p, and the post-state to q. *)
    ls_build_vc_ni ((p ∘ fst) * ((const g) \ (q ∘ snd)))%views g c.

  (** [ls_decomp_ni_funcs] decomposes [ax] into a list of function [BVCond]s
      covering non-interference. *)

  Definition ls_decomp_ni_funcs (ax : ViewsAxiom (LSt -> V) C) :
    list (BVCond ((LSt * LSt) -> Ep) ((LSt * LSt) -> Er)) :=
    List.map (ls_decomp_ni_single ax ∘ (sd_view (Ep := Ep))) ls.(ls_svd).

  (** [ls_decomp_funcs] decomposes [ax] into a list of function [BVCond]s
      covering partial correctness and non-interference. *)

  Definition ls_decomp_funcs (ax : ViewsAxiom (LSt -> V) C) :
    list (BVCond ((LSt * LSt) -> Ep) ((LSt * LSt) -> Er)) :=
    (ls_build_vc_pc ax.(pre) ax.(post) ax.(cmd))::ls_decomp_ni_funcs ax.

  (** [ls_decomp_ni] decomposes an axiom into a set of normal [BVCond]s
      covering non-interference. *)

  Definition ls_decomp_ni : ViewsAxiom (LSt -> V) C -> Ensemble (BVCond Ep Er) :=
    (* We change from a list to a set because fun_erase is undecidable in
       general.  This is the main limitation of LoStarling, and the motivation
       for choosing more restricted systems later on *)
    fun_erase (Er := Er) ∘ list_to_set ∘ ls_decomp_ni_funcs.

  (** [ls_decomp] decomposes an axiom into a set of normal [BVCond]s
      covering partial correctness and non-interference. *)

  Definition ls_decomp : ViewsAxiom (LSt -> V) C -> Ensemble (BVCond Ep Er) :=
    (* See above *)
    fun_erase (Er := Er) ∘ list_to_set ∘ ls_decomp_funcs.
End lostarling_outline_decompose.

Section lostarling_views_instance.
  Variables
    (V Ep Er GCtx : Type)
    (LSt C SSt: Set).

  Context
    {SV : Setoid                   V}
    {VV : ViewsSemigroup           V}
    {OV : OrderedViewsSemigroup    V}
    {DV : DecOrderedViewsSemigroup V}
    {AV : AdjView                  V}
    {DC : forall l l' : LSt, Decidable (l = l')}
    `{BE : Backend Ep Er GCtx unit SSt}.

  Variable
    ls : LoStarling V Ep Er C LSt.


  (* TODO: move *)

  Definition lst_semantics (gc : GCtx) : Semantics C (LSt * SSt) :=
    fun c '(l, s) '(l', s') =>
      re_interp gc ((l, l'), tt) (ls.(ls_avd) c) s s'.

  Definition ls_lsig (gc : GCtx) : LocalSignature C LSt SSt :=
    {|
      lsig_reifier := sd_syn_reifier gc ls.(ls_svd);
      lsig_lsem    := lst_semantics gc;
    |}.

  Definition ls_sig : GCtx -> Signature (LabelC C * LSt * LSt) SSt :=
    lsig_erase (SSt := SSt) ∘ ls_lsig.

  Local Open Scope program_scope.

  Definition ls_views_decomp
    : ViewsAxiom (LSt -> V) C -> Ensemble (ViewsAxiom V (LabelC C * LSt * LSt)) :=
    axioms_lvf_to_cvf (LSt := LSt) ∘ Singleton _.

  (** The LoStarling non-interference/monoidal template is just the extension
      of its VC builder to a template... *)

  Definition ls_template_ni : GCtx -> Template V (LabelC C * LSt * LSt) SSt :=
    builder_to_template (ls_decomp_ni ls) ls_views_decomp.

  (** ...and similarly for its instance. *)

  Definition ls_PreInstance :
    GCtx ->
    PreInstance (LabelC C * LSt * LSt) SSt :=
    builder_to_instance (ls_decomp_ni ls) ls_views_decomp ls_sig.

  (** From the CVF instance, we can back-form a LVF instance. *)

  Definition ls_LVFInstance (gc : GCtx) : LVFInstance C LSt SSt :=
    {|
      lvfi_sig := ls_lsig gc;
      lvfi_axioms := axioms_cvf_to_lvf (ls_PreInstance gc).(v_axioms)
    |}.

  Local Open Scope views_scope.

  (** μStarling's non-interference template strengthens the defining
      views template. *)

  Lemma ls_strengthens_defining_ni (gc : GCtx) :
    strengthens _ _ _
                (ls_sig gc)
                (ls_template_ni gc)
                (defining_views_template_ni
                   _ _ _
                   (sd_define gc ls.(ls_svd))).
  Proof.
    intros ax (ax' & Hax' & Hin) (g & Hg_df) s' (s & Hs & Htrans).
    simpl in *.
    (* First, use the fact that [g] is defined to simplify the conclusion. *)
    unfold ls_template_ni, builder_to_template in Hin.
    apply sd_define_lift_syn_define.
    destruct ax as [p [|((c & l) & l')] q].
    - (* An axiom with the command as Id can't possibly exist here *)
      contradiction.
    - (* Let's work out what p and q are. *)
      destruct Hax' as (pp & qq & Hpp & Hqq & Hdecomp%Singleton_inv).
      pose (has_def_has_syn_def Hg_df (V := V)) as Hasd.
      apply Exists_exists in Hasd.
      destruct Hasd as (gd & Hgd_in & Hgd_matches).
      (* This will be the witnessing VC that backs this strengthen. *)
      remember (ls_decomp_ni_single ls ax' gd.(sd_view)) as fvc.
      unfold sd_syn_define, compose.
      (* Claim that this goal interpretation is backed by the VC [fvc]. *)
      replace (sd_syn_define_expr g (ls.(ls_svd))) with (fvc.(bv_g) (l, l')).
      + destruct ax' as [pf cf qf].
        injection Hdecomp.
        intros <- <- <-.
        refine (Hin {| bv_w := fvc.(bv_w) (l, l');
                       bv_c := fvc.(bv_c) (l, l');
                       bv_g := fvc.(bv_g) (l, l'); |}
                    _ s s' tt _ _).
        * (* We need to show that the above decomposition is a legitimate
             function erase of fvc. *)
          exists fvc, (l, l').
          repeat split.
          apply in_map_iff.
          exists gd.
          split.
          -- apply symmetry, Heqfvc.
          -- apply Hgd_in.
        * (* Now show that the weakest precondition is backed up by [Hs]. *)
          destruct gd as (gg & ge).
          rewrite Heqfvc.
          simpl.
          unfold ls_reify, compose, apply.
          unfold DProd.dot_DProd, DProd.sub_DProd, DProd.lift_op, In.
          erewrite (sd_syn_reify_expr_eq_Proper _ _).
          -- apply sd_reify_syn_reify, Hs.
          -- subst.
             apply equiv_dot_right, eqv_sub_congr, Hgd_matches.
        * (* And now, show that the transition is valid. *)
          destruct gd as (gg & ge).
          rewrite Heqfvc.
          destruct cf.
          -- (* Id *)
             apply Singleton_inv in Htrans.
             inversion_clear Htrans.
             simpl.
             change (if Decidable_witness then re_id else re_empty) with (lifted_id (l', l')).
             rewrite lifted_id_same.
             now apply re_id_id.
          -- (* Command *)
            apply Htrans.
      + rewrite Heqfvc.
        destruct gd as (gg & ge).
        subst.
        apply sd_syn_define_expr_eq_Proper, Hgd_matches.
  Qed.

  (** It thus strengthens the free template... *)

  Corollary ls_strengthens_free_ni (gc : GCtx) :
    strengthens _ _ _
                (ls_sig gc)
                (ls_template_ni gc)
                (free_template_ni V (LabelC C * LSt * LSt) SSt).
  Proof.
    intros x Hin.
    refine (adjoint_strengthens_free_reifier_inc_ni V _ _ _ _ x _).
    {
      intros a b Hinc.
      simpl.
      etransitivity.
      - apply sd_reify_syn_reify.
      - etransitivity.
        + apply dreifier_inc, Hinc.
        + apply sd_reify_syn_reify.
    }
    refine (defining_views_ni_strengthens_adjoint_ni
              V _ _
              (sd_define gc ls.(ls_svd)) (ls_sig gc) _ x  _).
    {
      intro v.
      apply sd_reify_syn_reify.
    }
    apply ls_strengthens_defining_ni, Hin.
  Qed.

  (** ...and, as a result, is axiom-sound. *)

  Corollary ls_axiom_sound {VM : ViewsMonoid V} (gc : GCtx) :
    axiom_sound (ls_PreInstance gc).
  Proof.
    apply instantiate_sound with (t2 := free_template _ _ _).
    - intros ax Hin.
      apply free_monoid_reduce.
      + exact VM.
      + apply ls_strengthens_free_ni, Hin.
    - apply free_template_axiom_sound.
  Qed.

  (** This also means that the LVF instance is locally axiom-sound. *)

  Corollary lst_local_axiom_sound {VM : ViewsMonoid V} (gc : GCtx) :
    local_axiom_sound (ls_LVFInstance gc).
  Proof.
    intros [p c q] Hin.
    apply slactionj_lactionj.
    intros l l'.
    unfold lactionj, vactionj.
    assert (In _
               (ls_PreInstance gc).(v_axioms)
               (<| p(l) |> (Cmd (c, l, l')) <| q(l') |>)) as Hin'.
    {
      destruct (Hin l l') as (pp & qq & -> & -> & Hin').
      apply Hin'.
    }
    refine (ls_axiom_sound (exist _ _ Hin')).
  Qed.
End lostarling_views_instance.

Section lostarling_frontend.
  Variables
    (V Ep Er GCtx : Type)
    (LSt C SSt: Set).

  Context
    {SV : Setoid                   V}
    {VV : ViewsSemigroup           V}
    {OV : OrderedViewsSemigroup    V}
    {DV : DecOrderedViewsSemigroup V}
    {AV : AdjView                  V}
    {DC : forall l l' : LSt, Decidable (l = l')}
    `{BE : Backend Ep Er GCtx unit SSt}.

  Variables
    (ls : LoStarling V Ep Er C LSt).

  (** LoStarling forms a sound, if undecidable, frontend. *)

  Global Instance LoStarling_Frontend :
    Frontend (LSt -> V) V Ep Er unit GCtx C (LabelC C * LSt * LSt) SSt
             Ensemble
    :=
      {|
        fr_views_decomp := ls_views_decomp (V := V) (C := C);
        fr_backend_decomp := ls_decomp_ni ls;
        fr_instance := ls_PreInstance ls;
      |}.
  Proof.
    intros [pa ca qa] gc Hall_hoare [pf [|((cf & l) & l')] qf] Hdecomp.
    - (* We got a CVF reduction with a command of 'id', which isn't possible. *)
      contradiction Hdecomp.
    - (* The decomposition will now tell us that there exists an assertion
         axiom that lines up with our framework axiom... but the fact that
         we're only decomposing <<pa>> ca <<qa>> will immediately tell us that
         said axiom is that one. *)
      destruct Hdecomp as (pp & qq & <- & <- & His_a%Singleton_inv).
      injection His_a.
      intros <- <- <-.
      (* Now, we have the specific CVF transformation of our assertion
         axiom in hand. *)
      exists (<|pa|> ca <|qa|>)%views.
      split.
      + now exists pa, qa.
      + assumption.
  Defined.

  (** [ls_outline_safe] is outline safety by applying the backend solver
      function to the flattened form of the verification conditions. *)

  Definition ls_outline_safe: LocalOutline C LSt (V := V) -> Prop :=
    bi_solve ∘ flat_map_to_set (ls_decomp_ni ls) ∘ vcs
             ∘ (proj1_sig (P := is_local_outline (LSt := LSt))).

  Definition ls_multi_outline_safe: list (LocalOutline C LSt (V := V)) -> Prop :=
    bi_solve ∘ flat_map_to_set (ls_decomp_ni ls)
             ∘ flat_map vcs
             ∘ map (proj1_sig (P := is_local_outline (LSt := LSt))).

  Local Open Scope views_scope.
  Local Open Scope outline_scope.

  (** If we have [ls_outline_safe], then we have [outline_sound]. *)

  Theorem ls_outline_safe_outline_safe (o : LocalOutline C LSt (V := V)):
    ls_outline_safe o ->
    exists g : GCtx,
      outline_sound (ls_LVFInstance ls g).(lvfi_axioms) (proj1_sig o).
  Proof.
    intro Hsafe.
    assert (lg_safe (proj1_sig o) (F := LoStarling_Frontend)) as Hlg
        by apply Hsafe.
    apply frontend_outline_safe in Hlg.
    destruct Hlg as (g & Hlg).
    exists g.
    intros [p lc q] Hin_vcs.
    apply Hlg in Hin_vcs.
    unfold fr_top_axioms in Hin_vcs.
    intros l l'.
    exists (p l), (q l'); intuition.
    apply Hin_vcs.
    now (exists p, q).
  Qed.

  Theorem ls_multi_outline_safe_outline_safe (os : list (LocalOutline C LSt (V := V))) :
    ls_multi_outline_safe os ->
    exists g : GCtx,
      Forall
        (outline_sound (ls_LVFInstance ls g).(lvfi_axioms) ∘ lo_to_o (LSt := LSt))
        os.
  Proof.
    intro Hall_safe.
    unfold ls_multi_outline_safe, compose in Hall_safe.
    apply bi_solve_ideal in Hall_safe.
    destruct Hall_safe as (g & Hsolve).
    exists g.
    apply Forall_forall.
    intros o Hin [p c q] Hin_vcs l l'.
    exists (p l), (q l').
    intuition.
    exists <| p |> c <| q |>.
    split.
    - now (exists p, q).
    - intros bc Hin_flat.
      apply Hsolve, Exists_exists.
      exists <| p |> c <| q |>.
      intuition.
      rewrite in_flat_map.
      exists (proj1_sig o).
      intuition.
      apply in_map, Hin.
  Qed.

  Corollary ls_outline_safe_lvf_safe
            {VM : ViewsMonoid V}
            (o : LocalOutline C LSt (V := V)) :
    ls_outline_safe o ->
    exists g : GCtx,
      SSafe (ls_LVFInstance ls g).(lvfi_sig)
            ((proj1_sig o).(outpre))
            (local_outline_to_prog o)
            ((proj1_sig o).(outpost)).
  Proof.
    intros (g & Hsafe)%ls_outline_safe_outline_safe.
    exists g.
    (* Show that the above instance is locally axiom-sound first. *)
    pose (exist _
                (ls_LVFInstance ls g)
                (lst_local_axiom_sound ls g)
         ) as li.
    assert (ls_LVFInstance ls g = proj1_sig li) as -> by reflexivity.
    apply local_outline_sound_safe, Hsafe.
  Qed.

  Corollary ls_multi_outline_safe_lvf_multi_safe
            {VM : ViewsMonoid V}
            (df : LSt)
            (os : list (LocalOutline C LSt (V := V))) :
    ls_multi_outline_safe os ->
    exists g : GCtx,
      MSafe ((ls_LVFInstance ls g).(lvfi_sig))
            df
            (map (outpre ∘ lo_to_o (LSt := LSt)) os)
            (map (outpost ∘ lo_to_o (LSt := LSt)) os)
            (map (local_outline_to_prog (LSt := LSt)) os).
  Proof.
    intros (g & Hsafe)%ls_multi_outline_safe_outline_safe.
    exists g.
    (* Show that our instance is locally axiom-sound first. *)
    pose (exist _
                (ls_LVFInstance ls g)
                (lst_local_axiom_sound ls g)
         ) as li.
    assert (ls_LVFInstance ls g = proj1_sig li) as -> by reflexivity.
    (* Now we can use apply the top parallel rule. *)
    apply all_SSafe_MSafe_bounded; try (rewrite ! map_length; reflexivity).
    intros t Ht_in_bounds.
    unfold vsel, csel.
    (* Show that the defaults correspond to an idealised 'out of bounds'
       outline. *)
    remember (OSkip (const 1%views (B := LSt)) (const 1) (C := C)) as oob.
    assert (is_local_outline oob) as Hoob_l by now rewrite Heqoob.
    pose (exist _ oob Hoob_l : LocalOutline C LSt) as oob_l.
    (* This lets us rewrite all of these maps in a uniform manner: the
       default case uses the same functions as the map cases. *)
    replace
      (nth t (map (outpre ∘ lo_to_o (LSt:=LSt)) os) (const 1)) with
        ((nth t (map (outpre ∘ lo_to_o (LSt := LSt)) os)) ((outpre ∘ lo_to_o (LSt := LSt)) oob_l))
        by (subst; reflexivity).
    fold (LSkip C).
    assert (LSkip C = local_outline_to_prog oob_l)
      as -> by (subst; reflexivity).
    assert (const 1 = (outpost ∘ lo_to_o (LSt := LSt)) oob_l)
      as -> by (subst; reflexivity).
    (* This lets us collapse the nth and map together... *)
    rewrite ! map_nth.
    rewrite map_length in Ht_in_bounds.
    (* Finally, show that what we have here is a proven local outline. *)
    apply local_outline_sound_safe.
    apply (proj1 (Forall_forall _ os)) with (x := (nth t os oob_l)) in Hsafe; intuition.
    apply nth_In, Ht_in_bounds.
  Qed.
End lostarling_frontend.
