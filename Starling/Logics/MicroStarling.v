(** * MicroStarling frontend and resulting logic *)

From Coq Require Import
     Classes.SetoidClass
     Lists.List
     Program.Basics
     Sets.Constructive_sets.

From Starling Require Import
     Utils.List.Facts
     Views.Classes
     Views.Frameworks.Common     
     Views.Frameworks.CVF.Core     
     ProgramProof
     ProofRules
     Backend.Classes
     Frontend.Common
     Frontend.SynDefiner.

Set Implicit Arguments.

Record MicroStarling (V Ep Er : Type) (C : Set) :=
  mk_MicroStarling
    {
      ms_svd  : SynDefiner V Ep;
      ms_avd  : C -> Er;
    }.
    
Section microstarling_outline_decompose.

  (** ** Decomposing proof outlines into backend verification conditions *)
  
  Variables
    (V Ep Er GCtx : Type)
    (C S : Set).

  Context
    {SS : Setoid                   S}
    {SV : Setoid                   V}
    {VV : ViewsSemigroup           V}
    {OV : OrderedViewsSemigroup    V}
    {DV : DecOrderedViewsSemigroup V}
    {AV : AdjView                  V}
    `{BE : Backend Ep Er GCtx unit S}.
  
  Variable
    (ms : MicroStarling V Ep Er C).

  Local Open Scope backend_scope.
  Local Open Scope views_scope.
  Local Open Scope program_scope.

  (** [ms_label_avd] defines a label using a MicroStarling definer. *)

  Definition ms_label_avd (c : LabelC C) : Er :=
    label_maybe (re_id (RelEx := bi_relex)) ms.(ms_avd) c.
  
  (** [ms_build_vc_ni] converts a wpre/goal Hoare triple to a [BVCond]. *)
  
  Definition ms_build_vc_ni (w g : V) (c : LabelC C) : BVCond Ep Er :=
    << sd_syn_reify_expr w ms.(ms_svd) >> (ms_label_avd c) << sd_syn_define_expr g ms.(ms_svd) >>.

  Definition ms_build_vc_pc (p q : V) (c : LabelC C) : BVCond Ep Er :=
    << sd_syn_reify_expr p ms.(ms_svd) >> (ms_label_avd c) << sd_syn_reify_expr q ms.(ms_svd) >>.
  
  (** [ms_decomp_ni_single] builds a non-interference [BVCond] for a
      single axiom and goal view. *)
  
  Definition ms_decomp_ni_single '(mk_ViewsAxiom p c q) (g : V) : BVCond Ep Er :=
    ms_build_vc_ni (p * (g \ q)) g c.

  (** [ms_decomp_ni] builds all of the non-interference [BVCond]s for a
      single axiom. *)
  
  Definition ms_decomp_ni (ax : ViewsAxiom V C) : list (BVCond Ep Er) :=
    List.map (ms_decomp_ni_single ax ∘ (sd_view (Ep := Ep))) ms.(ms_svd).

  (** [ms_decomp] builds all of the [BVCond]s for a single axiom, including
      the local partial correctness condition. *)
  
  Definition ms_decomp (ax : ViewsAxiom V C) : list (BVCond Ep Er) :=
    (ms_build_vc_pc ax.(pre) ax.(post) ax.(cmd))::ms_decomp_ni ax.
End microstarling_outline_decompose.

Section microstarling_views_instance.
  Variables
    (V Ep Er GCtx: Type)
    (C S: Set).

  Context
    {SV : Setoid                   V}
    {VV : ViewsSemigroup           V}
    {OV : OrderedViewsSemigroup    V}
    {DV : DecOrderedViewsSemigroup V}
    {AV : AdjView                  V}
    `{BE : Backend Ep Er GCtx unit S}.

  Variable
    (ms: MicroStarling V Ep Er C).
  
  Local Open Scope program_scope.  

  Definition ms_def (a : GCtx) : Definer V S :=
    sd_define a ms.(ms_svd).
  
  (** We can lift a [MicroStarling] to a [Signature], if we have a global
      context. *)
  
  Definition ms_sig (a : GCtx) : Signature C S :=
    {|
      sig_sem := re_interp a tt ∘ ms.(ms_avd);
      sig_reifier := dreifier _ _ (ms_def a);
    |}.

  Lemma ms_sig_uses_definer (a : GCtx) : sig_uses_definer _ _ _ (ms_sig a) (ms_def a).
  Proof.
    intro v.
    reflexivity.
  Qed.
  
  (** The μStarling non-interference/monoidal template is just the extension
      of its VC builder to a template... *)
  
  Definition ms_template_ni : GCtx -> Template V C S :=
    builder_to_template (list_to_set ∘ (ms_decomp_ni ms)) (Singleton _).

  (** ...and similarly for its instance. *)
  
  Definition ms_PreInstance_ni : GCtx -> PreInstance C S :=
    builder_to_instance (list_to_set ∘ (ms_decomp_ni ms)) (Singleton _) ms_sig.

  (** We do a similar job for the 'full' template and instance. *)
  
  Definition ms_template : GCtx -> Template V C S :=
    builder_to_template (list_to_set ∘ (ms_decomp ms)) (Singleton _).

  Definition ms_PreInstance : GCtx -> PreInstance C S :=
    builder_to_instance (list_to_set ∘ (ms_decomp ms)) (Singleton _) ms_sig.
  
  (** μStarling's non-interference template strengthens the defining
      views template. *)
  
  Lemma ms_strengthens_defining_ni (a : GCtx):
    strengthens V C _
                (ms_sig a)
                (ms_template_ni a)
                (defining_views_template_ni V C S (ms_def a)).
  Proof.
    intros ax Hin (g & Hg_df) s' (s & Hs & Htrans).
    simpl in *.
    (* First, use the fact that [g] is defined to simplify the conclusion. *)
    unfold ms_template_ni, builder_to_template in Hin.
    destruct Hin as (ax' & ->%Singleton_inv & Hin).
    specialize (Hin (ms_decomp_ni_single ms ax g)).
    apply sd_define_lift_syn_define.
    destruct ax as [p c q].
    refine (Hin _ s s' tt _ _).
    - (* We need to show that there exists some backend condition generated by
         the MicroStarling frontend that justifies this axiom against [g].
         To do so, we note that if gg is a defining view, it must also have a
         definition in the syntactic definer... *)
      pose (has_def_has_syn_def Hg_df (V := V)) as Hasd.
      apply Exists_exists in Hasd.
      destruct Hasd as (gd & Hgd_in & Hgd_matches).
      apply in_map_iff.
      exists gd.
      split.
      + unfold compose, ms_decomp_ni_single, ms_build_vc_ni.
        (* Push through the view equivalence. *)        
        f_equal.
        * apply sd_syn_reify_expr_eq_Proper, equiv_dot_right, eqv_sub_congr, Hgd_matches.
        * apply sd_syn_define_expr_eq_Proper, Hgd_matches.
      + exact Hgd_in.
    - (* Now show that the precondition is valid. *)
      apply sd_reify_syn_reify, Hs.
    - (* And now, that the transition is valid. *)
      simpl in *.
      unfold label_sem in Htrans.
      destruct c.
      + apply re_id_id.
        apply Singleton_inv in Htrans.
        now rewrite Htrans.
      + apply Htrans.
  Qed.
      
  (** It thus strengthens the free template... *)
  
  Corollary ms_strengthens_free_ni (a : GCtx): 
    strengthens V C _
                (ms_sig a)
                (ms_template_ni a)
                (free_template_ni V C S).
  Proof.
    intros x Hin.
    refine (adjoint_strengthens_free_reifier_inc_ni V C S (ms_sig a) (dreifier_inc V S _) x _).
    refine (defining_views_ni_strengthens_adjoint_ni V C S (ms_def a) _ (ms_sig_uses_definer a) x _).
    apply ms_strengthens_defining_ni, Hin.
  Qed.

  (** ...and, as a result, is axiom-sound in the presence of a views monoid. *)

  Corollary ms_axiom_sound_ni {MV : ViewsMonoid V} (a : GCtx) :
    axiom_sound (ms_PreInstance_ni a).
  Proof.
    apply instantiate_sound with (t2 := free_template V C S).
    - intros ax Hin.
      apply free_monoid_reduce.
      + exact MV.
      + apply ms_strengthens_free_ni, Hin.
    - apply free_template_axiom_sound.
  Qed.

  (** We now show a similar result for the 'full' version. *)

  Lemma ms_strengthens_defining (a : GCtx) :
    strengthens V C _
                (ms_sig a)
                (ms_template a)
                (defining_views_template V C S (ms_def a)).
  Proof.
    intros ax (ax' & ->%Singleton_inv & Hms).
    split.
    - (* Partial correctness *)
      intros s' (s & Hin_p & Htrans).
      specialize (Hms (ms_build_vc_pc ms ax.(pre) ax.(post) ax.(cmd))).
      apply sd_reify_syn_reify.
      apply sd_reify_syn_reify in Hin_p.
      refine (Hms _ s s' tt Hin_p _).
      + (* The list of VC always has PC on the left. *)
        now left.
      + destruct ax.(cmd).
        * apply re_id_id.
          apply Singleton_inv in Htrans.
          now rewrite Htrans.
        * apply Htrans.
    - (* Non-interference *)
      apply ms_strengthens_defining_ni.
      exists ax.
      firstorder.
  Qed.

  Corollary ms_strengthens_free (a : GCtx) :
    strengthens V C _
                (ms_sig a)
                (ms_template a)
                (free_template V C S).
  Proof.
    intros x Hin.
    refine (adjoint_strengthens_free_reifier_inc V C S (ms_sig a) (dreifier_inc V S _) x _).
    refine (defining_views_strengthens_adjoint V C S (ms_def a) _ (ms_sig_uses_definer a) x _).
    apply ms_strengthens_defining, Hin.
  Qed.
  
  Corollary ms_axiom_sound (a : GCtx) :
    axiom_sound (ms_PreInstance a).
  Proof.
    apply instantiate_sound with (t2 := free_template V C S).
    - intros ax Hin.
      apply ms_strengthens_free, Hin.
    - apply free_template_axiom_sound.
  Qed.
End microstarling_views_instance.

Section microstarling_frontend.
  Variables
    (V Ep Er GCtx : Type)
    (C S: Set).

  Context
    {SV : Setoid                   V}
    {VV : ViewsSemigroup           V}
    {OV : OrderedViewsSemigroup    V}
    {DV : DecOrderedViewsSemigroup V}
    {AV : AdjView                  V}
    `{BE : Backend Ep Er GCtx unit S}.

  Variable
    (ms : MicroStarling V Ep Er C).

  Local Open Scope program_scope.
  
  (** MicroStarling forms two sound list frontends: one for semigroups, one for
      monoids.  The semigroup one is the default. *)

  Global Instance MicroStarling_Frontend :
    Frontend V V Ep Er unit GCtx C C S list :=
    builder_frontend (ms_decomp ms) (Singleton _) (ms_sig ms).

  Global Instance MicroStarling_Frontend_Monoid (MV : ViewsMonoid V) :
    Frontend V V Ep Er unit GCtx C C S list :=
    builder_frontend (ms_decomp_ni ms) (Singleton _) (ms_sig ms).    

  (** [ms_outline_safe_ni] is monoid outline safety by applying the
      backend solver function to the flattened form of the
      verification conditions. *)
  
  Definition ms_outline_safe_ni : Outline V C -> Prop :=
    bi_solve_list ∘ flat_map (ms_decomp_ni ms) ∘ vcs.
  
  (** [sm_outline_safe] is outline safety by applying the backend solver
      function to the flattened form of the verification conditions. *)
  
  Definition ms_outline_safe : Outline V C -> Prop :=
    bi_solve_list ∘ flat_map (ms_decomp ms) ∘ vcs.

  Local Open Scope outline_scope.
  
  (** If we have [ms_outline_sound_ni], then the outline's axioms are all
      contained in the monoid views instance for some global context. *)

  Theorem ms_outline_safe_outline_safe_ni {VM : ViewsMonoid V} (o : Outline V C):
    ms_outline_safe_ni o ->
    exists (g : GCtx), (ms_PreInstance_ni ms g |- o).
  Proof.
    intro Hsafe.
    assert (lg_safe o (F := MicroStarling_Frontend_Monoid VM))
      as (g & Hlg)%frontend_outline_safe
        by (apply (bi_solve_same (in_flat_map_set_same _ _) Hsafe)).
    exists g.
    intros x Hin_vcs.
    apply Hlg in Hin_vcs.
    apply Hin_vcs, In_singleton.
  Qed.

  (** If we have [ms_outline_sound], then the outline's axioms are all
      contained in the semigroup views instance for some global context. *)

  Theorem ms_outline_safe_outline_safe (o : Outline V C):
    ms_outline_safe o ->
    exists (g : GCtx), (ms_PreInstance ms g |- o).
  Proof.
    intro Hsafe.
    assert (lg_safe o (F := MicroStarling_Frontend))
      as (g & Hlg)%frontend_outline_safe
        by (apply (bi_solve_same (in_flat_map_set_same _ _) Hsafe)).
    exists g.
    intros x Hin_vcs.
    apply Hlg in Hin_vcs.
    apply Hin_vcs, In_singleton.
  Qed.  
End microstarling_frontend.
