(** * GStarling frontend and resulting logic *)

From Coq Require Import
     Arith
     Classes.SetoidClass
     Classes.SetoidDec
     FunInd
     Lists.List
     Lists.SetoidList
     Lists.SetoidPermutation
     Omega
     Program.Basics
     RelationPairs
     Sets.Constructive_sets
.

From Starling Require Import
     Utils.Ensemble.Facts
     Utils.List.Facts
     Utils.List.MapPrf
     Utils.List.Setoid
     Utils.Multiset.LMultiset
     Utils.Monads
     Views.Classes
     Views.Expr
     Views.Instances.LMultiset
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core
     Views.Frameworks.LVF
     Views.Transformers.Function
     Views.Transformers.Subtype
     ProgramProof
     ProofRules
     Backend.AbstractExpr
     Backend.Alpha
     Backend.Classes
     Backend.Desymbol
     Backend.Transformers.Function
     Frontend.Common
     Frontend.APred
.

Set Implicit Arguments.
Set Universe Polymorphism.

(** [GStarling] collects all of the various parameters of the GStarling
    frontend. *)

Record GStarling
       (Ep : Type@{_} -> Type@{_})
       (Ev : Type@{_} -> Type@{_})
       (Tag : Type@{_})
       (Var : Type@{_})
       (Val : Type@{_})
       (Symb VBop EBop VMop EMop C : Set) :=
  mk_GStarling
    {
      (* Variable equality decision *)
      gs_var_dec : forall (x y : Var), {x = y} + {x <> y};

      (* Local alphabet *)
      gs_local_alpha : list Var;

      (* Shared alphabet *)
      gs_shared_alpha : list Var;

      (* The two alphabets must be disjoint *)
      gs_local_shared_disj : Disjoint.ldisj gs_var_dec gs_local_alpha gs_shared_alpha;

      (* Prototype function *)
      gs_protos : Tag -> option TagProto;

      (* Guarded syntactic definer, using single-state predicates over
         shared state only *)
      gs_definer :
        APredDefiner
          Symb VBop EBop VMop EMop
          (Ev (DefVar (AVarL gs_shared_alpha)))
          (flattened_protos Tag gs_protos);

      (* Semantics, as a function from commands to two-state predicates over
         local and shared state *)
      gs_sem :
        C -> AbstractExpr Symb VBop EBop VMop EMop
                         (Ev (AMarkedVarL (gs_local_alpha ++ gs_shared_alpha)));
    }.

Section gstarling_decomposition.
  (** ** Decomposing GStarling atoms into backend conditions *)

  Variables
    Tag : Set.

  Polymorphic Universe v.
  Variable Var : Set. (*Type@{v}. *)

  Variable Ep : Set -> Set.
  Variable Ev : Set -> Set.

  Variables
      Val Symb VBop EBop VMop EMop C
    : Set.

  Polymorphic Universe c.
  Variable GCtx : Type@{c}.

  (** [GCtx] must be decidably inhabited. *)

  Variable (gctx_witness : GCtx).

  Context
    {T_Tag  : Tagger Tag}
    {S_Val  : Setoid Val}
    {E_Val  : EqDec S_Val}
    {E_Ep   : EqPredEx GCtx unit Ep (Val := Val)}
    {V_Ev   : ValEx Ev (Val := Val)}
    {AEC    : AbstractExprContext Symb VBop EBop VMop EMop (AP_Ep := E_Ep)}.

  Variable
    gs : GStarling Ep Ev Tag Var Val Symb VBop EBop VMop EMop C.

  Definition gs_alpha : list Var :=
    gs.(gs_local_alpha) ++ gs.(gs_shared_alpha).

  (** First, we locally define some shorthands for otherwise quite large
      type names. *)

  (** [LVar] is the dependent type of local variables. *)
  Definition LVar : Set := AVarL gs.(gs_local_alpha).

  (** [SVar] is the dependent type of shared variables. *)
  Definition SVar : Set := AVarL gs.(gs_shared_alpha).

  (** [NVar] is the dependent type of variables that are either local or
      shared. *)
  Definition NVar : Set := AVarL (gs.(gs_local_alpha) ++ gs.(gs_shared_alpha)).

  (** [MLVar] is the dependent type of marked local variables. *)

  Definition MLVar : Set := AMarkedVarL gs.(gs_local_alpha).

  (** [MSVar] is the dependent type of marked shared variables. *)

  Definition MSVar : Set := AMarkedVarL gs.(gs_shared_alpha).

  Inductive LooseCondVar : Set :=
  | CVGoal (n : nat)
  | CVLocal (l : Marked Var)
  | CVShared (s : Var).

  Definition lcv_in (ls : list Var) (ss : list Var) (c : LooseCondVar) : Prop :=
    match c with
    | CVGoal n => True
    | CVLocal l => ni ls (l.(m_var))
    | CVShared s => ni ss s
    end.

  Definition CondVar : Set :=
    { x : LooseCondVar | lcv_in gs.(gs_local_alpha) gs.(gs_shared_alpha) x }.

  Definition DVar (l : list Var) :=
    { x : DefVar Var | vars_in l x }.

  Definition DMVar (l : list Var) : Set :=
    { x : DefVar (Marked Var) | mvars_in l x }.

  Definition DLVar := DVar gs.(gs_local_alpha).
  Definition DSVar := DVar gs.(gs_shared_alpha).
  Definition DNVar := DVar (gs.(gs_local_alpha) ++ gs.(gs_shared_alpha)).

  Definition DMLVar := DMVar gs.(gs_local_alpha).
  Definition DMSVar := DMVar gs.(gs_shared_alpha).
  Definition DMNVar := DMVar (gs.(gs_local_alpha) ++ gs.(gs_shared_alpha)).

  Definition GSGuardExpr V : Set :=
    AbstractExpr Datatypes.Empty_set VBop EBop VMop EMop V.

  Definition GSGAPred V : Set :=
    Guarded (GSGuardExpr (Ev V)) (APred (Ev V) gs.(gs_protos)).

  Definition GSAPredDef : Set :=
    APredDef Symb VBop EBop VMop EMop (Ev (DefVar SVar)) (flattened_protos _ gs.(gs_protos)).

  Definition GSVExpr V : Set := VExpr (GSGAPred V).

  Definition GSLNormVExpr V : Set := LNormVExpr (GSGAPred V).

  (** [GSCondPredEx] is the type of predicate expressions over local,
      shared, and goal variables. *)

  Definition GSCondPredEx : Set :=
    AbstractExpr Symb VBop EBop VMop EMop (Ev CondVar).

  (** [GSCondRelEx] is the type of relation expressions (in this case,
      two-state predicate expressions) over local and shared variables.
      (Goal variables never change, and so we don't refer to them in
      the relation expressions. *)

  Definition GSCondRelEx : Set :=
    AbstractExpr Symb VBop EBop VMop EMop (Ev (AMarkedVarL (gs.(gs_local_alpha) ++ gs.(gs_shared_alpha)))).

  Global Instance GSVarPredEx_PredEx V
    : PredEx (AbstractExpr Symb VBop EBop VMop EMop (Ev V)) GCtx unit (V -> Val)
    := PredEx_Domain_Chained V (P_Ep := PredEx_AbstractExpr (ACtx := AEC)).

  Global Instance GSVarPredEx_ImplPredEx V
    : ImplPredEx (AbstractExpr Symb VBop EBop VMop EMop (Ev V)) GCtx unit (V -> Val)
    := ImplPredEx_Domain_Chained V (I_Ep := ImplPredEx_AbstractExpr (ACtx := AEC)).

  Definition lifted_ape_eq (V : Set) (x y : V)
    : AbstractExpr Symb VBop EBop VMop EMop (Ev V) :=
    ape_eq (mreturn x : Ev V) (mreturn y : Ev V)
           (EqPredEx := EqPredEx_AbstractExpr (ACtx := AEC)).
(*
  Lemma GSVarPredEx_state_fmap
        (A B : Type) (x : GSAbstractExpr (Ev A)) (f : A -> B) (gc : GCtx) (lc : unit) (s : B -> Val) :
    In (B -> Val) (pe_interp gc lc (aemap (fmap f) x)) s <->
    In (A -> Val) (pe_interp gc lc x) (s ∘ f).
  Proof.
    split; intros Hin%ape_state_fmap; cbn in *; rewrite fmap_identity in Hin.
    - eapply pe_interp_equiv.
      {
        intro v.
        apply symmetry, ve_state_fmap.
      }
      now apply ape_state_fmap, aemap_fmap.
    - apply aemap_fmap, ape_state_fmap.
      apply pe_interp_equiv with (y := (ve_interp (s ∘ f))); intuition.
      intro v.
      apply ve_state_fmap.
  Qed.
*)
  Lemma lifted_ape_eq_val_eq
        (A : Set) (x y : A) (gc : GCtx) (lc : unit) (s : A -> Val) :
    Ensembles.In _ (pe_interp gc lc (lifted_ape_eq x y)) s <->
    s x == s y.
  Proof.
    split.
    - intros Hin%ape_eq_val_eq.
      now rewrite ! ve_interp_mreturn in Hin.
    - intro Heq.
      apply ape_eq_val_eq.
      now rewrite ! ve_interp_mreturn.
  Qed.


  Global Instance GSCondRelEx_RelEx
    : RelEx
        (AbstractExpr Symb VBop EBop VMop EMop (Ev (AMarkedVarL (gs.(gs_local_alpha) ++ gs.(gs_shared_alpha)))))
        GCtx unit (PrfIrrFun Var Val (ni (gs.(gs_local_alpha) ++ gs.(gs_shared_alpha)))) :=
    RelEx_MarkedPredEx
      (gs.(gs_local_alpha) ++ gs.(gs_shared_alpha))
      (Ev := Ev)
      (E_Ep := EqPredEx_AbstractExpr (AP_Ep := E_Ep) (ACtx := AEC)).

  (** A large amount of functions for lifting one set of variables to another
      follows. *)

  Definition var_ston : SVar -> NVar :=
    AVarL_app_r _ _.

  Definition var_lton : LVar -> NVar :=
    AVarL_app_l _ _.

  Definition argcs : list nat :=
    map (get_argc (pfun := _) ∘ apd_tag (pfun := _)) gs.(gs_definer).

  Lemma in_def_in_argcs (gdef : GSAPredDef) :
    List.In gdef                      gs.(gs_definer) ->
    List.In (get_argc (apd_tag gdef)) argcs.
  Proof.
    intro Hin.
    apply in_map_iff.
    now (exists gdef).
  Qed.

  (** [GSBVCond] is the dependent type of backend conditions. *)

  Definition GSBVCond : Set :=
    BVCond GSCondPredEx GSCondRelEx.

  (** To convert [NVar] to [DefVar NVar n], just use [SharedVar]. *)

  Definition ev_ston : Ev SVar -> Ev NVar :=
    fmap var_ston.

  Definition ev_lton : Ev LVar -> Ev NVar :=
    fmap var_lton.

  Definition var_dlton : DLVar -> DNVar :=
    vars_in_map_var (AVarL_app_l _ _).

  Definition var_dmlton : DMLVar -> DMNVar :=
    mvars_in_map_var (AMarkedVarL_app_l _ _).

  Definition var_dston : DSVar -> DNVar :=
    vars_in_map_var (AVarL_app_r _ _).

  Definition var_dmston : DMSVar -> DMNVar :=
    mvars_in_map_var (AMarkedVarL_app_r _ _).

  Definition ev_dlton : Ev DLVar -> Ev DNVar :=
    fmap var_dlton.

  Definition ev_dston : Ev DSVar -> Ev DNVar :=
    fmap var_dston.

  Definition ev_dmlton : Ev DMLVar -> Ev DMNVar :=
    fmap var_dmlton.

  Definition ev_dmston : Ev DMSVar -> Ev DMNVar :=
    fmap var_dmston.


  Import EqNotations.

 Definition apred_bind_vars {A B T : Set}
            {TF : T -> option TagProto} (f : A -> Ev B) : APred (Ev A) TF -> APred (Ev B) TF :=
   apred_map (mbind f).

 Definition apred_map_vars {A B T : Set}
            {TF : T -> option TagProto} (f : A -> B) : APred (Ev A) TF -> APred (Ev B) TF :=
   apred_map (fmap f).

 Lemma lapred_map_fun_ext {A B T}
       (f g : A -> B) (p : LooseAPred T A) :
   (forall (a : A), f a = g a) ->
   lapred_map f p = lapred_map g p.
 Proof.
   intro Hfe.
   unfold lapred_map.
   f_equal.
   induction (lap_argv p); cbn; congruence.
 Qed.

 Corollary apred_map_fun_ext {A B T} {TF : T -> option TagProto}
       (f g : A -> B) (p : APred A TF) :
   (forall (a : A), f a = g a) ->
   proj1_sig (apred_map f p) = proj1_sig (apred_map g p).
 Proof.
   apply lapred_map_fun_ext.
 Qed.

 Corollary apred_map_vars_fun_ext {A B T : Set} {TF : T -> option TagProto}
       (f g : A -> B) (p : APred (Ev A) TF) :
   (forall (a : A), f a = g a) ->
   proj1_sig (apred_map_vars f p) = proj1_sig (apred_map_vars g p).
 Proof.
   intro Hfe.
   apply apred_map_fun_ext.
   intro a.
   now apply ve_Functor_fun_ext.
 Qed.

 Instance guard_ctxless_PredEx_Val A :
   PredEx (GSGuardExpr A) unit unit (A -> Val) :=
   {|
     pe_true := pe_true (PredEx := PredEx_EVal (P_Ep := ape_PredEx _));
     pe_conj := pe_conj (PredEx := PredEx_EVal (P_Ep := ape_PredEx _));
     pe_interp _ _ x :=
       pe_interp gctx_witness tt x
                 (PredEx := ape_PredEx _ (EqPredEx := EqPredEx_AbstractExpr (ACtx := AbstractExprContext_NoSymbols (AEC := AEC))));

     pe_true_unit_inc _ _ := pe_true_unit_inc gctx_witness tt
                                              (PredEx := ape_PredEx _ (EqPredEx := EqPredEx_AbstractExpr (ACtx := AbstractExprContext_NoSymbols (AEC := AEC))));

     pe_interp_equiv  _ _ := pe_interp_equiv gctx_witness tt
                                             (PredEx := ape_PredEx _ (EqPredEx := EqPredEx_AbstractExpr (ACtx := AbstractExprContext_NoSymbols (AEC := AEC))));
     pe_conj_distr    _ _ := pe_conj_distr gctx_witness tt
                                           (PredEx := ape_PredEx _ (EqPredEx := EqPredEx_AbstractExpr (ACtx := AbstractExprContext_NoSymbols (AEC := AEC))));
   |}.

 Instance guard_ctxless_PredEx_EVal A :
   PredEx (GSGuardExpr (Ev A)) unit unit (A -> EVal Ev) :=
   PredEx_EVal
     (P_Ep :=
        PredEx_Domain_Chained _
                              (P_Ep :=
                                 guard_ctxless_PredEx_Val (Ev A))
                              (V_Ev := V_Ev)
     ).

  Instance guard_ctxless_ImplPredEx_Val A :
    ImplPredEx (GSGuardExpr A) unit unit (A -> Val) :=
    {|
      pe_false := pe_false (ImplPredEx := ape_ImplPredEx _  (EqPredEx := EqPredEx_AbstractExpr (ACtx := AbstractExprContext_NoSymbols (AEC := AEC))));
      pe_impl  := pe_impl  (ImplPredEx := ape_ImplPredEx _ (EqPredEx := EqPredEx_AbstractExpr (ACtx := AbstractExprContext_NoSymbols (AEC := AEC))));
    |}.
  Proof.
    - (* pe_false_empty *)
      intros [] [] s Hin.
      cbn in Hin.
      now apply pe_false_False in Hin.
    - (* pe_impl_modus_ponens *)
      intros [] [] ep1 ep2 s Hin.
      cbn in Hin.
      now apply pe_impl_modus_ponens in Hin.
    - (* pe_impl_cond *)
      intros [] [] ep1 ep2 s Hcp.
      cbn in Hcp.
      now apply pe_impl_cond in Hcp.
  Defined.

  Instance guard_ctxless_ImplPredEx_EVal A :
    ImplPredEx (GSGuardExpr A) unit unit (A -> EVal Ev) :=
    ImplPredEx_EVal (I_Ep := guard_ctxless_ImplPredEx_Val A).

  Instance guard_ctxless_EqPredEx_Val :
    EqPredEx unit unit GSGuardExpr (Val := Val) :=
    {|
      ape_PredEx     := guard_ctxless_PredEx_Val;
      ape_ImplPredEx := guard_ctxless_ImplPredEx_Val;
      ape_Functor_Ep := AbstractExpr_Functor _ _ _ _ _;
      ape_eq         := ape_eq (EqPredEx := EqPredEx_AbstractExpr (ACtx := AbstractExprContext_NoSymbols (AEC := AEC)));
    |}.
  Proof.
    all: try easy.
    - (* ape_eq_val_eq *)
      intros A x y [] [] s.
      now apply ape_eq_val_eq.
    - (* ape_state_fmap *)
      intros A B x f [] [] s.
      split; intro Hin.
      + now apply ape_state_fmap.
      + now apply ape_state_fmap in Hin.
  Defined.

  (** For some reason, Coq's typeclass inference struggles to pick this up by
      itself, so we restate it. *)

  Instance EVal_Setoid : Setoid (EVal Ev) :=
    ValEx_Setoid (Val := Val) (V_Ev := V_Ev).

  Instance guard_ctxless_EqPredEx_EVal :
    EqPredEx unit unit GSGuardExpr (Val := EVal Ev) :=
    EqPredEx_EVal (E_Ep := guard_ctxless_EqPredEx_Val).

  Definition gapred_map_vars {A B : Set} (f : A -> B) : GSGAPred A ->
    GSGAPred B :=
    map_gapred Tag _ A B Ev Ev
               GSGuardExpr
               _
               (gs.(gs_protos))
               (fmap f (Functor := ve_Functor))
               id.

  Lemma gapred_map_vars_fun_ext {A B : Set}
            (f g : A -> B) (p : GSGAPred A) :
    (forall (a : A), f a = g a) ->
    gapred_mostly_equal _ _
                        (gapred_map_vars f p)
                        (gapred_map_vars g p).
  Proof.
    intro Hfe.
    destruct p as (pg & pi).
    split.
    - unfold g_guard, gapred_map_vars.
      apply map_ext.
      intro a.
      apply aemap_ext.
      intro b.
      now apply ve_Functor_fun_ext.
    - apply apred_map_fun_ext.
      intro a'.
      now apply ve_Functor_fun_ext.
  Qed.

  Definition AVarL_to_d (l : list Var) :
    AVarL l -> DVar l :=
    vars_in_lift (l := _).

  Definition AVarL_to_dm (m : Mark) (l : list Var) :
    AVarL l -> DMVar l :=
    mvars_in_lift (l := _) ∘ AVarL_to_AMarkedVarL m.

  Definition var_stoc (s : SVar) : CondVar :=
    exist _ (CVShared (proj1_sig s)) (proj2_sig s).

  Program Definition defvar_stoc (s : DefVar SVar) : CondVar :=
    destruct_DefVar (CVShared ∘ @proj1_sig _ _) CVGoal s.
  Next Obligation.
    now destruct s as [(s & Hs)|r].
  Defined.

  (* Not to be confused with defvar_stoc! *)
  Program Definition var_dstoc (s : DSVar) : CondVar :=
    destruct_DefVar CVShared CVGoal s.
  Next Obligation.
    now destruct s as ([s|r] & Hs).
  Defined.

  Definition ev_dstoc : Ev DSVar -> Ev CondVar :=
    fmap var_dstoc.

  Definition defpred_lift_vars :
    AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar SVar)) ->
    AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar CondVar)) :=
    aemap (fmap (defvar_map_var var_stoc)).

  (** [def_lift_vars] converts [SVar]s to [DNVar]s in a definition. *)

  Definition def_lift_vars (m : Mark) (d : GSAPredDef) :
    APredDef Symb VBop EBop VMop EMop (Ev (DefVar CondVar))
             (flattened_protos _ gs.(gs_protos))
    :=
    {|
      apd_tag := d.(apd_tag);
      apd_def := defpred_lift_vars d.(apd_def);
    |}.

  (** [definer_lift_vars] converts [SVar]s to [DefVar NVar]s in a definer. *)

  Definition definer_lift_vars (m : Mark) :
    APredDefiner Symb VBop EBop VMop EMop (Ev (DefVar SVar))
                 (flattened_protos _ gs.(gs_protos)) ->
    APredDefiner Symb VBop EBop VMop EMop (Ev (DefVar CondVar))
                 (flattened_protos _ gs.(gs_protos)) :=
    map (def_lift_vars m).

  Program Definition var_dmltoc (v : DMLVar) : CondVar :=
    match proj1_sig v with
    | ParamRef p => CVGoal p
    | SharedVar v => CVLocal v
    end.
  Next Obligation.
    destruct v0 as (u & Hu).
    cbn in *.
    now subst.
  Defined.

  (** Now we can start defining the moving parts of the frontend. *)

  (** [preprocess_apred] lifts an abstract predicate with no symbols and
      [DMLVar] variables to one with symbols and [CondVar] variables. *)

  Definition preprocess_apred :
    GAPred GSGuardExpr (Ev DMLVar) gs.(gs_protos) ->
    GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev CondVar) gs.(gs_protos) :=
    map_gapred Tag Val DMLVar CondVar Ev Ev
               _ _
               gs.(gs_protos) (fmap var_dmltoc)
                              (symbolise Symb (A := Ev CondVar))
                              (P_EEp1 := guard_ctxless_EqPredEx_Val).

  Global Instance preprocess_apred_Proper :
    Proper ((gapred_mostly_equal DMLVar Ev (protos := gs.(gs_protos)))
              ==> (gapred_mostly_equal CondVar Ev (protos := gs.(gs_protos))))
           preprocess_apred.
  Proof.
    intros (xg & xi) (yg & yi) (Hg & Hi).
    cbn in *.
    split; cbn.
    - now subst.
    - now rewrite Hi.
  Defined.

  (** [preprocess_wpre] lifts a weakest precondition with no symbols and
      [DMLVar] variables to one with symbols and [CondVar] variables. *)
  Definition preprocess_wpre :
    LNormVExpr (GAPred GSGuardExpr (Ev DMLVar) gs.(gs_protos)) ->
    LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop)
                       (Ev CondVar) gs.(gs_protos)) :=
    lvmap preprocess_apred.

  Global Instance preprocess_wpre_Proper :
    Proper ((lnorm_mostly_equal _ Ev (protos := gs.(gs_protos)))
              ==> (lnorm_mostly_equal _ Ev (protos := gs.(gs_protos))))
           preprocess_wpre.
  Proof.
    intros (x & Hx) (y & Hy).
    unfold lnorm_mostly_equal, "@@", forget_lnorm, proj1_sig in *.
    remember (gapred_mostly_equal DMLVar Ev (protos:=gs_protos gs)) as R.
    induction 1; cbn; try easy.
    subst.
    destruct x1, x2; try easy.
    constructor; cbn.
    - constructor.
      inversion H; subst.
      now apply preprocess_apred_Proper.
    - inversion Hx; subst.
      inversion Hy; subst.
      now apply IHForallAtoms2_2 with (Hx := H2) (Hy := H3).
  Defined.

  (** [gs_reify_wpre] reifies a weakest precondition into a predicate
      expression over shared and local state.  The weakest precondition
      can contain goal variables, so the result is a [GSCondPredEx]. *)
  Definition gs_reify_wpre
             (wp : LNormVExpr (GAPred GSGuardExpr (Ev DMLVar) gs.(gs_protos))) :
    GSCondPredEx :=
    gd_reify _ Ep Ev Val _ _ _ _ _ _ _ _
             gs.(gs_protos)
             (preprocess_wpre wp)
             (map (def_lift_vars Pre) gs.(gs_definer))
             (AEC := AEC).

  (** [gs_define_goal] converts a goal, in the form of a view definition,
      into a predicate expression over shared and goal state. *)

  Definition gs_define_goal
             (gdef : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    GSCondPredEx :=
    aemap (fmap defvar_stoc) (gdef.(proj1_sig).(apd_def)).


  Lemma gs_define_goal_prfirr
        (gdef gdef' : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    proj1_sig gdef = proj1_sig gdef' ->
    gs_define_goal gdef = gs_define_goal gdef'.
  Proof.
    destruct gdef as (gdef & Hgdef), gdef' as (gdef' & Hgdef').
    cbn.
    now intros <-.
  Qed.

  (** [gs_label_sem] gives the semantics for a label as a two-state predicate
      expression over local and shared state. *)

  Definition gs_label_sem : LabelC C -> GSCondRelEx :=
    label_maybe
      (re_id (RelEx := GSCondRelEx_RelEx))
      gs.(gs_sem).

  Local Open Scope backend_scope.

  Definition gs_build_vc
             (wp   : GSLNormVExpr DMLVar)
             (gdef : {x : GSAPredDef | List.In x gs.(gs_definer)})
             (c    : LabelC C) :
    GSBVCond :=
    << gs_reify_wpre wp >> (gs_label_sem c) << gs_define_goal gdef >>.

  Lemma gs_build_vc_gdef_prfirr
        (wp         : GSLNormVExpr DMLVar)
        (gdef gdef' : {x : GSAPredDef | List.In x gs.(gs_definer)})
        (c          : LabelC C) :
    proj1_sig gdef = proj1_sig gdef' ->
    gs_build_vc wp gdef c = gs_build_vc wp gdef' c.
  Proof.
    intro Hgdef.
    unfold gs_build_vc.
    f_equal.
    now apply gs_define_goal_prfirr.
  Qed.

  Local Open Scope signature_scope.

  Global Instance gs_reify_wpre_mostly_equal_Proper :
    Proper
      ((ForallAtoms2 (gapred_mostly_equal _ _ (protos := gs.(gs_protos))) @@ forget_lnorm (A := _))
        ==> eq)
      gs_reify_wpre.
  Proof.
    intros wp1 wp2 Heqwp.
    apply
         (gd_reify_Proper
            _ Ep Ev Val GCtx unit Symb VBop EBop VMop EMop Tag (gs.(gs_protos))
            (definer_lift_vars Pre (gs_definer gs))), preprocess_wpre_Proper,
    Heqwp.
  Defined.

  Global Instance gs_build_vc_wp_mostly_equal_Proper
         (gdef    : {x : GSAPredDef | List.In x gs.(gs_definer)})
         (c       : LabelC C) :
    Proper
      ((ForallAtoms2 (gapred_mostly_equal _ _ (protos := gs.(gs_protos))) @@ forget_lnorm (A := _))
        ==> eq)
      (fun wp => gs_build_vc wp gdef c).
  Proof.
    intros wp1 wp2 Heqwp.
    unfold gs_build_vc.
    f_equal.
    now apply gs_reify_wpre_mostly_equal_Proper.
  Defined.

  Local Open Scope views_scope.

  (** [lift_cond] lifts every local variable in a pre/postcondition
      view expression so that it has the right type for combining with a
      goal expression. *)

  Definition lift_cond (m : Mark) :
    GSVExpr LVar -> GSVExpr DMLVar :=
    vmap (gapred_map_vars (AVarL_to_dm m (l := _))).

  Import ListNotations.

  (** Lifts a goal variable reference to an argument. *)

  Definition inst_arg (k : nat) :
    Ev DMLVar :=
      mreturn (mvars_in_lift_ref _ k).

  Lemma inst_arg_fun_base (k : nat) (f : nat -> nat) :
    inst_arg (f k) = fmap (mvars_in_map_ref _ f) (inst_arg k).
  Proof.
    unfold inst_arg.
    now rewrite monad_fmap, monad_identity_left.
  Qed.

  (** Instantiates the argument vector of a goal atom. *)

  Fixpoint inst_argv (current rest : nat) :
           list (Ev DMLVar) :=
    match rest with
    | 0 => []
    | S rest' =>
        inst_arg current :: (inst_argv (S current) rest')
    end.

  Lemma inst_argv_length (base count : nat) :
    length (inst_argv base count) = count.
  Proof.
    unfold inst_argv.
    revert base.
    induction count; intuition.
    specialize (IHcount (S base)).
    cbn.
    congruence.
  Qed.

  Lemma inst_argv_succ (base count : nat) :
    inst_argv base (S count) = inst_arg base :: (inst_argv (S base) count).
  Proof.
    trivial.
  Qed.

  Lemma inst_argv_fun_base (k count : nat) (f : nat -> nat) :
    (forall n, f (S n) = S (f n)) ->
    inst_argv (f k) count = map (fmap (mvars_in_map_ref _ f)) (inst_argv k count).
  Proof.
    intro Hf.
    revert k.
    induction count; eauto.
    intros k.
    rewrite ! inst_argv_succ.
    cbn.
    now rewrite inst_arg_fun_base, <- Hf, IHcount.
  Qed.

  Corollary inst_argv_succ_base (k count : nat) :
    inst_argv (S k) count = map (fmap (mvars_in_map_ref _ S)) (inst_argv k count).
  Proof.
    now apply inst_argv_fun_base.
  Qed.

  Lemma inst_argv_add_base (k n count : nat) :
    inst_argv (n + k) count = map (fmap (mvars_in_map_ref _ (plus n))) (inst_argv k count).
  Proof.
    apply inst_argv_fun_base.
    intro z.
    omega.
  Qed.

  (** Every valid [nth] item of an [inst_argv] from [k] is the [n+k]th argument. *)
  Lemma inst_argv_nth (k count n : nat) (default : Ev DMLVar) :
    n < count -> nth n (inst_argv k count) default = inst_arg (k + n).
  Proof.
    revert count.
    induction n; intros count Hcount.
    - rewrite Nat.add_0_r.
      now destruct count.
    - destruct count; intuition.
      apply Nat.succ_lt_mono in Hcount.
      cbn.
      rewrite inst_argv_succ_base, Nat.add_succ_r, (inst_arg_fun_base (k + n)).
      erewrite (nth_indep _ default).
      + rewrite map_nth.
        f_equal.
        apply IHn, Hcount.
      + now rewrite map_length, inst_argv_length.
  Qed.

  (** Instantiates a goal atom with the given tag. *)

  Definition inst_atom
             (base : nat)
             (tag  : ATag gs.(gs_protos)) :
    APred (Ev DMLVar) gs.(gs_protos) :=
    mk_APred _ _ tag
             (exist _ (inst_argv base (get_argc tag)) (inst_argv_length _ _)).

  Lemma inst_atom_fun_base (k : nat) (f : nat -> nat) (t : ATag gs.(gs_protos)) :
    (forall n, f (S n) = S (f n)) ->
    proj1_sig (inst_atom (f k) t) =
    proj1_sig (apred_map_vars (mvars_in_map_ref _ f) (inst_atom k t)).
  Proof.
    intro Hf.
    unfold inst_atom, apred_map_vars, apred_map, lapred_map.
    cbn.
    f_equal.
    now apply inst_argv_fun_base.
  Qed.

  (** Instantiates a guarded goal atom with the given tag. *)

  Definition inst_gatom
             (base : nat)
             (tag  : ATag gs.(gs_protos)) :
    GSGAPred DMLVar :=
    mk_Guarded [pe_true (PredEx := guard_ctxless_PredEx_EVal _)]
               (inst_atom base tag).

  Definition map_refs_in_gapred
             {A : Set}
             (l : list A)
             (f : nat -> nat) :
    GSGAPred {x : DefVar (Marked A) | mvars_in l x} -> GSGAPred {x : DefVar (Marked A) | mvars_in l x} :=
    gapred_map_vars (mvars_in_map_ref l f).

  Lemma inst_gatom_fun_base (k : nat) (f : nat -> nat) (t : ATag gs.(gs_protos)) :
    (forall n, f (S n) = S (f n)) ->
    gapred_mostly_equal _ _
                        (inst_gatom (f k) t)
                        (map_refs_in_gapred _ f (inst_gatom k t)).
  Proof.
    intro Hf.
    split; try easy.
    now apply inst_atom_fun_base.
  Qed.

  Lemma inst_gatom_add_base (n k : nat) (t : ATag gs.(gs_protos)) :
    gapred_mostly_equal _ _
                        (inst_gatom (n + k) t)
                        (map_refs_in_gapred _ (plus n) (inst_gatom k t)).
  Proof.
    apply inst_gatom_fun_base.
    intro x.
    omega.
  Qed.

  (** Iterative core for instantiating a list of guarded goal atoms. *)

  Fixpoint inst_gatoms_iter
           (base : nat)
           (tags : list (ATag gs.(gs_protos)))
    : list (GSGAPred DMLVar) :=
    match tags with
    | [] => []
    | t::ts =>
      (inst_gatom base t)
        :: (inst_gatoms_iter (base + get_argc t) ts)
    end.

  (** The length of the generated list is equal to the length of the
      tag list. *)

  Lemma inst_gatoms_iter_length
           (tags : list (ATag gs.(gs_protos)))
           (base : nat) :
    length (inst_gatoms_iter base tags) = length tags.
  Proof.
    revert base.
    induction tags; intuition.
    cbn.
    now rewrite IHtags.
  Qed.

  Lemma inst_gatoms_iter_add_base
        (base disp : nat)
        (tags : list (ATag gs.(gs_protos))) :
    lists_mostly_equal _ _ _ _ _
      (inst_gatoms_iter (disp + base) tags)
      (map (map_refs_in_gapred _ (plus disp)) (inst_gatoms_iter base tags)).
  Proof.
    revert base.
    induction tags; cbn; constructor.
    - apply inst_gatom_add_base.
    - rewrite <- Nat.add_assoc.
      apply IHtags.
  Qed.

  Lemma map_refs_in_gapred_fun_ext
        (l : list Var)
        (f g : nat -> nat)
        (p : GSGAPred (DMVar l)) :
    (forall a : nat, f a = g a) ->
    gapred_mostly_equal _ _
                        (map_refs_in_gapred l f p)
                        (map_refs_in_gapred l g p).
  Proof.
    intro Hfex.
    apply gapred_map_vars_fun_ext.
    intros ([a|a] & Ha); cbn; try easy.
    now rewrite Hfex.
  Qed.

  (** Instantiates a list of tags into a list of guarded goal atoms. *)

  Definition inst_gatoms
             (tags : list (ATag gs.(gs_protos))) :
    list (GSGAPred DMLVar) :=
    inst_gatoms_iter 0 tags.

  Section inst_gdef.
    Variable
      gdef : {x : GSAPredDef | List.In x gs.(gs_definer)}.

    (** Instantiates a definition into a list of guarded goal atoms. *)

    Definition inst_gdef_list : list (GSGAPred DMLVar) :=
      (inst_gatoms (unflatten_tags _ _ gdef.(proj1_sig).(apd_tag))).

    Lemma inst_gdef_list_length :
      length inst_gdef_list = length (proj1_sig (apd_tag (proj1_sig gdef))).
    Proof.
      unfold inst_gdef_list, inst_gatoms, unflatten_tags.
      now rewrite inst_gatoms_iter_length, <- unflatten_tags_inner_length.
    Qed.

    (** Instantiates a definition into a list-normalised goal view. *)

    Definition inst_gdef : GSLNormVExpr DMLVar :=
      unlistify_lnorm inst_gdef_list.

    (** If we're listifying an [inst_gdef], that's the same as
  [inst_gdef_list]. *)
    Lemma inst_gdef_listify :
      listify_lnorm inst_gdef = inst_gdef_list.
    Proof.
      apply unlistify_listify_lnorm.
    Qed.

    (** If we're just throwing away the list-normalised goal, then
      we can simplify away some of the unlistification cruft. *)
    Lemma inst_gdef_forget :
      forget_lnorm inst_gdef = unlistify_inner inst_gdef_list.
    Proof.
      apply unlistify_inner_unlistify_lnorm.
    Qed.
  End inst_gdef.

  Lemma inst_gdef_list_prfirr
        (gdef gdef' : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    proj1_sig gdef = proj1_sig gdef' ->
    Forall2
      (gapred_mostly_equal _ _ (protos := gs.(gs_protos)))
      (inst_gdef_list gdef)
      (inst_gdef_list gdef').
  Proof.
    intro Hgdef_eq.
    destruct gdef as (gdef & Hgdef), gdef' as (gdef' & Hgdef').
    unfold proj1_sig in Hgdef_eq.
    subst.
    unfold inst_gdef_list.
    cbn.
    induction (inst_gatoms (unflatten_tags Tag (gs_protos gs) (apd_tag gdef')));
      constructor;
      auto.
    reflexivity.
  Qed.

  Corollary inst_gdef_prfirr
        (gdef gdef' : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    proj1_sig gdef = proj1_sig gdef' ->
    Forall2
      (gapred_mostly_equal _ _ (protos := gs.(gs_protos)))
      (listify_lnorm (inst_gdef gdef))
      (listify_lnorm (inst_gdef gdef')).
  Proof.
    unfold inst_gdef.
    rewrite ! unlistify_listify_lnorm.
    apply inst_gdef_list_prfirr.
  Qed.

  Definition build_wp_not_normalised
             (p q  : GSVExpr LVar)
             (gdef : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    GSVExpr DMLVar :=
      (Join (lift_cond Pre p)
            (Part (forget_lnorm (inst_gdef gdef)) (lift_cond Post q))).

  Lemma build_wp_not_normalised_gdef_prfirr
        (p q : GSVExpr LVar)
        (gdef gdef' : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    proj1_sig gdef = proj1_sig gdef' ->
    ForallAtoms2
      (gapred_mostly_equal _ _ (protos := gs.(gs_protos)))
      (build_wp_not_normalised p q gdef)
      (build_wp_not_normalised p q gdef').
  Proof.
    intros Hgdef_eq.
    constructor.
    - apply ForallAtoms2_Reflexive, gapred_mostly_equal_Reflexive.
    - constructor.
      + rewrite <- listify_unlistify_lnorm.
        replace (forget_lnorm (inst_gdef gdef')) with
            (forget_lnorm (unlistify_lnorm (listify_lnorm (inst_gdef gdef')))) by
            apply listify_unlistify_lnorm.
        apply forall2_lnorm_lists, inst_gdef_prfirr, Hgdef_eq.
      + apply ForallAtoms2_Reflexive, gapred_mostly_equal_Reflexive.
  Qed.

  Definition build_wp
             (p q  : GSVExpr LVar)
             (gdef : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    GSLNormVExpr DMLVar :=
    rmpart_guard_APred DMLVar tag_dec (build_wp_not_normalised p q gdef).

  Lemma build_wp_gdef_prfirr
        (p q : GSVExpr LVar)
        (gdef gdef' : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    proj1_sig gdef = proj1_sig gdef' ->
    ForallAtoms2
      (gapred_mostly_equal _ _ (protos := gs.(gs_protos)))
      (forget_lnorm (build_wp p q gdef))
      (forget_lnorm (build_wp p q gdef')).
  Proof.
    intro Hgdef_eq.
    unfold build_wp.
    now apply mostly_equal_rmpart_Proper, build_wp_not_normalised_gdef_prfirr.
  Qed.

  Import EqNotations.

  (** We can decompose an axiom against a single definition [gdef] by
      building the weakest precondition, then supplying it to
      [gs_build_vc]. *)

  Definition gs_build_vcs_single
             '(<| p |> c <| q |>)
             (gdef : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    GSBVCond :=
    gs_build_vc (build_wp p q gdef) gdef c.

  Lemma gs_build_vcs_single_prfirr
        (ax : ViewsAxiom (GSVExpr LVar) C)
        (gdef1 gdef2 : {x : GSAPredDef | List.In x gs.(gs_definer)}) :
    proj1_sig gdef1 = proj1_sig gdef2 ->
    gs_build_vcs_single ax gdef1 = gs_build_vcs_single ax gdef2.
  Proof.
    intro Hgdef.
    destruct ax as [p c q].
    simpl.
    rewrite gs_build_vc_gdef_prfirr with (gdef' := gdef2); try assumption.
    apply gs_build_vc_wp_mostly_equal_Proper.
    now apply build_wp_gdef_prfirr.
  Qed.

  (** The full decomposition of an axiom is the combination of each
      single-definition decomposition in the definer. *)

  Definition gs_build_vcs :
    ViewsAxiom (GSVExpr LVar) C -> list GSBVCond :=
    map_prf gs.(gs_definer) ∘ gs_build_vcs_single.

End gstarling_decomposition.

