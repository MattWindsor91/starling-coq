(** * 'Lowered' GStarling *)

(** ** The [GStarling] views instance *)

From Coq Require Import
     Arith
     Classes.DecidableClass
     Classes.SetoidClass
     Classes.SetoidDec
     Lists.List
     Lists.SetoidList
     Lists.SetoidPermutation
     Omega
     Program.Basics
     Program.Utils
     RelationPairs
     Sets.Constructive_sets
     Sorting.Permutation
.

From Starling Require Import
     Logics.GStarling.Core
     Logics.LoStarling
     Utils.Ensemble.Facts
     Utils.List.Facts
     Utils.List.MapPrf
     Utils.List.Setoid
     Utils.Multiset.LMultiset
     Utils.Monads
     Views.Classes
     Views.Expr
     Views.Instances.LMultiset
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core
     Views.Frameworks.LVF
     Views.Transformers.Function
     Views.Transformers.Subtype
     ProgramProof
     ProofRules
     Backend.AbstractExpr
     Backend.Alpha
     Backend.Classes
     Backend.Transformers.Function
     Frontend.Common
     Frontend.APred
.

Set Implicit Arguments.

Section gstarling_lowered.
  Variables
    Ep Ev
  : Set -> Set.

  Variables
    Var Val Symb VBop EBop VMop EMop C Tag
    : Set.

  Variables
    GCtx
    LCtx
    : Type.

  Context
    {T_Tag : Tagger Tag}
    {S_Val : Setoid Val}
    {D_Val : EqDec S_Val}
    {E_Ep  : EqPredEx GCtx unit Ep (Val := Val)}
    {V_Ev  : ValEx Ev (Val := Val)}
    {AEC   : AbstractExprContext Symb VBop EBop VMop EMop (AP_Ep := E_Ep)}.

  Existing Instance EVal_Setoid.

  Variable
    gs : GStarling Ep Ev Tag Var Val Symb VBop EBop VMop EMop C.

  Variables
    (gctx_witness : GCtx).

  Section goal_states.

    (** ** Goal states

        A goal state, in the lowered form of GStarling, is a piece of local
        backend context that represents the assignments that the solver has
        made to the parameters of a goal view.  In the meta-theory, it takes
        the form of a function from the naturals (parameter references) to
        expressible values. *)

    (** The variable alphabet for all of the various variables we
        manipulate in this section. *)
    Variable ls : list Var.


    (** Goal states map goal parameter references to expressible values. *)
    Definition GoalState : Set :=
      nat -> EVal Ev.

    (** [erase_goals g v] strips parameter references out of variable [v] by
      interpreting them as goal references using [g]. *)
    Program Definition erase_goals
            (g : GoalState)
            (v : { x : DefVar (Marked Var) | mvars_in ls x}) :
      Ev (AMarkedVarL ls) :=
      match proj1_sig v with
      | ParamRef r => eval_to_valex (AMarkedVarL ls) (g r)
      | SharedVar x => mreturn (exist _ x _) (Monad := ve_Monad (ValEx := V_Ev))
      end.

    (** If two goal states are equivalent, then they produce equivalent
        erasures over a variable. *)
    Global Instance erase_goals_Proper :
      Proper (SetoidClass.equiv ==> SetoidClass.equiv) erase_goals.
    Proof.
      intros g1 g2 Hg ([v|v] & Hv).
      - reflexivity.
      - intros x.
        cbn.
        unfold eval_to_valex.
        rewrite ! ve_state_fmap.
        setoid_rewrite elim_empty_ve_interp_equiv.
        apply Hg.
    Defined.

    (** [gs_add_goals] adds goals to a state function [s]. *)
    Definition gs_add_goals
               (s : AMarkedVarL ls -> Val)
               (g : GoalState)
               (v : { x : DefVar (Marked Var) | mvars_in ls x }) :
      Val :=
      ve_interp s (erase_goals g v).

    (** Two uses of [gs_add_goals] over equivalent state functions and goal
        states produce equivalent modifications of the same variable. *)
    Global Instance gs_add_goals_Proper :
      Proper (SetoidClass.equiv ==> SetoidClass.equiv ==> SetoidClass.equiv)
             gs_add_goals.
    Proof.
      intros s1 s2 Hs g1 g2 Hg v.
      unfold gs_add_goals.
      rewrite erase_goals_Proper.
      - now apply ve_interp_equiv.
      - assumption.
    Defined.

    (** [gs_add_goals_separate s s'] is a modified version of [gs_add_goals] that
        takes a separate pre-state [s] and post-state [s']. *)
    Definition gs_add_goals_separate (s s' : AVarL ls -> Val) :
      GoalState ->
      { x : DefVar (Marked Var) | mvars_in ls x } ->
      Val :=
      gs_add_goals (mark_states _ s s').

    (** It has a similar properness property to [gs_add_goals]. *)
    Global Instance gs_add_goals_separate_Proper :
      Proper (SetoidClass.equiv
                ==> SetoidClass.equiv
                ==> SetoidClass.equiv
                ==> SetoidClass.equiv)
             gs_add_goals_separate.
    Proof.
      intros s1 s2 Hs s'1 s'2 Hs' g1 g2 Hg v.
      apply gs_add_goals_Proper; intuition.
      now apply mark_states_Proper.
    Defined.
  End goal_states.

  Section lowered_atoms.

    Definition LowAtom := APred (EVal Ev) gs.(gs_protos).

    (** [LowAtom_EqDec] is the equality decidability proof for [LowAtom]'s underlying setoid. *)

    Global Instance LowAtom_EqDec : EqDec
                            (@subtype_Setoid (LooseAPred Tag (Ev Datatypes.Empty_set))
                (fun x : LooseAPred Tag (Ev Datatypes.Empty_set) =>
                 @valid Tag (Ev Datatypes.Empty_set) x
                   (gs.(gs_protos)))
                (@APred_Setoid Tag (Ev Datatypes.Empty_set)
                               (@ValEx_Setoid Val Ev S_Val V_Ev Datatypes.Empty_set))) :=
 (@subtype_EqDec (LooseAPred Tag (Ev Datatypes.Empty_set))
       _
       _
       (@LooseAPred_EqDec Tag (Ev Datatypes.Empty_set)
          (@ValEx_Setoid Val Ev S_Val V_Ev Datatypes.Empty_set)
          (EVal_EqDec (S_Val := S_Val))
          tag_dec)).

    Lemma atom_equiv_same_tag (a1 a2: LowAtom) :
      a1 == a2 -> tag_of a1 = tag_of a2.
    Proof.
      cbn.
      destruct a1 as ((t1 & a1) & Ha1), a2 as ((t2 & a2) & Ha2); cbn.
      now unfold APred_equiv.
    Qed.

    Lemma atom_permut_cons
          (x  : LowAtom)
          (xs : list LowAtom)
          (ys : list LowAtom) :
      PermutationA SetoidClass.equiv (x :: xs) (x :: ys) ->
      PermutationA SetoidClass.equiv xs ys.
    Proof.
      intro Hperm.
      apply (PermutationA_lmeq (DA := LowAtom_EqDec)) in Hperm.
      change xs with (unbag (LBag xs)).
      change ys with (unbag (LBag ys)).
      apply (lmeq_PermutationA (DA := LowAtom_EqDec)).
      intro a.
      specialize (Hperm a).
      cbn in *.
      unfold count_occ_equiv, compose.
      omega.
    Qed.
  End lowered_atoms.

  Section lowered_views.

    (** ** Lowered views

        Unlike regular GStarling views, lowered GStarling views are
        multisets of unguarded abstract predicates whose parameters
        are all expressible values.  *)

    Definition LowView := LMultiset LowAtom.

    Global Instance LowView_Setoid : Setoid LowView :=
      lms_Setoid gs.(gs_protos) tag_dec.

  End lowered_views.
End gstarling_lowered.