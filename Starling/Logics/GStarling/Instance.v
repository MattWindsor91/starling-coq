(** ** The [GStarling] views instance *)

From Coq Require Import
     Arith
     Classes.DecidableClass
     Classes.SetoidClass
     Classes.SetoidDec
     Lists.List
     Lists.SetoidList
     Lists.SetoidPermutation
     Omega
     Program.Basics
     Program.Utils
     RelationPairs
     Sets.Constructive_sets
     Sorting.Permutation
.

From Starling Require Import
     Logics.GStarling.Core
     Logics.GStarling.Lowered
     Logics.LoStarling
     Utils.Ensemble.Facts
     Utils.List.Facts
     Utils.List.MapPrf
     Utils.List.Setoid
     Utils.Multiset.LMultiset
     Utils.Monads
     Views.Classes
     Views.Expr
     Views.Instances.LMultiset
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core
     Views.Frameworks.LVF
     Views.Transformers.Function
     Views.Transformers.Subtype
     ProgramProof
     ProofRules
     Backend.AbstractExpr
     Backend.Alpha
     Backend.Classes
     Backend.Transformers.Function
     Frontend.Common
     Frontend.APred
.

Set Implicit Arguments.

Section gstarling_views_instance.
  Variables
    Ep Ev
  : Set -> Set.

  Variables
    Var Val Symb VBop EBop VMop EMop C Tag
    : Set.

  Variables
    GCtx
    LCtx
    : Type.

  Context
    {T_Tag : Tagger Tag}
    {S_Val : Setoid Val}
    {D_Val : EqDec S_Val}
    {E_Ep  : EqPredEx GCtx unit Ep (Val := Val)}
    {V_Ev  : ValEx Ev (Val := Val)}
    {AEC   : AbstractExprContext Symb VBop EBop VMop EMop (AP_Ep := E_Ep)}.

  Existing Instance EVal_Setoid.

  Variable
    gs : GStarling Ep Ev Tag Var Val Symb VBop EBop VMop EMop C.

  Variables
    (gctx_witness : GCtx).

  Global Instance PredEx_Guard V :
    PredEx (GSGuardExpr VBop EBop VMop EMop (Ev V)) unit unit (V -> EVal Ev) :=
    guard_ctxless_PredEx_EVal gctx_witness V (V_Ev := V_Ev)
  .

  (* Predicate interpretation decidability (TODO: weaken this? *)

   Hypothesis
    Hgs_pred_dec :
    forall V (e : GSGuardExpr VBop EBop VMop EMop (Ev V)) (s : V -> EVal Ev),
      { pe_interp tt tt e s (PredEx := PredEx_Guard V)
      } + {
        ~ pe_interp tt tt e s (PredEx := PredEx_Guard V)
      }
  .

  (** In the Views world, we model the local and shared state as separate,
      proof-irrelevant functions. *)

  (** Shared states map variables in [gs_shared_alpha] to values. *)

  Definition GSSSt : Set :=
    SVar gs -> Val.

  (** Proof-irrelevant version of [GSLSt]. *)
  Definition GSSStP : Set :=
    PrfIrrFun Var Val (ni (gs.(gs_shared_alpha))).

  (** Local states map variables in [gs_shared_alpha] to expressible values. *)
  Definition GSLSt : Set :=
    LVar gs -> EVal Ev.

  (** Proof-irrelevant version of [GSLSt]. *)
  Definition GSLStP : Set :=
    PrfIrrFun Var (EVal Ev) (ni (gs.(gs_local_alpha))) (S_R := EVal_Setoid (V_Ev := V_Ev)).


  (** We can use [GSLSt]s as 'normal' states, returning [Val],
      by interpreting the returned expressible values. *)

  Global Instance interp_empty_Proper :
    Proper (SetoidClass.equiv ==> SetoidClass.equiv) (ve_interp (elim_empty Val) (ValEx := V_Ev)).
  Proof.
    intros x y Hequiv.
    apply Hequiv.
  Defined.

  Definition gs_combined_state (l : GSLSt) (s : GSSSt) :
    NVar gs -> Val :=
    sjoin gs.(gs_var_dec) gs.(gs_local_shared_disj) (eval_lift l) s.

  Instance gs_combined_state_Proper :
    Proper (SetoidClass.equiv ==> SetoidClass.equiv ==> SetoidClass.equiv)
           gs_combined_state.
  Proof.
    intros l1 l2 Hl s1 s2 Hs.
    apply sjoin_equiv_Proper; intuition.
    intro v.
    now apply eval_equiv_req.
  Defined.

  Lemma gs_combined_state_equiv_reduce (l l' : GSLStP) (s s' : GSSStP) :
    gs_combined_state (extract_pfun l) (extract_pfun s)
    == gs_combined_state (extract_pfun l') (extract_pfun s') ->
    l == l' /\ s == s'.
  Proof.
    destruct l as ((l & Hl)), l' as ((l' & Hl')), s as ((s & Hs)), s' as ((s' & Hs')).
    intros (Heql%eval_lift_equiv_inj & Heqs)%sjoin_equiv_reduce; cbn in *;
      intuition.
    - apply gs.(gs_local_shared_disj).
    - apply Hl, H0.
    - apply Hl', H0.
  Qed.

    Definition gs_state_with_goals (s s' : GSSStP) :
      GoalState Ev ->
      DMSVar gs ->
      Val :=
      gs_add_goals_separate (extract_pfun s) (extract_pfun s').

    Global Instance gs_state_with_goals_Proper :
      Proper (SetoidClass.equiv
                ==> SetoidClass.equiv
                ==> SetoidClass.equiv
                ==> SetoidClass.equiv)
             gs_state_with_goals.
    Proof.
      intros s1 s2 Hs s'1 s'2 Hs' g1 g2 Hg v.
      apply gs_add_goals_separate_Proper; intuition; now apply PrfIrrFun_extract_Proper.
    Defined.

  (** As above, but also adding local pre- and post-state. *)

  Program Definition make_condvar_state_npi
             (l : MLVar gs -> Val)
             (s : SVar gs -> Val)
             (g : nat    -> Val)
             (v : CondVar gs) : Val :=
    match `v with
    | CVLocal    v' => l v'
    | CVShared   v' => s v'
    | CVGoal   _ v' => g v'
    end.
  Next Obligation.
    destruct v as (v & Hv).
    cbn in *.
    now subst.
  Qed.
  Next Obligation.
    destruct v as (v & Hv).
    cbn in *.
    now subst.
  Defined.

  Global Instance make_condvar_state_npi_Proper :
    Proper (SetoidClass.equiv
              ==> SetoidClass.equiv
              ==> SetoidClass.equiv
              ==> SetoidClass.equiv)
           make_condvar_state_npi.
  Proof.
    intros l1 l2 Hl s1 s2 Hs g1 g2 Hg ([v|v|v] & Hv); cbn; easy.
  Defined.

  Definition make_condvar_state (l l' : GSLStP) (s : GSSStP) (g : GoalState Ev)
    : CondVar gs -> Val :=
    make_condvar_state_npi
      (eval_lift (mark_states _ (extract_pfun l) (extract_pfun l')))
      (extract_pfun s)
      (eval_lift g).

  Global Instance make_condvar_state_Proper :
    Proper (SetoidClass.equiv
              ==> SetoidClass.equiv
              ==> SetoidClass.equiv
              ==> SetoidClass.equiv
              ==> SetoidClass.equiv)
           make_condvar_state.
  Proof.
    intros (l1) (l2) Hl (l'1) (l'2) Hl' (s1) (s2) Hs g1 g2 Hg.
    apply make_condvar_state_npi_Proper; cbn; intuition.
    - now apply eval_lift_Proper, mark_states_Proper.
    - now apply eval_lift_Proper.
  Defined.


  Program Definition gs_combined_state_p (l : GSLStP) (s : GSSStP) :
    PrfIrrFun Var Val (ni gs.(gs_alpha)) :=
    Pfun _ _ _ (gs_combined_state (extract_pfun l) (extract_pfun s)).
  Next Obligation.
    destruct l as ((l & Hl)), s as ((s & Hs)).
    cbn.
    unfold gs_combined_state.
    apply sjoin_prf_irr_dist; intuition.
    apply eval_equiv_req, Hl, H.
  Defined.

  (** We can model [gs_sem] as a LVF semantic function. *)

  Definition gs_semantics (gc : GCtx) : Semantics C (GSLStP * GSSStP) :=
    fun c '(lst, sst) '(lst', sst') =>
      re_interp gc tt (gs_sem gs c)
                (gs_combined_state_p lst sst)
                (gs_combined_state_p lst' sst')
              (RelEx := GSCondRelEx_RelEx gs)
.

  Local Definition GSAtom :=
    Guarded (AbstractExpr Datatypes.Empty_set VBop EBop VMop EMop (Ev (LVar gs)))
            (APred (Ev (LVar gs)) gs.(gs_protos)).

  Local Definition GSView := VExpr GSAtom.


  Instance GSAtomLanguage
    : AtomLanguage (APred (Ev (LVar gs)) gs.(gs_protos)) ((LVar gs -> EVal Ev) -> LowView gs) :=
    AtomLanguage_APred_LMultiset_Lowered (LVar gs) gs.(gs_protos).

  (** Make sure that Coq can pick this up. *)
  Instance LowView_Setoid : Setoid (LowView gs) := LowView_Setoid gs.

  Instance GSGAtomLanguage
    : AtomLanguage GSAtom
        ((LVar gs -> EVal Ev) -> LowView gs) :=
    guard_AtomLanguage (Hgs_pred_dec (V := LVar gs))
                       (P_Ep := PredEx_Guard (LVar gs))
                       (AL := GSAtomLanguage).

  Instance GSView_Setoid : Setoid GSView :=
    VExpr_Setoid (AL := GSGAtomLanguage).

  (** [inflate_valex] lifts an abstract predicate over expressible
      values to one over value expressions on shared variables. *)

  Definition inflate_valex :
    APred (EVal Ev) (flattened_protos _ gs.(gs_protos)) -> APred (Ev (SVar gs)) (flattened_protos _ gs.(gs_protos)) :=
    apred_map (eval_to_valex (SVar gs)).

  Global Instance inflate_valex_Proper:
    Proper (SetoidClass.equiv ==> SetoidClass.equiv) inflate_valex.
  Proof.
    intros ((xt & xv) & Hx) ((yt & yv) & Hy) (Hxyt & Hxyv).
    cbn in *.
    unfold lapred_map.
    split; cbn; intuition.
    clear Hx Hy.
    induction Hxyv; intuition.
    constructor; intuition.
    intro z.
    unfold eval_to_valex.
    now rewrite ! ve_state_fmap.
  Defined.

  Definition flatten_lowered_view_eval
             '(LBag v : LowView gs) :
    APred (EVal Ev) (flattened_protos _ gs.(gs_protos)) :=
    flatten_APred _ _ gs.(gs_protos) v.

  Definition flatten_lowered_view
    : LowView gs -> APred (Ev (SVar gs)) (flattened_protos _ gs.(gs_protos)) :=
    inflate_valex ∘ flatten_lowered_view_eval.

  Global Instance flatten_lowered_view_Proper:
    Proper (eqlistA SetoidClass.equiv @@ (@unbag _) ==> SetoidClass.equiv) flatten_lowered_view.
  Proof.
    intros (x) (y) Hxy%flatten_APred_Proper.
    apply inflate_valex_Proper, Hxy.
  Defined.

  Definition pattern_matches_lowered
             (ts : list Tag)
             '(LBag v : LowView gs) :
    list (LowView gs) :=
    map (@LBag _) (pattern_matches Tag ts v).

  Definition pattern_matches_exact_lowered
             (ts : list Tag)
             '(LBag v : LowView gs) :
    list (LowView gs) :=
    map (@LBag _) (pattern_matches_exact Tag ts v).

  Lemma pattern_matches_exact_lowered_incl
        (ts : list Tag)
        (v  : LowView gs) :
    incl (pattern_matches_exact_lowered ts v) (pattern_matches_lowered ts v).
  Proof.
    destruct v as (v).
    unfold pattern_matches_exact_lowered, pattern_matches_lowered.
    intros (x) (x' & <- & Hin%pattern_matches_exact_incl)%in_map_iff.
    now apply in_map.
  Qed.

  Import EqNotations.
  Local Open Scope signature_scope.
  Local Open Scope views_scope.

  Definition is_match (x v : LowView gs) (t : ATag (flattened_protos _ gs.(gs_protos))) : Prop :=
    InA eqbagA x (dedupeA eqbagbA (pattern_matches_lowered (proj1_sig t) v)).

  Definition is_exact_match (x v : LowView gs) (t : ATag (flattened_protos _ gs.(gs_protos))) : Prop :=
    InA eqbagA x (dedupeA eqbagbA (pattern_matches_exact_lowered (proj1_sig t) v)).

  Lemma is_exact_match_eqbag (x y z : LowView gs) (t : ATag (flattened_protos _ gs.(gs_protos))) :
    is_exact_match x z t ->
    eqbagA x y ->
    is_exact_match y z t.
  Proof.
    intros (m & Heq_xm & Hin)%InA_alt Heqb_xy.
    apply InA_alt.
    exists m.
    split; intuition.
    now transitivity x.
  Qed.

  Lemma submatch_same_argc
        (v : LowView gs)
        (t : ATag (flattened_protos _ gs.(gs_protos)))
        (lmatch : { x : LowView gs | is_match x v t }) :
    get_argc t = get_argc (ap_tag (flatten_lowered_view (proj1_sig lmatch))).
  Proof.
    apply same_tag_same_argc.
    destruct v as (v), lmatch as (lmatch & Hlmatch).
    cbn.
    apply InA_alt in Hlmatch.
    destruct Hlmatch as (y & Hy & Hlmatch).
    apply dedupeA_incl_general in Hlmatch.
    unfold pattern_matches_lowered in Hlmatch.
    apply in_map_iff in Hlmatch.
    destruct Hlmatch as (vm & <- & Hin).
    pose proof (Forall_forall_in _ _ _
                                 (pattern_matches_tags Tag _ _) Hin)
      as Htags.
    pose proof (Forall_forall_in _ _ _
                                 (pattern_matches_merged_length Tag _ _) Hin)
      as Hlen.
    destruct t as (ts & Hts).
    unfold compose in Hy.
    cbn in *.
    assert (flatten_lowered_view lmatch == flatten_lowered_view (LBag vm)) as Hflat.
    {
      now apply flatten_lowered_view_Proper.
    }
    cbn in Hflat.
    destruct Hflat as (Hft & _).
    cbn in Hft.
    rewrite Hft.
    (* Set up a pairwise induction. *)
    generalize dependent Htags.
    generalize dependent Hts.
    apply list_pairwise_ind with (xs := vm) (ys := ts); try easy.
    (* Only the inductive case is particularly hairy. *)
    intros x xs y ys Hind Hhas_proto Hfa.
    inversion Hfa; subst.
    cbn.
    f_equal.
    apply Hind; try easy.
    eapply flatten_cons_legal.
    now apply Hhas_proto.
  Qed.

  Definition promote_exact_match
             (d : GSAPredDef gs)
             (v : LowView gs)
             (lmatch : { x : LowView gs | is_exact_match x v d.(apd_tag) }) :
    { x : LowView gs | is_match x v d.(apd_tag) }.
  Proof.
    refine (exist _ (proj1_sig lmatch) _).
    unfold is_match.
    destruct lmatch as (x & Hx).
    unfold proj1_sig, is_exact_match in *.
    apply dedupeA_equivlistA_general; intuition.
    { apply eqbagbA_eqbagA_reflect. }
    apply dedupeA_equivlistA_general in Hx; intuition.
    2: { apply eqbagbA_eqbagA_reflect. }
    apply InA_alt in Hx.
    destruct Hx as (y & Heqy & Hiny%pattern_matches_exact_lowered_incl).
    apply InA_alt; intuition.
    now exists y.
  Defined.

  Definition inst_lowered_match
             (d : GSAPredDef gs)
             (lmatch : LowView gs) :
    AbstractExpr Symb VBop EBop VMop EMop (Ev (SVar gs)) :=
    inst_APred _ _ _ _ _ _ _ _ _ (flattened_protos _ gs.(gs_protos))
               (flatten_lowered_view lmatch) d.(apd_def).

  Local Open Scope signature_scope.

  Global Instance inst_lowered_match_inc_Proper (d : GSAPredDef gs) :
    Proper (eqbagA  ==> pe_inc (P_Ep := PredEx_Domain_Chained _ (P_Ep := PredEx_AbstractExpr (ACtx := AEC))))
           (inst_lowered_match d).
  Proof.
    intros m1 m2 Heqv gc lc s Hin.
    unfold inst_lowered_match, inst_APred in *.
    cbn in *.
    unfold compose in *.
    apply -> (ape_state_fmap (EqPredEx := EqPredEx_AbstractExpr (ACtx := AEC))) in Hin.
    apply <- (ape_state_fmap (EqPredEx := EqPredEx_AbstractExpr (ACtx := AEC))).
    refine (proj1 (pe_interp_equiv gc lc _ _ _ _) Hin).
    intro x.
    unfold compose in *.
    rewrite ! ve_state_mbind.
    apply ve_interp_equiv.
    intro y.
    unfold compose, inst_DefVar.
    destruct y; cbn; intuition.
    apply flatten_lowered_view_Proper in Heqv.
    cbn in Heqv.
    replace (ve_bot (SVar gs)) with (eval_to_valex (SVar gs) (ve_bot (Datatypes.Empty_set))).
    - destruct Heqv as (Heqvt & Heqvv).
      pose proof
           (eqlistA_nth r (eval_to_valex (SVar gs) (ve_bot Datatypes.Empty_set (ValEx := V_Ev)))
                        Heqvv
           ) as Hnth.
      cbn in Hnth.
      symmetry.
      apply Hnth.
    - unfold eval_to_valex.
      rewrite monad_fmap.
      apply ve_bot_mbind.
  Defined.

  Global Instance inst_lowered_match_Proper (d : GSAPredDef gs) :
    Proper (eqbagA ==>
                   SetoidClass.equiv (Setoid := pe_Setoid (P_Ep := PredEx_Domain_Chained _ (P_Ep := PredEx_AbstractExpr (ACtx := AEC)))))
           (inst_lowered_match d).
  Proof.
    intros m1 m2 Heqv gc lc.
    split; now apply inst_lowered_match_inc_Proper.
  Defined.

  Local Open Scope views_scope.

  Lemma v_equiv_length (v1 v2 : LowView gs) :
    v1 == v2 ->
    length (unbag v1) = length (unbag v2).
  Proof.
    destruct v1 as (l1), v2 as (l2).
    cbn.
    unfold lmeq.
    cbn.
    revert l2.
    induction l1; intros l2 Heqv.
    - cbn in Heqv.
      destruct l2; try easy.
      specialize (Heqv l).
      cbn in Heqv.
      now rewrite equiv_nat_refl in Heqv.
    - destruct l2.
      + specialize (Heqv a).
        cbn in Heqv.
        now rewrite equiv_nat_refl in Heqv.
      + pose proof (Heqv a) as Heqa.
        pose proof (Heqv l) as Heqg.
        rewrite ! count_occ_equiv_cons in Heqa, Heqg.
        rewrite equiv_nat_refl in Heqa, Heqg.
        rewrite equiv_nat_comm in Heqa.
        unfold equiv_nat in *.
        destruct
          (@SetoidDec.equiv_dec (APred (EVal Ev) gs.(gs_protos)) _ _ a l).
        * cbn.
          f_equal.
          apply IHl1.
          intros b.
          specialize (Heqv b).
          rewrite !count_occ_equiv_cons in Heqv.
          unfold equiv_nat in Heqv.
          destruct
            (@SetoidDec.equiv_dec (APred (EVal Ev) gs.(gs_protos)) _ _ a b),
            (@SetoidDec.equiv_dec (APred (EVal Ev) gs.(gs_protos)) _ _ l b);
            intuition.
         -- rewrite e0 in e.
            rewrite e in c.
            exfalso.
            now apply c.
         -- rewrite <- e in e0.
            rewrite e0 in c.
            exfalso.
            now apply c.
        * rewrite Nat.add_0_l in *.
          symmetry in Heqa.
          destruct (count_occ_equiv_punchout _ _ _ Heqa) as
              (a' & l2a & l2b & -> & Hequiv & Hcount_occ).
          cbn.
          f_equal.
          rewrite app_length.
          cbn.
          rewrite Nat.add_succ_r.
          rewrite <- app_length.
          change (S (length (l2a ++ l2b))) with (length (l::(l2a ++ l2b))).
          apply IHl1.
          intros z.
          specialize (Heqv z).
          rewrite ! count_occ_equiv_cons, count_occ_equiv_app,
          count_occ_equiv_cons in Heqv.
          rewrite ! count_occ_equiv_cons, count_occ_equiv_app.
          rewrite (equiv_nat_Proper z z (reflexivity _) a a' Hequiv) in Heqv.
          omega.
  Qed.

  Lemma pattern_eqv (v1 v2 : LowView gs) (ts : list Tag) :
    v1 == v2 ->
    Forall (fun m1 => Exists (eqbagA m1) (pattern_matches_lowered ts v2))
           (pattern_matches_lowered ts v1).
  Proof.
    intros Hveq%lmeq_PermutationA.
    apply Forall_forall.
    destruct v1 as (l1), v2 as (l2).
    unfold pattern_matches_lowered, pattern_matches.
    intros m1 (lm1 & <- & ((m & v) & <- & Hf)%in_map_iff)%in_map_iff.
    cbn.
    pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_match_permut _ tag_dec _ (atom_equiv_same_tag (gs := gs)) (atom_permut_cons (gs := gs)) ts _ _ Hveq) Hf) as ((m' & z') & Hin_m & Hperm)%Exists_exists.
    apply Exists_exists.
    exists (LBag m'); intuition.
    apply in_map.
    change m' with (fst (m', z')).
    now apply in_map.
  Qed.

  (** The pattern match lists of two equivalent views are equivalent modulo
      ordering and duplication. *)

  Global Instance pattern_eqv_inclA (ts : list Tag) :
    Proper (SetoidClass.equiv ==> inclA eqbagA)
           (pattern_matches_lowered ts).
  Proof.
    intros v1 v2 Heqv%(pattern_eqv ts) m (y & Hmy & Hin_y)%InA_alt.
    apply InA_altdef.
    apply (Forall_forall_in _ y _) in Heqv; intuition.
    apply Exists_equiv_trans with (x := y); intuition.
  Defined.

  Global Instance pattern_eqv_equivlistA (ts : list Tag) :
    Proper (SetoidClass.equiv ==> equivlistA eqbagA)
           (pattern_matches_lowered ts).
  Proof.
    intros v1 v2 Heqv m.
    split; now apply pattern_eqv_inclA.
  Defined.

  (** This means that they are setoid permutations after de-duplication... *)

  Global Instance pattern_eqv_permut (ts : list Tag) :
    Proper (SetoidClass.equiv ==> PermutationA eqbagA)
           (dedupeA eqbagbA (A := _) ∘ pattern_matches_lowered ts).
  Proof.
    intros v1 v2 Heqv.
    apply pattern_eqv_equivlistA with (ts := ts) in Heqv.
    apply NoDupA_equivlistA_PermutationA; intuition.
    - apply dedupeA_NoDupA_general, eqbagbA_eqbagA_reflect.
    - apply dedupeA_NoDupA_general, eqbagbA_eqbagA_reflect.
    - transitivity (pattern_matches_lowered ts v1).
      {
        apply symmetry, dedupeA_equivlistA_general; intuition.
        apply eqbagbA_eqbagA_reflect.
      }
      transitivity (pattern_matches_lowered ts v2).
      {
        apply Heqv.
      }
      apply dedupeA_equivlistA_general; intuition.
      apply eqbagbA_eqbagA_reflect.
  Qed.


  Corollary pattern_inc (v1 v2 : LowView gs) (ts : list Tag) :
    v1 <<= v2 ->
    Forall (fun m1 => Exists (eqbagA m1) (pattern_matches_lowered ts v2))
           (pattern_matches_lowered ts v1).
  Proof.
    intros Hinc%(inc_join _ _ (MV := _)).
    rewrite dot_comm in Hinc.
    apply Forall_forall.
    destruct v1 as (l1), v2 as (l2).
    unfold pattern_matches_lowered, pattern_matches.
    intros m1 (lm1 & <- & ((m & v) & <- & Hin_vx)%in_map_iff)%in_map_iff.
    cbn.
    pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_match_app _ _ (lminus l2 l1) _) Hin_vx)
      as ((vy & zy) & Hin_vy & <-)%Exists_exists.
    symmetry in Hinc.
    apply (Forall_forall_in _ _ _ (pattern_eqv _ Hinc)).
    apply in_map.
    (* in_map doesn't work here, because [zx <> zy]. *)
    apply in_map_iff.
    now exists (m, zy).  Qed.

  Global Instance pattern_inc_inclA (ts : list Tag) :
    Proper (inc ==> inclA eqbagA)
           (pattern_matches_lowered ts).
  Proof.
    intros v1 v2 Hinc%(pattern_inc ts) m (y & Hmy & Hin_y)%InA_alt.
    apply InA_altdef.
    apply (Forall_forall_in _ y _) in Hinc; intuition.
    apply Exists_equiv_trans with (x := y); intuition.
  Defined.

  (* TODO: To rename. *)
  Instance PredEx_Regular :
    PredEx (AbstractExpr Symb VBop EBop VMop EMop (Ev (SVar gs))) GCtx unit (SVar gs -> Val) :=
    (PredEx_Domain_Chained _ (P_Ep := PredEx_AbstractExpr (ACtx := AEC))).

  Instance PredEx_Cond :
    PredEx (GSCondPredEx gs) GCtx unit (CondVar gs -> Val) :=
    (PredEx_Domain_Chained _ (P_Ep := PredEx_AbstractExpr (ACtx := AEC))).

  Definition gs_reify_lowered_single
             (v : LowView gs)
             (d : APredDef Symb VBop EBop VMop EMop (Ev (DefVar (SVar gs))) (flattened_protos _ gs.(gs_protos))) :
    list (
        AbstractExpr Symb VBop EBop VMop EMop (Ev (SVar gs))) :=
    map (inst_lowered_match d)
        (dedupeA eqbagbA (pattern_matches_lowered (` (d.(apd_tag))) v)).

  Global Instance gs_reify_lowered_single_Proper (d : GSAPredDef gs) :
    Proper (SetoidClass.equiv ==> PermutationA SetoidClass.equiv)
           (flip gs_reify_lowered_single d).
  Proof.
    intros (m1) (m2) Heqv.
    apply (map_PermutationA_Proper (RA := eqbagA (T := LowAtom gs))).
    - now apply inst_lowered_match_Proper.
    - now apply pattern_eqv_permut.
  Defined.

  Definition gs_reify_lowered_to_expr (v : LowView gs) :
    AbstractExpr Symb VBop EBop VMop EMop (Ev (SVar gs)) :=
    gd_conj_over_definer
      (SVar gs)
      Ev Val GCtx unit Symb VBop EBop VMop EMop Tag
      (gs.(gs_protos))
      (gs_reify_lowered_single v)
      (gs.(gs_definer)).

  Definition gs_define_lowered_single
             (v : LowView gs)
             (d : APredDef Symb VBop EBop VMop EMop (Ev (DefVar (SVar gs))) (flattened_protos _ gs.(gs_protos))) :
    list (AbstractExpr Symb VBop EBop VMop EMop (Ev (SVar gs))) :=
    (map (inst_lowered_match d)
         (dedupeA eqbagbA (pattern_matches_exact_lowered (` (d.(apd_tag))) v))).

  Global Instance gs_reify_lowered_to_expr_Proper :
    Proper (SetoidClass.equiv ==> SetoidClass.equiv)
           gs_reify_lowered_to_expr.
  Proof.
    intros (m1) (m2) Heqv.
    apply iconj_permut_equiv.
    induction (gs_definer gs).
    - constructor.
    - rewrite ! flat_map_cons.
      apply PermutationA_app; intuition.
      now apply gs_reify_lowered_single_Proper.
  Defined.

  Instance PredEx_Regular_PrfIrr :
    PredEx (AbstractExpr Symb VBop EBop VMop EMop (Ev (SVar gs))) GCtx unit GSSStP :=
    PredEx_PrfIrr _ Ev
                  (ni gs.(gs_shared_alpha))
                  (P_Ep := PredEx_Regular).

  Definition interp_def (gc : GCtx) :
    AbstractExpr Symb VBop EBop VMop EMop (Ev (SVar gs)) ->
    Ensemble GSSStP :=
        pe_interp gc tt (PredEx := PredEx_Regular_PrfIrr).

  Definition gs_v_reify (gc : GCtx) : LowView gs -> Ensemble GSSStP :=
    interp_def gc ∘ gs_reify_lowered_to_expr.

  (** ...and thus their single reifications are equivalent. *)

  Global Instance gs_v_reify_equiv_Proper (gc : GCtx) :
    Proper (SetoidClass.equiv ==> Same_set GSSStP) (gs_v_reify gc).
  Proof.
    intros v1 v2 Heqv.
    split; intros ((s & Hs)) Hreify_v1; cbn in *; unfold compose in *.
    - apply gs_reify_lowered_to_expr_Proper with (y := v1); intuition.
    - apply gs_reify_lowered_to_expr_Proper with (y := v2); intuition.
  Defined.

  Definition gs_v_reifier (gc : GCtx) : Reifier GSSStP (V := LowView gs) :=
    exist _ (gs_v_reify gc) (gs_v_reify_equiv_Proper gc).

  Instance PredEx_Cond_PrfIrr :
    PredEx (GSCondPredEx gs) GCtx (GSLStP * GSLStP * GoalState Ev) GSSStP :=
    {
      pe_true := pe_true (PredEx := PredEx_Cond);
      pe_conj := pe_conj (PredEx := PredEx_Cond);

      pe_interp (gc : GCtx) '(l, l', g) e s :=
        pe_interp gc tt e (make_condvar_state l l' s g) (PredEx := PredEx_Cond);
    }.
  Proof.
    - (* pe_true_unit *)
      intros gc ((l & l') & g) s _.
      now apply pe_true_unit.
    - (* pe_interp_equiv *)
      intros gc ((l & l') & g) e s1 s2 Hsequiv.
      now apply pe_interp_equiv, make_condvar_state_Proper.
    - (* pe_conj_distr *)
      intros gc ((l & l') & g) ep1 ep2.
      split; intros s Hin.
      + now apply pe_conj_distr, Intersection_inv in Hin.
      + apply Intersection_inv in Hin.
        now apply pe_conj_distr.
  Defined.

  Definition interp_cond (gc : GCtx) (l l' : GSLStP) (g : GoalState Ev) :
    GSCondPredEx gs ->
    Ensemble GSSStP :=
    pe_interp gc (l, l', g) (PredEx := PredEx_Cond_PrfIrr).

  Definition id_on_shared_only : GSCondRelEx gs :=
    gen_frame_r gs.(gs_local_alpha)
                gs.(gs_shared_alpha)
                (E_Ep := EqPredEx_AbstractExpr (ACtx := AEC))
                (V_Ev := V_Ev).

  (* TODO: combine this with [re_id_id_MarkedPredEx] somehow. *)

  Lemma id_on_shared_only_id (gc : GCtx) (l1 l2 : GSLStP) (s1 s2 : GSSStP) :
    re_interp gc tt id_on_shared_only (gs_combined_state_p l1 s1) (gs_combined_state_p l2 s2)
              (RelEx := GSCondRelEx_RelEx gs)
    <->
    s1 == s2.
  Proof.
    split.
    - intro Hrid.
      apply (gen_frame_r_equiv gs.(gs_var_dec) _ _ gs.(gs_local_shared_disj)) in Hrid.
      unfold gs_combined_state_p, gs_combined_state in *.
      destruct l1 as ((l1 & Hl1)), l2 as ((l2 & Hl2)),
               s1 as ((s1 & Hs1)), s2 as ((s2 & Hs2)).
      cbn in *.
      now rewrite <-! sjoin_r in Hrid.
    - intro Heqv.
      apply (gen_frame_r_equiv gs.(gs_var_dec) _ _ gs.(gs_local_shared_disj)).
      unfold gs_combined_state_p, gs_combined_state in *.
      destruct l1 as ((l1 & Hl1)), l2 as ((l2 & Hl2)),
                                         s1 as ((s1 & Hs1)), s2 as ((s2 & Hs2)).
      cbn in *.
      now rewrite <-! sjoin_r.
  Qed.

  Instance RelEx_Cond_PrfIrr :
    RelEx (GSCondRelEx gs) GCtx (GSLStP * GSLStP * GoalState Ev) GSSStP :=
    {
      re_id := id_on_shared_only;
      re_empty := re_empty (RelEx := GSCondRelEx_RelEx gs);
      re_interp gc '(l, l', _) er s s' :=
        re_interp gc tt er
                  (gs_combined_state_p l s) (gs_combined_state_p l' s')
                  (RelEx := GSCondRelEx_RelEx gs);
    }.
  Proof.
    - (* re_id_id *)
      intros gc ((l & l') & _) s s'.
      apply id_on_shared_only_id.
    - (* re_empty_empty *)
      now intros gc ((l & l') & g) s s' Hemp%re_empty_empty.
  Defined.

  Definition gs_lsig (gc : GCtx) : LocalSignature C GSLStP GSSStP (V := LowView gs) :=
    {|
      lsig_reifier := gs_v_reifier gc;
      lsig_lsem := gs_semantics gc;
    |}.

  Definition gs_sig : GCtx -> Signature (LabelC C * GSLStP * GSLStP) GSSStP (V := LowView gs) :=
    lsig_erase (SSt := GSSStP) ∘ gs_lsig.

  (** [view_interp] interprets a local-variables view into a lowered
      view. *)

  Definition view_interp
             (v : GSVExpr gs (LVar gs)) (l : GSLStP) : LowView gs :=
    interpret_sm
      (al_inject (GSGAPred gs (LVar gs)) ((LVar gs -> EVal Ev) -> LowView gs) (AtomLanguage := GSGAtomLanguage))
      v
      (extract_pfun l).

  Definition gs_views_decomp_upper
             '(<| p |> c <| q |>) : ViewsAxiom (GSLStP -> LowView gs) C :=
    <| view_interp p |> c <| view_interp q |>.

  Definition gs_views_decomp_lower
    : ViewsAxiom (GSLStP -> LowView gs) C ->
      Ensemble (ViewsAxiom (LowView gs) (LabelC C * GSLStP * GSLStP)) :=
    axioms_lvf_to_cvf (LSt := GSLStP) ∘ Singleton _.

  Definition gs_views_decomp
    : ViewsAxiom GSView C -> Ensemble (ViewsAxiom (LowView gs) (LabelC C * GSLStP * GSLStP)) :=
    gs_views_decomp_lower ∘ gs_views_decomp_upper.

  (** [erase_locals] erases local dependencies in [v], given a local
      state [lst]. *)

  Program Definition erase_locals
          (rest : list Var) (lst : GSLSt) (v : AVarL (gs.(gs_local_alpha) ++ rest)) :
    Ev (AVarL rest) :=
    if inp gs.(gs_var_dec) (proj1_sig v) rest
    then mreturn (exist _ (proj1_sig v) _)
    else eval_to_valex _ (lst (exist _ (proj1_sig v) _)).
  Next Obligation.
    now apply Bool.Is_true_eq_left, inb_In in H.
  Defined.
  Next Obligation.
    unfold ni, proj1_sig in *.
    apply List.in_app_or in H0.
    destruct H0 as [Hv|Hv]; intuition.
    apply Bool.not_true_iff_false in H.
    contradict H.
    now apply Bool.Is_true_eq_true, inb_In.
  Defined.

  Lemma mvars_in_SharedVar
        (V : Type) (l : list V)
        (m : DefVar (Marked V))
        (s : Marked V) :
    m = SharedVar s ->
    mvars_in l m ->
    List.In (s.(m_var)) l.
  Proof.
    intros ->.
    intuition.
  Qed.

  (** [erase_mlocals] erases all local dependencies in a marked
      variable [m], given a local state pair [lst] and [lst']. *)
  Program Definition erase_mlocals
          (rest : list Var) (lst lst' : GSLSt)
          (v : AMarkedVarL (gs.(gs_local_alpha) ++ rest)) :
    Ev (AMarkedVarL rest) :=
    fmap (AVarL_to_AMarkedVarL v.(proj1_sig).(m_mark) (Var := Var))
         (erase_locals
            rest
            (choose_state lst lst' v.(proj1_sig).(m_mark))
            (v.(proj1_sig).(m_var)))
         (V := Ev).

  Definition erase_mlocals_ev
             (rest : list Var)
             (lst lst' : GSLSt) :
    Ev (AMarkedVarL (gs.(gs_local_alpha) ++ rest)) -> Ev (AMarkedVarL rest) :=
    mbind (erase_mlocals rest lst lst') (V := Ev).

  Lemma marked_empty_list_false (v : AMarkedVarL (nil : list Var)) : False.
  Proof.
    destruct v as (v & Hv).
    intuition.
  Qed.

  Definition remove_empty_varlist
             (v : Ev (AMarkedVarL (nil : list Var))) :
    EVal Ev :=
    fmap (fun var => False_rec _ (marked_empty_list_false var)) v.

  Program Definition erase_locals_d (rest : list Var) (lst lst' : GSLSt)
          (v : { x : DefVar (Marked Var) | mvars_in (gs.(gs_local_alpha) ++ rest) x }) :
    Ev { x : DefVar (Marked Var) | mvars_in rest x } :=
    match proj1_sig v with
    | ParamRef p =>
      mreturn (exist _ (ParamRef (Var := Marked Var) p) _)
    | SharedVar v =>
      fmap (mvars_in_lift (l := rest)) (erase_mlocals rest lst lst' v)
    end.

  Local Open Scope monad_scope.

  (** [erase_locals_and_goal] erases local-pre, local-post, and goal variables, taking
      some arbitrary list of shared variables to preserve. *)
  Definition erase_locals_and_goal (rest : list Var)
             (lst lst' : GSLSt) (gst : GoalState Ev)
             (v : { x : DefVar (Marked Var) | mvars_in (gs.(gs_local_alpha) ++ rest) x }) :
    Ev (AMarkedVarL rest) :=
    (erase_locals_d rest lst lst' v) >>= (erase_goals _ gst).

  (** [erase_DNVar_goal] erases a [DNVar] into an [MNVar], using
      a goal state [gst] to substitute for [ParamRef]s. *)
   Definition erase_DNVar_goal
             (lst lst' : GSLSt)
             (gst : GoalState Ev)
             (v : DMNVar gs) : Ev (MSVar gs) :=
     erase_locals_and_goal _ lst lst' gst v.

   Program Definition lift_dlvar (v : DMLVar gs) :
     { x : DefVar (Marked Var) | mvars_in (gs.(gs_local_alpha) ++ nil) x } :=
     v.
   Next Obligation.
     rewrite app_nil_r.
     apply (proj2_sig v).
   Defined.

  (** [erase_DNVar_goal] erases a [DNVar] into an [SVar], using
      a goal state [gst] to substitute for [ParamRef]s. *)
  Definition erase_DLVar_goal
             (lst lst' : GSLSt)
             (gst : GoalState Ev)
             (v : DMLVar gs) : EVal Ev :=
    remove_empty_varlist (erase_locals_and_goal nil lst lst' gst (lift_dlvar v)).

  Definition erase_DNVar_ev (lst lst' : GSLSt) :
    Ev (DMNVar gs) ->
    Ev (DMSVar gs) :=
    mbind (erase_locals_d _ lst lst') (V := Ev).

  Definition erase_def_goals (g : GoalState Ev): DefVar (SVar gs) -> Ev (SVar gs) :=
    destruct_DefVar mreturn (eval_to_valex (SVar gs) ∘ g).

  Definition erase_DLVar_goal_gapred (gc : GCtx) (lst lst' : GSLSt) (gst : GoalState Ev) :
    GSGAPred gs (DMLVar gs) ->
    GSGAPred gs (EVal Ev) :=
    gapred_map_vars gc (erase_DLVar_goal lst lst' gst).

  (** To lower a goal instantiation into a Views-level atom, we throw away the
      (always-true-by-construction) guard, then use local and goal states to
      substitute out the variables. *)

  Definition lower_goal_atom (gc : GCtx) (lst lst' : GSLSt) (gst : GoalState Ev) :
    GSGAPred gs (DMLVar gs) -> LowAtom gs :=
    apred_bind_vars (erase_DLVar_goal lst lst' gst) (Ev := Ev) (B := Datatypes.Empty_set) ∘ (@g_item _ _).

  Definition gs_backend_decomp : ViewsAxiom GSView C -> Ensemble (GSBVCond gs) :=
    list_to_set ∘ (gs_build_vcs gctx_witness (gs := gs)).

  (** The GStarling template. *)

  Definition gs_template (gc : GCtx) : Template (LowView gs) (LabelC C * GSLStP * GSLStP) GSSStP :=
    builder_to_template gs_backend_decomp gs_views_decomp gc
                        (LCtx := GSLStP * GSLStP * GoalState Ev)
                        (P_Ep := PredEx_Cond_PrfIrr)
                        (R_Er := RelEx_Cond_PrfIrr)
                        (SSt := GSSStP)
  .

  Definition gs_PreInstance : GCtx ->
    PreInstance (LabelC C * GSLStP * GSLStP) GSSStP (V := LowView gs) :=
    builder_to_instance gs_backend_decomp gs_views_decomp gs_sig
                        (LCtx := GSLStP * GSLStP * GoalState Ev)
                        (P_Ep := PredEx_Cond_PrfIrr)
                        (R_Er := RelEx_Cond_PrfIrr)
                        (SSt := GSSStP)
  .

  Definition gs_LVFInstance (gc : GCtx) : LVFInstance C GSLStP GSSStP (V := LowView gs) :=
    {|
      lvfi_sig := gs_lsig gc;
      lvfi_axioms := axioms_cvf_to_lvf (gs_PreInstance gc).(v_axioms)
    |}.

  (** For soundness, see the Soundness module. *)
End gstarling_views_instance.

