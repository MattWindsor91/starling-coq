(** * GStarling frontend and resulting logic *)

From Starling Require Import
     Logics.GStarling.Core
     Logics.GStarling.Instance
.