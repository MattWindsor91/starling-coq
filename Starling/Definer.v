(* * Definers *)

From Coq Require Import
     Classes.SetoidClass
     Init.Specif.

Require Import Starling.Expr.

From Starling Require Import
     Views.Classes
     Views.Transformers.Subtype.

Class Definer (D B V E : Type) `{AdjView V} `{BEval B E} :=
  { define             : D -> V -> option B }.

Section Definer_evaluate.
  Context { B (* Boolean expression *)
            D (* Definer *)
            E (* Environment *)
            V (* View *)
            : Type }
          `{BEval B E}
          `{Definer D B V E}.

  Variable (d : D) (e : E) (v : V).
  
  Definition eval_defb (b : B) : bool :=
    match (eval e b) with
    | Some b => b
    | None => false
    end.
  
  Definition defb : bool := 
    match (define d v) with
    | Some def => eval_defb def
    | None => true
  end.

  Definition eval_def (b : B) : Prop :=
    Bool.Is_true (eval_defb b).

  Definition def : Prop :=
    Bool.Is_true defb.

  Lemma def_present (b : B) :
    def -> define d v = Some b -> eval_def b.
  Proof.
    intros Hdef Hdefb.
    unfold def, defb in Hdef.
    rewrite Hdefb in Hdef.
    exact Hdef.
  Qed.
End Definer_evaluate.

Section Definer_properties.
  Variable (D V : Type).

  Context { B (* Boolean expression *)
            E (* Environment *)
            : Type }
          `{BEval B E}
          `{Definer D B V E}.

  Variable (d : D) (e : E) (v : V).

  Definition def_eqv :=
    forall (a b : V), a == b -> def d e a <-> def d e b.

End Definer_properties.
             
Section Definer_restrict.
  Local Open Scope views_scope.
  
  (** ** Restricting views in a [Definer]

      If we have a [Definer] over views type [V], we can generate a [Definer]
      over the subtype [{ V | P }] for any property [P] just by referencing the
      original [Definer]. *)

  Variable D V : Type.

  Context {B E : Type}
          `{BEval B E, Definer D B V E}.

  (** We need a proposition [P], with the various closure hypotheses needed
      for [{ x : V | P x }] to be a view. *)

  Variable P : V -> Prop.
  Hypothesis Hdot_inc : forall x y : V, P x -> P y -> P (x * y).
  Hypothesis Hsub_inc : forall x y : V, P x -> P y -> P (x \ y).

  Instance Setoid_sub : Setoid { x : V | P x }.
  Proof.
    apply subtype_Setoid; assumption.
  Defined.

  Instance ViewsSemigroup_sub : ViewsSemigroup { x : V | P x }.
  Proof.
    apply subtype_ViewsSemigroup; assumption.
  Defined.
  
  Instance AdjView_sub : AdjView { x : V | P x }.
  Proof.
    apply subtype_AdjView; assumption.
  Defined.

  (** The restricted definer simply expands the subtype back into the
      original type. *)
  
  Global Instance Definer_restrict : Definer D B { x : V | P x } E :=
    { define d v := define d (proj1_sig v) }.
  
End Definer_restrict.

Section Definer_expand.

  (** ** Expanding views in a [Definer]

      If we have a [Definer] over views subtype [{ V | P }], where [P] is a
      decidable property, we can generate a [Definer] over the subtype [V] by
      setting all non-[P] [V] definitions to true.

      [V] must be a view in its own right. *)

  Variable D V : Type.
  Context
    {SV: Setoid V}
    {VV: ViewsSemigroup V}
    {OV: OrderedViewsSemigroup V}
    {AV: AdjView V}.

  (** We need a decidable proposition [P]. *)
  
  Variable P : V -> Prop.
  Hypothesis Hdec : forall v : V, { P v } + { ~ P v }.

  Context
    {B E: Type}
    {EV: Eval B bool E}
    {BEV: BEval B E}
    {RSV: Setoid {x: V | P x}}
    {RVV: ViewsSemigroup {x: V | P x}}
    {ROV: OrderedViewsSemigroup {x: V | P x}}
    {RAV: AdjView {x: V | P x}}
    {RD: Definer D B {x: V | P x} E}.

  (** The expanded definer simply returns undefined when [P x] doesn't
      hold. *)

  Definition expand_define (d : D) (v : V) : option B :=
    match Hdec v with
    | left p => @define D B {x: V | P x} E RSV RVV ROV RAV EV BEV RD d (exist P v p)
    | right _ => None
    end.
  
  Global Instance Definer_expand : Definer D B V E :=
    { define := expand_define }.
  
End Definer_expand.