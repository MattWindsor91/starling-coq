(** * Alphabet support: marked variables *)

From Coq Require Import
     Classes.SetoidClass
     Program.Basics
     Lists.List.

From Starling Require Import
     Backend.Alpha.SJoin
     Backend.Alpha.Var
     Utils.List.Disjoint
     Views.Transformers.Function
     Views.Transformers.Product
.

Inductive Mark :=
| Pre
| Post.

(** We can use a [Mark] to decide which of two states to select. *)

Definition choose_state {LSt : Type} (lst lst' : LSt) (m : Mark) : LSt :=
  match m with
  | Pre => lst
  | Post => lst'
  end.

(** [Marked] represents a variable marked with the state it's in. *)

Record Marked Var :=
  mk_Marked
    {
      m_mark : Mark;
      m_var  : Var;
    }.

Arguments mk_Marked [Var].
Arguments m_mark    [Var].
Arguments m_var     [Var].

(** [AMarkedVarL] is notation for a variable whose unmarked form appears in
    a given alphabet list. *)

Notation AMarkedVarL := (fun l => { x : Marked _ | List.In (m_var x) l }).

Section alpha_marked.

  Context { Var : Type }.

  Import ListNotations.
  Local Open Scope program_scope.

  Definition lift_AMarkedVarL (m : Mark) (x : Var) : AMarkedVarL [x].
  Proof.
    exists (mk_Marked m x).
    apply in_eq.
  Defined.

  Definition AVarL_to_AMarkedVarL {l : list Var} (m : Mark) (x : AVarL l) : AMarkedVarL l :=
    exist _ (mk_Marked m (proj1_sig x)) (proj2_sig x).

  Definition AMarkedVarL_cons (x : Var) (xs : list Var) : AMarkedVarL xs -> AMarkedVarL (x::xs).
  Proof.
    intros (v & Hv).
    exists v.
    apply in_cons, Hv.
  Defined.

  Definition AMarkedVarL_app_l (xs ys : list Var) : AMarkedVarL xs -> AMarkedVarL (xs++ys).
  Proof.
    intros (v & Hv).
    exists v.
    apply in_or_app.
    now left.
  Defined.

  Definition AMarkedVarL_app_r (xs ys : list Var) : AMarkedVarL ys -> AMarkedVarL (xs++ys).
  Proof.
    intros (v & Hv).
    exists v.
    apply in_or_app.
    now right.
  Defined.

  Section remove_domain.
    Context { Val : Type }.

    (** We can contravariantly _remove_ variables from the domain of a
        marked state function. *)

    Definition uncons_MarkedVarLFun (v : Var) (vs : list Var) (f : AMarkedVarL (v :: vs) -> Val)
      : AMarkedVarL vs -> Val :=
      f ∘ (AMarkedVarL_cons v vs).

    Definition unapp_MarkedVarLFun_l (xs ys : list Var) (f : AMarkedVarL (xs++ys) -> Val)
      : AMarkedVarL xs -> Val :=
      f ∘ (AMarkedVarL_app_l xs ys).

    Definition unapp_MarkedVarLFun_r (xs ys : list Var) (f : AMarkedVarL (xs++ys) -> Val)
      : AMarkedVarL ys -> Val :=
      f ∘ (AMarkedVarL_app_r xs ys).

  End remove_domain.

  Section state_marking.

    Variable Val : Type.

    Context {S_Val : Setoid Val}.

    (** We can convert a pair of unmarked state functions into a single marked
      function. *)

    Definition mark_states {l : list Var} (s s' : AVarL l -> Val)
               '(exist _ (mk_Marked m v) Hv : AMarkedVarL l) : Val :=
      (choose_state s s' m) (exist _ v Hv).

    Global Instance mark_states_Proper {l : list Var} :
      Proper (equiv ==> equiv ==> equiv) (mark_states (l := l)).
    Proof.
      intros s1 s2 Hs s'1 s'2 Hs' (([|] & vv) & Hv); cbn in *; intuition.
    Defined.

    Lemma uncons_mark_states_dist (v : Var) (vs : list Var)
          (s s' : AVarL (v :: vs) -> Val) :
      mark_states (uncons_VarLFun _ _ s) (uncons_VarLFun _ _ s') ==
      uncons_MarkedVarLFun _ _ (mark_states s s').
    Proof.
      now intros (([|] & x) & Hx).
    Qed.

    Lemma unapp_mark_states_dist_l (xs ys : list Var)
          (s s' : AVarL (xs++ys) -> Val) :
      mark_states (unapp_VarLFun_l _ _ s) (unapp_VarLFun_l _ _ s') ==
      unapp_MarkedVarLFun_l _ _ (mark_states s s').
    Proof.
      now intros (([|] & x) & Hx).
    Qed.

    Lemma unapp_mark_states_dist_r (xs ys : list Var)
          (s s' : AVarL (xs++ys) -> Val) :
      mark_states (unapp_VarLFun_r _ _ s) (unapp_VarLFun_r _ _ s') ==
      unapp_MarkedVarLFun_r _ _ (mark_states s s').
    Proof.
      now intros (([|] & x) & Hx).
    Qed.

    (** We can destruct a single marked function into a function that, given a
        marker, returns the pre-state or post-state accordingly. *)

    Definition unmark_state {l : list Var} (s : AMarkedVarL l -> Val)
               (m : Mark)
               '(exist _ v Hv : AVarL l) : Val :=
      s (exist _ (mk_Marked m v) Hv).

    (** Marking a pair of states, then projecting the result through
        [unmark_state], gives an equivalent function. *)

    Lemma mark_unmark_equiv {l : list Var} (s s' : AVarL l -> Val) :
      (s, s') ==
      (unmark_state (mark_states s s') Pre,
       unmark_state (mark_states s s') Post).
    Proof.
      split; now intros (v & Hv).
    Qed.
  End state_marking.
End alpha_marked.