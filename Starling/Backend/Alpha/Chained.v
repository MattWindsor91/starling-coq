From Coq Require Import
     Classes.SetoidClass
     Sets.Constructive_sets
.

From Starling Require Import
     Utils.Ensemble.Facts
     Utils.List.Facts
     Backend.Classes
     Backend.Alpha.Classes
     Backend.Alpha.PrfIrr
     Backend.Alpha.Var
     Backend.Transformers.Subtype
.

Set Implicit Arguments.
Set Universe Polymorphism.

Section domain_chaining.

  (** We can use predicate expressions over variable expressions with variable
      states (instead of [Ev Var -> option Val]). *)

  Variables
    Ep
    GCtx
    LCtx
    Var
    Val
  : Type.

  Variable
    Ev : Type -> Type.

  Context
    {S_Val : Setoid Val}
    {V_Ev : ValEx Ev (Val := Val)}
    {P_Ep : PredEx Ep GCtx LCtx (Ev Var -> Val)}
    {I_Ep : ImplPredEx Ep GCtx LCtx (Ev Var -> Val)}
  .

  (** We can use predicate expressions over variable expressions with variable
    states (instead of [Ev Var -> option Val]). *)

  Global Instance PredEx_Domain_Chained : PredEx Ep GCtx LCtx (Var -> Val) :=
    {|
      pe_true := pe_true (PredEx := P_Ep);
      pe_conj := pe_conj (PredEx := P_Ep);

      pe_interp (gc : GCtx) (lc : LCtx) (e : Ep) (s : Var -> Val) :=
        pe_interp gc lc e (ve_interp s (ValEx := V_Ev)) (PredEx := P_Ep);
    |}.
  Proof.
    - (* pe_true_unit *)
      intros gc lc s _.
      apply pe_true_True.
    - (* pe_interp_equiv *)
      intros gc lc ep s s' Hequiv.
      apply pe_interp_equiv.
      intro x.
      apply ve_interp_equiv, Hequiv.
    - (* pe_conj_distr *)
      intros gc lc ep1 ep2.
      split; intros s.
      + (* conjunction -> intersection *)
        intros Hin%pe_conj_distr%Intersection_inv.
        apply Intersection_intro; apply Hin.
      + (* conjunction <- intersection *)
        intros Hin%Intersection_inv.
        apply pe_conj_distr, Intersection_intro; apply Hin.
  Defined.

  (** Same for [ImplPredEx]. *)

  Global Instance ImplPredEx_Domain_Chained : ImplPredEx Ep GCtx LCtx (Var -> Val) :=
    {|
      pe_false := pe_false (ImplPredEx := I_Ep);
      pe_impl := pe_impl (ImplPredEx := I_Ep);
    |}.
  Proof.
    - (* pe_false_empty *)
      intros gc lc s Hfalse.
      contradict Hfalse.
      cbn.
      apply pe_false_False.
    - (* pe_impl_modus_ponens *)
      intros gc lc ep s s' Hequiv.
      cbn in *.
      eapply pe_impl_modus_ponens, Hequiv.
    - (* pe_impl_cond *)
      intros gc lc ep1 ep2 s Hcond.
      cbn in *.
      eapply pe_impl_cond, Hcond.
  Defined.
End domain_chaining.

Section eval_chaining.
  Variables
    Ep
    GCtx
    LCtx
    Var
    Val
  : Type.

  Variable
    Ev : Type -> Type.

  Context
    {S_Val : Setoid Val}
    {V_Ev : ValEx Ev (Val := Val)}
    {P_Ep : PredEx Ep GCtx LCtx (Var -> Val)}
    {I_Ep : ImplPredEx Ep GCtx LCtx (Var -> Val)}
  .

  (** We can use predicate expressions over variable expressions with variable
    states (instead of [Ev Var -> option Val]). *)

  Global Instance PredEx_EVal : PredEx Ep GCtx LCtx (Var -> EVal Ev) :=
    {|
      pe_true := pe_true (PredEx := P_Ep);
      pe_conj := pe_conj (PredEx := P_Ep);

      pe_interp (gc : GCtx) (lc : LCtx) (e : Ep) (s : Var -> EVal Ev) :=
        pe_interp gc lc e (eval_lift s (V_Ev := V_Ev)) (PredEx := P_Ep);
    |}.
  Proof.
    - (* pe_true_unit *)
      intros gc lc s _.
      apply pe_true_True.
    - (* pe_interp_equiv *)
      intros gc lc ep s s' Hequiv.
      apply pe_interp_equiv.
      intro x.
      apply Hequiv.
    - (* pe_conj_distr *)
      intros gc lc ep1 ep2.
      split; intros s.
      + (* conjunction -> intersection *)
        intros Hin%pe_conj_distr%Intersection_inv.
        apply Intersection_intro; apply Hin.
      + (* conjunction <- intersection *)
        intros Hin%Intersection_inv.
        apply pe_conj_distr, Intersection_intro; apply Hin.
  Defined.

  (** Same for [ImplPredEx]. *)

  Global Instance ImplPredEx_EVal : ImplPredEx Ep GCtx LCtx (Var -> EVal Ev) :=
    {|
      pe_false := pe_false (ImplPredEx := I_Ep);
      pe_impl := pe_impl (ImplPredEx := I_Ep);
    |}.
  Proof.
    - (* pe_false_empty *)
      intros gc lc s Hfalse.
      contradict Hfalse.
      cbn.
      apply pe_false_False.
    - (* pe_impl_modus_ponens *)
      intros gc lc ep s s' Hequiv.
      cbn in *.
      eapply pe_impl_modus_ponens, Hequiv.
    - (* pe_impl_cond *)
      intros gc lc ep1 ep2 s Hcond.
      cbn in *.
      eapply pe_impl_cond, Hcond.
  Defined.
End eval_chaining.

Section prfirr.
  Variables
    GCtx
    LCtx
    Var
    Val
  : Type.

  Variables
    Ep
    Ev
    : Type -> Type.

  Variable P : Var -> Prop.

  Context
    {S_Val : Setoid Val}
    {V_Ev : ValEx Ev (Val := Val)}
    {P_Ep : PredEx (Ep (Ev {x : Var | P x})) GCtx LCtx ({x : Var | P x} -> Val)}
    {I_Ep : ImplPredEx (Ep (Ev {x : Var | P x})) GCtx LCtx ({x : Var | P x} -> Val)}
  .

  (* TODO: Not having to duplicate these would be good. *)
  Global Instance PredEx_PrfIrr
    : PredEx (Ep (Ev {x : Var | P x})) GCtx LCtx (PrfIrrFun Var Val P) :=
    {|
      pe_true   := pe_true (PredEx := P_Ep);
      pe_conj   := pe_conj (PredEx := P_Ep);
      pe_interp gc lc e s :=
        pe_interp gc lc e (extract_pfun s) (PredEx := P_Ep);
    |}.
  Proof.
    - (* pe_true_unit *)
      intros gc lc.
      apply (proj1 (Full_set_inc_same _)).
      intros ((s & Hs)) _.
      apply pe_true_True.
    - (* pe_interp_equiv *)
      intros gc lc ep ((s & Hs)) ((s' & Hs')) Hequiv.
      now apply pe_interp_equiv.
    - (* pe_conj_distr *)
      intros gc lc ep1 ep2.
      split; intros ((s & Hs)).
      + (* conjunction -> intersection *)
        intros Hin%pe_conj_distr%Intersection_inv.
        apply Intersection_intro; apply Hin.
      + (* conjunction <- intersection *)
        intros Hin%Intersection_inv.
        apply pe_conj_distr, Intersection_intro; apply Hin.
  Defined.

  Global Instance ImplPredEx_PrfIrr
    : ImplPredEx (Ep (Ev {x : Var | P x})) GCtx LCtx (PrfIrrFun Var Val P)  :=
    {|
      pe_false := pe_false (ImplPredEx := I_Ep);
      pe_impl := pe_impl (ImplPredEx := I_Ep);
    |}.
  Proof.
    - (* pe_false_empty *)
      intros gc lc ((s & Hs)) Hfalse.
      cbn in Hfalse.
      now apply pe_false_False in Hfalse.
    - (* pe_impl_modus_ponens *)
      intros gc lc ep1 ep2 ((s & Hs)) Hequiv.
      cbn in *.
      eapply pe_impl_modus_ponens, Hequiv.
    - (* pe_impl_cond *)
      intros gc lc ep1 ep2 ((s & Hs)) Hcond.
      cbn in *.
      eapply pe_impl_cond, Hcond.
  Defined.
End prfirr.

Section eval_EqPredEx.

  Variables
    GCtx
    LCtx
    Val
  : Type.

  Variables
    Ep
    Ev
    : Type -> Type.

  Context
    {S_Val : Setoid Val}
    {V_Ev : ValEx Ev (Val := Val)}
    {E_Ep : EqPredEx GCtx LCtx Ep}.

  Ltac fmap_eval A :=
    intros;
    intros gc lc;
    cbn;
    split; intros s; apply A.

  Global Instance EqPredEx_EVal :
         EqPredEx GCtx LCtx Ep (Val := Ev Datatypes.Empty_set) :=
    {
      ape_PredEx     (Var : Type) := PredEx_EVal (Ep := Ep Var) (V_Ev := V_Ev);
      ape_ImplPredEx (Var : Type) := ImplPredEx_EVal (Ep := Ep Var) (V_Ev := V_Ev);

      ape_eq := ape_eq;
    }.
  Proof.
    - (* ape_eq_val_eq *)
      intros Var x y gc lc s.
      split.
      + intros Hin%ape_eq_val_eq.
        apply eval_equiv_req, Hin.
      + intros Hin%eval_equiv_req.
        now apply ape_eq_val_eq.
    - (* ape_state_fmap *)
      intros A B x f gc lc s.
      apply ape_state_fmap.
    - fmap_eval ape_fmap_true.
    - fmap_eval ape_fmap_false.
    - fmap_eval ape_fmap_conj.
    - fmap_eval ape_fmap_impl.
    - fmap_eval ape_fmap_eq.
  Defined.
End eval_EqPredEx.