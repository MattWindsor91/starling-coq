From Coq Require Import
     Omega
     Program.Program
     Vectors.Fin
.

From Starling Require Import
     Backend.Alpha.Marked
     Backend.Alpha.Var
     Utils.List.Facts
.

Inductive DefVar (Var : Type) : Type :=
| SharedVar (v : Var)
| ParamRef (r : nat).

Arguments SharedVar {Var} v.
Arguments ParamRef {Var} r.

Set Implicit Arguments.

Section DefVar_ops.
  Local Open Scope program_scope.

  Definition destruct_DefVar
             {Var T : Type}
             (f : Var -> T) (g : nat -> T)
             (v : DefVar Var) : T :=
    match v with
    | SharedVar s => f s
    | ParamRef r => g r
    end.

  (** [defvar_map] applies [fv] to [v] if it's a variable, and
      [fk] to [v] if it's a parameter reference. *)

  Definition defvar_map
             {Var Var' : Type}
             (fv : Var -> Var') (fk : nat -> nat) :
             DefVar Var -> DefVar Var' :=
    destruct_DefVar (SharedVar ∘ fv) (ParamRef ∘ fk).

  Lemma defvar_map_id {Var : Type} {n : nat} (v : DefVar Var) :
    defvar_map id id v = v.
  Proof.
    now destruct v.
  Qed.

  Definition on_vars {Var : Type} (P : Var -> Prop) : DefVar Var -> Prop :=
    destruct_DefVar P (const True).

  Definition vars_in {Var : Type} (l : list Var) : DefVar Var -> Prop :=
    on_vars (ni l).

  (** We can lift an [AVarL] into a [DefVar] with a [vars_in] stanza. *)

  Definition vars_in_lift {Var : Type}
          (l : list Var) (v : AVarL l) :
    { x : DefVar Var | vars_in l x } :=
    match v with
    | exist _ v Hv => exist _ (SharedVar v) Hv
    end.

  (** We can also lift a parameter reference. *)

  Definition vars_in_lift_ref {Var : Type}
             (l : list Var) (k : nat) :
    { x : DefVar Var | vars_in l x } :=
    exist _ (ParamRef k) I.

  Definition defvar_vars_in_lift {Var : Type}
             (l : list Var) (dv : DefVar (AVarL l)) :
    { x : DefVar Var | vars_in l x } :=
    match dv with
    | SharedVar (exist _ s Hs) => exist _ (SharedVar s) Hs
    | ParamRef k => exist _ (ParamRef k) I
    end.

  Definition mvars_in {Var : Type} (l : list Var) : DefVar (Marked Var) -> Prop :=
    on_vars (ni l ∘ (@m_var Var)).

  (** We can lift an [AMarkedVarL] into a [DefVar] with an [mvars_in]
      stanza. *)

  Definition mvars_in_lift {Var : Type}
          (l : list Var) (v : AMarkedVarL l) :
    { x : DefVar (Marked Var) | mvars_in l x } :=
    match v with
    | exist _ v Hv => exist _ (SharedVar v) Hv
    end.

  (** We can also lift a parameter reference. *)

  Definition mvars_in_lift_ref {Var : Type}
             (l : list Var) (k : nat) :
    { x : DefVar (Marked Var) | mvars_in l x } :=
    exist _ (ParamRef k) I.

  Definition defvar_mvars_in_lift {Var : Type}
             (l : list Var) (dv : DefVar (AMarkedVarL l)) :
    { x : DefVar (Marked Var) | mvars_in l x } :=
    match dv with
    | SharedVar (exist _ s Hs) => exist _ (SharedVar s) Hs
    | ParamRef k => exist _ (ParamRef k) I
    end.

  Definition defvar_map_var {A B : Type} (f : A -> B) :
    DefVar A -> DefVar B :=
    defvar_map f id.

  Definition defvar_map_ref {A : Type} (f : nat -> nat) :
    DefVar A -> DefVar A :=
    defvar_map id f.

  Definition vars_in_map {Var : Type}
             (l l' : list Var)
             (fv : AVarL l -> AVarL l')
             (fk : nat -> nat)
             (v : { x : DefVar Var | vars_in l x }) :
    { x : DefVar Var | vars_in l' x }.
  Proof.
    refine
      match v with
      | exist _ u Hu =>
        match u as u' return (u = u') -> _ with
        | SharedVar s => fun _ => exist _ (SharedVar (` (fv (exist _ s _)))) _
        | ParamRef  k => fun _ => exist _ (ParamRef (fk k)) _
        end _
      end; intuition.
    destruct v as (v & Hv).
    Unshelve.
    3: now subst.
    - subst.
      apply (proj2_sig (fv (exist (ni l) s Hu))).
    - easy.
  Defined.

  Definition vars_in_map_var {Var : Type}
             (l l' : list Var) (f : AVarL l -> AVarL l') :
    { x : DefVar Var | vars_in l x } ->
    { x : DefVar Var | vars_in l' x } :=
    vars_in_map f id.

  Definition vars_in_map_ref {Var : Type}
             (l : list Var) (f : nat -> nat) :
    { x : DefVar Var | vars_in l x } ->
    { x : DefVar Var | vars_in l x } :=
    vars_in_map id f.

  Definition mvars_in_map {Var : Type}
             (l l' : list Var)
             (fv : AMarkedVarL l -> AMarkedVarL l')
             (fk : nat -> nat)
             (v : { x : DefVar (Marked Var) | mvars_in l x }) :
    { x : DefVar (Marked Var) | mvars_in l' x }.
  Proof.
    refine
      match v with
      | exist _ u Hu =>
        match u as u' return (u = u') -> _ with
        | SharedVar s => fun _ => exist _ (SharedVar (` (fv (exist _ s _)))) _
        | ParamRef  k => fun _ => exist _ (ParamRef (fk k)) _
        end _
      end; intuition.
    destruct v as (v & Hv).
    Unshelve.
    3: now subst.
    - subst.
      apply (proj2_sig (fv (exist (ni l ∘ m_var (Var := _)) s Hu))).
    - easy.
  Defined.

  Definition mvars_in_map_var {Var : Type}
             (l l' : list Var) (f : AMarkedVarL l -> AMarkedVarL l') :
    { x : DefVar (Marked Var) | mvars_in l x } ->
    { x : DefVar (Marked Var) | mvars_in l' x } :=
    mvars_in_map f id.

  Definition mvars_in_map_ref {Var : Type}
             (l : list Var) (f : nat -> nat) :
    { x : DefVar (Marked Var) | mvars_in l x } ->
    { x : DefVar (Marked Var) | mvars_in l x } :=
    mvars_in_map id f.

  Lemma fin_up_inc (n k : nat) (Hnk : n <= k) : n + (k - n) = k.
  Proof.
    omega.
  Qed.

  (** [Fin.cast]'s result doesn't depend on the proof of [n = k]. *)

  Lemma cast_prfirr (n k : nat) (Hnk Hnk' : n = k) (x : Fin.t n) :
    Fin.cast x Hnk = Fin.cast x Hnk'.
  Proof.
    generalize dependent k.
    induction x.
    - cbn.
      now destruct k.
    - cbn.
      destruct k; try easy.
      intros Hnk Hnk'.
      f_equal.
      apply (IHx _ (f_equal Init.Nat.pred Hnk) (f_equal Init.Nat.pred Hnk')).
  Qed.

  (** We can lift a [Fin.t] into a reflexive cast. *)

  Lemma cast_add (n : nat) (x : Fin.t n) :
    x = Fin.cast x eq_refl.
  Proof.
    induction x; cbn; congruence.
  Qed.

  Definition fin_up (n k : nat) (Hnk : n <= k) (x : Fin.t n) : Fin.t k :=
    Fin.cast (Fin.L (k-n) x) (fin_up_inc Hnk).

  Lemma fin_up_prfirr (n k : nat) (Hnk Hnk' : n <= k) (x : Fin.t n) :
    fin_up Hnk x = fin_up Hnk' x.
  Proof.
    apply cast_prfirr.
  Qed.
End DefVar_ops.
