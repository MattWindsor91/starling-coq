(** * Alphabet support: using marked [PredEx] as [RelEx] *)

From Coq Require Import
     Bool
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles
     (* These are placed here to override earlier imports *)
     Lists.List
     Classes.SetoidClass.

From Starling Require Import
     Views.Transformers.Subtype  (* For Setoid on prfirr functions *)
     Views.Transformers.Function (* For Setoid X -> Y *)
     Utils.Ensemble.Facts
     Utils.List.Disjoint
     Utils.List.Facts
     Utils.Monads
     Utils.Option.Facts
     Backend.Classes
     Backend.Alpha.Classes
     Backend.Alpha.Chained
     Backend.Alpha.Marked
     Backend.Alpha.PrfIrr
     Backend.Alpha.SJoin
     Backend.Alpha.Var
     Backend.Transformers.Subtype (* For PredEx on prfirr functions *)
.
Set Implicit Arguments.
Set Universe Polymorphism.

Section marked_framing.
  Polymorphic Universe v.
  Variable Var : Type@{v}.

  Polymorphic Universe ev.
  Variable Ev : Type@{v} -> Type@{ev}.

  Polymorphic Universe ep.
  Variable Ep : Type@{v} -> Type@{ep}.

  Variables
    (Val : Set)
    (LCtx GCtx : Type).

  Hypothesis Heq_dec : forall (x y : Var), {x = y} + {x <> y}.

  Context
    {S_Val : Setoid Val}
    {V_Ev  : ValEx Ev (Val := Val)}
    {E_Ep  : EqPredEx GCtx LCtx Ep (Val := Val)}.

  Import ListNotations.

  Definition return_AVarL (x : Var) : Ev (AVarL [x]) := mreturn (lift_AVarL x).

  (** Lifts a variable to a marked variable expression. *)

  Definition return_AMarkedVarL (m : Mark) (x : Var) :
    Ev (AMarkedVarL [x]) :=
    mreturn (lift_AMarkedVarL m x).

  Definition gen_frame_eq (x : Var) : Ep (Ev (AMarkedVarL [x])) :=
    ape_eq (return_AMarkedVarL Pre x)
           (return_AMarkedVarL Post x).

  Definition gen_frame (l : list Var) : Ep (Ev (AMarkedVarL l)).
  Proof.
    induction l.
    - apply (pe_true (PredEx := ape_PredEx _)).
    - (* First, expand the variable set of the recursive case. *)
      apply (map_vars (AMarkedVarL_cons a l)) in IHl.
      (* Then, build the new frame... *)
      pose (gen_frame_eq a) as nframe.
      (* ...and expand its variable set to match. *)
      apply (map_vars (AMarkedVarL_app_l [a] l)) in nframe.
      exact (pe_conj nframe IHl (PredEx := ape_PredEx _)).
  Defined.

  Definition gen_frame_r (l l' : list Var) : Ep (Ev (AMarkedVarL (l ++ l'))) :=
    map_vars (AMarkedVarL_app_r l l') (gen_frame l').

  Definition add_frame (l l' : list Var) (ep : Ep (Ev (AMarkedVarL l)))
    : Ep (Ev (AMarkedVarL (l ++ l'))) :=
    pe_conj
      (map_vars (AMarkedVarL_app_l l l') ep)
      (gen_frame_r l l').

  Definition re_interp_MarkedPredEx
             (l : list Var) (gc : GCtx) (lc : LCtx) (e : Ep (Ev (AMarkedVarL l)))
             (s s' : PrfIrrFun Var Val (ni l)) : Prop :=
    pe_interp gc lc e
              (ve_interp (Var := AMarkedVarL l) (extract_pfun (mark_states_prfirr _ s s'))).

  Local Open Scope predicate_scope.

  (** [gen_frame] is the identity when used as a relational expression. *)
  Lemma re_id_id_MarkedPredEx (l : list Var) (gc : GCtx) (lc : LCtx):
    (re_interp_MarkedPredEx gc lc (gen_frame l) : binary_relation (PrfIrrFun Var Val (ni l)))
      <∙>
      equiv.
  Proof.
    intros s1 s2.
    destruct s1 as ((s1' & Hs1')) eqn:Hs1, s2 as ((s2' & Hs2')) eqn:Hs2.
    split.
    - (* id -> s == s' *)
      intros Hinterp (x & Hx).
      induction l.
      + (* Base case: no variables (contradiction) *)
        contradiction.
      + (* Inductive case: either x is a or it's in l. *)
        apply pe_conj_distr, Intersection_inv in Hinterp.
        destruct Hinterp as [Hinterp_a Hinterp_l].
        destruct Hx as [->|Hx_l].
        * (* It's a. *)
          unfold gen_frame_eq, map_vars, AMarkedVarL_app_l in Hinterp_a.
          apply ape_fmap_eq in Hinterp_a.
          (* Now follows a large amount of monad manipulation. *)
          unfold return_AMarkedVarL in Hinterp_a.
          rewrite ! monad_fmap in Hinterp_a.
          unfold compose in Hinterp_a.
          autorewrite with monad in Hinterp_a.
          apply ape_eq_val_eq, ve_eq_eval_var in Hinterp_a.
          cbn in *.
          (* Now we need to push through the proof irrelevance. *)
          rewrite (Hs1' _ (exist (fun x0 : Var => x = x0 \/ In x0 l) x (in_or_app [x] l x (or_introl (in_eq x [])))));
            try reflexivity.
          rewrite (Hs2' _ (exist (fun x0 : Var => x = x0 \/ In x0 l) x (in_or_app [x] l x (or_introl (in_eq x [])))));
            try reflexivity.
          apply Hinterp_a.
        * (* It's in l. *)
          destruct (uncons_PrfIrrVarLFun _ _ _ s1) as ((us1' & Hus1')) eqn:Hus1,
                   (uncons_PrfIrrVarLFun _ _ _ s2) as ((us2' & Hus2')) eqn:Hus2.                                               specialize (IHl _ _ us1' Hus1' Hus1 us2' Hus2' Hus2).
          subst.
          (* Push through proof irrelevance. *)
          rewrite (Hs1' _ (AVarL_cons a _ (exist (fun x : Var => flip (In (A:= Var)) l x) x Hx_l)));
            try reflexivity.
          rewrite (Hs2' _ (AVarL_cons a _ (exist (fun x : Var => flip (In (A:= Var)) l x) x Hx_l)));
            try reflexivity.
          inversion Hus1; subst.
          inversion Hus2; subst.
          apply IHl.
          (* We need to push through a lot of fmap equivalences here. *)
          eapply pe_interp_equiv.
          -- intro v.
             apply ve_interp_equiv.
             cbn.
             rewrite uncons_mark_states_dist.
             intro u.
             now instantiate (1 := mark_states _ s1' s2' ∘ (AMarkedVarL_cons a l)).
          -- apply -> ape_state_fmap in Hinterp_l.
             eapply pe_interp_equiv.
             ++ instantiate (1 := (ve_interp (Var := _) (mark_states _ s1' s2') ∘
                                             fmap (AMarkedVarL_cons a l))).
                intros u.
                apply symmetry, ve_state_fmap.
             ++ apply Hinterp_l.
    - (* s == s' -> id *)
      intro Heqv.
      unfold gen_frame.
      induction l; cbn.
      + apply pe_true_True.
      + specialize (IHl (uncons_PrfIrrVarLFun _ _ _ s1)
                        (uncons_PrfIrrVarLFun _ _ _ s2)).
        cbn in *.
        apply pe_conj_distr, Intersection_intro.
        * (* Show the newly added variable works correctly. *)
          unfold gen_frame_eq, map_vars, AMarkedVarL_app_l.
          apply ape_fmap_eq.
          (* Now follows a large amount of monad manipulation. *)
          unfold return_AMarkedVarL.
          rewrite ! monad_fmap, ! monad_identity_left.
          apply ape_eq_val_eq, ve_eq_eval_var, Heqv.
        * (* Inductive case. *)
          apply ape_state_fmap.
          eapply pe_interp_equiv.
          -- instantiate (1 := ve_interp (Var := _)
                                         (mark_states _
                                                      (uncons_VarLFun _ _ s1')
                                                      (uncons_VarLFun _ _ s2'))).
             intro u.
             unfold flip, compose.
             rewrite ve_state_fmap.
             apply ve_interp_equiv, symmetry.
             apply uncons_mark_states_dist.
          -- eapply IHl.
             ++ now rewrite Hs1.
             ++ now rewrite Hs2.
             ++ intro u.
                apply Heqv.
  Qed.

  Global Instance re_interp_MarkedPredEx_Proper
         (l : list Var)
         (e : Ep (Ev (AMarkedVarL l)))
         (gc : GCtx) (lc : LCtx) :
    Proper (equiv ==> equiv ==> iff) (re_interp_MarkedPredEx gc lc e).
  Proof.
    intros s1 s2 Hs s'1 s'2 Hs'.
    apply pe_interp_equiv.
    intro v.
    now apply ve_interp_equiv, PrfIrrFun_extract_Proper, mark_states_prfirr_Proper.
  Defined.

  Definition re_empty_MarkedPredEx (l : list Var) :
    Ep (Ev (AMarkedVarL l)) :=
    pe_false (ImplPredEx := ape_ImplPredEx _).

  Lemma re_empty_empty_MarkedPredEx (l : list Var) (gc : GCtx) (lc : LCtx) :
    (re_interp_MarkedPredEx gc lc (re_empty_MarkedPredEx l)
     : binary_relation (PrfIrrFun Var Val (ni l)))
      -∙> false_predicate.
  Proof.
    intros s s' Hcontra.
    now apply pe_false_empty in Hcontra.
  Qed.

  (** If our states are functions from unmarked variables to values that don't
        depend on the proof of said variables being in a specific list, we can
        lift our predicate expression language to a relational one by using
        marking! *)

  Global Instance RelEx_MarkedPredEx (l : list Var)
    : RelEx (Ep (Ev (AMarkedVarL l))) GCtx LCtx (PrfIrrFun Var Val (ni l)) :=
    {
      re_id := gen_frame l;
      re_empty := re_empty_MarkedPredEx l;
      re_interp := re_interp_MarkedPredEx (l := l);
      re_id_id := re_id_id_MarkedPredEx (l := l);
      re_empty_empty := re_empty_empty_MarkedPredEx (l := l);
    }.

  (** [frame_BVCond] frames a [BVCond] from variable list [l] to
        variable list [l']. *)

  Local Open Scope backend_scope.

  Lemma sjoin_map_l (a1 a2 : list Var) (gc : GCtx) (lc : LCtx)
        (ep : Ep (Ev (AVarL a1)))
        (s1 : AVarL a1 -> Val)
        (s2 : AVarL a2 -> Val)
        (Hprf_irr : forall v v', proj1_sig v = proj1_sig v' -> s1 v == s1 v')
        (Hdisj : ldisj Heq_dec a1 a2) :
    pe_interp gc lc
              (map_vars (AVarL_app_l a1 a2) ep)
              (sjoin Heq_dec Hdisj s1 s2)
    <->
    pe_interp gc lc ep s1.
  Proof.
    split.
    - (* joined -> unjoined *)
      intro Hin_sjoin.
      apply -> ape_state_fmap in Hin_sjoin.
      cbn.
      eapply pe_interp_equiv.
      + intro v.
        instantiate
          (1 := ve_interp
                  (sjoin Heq_dec Hdisj s1 s2) ∘
                  fmap (AVarL_app_l a1 a2)).
        unfold compose.
        rewrite ve_state_fmap.
        apply ve_interp_equiv, (sjoin_l _ Hdisj), Hprf_irr.
      + apply Hin_sjoin.
    - (* unjoined -> joined *)
      intro Hin_orig.
      apply ape_state_fmap.
      eapply pe_interp_equiv.
      2: apply Hin_orig.
      intro ev.
      unfold flip, compose.
      rewrite ve_state_fmap.
      apply ve_interp_equiv, symmetry, (sjoin_l _ Hdisj), Hprf_irr.
  Qed.

  Lemma sjoin_preserves_prfirr
        (a1 a2 : list Var) (gc : GCtx) (lc : LCtx)
        (s1 : AVarL a1 -> Val)
        (s2 : AVarL a2 -> Val)
        (Hprf_irr1 : forall v v', proj1_sig v = proj1_sig v' -> s1 v == s1 v')
        (Hprf_irr2 : forall v v', proj1_sig v = proj1_sig v' -> s2 v == s2 v')
        (Hdisj Hdisj' : ldisj Heq_dec a1 a2) :
    forall v v', proj1_sig v = proj1_sig v' -> sjoin _ Hdisj s1 s2 v == sjoin _ Hdisj' s1 s2 v'.
  Proof.
    intros (v & Hv) (v' & Hv') Heq_v.
    unfold proj1_sig in Heq_v.
    subst.
    reduce_sjoin' v' a1 a2.
  Qed.

  Lemma sjoin_interp_swap
        (a1 a2 : list Var) (gc : GCtx) (lc : LCtx)
        (ep : Ep (Ev (AVarL a1)))
        (s1 : AVarL a1 -> Val)
        (s2 : AVarL a2 -> Val)
        (Hprf_irr1 : forall v v', proj1_sig v = proj1_sig v' -> s1 v == s1 v')
        (Hprf_irr2 : forall v v', proj1_sig v = proj1_sig v' -> s2 v == s2 v')
        (Hdisj : ldisj Heq_dec a1 a2)
        (Hdisj' : ldisj Heq_dec a2 a1) :
    pe_interp gc lc (map_vars (AVarL_app_l a1 a2) ep) (sjoin Heq_dec Hdisj s1 s2) <->
    pe_interp gc lc (map_vars (AVarL_app_r a2 a1) ep) (sjoin Heq_dec Hdisj' s2 s1).
  Proof.
    split;
      (* Common to both sides *)
      intro Horig;
      apply ape_state_fmap;
      apply -> ape_state_fmap in Horig;
      autounfold in *;
      eapply pe_interp_equiv;
      try apply Horig;
      intro e;
      unfold flip;
      rewrite ! ve_state_fmap;
      apply ve_interp_equiv;
      intro v;
      unfold compose.
    - (* Forwards *)
      transitivity (sjoin Heq_dec
                          (ldisj_comm Heq_dec a1 a2 Hdisj)
                          s2 s1
                          (AVarL_flip_app _ _ (AVarL_app_l a1 a2 v))).
      {
        apply (sjoin_preserves_prfirr gc lc s2 s1 Hprf_irr2 Hprf_irr1).
        now destruct v as (v & Hv).
      }
      apply symmetry, sjoin_comm.
    - (* Backwards *)
      symmetry.
      transitivity (sjoin Heq_dec
                          (ldisj_comm Heq_dec a1 a2 Hdisj)
                          s2 s1
                          (AVarL_flip_app _ _ (AVarL_app_l a1 a2 v))).
      {
        apply (sjoin_preserves_prfirr gc lc s2 s1 Hprf_irr2 Hprf_irr1).
        now destruct v as (v & Hv).
      }
      apply symmetry, sjoin_comm.
  Qed.

  Corollary sjoin_map_r (a1 a2 : list Var) (gc : GCtx) (lc : LCtx)
            (ep : Ep (Ev (AVarL a2)))
            (s1 : AVarL a1 -> Val)
            (s2 : AVarL a2 -> Val)
            (Hprf_irr1 : forall v v', proj1_sig v = proj1_sig v' -> s1 v == s1 v')
            (Hprf_irr2 : forall v v', proj1_sig v = proj1_sig v' -> s2 v == s2 v')
            (Hdisj : ldisj Heq_dec a1 a2) :
    pe_interp gc lc
              (map_vars (AVarL_app_r a1 a2) ep)
              (sjoin Heq_dec Hdisj s1 s2) <->
    pe_interp gc lc ep s2.
  Proof.
    split.
    - (* joined -> unjoined *)
      intros Hin_sjoin.
      eapply sjoin_interp_swap in Hin_sjoin; try assumption.
      instantiate (1 := ldisj_comm _ _ _ Hdisj) in Hin_sjoin.
      apply (sjoin_map_l gc lc ep s2 s1 Hprf_irr2 (ldisj_comm _ _ _ Hdisj)), Hin_sjoin.
    - (* unjoined -> joined *)
      intros Hin_unjoin.
      eapply sjoin_interp_swap; try assumption.
      instantiate (1 := ldisj_comm _ _ _ Hdisj).
      apply (sjoin_map_l gc lc ep s2 s1 Hprf_irr2 (ldisj_comm _ _ _ Hdisj)), Hin_unjoin.
  Qed.

  (** If we only frame one side of a split variable alphabet, we still get
      an equivalence over states restricted to that alphabet. *)

  Lemma gen_frame_r_equiv
        (l1 l2 : list Var)
        (Hdisj: ldisj Heq_dec l1 l2)
        (gc : GCtx) (lc : LCtx) (s s' : PrfIrrFun Var Val (ni (l1++l2))) :
    re_interp_MarkedPredEx gc lc (gen_frame_r l1 l2) s s' <->
    unapp_PrfIrrVarLFun_r _ _ _ s == unapp_PrfIrrVarLFun_r _ _ _ s'.
  Proof.
    split.
    - intro Hinterp.
      pose proof (state_split_PrfIrr _ Heq_dec Hdisj s) as Hs_split.
      pose proof (state_split_PrfIrr _ Heq_dec Hdisj s') as Hs'_split.
      apply (re_id_id_MarkedPredEx gc lc (unapp_PrfIrrVarLFun_r Val l1 l2 s) (unapp_PrfIrrVarLFun_r Val l1 l2 s')).
      apply (re_interp_MarkedPredEx_Proper _ gc lc s _ Hs_split s' _ Hs'_split) in Hinterp.
      destruct s as ((s & Hs)), s' as ((s' & Hs')).
      apply -> ape_state_fmap in Hinterp.
      refine (proj1 (pe_interp_equiv gc lc _ _ _ _) Hinterp).
      intro v.
      unfold compose.
      rewrite ve_state_fmap.
      apply ve_interp_equiv.
      intros (([|] & k) & Hk); cbn in *; reduce_sjoin' k l1 l2.
    - intro Hequiv.
      pose proof (state_split_PrfIrr _ Heq_dec Hdisj s) as Hs_split.
      pose proof (state_split_PrfIrr _ Heq_dec Hdisj s') as Hs'_split.
      apply (re_interp_MarkedPredEx_Proper _ gc lc s _ Hs_split s' _ Hs'_split).
      apply (re_id_id_MarkedPredEx gc lc (unapp_PrfIrrVarLFun_r Val l1 l2 s) (unapp_PrfIrrVarLFun_r Val l1 l2 s')) in Hequiv.
      destruct s as ((s & Hs)), s' as ((s' & Hs')).
      apply ape_state_fmap.
      cbn in *.
      refine (proj1 (pe_interp_equiv gc lc _ _ _ _) Hequiv).
      intro v.
      unfold compose.
      rewrite ve_state_fmap.
      apply ve_interp_equiv.
      intros (([|] & k) & Hk); cbn in *; reduce_sjoin' k l1 l2.
  Qed.

  (** [frame_conj] conjoins two predicate expressions with different
      alphabets. *)

  Definition frame_conj (l r : list Var)
             (le : Ep (Ev (AVarL l)))
             (re : Ep (Ev (AVarL r)))
    : Ep (Ev (AVarL (l++r))) :=
    pe_conj (map_vars (AVarL_app_l l r) le)
            (map_vars (AVarL_app_r l r) re)
            (PredEx := ape_PredEx _ (EqPredEx := E_Ep)).

  (** [frame_conj_marked] does the same, but for marked variables. *)

  Definition frame_conj_marked (l r : list Var)
             (le : Ep (Ev (AMarkedVarL l)))
             (re : Ep (Ev (AMarkedVarL r)))
    : Ep (Ev (AMarkedVarL (l++r))) :=
    pe_conj (map_vars (AMarkedVarL_app_l l r) le)
            (map_vars (AMarkedVarL_app_r l r) re)
            (PredEx := ape_PredEx _ (EqPredEx := E_Ep)).

  (** Using these conjunctions, we can combine two [BVCond]s. *)

  Definition combine_BVCond  (l r : list Var)
          (lx : BVCond (Ep (Ev (AVarL       l)))
                       (Ep (Ev (AMarkedVarL l))))
          (rx : BVCond (Ep (Ev (AVarL       r)))
                       (Ep (Ev (AMarkedVarL r)))) :
    BVCond (Ep (Ev (AVarL       (l++r))))
           (Ep (Ev (AMarkedVarL (l++r)))) :=
    mk_BVCond
      (frame_conj        lx.(bv_w) rx.(bv_w))
      (frame_conj_marked lx.(bv_c) rx.(bv_c))
      (frame_conj        lx.(bv_g) rx.(bv_g)).

  Hint Unfold
       frame_conj
       frame_conj_marked
       unapp_VarLFun_l.

  Theorem bvhoare_compositionality (gc : GCtx) (l r : list Var)
          (lx : BVCond (Ep (Ev (AVarL l)))
                       (Ep (Ev (AMarkedVarL l))))
          (rx : BVCond (Ep (Ev (AVarL r)))
                       (Ep (Ev (AMarkedVarL r)))) :
    ldisj Heq_dec l r ->
    bvhoare gc lx (LCtx := LCtx)
            (S := PrfIrrFun Var Val (ni l))
            (P_Ep := PredEx_PrfIrr Ep Ev (ni l))
            (P_Er := RelEx_MarkedPredEx l) ->
    bvhoare gc rx (LCtx := LCtx)
            (S := PrfIrrFun Var Val (ni r))
            (P_Ep := PredEx_PrfIrr Ep Ev (ni r))
            (P_Er := RelEx_MarkedPredEx r) ->
    bvhoare gc (combine_BVCond lx rx) (LCtx := LCtx)
            (S := PrfIrrFun Var Val _)
            (P_Ep := PredEx_PrfIrr Ep Ev (ni _))
            (P_Er := RelEx_MarkedPredEx _).
  Proof.
    intros Hdisj Hlx Hrx s s' lc Hin_p Hcmd.
    (* Push through the [sjoin]s. *)
    eapply pe_interp_equiv.
    {
      apply state_split_PrfIrr with (Hdisj := Hdisj).
    }
    eapply pe_interp_equiv in Hin_p.
    2: {
      apply symmetry, state_split_PrfIrr with (Hdisj := Hdisj).
    }
    (* Now strip away the proof irrelevance wrappers. *)
    destruct s as ((s & Hs)), s' as ((s' & Hs')).
    cbn in *.
    unfold re_interp_MarkedPredEx, add_frame in Hcmd.
    cbn in Hcmd.
    autounfold in *.
    (* Split the predicates into [l] and [r] sections. *)
    destroy_conj.
    + (* l section *)
      apply sjoin_map_l.
      * intros (v & Hv) (v' & Hv') Heq.
        now apply Hs'.
      * apply
          (Hlx (unapp_PrfIrrVarLFun_l _ _ _ (Pfun _ _ _ (exist _ s Hs)))
               (unapp_PrfIrrVarLFun_l _ _ _ (Pfun _ _ _ (exist _ s' Hs')))).
        -- (* Show that the precondition held *)
          apply sjoin_map_l in H.
          ++ apply H.
          ++ intros (v & Hv) (v' & Hv').
             intuition.
        -- (* Show that the command held *)
          (* Move the unapp outside of mark_states. *)
          eapply pe_interp_equiv.
          {
            intro v.
            apply ve_interp_equiv, unapp_mark_states_dist_l.
          }
          (* Get the two commands into roughly the same form. *)
          apply -> ape_state_fmap in H1.
          (* Now we can show that their states are equivalent. *)
          refine (proj1 (pe_interp_equiv gc lc lx.(bv_c) _ _ _) H1).
          intro v.
          apply ve_state_fmap.
    + (* r section *)
      apply sjoin_map_r;
        try (intros (v & Hv) (v' & Hv') Heq; now apply Hs').
      * apply
          (Hrx (unapp_PrfIrrVarLFun_r _ _ _ (Pfun _ _ _ (exist _ s Hs)))
               (unapp_PrfIrrVarLFun_r _ _ _ (Pfun _ _ _ (exist _ s' Hs')))).
        -- (* Show that the precondition held *)
          apply sjoin_map_r in H0;
            try (intros (v & Hv) (v' & Hv');
                 unfold unapp_VarLFun_l, unapp_VarLFun_r, compose;
                 intuition).
          apply H0.
        -- (* Show that the command held *)
          (* Move the unapp outside of mark_states. *)
          eapply pe_interp_equiv.
          {
            intro v.
            apply ve_interp_equiv, unapp_mark_states_dist_r.
          }
          (* Get the two commands into roughly the same form. *)
          unfold unapp_MarkedVarLFun_r.
          apply -> ape_state_fmap in H2.
          (* Now we can show that their states are equivalent. *)
          refine (proj1 (pe_interp_equiv gc lc rx.(bv_c) _ _ _) H2).
          intro v.
          apply ve_state_fmap.
  Qed.

  (** Framing is a special case of composition. *)

  Definition frame_BVCond (l l' : list Var)
             (ip : Ep (Ev (AVarL l')))
             (x  : BVCond (Ep (Ev (AVarL l)))
                          (Ep (Ev (AMarkedVarL l)))) :
    BVCond (Ep (Ev (AVarL (l++l'))))
           (Ep (Ev (AMarkedVarL (l++l')))) :=
    combine_BVCond x (mk_BVCond ip (gen_frame l') ip).

  (** Framing inside a [bvhoare] both preserves the original condition,
              and also proves stability of any property [ip] over the newly
              framed-on state. *)

  Corollary framing_preserves_bvhoare (gc : GCtx) (l l' : list Var)
          (ip : Ep (Ev (AVarL l')))
          (x : BVCond (Ep (Ev (AVarL l)))
                      (Ep (Ev (AMarkedVarL l)))) :
    ldisj Heq_dec l l' ->
    bvhoare gc x (LCtx := LCtx)
            (S := PrfIrrFun Var Val (ni l))
            (P_Ep := PredEx_PrfIrr Ep Ev (ni l))
            (P_Er := RelEx_MarkedPredEx l) ->
    bvhoare gc (frame_BVCond ip x) (LCtx := LCtx)
            (S := PrfIrrFun Var Val _)
            (P_Ep := PredEx_PrfIrr Ep Ev (ni _))
            (P_Er := RelEx_MarkedPredEx _).
  Proof.
    intros Hdisj Hx.
    pose (rel_id_preservation ip gc (R_Er := RelEx_MarkedPredEx _)) as Hpres.
    now apply bvhoare_compositionality.
  Qed.
End marked_framing.