(** * Alphabet support: base classes *)

From Coq Require Import
     Bool
     Program.Basics
     Relations
     RelationPairs
     Sets.Constructive_sets
     Sets.Ensembles
     (* These are placed here to override earlier imports *)
     Lists.List
     Classes.SetoidClass
     Classes.SetoidDec
.

From Starling Require Import
     Views.Transformers.Subtype  (* For Setoid on prfirr functions *)
     Views.Transformers.Function (* For Setoid X -> Y *)
     Utils.Ensemble.Facts
     Utils.List.Disjoint
     Utils.List.Facts
     Utils.Monads
     Utils.Option.Facts
     Backend.Classes
     Backend.Alpha.Marked
     Backend.Alpha.PrfIrr
     Backend.Alpha.SJoin
     Backend.Alpha.Var
     Backend.Transformers.Subtype (* For PredEx on prfirr functions *)
.
Set Implicit Arguments.
Set Universe Polymorphism.

Section starling_logic_backend_classes_alphapredex.

  Local Open Scope program_scope.
  Local Open Scope monad_scope.

  Class ValEx@{v l e} (Val : Type@{l}) (Ev : Type@{v} -> Type@{e}) {S_Val : Setoid Val} :=
    mk_ValExp
      {
        (** Value expressions can be interpreted into values, given a state. *)
        ve_interp (Var : Type@{v}) (s : Var -> Val) (x : Ev Var) : Val;

        (** Value expressions are functors over variables... *)
        ve_Functor :> Functor Ev;

        (** ...and monads. *)
        ve_Monad :> Monad Ev;

        (** There must be a way of expressing a 'null' or 'bottom' value. *)
        ve_bot (Var : Type@{v}) : Ev Var;

        (** [ve_bot] must return the same value over all states and
            variable alphabets. *)
        ve_bot_same (Var1 Var2 : Type@{v}) (s1 : Var1 -> Val) (s2 : Var2 -> Val):
          ve_interp s1 (ve_bot Var1) == ve_interp s2 (ve_bot Var2);

        (** [ve_bot] mustn't have any variables. *)
        ve_bot_mbind (A B : Type@{v}) (f : A -> Ev B) :
          f =<< ve_bot A = ve_bot B;

        (** We require a degree of functional extensionality on the monad:
            if two functions [f] and [g] always give equal results, then the
            results of binding them on [Ev] are always equal. *)
        ve_Monad_fun_ext (A B : Type@{v}) (f g : A -> Ev B) (x : Ev A) :
          (forall a : A, f a = g a) -> x >>= f = x >>= g;

        (** Replacing all of the variables in an expression with further
            expressions is equivalent to composing the function onto the
            state, and lifting the state to another round of interpretation.

            This property is perhaps more understandable when seen in its [fmap]
            form below. *)

        ve_state_mbind (A B : Type@{v}) (x : Ev A) (f : A -> Ev B) (s : B -> Val) :
          ve_interp s (f =<< x) == ve_interp (ve_interp s ∘ f) x;

        (** If two states are equivalent, so are the resulting values. *)

        ve_interp_equiv (Var : Type@{v}) (ev : Ev Var) :
          Proper (SetoidClass.equiv ==> SetoidClass.equiv) (flip (ve_interp (Var := Var)) ev);

        (** Interpreting an [mreturn] always gives back a defined value
            equivalent to the original. *)

        ve_interp_mreturn (Var : Type@{v}) (x : Var) (s : Var -> Val) :
          ve_interp s (mreturn x) == s x;
      }.

  Import EqNotations.

  Lemma ve_Monad_fun_ext_rew
        {Val A A' B : Type} {Ev : Type -> Type}
        {S_Val : Setoid Val}
        {V_Ev  : ValEx Ev}
        (f : A -> Ev B) (g : A' -> Ev B)
        (x : Ev A) (x' : Ev A')
        (H : A = A') :
    (x = rew <- H in x') ->
    (forall a : A, f a = g (rew H in a)) ->
    x >>= f = x' >>= g.
  Proof.
    subst.
    cbn.
    intros ->.
    apply ve_Monad_fun_ext.
  Qed.

  Lemma ve_rew_fmap
        {Val A B B' : Type} {Ev : Type -> Type}
        {S_Val : Setoid Val}
        {V_Ev  : ValEx Ev}
        (f : A -> B)
        (x : Ev A)
        (H : B' = B) :
    fmap (rew <- [fun x => A -> x] H in f) x (Functor := ve_Functor (Ev := Ev)) =
    rew <- [Ev] H in (fmap f x (Functor := ve_Functor (Ev := Ev))).
  Proof.
    now subst.
  Qed.

  Lemma ve_rew_fmap'
        {Val A A' B : Type} {Ev : Type -> Type}
        {S_Val : Setoid Val}
        {V_Ev  : ValEx Ev}
        (f : A -> B)
        (x : Ev A)
        (H : A' = A) :
    fmap f (rew <- H in x) (Functor := ve_Functor (Ev := Ev)) =
    fmap f x (Functor := ve_Functor (Ev := Ev)).
  Proof.
    now subst.
  Qed.

  Lemma ve_Functor_fun_ext
        {Val A B : Type} {Ev : Type -> Type}
        {S_Val : Setoid Val}
        {V_Ev  : ValEx Ev}
        (f g : A -> B) (x : Ev A) :
    (forall a : A, f a = g a) -> fmap f x = fmap g x.
  Proof.
    intro Hfe.
    rewrite ! monad_fmap.
    apply ve_Monad_fun_ext.
    intro a.
    unfold compose.
    congruence.
  Qed.

  Lemma ve_state_mbind_flip
        {Val A B : Type} {Ev : Type -> Type}
        {S_Val : Setoid Val}
        {V_Ev  : ValEx Ev}
        (x : Ev A) (f : A -> Ev B) (s : B -> Val) :
    ve_interp s (x >>= f) == ve_interp (ve_interp s ∘ f) x.
  Proof.
    unfold flip.
    apply ve_state_mbind.
  Qed.

  Lemma ve_state_fmap
        {Val A B : Type} {Ev : Type -> Type}
        {S_Val : Setoid Val}
        {V_Ev  : ValEx Ev}
        (x : Ev A) (f : A -> B) (s : B -> Val) :
    ve_interp s (fmap f x) == ve_interp (s ∘ f) x.
  Proof.
    rewrite monad_fmap.
    rewrite (ve_state_mbind x (mreturn ∘ f) s).
    apply ve_interp_equiv.
    unfold compose.
    intro z.
    apply ve_interp_mreturn.
  Qed.

  Lemma ve_bot_mbind_flip
        {Val A B : Type} {Ev : Type -> Type}
        {S_Val : Setoid Val}
        {V_Ev  : ValEx Ev}
        (f : A -> Ev B) :
    ve_bot A >>= f = ve_bot B.
  Proof.
    apply ve_bot_mbind.
  Qed.

  Lemma ve_bot_fmap
        {Val A B : Type} {Ev : Type -> Type}
        {S_Val : Setoid Val}
        {V_Ev  : ValEx Ev}
        (f : A -> B) :
    fmap f (ve_bot A) (Functor := ve_Functor (Ev := Ev)) = ve_bot B.
  Proof.
    rewrite monad_fmap.
    unfold compose.
    apply ve_bot_mbind.
  Qed.

  Class EqPredEx@{c v l e} (Val : Type@{l}) (GCtx LCtx : Type@{c}) (Ep : Type@{v} -> Type@{e})
        {S_Val : Setoid Val} :=
    mk_EqPredEx
      {
        (** Alphabetised predicate expressions are predicate expressions over
            state functions with the same domain. *)

        ape_PredEx (EVal : Type@{v}) :> PredEx (Ep EVal) GCtx LCtx (EVal -> Val);

        ape_ImplPredEx (EVal : Type@{v}) :> ImplPredEx (Ep EVal) GCtx LCtx (EVal -> Val);

        (** We can compare two values. *)

        ape_eq (EVal : Type@{v}) (x y : EVal) : Ep EVal;

        (** [ape_eq] holds iff the two value expressions return equivalent
            (and defined!) values in the state. *)

        ape_eq_val_eq (A : Type@{v}) (x y : A) (gc : GCtx) (lc : LCtx) (s : A -> Val) :
          Ensembles.In _ (pe_interp gc lc (ape_eq x y)) s <->
          s x == s y;

        (** We can map over all of the value expressions.
            We don't necessarily have a monad. *)

        ape_Functor_Ep :> Functor Ep;

        (** Mapping over all of the value expressions gives the same result as
            composing the function onto the state. *)

        ape_state_fmap (A B : Type@{v}) (x : Ep A) (f : A -> B) (gc : GCtx) (lc : LCtx) (s : B -> Val) :
          Ensembles.In _ (pe_interp gc lc (fmap f x)) s <->
          Ensembles.In _ (pe_interp gc lc x         ) (s ∘ f);

        (*
         * Distribution of [fmap]
         *)

        (** [fmap] doesn't affect [pe_true]. *)

        ape_fmap_true (A B : Type@{v}) (f : A -> B) :
          (fmap f (pe_true (PredEx := ape_PredEx A))) ==
          (pe_true (PredEx := ape_PredEx B));

        (** [fmap] doesn't affect [pe_false]. *)

        ape_fmap_false (A B : Type@{v}) (f : A -> B) :
         (fmap f (pe_false (ImplPredEx := ape_ImplPredEx A))) ==
         (pe_false (ImplPredEx := ape_ImplPredEx B));

        (** Mapping distributes over [pe_conj]. *)

        ape_fmap_conj (A B : Type@{v}) (x y : Ep A) (f : A -> B) :
          fmap f (pe_conj x y (PredEx := ape_PredEx A)) ==
          (pe_conj (fmap f x) (fmap f y) (PredEx := ape_PredEx B));

        (** Mapping distributes over [pe_impl]. *)

        ape_fmap_impl (A B : Type@{v}) (x y : Ep A) (f : A -> B) :
          fmap f (pe_impl x y (ImplPredEx := ape_ImplPredEx A)) ==
          (pe_impl (fmap f x) (fmap f y) (ImplPredEx := ape_ImplPredEx B));

        (** [fmap] must distribute across [ape_eq]. *)

        ape_fmap_eq (A B : Type@{v}) (x y : A) (f : A -> B) :
          fmap f (ape_eq x y) == ape_eq (f x) (f y);
      }.

  Section ValEx_setoid.
    (** ** [Setoid] instance for all [ValEx]

        Value expressions can be compared equivalent.  Two value expressions
        are equivalent provided that they return the same value for all
        states. *)

    Polymorphic Universe v.
    Polymorphic Universe l.
    Polymorphic Universe e.

    Variables
      (Val : Type@{l})
      (Ev  : Type@{v} -> Type@{e})
    .

    Context
      {S_Val : Setoid Val}
      {V_Ev : ValEx Ev (Val := Val)}
    .

    Definition equiv_by_ve_interp (Var : Type@{v}) (v1 v2 : Ev Var) :=
      forall (s : Var -> Val),
        ve_interp s v1 (ValEx := V_Ev) == ve_interp s v2 (ValEx := V_Ev).

    Global Instance equiv_by_ve_interp_Equivalence {Var : Type@{v}} :
      Equivalence (equiv_by_ve_interp (Var := Var)) :=
      {}.
    Proof.
      - easy.
      - easy.
      - intros x y z Heqv_xy Heqv_yz s.
        now transitivity (ve_interp s y).
    Defined.

    Global Instance ValEx_Setoid {Var : Type@{v}} : Setoid (Ev Var) :=
      {|
        equiv := equiv_by_ve_interp (Var := Var);
        setoid_equiv := equiv_by_ve_interp_Equivalence;
      |}.
  End ValEx_setoid.

  Section expressible_values.

    (** ** Expressible values

        An _expressible value_ is any value that we can represent as a
        value expression with no variables.

        For simplicity, we model expressible values as just that:
        variable-less [ValEx]es, or [Ev Empty_set] for some [Ev]. *)

    Polymorphic Universe v.
    Polymorphic Universe l.
    Polymorphic Universe e.

    Variables
      (Val : Type@{l})
      (Ev  : Type@{v} -> Type@{e})
    .

    Context
      {S_Val : Setoid Val}
      {V_Ev : ValEx Ev (Val := Val)}
    .

    Section elim_empty.
      Variable T : Type.
      Context {ST : Setoid T}.

      Definition elim_empty (x : Datatypes.Empty_set) : T :=
      match x with end.

      (** Every possible function from [Empty_set] is vacuously equivalent to
          [elim_empty], so long as the domain is a setoid. *)

      Lemma elim_empty_equiv (s : Datatypes.Empty_set -> T) :
        s == elim_empty.
      Proof.
        intros [].
      Qed.
    End elim_empty.

    (** As a result, every [ve_interp] over a [Empty_set] function is
          equivalent to a [ve_interp] over [elim_empty]. *)

    Lemma elim_empty_ve_interp_equiv
          (s : Datatypes.Empty_set -> Val) (e : Ev (Datatypes.Empty_set)) :
      ve_interp s e == ve_interp (elim_empty Val) e.
    Proof.
      apply ve_interp_equiv, elim_empty_equiv.
    Qed.

    (** This gives us a slightly simpler (and
        decidability-preserving!)  definition for [EVal]
        equivalence. *)

    Local Open Scope signature_scope.

    Definition eval_equiv : relation (Ev Datatypes.Empty_set) :=
      equiv @@ (ve_interp (elim_empty Val)).

    Lemma eval_equiv_req :
      relation_equivalence eval_equiv equiv.
    Proof.
      intros e1 e2.
      split; intros Heval.
      - intro s.
        now setoid_rewrite elim_empty_ve_interp_equiv.
      - apply Heval.
    Qed.

    Lemma eval_equiv_dec {D_Val : EqDec S_Val} (x y : Ev Datatypes.Empty_set) :
      { x == y } + { x =/= y }.
    Proof.
      destruct (ve_interp (elim_empty Val) x == ve_interp (elim_empty Val) y).
      - apply eval_equiv_req in e.
        now left.
      - right.
        intros Hequiv.
        apply c, eval_equiv_req, Hequiv.
    Qed.

    Global Instance EVal_EqDec {D_Val : EqDec S_Val} :
      EqDec (ValEx_Setoid (Var := Datatypes.Empty_set)) :=
      {
        equiv_dec := eval_equiv_dec;
      }.

    (** We can always inflate an [Ev Empty_set] to any [Ev X] by
        mapping all variables into proofs that they can't possibly
        exist. *)

    Definition eval_to_valex (T : Type) : Ev Datatypes.Empty_set -> Ev T :=
      fmap (elim_empty T).

    (** We can also lift any state returning expressible values to one returning
        normal values. *)

    Definition eval_lift (T : Type) (s : T -> Ev Datatypes.Empty_set) : T -> Val :=
      ve_interp (elim_empty Val) ∘ s.

    Lemma eval_lift_to_valex (T : Type)
          (x : Ev Datatypes.Empty_set)
          (s : T -> Val) :
      ve_interp s (eval_to_valex T x) == ve_interp (s ∘ elim_empty T) x.
    Proof.
      apply ve_state_fmap.
    Qed.

    (** Equivalence distributes across [eval_lift]... *)
    Global Instance eval_lift_Proper (T : Type) :
      Proper (SetoidClass.equiv ==> SetoidClass.equiv)
             (eval_lift (T := T)).
    Proof.
      intros f g Hfuns x.
      specialize (Hfuns x).
      cbn in Hfuns.
      now unfold eval_lift, compose.
    Defined.

    (** ...and [eval_lift] is an injection modulo it. *)
    Lemma eval_lift_equiv_inj (T : Type) (f g : T -> Ev Datatypes.Empty_set) :
      eval_lift (T := T) f == eval_lift (T := T) g -> f == g.
    Proof.
      intros Hfuns x.
      specialize (Hfuns x).
      cbn in *.
      unfold eval_lift, compose in Hfuns.
      now apply eval_equiv_req.
    Defined.

    Lemma eval_fmap_elim (T : Type)
          (x : Ev Datatypes.Empty_set)
          (f : Datatypes.Empty_set -> T) :
      fmap f x = eval_to_valex T x.
    Proof.
      unfold eval_to_valex.
      now apply ve_Functor_fun_ext.
    Qed.

    Corollary eval_to_valex_id
          (x : Ev Datatypes.Empty_set) :
      x = eval_to_valex (Datatypes.Empty_set) x.
    Proof.
      replace x with (fmap id x) at 1 by apply fmap_identity.
      apply eval_fmap_elim.
    Qed.

    Definition EVal := Ev Datatypes.Empty_set.

    Section empty_lists.
      Variable Var : Type@{v}.

      Lemma empty_list_false (v : AVarL (nil : list Var)) : False.
      Proof.
        now destruct v.
      Qed.

      Lemma marked_empty_list_false (v : AMarkedVarL (nil : list Var)) : False.
      Proof.
        now destruct v.
      Qed.

      (** We can convert an expression over an empty variable list to an
        expressible value by asserting false if we meet a variable. *)

      Definition remove_empty_list
                 (v : Ev (AVarL (nil : list Var))) : EVal :=
        fmap (fun var => False_rec _ (empty_list_false var)) v.

      Definition remove_marked_empty_list
                 (v : Ev (AMarkedVarL (nil : list Var))) : EVal :=
        fmap (fun var => False_rec _ (marked_empty_list_false var)) v.

      Global Instance Setoid_EVal : Setoid EVal := ValEx_Setoid (S_Val := S_Val).
    End empty_lists.
  End expressible_values.


  Section EqPredEx_facts.
    Polymorphic Universe vp.
    Polymorphic Universe vv.
    Polymorphic Universe l.
    Polymorphic Universe ep.
    Polymorphic Universe ev.
    Polymorphic Universe lc.
    Polymorphic Universe gc.

    Variables
      (Val : Type@{l})
      (Ep  : Type@{vp} -> Type@{ep})
      (Ev  : Type@{vv} -> Type@{ev})
      (LCtx : Type@{lc})
      (GCtx : Type@{gc})
    .

    Context
      {S_Val : Setoid Val}
      {E_Ep : EqPredEx GCtx LCtx Ep (Val := Val)}
      {V_Ev : ValEx Ev (Val := Val)}.


    (** Two variables return equivalent values iff their projections to value
            expressions return equivalent values. *)

    Lemma ve_eq_eval_var (Var : Type@{vv}) (s s' : Var -> Val) (x y : Var) :
      s x == s' y <-> ve_interp s (mreturn x) == ve_interp s' (mreturn y).
    Proof.
      now rewrite ! ve_interp_mreturn.
    Qed.

    (** If an [EqPredEx] is parametrised over value expressions,
        we can map over all of the variables by chaining [fmap]s. *)

    Definition map_vars {A B : Type@{vv}} (f : A -> B) : Ep (Ev A) -> Ep (Ev B) :=
      fmap (fmap f).

    Local Open Scope monad_scope.

  End EqPredEx_facts.
End starling_logic_backend_classes_alphapredex.

Hint Rewrite
     @ve_bot_mbind
     @ve_state_mbind
     @ve_interp_mreturn
     @ve_state_fmap
  : valex.