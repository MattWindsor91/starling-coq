(** * Alphabet support: state joining *)

From Coq Require Import
     Bool
     Classes.SetoidClass
     Lists.List
     Program.Basics
     Program.Utils
.

From Starling Require Import
     Utils.List.Disjoint
     Utils.List.Facts
     Views.Transformers.Function
     Backend.Alpha.Var.

Set Implicit Arguments.

Section sjoin_def.

  Variable Var : Type.

  Hypothesis Heq_dec : forall (x y : Var), {x = y} + {x <> y}.

  (** Sumbool version of [inb], used to make [sjoin] less painful to work with. *)

  Definition inp (x : Var) (xs : list Var) : { inb Heq_dec x xs = true } + { inb Heq_dec x xs = false }.
  Proof.
    case_eq (inb Heq_dec x xs); tauto.
  Defined.

  Lemma in_but_not_in (a1 a2 : list Var) (v : AVarL (a1 ++ a2)) :
    inb Heq_dec (`v) a1 = false ->
    inb Heq_dec (`v) a2 = false ->
    False.
  Proof.
    intros Hnot_in_a1 Hnot_in_a2.
    destruct v as (v & Hv).
    destruct (in_app_or _ _ _ Hv) as
        [H%(inb_In Heq_dec)%Is_true_eq_true|H%(inb_In Heq_dec)%Is_true_eq_true];
      eapply eq_true_false_abs; eauto.
  Qed.

  (** Two states agree if, for all variables in both states' domains, both
      states provide precisely the same result on that variable. *)

  Definition states_agree (Val : Type) {S_Val : Setoid Val} (a1 a2: list Var)
             (s1 : AVarL a1 -> Val)
             (s2 : AVarL a2 -> Val) :=
    forall (v : Var) (Hin_a1 : In v a1) (Hin_a2 : In v a2),
      s1 (exist _ v Hin_a1) == s2 (exist _ v Hin_a2).

  (** State agreement is reflexive if the state has proof irrelevance. *)

  Lemma states_agree_refl (Val : Type) {S_Val : Setoid Val} (a : list Var) (s : AVarL a -> Val) :
    (forall (v1 v2 : AVarL a), `v1 = `v2 -> s v1 == s v2) ->
    states_agree s s.
  Proof.
    intros Hpi v Hin_a1 Hin_a2.
    auto.
  Qed.

  (** State disjointness implies state agreement. *)

  Lemma states_agree_disj (Val : Type) {S_Val : Setoid Val}
        (a1 a2 : list Var)
        (s1 : AVarL a1 -> Val)
        (s2 : AVarL a2 -> Val) :
    ldisj Heq_dec a1 a2 -> states_agree s1 s2 (S_Val := S_Val).
  Proof.
    intros Hdisj v Hin_a1 Hin_a2.
    contradiction (ldisj_not_both Heq_dec a1 a2 v Hdisj).
    - now apply (inb_In Heq_dec), Is_true_eq_true in Hin_a1.
    - now apply (inb_In Heq_dec), Is_true_eq_true in Hin_a2.
  Qed.

(*
  (** Joining two states with disjoint domains and possibly different ranges *)

  Definition sjoin_sum (Val1 Val2 : Type) (a1 a2: list Var)
             (Hdisj: ldisj Heq_dec a1 a2)
             (s1 : AVarL a1 -> Val1)
             (s2 : AVarL a2 -> Val2)
             (v : AVarL (a1 ++ a2)) :
    Val1 + Val2 :=
    match inp (`v) a1, inp (`v) a2 with
    | left Hin_a1, left Hin_a2 =>
      (* Contradiction: can't be in both! *)
      match ldisj_not_both Heq_dec a1 a2 (`v) Hdisj Hin_a1 Hin_a2 with end
    | left Hin_a1, right _ =>
      (* In the first state. *)
      inl (s1 (exist _ (`v) (proj2 (inb_In Heq_dec _ _) (Is_true_eq_left _ Hin_a1))))
    | right _, left Hin_a2 =>
      (* In the second state. *)
      inr (s2 (exist _ (`v) (proj2 (inb_In Heq_dec _ _) (Is_true_eq_left _ Hin_a2))))
    | right Hnot_in_a1, right Hnot_in_a2 =>
      (* Contradiction: can't be in neither *)
      match (in_but_not_in a1 a2 v Hnot_in_a1 Hnot_in_a2) with end
    end.
*)
  (** Joining two states with possibly overlapping domains but the same range *)

  Definition sjoin_agree (Val: Type) {S_Val : Setoid Val} (a1 a2: list Var)
             (s1 : AVarL a1 -> Val)
             (s2 : AVarL a2 -> Val)
             (Hagree: states_agree s1 s2 (S_Val := S_Val))
             (v : AVarL (a1 ++ a2)) : Val :=
    match inp (`v) a1, inp (`v) a2 with
    | left Hin_a1, _ =>
      (* If both provide the value, then the state agreement means we
         can arbitrarily take the first one. *)
      s1 (exist _ (`v) (proj2 (inb_In Heq_dec _ _) (Is_true_eq_left _ Hin_a1)))
    | right _, left Hin_a2 =>
      s2 (exist _ (`v) (proj2 (inb_In Heq_dec _ _) (Is_true_eq_left _ Hin_a2)))
    | right Hnot_in_a1, right Hnot_in_a2 =>
      (* Contradiction: can't be in neither *)
      match (in_but_not_in a1 a2 v Hnot_in_a1 Hnot_in_a2) with end
    end.


  (** Joining two states with disjoint domains *)

  Definition sjoin (Val: Type) (a1 a2: list Var)
             (Hdisj: ldisj Heq_dec a1 a2)
             (s1 : AVarL a1 -> Val)
             (s2 : AVarL a2 -> Val)
             (v : AVarL (a1 ++ a2)) :
    Val :=
    sjoin_agree (states_agree_disj s1 s2 Hdisj) v.
End sjoin_def.

Ltac elim_sjoin_contradiction v :=
  match goal with
  | [ Hin : In v ?a, H : inb ?hdec v ?a = false |- _ ] =>
    (* In (a), but our destruction claims otherwise. *)
    pose (proj1 (inb_In hdec v a) Hin) as Hbang;
    apply Is_true_eq_true in Hbang;
    now rewrite H in Hbang
  | [ Hdisj : ldisj ?Heq_dec ?a1 ?a2, H1 : inb ?Heq_dec v ?a1 = true, H2 : inb ?Heq_dec v ?a2 = true |- _ ] =>
    destruct (ldisj_not_both Heq_dec a1 a2 v Hdisj H1 H2)
  | [ |- context [ in_but_not_in ?a1 ?a2 ?v ?H ?H0 ] ] =>
    destruct (in_but_not_in a1 a2 v H H0)
  | [ H : inb _ v ?a = true, H' : inb _ v ?a = false |- _ ] =>
    destruct (eq_true_false_abs _ H H')
  end.

Ltac reduce_sjoin'_inner v a1 a2 H :=
  unfold unapp_VarLFun_l, unapp_VarLFun_r, compose, sjoin, sjoin_agree;
  cbn;
  let H1 := fresh "Hin_a1" in
  let H2 := fresh "Hin_a2" in
  destruct (inp H v a1) as [H1|H1], (inp H v a2) as [H2|H2];
  intuition;
  repeat (elim_sjoin_contradiction v).

Ltac reduce_sjoin' v a1 a2 :=
  match type of v with
  | ?vt =>
    match goal with
    | [ H: forall x y : vt, { x = y } + { x <> y } |- _ ] =>
      reduce_sjoin'_inner v a1 a2 H
    | [ H: forall x y : vt, { x = y } + { x = y -> False } |- _ ] =>
      reduce_sjoin'_inner v a1 a2 H
    | _ => fail "Need decidable equality for variables"
    end
  end.

Ltac reduce_sjoin_inner v :=
  match type of v with
  | AVarL (?a1 ++ ?a2) =>
    let v' := fresh "v" in
    let Hv := fresh "Hv" in
    destruct v as (v' & Hv);
    reduce_sjoin' v' a1 a2
  | {x | In x (?a1 ++ ?a2)} =>
    let v' := fresh "v" in
    let Hv := fresh "Hv" in
    destruct v as (v' & Hv);
    reduce_sjoin' v' a1 a2
  | AVarL _ => fail "Variable must have type [AVarL (x ++ y)] for some [x] and [y];"
                   "try [reduce_sjoin']"
  | {x | In x _} =>
    fail "Variable must have type [{x | In x (xs ++ ys)] for some [xs] and [ys];"
         "try [reduce_sjoin']"

  end.

Tactic Notation "reduce_sjoin" := let v := fresh in (intro v; reduce_sjoin_inner v).
Tactic Notation "reduce_sjoin" constr(v) := reduce_sjoin_inner v.

Section sjoin_facts.
  Variables
    Var
    Val : Type.

  Variables
    a1 a2 : list Var. (* Alphabets *)

  Hypothesis
    Heq_dec : forall v v' : Var, {v = v'} + {v <> v'}.

  Hypothesis
    Hdisj : ldisj Heq_dec a1 a2.

  Context
    {S_Val : Setoid Val}.

  Definition AVarL_flip_app (v : AVarL (a1++a2)) : AVarL (a2++a1).
  Proof.
    destruct v as (v & Hv%in_app_or).
    exists v.
    apply in_or_app.
    tauto.
  Defined.

  (** The order in which we present the variables doesn't matter. *)

  Lemma sjoin_comm (s1 : AVarL a1 -> Val) (s2 : AVarL a2 -> Val) :
    sjoin Heq_dec Hdisj s1 s2 ==
    sjoin Heq_dec (ldisj_comm _ _ _ Hdisj) s2 s1 ∘ AVarL_flip_app.
  Proof.
    unfold compose.
    intro v.
    destruct (AVarL_flip_app v) as (v' & Hv') eqn:Hflip.
    unfold AVarL_flip_app in Hflip.
    destruct v as (v & Hv).
    injection Hflip; intros <-.
    reduce_sjoin' v a1 a2.
  Qed.

  (** If we join two state functions then discard the domain of one of
      them, we effectively end up with the original function. *)

  Lemma sjoin_l (s1 : AVarL a1 -> Val) (s2 : AVarL a2 -> Val)
        (Hprf_irr : forall v v', `v = `v' -> s1 v == s1 v') :
    s1 == (unapp_VarLFun_l a1 a2 (sjoin Heq_dec Hdisj s1 s2)).
  Proof.
    intros (v & Hv).
    reduce_sjoin' v a1 a2.
  Qed.

  Lemma sjoin_r (s1 : AVarL a1 -> Val) (s2 : AVarL a2 -> Val)
        (Hprf_irr : forall v v', `v = `v' -> s2 v == s2 v') :
    s2 == (unapp_VarLFun_r a1 a2 (sjoin Heq_dec Hdisj s1 s2)).
  Proof.
    intros (v & Hv).
    reduce_sjoin' v a1 a2.
  Qed.

  (** If a state function [s] doesn't care about the proofs of alphabet
        membership, _and_ its alphabet can be split into two disjoint
        sub-alphabets, we can model [s] as the [sjoin] of restricted
        domain versions of [s]. *)

  Lemma state_split (s : AVarL (a1 ++ a2) -> Val)
        (Hprf_irr : forall v v' : AVarL (a1 ++ a2), `v = `v' -> s v == s v') :
    s == sjoin Heq_dec
               Hdisj
               (unapp_VarLFun_l a1 a2 s)
               (unapp_VarLFun_r a1 a2 s).
  Proof.
    reduce_sjoin.
  Qed.

  Global Instance sjoin_equiv_Proper :
    Proper (equiv ==> equiv ==> equiv) (sjoin Heq_dec Hdisj).
  Proof.
    intros x x' Hxe y y' He'.
    reduce_sjoin.
  Qed.

  (** The inverse of [sjoin_equiv_Proper]: if two [sjoin]s are equivalent,
      the respective pairs of original states are. *)

  Lemma sjoin_equiv_reduce (l l' : AVarL a1 -> Val) (r r' : AVarL a2 -> Val) :
    (forall v v', `v = `v' -> l  v == l  v') ->
    (forall v v', `v = `v' -> l' v == l' v') ->
    (forall v v', `v = `v' -> r  v == r  v') ->
    (forall v v', `v = `v' -> r' v == r' v') ->
    sjoin Heq_dec Hdisj l r == sjoin Heq_dec Hdisj l' r' ->
    l == l' /\ r == r'.
  Proof.
    intros Hprf_irr_l Hprf_irr_l' Hprf_irr_r Hprf_irr_r' Hsjoin.
    split;
      intros (v & Hv);
      pose (proj1 (inb_In Heq_dec _ _) Hv) as Hv';
      apply Is_true_eq_true in Hv'.
    - specialize (Hsjoin (exist _ v (in_or_app a1 a2 v (or_introl Hv)))).
      unfold sjoin, sjoin_agree in Hsjoin.
      cbn in Hsjoin.
      destruct (inp Heq_dec v a1) as [Ha1|Hna1], (inp Heq_dec v a2) as [Ha2|Hna2];
        intuition;
        try (now rewrite Hna1 in Hv').
      + now destruct (ldisj_not_both Heq_dec a1 a2 v Hdisj Ha1 Ha2).
      + rewrite Hprf_irr_l, Hprf_irr_l'; eauto.
    - specialize (Hsjoin (exist _ v (in_or_app a1 a2 v (or_intror Hv)))).
      unfold sjoin, sjoin_agree in Hsjoin.
      cbn in Hsjoin.
      destruct (inp Heq_dec v a1) as [Ha1|Hna1], (inp Heq_dec v a2) as [Ha2|Hna2];
        intuition;
        try (now rewrite Hna2 in Hv').
      + now destruct (ldisj_not_both Heq_dec a1 a2 v Hdisj Ha1 Ha2).
      + rewrite Hprf_irr_r, Hprf_irr_r'; eauto.
  Qed.
End sjoin_facts.