(** * Alphabet support: dependently typed variable domains

    We use lists as a model for variable alphabets because:

    - Ensembles and unisets aren't finite (and we need finite sets
      to be able to frame over them!
    - Finite sets are quite tricky to work with (modules!!), and it's
      much easier to get lists working.

      It may be worth porting all of this to [MSet]s later on. *)

From Coq Require Import
     Program.Basics
     Lists.List.

(** We use a notation instead of a definition to allow for universe
    polymorphism. *)

Notation AVarL := (fun l => { x | In x l }).

Section variable_lists.

  Context { Var : Type }.

  Import ListNotations.
  Local Open Scope program_scope.

  Definition lift_AVarL (x : Var) : AVarL [x].
  Proof.
    exists x.
    apply in_eq.
  Defined.
  
  (** We can always widen a variable list. *)

  Definition AVarL_cons (x : Var) (xs : list Var) (v : AVarL xs) : AVarL (x::xs) :=
    exist _ (proj1_sig v) (in_cons _ _ xs (proj2_sig v)).
  
  Definition AVarL_app_l (xs ys : list Var) (v : AVarL xs) : AVarL (xs++ys) :=
    exist _ (proj1_sig v) (in_or_app xs ys _ (or_introl (proj2_sig v))).

  Definition AVarL_app_r (xs ys : list Var) (v : AVarL ys) : AVarL (xs++ys) :=
    exist _ (proj1_sig v) (in_or_app xs ys _ (or_intror (proj2_sig v))).    
  
  Section remove_domain.
    Context { Val : Type }.
    
    (** We can contravariantly _remove_ variables from the domain of a
        state function. *)

    Definition uncons_VarLFun (x : Var) (xs : list Var) (f : AVarL (x :: xs) -> Val)
      : AVarL xs -> Val :=
      f ∘ (AVarL_cons x xs).

    Definition unapp_VarLFun_l (xs ys : list Var) (f : AVarL (xs ++ ys) -> Val)
      : AVarL xs -> Val :=
      f ∘ (AVarL_app_l xs ys).      

    Definition unapp_VarLFun_r (xs ys : list Var) (f : AVarL (xs ++ ys) -> Val)
      : AVarL ys -> Val :=
      f ∘ (AVarL_app_r xs ys).
  End remove_domain.

End variable_lists.
