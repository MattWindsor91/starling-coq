(** * Alphabet support: base classes *)

From Coq Require Import
     Bool
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles
     (* These are placed here to override earlier imports *)
     Lists.List
     Classes.SetoidClass.

From Starling Require Import
     Views.Transformers.Subtype  (* For Setoid on prfirr functions *)
     Views.Transformers.Function (* For Setoid X -> Y *)
     Utils.Ensemble.Facts
     Utils.List.Disjoint     
     Utils.List.Facts
     Utils.Monads
     Utils.Option.Facts
     Backend.Classes
     Backend.Alpha.Classes     
     Backend.Alpha.Marked
     Backend.Alpha.PrfIrr     
     Backend.Alpha.SJoin
     Backend.Alpha.Var
     Backend.Transformers.Subtype (* For PredEx on prfirr functions *)
.

Set Implicit Arguments.

(* Space for rent *)