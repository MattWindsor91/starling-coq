(** * Alphabet support: proof-irrelevant state functions *)

From Coq Require Import
     Classes.SetoidClass
     Program.Basics
     Program.Utils
     Lists.List.

From Starling Require Import
     Utils.List.Disjoint
     Utils.List.Facts
     Views.Transformers.Function
     Views.Transformers.Subtype
     Backend.Alpha.Marked
     Backend.Alpha.SJoin
     Backend.Alpha.Var.

(** A function over a sigma type that doesn't care what the proof inside
    the sigma type is.

    This is [Inductive] to allow for limited universe polymorphism. *)

Inductive PrfIrrFun (D R : Type) {S_R : Setoid R} (P : D -> Prop) : Type :=
| Pfun
  (f : { s : { x : D | P x } -> R | forall v v', `v = `v' -> s v == s v' }).

Definition extract_pfun {D R : Type} {S_R : Setoid R} {P : D -> Prop}
           '(Pfun _ _ _ f) : {x : D | P x} -> R := `f.

(** We can compose certain types of function onto the end of a [PrfIrrFun]. *)

Definition compose_pfun {D I R : Type} {S_I : Setoid I} {S_R : Setoid R} {P : D -> Prop}
           (f : PrfIrrFun D I P)
           (g : I -> R)
           {g_Proper : Proper (equiv ==> equiv) g} :
  PrfIrrFun D R P.
Proof.
  refine (Pfun _ _ _ (exist _ (g ∘ extract_pfun f) _)).
  intros v v' Heq_v.
  apply g_Proper.
  destruct f as (f).
  apply (proj2_sig f), Heq_v.
Defined.

Lemma compose_pfun_equiv {D I R : Type} {S_I : Setoid I} {S_R : Setoid R} {P : D -> Prop}
      (f : PrfIrrFun D I P)
      (g : I -> R)
      {g_Proper : Proper (equiv ==> equiv) g}
      (v : { x : D | P x}) :
  extract_pfun (compose_pfun f g) v == g (extract_pfun f v).
Proof.
  now cbn.
Qed.

Global Instance PrfIrrFun_Setoid
       {D R : Type} {S_D : Setoid R} {P : D -> Prop} :
  Setoid (PrfIrrFun D R P) :=
  {
    equiv '(Pfun _ _ _ f) '(Pfun _ _ _ g) := equiv f g;
  }.
Proof.
  split.
  - now intros (f).
  - now intros (f) (g).
  - intros (f) (g) (h) Hfg Hgh.
    now transitivity g.
Defined.

Global Instance PrfIrrFun_extract_Proper
       {D R : Type} {S_D : Setoid R} {P : D -> Prop} :
  Proper (equiv ==> equiv) (extract_pfun (P := P)).
Proof.
  now intros ((f1 & Hf1)) ((f2 & Hf2)) Hf_equiv.
Defined.

Section alpha_proof_irrelevant_states.

  Variable Val : Type.

  Context { Var : Type }
          { S_Val : Setoid Val }.

  Local Open Scope program_scope.

  (** A [PrfIrrFun] from [AVarL]s. *)

  Notation PrfIrrVarLFun :=
    (fun l => PrfIrrFun Var Val (ni l)).

  (** A [PrfIrrFun] from [AMarkedVarL]s. *)

  Notation PrfIrrMarkedVarLFun :=
    (fun l => PrfIrrFun (Marked _) _ (ni l ∘ (@m_var _))).

  Section remove_domain.

    (** We can contravariantly remove a variable from the domain of a
        [PrfIrrVarLFun]. *)

    Definition uncons_PrfIrrVarLFun (v : Var) (vs : list Var)
    : PrfIrrVarLFun (v :: vs) -> PrfIrrVarLFun vs.
    Proof.
      inversion 1 as [(f & Hf)].
      constructor.
      exists (uncons_VarLFun _ _ f).
      eauto.
    Defined.

    Definition unapp_PrfIrrVarLFun_l (xs ys : list Var)
      : PrfIrrVarLFun (xs ++ ys) -> PrfIrrVarLFun xs.
    Proof.
      inversion 1 as [(f & Hf)].
      constructor.
      exists (unapp_VarLFun_l _ _ f).
      eauto.
    Defined.

    Definition unapp_PrfIrrVarLFun_r (xs ys : list Var)
      : PrfIrrVarLFun (xs ++ ys) -> PrfIrrVarLFun ys.
    Proof.
      inversion 1 as [(f & Hf)].
      constructor.
      exists (unapp_VarLFun_r _ _ f).
      eauto.
    Defined.

  End remove_domain.

  (** We can convert a pair of proof-irrelevant unmarked state
        functions to a single proof-irrelevant marked state function. *)

  Definition mark_states_prfirr {l : list Var} (s1 s2 : PrfIrrVarLFun l)
    : PrfIrrMarkedVarLFun l.
  Proof.
    inversion s1 as [(s1' & Hs1)].
    inversion s2 as [(s2' & Hs2)].
    constructor.
    exists (mark_states _ s1' s2').
    intros ((m & v) & Hmv) ((m' & v') & Hmv').
    injection 1.
    intros <- <-.
    cbn in *.
    destruct m; intuition.
  Defined.

  Global Instance mark_states_prfirr_Proper {l : list Var} :
    Proper (equiv ==> equiv ==> equiv) (mark_states_prfirr (l := l)).
  Proof.
    intros ((s1a & Hs1a)) ((s1b & Hs1b)) Hs1 ((s2a & Hs2a)) ((s2b & Hs2b)) Hs2.
    now apply mark_states_Proper.
  Defined.

  Section sjoin.

    Context {a1 a2 : list Var}.

    Hypothesis Heq_dec : forall x y : Var, { x = y } + { x <> y }.

    Hypothesis Hdisj : ldisj Heq_dec a1 a2.

    Lemma sjoin_agree_prf_irr_dist
          (s1 : AVarL a1 -> Val)
          (s2 : AVarL a2 -> Val)
          (Hpi1 : forall v v', `v = `v' -> s1 v == s1 v')
          (Hpi2 : forall v v', `v = `v' -> s2 v == s2 v')
          (Hagr : states_agree s1 s2) :
       (forall v v', `v = `v' -> sjoin_agree Heq_dec Hagr v == sjoin_agree Heq_dec Hagr v').
    Proof.
      intros (v & Hv) (v' & Hv') Hvv.
      cbn in *.
      subst.
      unfold sjoin_agree.
      cbn.
      destruct (inp Heq_dec v' a1), (inp Heq_dec v' a2); intuition.
      destruct (in_but_not_in Heq_dec a1 a2 (exist _ v' Hv) e e0).
    Qed.

    Corollary sjoin_prf_irr_dist
          (s1 : AVarL a1 -> Val)
          (s2 : AVarL a2 -> Val) :
       (forall v v', `v = `v' -> s1 v == s1 v') ->
       (forall v v', `v = `v' -> s2 v == s2 v') ->
       (forall v v', `v = `v' -> sjoin Heq_dec Hdisj s1 s2 v == sjoin Heq_dec Hdisj s1 s2 v').
    Proof.
      intros H1 H2.
      pose (states_agree_disj Heq_dec s1 s2 Hdisj) as Hagr.
      now apply sjoin_agree_prf_irr_dist with (Hagr := Hagr).
    Qed.

    (** A proof-irrelevant version of [states_agree]. *)
    Definition states_agree_PrfIrr
               (s1 : PrfIrrVarLFun a1) (s2 : PrfIrrVarLFun a2) : Prop :=
      states_agree (extract_pfun s1) (extract_pfun s2).

    (** A proof-irrelevant version of [sjoin_agree]. *)

    Program Definition sjoin_agree_PrfIrr
            (s1 : PrfIrrVarLFun a1)
            (s2 : PrfIrrVarLFun a2)
            (Hagr : states_agree_PrfIrr s1 s2) :=
      Pfun _ _ _ (sjoin_agree Heq_dec Hagr).
    Next Obligation.
      destruct s1 as ((s1 & Hs1)), s2 as ((s2 & Hs2)); cbn.
      now apply sjoin_agree_prf_irr_dist.
    Defined.

    (** A proof-irrelevant version of [sjoin]. *)

    Program Definition sjoin_PrfIrr
            (s1 : PrfIrrVarLFun a1)
            (s2 : PrfIrrVarLFun a2) :
      PrfIrrVarLFun (a1 ++ a2) :=
      Pfun _ _ _ (sjoin Heq_dec Hdisj (extract_pfun s1) (extract_pfun s2)).
    Next Obligation.
      destruct s1 as ((s1 & Hs1)), s2 as ((s2 & Hs2)); cbn.
      now apply sjoin_prf_irr_dist.
    Defined.

    (** Like [sjoin_equiv_Proper], but for [sjoin_PrfIrr]. *)

    Global Instance sjoin_PrfIrr_equiv_Proper :
      Proper (equiv ==> equiv ==> equiv) sjoin_PrfIrr.
    Proof.
      intros x1 x2 Hxe y1 y2 Hye.
      destruct x1 as [(x1' & Hx1)],
               x2 as [(x2' & Hx2)],
               y1 as [(y1' & Hy1)],
               y2 as [(y2' & Hy2)].
      now apply sjoin_equiv_Proper.
    Defined.

    (** Like [sjoin_equiv_reduce], but for [sjoin_PrfIrr]. *)

    Lemma sjoin_PrfIrr_equiv_reduce (l l' : PrfIrrVarLFun a1) (r r' : PrfIrrVarLFun a2) :
      sjoin_PrfIrr l r == sjoin_PrfIrr l' r' ->
      l == l' /\ r == r'.
    Proof.
      destruct l as [(l & Hl)], l' as [(l' & Hl')], r as [(r & Hr)], r' as [(r' & Hr')].
      apply sjoin_equiv_reduce;
        intros v v' Hproj.
      - now rewrite (Hl  v v' Hproj).
      - now rewrite (Hl' v v' Hproj).
      - now rewrite (Hr  v v' Hproj).
      - now rewrite (Hr' v v' Hproj).
    Qed.

    (** We can lift all of our join-then-discard results to proof-irrelevant
        functions. *)

    Lemma sjoin_PrfIrr_l (s1 : PrfIrrVarLFun a1) (s2 : PrfIrrVarLFun a2) :
      s1 == (unapp_PrfIrrVarLFun_l a1 a2 (sjoin_PrfIrr s1 s2)).
    Proof.
      destruct s1 as ((s1 & Hs1)), s2 as ((s2 & Hs2)).
      apply sjoin_l.
      (* Now rearrange the proof irrelevance result from [s]. *)
      intros v v' Hproj.
      now rewrite (Hs1 v v' Hproj).
    Qed.

    Lemma sjoin_PrfIrr_r (s1 : PrfIrrVarLFun a1) (s2 : PrfIrrVarLFun a2) :
      s2 == (unapp_PrfIrrVarLFun_r a1 a2 (sjoin_PrfIrr s1 s2)).
    Proof.
      destruct s1 as ((s1 & Hs1)), s2 as ((s2 & Hs2)).
      apply sjoin_r.
      (* Now rearrange the proof irrelevance result from [s]. *)
      intros v v' Hproj.
      now rewrite (Hs2 v v' Hproj).
    Qed.

    Lemma state_split_PrfIrr (s : PrfIrrVarLFun (a1 ++ a2)) :
      s ==
      sjoin_PrfIrr
        (unapp_PrfIrrVarLFun_l a1 a2 s)
        (unapp_PrfIrrVarLFun_r a1 a2 s).
    Proof.
      destruct s as [(s & Hs)].
      apply state_split.
      (* Now rearrange the proof irrelevance result from [s]. *)
      intros v v' Hproj.
      now rewrite (Hs v v' Hproj).
    Qed.
  End sjoin.

  (** [states_agree_PrfIrr] is reflexive. *)
  Lemma states_agree_PrfIrr_reflexive (a : list Var) :
    Reflexive (states_agree_PrfIrr (a1 := a)).
  Proof.
    intros s v Hin_a1 Hin_a2.
    destruct s.
    destruct f as (f & Hf); auto.
  Qed.

  (** Modulo membership proofs, [sjoin_agree]-ing a state onto itself is
      idempotent. *)

  Corollary sjoin_agree_PrfIrr_idem
            (a : list Var) (s : PrfIrrVarLFun a)
            (Heq_dec : forall x y : Var, { x = y } + { x <> y })
            (v : AVarL a)
            (v' : AVarL (a ++ a)) :
    `v = `v' ->
    extract_pfun s v
    ==
    extract_pfun (sjoin_agree_PrfIrr Heq_dec s s (states_agree_PrfIrr_reflexive a s))
                 v'.
  Proof.
    destruct v as (v & Hv), v' as (v' & Hv'), s as [(s & Hs)].
    cbn.
    intros ->.
    unfold sjoin_agree.
    cbn.
    destruct (inp Heq_dec v' a); auto.
    contradiction (in_but_not_in Heq_dec a a (exist _ v' Hv')).
  Qed.
End alpha_proof_irrelevant_states.