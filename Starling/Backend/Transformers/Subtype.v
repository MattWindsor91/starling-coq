(** * Subtype instance transformers for PredEx and RelEx

    This unit contains derived instances that convert [PredEx] etc. over
    a shared state [SSt] to the same instances over a subtype of [SSt].

    Most of the operations remain the same, with the exception of the
    interpretations, which must now [proj1_sig] over the state. *)

From Coq Require Import
     Classes.RelationClasses
     Classes.Morphisms
     Classes.RelationPairs
     Classes.SetoidClass
     Sets.Ensembles
     Sets.Constructive_sets
     Program.Basics.

From Starling Require Import
     Utils.Ensemble.Facts
     Views.Transformers.Subtype
     Backend.Classes.

Set Implicit Arguments.

Local Open Scope backend_scope.
Local Open Scope program_scope.
Local Open Scope signature_scope.

Section transformers_subtype.

  Variables
    GCtx    (* Global context *)
    LCtx    (* Local context *)
  : Type.
  
  Variable
    SSt     (* Shared state *)
    : Type.

  Variable
    P (* Subtyping proposition *)
    : SSt -> Prop.

  Context
    {S_SSt : Setoid SSt}.

  Global Instance PredEx_State_Subtype (Ep : Type) {P_Ep : PredEx Ep GCtx LCtx SSt}
    : PredEx Ep GCtx LCtx {x : SSt | P x} :=
    {|
      pe_true := pe_true;
      pe_conj := pe_conj;
      pe_interp (g : GCtx) (l : LCtx) (ep : Ep) :=
        pe_interp g l ep ∘ (proj1_sig (P := P));
    |}.
  Proof.
    - (* pe_true_unit_inc *)
      intros gc lc.
      intros (x & Hx) _.
      apply pe_true_True.
    - (* pe_equiv_equiv *)
      intros gc lc ep (s & Hs) (s' & Hs') Hequiv.
      apply pe_interp_equiv, Hequiv.
    - (* pe_conj_distr *)
      intros gc lc e1 e2.
      split; intros (s & Hs).
      + (* conjunction -> intersection *)
        intros Hin%pe_conj_distr%Intersection_inv.
        apply Intersection_intro; apply Hin.
      + (* conjunction <- intersection *)
        intros Hin%Intersection_inv.
        apply pe_conj_distr, Intersection_intro; apply Hin.
  Defined.

  Global Instance ImplPredEx_State_Subtype (Ep : Type)
         {P_Ep : PredEx Ep GCtx LCtx SSt}
         {L_Ep : ImplPredEx Ep GCtx LCtx SSt}
    : ImplPredEx Ep GCtx LCtx {x : SSt | P x} :=
    {|
      pe_impl  := pe_impl;
      pe_false := pe_false;
    |}.
  Proof.
    - (* false *)
      intros gc lc x Hin_false.
      exfalso.
      eapply Noone_in_empty, pe_false_empty, Hin_false.
    - (* modus ponens *)
      intros gc lc e1 e2 (s & Hs).
      apply (pe_impl_modus_ponens gc lc e1 e2 s).
    - (* conditional proof *)
      intros gc lc e1 e2 (s & Hs).
      apply (pe_impl_cond gc lc e1 e2 s).
  Defined.

  Global Instance RelEx_State_Subtype (Er : Type) {R_Er : RelEx Er GCtx LCtx SSt}
    : RelEx Er GCtx LCtx {x : SSt | P x} :=
    {|
      re_id := re_id;
      re_empty := re_empty;

      re_interp (g : GCtx) (l : LCtx) (er : Er) :=
        re_interp g l er @@ (proj1_sig (P := P));
    |}.
  Proof.
    - (* re_id_id *)
      intros gc lc (s & Hs) (s' & Hs').
      apply (re_id_id gc lc s s').
    - (* re_empty_empty *)
      intros gc lc (s & Hs) (s' & Hs').
      apply (re_empty_empty gc lc s s').
  Defined.
End transformers_subtype.