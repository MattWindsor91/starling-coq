(** * Function instance transformers for PredEx and RelEx *)

From Coq Require Import
     Classes.RelationClasses
     Classes.Morphisms
     Classes.SetoidClass
     Sets.Ensembles
     Sets.Constructive_sets
     Program.Basics
     ssreflect ssrfun ssrbool
.

From Starling Require Import
     Views.Transformers.Function
     Backend.Classes.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope backend_scope.

Section transformers_function.

  Variables
    GCtx    (* Global context *)
    LCtx_Ex (* Existing local context *)
    LCtx    (* New local context *)
  : Type.

  Variable
    SSt     (* Shared state *)
    : Set.

  Context {S_SSt : Setoid SSt}.

  Global Instance PredEx_Fun (Ep : Type) {P_Ep : PredEx Ep GCtx LCtx_Ex SSt}
    : PredEx (LCtx -> Ep) GCtx (LCtx * LCtx_Ex) SSt :=
    {|
      pe_true := const pe_true;
      pe_conj := pointwise_extension pe_conj (Tcons LCtx Tnil);
      pe_interp (g : GCtx) (lc : LCtx * LCtx_Ex) (f : LCtx -> Ep) :=
        let: (l, le) := lc in In _ (pe_interp g le (f l))
    |}.
  Proof.
    - (* pe_true_unit *)
      move: pe_true_unit => Hunit gc [l le].
      by move: {Hunit} (Hunit Ep GCtx LCtx_Ex SSt S_SSt P_Ep gc le) => [Hunit].
    - (* pe_equiv_proper *)
      move=> gc [l le] ep.
      exact: pe_interp_equiv.
    - (* pe_conj_distr *)
      move=> gc [l le] e1 e2.
      exact: pe_conj_distr.
  Defined.

  Global Instance ImplPredEx_Fun (Ep : Type)
         {P_Ep : PredEx Ep GCtx LCtx_Ex SSt}
         {L_Ep : ImplPredEx Ep GCtx LCtx_Ex SSt}
    : ImplPredEx (LCtx -> Ep) GCtx (LCtx * LCtx_Ex) SSt :=
    {|
      pe_impl := pointwise_extension pe_impl (Tcons LCtx Tnil);
      pe_false := const pe_false;
    |}.
  Proof.
    - (* false *)
      move: pe_false_empty => Hempty gc [l le].
      by move: {Hempty} (Hempty Ep GCtx LCtx_Ex SSt S_SSt P_Ep L_Ep gc le) => [Hempty].
    - (* modus ponens *)
      move=> gc [l le] e1 e2.
      exact: pe_impl_modus_ponens.
    - (* conditional proof *)
      move=> gc [l le] e1 e2.
      exact: pe_impl_cond.
  Defined.

  Global Instance RelEx_Fun (Er : Type) {R_Er : RelEx Er GCtx LCtx_Ex SSt}
    : RelEx (LCtx -> Er) GCtx (LCtx * LCtx_Ex) SSt :=
    {|
      re_id := const re_id;
      re_empty := const re_empty;

      re_interp (g : GCtx) (lc : LCtx * LCtx_Ex) (f : LCtx -> Er) :=
        let: (l, le) := lc in re_interp g le (f l)
    |}.
  Proof.
    - (* re_id_id *)
      move=> gc [l le].
      exact: re_id_id.
    - (* re_empty_empty *)
      move=> gc [l le].
      exact: re_empty_empty.
  Defined.

  Section transformers_function_erase.
    (** ** Erasing functions in backend conditions

        If the predicate and relation expressions in a backend condition are
        functions, we can lift out the functions by moving the quantifications
        into the condition sets.  In general, this doesn't give us a finite
        condition set! *)

    Variables
      Ep (* Predicate expressions *)
      Er (* Relation expressions *)
    : Type.

    Context
      {P_Ep : PredEx Ep GCtx LCtx_Ex SSt}
      {R_Er : RelEx Er GCtx LCtx_Ex SSt}.

    (** [fun_apply] applies a functional backend condition to a local
        context. *)

    Definition fun_apply
               (x : BVCond (LCtx -> Ep) (LCtx -> Er))
               (lc : LCtx)
      : BVCond Ep Er :=
      << x.(bv_w) lc >> (x.(bv_c) lc) << x.(bv_g) lc >>.

    (** [fun_erase] erases a function dependency on [LCtx] across a
        set [xs] of functional backend conditions by returning a set
        whose members are the [fun_apply] of an element of [xs] to
        some local context. *)

    Definition fun_erase
               (xs: Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er))) :
      Ensemble (BVCond Ep Er) :=
      (fun (x : BVCond Ep Er) =>
         exists (x' : BVCond (LCtx -> Ep) (LCtx -> Er)) (l : LCtx),
           Ensembles.In _ xs x' /\ x = fun_apply x' l).

    Lemma fun_erase_fun_apply
          (xs: Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er)))
          (x : BVCond (LCtx -> Ep) (LCtx -> Er)) (l : LCtx) :
      In _ xs x -> In _ (fun_erase xs) (fun_apply x l).
    Proof.
        by exists x, l.
    Qed.

    (** The erasure of the empty set is the empty set. *)

    Lemma fun_erase_Empty: Same_set _ (fun_erase (Empty_set _)) (Empty_set _).
    Proof.
      by apply: conj; move=> s //= [x] [_] [Hin] _ //=.
    Qed.

    (** Local erasure distributes across [Union]. *)

    Lemma fun_erase_Union (xs ys: Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er))):
      Same_set _
               (fun_erase (Union _ xs ys))
               (Union _ (fun_erase xs) (fun_erase ys)).
    Proof.
      apply: conj; move=> x.
      - move=> [x'] [l] [Hin] ->.
        move/(Union_inv _ _ _ _): Hin => [Hin|Hin]; [left|right]; exact: fun_erase_fun_apply.
      - by move/(Union_inv _ _ _ _) => [[x'] [l] [Hin] ->|[x'] [l] [Hin] ->]; exists x', l; apply: conj; move=> //=; [left|right].
    Qed.

    Lemma fun_erase_Proper: Proper (Same_set _ ==> Same_set _) fun_erase.
      move=> xs ys [Hxs_ys] Hys_xs.
      by apply: conj;
        move=> s [x] [l] [Hin] ->;
        exists x, l; apply: conj; move=> //=; [move/Hxs_ys: Hin|move/Hys_xs: Hin].
    Qed.

    (** If the verification condition [x] satisfies the
        verification-condition Hoare judgement over [gc], its
        erasure satisfies the function erasure over all local contexts, and
        vice versa. *)

    Lemma bvhoare_fun (gc : GCtx) (x : BVCond (LCtx -> Ep) (LCtx -> Er)) :
      bvhoare gc x <-> forall (lc : LCtx), bvhoare gc (fun_apply x lc).
    Proof.
      apply: conj.
      - (* -> *)
        move=> Hbv_fun lc s s' lc'.
        by apply: (Hbv_fun s s' (lc, lc')).
      - (* <- *)
        move=> Hbv s s' [lc] lc'.
        by apply: Hbv.
    Qed.

    Lemma bvhoare_fun_erase (gc : GCtx) (xs : Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er))) :
      Included _ xs             (bvhoare gc) <->
      Included _ (fun_erase xs) (bvhoare gc).
    Proof.
      apply: conj.
      - move=> Hloc [w c g] [x'] [lc] [Hin] -> /=.
        move/Hloc: Hin lc.
        by move/(bvhoare_fun gc x').
      - move=> Herased x Hin.
        have: (forall (lc : LCtx), bvhoare gc (fun_apply x lc)); last by move/bvhoare_fun.
        move=> lc.
        move/(fun_erase_fun_apply lc): Hin.
          by move/Herased.
    Qed.

    Corollary ideal_solve_fun_erase (xs: Ensemble (BVCond (LCtx -> Ep) (LCtx -> Er))) :
      ideal_solve xs <->
      ideal_solve (fun_erase xs).
    Proof.
      apply: conj; move=> [gc] Hloc; exists gc; exact/bvhoare_fun_erase.
    Qed.
  End transformers_function_erase.
End transformers_function.