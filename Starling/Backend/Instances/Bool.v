(** * Backend interface instances for [bool] *)

From Coq Require Import
     Bool
     Sets.Constructive_sets
     Sets.Ensembles
     Lists.List
     Relations.Relation_Definitions
     Classes.RelationClasses
     Classes.SetoidClass
     Classes.SetoidDec
     Program.Basics.

From Starling Require Import
     Utils.Ensemble.Facts
     Utils.List.Facts
     Backend.Alpha
     Backend.Classes.

Chapter starling_logic_backend_instances_bool.

Local Open Scope predicate_scope.

Variables
  (S  : Type).

Context {S_S : Setoid S}.

(** Propositions can be [PredEx]es over any setoid, if the proposition
      always maps equivalent values to the same truth result. *)
Definition SetoidBool : Type := { x : S -> bool | Proper (equiv ==> eq) x }.

Definition true_SetoidBool : SetoidBool.
Proof.
  exists (const true).
  intros x y _.
  split; intros _; exact I.
Defined.

Definition conj_SetoidBool : SetoidBool -> SetoidBool -> SetoidBool.
Proof.
  intros (x & Hxp) (y & Hyp).
  exists (fun v => x v && y v).
  intros s1 s2 Heq.
  now rewrite (Hxp s1 s2 Heq), (Hyp s1 s2 Heq).
Defined.

Global Instance SetoidBool_PredEx : PredEx SetoidBool unit unit S (S_S := S_S) :=
  {
    pe_true := true_SetoidBool;
    pe_conj := conj_SetoidBool;
    pe_interp tt tt p s := Is_true ((proj1_sig p) s)
  }.
Proof.
  - now intros [] [] s Hin.
  - intros [] [] (ep & Hep) s s' Heq.
    cbn.
    now rewrite (Hep s s' Heq).
  - intros [] [] (ep1 & Hep1) (ep2 & Hep2).
    split.
    + now intros s Hin_s%andb_prop_elim.
    + intros s Hin_inter%Intersection_inv.
      now apply andb_prop_intro.
Defined.

(** Booleans are trivially [PredEx]es, if the setoid is identity. *)

Global Instance Bool_PredEx : PredEx (S -> bool) unit unit S (S_S := eq_setoid S) :=
  {
    pe_true := const true;
    pe_conj p q s := p s && q s;
    pe_interp tt tt p s := Is_true (p s);
  }.
Proof.
  - now intros [] [] s Hin.
  - intros [] [] p q.
    split.
    + now intros s Hin_s%andb_prop_elim.
    + intros s Hin_inter%Intersection_inv.
      now apply andb_prop_intro.
Defined.

(** Booleans are trivially [ImplPredEx]es. *)

Global Instance Bool_ImplPredEx : ImplPredEx (S -> bool) unit unit S (P_Ep := Bool_PredEx) :=
  {
    pe_impl p q s := implb (p s) (q s);
    pe_false := const false;
  }.
Proof.
  - (* False. *)
    intuition.
  - (* Modus ponens *)
    intros gc lc p q s (Hin_s_impl & Hin_s_2)%andb_prop_elim.
    cbn in *.
    destruct (p s), (q s); intuition.
  - (* Conditional proof *)
    intros gc lc p q s Hcond.
    cbn in *.
    destruct (p s), (q s); intuition.
Defined.

(** Booleans with contexts are [PredEx]es. *)

Global Instance Bool_PredEx_Aux (GCtx LCtx : Type): PredEx (GCtx -> LCtx -> S -> bool) GCtx LCtx S (S_S := eq_setoid S) :=
  {
    pe_true _ _ _ := true;
    pe_conj p q gc lc s := p gc lc s && q gc lc s;
    pe_interp gc lc p s := Is_true (p gc lc s);
  }.
Proof.
  - now intros gc lc.
  - intros gc lc p q.
    split.
    + now intros s Hin_s%andb_prop_elim.
    + intros s Hin_inter%Intersection_inv.
      now apply andb_prop_intro.
Defined.

Global Instance Bool_ImplPredEx_Aux (GCtx LCtx : Type) : ImplPredEx (GCtx -> LCtx -> S -> bool) GCtx LCtx S (S_S := eq_setoid S) :=
  {
    pe_impl p q gc lc s := implb (p gc lc s) (q gc lc s);
    pe_false _ _ _ := false
  }.
Proof.
  - (* False. *)
    intuition.
  - (* Modus ponens *)
    intros gc lc p q s (Hin_s_impl & Hin_s_2)%andb_prop_elim.
    cbn in *.
    destruct (p gc lc s), (q gc lc s); intuition.
  - (* Conditional proof *)
    intros gc lc p q s Hcond.
    cbn in *.
    destruct (p gc lc s), (q gc lc s); intuition.
Defined.

(** Boolean relations are [RelEx]es whenever equality is decidable. *)

Global Instance Bool_RelEx { ED : SetoidDec.EqDec (eq_setoid S) }
  : RelEx (S -> S -> bool) unit unit S (Setoid_S := eq_setoid S) :=
  {
    re_id s s' := @equiv_decb _ (eq_setoid S) ED s s';
    re_interp tt tt r s s' := Is_true (r s s');
    re_empty s s' := false
  }.
Proof.
  - intros gc lc s s'.
    unfold "==b".
    cbn.
    destruct (equiv_dec s s'); intuition.
  - firstorder.
Defined.

Global Instance Bool_RelEx_Aux (GCtx LCtx : Type) { ED : EqDec (eq_setoid S) }
  : RelEx (GCtx -> LCtx -> S -> S -> bool) GCtx LCtx S (Setoid_S := eq_setoid S) :=
  {
    re_id gc lc s s' := @equiv_decb _ (eq_setoid S) ED s s';
    re_interp gc lc r s s' := Is_true (r gc lc s s');
    re_empty gc lc s s' := false
  }.
Proof.
  - intros gc lc s s'.
    unfold "==b".
    cbn.
    destruct (equiv_dec s s'); intuition.
  - firstorder.
Defined.

(** We can now build a [Backend] over props and relations. *)

Global Instance Bool_Backend { ED : EqDec (eq_setoid S) } :
  Backend (S -> bool) (S -> S -> bool) unit unit S :=
  {
    bi_setoid := eq_setoid S;
    bi_solve := ideal_solve (S_S := eq_setoid S)
  }.
Proof.
  - intros xs ys Hxy_same (a & Hxs).
    exists a.
    firstorder.
  - firstorder.
Defined.

Global Instance Bool_CompBackend { ED : EqDec (eq_setoid S) } :
  CompBackend (BI := Bool_Backend) :=
  {
  }.
Proof.
  - now exists tt.
  - intros xs ys ([] & Hxs) ([] & Hys).
    exists tt.
    intros x [|]; intuition.
  - intros xs ys ([] & Hxys).
    split; exists tt; intuition.
Defined.

Global Instance Bool_Backend_Aux (GCtx LCtx : Type) { ED : EqDec (eq_setoid S) } :
  Backend (GCtx -> LCtx -> S -> bool) (GCtx -> LCtx -> S -> S -> bool) GCtx LCtx S :=
  {
    bi_setoid := eq_setoid S;
    bi_solve := ideal_solve (S_S := eq_setoid S);
  }.
Proof.
  - intros xs ys Hxy_same (a & Hxs).
    exists a.
    firstorder.
  - intuition.
Defined.

End starling_logic_backend_instances_bool.
