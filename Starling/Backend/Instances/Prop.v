(** * Backend interface instances for [Prop] and [relation] *)

From Coq Require Import
     Sets.Constructive_sets
     Sets.Ensembles
     Lists.List
     Relations.Relation_Definitions
     Classes.RelationClasses
     Classes.SetoidClass
     Classes.SetoidDec
     Program.Basics.

From Starling Require Import
     Utils.Ensemble.Facts
     Utils.List.Facts
     Backend.Alpha
     Backend.Classes.

Chapter starling_logic_backend_instances_prop.

Local Open Scope predicate_scope.

Variables
  (S  : Type).

Context {S_S : Setoid S}.

(** Propositions can be [PredEx]es over any setoid, if the proposition
      always maps equivalent values to the same truth result. *)
Definition SetoidProp : Type := { x : S -> Prop | Proper (equiv ==> iff) x }.

Definition true_SetoidProp : SetoidProp.
Proof.
  exists (true_predicate : unary_predicate S).
  intros x y _.
  split; intros _; exact I.
Defined.

Definition conj_SetoidProp : SetoidProp -> SetoidProp -> SetoidProp.
Proof.
  intros (x & Hxp) (y & Hyp).
  exists (predicate_intersection (Tcons S Tnil) x y).
  intros s1 s2 Heq.
  pose (symmetry Heq) as Hqe.
  split; intros (Hx & Hy); split.
  - apply (Hxp s1); assumption.
  - apply (Hyp s1); assumption.
  - apply (Hxp s2); assumption.
  - apply (Hyp s2); assumption.
Defined.

Global Instance SetoidProp_PredEx : PredEx SetoidProp unit unit S (S_S := S_S) :=
  {
    pe_true := true_SetoidProp;
    pe_conj := conj_SetoidProp;
    pe_interp := const (const (proj1_sig (A := S -> Prop) (P := Proper (equiv ==> iff))));
  }.
Proof.
  - now intros [] [] s Hin.
  - intros [] [] (ep & Hep) s s' Heq.
    split; unfold const.
    + apply Hep, symmetry, Heq.
    + apply Hep, Heq.
  - intros [] [] (ep1 & Hep1) (ep2 & Hep2).
    split.
    + firstorder.
    + now intros s Hin_inter%Intersection_inv.
Defined.

(** Propositions are trivially [PredEx]es, if the setoid is identity. *)

Global Instance Prop_PredEx : PredEx (S -> Prop) unit unit S (S_S := eq_setoid S) :=
  {
    pe_true := (true_predicate : predicate (Tcons S Tnil));
    pe_conj := predicate_intersection (Tcons S Tnil);
    pe_interp := const (const id);
  }.
Proof.
  - now intros [] [] s Hin.
  - intros x y ep1 ep2.
    split; intros s Hin.
    + firstorder.
    + destruct Hin as [s Hin1 Hin2]; firstorder.
Defined.

(** Propositions are trivially [ImplPredEx]es. *)

Global Instance Prop_ImplPredEx : ImplPredEx (S -> Prop) unit unit S (P_Ep := Prop_PredEx) :=
  {
    pe_impl := pointwise_extension impl (Tcons S Tnil);
    pe_false := (false_predicate : predicate (Tcons S Tnil));
  }.
Proof.
  - (* False. *)
    intuition.
  - (* Modus ponens *)    
    firstorder.
  - (* Conditional proof *)
    firstorder.
Defined.

(** Propositions with contexts are [PredEx]es. *)

Global Instance Prop_PredEx_Aux (GCtx LCtx : Type): PredEx (GCtx -> LCtx -> S -> Prop) GCtx LCtx S (S_S := eq_setoid S) :=
  {
    pe_true := (true_predicate : predicate (Tcons GCtx (Tcons LCtx (Tcons S Tnil))));
    pe_conj := predicate_intersection (Tcons GCtx (Tcons LCtx (Tcons S Tnil)));
    pe_interp x y f := f x y;
  }.
Proof.
  - now intros gc lc.
  - intros aux ep1 ep2.
    split; intros s Hin.
    + firstorder.
    + destruct Hin as [s Hin1 Hin2]; firstorder.
Defined.

Global Instance Prop_ImplPredEx_Aux (GCtx LCtx : Type) : ImplPredEx (GCtx -> LCtx -> S -> Prop) GCtx LCtx S (S_S := eq_setoid S) :=
  {
    pe_impl := pointwise_extension impl (Tcons GCtx (Tcons LCtx (Tcons S Tnil)));
    pe_false := (false_predicate : predicate (Tcons GCtx (Tcons LCtx (Tcons S Tnil))));
  }.
- (* False *)
  intuition.
- (* Modus ponens *)
  firstorder.
- (* Conditional proof *)
  firstorder.
Defined.

(** Relations are trivially [RelEx]es. *)

Global Instance Prop_RelEx: RelEx (relation S) unit unit S (Setoid_S := eq_setoid S) :=
  {
    re_id := eq;
    re_interp := const (const id);
    re_empty := (false_predicate : predicate (Tcons S (Tcons S Tnil)));
  }.
Proof.
  - intros gc lc s.
    apply predicate_equivalence_equivalence.
  - firstorder.
Defined.

Global Instance Prop_RelEx_Aux (GCtx LCtx: Type): RelEx (GCtx -> LCtx -> relation S) GCtx LCtx S (Setoid_S := eq_setoid S) :=
  {
    re_id x y := eq;
    re_interp x y f := f x y;
    re_empty := (false_predicate : predicate (Tcons GCtx (Tcons LCtx (Tcons S (Tcons S Tnil)))));
  }.
Proof.
  - intros gc lc.
    apply predicate_equivalence_equivalence.
  - firstorder.
Defined.

(** We can now build a [Backend] over props and relations. *)

Global Instance Prop_Backend:
  Backend (S -> Prop) (relation S) unit unit S :=
  {
    bi_setoid := eq_setoid S;    
    bi_solve := ideal_solve (S_S := eq_setoid S)
  }.
Proof.
  - intros xs ys Hxy_same (a & Hxs).
    exists a.
    firstorder.
  - firstorder.
Defined.

Global Instance Prop_CompBackend:
  CompBackend (BI := Prop_Backend) :=
  {
  }.
Proof.
  - now exists tt.
  - intros xs ys ([] & Hxs) ([] & Hys).
    exists tt.
    intros x [|]; intuition.
  - intros xs ys ([] & Hxys).
    split; exists tt; intuition.
Defined.

Global Instance Prop_Backend_Aux (GCtx LCtx : Type):
  Backend (GCtx -> LCtx -> S -> Prop) (GCtx -> LCtx -> relation S) GCtx LCtx S :=
  {
    bi_setoid := eq_setoid S;
    bi_solve := ideal_solve (S_S := eq_setoid S);
  }.
Proof.
  - intros xs ys Hxy_same (a & Hxs).
    exists a.
    firstorder.
  - intuition.
Defined.

End starling_logic_backend_instances_prop.
