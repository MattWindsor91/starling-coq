(** * Alphabetised predicate and relation expressions
    This file just re-exports the [Backend.Alpha] namespace. *)

From Starling Require Export
     Backend.Alpha.Classes
     Backend.Alpha.Chained
     Backend.Alpha.DefVar
     Backend.Alpha.Local
     Backend.Alpha.Marked
     Backend.Alpha.PredAsRel
     Backend.Alpha.PrfIrr
     Backend.Alpha.SJoin
     Backend.Alpha.Var.