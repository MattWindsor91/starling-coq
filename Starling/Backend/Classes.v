(** * Core classes for theory expressions

    This library contains the core typeclasses for 'theory
    expressions' Starling.  Of note are:

    - Class [PredEx]: predicate expressions
    - Class [ImplPredEx]: predicate expressions with implication
    - Class [RelEx]: relation expressions
    - Class [Backend]: backend interfaces
    - Class [CompositionalBackend]: backend interfaces with proof compositionality *)

From Coq Require Import
     Classes.RelationClasses
     Classes.SetoidClass
     Lists.List
     Lists.SetoidPermutation
     Program.Basics
     Relations.Relation_Definitions
     Sets.Constructive_sets.

From Starling Require Import
     Utils.Ensemble.Facts
     Views.Classes.

Chapter starling_logic_backend_classes.

Section starling_logic_backend_classes_predex.

  (** ** Predicate expressions

   Class [PredEx] captures predicate expressions with type [Ep], over
   the global context type [GCtx], local context type [LCtx], and state type
   [S]. *)

  Class PredEx (Ep GCtx LCtx : Type) (S: Type) {S_S : Setoid S} :=
    mk_PredEx
      { (*
         * Operators
         *)

        (** [pe_true] is the truth expression. *)

        pe_true: Ep;

        (** [pe_conj] is the classical conjunction operator on
            expressions. *)

        pe_conj: Ep -> Ep -> Ep;

        (** [pe_interp] interprets an expression as a state
            set, given some backend context. *)

        pe_interp: GCtx -> LCtx -> Ep -> Ensemble S;

        (*
         * Laws
         *)

        (** [pe_true] must be true for all states.
            (Every state is included in [Full_set], so we only need one
            direction. *)

        pe_true_unit_inc gc lc:
          Included S
                   (Full_set S)
                   (pe_interp gc lc pe_true);

        (** Equivalent states must produce the same interpretation. *)

        pe_interp_equiv gc lc ep :
          Proper (SetoidClass.equiv ==> iff) (pe_interp gc lc ep);

        (** [pe_conj] must behave like classical conjunction. *)

        pe_conj_distr gc lc ep1 ep2:
          Same_set S
                   (pe_interp gc lc (pe_conj ep1 ep2))
                   (Intersection S
                                 (pe_interp gc lc ep1)
                                 (pe_interp gc lc ep2));
      }.

  Class ImplPredEx (Ep GCtx LCtx : Type) (S : Type) {S_S : Setoid S} {P_Ep : PredEx Ep GCtx LCtx S} :=
    mk_ImplPredEx
      {
        (*
         * Operators
         *)

        (** [pe_impl] is the classical implication operator on
            expressions. *)

        pe_impl: Ep -> Ep -> Ep;

        (** [pe_false] is the false expression. *)

        pe_false : Ep;

        (*
         * Laws
         *)

        (** [pe_false] must be empty for all states.
            (Every state includes [Empty_set], so we only need one
            direction. *)

        pe_false_empty_inc gc lc:
          Included S
                   (pe_interp gc lc pe_false)
                   (Empty_set S);

        (** [pe_impl] must satisfy modus ponens. *)

        pe_impl_modus_ponens gc lc ep1 ep2 :
          Included S
                   (pe_interp gc lc (pe_conj (pe_impl ep1 ep2) ep1))
                   (pe_interp gc lc ep2);

        (** [pe_impl] must satisfy conditional proof. *)

        pe_impl_cond gc lc ep1 ep2 s :
          (In S (pe_interp gc lc ep1) s ->
           In S (pe_interp gc lc ep2) s) ->
          In S (pe_interp gc lc (pe_impl ep1 ep2)) s;

      }.

End starling_logic_backend_classes_predex.

Section starling_logic_backend_classes_relex.
  Local Open Scope predicate_scope.

  Class RelEx (Er GCtx LCtx: Type) (S: Type) {Setoid_S : Setoid S} :=
    mk_RelEx
      { (*
         * Operators
         *)

        (* Identity relation expression *)
        re_id : Er;

        (* Empty relation expression *)
        re_empty : Er;

        (* Interpretation *)
        re_interp: GCtx -> LCtx -> Er -> relation S;

        (*
         * Laws
         *)

        re_id_id gc lc :
          (re_interp gc lc re_id : binary_relation S) <∙> (SetoidClass.equiv);

        re_empty_empty gc lc :
          (re_interp gc lc re_empty : binary_relation S) -∙> false_predicate;
      }.

End starling_logic_backend_classes_relex.

Set Implicit Arguments.

Section starling_logic_backend_classes_bvcond.
  (** A [BVCond] is a backend verification condition. *)

  Record BVCond (Ep Er : Type) :=
    mk_BVCond
      {

        (** [bv_w] is the weakest-precondition predicate expression. *)

        bv_w: Ep;

        (** [bv_c] is the command relation expression. *)

        bv_c: Er;

        (** [bv_g] is the goal predicate expression. *)

        bv_g: Ep
      }.

  Context
    {Ep    : Type}
    {Er    : Type}
    {GCtx  : Type}
    {LCtx  : Type}
    {S     : Type}
    {S_S   : Setoid S}
    {P_Ep  : PredEx Ep GCtx LCtx S}
    {P_Er  : RelEx  Er GCtx LCtx S}.

  (** [bvhoare] is the Hoare judgement on backend verification conditions,
      given a specific solver context [aux]. *)

  Definition bvhoare (gc : GCtx) (x : BVCond Ep Er): Prop :=
    forall (s s' : S) (lc : LCtx),
      Ensembles.In S (pe_interp gc lc x.(bv_w)) s ->
      re_interp gc lc x.(bv_c) s s' ->
      Ensembles.In S (pe_interp gc lc x.(bv_g)) s'.

  (** [bvhoare] on a [rel_id] amounts to an entailment on the
      conditions. *)

  Lemma bvhoare_id (gc : GCtx) (x : BVCond Ep Er) :
    x.(bv_c) = re_id ->
    (forall lc : LCtx,
        (Ensembles.Included S
                            (pe_interp gc lc x.(bv_w))
                            (pe_interp gc lc x.(bv_g)))) ->
    bvhoare gc x.
  Proof.
    destruct x as [w c g].
    cbn.
    intros -> Hinc s s' lc Hin_p Hcmd%re_id_id.
    apply Hinc in Hin_p.
    eapply pe_interp_equiv.
    - apply symmetry, Hcmd.
    - assumption.
  Qed.

  (** [ideal_solve] is the idealised solver function on sets [xs] of
      conditions. *)

  Definition ideal_solve (xs: Ensemble (BVCond Ep Er)): Prop :=
    exists (a: GCtx), forall (x: BVCond Ep Er),
        Ensembles.In _ xs x -> bvhoare a x.

  (** [ideal_solve_list] is the idealised solver function on
      lists [xs] of conditions. *)

  Definition ideal_solve_list (xs: list (BVCond Ep Er)): Prop :=
    exists (a: GCtx), Forall (bvhoare a) xs.

  (** [ideal_solve_list] is equivalent to [ideal_solve] when the set is
      just the set of all [x] that is in the list [xs]. *)

  Lemma ideal_solve_list_as_set (xs: list (BVCond Ep Er)):
    ideal_solve (flip (List.In (A := BVCond Ep Er)) xs) <->
    ideal_solve_list xs.
  Proof.
    split; intros (a & His); exists a; apply Forall_forall, His.
  Qed.
End starling_logic_backend_classes_bvcond.

Section starling_logic_backend_classes_interface.
  (** [Backend] is the class of backend interfaces. *)

  Class Backend (Ep Er GCtx LCtx : Type) (S: Type) :=
    mk_Backend
      {
        bi_setoid :> Setoid S;
        bi_predex :> PredEx Ep GCtx LCtx S;
        bi_relex :> RelEx Er GCtx LCtx S;

        bi_solve: Ensemble (BVCond Ep Er) -> Prop;
        bi_solve_same xs ys: Same_set _ xs ys -> bi_solve xs -> bi_solve ys;
        bi_solve_ideal xs: bi_solve xs -> ideal_solve xs
      }.

  (** [bi_solve_list] adapts [bi_solve] to lists. *)

  Definition bi_solve_list {Ep Er GCtx LCtx : Type} {S: Type} {BI: Backend Ep Er GCtx LCtx S} (xs: list (BVCond Ep Er)): Prop :=
    bi_solve (flip (List.In (A := _)) xs).

  Lemma bi_solve_list_ideal {Ep Er GCtx LCtx : Type} {S: Type} {BI: Backend Ep Er GCtx LCtx S} (xs: list (BVCond Ep Er)):
    bi_solve_list xs -> ideal_solve_list xs.
  Proof.
    intro Hbsl.
    apply ideal_solve_list_as_set, bi_solve_ideal, Hbsl.
  Qed.

  (** [CompBackend] is the class of compositional backend interfaces. *)

  Class CompBackend (Ep Er GCtx LCtx : Type) (S: Type) {BI: Backend Ep Er GCtx LCtx S} :=
    mk_CompBackend
      {
        cbi_solve_empty: bi_solve (Empty_set _);
        cbi_solve_union xs ys: bi_solve xs -> bi_solve ys -> bi_solve (Union _ xs ys);
        cbi_solve_noinu xs ys: bi_solve (Union _ xs ys) -> bi_solve xs /\ bi_solve ys;
      }.

  Section starling_logic_backend_classes_cbi_facts.
    Variables
      (Ep Er: Type)
      (S: Type).

    Context
      `{CBI: CompBackend Ep Er S}.

    (** A compositional solver can solve an empty list [nil]. *)

    Lemma cbi_solve_list_nil:
      bi_solve_list nil.
    Proof.
      apply bi_solve_same with (xs := (Empty_set _)).
      - split; intros s Hin; contradiction.
      - apply cbi_solve_empty.
    Qed.

    (** If a compositional solver can solve a list [xs], as well as a list
        [xs], it can solve [xs++ys]. *)

    Lemma list_union_is_app (xs ys: list (BVCond Ep Er)):
      Same_set
        _
        (Union _ (flip (@List.In _) xs) (flip (@List.In _) ys))
        (flip (@List.In _) (xs ++ ys)).
    Proof.
      split; intros s.
      - intros [|]; apply List.in_or_app; auto.
      - intros Hxys.
        unfold Ensembles.In, flip in Hxys.
        apply List.in_app_or in Hxys.
        destruct Hxys as [H|H].
        + now left.
        + now right.
    Qed.

    Lemma cbi_solve_list_app (xs ys: list (BVCond Ep Er)):
      bi_solve_list xs ->
      bi_solve_list ys ->
      bi_solve_list (xs ++ ys).
    Proof.
      intros Hxs Hys.
      unfold bi_solve_list, flip.
      apply bi_solve_same with (xs0 := Union _ (fun x => List.In x xs) (fun y => List.In y ys)).
      - apply list_union_is_app.
      - apply cbi_solve_union; assumption.
    Qed.

    (** The converse of [cbi_solve_list_app]. *)

    Lemma cbi_solve_list_ppa (xs ys: list (BVCond Ep Er)):
      bi_solve_list (xs ++ ys) ->
      bi_solve_list xs /\ bi_solve_list ys.
    Proof.
      intro Hxys.
      eapply bi_solve_same in Hxys.
      - apply cbi_solve_noinu, Hxys.
      - symmetry.
        apply list_union_is_app.
    Qed.

    (** If a compositional solver can solve a list [xs], as well as a singleton
        list [x], it can solve [x::xs]. *)

    Lemma cbi_solve_list_cons (x: BVCond Ep Er) (xs: list (BVCond Ep Er)):
      bi_solve_list (x::nil) ->
      bi_solve_list xs ->
      bi_solve_list (x::xs).
    Proof.
      intros Hx Hxs.
      replace (x::xs) with ((x::nil) ++ xs).
      - apply cbi_solve_list_app; assumption.
      - reflexivity.
    Qed.
  End starling_logic_backend_classes_cbi_facts.
End starling_logic_backend_classes_interface.

End starling_logic_backend_classes.

Hint Resolve
     pe_true_unit_inc
     pe_false_empty_inc
     pe_impl_cond
  : backend.

Notation TT := pe_true.
Notation FF := pe_false.
Infix "&" := pe_conj (at level 40, left associativity) : backend_scope.
Infix "==>" := pe_impl (at level 55, right associativity) : backend_scope.

(* Not to be confused with <| p |> c <| q |>, which is used for Views
   axioms and other 'atomic Hoare triples'. *)

Notation "<< w >> c << g >>" :=
  (mk_BVCond w c g) (at level 0, c, w, g at next level) : backend_scope.

Section starling_logic_backend_classes_predex_facts.

  (** ** Facts about [PredEx] *)

  Variables
    (Ep   : Type)  (* Predicate expressions *)
    (GCtx : Type)  (* Global solver context *)
    (LCtx : Type)  (* Local solver context *)
    (S    : Type). (* States *)

  Context
    {S_S  : Setoid S}
    {P_Ep : PredEx Ep GCtx LCtx S}.

  Lemma pe_true_unit (gc : GCtx) (lc : LCtx) :
    Same_set S
             (pe_interp gc lc pe_true)
             (Full_set S).
  Proof.
    apply (Full_set_inc_same (A := S)), pe_true_unit_inc.
  Qed.

  Lemma pe_true_True (gc : GCtx) (lc : LCtx) (s : S) :
    In S (pe_interp gc lc pe_true) s.
  Proof.
    now apply pe_true_unit.
  Qed.

  Open Scope program_scope.

  (** If two functions produce equivalent states given the same input,
      then composing them onto the interpretation of a predicate
      yields the same set. *)

  Corollary pe_interp_compose_same_set
            (S' : Type) (gc : GCtx) (lc : LCtx) (p : Ep) (f g : S' -> S) :
    (forall x : S', f x == g x) ->
    Same_set S'
      (pe_interp gc lc p ∘ f)
      (pe_interp gc lc p ∘ g).
  Proof.
    intro Hequiv.
    split; intros s Hin; specialize (Hequiv s); eapply pe_interp_equiv; eauto.
    now symmetry.
  Qed.

  Section pe_equiv.

    (** [PredEx]es are equivalent when, for all contexts, their interpretations
      are the same set. *)

    Definition pe_equiv (ep1 ep2 : Ep) : Prop :=
      forall (gc : GCtx) (lc : LCtx),
        Same_set S (pe_interp gc lc ep1) (pe_interp gc lc ep2).

    Global Instance pe_equiv_Reflexive : Reflexive pe_equiv.
    Proof.
      firstorder.
    Defined.

    Global Instance pe_equiv_Symmetric : Symmetric pe_equiv.
    Proof.
      firstorder.
    Defined.

    Global Instance pe_equiv_Transitive : Transitive pe_equiv.
    Proof.
      intros x y z Hxy Hyz gc lc.
      etransitivity.
      - apply Hxy.
      - apply Hyz.
    Defined.

    Global Instance pe_equiv_Equivalence : Equivalence pe_equiv := {}.

    (** Thus, [PredEx]es form a setoid. *)

    Global Instance pe_Setoid : Setoid Ep :=
      {
        equiv := pe_equiv;
      }.
  End pe_equiv.

  (** [pe_conj] is proper over [PredEx] equivalence. *)

  Global Instance pe_conj_equiv_Proper :
    Proper (SetoidClass.equiv ==> SetoidClass.equiv ==> SetoidClass.equiv) pe_conj.
  Proof.
    intros a b Hab c d Hcd gc lc.
    rewrite ! pe_conj_distr.
    split;
      intros s (Hin_ab & Hin_cd)%Intersection_inv;
      apply Intersection_intro;
      try (now apply Hab);
      try (now apply Hcd).
  Defined.

  (** [pe_true] is the left identity of [pe_conj]. *)

  Lemma pe_true_l (ep : Ep) : pe_conj pe_true ep == ep.
  Proof.
    intros gc lc.
    transitivity (Intersection S
                               (pe_interp gc lc TT)
                               (pe_interp gc lc ep)).
    - apply pe_conj_distr.
    - split.
      + intros _ [s _ Hin_ep].
        exact Hin_ep.
      + intros s Hin.
        split.
        * apply pe_true_unit.
          constructor.
        * exact Hin.
  Qed.

  (** [pe_conj] is associative. *)

  Lemma pe_conj_assoc (ep1 ep2 ep3: Ep):
    (pe_conj (pe_conj ep1 ep2) ep3) ==
    (pe_conj ep1 (pe_conj ep2 ep3)).
  Proof.
    intros gc lc.
    split; intros s Hin%pe_conj_distr; apply pe_conj_distr.
    - destruct Hin as [s Hin_12%pe_conj_distr Hin_3].
      destruct Hin_12 as [s Hin_1 Hin_2].
      split; intuition.
      now apply pe_conj_distr.
    - destruct Hin as [s Hin_1 Hin_23%pe_conj_distr].
      destruct Hin_23 as [s Hin_2 Hin_3].
      split; intuition.
      now apply pe_conj_distr.
  Qed.

  (** [pe_conj] is commutative. *)

  Lemma pe_conj_comm (ep1 ep2 : Ep):
    pe_conj ep1 ep2 == pe_conj ep2 ep1.
  Proof.
    intros gc lc.
    transitivity (Intersection S
                               (pe_interp gc lc ep1)
                               (pe_interp gc lc ep2)).
    {
      apply pe_conj_distr.
    }
    transitivity (Intersection S
                               (pe_interp gc lc ep2)
                               (pe_interp gc lc ep1)).
    {
      split; intros s Hin%Intersection_inv; apply Intersection_intro; apply Hin.
    }
    symmetry.
    apply pe_conj_distr.
  Qed.

  (** [pe_true] is the right identity of [pe_conj]. *)

  Corollary pe_true_r (ep : Ep) : pe_conj ep pe_true == ep.
  Proof.
    rewrite pe_conj_comm.
    apply pe_true_l.
  Qed.

  (** [PredEx]es actually form a (somewhat pointless!) views semigroup. *)

  Global Instance PredEx_ViewsSemigroup : ViewsSemigroup Ep :=
    {
      dot := pe_conj;
      dot_assoc := pe_conj_assoc;
      dot_comm := pe_conj_comm;
      equiv_dot_left a b c Hab := pe_conj_equiv_Proper Hab (reflexivity _);
    }.


  Section pe_inc.
    (** ** Contextual inclusion relation

        A [PredEx] is less than or equal to another [PredEx], modulo a
        [GCtx] [gc] and [LCtx] [lc], when it contains *no fewer*
        states on those contexts. *)

    Definition pe_inc_contextual
               (gc : GCtx) (lc : LCtx) (ep1 ep2 : Ep) : Prop :=
        Included S (pe_interp gc lc ep2) (pe_interp gc lc ep1).

    Global Instance pe_inc_contextual_Reflexive (gc : GCtx) (lc : LCtx)
      : Reflexive (pe_inc_contextual gc lc).
    Proof.
      firstorder.
    Defined.

    Global Instance pe_inc_contextual_Transitive (gc : GCtx) (lc : LCtx)
      : Transitive (pe_inc_contextual gc lc).
    Proof.
      intros x y z Hxy Hyz.
      red.
      etransitivity; eauto.
    Defined.

    (** ** Inclusion relation

        A [PredEx] is less than or equal to another [PredEx] when it
        contains *no fewer* states on all contexts.  (In other words,
        [<<=] is *refinement* order on [PredEx]es. *)

    Definition pe_inc (ep1 ep2 : Ep) : Prop :=
      forall (gc : GCtx) (lc : LCtx), pe_inc_contextual gc lc ep1 ep2.

    Global Instance pe_inc_Reflexive : Reflexive pe_inc.
    Proof.
      firstorder.
    Defined.

    Global Instance pe_inc_Transitive : Transitive pe_inc.
    Proof.
      intros x y z Hxy Hyz gc lc.
      etransitivity; eauto.
    Defined.

    (** Thus, [PredEx]es form a [PreOrder]... *)

    Global Instance pe_inc_PreOrder : PreOrder pe_inc := {}.

    (** Said [PreOrder] has the expected relationship with
        [pe_equiv]. *)

    Lemma pe_inc_equiv :
      relation_equivalence pe_equiv (relation_conjunction pe_inc (flip pe_inc)).
    Proof.
      firstorder.
    Qed.

    (** This gives us a [PartialOrder] on [PredEx]es. *)

    Global Instance pe_inc_PartialOrder : PartialOrder pe_equiv pe_inc :=
      {
        partial_order_equivalence := pe_inc_equiv;
      }.

    (** [pe_conj] is proper over [PredEx] ordering. *)

    Global Instance pe_conj_inc_Proper :
      Proper (pe_inc ==> pe_inc ==> pe_inc) pe_conj.
    Proof.
      intros a b Hab c d Hcd gc lc s
             (Hin_ab & Hin_cd)%pe_conj_distr%Intersection_inv.
      apply pe_conj_distr, Intersection_intro;
      firstorder.
    Defined.

    (** [a] is included in [a & b]. *)

    Lemma pe_inc_conj_mono_left (a b : Ep) :
      pe_inc a (pe_conj a b).
    Proof.
      now intros gc lc s (Hin_a & _)%pe_conj_distr%Intersection_inv.
    Defined.

    Global Instance PredEx_OrderedViewsSemigroup
      : OrderedViewsSemigroup Ep :=
      {
        inc := pe_inc;
        inc_dot_left a b c Hab := pe_conj_inc_Proper Hab (reflexivity _);
        inc_dot_mono_left := pe_inc_conj_mono_left;
      }.

  End pe_inc.

  (** [iconj] is iterated conjunction over a list of expressions. *)

  Definition iconj : list Ep -> Ep :=
    fold_right pe_conj pe_true.

  (** We can unfold one step of [iconj]. *)

  Lemma iconj_cons_interp (x : Ep) (xs : list Ep) :
    pe_conj x (iconj xs) == iconj (x::xs).
  Proof.
    reflexivity.
  Qed.

  (** We can join two [iconj]es together, and the result is equivalent
      (but not equal!) to a single [iconj]. *)

  Corollary iconj_app_interp (xs ys : list Ep) :
    (pe_conj (iconj xs) (iconj ys)) == (iconj (xs ++ ys)).
  Proof.
    intros gc lc.
    induction xs.
    - (* nil *) apply pe_true_l.
    - (* cons *)
      rewrite <- app_comm_cons.
      rewrite pe_conj_distr.
      symmetry.
      etransitivity.
      + apply symmetry, iconj_cons_interp.
      + split.
        * intros s (Hin_a & (Hin_xs & Hin_ys)%IHxs%pe_conj_distr%Intersection_inv)%pe_conj_distr%Intersection_inv.
          split; intuition.
          apply pe_conj_distr, Intersection_intro; assumption.
        * intros s ((Hin_a & Hin_xs)%pe_conj_distr%Intersection_inv & Hin_ys)%Intersection_inv.
          apply pe_conj_distr, Intersection_intro; intuition.
          apply IHxs, pe_conj_distr, Intersection_intro; assumption.
  Qed.

  Lemma iconj_permut_inc (xs ys : list Ep) :
    PermutationA pe_equiv xs ys ->
    pe_inc (iconj xs) (iconj ys).
  Proof.
    induction 1; intuition.
    - (* Skip *)
      intros gc lc s (Hin_x2 & Hin_l2)%iconj_cons_interp%pe_conj_distr%Intersection_inv.
      apply pe_conj_distr, Intersection_intro; firstorder.
    - (* Swap *)
      intros gc lc s
             (
               Hin_x &
               (Hin_y & Hin_l)%iconj_cons_interp%pe_conj_distr%Intersection_inv
             )%iconj_cons_interp%pe_conj_distr%Intersection_inv.
      repeat apply pe_conj_distr, Intersection_intro; firstorder.
    - (* Transitivity *)
      now transitivity (iconj l₂).
  Qed.

  Corollary iconj_permut_equiv (xs ys : list Ep) :
    PermutationA pe_equiv xs ys ->
    SetoidClass.equiv (iconj xs) (iconj ys).
  Proof.
    intro Hequiv.
    apply inc_antisymm; now apply iconj_permut_inc.
  Qed.

  Lemma iconj_cons_inc (x : Ep) (xs : list Ep) :
    pe_inc (iconj xs) (iconj (x::xs)).
  Proof.
    rewrite <- iconj_cons_interp, pe_conj_comm.
    apply pe_inc_conj_mono_left.
  Qed.

  Lemma iconj_app_inc (x : Ep) (xs ys : list Ep) :
    pe_inc (iconj xs) (iconj (xs++ys)).
  Proof.
    rewrite <- iconj_app_interp.
    apply pe_inc_conj_mono_left.
  Qed.

  (** An [iconj] over a concatenated list of predicate lists is
      equivalent to an [iconj] over the [iconj] of each list. *)
  Lemma iconj_concat (xs : list (list Ep)) :
    (iconj (concat xs)) == (iconj (map iconj xs)).
  Proof.
    induction xs; cbn; intuition.
    now rewrite <- iconj_app_interp, IHxs.
  Qed.

  (** An [iconj] over a flat-map on predicate lists is
      equivalent to an [iconj] over the map of [iconj] composed with
      the original function. *)
  Corollary iconj_flat_map {A : Type} (f : A -> list Ep) (xs : list A) :
    iconj (flat_map f xs) == iconj (map (iconj ∘ f) xs).
  Proof.
    now rewrite flat_map_concat_map, iconj_concat, map_map.
  Qed.

  Lemma iconj_Forall (xs : list Ep) (gc : GCtx) (lc : LCtx) (s : S):
    pe_interp gc lc (iconj xs) s <->
    Forall (fun x => pe_interp gc lc x s) xs.
  Proof.
    induction xs; split; intuition.
    - apply pe_true_True.
    - apply pe_conj_distr, Intersection_inv in H.
      constructor; intuition.
    - inversion H.
      apply pe_conj_distr, Intersection_intro; intuition.
  Qed.

  (** A predicate expression is [pe_pure] if its truth doesn't depend on the
        shared state. *)

  Definition pe_pure (ep : Ep) :=
    forall (gc : GCtx) (lc : LCtx),
      Inhabited S (pe_interp gc lc ep) ->
      Same_set S (pe_interp gc lc ep) (Full_set S).

  (** [pe_true] is always pure. *)

  Lemma pe_true_pure : pe_pure pe_true.
  Proof.
    intros gc lc _.
    apply pe_true_unit.
  Qed.

  (** Two expressions are pure if their conjunction is.

        We don't have the converse: for example, if one side is false, the
        conjunction will be pure even if the other side isn't. *)

  Lemma pe_conj_pure (e1 e2 : Ep) :
    pe_pure e1 -> pe_pure e2 -> pe_pure (pe_conj e1 e2).
  Proof.
    intros He1_pure He2_pure gc lc Hinh.
    apply (Full_set_inc_same (A := S)).
    transitivity (Intersection S (pe_interp gc lc e1) (pe_interp gc lc e2)).
    - constructor.
      + (* e1 *)
        apply He1_pure; intuition.
        apply Included_Inhabited with (xs := pe_interp gc lc (pe_conj e1 e2)); intuition.
        rewrite pe_conj_distr.
        now intros y Hy%Intersection_inv.
      + (* e2 *)
        apply He2_pure; intuition.
        apply Included_Inhabited with (xs := pe_interp gc lc (pe_conj e1 e2)); intuition.
        rewrite pe_conj_distr.
        now intros y Hy%Intersection_inv.
    - apply pe_conj_distr.
  Qed.
End starling_logic_backend_classes_predex_facts.

Hint Resolve
     pe_true_True : backend.

Section starling_logic_backend_classes_implpredex_facts.

  (** ** Facts about [ImplPredEx] *)

  Variables
    (Ep   : Type) (* Predicate expressions *)
    (GCtx : Type) (* Global solver context *)
    (LCtx : Type) (* Local solver context *)
    (S    : Type). (* States *)

  Context
    {S_S  : Setoid S}
    {P_Ep : PredEx Ep GCtx LCtx S}
    {I_Ep : ImplPredEx Ep GCtx LCtx S}.

  Local Open Scope backend_scope.

  Lemma pe_false_empty (gc : GCtx) (lc : LCtx) :
    Same_set S
             (pe_interp gc lc pe_false)
             (Empty_set S).
  Proof.
    auto with backend sets.
  Qed.

  Lemma pe_false_False (gc : GCtx) (lc : LCtx) (s : S) :
    ~ In S (pe_interp gc lc pe_false) s.
  Proof.
    now intros Hin_false%pe_false_empty.
  Qed.

  (** [pe_not] is a negation operator for expressions. *)

  Definition pe_not : Ep -> Ep :=
    flip pe_impl pe_false.

  (** [pe_not] must negate its expression. *)

  Lemma pe_not_neg (gc : GCtx) (lc : LCtx) (ep : Ep) :
    Same_set S
             (pe_interp gc lc (pe_not ep))
             (Complement S (pe_interp gc lc ep)).
  Proof.
    split; intros s Hin_s.
    - intro Hin_ep.
      pose (Intersection_intro S _ _ s Hin_s Hin_ep) as Hconj.
      now apply pe_conj_distr, pe_impl_modus_ponens, pe_false_empty in Hconj.
    - now apply pe_impl_cond.
  Qed.

  (** We can build a modus ponens using Coq's implications, rather than
      building the conjunction inside the language. *)

  Lemma pe_impl_modus_ponens_direct (gc : GCtx) (lc : LCtx) (ep1 ep2 : Ep) (s : S) :
    In S (pe_interp gc lc (ep1 ==> ep2)) s ->
    In S (pe_interp gc lc ep1) s ->
    In S (pe_interp gc lc ep2) s.
  Proof.
    intros Hin_impl Hin_ep1.
    apply pe_impl_modus_ponens with (ep3 := ep1).
    auto with backend sets.
    now apply pe_conj_distr, Intersection_intro.
  Qed.

  Lemma pe_true_impl (ep : Ep) :
    (TT ==> ep) == ep.
  Proof.
    intros gc lc.
    split.
    - intros s Hin_impl.
      now apply pe_true_r, pe_impl_modus_ponens in Hin_impl.
    - auto with backend sets.
  Qed.

  Lemma pe_impl_tauto (ep : Ep) :
    (ep ==> ep) == TT.
  Proof.
    intros gc lc.
    auto with backend sets.
  Qed.

  (** The negation of [TT] is, also, false. *)

  Lemma pe_not_true_empty (gc : GCtx) (lc : LCtx) :
    Same_set S
             (pe_interp gc lc (pe_not TT))
             (Empty_set S).
  Proof.
    split.
    - intros s Hin_s%pe_not_neg.
      exfalso.
      auto with backend sets.
    - now intros x Hboom%Noone_in_empty.
  Qed.

  Corollary pe_not_true_false :
    pe_not TT == FF.
  Proof.
    intros gc lc.
    rewrite pe_not_true_empty.
    auto with backend sets.
  Qed.

  Lemma pe_impl_vacuous (ep : Ep) :
    (FF ==> ep) == TT.
  Proof.
    intros gc lc.
    rewrite pe_true_unit.
    apply (Full_set_inc_same (A := S)).
    intros s _.
    apply pe_impl_cond.
    now intros Hin_false%pe_false_empty.
  Qed.

 (** [pe_impl] is proper over [PredEx] equivalence. *)

  Global Instance pe_impl_equiv_Proper :
    Proper (SetoidClass.equiv ==> SetoidClass.equiv ==> SetoidClass.equiv) pe_impl.
  Proof.
    intros a b Hab c d Hcd gc lc.
    split;
      intros s Hin_ac;
      apply pe_impl_cond;
      intros Hin_b%Hab;
      apply Hcd.
    - now apply pe_impl_modus_ponens_direct with (ep1 := a).
    - now apply pe_impl_modus_ponens_direct with (ep1 := b).
  Defined.

End starling_logic_backend_classes_implpredex_facts.

Hint Resolve
     pe_false_False
  : backend.

Section starling_logic_backend_classes_relex_facts.

  Variables
    Ep   (* Predicate expressions *)
    Er   (* Relation expressions *)
    GCtx (* Global solver context *)
    LCtx (* Local solver context *)
    S    (* States *)
  : Type.

  Context
    {S_S  : Setoid S}
    {P_Ep : PredEx Ep GCtx LCtx S}
    {R_Er : RelEx  Er GCtx LCtx S}.

  Local Open Scope views_scope.
  Local Open Scope backend_scope.

  (** The identity relation turns any Hoare triple it inhabits
      into an entailment. *)

  Lemma rel_id_entailment (p q : Ep) :
    q <<= p <-> forall gc : GCtx, bvhoare gc << p >> re_id << q >>.
  Proof.
    split.
    - intros Hinc gc s s' lc Hin_p%Hinc Hcmd%re_id_id%symmetry.
      now apply pe_interp_equiv with (y := s).
    - intros Hid gc lc s Hin_p.
      now apply (Hid gc s s lc Hin_p), re_id_id.
  Qed.

  Corollary rel_id_preservation (p : Ep) (gc : GCtx) :
    bvhoare gc << p >> re_id << p >>.
  Proof.
    now apply rel_id_entailment.
  Qed.

  (** ** Contextual inclusion relation

        A [RelEx] is less than or equal to another [RelEx] modulo
        a [GCtx] [gc] and [LCtx] [lc] when it
        contains *no fewer* state pairs on those contexts. *)

  Definition re_inc_contextual (gc : GCtx) (lc : LCtx) (er1 er2 : Er) : Prop :=
    subrelation (re_interp gc lc er2) (re_interp gc lc er1).

  (** ** Inclusion relation

        A [RelEx] is less than or equal to another [RelEx] when it
        contains *no fewer* state pairs on all contexts.  (In other
        words, [<<=] is *refinement* order on [PredEx]es. *)

  Definition re_inc (er1 er2 : Er) : Prop :=
    forall (gc : GCtx) (lc : LCtx), re_inc_contextual gc lc er1 er2.

  Definition re_equiv_contextual (gc : GCtx) (lc : LCtx) (er1 er2 : Er) : Prop :=
    relation_equivalence (re_interp gc lc er1) (re_interp gc lc er2).

  Definition re_equiv (er1 er2 : Er) : Prop :=
    forall (gc : GCtx) (lc : LCtx), re_equiv_contextual gc lc er1 er2.

  (* TODO: move *)

  Definition cond_inc_contextual
             (gc : GCtx) (lc : LCtx) (x y : BVCond Ep Er) : Prop :=
    pe_inc_contextual   gc lc (bv_w y) (bv_w x)
    /\ re_inc_contextual gc lc (bv_c y) (bv_c x)
    /\ pe_inc_contextual gc lc (bv_g x) (bv_g y)
  .

  (** If [x] is a refinement of [y], then performing Hoare reasoning on
      [y] implies the same reasoning on [x]. *)

  Lemma cond_inc_contextual_strengthens (gc : GCtx) (x y : BVCond Ep Er) :
    (forall lc, cond_inc_contextual gc lc x y) ->
    bvhoare gc y ->
    bvhoare gc x.
  Proof.
    intros Hcond_inc Hhoare s s' lc Hin_wy Hin_cy.
    destruct (Hcond_inc lc) as (Hci_w & Hci_c & Hci_g).
    apply Hci_g.
    specialize (Hci_w s    Hin_wy).
    specialize (Hci_c s s' Hin_cy).
    apply (Hhoare s s' lc Hci_w Hci_c).
  Qed.

  Corollary cmd_refine_strengthens (gc : GCtx) (w g : Ep) (c c' : Er) :
    (forall lc, re_inc_contextual gc lc c c') ->
    bvhoare gc << w >> c << g >> -> bvhoare gc << w >> c' << g >>.
  Proof.
    intro Hci_c.
    apply cond_inc_contextual_strengthens; split; firstorder.
  Qed.
End starling_logic_backend_classes_relex_facts.

(** Decision procedure for reducing [pe_conj] interpretations. *)

Ltac destroy_conj :=
  intros;
  repeat
    match goal with
    | [ |- In _ (pe_interp _ _ (pe_conj _ _)) _ ] =>
      (* Eliminate Ensemble in goal *)
      unfold In
    | [ H: In _ (pe_interp _ _ (pe_conj _ _ )) _ |- _ ] =>
      (* Eliminate Ensemble in hypothesis *)
      unfold In in H
    | [ |- pe_interp _ _ (pe_conj _ _) _ ] =>
      (* Conjunction in goal *)
      apply pe_conj_distr, Intersection_intro
    | [ H: pe_interp _ _ (pe_conj _ _) _ |- _ ] =>
      (* Conjunction in hypothesis *)
      apply pe_conj_distr, Intersection_inv in H;
      destruct H
    end;
  intuition.

Notation "¬ x" := (pe_not x) (at level 30) : backend_scope.
