(** ** Removing symbols from abstract expression contexts *)

From Coq Require Import
     Classes.SetoidClass
.

From Starling Require Import
     Backend.Alpha
     Backend.AbstractExpr
     Backend.Classes
.

Set Implicit Arguments.
Set Universe Polymorphism.

Section desymbol.
  Variables
    GCtx
    LCtx
    Val
  : Type.

  Variables
    Symb
    VBop
    EBop
    VMop
    EMop
    : Set.

  Variables
    Ep
    Ev
    : Type -> Type.

  Context
    {S_Val  : Setoid Val}
    {E_Ep   : EqPredEx GCtx LCtx Ep (Val := Val)}
    {V_Ev   : ValEx Ev (Val := Val)}
    {AEC    : AbstractExprContext Symb VBop EBop VMop EMop (AP_Ep := E_Ep)}
  .

  Global Instance AbstractExprContext_NoSymbols :
    AbstractExprContext Empty_set VBop EBop VMop EMop (AP_Ep := E_Ep) :=
    {
      cemop := cemop (AbstractExprContext := AEC);
      cebop := cebop (AbstractExprContext := AEC);
      cvmop := cvmop (AbstractExprContext := AEC);
      cvbop := cvbop (AbstractExprContext := AEC);
      csymb _ s := match s with end;

      cemop_equiv_Proper := cemop_equiv_Proper (AbstractExprContext := AEC);
      cebop_equiv_Proper := cebop_equiv_Proper (AbstractExprContext := AEC);

      fmap_cemop := fmap_cemop (AbstractExprContext := AEC);
      fmap_cebop := fmap_cebop (AbstractExprContext := AEC);
      fmap_cvmop := fmap_cvmop (AbstractExprContext := AEC);
      fmap_cvbop := fmap_cvbop (AbstractExprContext := AEC);

      ctx_ind_cemop := ctx_ind_cemop (AbstractExprContext := AEC);
      ctx_ind_cebop := ctx_ind_cebop (AbstractExprContext := AEC);
      ctx_ind_cvmop := ctx_ind_cvmop (AbstractExprContext := AEC);
      ctx_ind_cvbop := ctx_ind_cvbop (AbstractExprContext := AEC);
    }.
  Proof.
    destruct s.
  Defined.

  Fixpoint symbolise {A : Type}
           (a : AbstractExpr Empty_set VBop EBop VMop EMop A) :
    AbstractExpr Symb VBop EBop VMop EMop A :=
    match a with
    | EBool b => EBool b
    | EEq x y => EEq x y
    | ECustomVBop o x y => ECustomVBop o x y
    | ECustomVMop o x   => ECustomVMop o x
    | ECustomEBop o x y => ECustomEBop o (symbolise x) (symbolise y)
    | ECustomEMop o x   => ECustomEMop o (symbolise x)
    | EConj x y => EConj (symbolise x) (symbolise y)
    | EImpl x y => EImpl (symbolise x) (symbolise y)
    | ESymb s _ => match s with end
    end.

  (** Abstract expressions constructed with this context have no symbols. *)

  Lemma guards_symbolic {A : Type}
        (a : AbstractExpr Empty_set VBop EBop VMop EMop A) :
    symbolic a = false.
  Proof.
    induction a; try easy; cbn; rewrite IHa1, IHa2; trivial.
  Qed.
End desymbol.