(** * Abstract predicate and relation expressions. *)

From Coq Require Import
     Relations
     Classes.SetoidClass
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     Utils.Ensemble.Facts
     Utils.List.Facts
     Utils.Monads
     Backend.Alpha
     Backend.Classes.

Set Implicit Arguments.
Set Universe Polymorphism.

Inductive AbstractExpr@{v}
          (Symb : Set)
          (VBop : Set)
          (EBop : Set)
          (VMop : Set)
          (EMop : Set)
          (Var : Type@{v}) : Type@{v} :=
  | EBool                  (b : bool)
  | EConj                  (x y : AbstractExpr Symb VBop EBop VMop EMop Var)
  | EImpl                  (x y : AbstractExpr Symb VBop EBop VMop EMop Var)
  | ECustomEMop (m : EMop) (x   : AbstractExpr Symb VBop EBop VMop EMop Var)
  | ECustomEBop (b : EBop) (x y : AbstractExpr Symb VBop EBop VMop EMop Var)
  | ESymb                  (s : Symb) (xs : list Var)
  | EEq                    (x y : Var)
  | ECustomVMop (m : VMop) (x   : Var)
  | ECustomVBop (m : VBop) (x y : Var).

Arguments EBool [Symb VBop EBop VMop EMop Var].
Arguments EConj [Symb VBop EBop VMop EMop Var].
Arguments EImpl [Symb VBop EBop VMop EMop Var].
Arguments ECustomEMop [Symb VBop EBop VMop EMop Var].
Arguments ECustomEBop [Symb VBop EBop VMop EMop Var].
Arguments ESymb [Symb VBop EBop VMop EMop Var].
Arguments EEq [Symb VBop EBop VMop EMop Var].
Arguments ECustomVMop [Symb VBop EBop VMop EMop Var].
Arguments ECustomVBop [Symb VBop EBop VMop EMop Var].

Section starling_logic_backend_abstractexpr.
  Variables
    Symb
    VBop
    EBop
    VMop
    EMop : Set.

  Polymorphic Universe v.

  Polymorphic Universe ep.
  Variable Ep : Type@{v} -> Type@{ep}.

  Polymorphic Universe gc.
  Variable GCtx : Type@{gc}.

  Polymorphic Universe lc.
  Variable LCtx : Type@{lc}.

  Variables Val : Type.

  Context
    {S_Val : Setoid Val}
    {AP_Ep : EqPredEx GCtx LCtx Ep (Val := Val)}.

  (* TODO: make this not necessary... *)
  Instance Setoid_Ep {Var : Type@{v}} : Setoid (Ep Var) :=
    pe_Setoid (P_Ep := ape_PredEx _).


  (** An [AbstractExpr] is [symbolic] if it contains elements from
      [Symb].  [Symbolic] is decidable, and thus we represent it as
      a Boolean. *)

  Fixpoint symbolic {T : Type@{v}} (a : AbstractExpr Symb VBop EBop VMop EMop T) : bool :=
    match a with
    | EBool _
    | EEq _ _
    | ECustomVBop _ _ _
    | ECustomVMop _ _ =>
      (* These productions contain only values, and thus can't be
         symbolic. *)
      false
    | ESymb _ _ =>
      (* Symbols are always symbolic. *)
      true
    | ECustomEMop _ x => symbolic x
    | EConj x y
    | EImpl x y
    | ECustomEBop _ x y => symbolic x || symbolic y
    end.

  Local Open Scope backend_scope.
  Local Open Scope program_scope.

  Section interpret.

    Definition ctx_independent {Var : Type@{v}} (x : Ep Var) :=
      forall (gc1 gc2 : GCtx) (lc1 lc2 : LCtx),
        Included
          (Var -> Val)
          (pe_interp gc1 lc1 x)
          (pe_interp gc2 lc2 x).

    Lemma ctx_independent_conj {Var : Type@{v}} (x y : Ep Var) :
      ctx_independent x ->
      ctx_independent y ->
      ctx_independent
        (pe_conj x y (PredEx := ape_PredEx _)).

    Proof.
      intros Hx Hy gc1 gc2 lc1 lc2 Hprev; destroy_conj.
      - eapply Hx, H.
      - eapply Hy, H0.
    Qed.

    (** An [AbstractExprContext] is a set of operations projecting the custom
        operations in an [AbstractExpr] into the concrete expression set.

        These operations must work on any possible [Var] type.  This
        restriction is needed to make the typeclass instances for
        [AbstractExpr] work later on. *)

    Class AbstractExprContext :=
      mk_AbstractExprContext
        {
          cemop (Var : Type@{v}) : EMop -> Ep   Var -> Ep Var;
          cebop (Var : Type@{v}) : EBop -> Ep   Var -> Ep Var -> Ep Var;
          cvmop (Var : Type@{v}) : VMop ->      Var -> Ep Var;
          cvbop (Var : Type@{v}) : VBop ->      Var ->    Var -> Ep Var;
          csymb (Var : Type@{v}) : Symb -> list Var -> Ep Var;

          (*
           * Laws
           *)

          cemop_equiv_Proper (Var : Type@{v}) (o : EMop) :> Proper (equiv ==> equiv) (@cemop Var o);
          cebop_equiv_Proper (Var : Type@{v}) (o : EBop) :> Proper (equiv ==> equiv ==> equiv) (@cebop Var o);

          fmap_cemop (A B : Type@{v}) (o : EMop) (x : Ep A) (f : A -> B) :
            fmap f (cemop o x) == cemop o (fmap f x);
          fmap_cebop (A B : Type@{v}) (o : EBop) (x y : Ep A) (f : A -> B) :
            fmap f (cebop o x y) == cebop o (fmap f x) (fmap f y);
          fmap_cvmop (A B : Type@{v}) (o : VMop) (x : A) (f : A -> B) :
            fmap f (cvmop o x) == cvmop o (f x);
          fmap_cvbop (A B : Type@{v}) (o : VBop) (x y : A) (f : A -> B) :
            fmap f (cvbop o x y) == cvbop o (f x) (f y);
          fmap_csymb (A B : Type@{v}) (s : Symb) (xs : list A) (f : A -> B) :
            fmap f (csymb s xs) == csymb s (List.map f xs);

          (*
           * These rules, taken together, ensure that only symbols may introduce
           * dependencies on external contexts.  This lets us easily drop
           * contexts later.
           *)

          ctx_ind_cemop (A : Type@{v}) (o : EMop) (x : Ep A) :
            ctx_independent (cemop o x) <-> ctx_independent x;

          ctx_ind_cebop (A : Type@{v}) (o : EBop) (x y : Ep A) :
            ctx_independent (cebop o x y) <-> ctx_independent x /\ ctx_independent y;

          ctx_ind_cvmop (A : Type@{v}) (o : VMop) (x : A) :
            ctx_independent (cvmop o x);

          ctx_ind_cvbop (A : Type@{v}) (o : VBop) (x y : A) :
            ctx_independent (cvbop o x y);
        }.

    Context {Ctx : AbstractExprContext}.

    Fixpoint interp_abstract {Var : Type@{v}} (x : AbstractExpr Symb VBop EBop VMop EMop Var) : Ep Var :=
      match x with
      | EBool           v => if v
                            then (TT (PredEx := ape_PredEx Var))
                            else (FF (ImplPredEx := ape_ImplPredEx Var))
      | EConj         x y => pe_conj (interp_abstract x)
                                    (interp_abstract y)
                                    (PredEx := ape_PredEx Var)
      | EImpl         x y => pe_impl (interp_abstract x)
                                    (interp_abstract y)
                                    (ImplPredEx := ape_ImplPredEx Var)
      | ECustomEMop m x   => cemop m (interp_abstract x)
      | ECustomEBop m x y => cebop m (interp_abstract x) (interp_abstract y)
      | ESymb       s xs  => csymb s xs
      | EEq           a b => ape_eq a b
      | ECustomVMop m a   => cvmop m a
      | ECustomVBop m a b => cvbop m a b
      end.

    (** If an expression has no symbols, it must be context-independent. *)

    Lemma ctx_ind_symbolic {A : Type@{v}} (x : AbstractExpr Symb VBop EBop VMop EMop A) :
      symbolic x = false -> ctx_independent (interp_abstract x).
    Proof.
      induction x; cbn;
        repeat match goal with
               | [ |- context [symbolic ?x]] => destruct (symbolic x)
               | [ H: false = false -> _ |- _] => specialize (H (reflexivity _)); cbn
               | [ |- false = false -> _] => intros _
               end;
        try easy.
      - intros gc1 gc2 lc1 lc2 s Hin_prev.
        destruct b.
        + apply pe_true_True.
        + now apply pe_false_False in Hin_prev.
      - now apply ctx_independent_conj.
      - intros gc1 gc2 lc1 lc2 s Hin_prev.
        apply pe_impl_cond.
        intros Hin_x.
        eapply IHx2, pe_impl_modus_ponens_direct.
        + apply Hin_prev.
        + eapply IHx1, Hin_x.
      - now apply ctx_ind_cemop.
      - now apply ctx_ind_cebop.
      - intros gc1 gc2 lc1 lc2 s Hin_prev%ape_eq_val_eq.
        now apply ape_eq_val_eq.
      - apply ctx_ind_cvmop.
      - apply ctx_ind_cvbop.
    Qed.
  End interpret.

  Hint Rewrite
       ape_fmap_true
       ape_fmap_false
       ape_fmap_conj
       ape_fmap_impl
       ape_fmap_eq : ape.

  Section monadic_classes.

    (** ** [Functor] for [AbstractExpr] *)

    Fixpoint aemap {A B : Type@{v}} (f : A -> B) (e : AbstractExpr Symb VBop EBop VMop EMop A)
    : AbstractExpr Symb VBop EBop VMop EMop B :=
      match e with
      | EBool         v   => EBool         v
      | EConj         x y => EConj         (aemap f x) (aemap f y)
      | EImpl         x y => EImpl         (aemap f x) (aemap f y)
      | ECustomEMop m x   => ECustomEMop m (aemap f x)
      | ECustomEBop m x y => ECustomEBop m (aemap f x) (aemap f y)
      | ESymb       s xs  => ESymb       s (List.map f xs)
      | EEq           a b => EEq           (f a) (f b)
      | ECustomVMop m a   => ECustomVMop m (f a)
      | ECustomVBop m a b => ECustomVBop m (f a) (f b)
      end.

    (** Abstract [fmap] is equivalent to concrete [fmap]. *)

    Lemma aemap_fmap {A B : Type@{v}} (f : A -> B) (e : AbstractExpr Symb VBop EBop VMop EMop A)
      {Ctx : AbstractExprContext} :
      interp_abstract (aemap f e) == fmap f (interp_abstract e).
    Proof.
      induction e; cbn; autorewrite with ape.
      - (* Booleans *)
        destruct b; symmetry.
        + apply ape_fmap_true.
        + apply ape_fmap_false.
      - (* Conjunctions *)
        now rewrite IHe1, IHe2.
      - (* Implications *)
        now rewrite IHe1, IHe2.
      - (* Cemops *)
        now rewrite fmap_cemop, IHe.
      - (* Cebops *)
        now rewrite fmap_cebop, IHe1, IHe2.
      - (* Symbols *)
        now rewrite fmap_csymb.
      - (* Equalities *)
        reflexivity.
      - (* Cvmops *)
        apply symmetry, fmap_cvmop.
      - (* Cvbops *)
        apply symmetry, fmap_cvbop.
    Qed.

    (** If we have two functions that always produce equal results,
        [aemap] over those functions on the same abstract expr produces
        equal results. *)

    Lemma aemap_ext {A B : Type@{v}} (f g : A -> B) (e : AbstractExpr Symb VBop EBop VMop EMop A) :
      (forall a : A, f a = g a) ->
      aemap f e = aemap g e.
    Proof.
      intro Hext.
      induction e; cbn; try easy; try congruence.
      induction xs; cbn; congruence.
    Qed.

    Lemma aemap_identity {A : Type@{v}} (e : AbstractExpr Symb VBop EBop VMop EMop A) : aemap id e = e.
    Proof.
      induction e; try reflexivity; cbn;
        now repeat match goal with
                   | [ |- context [ List.map id ?x ] ] => rewrite List.map_id
                   | [ H: aemap id ?v1 = ?v1 |- _ ] => rewrite H
                   end.
    Qed.

    Lemma aemap_compose {A B C} (f : A -> B) (g : B -> C) (e : AbstractExpr Symb VBop EBop VMop EMop A) :
      aemap (g ∘ f) e = (aemap g ∘ aemap f) e.
    Proof.
      induction e; try reflexivity; cbn;
        now repeat match goal with
                   | [ |- context [ List.map ?f (List.map ?g ?x) ] ] => rewrite List.map_map
                   | [ H: aemap (_ ∘ _) ?v1 = (aemap _ ∘ aemap _) ?v1 |- _ ] => rewrite H
                   end.
    Qed.

    (** [AbstractExpr]s are [Functor]s over their values. *)

    Global Instance AbstractExpr_Functor : Functor (AbstractExpr Symb VBop EBop VMop EMop) :=
      {|
        fmap := @aemap;
        fmap_identity := @aemap_identity;
        fmap_compose := @aemap_compose;
      |}.
  End monadic_classes.

  Section ex_classes.

    Context
      {ACtx : AbstractExprContext}.

    (** An [AbstractExpr], then, is a [PredEx]... *)

    Global Instance PredEx_AbstractExpr {Var}
      : PredEx (AbstractExpr Symb VBop EBop VMop EMop Var) GCtx LCtx (Var -> Val) :=
      {
        pe_true := EBool true;
        pe_conj := EConj (Var := Var);
        pe_interp gc lc := (pe_interp gc lc) ∘ (interp_abstract (Ctx := ACtx))
      }.
    Proof.
      - (* pe_true_unit *)
        apply pe_true_unit.
      - (* pe_interp_equiv *)
        intros gc lc ep.
        apply pe_interp_equiv.
      - (* pe_conj_distr *)
        intros gc lc ep1 ep2.
        apply pe_conj_distr.
    Defined.

    (** ...and an [ImplPredEx]... *)

    Global Instance ImplPredEx_AbstractExpr {Var} :
      ImplPredEx (AbstractExpr Symb VBop EBop VMop EMop Var) GCtx LCtx (Var -> Val) :=
      {
        pe_false := EBool false;
        pe_impl := EImpl (Var := Var);
      }.
    Proof.
      - (* pe_false_empty *)
        apply pe_false_empty.
      - (* pe_modus_ponens *)
        intros gc lc ep1 ep2.
        cbn.
        apply pe_impl_modus_ponens.
      - (* pe_impl_cond *)
        intros gc lc ep1 ep2 s.
        cbn.
        apply pe_impl_cond.
    Defined.

    (** ...and also an [EqPredEx]! *)

    Global Instance EqPredEx_AbstractExpr : EqPredEx GCtx LCtx (AbstractExpr Symb VBop EBop VMop EMop) (Val := Val) :=
      {
        ape_PredEx := @PredEx_AbstractExpr;
        ape_eq := EEq (EMop := EMop);
      }.
    Proof.
      - (* ape_eq_val_eq *)
        apply ape_eq_val_eq.
      - (* ape_state_fmap *)
        cbn.
        unfold compose.
        intros A B c f gc lc s.
        split.
        + intros Hin_fmap%aemap_fmap.
          now apply ape_state_fmap.
        + intros Hin_state%ape_state_fmap.
          now apply aemap_fmap.
      - (* ape_fmap_true *)
        intros A B f gc lc.
        split.
        + intros s _.
          apply pe_true_True.
        + intros s _.
          apply aemap_fmap, ape_fmap_true, pe_true_True.
      - (* ape_fmap_false *)
        intros A B f gc lc.
        split.
        + now intros s Hin%aemap_fmap%ape_fmap_false.
        + now intros s Hin_false.
      - (* ape_fmap_conj *)
        intros A B x y f gc lc.
        split.
        + intros s (Hin_x&Hin_y)%aemap_fmap%ape_fmap_conj%pe_conj_distr%Intersection_inv.
          destroy_conj;
            fold (interp_abstract (Var := B)) in *;
            now apply aemap_fmap.
        + intros s (Hin_x&Hin_y)%pe_conj_distr%Intersection_inv.
          apply aemap_fmap, ape_fmap_conj.
          destroy_conj;
            fold (interp_abstract (Var := A)) in *;
            now apply aemap_fmap.
      - (* ape_fmap_impl *)
        intros A B x y f gc lc.
        split.
        + intros s Hin_imp%aemap_fmap%ape_fmap_impl.
          apply pe_impl_cond.
          intros Hin_x%aemap_fmap.
          now eapply aemap_fmap, pe_impl_modus_ponens_direct with (ep1 := fmap f (interp_abstract x)).
        + intros s Hin_imp.
          apply pe_impl_cond.
          intros Hin_x.
          now eapply pe_impl_modus_ponens_direct with (ep1 := (interp_abstract (fmap f x))).
      - (* ape_fmap_eq *)
        intros A B x y f gc lc.
        split.
        + now intros s Hin_eq%aemap_fmap%ape_fmap_eq.
        + intros s Hin_eq%ape_fmap_eq.
          now apply aemap_fmap.
    Defined.

  End ex_classes.

  Section Forall.

    Context
      {ACtx : AbstractExprContext}.

    (** [Forall2_AE_Vars] relates two structured predicate expressions
        provided that they are structurally equal modulo variables, and
        all variables are pairwise related through [P]. *)

    Inductive Forall2_AE_Vars {V1 V2 : Type@{v}}
              (P : V1 -> V2 -> Prop) :
      AbstractExpr Symb VBop EBop VMop EMop V1 ->
      AbstractExpr Symb VBop EBop VMop EMop V2 -> Prop :=
    | Forall2_Bool (b : bool) :
        Forall2_AE_Vars P (EBool b) (EBool b)
    | Forall2_Conj x1 x2 y1 y2 :
        Forall2_AE_Vars P x1 x2 ->
        Forall2_AE_Vars P y1 y2 ->
        Forall2_AE_Vars P (EConj x1 y1) (EConj x2 y2)
    | Forall2_Impl x1 x2 y1 y2 :
        Forall2_AE_Vars P x1 x2 ->
        Forall2_AE_Vars P y1 y2 ->
        Forall2_AE_Vars P (EImpl x1 y1) (EImpl x2 y2)
    | Forall2_CustomEMop m x1 x2 :
        Forall2_AE_Vars P x1 x2 ->
        Forall2_AE_Vars P (ECustomEMop m x1) (ECustomEMop m x2)
    | Forall2_CustomEBop m x1 x2 y1 y2 :
        Forall2_AE_Vars P x1 x2 ->
        Forall2_AE_Vars P y1 y2 ->
        Forall2_AE_Vars P (ECustomEBop m x1 y1) (ECustomEBop m x2 y2)
    | Forall2_Symb s xs1 xs2 :
        List.Forall2 P xs1 xs2 ->
        Forall2_AE_Vars P (ESymb s xs1) (ESymb s xs2)
    | Forall2_Eq a1 a2 b1 b2 :
        P a1 a2 ->
        P b1 b2 ->
        Forall2_AE_Vars P (EEq a1 b1) (EEq a2 b2)
    | Forall2_CustomVMop m x1 x2 :
        P x1 x2 ->
        Forall2_AE_Vars P (ECustomVMop m x1) (ECustomVMop m x2)
    | Forall2_CustomVBop m x1 x2 y1 y2 :
        P x1 x2 ->
        P y1 y2 ->
        Forall2_AE_Vars P (ECustomVBop m x1 y1) (ECustomVBop m x2 y2).

    Global Instance Forall2_AE_Vars_Reflexive {V} (R : relation V)
      {R_R : Reflexive R} :
      Reflexive (Forall2_AE_Vars R).
    Proof.
      intro x.
      induction x; constructor; intuition.
      induction xs; intuition.
    Defined.

    Lemma Forall2_AE_Vars_refl {V} (x y : AbstractExpr Symb VBop EBop VMop EMop V) :
      Forall2_AE_Vars eq x y <-> x = y.
    Proof.
      split.
      - induction 1; subst; intuition.
        induction H; subst; intuition.
        now inversion IHForall2.
      - now intros ->.
    Qed.

    Lemma Forall2_AE_Vars_fmap {X Y X' Y'}
          (P : X -> Y -> Prop)
          (Q : X' -> Y' -> Prop)
          (f : X' -> X) (g : Y' -> Y) (x : AbstractExpr Symb VBop EBop VMop EMop X')
          (y : AbstractExpr Symb VBop EBop VMop EMop Y') :
      Forall2_AE_Vars Q x y ->
      (forall v1 v2, Q v1 v2 -> P (f v1) (g v2)) ->
      Forall2_AE_Vars P (fmap f x) (fmap g y).
    Proof.
      induction 1; intros; constructor; intuition.
      now apply Forall2_map with (Q := Q).
    Qed.
  End Forall.

End starling_logic_backend_abstractexpr.