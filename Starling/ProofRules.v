(** * Starling proof rules *)

From Coq Require Import
     Program.Basics
     Relations.Relation_Definitions
     Classes.SetoidClass (* This comes after Relations because of [equiv] *)
     Lists.List
     Sets.Constructive_sets
     Sets.Ensembles.

Require Import Starling.Expr.

From Starling Require Import
     Definer
     Reifier
     ProgramProof
     Utils.Option.Facts
     Views.Classes
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core.

Section proof_rules.
  Local Open Scope views_scope.

  Variable V: Type.
  Context {SV: Setoid V} {VV: ViewsSemigroup V}.

  Variable C: Set.
  Variable S: Set.

  Section templates.
    Definition Template := Signature C S -> Ensemble (ViewsAxiom V C).

    Definition instantiate (s: Signature C S) (t: Template) : PreInstance C S :=
      {|
        v_sig := s;
        v_axioms := t s
      |}.

    Definition axmap (xs ys: Ensemble (ViewsAxiom V C)):
      Included _ xs ys ->
      {x: ViewsAxiom V C | In _ xs x} ->
      {y: ViewsAxiom V C | In _ ys y}.
    Proof.
      intros Hincl (x & Hx).
      split with (x := x).
      apply Hincl, Hx.
    Defined.

    (** A template [t1] strengthens another template [t2] on a signature
      [s] if [t1]'s axiomatisation is included in that of [t2] over
      [s]. *)
    
    Definition strengthens (s : Signature C S) (t1 t2 : Template) :=
      Included _ (t1 s) (t2 s).

    (** If a template [t1] strengthens a template [t2], then soundness
      of [t2] implies soundness of [t1]. *)
    
    Corollary instantiate_sound (s: Signature C S) (t1 t2: Template):
      strengthens s t1 t2 ->
      axiom_sound (instantiate s t2) ->
      axiom_sound (instantiate s t1).
    Proof.
      intros Hincl Has_t2 (ax & Hax_t1).
      pose (Hincl _ Hax_t1) as Hax_t2.
      simpl.
      apply (Has_t2 (exist _ _ Hax_t2)).
    Qed.

    Definition partial_correctness: Template :=
      fun (s : Signature C S) (a : ViewsAxiom V C) =>
        vhoare s a.(cmd) a.(pre) a.(post).
    
    (** [lift_ni] lifts a non-interference template into a semigroup
      compatible template. *)
    
    Definition lift_ni (t : Template) : Template :=
      fun (s: Signature C S) (a: ViewsAxiom V C) =>
        partial_correctness s a /\ t s a.

    (** Template strengthening distributes over [lift_ni]. *)
    
    Lemma lift_ni_strengthens (s: Signature C S) (t1 t2: Template) :
      strengthens s t1 t2 ->
      strengthens s (lift_ni t1) (lift_ni t2).
    Proof.
      intros Hstr sig (Hin_t1_pc & Hin_t2_ni).
      split.
      - exact Hin_t1_pc.
      - apply Hstr, Hin_t2_ni.
    Qed.
  End templates.
  
  Section proof_rules_free.
    (** [free_template_ni] is the non-interference part of the free
        template. *)
    
    Definition free_template_ni : Template :=
      fun s a => forall f, vhoare s a.(cmd) (a.(pre) * f) (a.(post) * f).

    (** [free_template] is the full free template. *)
    
    Definition free_template : Template := lift_ni free_template_ni.

    (** Any instance generated using [free_template] is axiom-sound. *)
    
    Lemma free_template_axiom_sound (sig : Signature C S):
      axiom_sound (instantiate sig free_template).
    Proof.
      (* Axiom soundness. *)
      intros (a & (Ha_pc & Ha_ni)).
      split.
      - (* p -c-> q *)
        intros s' (s & Hlhs_s & Hlhs_s').
        apply Ha_pc.
        exists s.
        split; assumption.
      - (* p*v -c-> q*v *)
        intros v s' (s & Hlhs_s & Hlhs_s').
        apply Ha_ni.
        exists s.
        split; assumption.
    Defined.

    (** [free_instance], for a signature [sig], is the sound views
        instance over the free template. *)
    
    Definition free_instance (sig : Signature C S) : ViewsInstance C S :=
      exist _
            (instantiate sig free_template)
            (free_template_axiom_sound sig).

    (** All templates [t] that are axiom-sound for a given signature
        [sig] strengthen the free template. *)
    
    Theorem free_template_maximal (sig: Signature C S) (t: Template):
      axiom_sound (instantiate sig t) ->
      strengthens sig t free_template.
    Proof.
      intros Haxsound a Hin_a_t.
      unfold free_template, lift_ni, In in *.
      destruct (Haxsound (exist _ a Hin_a_t)) as (Hpc & Hni).
      split.
      - apply Hpc.
      - intro v.
        apply Hni.
    Qed.
  End proof_rules_free.

  Section proof_rules_adjoint.
    Context
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V}.

    (** [adjoint_template_ni] is the non-interference part of the free
        template. *)
    
    Definition adjoint_template_ni: Template :=
      fun s a =>
        forall g,
          vhoare s a.(cmd) (a.(pre) * (g \ a.(post))) g.

    (** [adjoint_template] is the full adjoint template. *)
    
    Definition adjoint_template: Template := lift_ni adjoint_template_ni.    

    (** [adjoint_compat] is the adjoint-template compatibility
        requirement. *)

    Definition adjoint_compat (sig : Signature C S) :=
      forall x y z : V, rinc sig (x * y) (x * (z * y \ z)).

    (** An [AdjointCompatSignature] is a signature with adjoint compatibility. *)
    
    Definition AdjointCompatSignature := {sig : Signature C S | adjoint_compat sig}.

    (** One way to prove this side-condition is when we have [reifier_inc]. *)

    Lemma adjoint_compat_reifier_inc (sig : Signature C S):
      reifier_inc (sig_reifier sig) -> adjoint_compat sig.
    Proof.
      intros Hinc x y z e.
      refine (Hinc (x * y) _ _ _).
      apply inc_dot_right, sub_adjoint, reflexivity.
    Qed.

    (** Another way is when we have a [FullView]. *)

    Lemma adjoint_compat_FullView {FV : FullView V} (sig : Signature C S):
      adjoint_compat sig.
    Proof.
      intros x y z.
      apply (proj2_sig (sig_reifier sig) (x * (z * y \ z))), equiv_dot_right, symmetry.
      setoid_rewrite dot_comm.
      apply dot_sub_cancel.
    Qed.

    (** If [adjoint_compat] holds on a signature, the adjoint NI template
        strengthens the free template. *)
    
    Lemma adjoint_strengthens_free_ni (sig: AdjointCompatSignature):
      strengthens (proj1_sig sig) adjoint_template_ni free_template_ni.
    Proof.
      intros [p c q] Hadj f.
      apply vhoare_cons_p with (hp' := p * ((q * f) \ q)).
      - destruct sig as (sig & Hadj_compat).
        apply Hadj_compat.
      - apply Hadj.
    Qed.
    
    (** If [adjoint_compat] holds on a signature, the adjoint template
        strengthens the free template. *)
    
    Corollary adjoint_strengthens_free (sig: AdjointCompatSignature):
      strengthens (proj1_sig sig) adjoint_template free_template.
    Proof.
      apply lift_ni_strengthens, adjoint_strengthens_free_ni.
    Qed.

    Corollary adjoint_strengthens_free_reifier_inc_ni (sig: Signature C S):
      reifier_inc (sig_reifier sig) ->
      strengthens sig adjoint_template_ni free_template_ni.
    Proof.
      intro Hinc.
      pose (asig := exist adjoint_compat sig (adjoint_compat_reifier_inc sig Hinc)).
      replace sig with (proj1_sig asig).
      - apply adjoint_strengthens_free_ni.
      - reflexivity.
    Qed.

    Corollary adjoint_strengthens_free_FullView_ni {FV: FullView V} (sig: Signature C S):
      strengthens sig adjoint_template_ni free_template_ni.
    Proof.
      intro Hinc.
      pose (asig := exist adjoint_compat sig (adjoint_compat_FullView sig)).
      replace sig with (proj1_sig asig).
      - apply adjoint_strengthens_free_ni.
      - reflexivity.
    Qed.    
    
    Corollary adjoint_strengthens_free_reifier_inc (sig: Signature C S):
      reifier_inc (sig_reifier sig) ->
      strengthens sig adjoint_template free_template.
    Proof.
      intro Hinc.
      apply lift_ni_strengthens, adjoint_strengthens_free_reifier_inc_ni, Hinc.
    Qed.

    Corollary adjoint_strengthens_free_FullView {FV: FullView V} (sig: Signature C S) :
      strengthens sig adjoint_template free_template.
    Proof.
      apply lift_ni_strengthens, adjoint_strengthens_free_FullView_ni.
    Qed.    

    (** If we have a [FullView] and [reifier_inc], the free template strengthens the
        adjoint template (ie, they are equivalent). *)

    Lemma free_strengthens_adjoint_FullView_inc {FV: FullView V} (sig: Signature C S):
      reifier_inc (sig_reifier sig) ->
      strengthens sig free_template adjoint_template.
    Proof.
      intro Hinc.
      apply lift_ni_strengthens.
      intros [p c q] Hin_free g.
      apply vhoare_cons_q with (hq' := q * (g \ q)).
      - intro s.
        apply Hinc, sub_adjoint_back.
        reflexivity.
      - apply Hin_free.
    Qed.
  End proof_rules_adjoint.

  (** Defining-view template *)
  
  Section proof_rules_defining_view.
    Context
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V}.

    Definition Definer: Type := V -> option (Ensemble S).

    Definition dfn_lift (dfn: Definer) (v: V): Ensemble S :=
      maybe (Full_set S) id (dfn v).

    Definition SubviewsOf (v: V): Type := { u: V | u <<= v }.

    Definition lift_SubviewsOf (v: V): SubviewsOf v.
    Proof.
      refine (exist (flip inc v) v (reflexivity _)).
    Defined.

    Definition has_def (dfn: Definer) (v: V): Prop := exists d, dfn v = Some d.
    
    Definition DefiningViews (dfn: Definer): Type := {v: V | has_def dfn v}.

    Lemma defining_views_dichotomy (dfn: Definer) (v: V):
      (exists (v': DefiningViews dfn), proj1_sig v' = v)
      \/
      (dfn v = None).
    Proof.
      destruct (dfn v) as [d|] eqn:Hd.
      - left.
        assert (Hdfne: exists d, dfn v = Some d)
          by (exists d; assumption).
        exists (exist _ v Hdfne).
        reflexivity.
      - right.
        reflexivity.
    Qed.
    
    (** [dreify] is the definer reification over [dfn_lift]. *)
    
    Definition dreify (dfn: Definer) (v: V): Ensemble S :=
      fun s => forall (u: SubviewsOf v), In S (dfn_lift dfn (proj1_sig u)) s.

    Lemma dreify_eqv (dfn: Definer): Proper (equiv ==> Same_set S) (dreify dfn).
    Proof.
      intros x y Heqv_xy.
      split; intros s Hin (u & Hu).
      - assert (Hux: u <<= x).
        {
          transitivity y.
          - apply Hu.
          - apply equiv_inc, symmetry, Heqv_xy.
        }
        apply (Hin (exist _ u Hux)).
      - assert (Huy: u <<= y).
        {
          transitivity x.
          - apply Hu.
          - apply equiv_inc, Heqv_xy.
        }
        apply (Hin (exist _ u Huy)).
    Qed.

    (** If a subview [u] of a view [v] has a definition, then any state
        in the reification of [v] satisfies that definition. *)
    
    Lemma dreify_single_def_subview (dfn: Definer) (v: V) (u: SubviewsOf v) (def: Ensemble S):
      dfn (proj1_sig u) = Some def -> Included _ (dreify dfn v) def.
    Proof.
      intros Hdef s Hin_reify_s.
      specialize (Hin_reify_s u).
      unfold dfn_lift in Hin_reify_s.
      rewrite Hdef in Hin_reify_s.
      exact Hin_reify_s.
    Qed.

    (** If a view [v] has a definition, then any state
        in the reification of [v] satisfies that definition. *)
    
    Corollary dreify_single_def (dfn: Definer) (v: V) (def: Ensemble S):
      dfn v = Some def -> Included _ (dreify dfn v) def.
    Proof.
      replace v with (proj1_sig (lift_SubviewsOf v)).
      - apply dreify_single_def_subview.
      - reflexivity.
    Qed.
      
    Definition dreifier (dfn: Definer): Reifier S := exist _ (dreify dfn) (dreify_eqv dfn).

    Lemma dreifier_inc (dfn: Definer): reifier_inc (dreifier dfn).
    Proof.
      intros x y s Hinc_xy Hreify_y (u & Huy).
      refine (Hreify_y (exist _ u _)).
      transitivity y; assumption.
    Qed.

    (** [sig_uses_definer] tells us that the signature [sig]'s reifier
        is equivalent to a defining reifier using [dfn]. *)

    Definition sig_uses_definer (sig : Signature C S) (dfn : Definer) :=
      forall (v : V), Same_set S
                          (proj1_sig (dreifier dfn) v)
                          (proj1_sig (sig.(sig_reifier)) v).    

    
    (* Since [dreifier] has [reifier_inc], any signature using it is
       adjoint-compatible. *)

    Lemma mk_definer_sig_adjoint_compat (sig : Signature C S) (dfn : Definer):
      sig_uses_definer sig dfn ->
      adjoint_compat sig.
    Proof.
      intros Heq.
      apply adjoint_compat_reifier_inc.
      intros x y Hinc.
      etransitivity.
      - apply Heq.
      - etransitivity.
        + apply dreifier_inc, Hinc.
        + apply Heq.
    Qed.

    (** [defining_views_template_ni] is the non-interference part of
        the defining-views template. *)
    
    Definition defining_views_template_ni
               (d : Definer) :
      Template :=
      fun s a =>
        forall (g: DefiningViews d),
          shoare
                 s
                 a.(cmd)
                 (dreify d (a.(pre) * ((proj1_sig g) \ a.(post))))
                 (dfn_lift d (proj1_sig g)).
        
    Definition defining_views_template (d : Definer) : Template :=
      lift_ni (defining_views_template_ni d).

    Lemma defining_views_ni_strengthens_adjoint_ni
          (dfn : Definer)
          (sig : Signature C S) :
      sig_uses_definer sig dfn ->
      strengthens sig
                  (defining_views_template_ni dfn)
                  adjoint_template_ni.
    Proof.
      intros Hreify [p c q] Hin_dv g s' (s & Hs & Htrans).
      apply Hreify.
      intros (u & Hu).
      unfold dfn_lift.
      simpl.
      destruct (defining_views_dichotomy dfn u) as [(ud & Hud)| ->].
      - specialize (Hin_dv ud s').
        simpl in Hin_dv.
        unfold dfn_lift in Hin_dv.
        destruct (proj2_sig ud) as (d & Hd).
        rewrite Hud, Hd in *.
        apply Hin_dv.
        exists s.
        split.
        + eapply dreifier_inc.
          * apply inc_dot_proper.
            -- reflexivity.
            -- apply inc_sub_congr, Hu.
          * apply Hreify, Hs.
        + apply Htrans.
      - easy.
    Qed.

    Lemma defining_views_strengthens_adjoint
          (dfn : Definer)
          (sig : Signature C S) :
      sig_uses_definer sig dfn ->
      strengthens sig (defining_views_template dfn) adjoint_template.
    Proof.
      intro Hsud.
      apply lift_ni_strengthens, defining_views_ni_strengthens_adjoint_ni, Hsud.
    Qed.
  End proof_rules_defining_view.
  
  Section proof_rules_monoid.
    (** If the non-interference part of a signature implies the partial
        correctness part, then that part alone strengthens the whole. *)

    Lemma redundant_pc_strengthen
          (sig : Signature C S) (t : Template) :
      strengthens sig t partial_correctness ->
      strengthens sig t (lift_ni t).
    Proof.
      intros Hstr_t_pc a Hin_t.
      split.
      - apply Hstr_t_pc, Hin_t.
      - apply Hin_t.
    Qed.

    (** Since many templates' non-interference parts have this property
        for monoids, we can remove the partial correctness part when we
        have a monoid. *)

    (** For the free template, the non-interference part entails partial
        correctness when the context is [1]. *)
    
    Theorem free_monoid_reduce
            {MV: ViewsMonoid V}
            (sig: Signature C S):
      strengthens sig free_template_ni free_template.
    Proof.
      apply redundant_pc_strengthen.
      intros [p c q] Hin_free.
      apply vhoare_cons with (hp' := p * 1) (hq' := q * 1).
      - apply (proj2_sig (sig_reifier sig)), symmetry, dot_one_r.
      - apply (proj2_sig (sig_reifier sig)), dot_one_r.
      - apply Hin_free.
    Qed.

    (** For the adjoint template, non-interference entails partial correctness
        when the goal is the postcondition.  This property relies on the
        use of a monoid indirectly. *)
    
    Theorem adjoint_monoid_reduce
            {MV: ViewsMonoid V} {OV: OrderedViewsSemigroup V} {AV: AdjView V}
            (sig: Signature C S):
      strengthens sig adjoint_template_ni adjoint_template.
    Proof.
      apply redundant_pc_strengthen.
      intros [p c q] Hin_adj.
      apply vhoare_cons_p with (hp' := p * (q \ q)).
      - apply (proj2_sig (sig_reifier sig)).
        transitivity (p * 1).
        + apply equiv_dot_proper.
          * reflexivity.
          * apply sub_self.
        + apply symmetry, dot_one_r.
      - apply Hin_adj.
    Qed.

    (** For the defining-views template, non-interference implies
        partial correctness when applied to every defining-view
        goal up to and including the postcondition.  Unlike the earlier
        two templates, this needs us to apply the template to several
        contexts! *)
    
    Theorem defining_views_monoid_reduce
            {MV: ViewsMonoid V} {OV: OrderedViewsSemigroup V} {AV: AdjView V}
            (sig : Signature C S)
            (dfn : Definer) :
      sig_uses_definer sig dfn ->      
      strengthens sig
                  (defining_views_template_ni dfn)
                  (defining_views_template dfn).
    Proof.
      intro Hsud.
      apply redundant_pc_strengthen.
      intros [p c q] Hin_dv s' (s & Hs & Htrans).
      apply Hsud.
      apply Hsud in Hs.
      intros (u & Hu).
      unfold dfn_lift.
      simpl in *.
      destruct (defining_views_dichotomy dfn u) as [(ud & Hud)| ->].
      - specialize (Hin_dv ud s').
        simpl in Hin_dv.
        unfold dfn_lift in Hin_dv.
        destruct (proj2_sig ud) as (d & Hd).
        rewrite Hud, Hd in *.
        apply Hin_dv.
        exists s.
        split.
        + refine (dreifier_inc _ p _ _ s Hs).
          transitivity (p * 1).
          * apply equiv_inc, symmetry, dot_one_r.
          * apply inc_dot_proper.
            -- reflexivity.
            -- apply sub_adjoint.
               transitivity q.
               ++ apply Hu.
               ++ apply equiv_inc, dot_one_r.
        + apply Htrans.
      - easy.
    Qed.    
  End proof_rules_monoid.
End proof_rules.
