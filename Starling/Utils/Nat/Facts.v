(** * Properties about natural numbers used elsewhere 

    All of these facts are fairly straightforward inequality results.
    As such, we use the [omega] tactic to prove them automatically. *)

Require Import
        Arith
        Omega.

(* Note: a lot of these lemmata exist only because they were used in a much
   longer proof chain originally, but have been made obsolete by replacing the
   proofs with omega.  We keep them here in case they're useful elsewhere
   on their own. *)

(*
 * Proving (a + b) - c = (a - c) + (b - (c - a)).
 *)

Lemma minus_of_pred_lt (a b : nat) :
  0 < b -> b < a -> a - Nat.pred b = S(a - b).
Proof.
  omega.
Qed.

Lemma minus_of_pred (a b : nat) :
  0 < b -> b <= a -> a - Nat.pred b = S(a - b).
Proof.
  omega.
Qed.

(* Strictest form of the minus-of-minus rule. *)
Lemma minus_of_minus_strict (a b c : nat) :
   b < c + a -> 0 < a -> c < b -> a - (b - c) = (a + c) - b.
Proof.
  omega.
Qed.

(* Weakening of minus_of_minus_strict such that a can be 0. *)
Lemma minus_of_minus_strict_a0 (a b c : nat) :
   b < c + a -> c < b -> a - (b - c) = (a + c) - b.
Proof.
  omega.
Qed.

(* Generalise minus_of_minus_strict_a0 to the case where b >= c. *)
Lemma minus_of_minus_weak (a b c : nat):
   b < c + a -> c <= b -> a - (b - c) = (a + c) - b.
Proof.
  omega.
Qed.

(* Generalise minus_of_minus_weak to the case where b <= c + a. *)
Lemma minus_of_minus (a b c : nat) :
  b <= c + a -> c <= b -> a - (b - c) = (a + c) - b.
Proof.
  omega.  
Qed.

(* Now we can start proving the actual theorem by case analysis. *)

(* Case of minus_of_plus where a + b <= c. *)
Lemma minus_of_plus_ablc (a b c : nat) :
  a + b <= c -> (a + b) - c = (a - c) + (b - (c - a)).
Proof.
  omega.
Qed.

(* Case of minus_of_plus where a + b > c. and c < a. *)
Lemma minus_of_plus_abgc_cla (a b c : nat) :
  c < a + b -> c < a -> (a + b) - c = (a - c) + (b - (c - a)).
Proof.
  omega.
Qed.

(* Case of minus_of_plus where a + b > c. and c >= a. *)
Lemma minus_of_plus_abgc_cga (a b c : nat) :
  c < a + b -> a <= c -> (a + b) - c = (a - c) + (b - (c - a)).
Proof.
  omega.
Qed.

(* Joining the above two cases. *)
Lemma minus_of_plus_abgc (a b c : nat) :
  c < a + b -> (a + b) - c = (a - c) + (b - (c - a)).
Proof.
  omega.
Qed.

(* Joining all heretofore proved cases. *)
Theorem minus_of_plus (a b c : nat) :
   (a + b) - c = (a - c) + (b - (c - a)).
Proof.
  omega.
Qed.