(** * Utilities for Cartesian products *)

From Coq Require Import
     Relations.Relations
     Classes.RelationClasses     
     Classes.RelationPairs
     Program.Basics.

Set Implicit Arguments.

Chapter product_utils.

(** [prodmap] maps two functions over a pair. *)

Definition prodmap (A B A' B' : Type)
           (fa : A -> A')
           (fb : B -> B')
           '((a, b) : A * B) : A' * B' :=
  (fa a, fb b).

Lemma prodmap_id (A B : Type) (ab : A * B) :
  prodmap id id ab = ab.
Proof.
  destruct ab; reflexivity.
Qed.

(** We can discard the [snd] of a [prodmap] if we only want the [fst]. *)

Lemma prodmap_fst (A B A' B' : Type)
      (fa : A -> A')
      (fb : B -> B')
      (ab : A * B) :
  fst (prodmap fa fb ab) = fa (fst ab).
Proof.
  destruct ab as (a & b).
  reflexivity.
Qed.

(** We can discard the [fst] of a [prodmap] if we only want the [snd]. *)

Lemma prodmap_snd (A B A' B' : Type)
      (fa : A -> A')
      (fb : B -> B')
      (ab : A * B) :
  snd (prodmap fa fb ab) = fb (snd ab).
Proof.
  destruct ab as (a & b).
  reflexivity.
Qed.

(** [prodmap2] maps two binary operations over two pairs. *)

Definition prodmap2 (A1 A2 A3 B1 B2 B3 : Type)
           (fa : A1 -> A2 -> A3)
           (fb : B1 -> B2 -> B3)
           '((a1, b1) : A1 * B1)
           '((a2, b2) : A2 * B2)
           : A3 * B3 :=
  (fa a1 a2, fb b1 b2).

(** These follow trivially from the instances in [Coq.Classes.RelationPairs]. *)

Global Instance RelProd_PreOrder (A B : Type)
         (fa : relation A) (fb : relation B)
         `{PreOrder A fa, PreOrder B fb}
  : PreOrder (RelProd fa fb) := {}.

Global Instance RelProd_Equivalence (A B : Type)
         {fa : relation A} {fb : relation B}
         `{Equivalence A fa, Equivalence B fb}
  : Equivalence (RelProd fa fb) := {}.

End product_utils.