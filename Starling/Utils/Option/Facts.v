(*
 * Starling extensions to, and proofs for, the 'option' type.
 *)

From Coq Require Import
     Program.Basics
     Relations
     Classes.SetoidClass     
     Lists.List.

From Starling Require Import
     Utils.Monads.

Local Open Scope program_scope.

(** * Option combinators

    We first define some helper combinators for option. *)

(** 'bind' chains two partial computations. *)

Definition bind {A : Type} {B: Type} (f : A -> option B) (x : option A) : option B :=
    match x with
    | Some xi => f xi
    | None => None
    end.

(** 'map2' lifts a binary function over two partial results. *)

Definition map2 {A : Type} {B: Type} {C: Type}
  (f : A -> B -> C) (x : option A) (y : option B) 
  : option C :=
    match x, y with
    | Some xi, Some yi => Some (f xi yi)
    | _, _ => None
    end.

(** 'bind2' binds a binary function over two partial results. *)

Definition bind2 {A : Type} {B: Type} {C: Type}
  (f : A -> B -> option C) (x : option A) (y : option B) 
  : option C :=
    match x, y with
    | Some xi, Some yi => f xi yi
    | _, _ => None
    end.

(** 'collect' lifts a list of partial results to a partial list result. *)

Fixpoint collect {A: Type} (xs: list (option A)) : option (list A) :=
  match xs with
    | nil => Some nil
    | (Some x) :: xs => option_map (fun ys => x :: ys) (collect xs)
    | _ => None
  end.

(** 'maybe' executes a function over 'Some', or returns a default given
    'None'. *)

Definition maybe {A B : Type} (d : B) (f : A -> B) (o : option A) : B :=
  match o with
  | Some x => f x
  | None   => d
  end.

(** [bindR] binds a relation over two partial results.
    The resulting relation is [True] iff both results are [Some], and
    the original relation holds over their extraction. *)

Definition bindR {A : Type} (R : relation A) : relation (option A) :=
  fun x y => maybe False id (map2 R x y).

Section bindR_facts.

  Variables
    (A : Type)
    (R : relation A).
  
  Lemma bindR_Some (x y : A) :
    bindR R (Some x) (Some y) <-> R x y.
  Proof.
    intuition.
  Qed.
End bindR_facts.

(** * Option proofs *)

(** If an option is not 'None', it must be 'Some' something. *)

Theorem not_none_is_some {A} (n : option A) :
  n <> None -> exists sn, n = Some sn.
Proof.
  intro H.
  destruct n.
  - exists a; reflexivity.
  - contradiction H; reflexivity.
Qed.

(** If an option is 'Some' something, it is not 'None'. *)

Theorem some_is_not_none {A} (n : option A) :
  (exists sn, n = Some sn) -> n <> None.
Proof.
  destruct n; intros [b HN].
  - discriminate.
  - discriminate HN.
Qed.

(** If an option is not 'Some' something, it must be 'None'. *)

Theorem not_some_is_none {A} (n : option A) :
  (forall sn, n <> Some sn) -> n = None.
Proof.
  intro H.
  destruct n.
  - contradiction H with (sn := a); reflexivity.
  - reflexivity.
Qed.

(** If an option is 'None', it cannot be 'Some' anything. *)

Theorem none_is_not_some {A} (n : option A) :
  n = None -> (forall sn, n <> Some sn).
Proof.
  intros H sn.
  destruct n.
  - rewrite H; discriminate.
  - discriminate.
Qed.

Section bind2_facts.
  Context
    {A B C: Type}.

  Variable
    (f: A -> B -> option C).
            
  (** The result of binding over two 'Some's is the result of the two
    bindings.

    This is trivial, but stating it this way is useful. *)

  Lemma bind2_some (a: A) (b: B):
    bind2 f (Some a) (Some b) = f a b.
  Proof.
    reflexivity.
  Qed.

  (** We can rephrase the above in a more ergonomic way. *)

  Corollary bind2_some_gen (ao : option A) (bo : option B):
    forall a b c,
      ao = Some a -> bo = Some b -> f a b = c -> bind2 f ao bo = c.
  Proof.
    intros a b c Ha Hb Hc.
    rewrite Ha, Hb, <- Hc.
    apply bind2_some.
  Qed.

  (** If a [bind2] succeeded, the ingredients must themselves be 'Some'. *)

  Lemma bind2_some_back (fab : C) (a : option A) (b : option B):
    bind2 f a b = Some fab ->
    exists (ao : A) (bo : B), a = Some ao /\ b = Some bo.
  Proof.
    intro H.
    destruct a, b; try discriminate H.
    exists a, b; split; reflexivity.
  Qed.

  (** If we know that a [bind2] succeeded, we can recover its actual value. *)

  Lemma bind2_some_both (a : option A) (b : option B) (c : C):
    bind2 f a b = Some c ->
    exists (ao : A) (bo : B),
      a = Some ao /\ b = Some bo /\ Some c = f ao bo.
  Proof.
    intros Hc.
    pose (bind2_some_back c a b Hc) as Hsb.
    destruct Hsb as [ao [bo [Hao Hbo]]].
    exists ao, bo.
    repeat split; try assumption.
    rewrite Hao, Hbo in Hc.
    pose (bind2_some ao bo) as Hfab.
    rewrite Hfab in Hc.
    symmetry.
    inversion Hc; reflexivity.
  Qed.
End bind2_facts.

(** The result of mapping over two 'Some's is the 'Some' of the map.

    This is trivial, but stating it this way is useful. *)

Theorem map2_some {A} {B} {C} (a : A) (b : B) (f : A -> B -> C) :
   map2 f (Some a) (Some b) = Some (f a b).
Proof.
  reflexivity.
Qed.

(** We can rephrase the above in a more ergonomic way. *)

Corollary map2_some_gen {A} {B} {C}
          (ao : option A) (bo : option B) (f : A -> B -> C) :
  forall a b c,
    ao = Some a -> bo = Some b -> f a b = c -> map2 f ao bo = Some c.
Proof.
  intros a b c Ha Hb Hc.
  rewrite Ha, Hb, <- Hc.
  apply map2_some.
Qed.

(** If a 'map2' succeeded, the ingredients must themselves be 'Some'. *)

Theorem map2_some_back {A} {B} {C}
  (fab : C) (a : option A) (b : option B) (f : A -> B -> C) :
  map2 f a b = Some fab ->
  exists (ao : A) (bo : B), a = Some ao /\ b = Some bo.
Proof.
  intro H.
  destruct a, b; try discriminate H.
  exists a, b; split; reflexivity.
Qed.

(** If we know that a [map2] succeeded, we can recover its actual value. *)

Theorem map2_some_both {A} {B} {C} (a : option A) (b : option B) (c : C) (f : A -> B -> C) :
   map2 f a b = Some c ->
   exists (ao : A) (bo : B),
     a = Some ao /\ b = Some bo /\ c = f ao bo.
Proof.
  intros Hc.
  pose (map2_some_back c a b f Hc) as Hsb.
  destruct Hsb as [ao [bo [Hao Hbo]]].
  exists ao, bo.
  repeat split; try assumption.
  rewrite Hao, Hbo in Hc.
  pose (map2_some ao bo f) as Hfab.
  rewrite Hfab in Hc.
  symmetry.
  inversion Hc; reflexivity.
Qed.

(** If either ingredient in a 'map2' is None, the result is None. *)

Theorem map2_none {A} {B} (a b : option A) (f : A -> A -> B) :
  a = None \/ b = None -> map2 f a b = None.
Proof.
  intro H.
  destruct H; rewrite H.
  - reflexivity.
  - destruct a; reflexivity.
Qed.

(** If a 'map2' failed, one of the ingredients must be 'None'. *)

Lemma map2_none_back {A} {B} (a b : option A) (f : A -> A -> B):
  map2 f a b = None -> a = None \/ b = None.
Proof.
  intro H.
  unfold map2 in H.
  destruct a, b; try discriminate H.
  - right; reflexivity.
  - left; reflexivity.
  - right; reflexivity.
Qed.

(** [map2] on a commutative function preserves commutativity. *)

Lemma map2_comm {A} {B} (a b : option A) (f : A -> A -> B):
  (forall x y, f x y = f y x) ->
  map2 f a b = map2 f b a.
Proof.
  intro Hcomm.
  (* Chop down the state space a bit by doing the option case analysis
     at this level. *)
  case_eq (map2 f b a).
  - intros c Hmba.
    (* First, show that [Hmba] implies [a] and [b] are defined. *)
    pose (map2_some_both b a c f Hmba) as Habdef.
    destruct Habdef as [bo [ao [Hbo [Hao Hc]]]].
    (* Then, use commutativity to flip the definition of [c]. *)
    rewrite Hcomm in Hc.
    symmetry in Hc.
    (* Then, use the definitions to justify the reverse being defined. *)
    apply (map2_some_gen a b f ao bo c Hao Hbo Hc).
  - intro Hmba.
    (* We just need to show that one of the parameters is [None]. *)
    apply map2_none.
    pose (map2_none_back b a f Hmba) as Habdef.
    destruct Habdef as [Hbn|Han].
    + right; exact Hbn.
    + left; exact Han.
Qed.

(** [map2] on an associative function preserves associativity. *)

Lemma map2_assoc {A} (a b c : option A) (f : A -> A -> A):
  (forall x y z, f x (f y z) = f (f x y) z) ->
  map2 f a (map2 f b c) = map2 f (map2 f a b) c.
Proof.
  intro Hassoc.
  (* Similar process to [map2_comm]. *)
  case_eq (map2 f (map2 f a b) c).
  - intros abco Hml.
    (* Show that [Hml] implies [map2 f a b] and [c] are defined. *)
    pose (map2_some_both (map2 f a b) c abco f Hml) as Hldef.
    destruct Hldef as [abo [co [Hmab [Hc Habc]]]].
    (* We can now back-form that [a] and [b] are defined. *)
    pose (map2_some_both a b abo f Hmab) as Habdef.
    destruct Habdef as [ao [bo [Ha [Hb Habo]]]].
    (* And now figure out that [map2 f b c] is defined. *)
    pose (map2_some bo co f) as Hmbc.
    rewrite <- Hb, <- Hc in Hmbc.
    (* Now we can rewrite the goal to expand out all of the definitions. *)
    rewrite Ha, Hmbc, Habc, Habo.
    rewrite map2_some.
    f_equal.
    apply Hassoc.
  - (* As for [map2_comm], show that one of the parameters is [None]. *)
    intro Hml.
    apply map2_none.
    pose (map2_none_back (map2 f a b) c f Hml) as Hmldef.
    destruct Hmldef as [Habn|Hcn].
    + (* This time, each case is a little more complicated.
         We have to destruct inside as well as outside. *)
      pose (map2_none_back a b f Habn) as Habndef.
      destruct Habndef as [Han|Hbn].
      * left; exact Han.
      * right.
        apply map2_none.
        left; exact Hbn.
    + right.
      apply map2_none.
      right; exact Hcn.
Qed.

(** If [A] equality is decidable, [option A] equality is. *)

Lemma option_eq_dec {A} :
  (forall a b : A, { a = b } + { a <> b }) ->
  (forall a b : option A, { a = b } + { a <> b }).
Proof.
  intros Hdec a b.
  decide equality.
Qed.

(** We can destruct an option into a [sumor] where each
    leg carries information about its relation to the original [a]. *)

Definition option_sumor {A} (a : option A) :
  { x : A | a = Some x } + { a = None }.
Proof.
  destruct a; eauto.
Defined.

Section option_setoid.
  Context
    {A : Type}
    {S_A : Setoid A}.

  Definition option_equiv (x y : option A) : Prop :=
    match x, y with
    | Some x', Some y' => x' == y'
    | None   , None    => True
    | _      , _       => False
    end.

  Global Instance option_equiv_Reflexive : Reflexive option_equiv.
  Proof.
    intros [|]; now cbn.
  Defined.

  Global Instance option_equiv_Symmetric : Symmetric option_equiv.
  Proof.
    intros [|] [|]; now cbn.
  Defined.

  Global Instance option_equiv_Transitive : Transitive option_equiv.
  Proof.
    intros [|] [|] [|]; cbn; intuition.
    now rewrite H.
  Defined.

  Global Instance option_equiv_Equivalence : Equivalence option_equiv := {}.

  Global Instance option_Setoid : Setoid (option A) :=
    {
      equiv := option_equiv;
    }.

  (** Something is equivalent to [None] iff it is [None] itself. *)

  Lemma eq_None_equiv_None (x : option A) :
    x = None <-> x == None.
  Proof.
    now destruct x.
  Qed.

  (** Something is not [None] iff it is equivalent to a [Some]. *)

  Lemma not_None_equiv_Some (x : option A) :
    x <> None <-> exists (a : A), x == Some a.
  Proof.
    destruct x; split; firstorder.
    - now (exists a).
    - discriminate.
  Qed.
  
  (** [equiv_and_Some] is true iff the two options are both equivalent,
      _and_ both contain a value. *)

  Definition equiv_and_Some (x y : option A) : Prop :=
    x <> None /\ y <> None /\ x == y.

  Lemma equiv_and_Some_equiv (x y : option A) :
    equiv_and_Some x y -> equiv x y.
  Proof.
    firstorder.
  Qed.

  Lemma equiv_Some_equiv_and_Some (x y : option A) (a b : A) :
    x == Some a ->
    y == Some b ->
    x == y ->
    equiv_and_Some x y.
  Proof.
    repeat split;
      match goal with
      | [ H: ?x == Some ?a |- ?x <> None ] => apply not_None_equiv_Some; now (exists a)
      | _ => assumption
      end.
  Qed.
    
End option_setoid.

Section option_monad.

  Global Instance option_Functor : Functor option :=
    {
      fmap := option_map
    }.
  Proof.
    - (* identity *)
      now intros A [|].
    - (* compose *)
      now intros A B C f g [|].
  Defined.

  Global Instance option_Monad : Monad option :=
    {
      mbind := @bind;
      mreturn := @Some;
    }.
  Proof.
    - (* monad_identity_left *)
      intuition.
    - (* monad_identity_right *)
      now intros A [|].
    - (* monad_assoc *)
      now intros A B C [|] f g.
    - (* monad_fmap *)
      now intros A B f [|].
  Defined.
  
End option_monad.

Infix "=!=" := (equiv_and_Some) (at level 80) : option_scope.