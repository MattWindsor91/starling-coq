(** * Finite, list-based multisets *)

From Coq Require Import
     Arith.PeanoNat
     Classes.DecidableClass
     Classes.Morphisms
     Classes.RelationClasses
     Lists.List
     Lists.SetoidList
     Classes.SetoidClass
     Classes.SetoidDec
     Program.Basics
     Omega
     RelationPairs
     Sets.Multiset
     Lists.SetoidPermutation
     Sorting.Permutation
.

From Starling Require Import
     Utils.List.Facts
     Utils.List.Setoid
     Utils.Monads
     Utils.Multiset.Subset
     Utils.Multiset.Minus
     Utils.Nat.Facts
.

Set Implicit Arguments.

Section list_ops.
  Context
    {T: Type}.

  Section count_occ.

    Hypothesis Hdec: forall a b: T, {a = b} + {a <> b}.

    Lemma count_occ_app (l1 l2: list T) (x: T):
      count_occ Hdec (l1 ++ l2) x = count_occ Hdec l1 x + count_occ Hdec l2 x.
    Proof.
      induction l1.
      - rewrite app_nil_l.
        symmetry.
        apply Nat.add_0_l.
      - rewrite <- app_comm_cons.
        destruct (Hdec a x) as [Hax|Hax].
        + rewrite count_occ_cons_eq, count_occ_cons_eq; try apply Hax.
          rewrite Nat.add_comm, Nat.add_succ_r, Nat.add_comm.
          f_equal.
          apply IHl1.
        + rewrite count_occ_cons_neq, count_occ_cons_neq; assumption.
    Qed.

    Corollary count_occ_app_comm (l1 l2: list T) (x: T):
      count_occ Hdec (l1 ++ l2) x = count_occ Hdec (l2 ++ l1) x.
    Proof.
      rewrite count_occ_app, count_occ_app.
      apply Nat.add_comm.
    Qed.
  End count_occ.

  Section minus.
    Context
      {ST : Setoid T}
      {DT : EqDec ST}.

    Fixpoint rem1 (x: T) (l: list T): list T :=
      match l with
      | nil => nil
      | y::tl => if y == x then tl else y::(rem1 x tl)
      end.

    Lemma rem1_nil (x: T):
      rem1 x nil == nil.
    Proof.
      reflexivity.
    Qed.

    Lemma rem1_count_occ_maybe (x y: T) (l: list T):
      count_occ_equiv x (rem1 y l) = if y == x then (count_occ_equiv x l - 1) else (count_occ_equiv x l).
    Proof.
      induction l; destruct (equiv_dec y x); firstorder; cbn in *.
      - unfold equiv_nat at 2.
        destruct (equiv_dec a x), (equiv_dec a y); firstorder.
        + now rewrite <- e in e0.
        + now rewrite <- e in c.
        + cbn in *.
          now rewrite equiv_nat_not.
      - unfold equiv_nat at 2.
        unfold count_occ_equiv, compose in IHl.
        rewrite <- IHl.
        destruct (equiv_dec a x), (equiv_dec a y); firstorder; cbn.
        + rewrite <- e, <- e0 in c.
          now assert (a == a) by reflexivity.
        + now rewrite equiv_nat_equiv.
        + now rewrite equiv_nat_not.
    Qed.

    Corollary rem1_count_occ (x: T) (l: list T):
      count_occ_equiv x (rem1 x l) = count_occ_equiv x l - 1.
    Proof.
      rewrite rem1_count_occ_maybe.
      destruct (equiv_dec x x); firstorder.
    Qed.

    Definition lminus (l1 l2: list T): list T :=
      fold_right rem1 l1 l2.

    Lemma lminus_nil_right (l: list T):
      lminus l nil = l.
    Proof.
      unfold lminus, fold_right.
      destruct l; reflexivity.
    Qed.

    Lemma lminus_nil_left (l: list T):
      lminus nil l = nil.
    Proof.
      unfold lminus, fold_right.
      induction l.
      - reflexivity.
      - rewrite IHl.
        apply rem1_nil.
    Qed.

    Lemma lminus_cons (t: T) (l1 l2: list T):
      lminus l1 (t::l2) = rem1 t (lminus l1 l2).
    Proof.
      reflexivity.
    Qed.

    Lemma lminus_count_occ (l1 l2: list T) (x: T):
      count_occ_equiv x (lminus l1 l2) = count_occ_equiv x l1 - count_occ_equiv x l2.
    Proof.
      induction l2.
      - rewrite lminus_nil_right.
        apply symmetry, Nat.sub_0_r.
      - rewrite lminus_cons, rem1_count_occ_maybe.
        unfold count_occ_equiv at 4.
        rewrite IHl2.
        destruct (a == x) as [Hax|Hax]; cbn.
        + now rewrite <- Nat.sub_add_distr, Nat.add_1_r, equiv_nat_equiv.
        + now rewrite equiv_nat_not.
    Qed.
  End minus.
End list_ops.

Section multisets_lmultiset.
  Variable
    (A: Type).

  (** [A] must form a decidable setoid. *)

  Context
    {SA : Setoid A}
    {DA : EqDec SA}.

  Inductive LMultiset : Type :=
    LBag (xs: list A).

  Definition unbag '(LBag xs) : list A := xs.

  Definition EmptyLBag := LBag nil.

  Definition lmultiplicity '(LBag l: LMultiset) (a: A): nat :=
    count_occ_equiv a l.

  Lemma lmultiplicity_EmptyLBag (a: A):
    lmultiplicity EmptyLBag a = 0.
  Proof.
    reflexivity.
  Qed.

  Lemma lmultiplicity_In (l: list A) (a: A):
    0 < lmultiplicity (LBag l) a <-> InA equiv a l.
  Proof.
    split.
    - intro Hmult.
      induction l.
      + easy.
      + apply InA_cons.
        cbn in *.
        unfold equiv_nat in Hmult.
        destruct (a0 == a) as [->|]; firstorder.
    - unfold lmultiplicity.
      induction l.
      + easy.
      + inversion_clear 1; cbn in *.
        * unfold equiv_nat at 1.
          destruct (a0 == a); firstorder.
        * apply Nat.lt_lt_add_l; firstorder.
  Qed.

  Section multisets_lmultiset_wrap.
    (** We can view any [LMultiset] as a [multiset] just by lifting its
        multiplicity function to a [Bag]. *)

    Definition as_bag (m: LMultiset): multiset A :=
      Bag (fun x => lmultiplicity m x).

    Lemma as_bag_multiplicity (m: LMultiset) (a: A):
      multiplicity (as_bag m) a = lmultiplicity m a.
    Proof.
      reflexivity.
    Qed.
  End multisets_lmultiset_wrap.

  Section multisets_lmultiset_equivalence.
    Definition lmeq (m1 m2: LMultiset) :=
      forall a: A, lmultiplicity m1 a = lmultiplicity m2 a.

    (** Two [as_bag] projections are equivalent, and only if, their
        underlying [LMultisets] are. *)

    Corollary lmeq_meq (m1 m2: LMultiset):
      lmeq m1 m2 = meq (as_bag m1) (as_bag m2).
    Proof.
      reflexivity.
    Qed.

    Global Instance lmeq_Reflexive: Reflexive lmeq.
    Proof.
      firstorder.
    Defined.

    Global Instance lmeq_Symmetric: Symmetric lmeq.
    Proof.
      firstorder.
    Defined.

    Global Instance lmeq_Transitive: Transitive lmeq.
    Proof.
      intros x y z Hxy Hyz a.
      specialize (Hxy a).
      specialize (Hyz a).
      firstorder.
    Defined.

    Lemma PermutationA_lmeq (l1 l2 : list A) :
      PermutationA equiv l1 l2 -> lmeq (LBag l1) (LBag l2).
    Proof.
      induction 1; intro a; cbn; intuition.
      - specialize (IHPermutationA a).
        cbn in *.
        unfold count_occ_equiv, compose in IHPermutationA.
        rewrite (equiv_nat_Proper a a (reflexivity _) x₁ x₂ H).
        omega.
      - specialize (IHPermutationA1 a).
        specialize (IHPermutationA2 a).
        unfold lmultiplicity in *.
        omega.
    Qed.

    Corollary Permutation_lmeq (l1 l2 : list A) :
      Permutation l1 l2 -> lmeq (LBag l1) (LBag l2).
    Proof.
      intro Hperm.
      apply PermutationA_lmeq, Permutation_PermutationA; intuition.
    Qed.

    (** Any two equivalent list multisets are setoid permutations of
        one another. *)

    Lemma lmeq_PermutationA (m1 m2 : LMultiset) :
      lmeq m1 m2 -> PermutationA equiv (unbag m1) (unbag m2).
    Proof.
      destruct m1 as (l1), m2 as (l2).
      unfold lmeq.
      cbn.
      revert l2.
      induction l1; intros l2 Heqv.
      - destruct l2; try easy.
        specialize (Heqv a).
        cbn in Heqv.
        now rewrite equiv_nat_refl in Heqv.
      - destruct l2.
        + specialize (Heqv a).
          cbn in Heqv.
          now rewrite equiv_nat_refl in Heqv.
        + pose proof (Heqv a) as Heqa.
          rewrite ! count_occ_equiv_cons, equiv_nat_refl, equiv_nat_comm in Heqa.
          unfold equiv_nat in *.
          destruct (a == a0).
          * cbn.
            f_equal.
            constructor; intuition.
            apply IHl1.
            intros b.
            specialize (Heqv b).
            rewrite !count_occ_equiv_cons in Heqv.
            unfold equiv_nat in Heqv.
            destruct (a == b), (a0 == b); intuition.
            -- rewrite e0 in e.
               firstorder.
            -- rewrite <- e in e0.
               firstorder.
          * rewrite Nat.add_0_l in *.
            symmetry in Heqa.
            destruct (count_occ_equiv_punchout _ _ _ Heqa) as
                (a' & l2a & l2b & -> & Hequiv & Hcount_occ).
            cbn.
            f_equal.
            (* First, replace [a] with [a']... *)
            eapply permA_trans.
            {
              apply permA_skip.
              - apply Hequiv.
              - (* Next, replace [l1] with [l2]... *)
                apply IHl1 with (l2 := a0 :: l2a ++ l2b).
                intro b.
                specialize (Heqv b).
                rewrite ! count_occ_equiv_cons, count_occ_equiv_app,
                count_occ_equiv_cons,
                (equiv_nat_Proper b b (reflexivity _) a a' Hequiv) in Heqv.
                rewrite ! count_occ_equiv_cons, count_occ_equiv_app.
                omega.
            }
            eapply permA_trans.
            {
              rewrite app_comm_cons.
              apply PermutationA_middle.
              intuition.
            }
            now rewrite app_comm_cons.
    Qed.

    Lemma lmeq_length (v1 v2 : LMultiset) :
      lmeq v1 v2 ->
      length (unbag v1) = length (unbag v2).
    Proof.
      now intros Hlmeq%lmeq_PermutationA%PermutationA_length.
    Qed.

    Global Instance lmeq_Equivalence: Equivalence lmeq := {}.
  End multisets_lmultiset_equivalence.

  Section multisets_lmultiset_inclusion.
    Definition lsubset (m1 m2: LMultiset) :=
      forall a: A, lmultiplicity m1 a <= lmultiplicity m2 a.

    (** Two [as_bag] projections are related by inclusion if, and only
        if, their underlying [LMultisets] are. *)

    Corollary lsubset_subset (m1 m2: LMultiset):
      lsubset m1 m2 = subset (as_bag m1) (as_bag m2).
    Proof.
      reflexivity.
    Qed.

    Global Instance lsubset_Reflexive: Reflexive lsubset.
    Proof.
      firstorder.
    Defined.

    Global Instance lsubset_Transitive: Transitive lsubset.
    Proof.
      intros x y z Hxy Hyz a.
      specialize (Hxy a).
      specialize (Hyz a).
      firstorder.
    Defined.

    Global Instance lsubset_PreOrder: PreOrder lsubset := {}.

    Global Instance lsubset_PartialOrder: PartialOrder lmeq lsubset.
    Proof.
      intros x y.
      split.
      - intros Hlmeq_xy.
        split; intro a; rewrite Hlmeq_xy; reflexivity.
      - intros (Hlsubset_xy & Hlsubset_yx) a.
        specialize (Hlsubset_xy a).
        specialize (Hlsubset_yx a).
        apply Nat.le_antisymm; assumption.
    Defined.
  End multisets_lmultiset_inclusion.

  Section multisets_lmultiset_union.
    Definition lmunion '(LBag m1) '(LBag m2) :=
      LBag (app m1 m2).

    Lemma lmunion_count (m1 m2: LMultiset) (a: A):
      lmultiplicity (lmunion m1 m2) a = lmultiplicity m1 a + lmultiplicity m2 a.
    Proof.
      unfold lmultiplicity.
      destruct m1 as (l1), m2 as (l2).
      apply count_occ_equiv_app.
    Qed.

    (** A [multiset] constructed by [munion]ing two projections is
        equivalent to the projection of the [lmunion] of their
        underlying [LMultiset]s. *)

    Lemma lmunion_munion (m1 m2: LMultiset):
      meq (munion (as_bag m1) (as_bag m2)) (as_bag (lmunion m1 m2)).
    Proof.
      intro a.
      rewrite as_bag_multiplicity, lmunion_count.
      reflexivity.
    Qed.

    Lemma lmunion_comm (m1 m2: LMultiset):
      lmeq (lmunion m1 m2) (lmunion m2 m1).
    Proof.
      intro a.
      rewrite ! lmunion_count.
      apply Nat.add_comm.
    Qed.

    Lemma lmunion_ass (m1 m2 m3: LMultiset):
      lmeq (lmunion (lmunion m1 m2) m3) (lmunion m1 (lmunion m2 m3)).
    Proof.
      intro a.
      rewrite ! lmunion_count.
      apply symmetry, Nat.add_assoc.
    Qed.

    Lemma lmunion_empty_left (m: LMultiset):
      lmeq (lmunion EmptyLBag m) m.
    Proof.
      intro a.
      rewrite lmunion_count.
      reflexivity.
    Qed.

    Lemma lmunion_empty_right (m: LMultiset):
      lmeq m (lmunion m EmptyLBag).
    Proof.
      transitivity (lmunion EmptyLBag m).
      - apply symmetry, lmunion_empty_left.
      - apply lmunion_comm.
    Qed.

    (** If we swap the two list multisets in a [munion] for two
        equivalent list multisets, the result is equivalent to our
        original union. *)

    Global Instance lmunion_lmeq_Proper
      : Proper (lmeq ==> lmeq ==> lmeq) lmunion.
    Proof.
      intros x x' Hlmeq_x y y' Hlmeq_y a.
      rewrite ! lmunion_count.
      rewrite Hlmeq_x, Hlmeq_y.
      reflexivity.
    Defined.

    Corollary lmeq_lmunion_left (x y z: LMultiset):
      lmeq x y -> lmeq (lmunion x z) (lmunion y z).
    Proof.
      intros Hmeq_xy.
      apply lmunion_lmeq_Proper.
      - exact Hmeq_xy.
      - reflexivity.
    Qed.

    (** If we swap the two list multisets in a [munion] for two superset
        multisets, the result is a superset of our original union. *)

    Global Instance lmunion_lsubset_Proper
      : Proper (lsubset ==> lsubset ==> lsubset) lmunion.
    Proof.
      intros x x' Hlmeq_x y y' Hlmeq_y a.
      rewrite ! lmunion_count.
      specialize (Hlmeq_x a).
      specialize (Hlmeq_y a).
      apply Nat.add_le_mono; assumption.
    Defined.

    Corollary lsubset_lmunion_left (x y z: LMultiset):
      lsubset x y -> lsubset (lmunion x z) (lmunion y z).
    Proof.
      intros Hmeq_xy.
      apply lmunion_lsubset_Proper.
      - exact Hmeq_xy.
      - reflexivity.
    Qed.

    Lemma lsubset_lmunion_mono_left (x y: LMultiset):
      lsubset x (lmunion x y).
    Proof.
      intro a.
      rewrite lmunion_count.
      apply Nat.le_add_r.
    Qed.
  End multisets_lmultiset_union.

  Section multisets_lmultiset_minus.
    Definition lmminus '(LBag m1) '(LBag m2) :=
      LBag (lminus m1 m2).

    Lemma lmminus_count (m1 m2: LMultiset) (a: A):
      lmultiplicity (lmminus m1 m2) a = lmultiplicity m1 a - lmultiplicity m2 a.
    Proof.
      unfold lmultiplicity.
      destruct m1 as (l1), m2 as (l2).
      apply lminus_count_occ.
    Qed.

    (** Most of these proofs start by unfolding to a multiplicity
        formula, so we automate this using a rewrite base. *)

    Hint Rewrite lmunion_count lmminus_count : lmunion_minus_base.

    Ltac start_lmunion_minus :=
      match goal with
      | |- lmeq ?x ?y => intro a; autorewrite with lmunion_minus_base
      | _ => fail "not a LMultiset equivalence"
      end.

    (** A [multiset] constructed by [mminus]ing two projections is
        equivalent to the projection of the [lmminus] of their
        underlying [LMultiset]s. *)

    Lemma lmminus_mminus (m1 m2: LMultiset):
      meq (mminus (as_bag m1) (as_bag m2)) (as_bag (lmminus m1 m2)).
    Proof.
      intro a.
      rewrite as_bag_multiplicity, lmminus_count.
      reflexivity.
    Qed.

    Lemma lmminus_empty_left (m: LMultiset):
      lmeq (lmminus EmptyLBag m) EmptyLBag.
    Proof.
      start_lmunion_minus.
      apply Nat.sub_0_l.
    Qed.

    Lemma lmminus_empty_right (m: LMultiset):
      lmeq (lmminus m EmptyLBag) m.
    Proof.
      start_lmunion_minus.
      apply Nat.sub_0_r.
    Qed.

    Lemma lmeq_adjoint (m n o: LMultiset):
      lmeq m (lmunion n o) -> lmeq (lmminus m n) o.
    Proof.
      intros Hm_no a.
      specialize (Hm_no a).
      rewrite lmminus_count, Hm_no, lmunion_count.
      rewrite Nat.add_comm.
      apply Nat.add_sub.
    Qed.

    Lemma lsubset_adjoint (m n o: LMultiset):
      lsubset m (lmunion n o) <-> lsubset (lmminus m n) o.
    Proof.
      split.
      - intros Hm_no a.
        rewrite lmminus_count.
        apply Nat.le_sub_le_add_l.
        rewrite <- lmunion_count.
        apply Hm_no.
      - intros Hmn_o a.
        rewrite lmunion_count.
        apply Nat.le_sub_le_add_l.
        rewrite <- lmminus_count.
        apply Hmn_o.
    Qed.

    Lemma lsubset_lmminus_congr (m n o: LMultiset):
      lsubset m n -> lsubset (lmminus m o) (lmminus n o).
    Proof.
      intros Hinc_mn a.
      rewrite ! lmminus_count.
      apply Nat.sub_le_mono_r, Hinc_mn.
    Qed.

    Lemma lmminus_lmunion (m1 m2: LMultiset):
      lmeq m1 (lmminus (lmunion m1 m2) m2).
    Proof.
      start_lmunion_minus.
      apply symmetry, Nat.add_sub.
    Qed.

    Lemma lmminus_of_lmunion (m n o : LMultiset) :
      lmeq (lmminus (lmunion m n) o) (lmunion (lmminus m o) (lmminus n (lmminus o m))).
    Proof.
      start_lmunion_minus.
      apply minus_of_plus.
    Qed.
  End multisets_lmultiset_minus.

  Section multisets_lmultiset_dlsubset.
    Definition dlsubset_bool (m1 m2: LMultiset): bool :=
      match lmminus m1 m2 with
      | LBag nil => true
      | _        => false
      end.

    Definition dlsubset (m1 m2: LMultiset): Prop := Bool.Is_true (dlsubset_bool m1 m2).

    (** [dlsubset] trivially decides [dlsubset_bool]. *)

    Global Instance Decidable_dlsubset (m1 m2: LMultiset): Decidable (dlsubset m1 m2) :=
      {
        Decidable_witness := dlsubset_bool m1 m2
      }.
    Proof.
      split.
      - apply Bool.Is_true_eq_left.
      - apply Bool.Is_true_eq_true.
    Defined.

    Lemma dlsubset_lsubset (m1 m2: LMultiset): dlsubset m1 m2 -> lsubset m1 m2.
    Proof.
      intros Hdl x.
      unfold dlsubset, dlsubset_bool in Hdl.
      destruct (lmminus m1 m2) as [[|foo]] eqn:Hl12.
      - pose (Hmul_empty := lmultiplicity_EmptyLBag x).
        unfold EmptyLBag in Hmul_empty.
        rewrite <- Hl12 in Hmul_empty.
        rewrite lmminus_count in Hmul_empty.
        apply Nat.sub_0_le, Hmul_empty.
      - contradiction.
    Qed.

    Lemma lsubset_dlsubset (m1 m2: LMultiset): lsubset m1 m2 -> dlsubset m1 m2.
    Proof.
      intros Hl.
      unfold dlsubset, dlsubset_bool.
      destruct (lmminus m1 m2) as [[|]] eqn:Hlm.
      - reflexivity.
      - pose (lmminus_count m1 m2 a) as Hcount.
        rewrite Hlm in Hcount.
        destruct m1 as (l1), m2 as (l2).
        specialize (Hl a).
        unfold lmultiplicity in *.
        rewrite count_occ_equiv_cons_eq in Hcount by reflexivity.
        apply Nat.sub_0_le in Hl.
        rewrite Hl in Hcount.
        discriminate.
    Qed.

    (** Since we have a way to decide multiset subset with [dlsubset], and
        it is equivalent to [lsubset], the original [lsubset] is decidable. *)

    Global Instance Decidable_lsubset (m1 m2: LMultiset): Decidable (lsubset m1 m2) :=
      {
        Decidable_witness := dlsubset_bool m1 m2
      }.
    Proof.
      transitivity (dlsubset m1 m2); split.
      - apply Bool.Is_true_eq_left.
      - apply Bool.Is_true_eq_true.
      - apply dlsubset_lsubset.
      - apply lsubset_dlsubset.
    Defined.
  End multisets_lmultiset_dlsubset.
End multisets_lmultiset.

(** Lists are functors, thus so are [LMultiset]s. *)

Global Instance Functor_LMultiset : Functor LMultiset :=
  {
    fmap A B f '(LBag xs) := LBag (fmap f xs)
  }.
Proof.
  - (* Identity *)
    intros A [v].
    now rewrite fmap_identity.
  - (* Compose *)
    intros A B C f g [v].
    now rewrite fmap_compose.
Defined.

(** Lists are monads, thus so are [LMultiset]s. *)

Local Open Scope program_scope.

Global Instance Monad_LMultiset : Monad LMultiset :=
  {
    mbind A B f '(LBag xs) := LBag (mbind ((fun '(LBag ys) => ys) ∘ f) xs);
    mreturn A := (@LBag A) ∘ mreturn
  }.
Proof.
  - (* Left identity *)
    intros A B a f.
    cbn.
    unfold compose.
    destruct (f a) as [ys].
    now autorewrite with flat_map list.
  - (* Right identity *)
    intros A [v].
    f_equal.
    apply monad_identity_right.
  - (* Associativity *)
    intros A B C [v] f g.
    f_equal.
    unfold compose.
    cbn.
    induction v; autorewrite with flat_map list; intuition.
    rewrite IHv.
    destruct (f a) as [ys].
    now autorewrite with flat_map list.
  - (* fmap *)
    now intros A B f [v].
Defined.

Section eqbagA.

  Context
    {T  : Type}
    {ST : Setoid T}
    {DT : EqDec ST}
  .

  Local Open Scope signature_scope.

  Definition eqbagA : relation (LMultiset T) :=
    eqlistA SetoidClass.equiv @@ (@unbag _).

  Definition eqbagbA (x y : LMultiset T) : bool :=
    eqlistbA (equivb (SA := ST)) (unbag x) (unbag y).

  Lemma eqbagbA_eqbagA_reflect (x y : LMultiset T) :
    Bool.reflect (eqbagA x y) (eqbagbA x y).
  Proof.
    apply eqlistbA_eqlistA_reflect, equivb_equiv_reflect.
  Qed.

  Lemma eqbagA_equiv_sub :
    subrelation eqbagA (lmeq (A := T)).
  Proof.
    intros x y Hbag%eqlistA_PermutationA%(PermutationA_lmeq (DA := DT)).
    now destruct x, y.
  Qed.

  Lemma Exists_equiv_trans
        (x y : LMultiset T) (xs : list (LMultiset T)) :
    eqbagA x y ->
    Exists (eqbagA x) xs ->
    Exists (eqbagA y) xs.
  Proof.
    intros Heqv_y (z & Hin_z & Heqv_z)%Exists_exists.
    apply Exists_exists.
    exists z; intuition.
    destruct x as (x), y as (y), z as (z).
    unfold eqbagA, "@@" in *.
    cbn in *.
    now rewrite <- Heqv_y.
  Qed.

  Global Instance eqbagA_Equivalence :
    Equivalence eqbagA := {}.
  Proof.
    - intros (x).
      apply eqlistA_equiv, setoid_equiv.
    - intros (x) (y).
      apply eqlistA_equiv, setoid_equiv.
    - intros (x) (y) (z).
      apply eqlistA_equiv, setoid_equiv.
  Defined.
End eqbagA.