(** * Miscellaneous multiset facts *)

Require Import Coq.Sets.Multiset.
Require Import Coq.Classes.RelationClasses.
Require Import Coq.Arith.PeanoNat.


(** ** Multiset union is cancellative *)

Theorem munion_cancel_left {A} (a b c : multiset A) :
  meq (munion a b) (munion a c) -> meq b c.
Proof.
  intros Habc x.
  unfold meq in Habc.
  specialize Habc with x.
  unfold munion in Habc.
  simpl in Habc.
  rewrite <- Nat.add_cancel_l.
  exact Habc.
Qed.

Theorem munion_cancel_right {A} (a b c : multiset A) :
  meq (munion b a) (munion c a) -> meq b c.
Proof.
  intros Habc x.
  unfold meq in Habc.
  specialize Habc with x.
  unfold munion in Habc.
  simpl in Habc.
  rewrite <- Nat.add_cancel_r.
  exact Habc.
Qed.

(** ** Multiset equivalence is, indeed, an equivalence *)

Instance meq_Equivalence {A : Type} : Equivalence (@meq A).
Proof.
  split.
  - intro x.
    apply meq_refl.
  - intros x y.
    apply meq_sym.
  - intros x y z.
    apply meq_trans.
Defined.