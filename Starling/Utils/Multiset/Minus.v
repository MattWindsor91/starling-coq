Require Import
        Arith
        Coq.Sets.Multiset
        Coq.Arith.PeanoNat
        Coq.ZArith.BinInt
        Coq.Classes.RelationClasses
        Coq.Lists.List.

Require Import
        Starling.Utils.Nat.Facts.

(*
 * Multiset minus.
 *)

Definition mminus {A} (m1 m2: multiset A) :=
  Bag
    (fun a:A => (multiplicity m1 a - multiplicity m2 a)).

Theorem mminus_empty_left {A} (a : multiset A) :
  meq (mminus (EmptyBag A) a) (EmptyBag A).
Proof.
  unfold meq.
  reflexivity.
Qed.

Theorem mminus_empty_right {A} (a : multiset A):
  meq (mminus a (EmptyBag A)) a.
Proof.
  intro a0.
  apply Nat.sub_0_r.
Qed.

(** [mminus] and [munion] have a one-way adjoint property over [meq]. *)

Lemma meq_adjoint {A} (p q r : multiset A) : 
  meq p (munion q r) -> meq (mminus p q) r.
Proof.
  (* By appeal to the natural numbers. *)
  intros H x.
  specialize (H x).
  simpl.
  rewrite H.
  simpl.
  rewrite Nat.add_comm, Nat.add_sub.
  reflexivity.
Qed.

Theorem mminus_of_munion {A} (a b c : multiset A) :
  meq
    (mminus (munion a b) c)
    (munion (mminus a c) (mminus b (mminus c a))).
Proof.
  intro a0.
  apply minus_of_plus.
Qed.

Theorem mminus_by_munion {A} (a b c : multiset A) :
  meq
    (mminus a (munion b c))
    (mminus (mminus a b) c).
Proof.
  intro a0.
  apply Nat.sub_add_distr.
Qed.

Lemma mminus_munion {A} (a b : multiset A) :
  meq a (mminus (munion a b) b).
Proof.
  (* By appeal to the natural numbers. *)
  intro x.
  simpl.
  rewrite <- Nat.add_sub_assoc.
  - rewrite Nat.sub_diag, Nat.add_0_r.
    reflexivity.
  - apply Nat.le_refl.
Qed.

(** If [eqA] is a decidable equivalence backing a multiset, then we can reduce a
    minus of two singletons to a decision on [eqA]. *)

Lemma mminus_singleton {A} (a b : A) (eqA : A -> A -> Prop)
      {eqA_Eq : Equivalence eqA}
      (HeqA : forall (x y : A), {eqA x y} + {~ eqA x y}):
  meq
    (mminus (SingletonBag eqA HeqA a) (SingletonBag eqA HeqA b))
    (if HeqA a b then EmptyBag A else SingletonBag eqA HeqA a).
Proof.
  destruct HeqA as [Heq|Hneq]; intros x.
  - simpl.
    destruct (HeqA a x) as [Hax|Hnax], (HeqA b x) as [Hbx|Hnbx]; try reflexivity.
    absurd (eqA b x).
    + exact Hnbx.
    + transitivity a.
      * symmetry.
        exact Heq.
      * exact Hax.
  - simpl.
    destruct (HeqA a x) as [Hax|Hnax], (HeqA b x) as [Hbx|Hnbx]; try reflexivity.
    absurd (eqA a b).
    + exact Hneq.
    + transitivity x.
      * exact Hax.
      * symmetry.
        exact Hbx.
Qed.