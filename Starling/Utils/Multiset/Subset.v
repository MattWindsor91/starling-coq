(** * Subsetting of multisets
    We now define a 'subset' operation on multisets. *)

Require Import
        Coq.Arith.PeanoNat
        Coq.Classes.RelationClasses
        Coq.Logic.Decidable
        Coq.Sets.Multiset.

Require Import
        Starling.Utils.Multiset.Minus.

(** A multiset [x] is a subset of a multiset [y] if, and only if, for all
    items [i], [i] is in [x] at most as many times as it is in [y]. *)

Definition subset {A} (x y : multiset A) :=
  forall (i : A),
    multiplicity x i <= multiplicity y i.

Section subset_facts.
  Context {A : Type}.

  (** ** Multiset subset is a partial ordering
    Subset is reflexive: *)

  Theorem subset_refl (x : multiset A) :
    subset x x.
  Proof.
    (* By appeal to the reflexivity of natural numbers. *)
    unfold subset.
    reflexivity.
  Qed.

  Global Instance subset_Reflexive : Reflexive (@subset A) :=
    { reflexivity := subset_refl }.

  (** Subset is antisymmetric in respect to multiset equivalence: *)

  Theorem subset_antisymm (x y : multiset A) :
    subset x y -> subset y x -> meq x y.
  Proof.
    (* By appeal to the ordering of natural numbers. *)
    intros Hxy Hyx i.
    apply (Nat.le_antisymm (multiplicity x i) (multiplicity y i)
                           (Hxy i) (Hyx i)).
  Qed.

  (** To show antisymmetry in Coq, we must first show that [meq] is an
      equivalence. *)

  Global Instance meq_Symmetric : Symmetric (@meq A) := { symmetry := @meq_sym A }.
  Global Instance meq_Reflexive : Reflexive (@meq A) := { reflexivity := @meq_refl A }.
  Global Instance meq_Transitive : Transitive (@meq A) := { transitivity := @meq_trans A}.
  Global Instance meq_Equivalence : Equivalence (@meq A) := {}.

  Global Instance subset_Antisymmetric
    : Antisymmetric (multiset A) (@meq A) (@subset A) :=
    { antisymmetry := subset_antisymm }.

  (** Finally, subset is transitive. *)

  Theorem subset_trans (x y z : multiset A) :
    subset x y -> subset y z -> subset x z.
  Proof.
    (* By appeal to the transitivity of natural numbers. *)
    intros Hxy Hyz i.
    apply (Nat.le_trans (multiplicity x i) (multiplicity y i) (multiplicity z i)
                        (Hxy i) (Hyz i)).
  Qed.

  Global Instance subset_Transitive
    : Transitive (@subset A) :=
    { transitivity := subset_trans }.

  (** Subset is thus a preorder. *)

  Global Instance subset_PreOrder
    : PreOrder (@subset A) :=
    { PreOrder_Reflexive := subset_Reflexive;
      PreOrder_Transitive := subset_Transitive }.

  (** [subset] is a partial order up to [meq]. *)

  (** Multiset equivalence entails subset.

      (This may have come from Matt Parkinson?) *)

  Theorem subset_meq (p q : multiset A) :
    meq p q -> subset p q.
  Proof.
    (* By appeal to the ordering properties of natural numbers. *)
    intros H i.
    rewrite Nat.lt_eq_cases.
    right.
    apply H.
  Qed.

  Global Instance subset_PartialOrder
    : PartialOrder (@meq A) (@subset A) := {}.
  Proof.
    intros x y.
    split.
    - intro Hmxy.
      split.
      + apply subset_meq, Hmxy.
      + apply subset_meq, symmetry, Hmxy.
    - intro H.
      apply subset_antisymm; apply H.
  Qed.

  (** ** Other subset facts.
      These come mainly from Matt Parkinson. *)
        
  (** The subtraction of a multiset [q] from a multiset [p] is always a
      subset of [p]. *)

  Theorem mminus_subset (p q : multiset A) : 
    subset (mminus p q) p.
  Proof.
    (* By appeal to the natural numbers. *)
    intro i.
    apply Nat.le_sub_l.
  Qed.

  (** [EmptyBag] is always a subset of any other multiset. *)

  Theorem emptybag_subset (p : multiset A) :
    subset (EmptyBag A) p.
  Proof.
    (* By appeal to the natural numbers. *)
    intro i.
    unfold EmptyBag, multiplicity.
    apply Nat.succ_le_mono, Nat.lt_0_succ.
  Qed.

  (** Subset distributes through union with a common subset. *)

  Theorem subset_munion_left (a b c : multiset A) :
    subset a b -> subset (munion a c) (munion b c).
  Proof.
    (* By appeal to the natural numbers. *)
    intros Hsqr i.
    apply Nat.add_le_mono_r.
    exact (Hsqr i).
  Qed.

  (** A multiset is always a subset of its union. *)

  Theorem subset_munion_mono_left (a b: multiset A):
    subset a (munion a b).
  Proof.
    (* By appeal to the natural numbers. *)
    intros i.
    apply Nat.le_add_r.
  Qed.
  
  (** Subset distributes through minus with a common difference. *)

  Theorem subset_mminus_congr (a b c : multiset A) :
    subset a b -> subset (mminus a c) (mminus b c).
  Proof.
    (* By appeal to the natural numbers. *)
    intros Hsqr i.
    apply Nat.sub_le_mono_r.
    exact (Hsqr i).
  Qed.

  (** If [a] is a subset of [b], then subtracting and adding [a] to [b] has
      no effect. *)

  Lemma munion_mminus_strict (a b : multiset A) :
    subset a b -> meq (munion (mminus b a) a) b.
  Proof.
    intros Hsub x.
    apply Nat.sub_add, Hsub.
  Qed.
  
  Corollary munion_mminus (a b : multiset A) :
    subset a b -> subset (munion (mminus b a) a) b.
  Proof.
    intro Hsub.
    apply subset_meq, munion_mminus_strict, Hsub.
  Qed.
  
  (** [mminus] and [munion] have an adjoint property over subset. *)

  Lemma subset_adjoint (p q r : multiset A) : 
    subset p (munion q r) <-> subset (mminus p q) r.
  Proof.
    (* By appeal to the natural numbers. *)
    split; intros H x; apply Nat.le_sub_le_add_l, H.
  Qed.
End subset_facts.
