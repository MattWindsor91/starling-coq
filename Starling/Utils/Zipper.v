(** * Zipper data structure *)

From Coq Require Import
     Arith
     Classes.Morphisms
     Classes.RelationClasses
     FunInd
     Lists.List
     Omega
     Program.Basics
     Recdef
     Relations
     RelationPairs
.

From Starling Require Import
     Utils.List.Facts
.

Set Universe Polymorphism.

Record Zipper A :=
  mk_Zipper
    {
      zp_left  : list A;
      zp_right : list A;
    }.

Arguments zp_left  [A].
Arguments zp_right [A].

Set Implicit Arguments.


Import ListNotations.

(** We can convert a list to a [Zipper]; each list item moves into the
    forwards list. *)

Definition list_to_Zipper (A : Type) (lst : list A) : Zipper A :=
  mk_Zipper A [] lst.

Section Zipper_operations.

  Variables
    (A : Type)
    (z : Zipper A).

  Definition Zipper_to_list : list A :=
    (rev z.(zp_left)) ++ z.(zp_right).

  Definition Zipper_length_l : nat :=
    z.(zp_left).(length).

  Definition Zipper_length_r : nat :=
    z.(zp_right).(length).

  (** The length of a [Zipper] is the length of its inner lists. *)

  Definition Zipper_length : nat :=
    Zipper_length_l + Zipper_length_r.

  Definition move_left : Zipper A :=
    match z with
    | mk_Zipper _ []      rs => z
    | mk_Zipper _ (l::ls) rs => mk_Zipper _ ls (l::rs)
    end.

  Definition move_right : Zipper A :=
    match z with
    | mk_Zipper _ ls []      => z
    | mk_Zipper _ ls (r::rs) => mk_Zipper _ (r::ls) rs
    end.

  Definition cur : option A :=
    z.(zp_right).(hd_error).

  Definition rem : Zipper A :=
    match z.(zp_right) with
    | []      => z
    | (x::xs) => mk_Zipper _ z.(zp_left) xs
    end.

  Definition push_left (a : A) : Zipper A :=
    mk_Zipper _ (z.(zp_left) ++ [a]) z.(zp_right).

  Definition push_right (a : A) : Zipper A :=
    mk_Zipper _ z.(zp_left) (z.(zp_right) ++ [a]).

  Definition modify_left (f : list A -> list A) : Zipper A :=
    mk_Zipper _ (f (z.(zp_left))) z.(zp_right).

  Definition replace_left : list A -> Zipper A :=
    modify_left ∘ const.

  Definition insert_left : A -> Zipper A :=
    modify_left ∘ cons.

  Definition insert_cur (a : A) : Zipper A :=
    mk_Zipper _ z.(zp_left) (a :: z.(zp_right)).

  Definition insert_right (a : A) : Zipper A :=
    match z.(zp_right) with
    | [] => mk_Zipper _ z.(zp_left) [a]
    | zc::zr => mk_Zipper _ z.(zp_left) (zc::a::zr)
    end.

  (** A zipper is leftmost if its left stack is empty, and
      rightmost if its right stack is empty. *)

  Definition leftmostb : bool :=
    if z.(zp_left) then true else false.
  Definition leftmost : Prop := z.(zp_left) = [].
  Definition rightmostb : bool :=
    if z.(zp_right) then true else false.
  Definition rightmost : Prop := z.(zp_right) = [].

End Zipper_operations.

Local Open Scope program_scope.
Local Open Scope signature_scope.

(** [Zipper_lists_incl] is an inclusion-modulo-position relation on
    zippers based on their lists. *)
Definition Zipper_lists_incl {A : Type} : relation (Zipper A) :=
  (@incl A) @@ (@Zipper_to_list A).

(** [Zipper_lists_equiv] is a Leibniz-equality relation on
    zippers based on their lists. *)
Definition Zipper_lists_eq {A : Type} : relation (Zipper A) :=
  eq @@ (@Zipper_to_list A).

Lemma zipper_list_length (A : Type) (z : Zipper A) :
  Zipper_length z = length (Zipper_to_list z).
Proof.
  destruct z as (zl & zr).
  cbn.
  now rewrite app_length, rev_length.
Qed.

Definition rewind A (z : Zipper A) : Zipper A :=
  {| zp_left := []; zp_right := rev z.(zp_left) ++ z.(zp_right) |}.

Definition fast_forward A (z : Zipper A) : Zipper A :=
  {| zp_left := rev z.(zp_right) ++ z.(zp_left); zp_right := [] |}.

Section Zipper_facts.

  Variables
    (A : Type).

  Lemma move_left_length_preserve (z : Zipper A) :
    Zipper_length z = Zipper_length (move_left z).
  Proof.
    destruct z as ([|zl] & zr); cbn; intuition.
  Qed.

  Lemma rewind_length_preserve (z : Zipper A) :
    Zipper_length z = Zipper_length (rewind z).
  Proof.
    destruct z as (zl & zr).
    cbn.
    now rewrite app_length, rev_length.
  Qed.

  Lemma move_left_list_preserve (z : Zipper A) :
    Zipper_to_list z = Zipper_to_list (move_left z).
  Proof.
    destruct z as ([|zh zl] & zr); cbn; intuition.
    now rewrite <- app_assoc.
  Qed.

  Lemma cur_zp_right (z : Zipper A) (m : A) :
    cur z = Some m ->
    exists (ms : list A), z.(zp_right) = m::ms.
  Proof.
    intro Hcur.
    destruct z as (zl & [|]); try easy.
    inversion Hcur; subst.
    now exists l.
  Qed.

  Lemma cur_zp_right_none (z : Zipper A) :
    cur z = None -> z.(zp_right) = [].
  Proof.
    destruct z as (zl & [|]); try easy.
  Qed.

  Lemma zp_right_cons_move_right (z : Zipper A) (m : A) (ms : list A) :
    z.(zp_right) = m::ms ->
    z.(move_right).(zp_right) = ms.
  Proof.
    intro Hzr.
    destruct z as (zl & zr).
    cbn in Hzr.
    now rewrite Hzr.
  Qed.

  Lemma move_right_inv (a : A) (zl zr : list A) (y : Zipper A) :
    {| zp_left := a::zl; zp_right := zr |} = move_right y ->
    zr = [] \/ y = {| zp_left := zl; zp_right := a::zr |}.
  Proof.
    destruct y as (yl & [|yc yr]); cbn.
    - inversion 1; subst.
      now left.
    - inversion 1; subst.
      now right.
  Qed.

  Lemma move_right_length_preserve (z : Zipper A) :
    Zipper_length z = Zipper_length (move_right z).
  Proof.
    destruct z as (zl & [|zr]); cbn; intuition.
  Qed.

  Lemma move_right_list_preserve (z : Zipper A) :
    Zipper_lists_eq z (move_right z).
  Proof.
    unfold Zipper_lists_eq, "@@".
    destruct z as (zl & [|zr]); cbn; intuition.
    now rewrite <- app_assoc.
  Qed.

  Lemma move_right_length_r (z : Zipper A) :
    Zipper_length_r (move_right z) = Nat.pred (Zipper_length_r z).
  Proof.
    unfold move_right, zp_right in *.
    destruct z as (zl & [|zc zr']); firstorder.
  Qed.

  Lemma move_right_length_r_cur_dec (a : A) (z : Zipper A) :
    cur z = Some a ->
    Zipper_length_r (move_right z) < Zipper_length_r z.
  Proof.
    intro Ha.
    destruct (cur_zp_right _ Ha) as (zr & Hzr).
    unfold move_right, zp_right in *.
    destruct z as (zl & zr'); cbn; subst; intuition.
  Qed.

  Lemma move_right_cur (a : A) (z : Zipper A) :
    cur (move_right z) = Some a ->
    exists b : A, cur z = Some b.
  Proof.
    intro Ha.
    destruct z as (zl & [|zr]); cbn; intuition.
    - discriminate.
    - now (exists zr).
  Qed.

  Lemma fast_forward_length_preserve (z : Zipper A) :
    Zipper_length z = Zipper_length (fast_forward z).
  Proof.
    destruct z as (zl & zr).
    cbn.
    rewrite Nat.add_0_r, app_length, rev_length.
    apply Nat.add_comm.
  Qed.

  (** A rewound zipper is at its leftmost. *)

  Lemma rewind_leftmost (z : Zipper A) :
    leftmost z.(rewind).
  Proof.
    now destruct z.
  Qed.

  (** Rewinding an already-rewound zipper has no effect. *)

  Lemma rewind_idem (z : Zipper A) :
    z.(rewind).(rewind) = z.(rewind).
  Proof.
    now destruct z.
  Qed.

  Lemma to_list_rewind (z : Zipper A) :
    Zipper_to_list z = z.(rewind).(zp_right).
  Proof.
    now destruct z.
  Qed.

  (** Rewinding preserves the list of a zipper. *)

  Corollary to_list_rewind_equal (z : Zipper A) :
    Zipper_to_list z = Zipper_to_list (rewind z).
  Proof.
    rewrite ! to_list_rewind.
    now rewrite rewind_idem.
  Qed.

  (** Constructing a zipper from a list always gives a leftmost zipper. *)

  Lemma list_leftmost (l : list A) :
    leftmost (list_to_Zipper l).
  Proof.
    firstorder.
  Qed.

  Lemma leftmost_list (z : Zipper A) :
    leftmost z -> Zipper_to_list z = z.(zp_right).
  Proof.
    intro Hlm.
    unfold leftmost in Hlm.
    destruct z; cbn in *; subst; intuition.
  Qed.

  Lemma rightmost_list (z : Zipper A) :
    rightmost z -> Zipper_to_list z = rev z.(zp_left).
  Proof.
    intro Hlm.
    unfold rightmost in Hlm.
    destruct z; cbn in *; subst; intuition.
  Qed.

  Lemma to_list_idem (ls : list A) :
    Zipper_to_list (list_to_Zipper ls) = ls.
  Proof.
    trivial.
  Qed.

  Lemma push_left_list (a : A) (z : Zipper A) :
    Zipper_to_list (push_left z a) = a::(Zipper_to_list z).
  Proof.
    cbn.
    now autorewrite with list.
  Qed.

  Lemma insert_cur_rem_idem (a : A) (x : Zipper A) :
    cur x = Some a ->
    insert_cur (rem x) a = x.
  Proof.
    destruct x as (xl & [|y xr]); try easy.
    now inversion 1.
  Qed.

  Lemma rem_length_pred_r (z : Zipper A) :
    Zipper_length_r (rem z) = pred (Zipper_length_r z).
  Proof.
    now destruct z as (zl & [|]).
  Qed.

  Lemma rem_length_pred_l (z : Zipper A) :
    Zipper_length_l (rem z) = Zipper_length_l z.
  Proof.
    now destruct z as (zl & [|]).
  Qed.

  Lemma rem_length (z : Zipper A) :
    Zipper_length (rem z) = Zipper_length_l z + pred (Zipper_length_r z).
  Proof.
    now destruct z as (zl & [|]).
  Qed.

  (** The list of a zipper after [rem]oval is included in the original
      zipper's list, modulo position. *)

  Lemma rem_incl (z : Zipper A) :
    Zipper_lists_incl (rem z) z.
  Proof.
    intros x [Hx%in_rev|Hx]%in_app_or; apply in_or_app.
    - left.
      apply -> in_rev.
      now destruct z as (zl & [|]).
    - right.
      destruct z as (zl & [|]); cbn; intuition.
  Qed.

  Lemma rem_insert_cur_idem (a : A) (x : Zipper A) :
    rem (insert_cur x a) = x.
  Proof.
    now destruct x as (xl & [|y xr]).
  Qed.

  Corollary rem_insert_cur (a : A) (x y : Zipper A) :
    cur y = Some a ->
    (x = rem y <-> insert_cur x a = y).
  Proof.
    firstorder; subst.
    - now rewrite insert_cur_rem_idem.
    - now rewrite rem_insert_cur_idem.
  Qed.

  Lemma rem_Add (a : A) (x: Zipper A) :
    cur x = Some a ->
    Add a (Zipper_to_list (rem x)) (Zipper_to_list x).
  Proof.
    intros <-%insert_cur_rem_idem.
    apply Add_app.
  Qed.

  Lemma replace_left_cur (x : Zipper A) (l : list A) :
    cur (replace_left x l) = cur x.
  Proof.
    firstorder.
  Qed.

  Lemma rewind_list_round_trip (z : Zipper A) :
    z.(rewind) = z.(Zipper_to_list).(list_to_Zipper).
  Proof.
    now destruct z.
  Qed.
End Zipper_facts.

Hint Unfold
     Zipper_lists_eq
  : zipper.

Hint Rewrite ->
     to_list_idem
     move_right_length_r
     push_left_list
     rem_length_pred_l
     rem_length_pred_r
     rewind_idem
     replace_left_cur
  : zipper.

Hint Rewrite <-
     to_list_rewind_equal
     move_right_length_preserve
  : zipper.

Section Zipper_shift_props.
  Variable A : Type.

  (** The zipper [z] is a right shift of zipper [z'] when it contains the
      same elements and no elements in the right partition of [z] are in the
      left partition of [z']. *)

  Definition is_right_shift (z z' : Zipper A) : Prop :=
    Zipper_to_list z = Zipper_to_list z' /\
    Zipper_length_r z <= Zipper_length_r z'.

  (** [is_left_shift] is the mirror image of [is_right_shift]. *)

  Definition is_left_shift (z z' : Zipper A) : Prop :=
    Zipper_to_list z = Zipper_to_list z' /\
    Zipper_length_l z <= Zipper_length_l z'.

  Lemma is_right_shift_refl (z : Zipper A) : is_right_shift z z.
  Proof.
    firstorder.
  Qed.

  Lemma is_left_shift_refl (z : Zipper A) : is_left_shift z z.
  Proof.
    firstorder.
  Qed.

  Lemma both_shifts_equality (z z' : Zipper A) :
    is_left_shift z z' -> is_right_shift z z' -> z = z'.
  Proof.
    intros (Hlist & Hlen_l) (_ & Hlen_r).
    assert (Zipper_length z = Zipper_length z') as Hlen_sum.
    {
      rewrite ! zipper_list_length.
      now f_equal.
    }
    destruct z as (zl & zr), z' as (zl' & zr').
    cbn in *.
    assert (length zl = length zl') as Hlen_l' by omega.
    assert (length (rev zl) = length (rev zl')) as Hlen_rl
        by now rewrite ! rev_length.
    destruct (app_split_same _ _ _ _ Hlist Hlen_rl) as (Hrzl & ->).
    apply rev_inj in Hrzl.
    now f_equal.
  Qed.

  Lemma move_left_left_shift (z : Zipper A) :
    is_left_shift (move_left z) z.
  Proof.
    firstorder.
    - now rewrite <- move_left_list_preserve.
    - destruct z as ([|zh zl] & zr); cbn; omega.
  Qed.

  Lemma move_right_right_shift (z : Zipper A) :
    is_right_shift (move_right z) z.
  Proof.
    firstorder.
    - now rewrite <- move_right_list_preserve.
    - destruct z as (zl & [|zc zr]); cbn; omega.
  Qed.

  Lemma left_shift_transitive (z1 z2 z3 : Zipper A) :
    is_left_shift z1 z2 ->
    is_left_shift z2 z3 ->
    is_left_shift z1 z3.
  Proof.
    intros (H12_lists & H12_len) (H23_lists & H23_len).
    split; intuition.
    congruence.
  Qed.


  Corollary move_left_left_shift_backwards (x y : Zipper A) :
    is_left_shift x (move_left y) ->
    is_left_shift x y.
  Proof.
    intros Hshift.
    eapply left_shift_transitive.
    - exact Hshift.
    - apply move_left_left_shift.
  Qed.

  Lemma right_shift_transitive (z1 z2 z3 : Zipper A) :
    is_right_shift z1 z2 ->
    is_right_shift z2 z3 ->
    is_right_shift z1 z3.
  Proof.
    intros (H12_lists & H12_len) (H23_lists & H23_len).
    split; intuition.
    congruence.
  Qed.

  (** The only right shift of a [rightmost] zipper is that zipper itself. *)

  Lemma right_shift_rightmost (z1 z2 : Zipper A) :
    rightmost z1 ->
    is_right_shift z2 z1 ->
    z2 = z1.
  Proof.
    intros Hrm Hrs.
    unfold rightmost in Hrm.
    destruct Hrs as (Hrs_list & Hrs_len).
    destruct z1 as (zl1 & zr1), z2 as (zl2 & zr2); cbn in *; subst.
    cbn in Hrs_len.
    destruct zr2; try easy.
    rewrite ! app_nil_r in Hrs_list.
    apply rev_inj in Hrs_list.
    now subst.
  Qed.

  Corollary move_right_right_shift_backwards (x y : Zipper A) :
    is_right_shift x (move_right y) ->
    is_right_shift x y.
  Proof.
    intros Hshift.
    eapply right_shift_transitive.
    - exact Hshift.
    - apply move_right_right_shift.
  Qed.

  (** Trivia: left and right shifts thus form partial orderings! *)

  (** If [x] is a right shift of [y], then there is a piece of the list that has
      moved from right to left, and reversed. *)

  Lemma right_shift_left_suffix (x y : Zipper A) :
    is_right_shift x y <->
    exists (l : list A),
      x.(zp_left) = l ++ y.(zp_left) /\
      y.(zp_right) = rev l ++ x.(zp_right).
  Proof.
    destruct x as (xl & xr), y as (yl & yr).
    cbn in *.
    split.
    - intros (Hlist & Hlen).
      cbn in *.
      generalize dependent yr.
      generalize dependent yl.
      generalize dependent xl.
      induction xr; intros xl yl yr Hlist Hlen.
      + exists (rev yr).
        rewrite app_nil_r in *.
        now rewrite rev_involutive, <- (rev_involutive xl), Hlist, rev_app_distr, rev_involutive.
      + destruct yr as [|b yr]; try easy.
        cbn in Hlen.
        apply Nat.succ_le_mono in Hlen.
        change (a::xr) with ([a]++xr) in *.
        change (b::yr) with ([b]++yr) in *.
        rewrite ! app_assoc, <-! rev_cons in Hlist.
        destruct (IHxr (a::xl) (b::yl) yr Hlist Hlen) as (l & Hxl & Hyr).
        destruct l as [|lh l].
        * cbn in *.
          inversion Hxl; subst.
          now (exists []).
        * rewrite <- app_comm_cons in Hxl.
          inversion Hxl; subst.
          exists (l ++ [b]).
          rewrite <- app_assoc; intuition.
          now rewrite rev_unit, rev_cons, <- app_comm_cons, app_nil_l, <- app_comm_cons, app_assoc.
    - intros (l & -> & ->).
      split; cbn.
      + now rewrite rev_app_distr, app_assoc.
      + rewrite app_length.
        omega.
  Qed.

  (** If an element is in the right list of a zipper, then there
      exists a right shift such that that element is the cursor. *)

  Lemma in_right_right_shift (x : Zipper A) :
    Forall
      (fun a => exists y, is_right_shift y x /\ cur y = Some a)
      (zp_right x).
  Proof.
    apply Forall_forall.
    destruct x as (xl & xr).
    cbn.
    intros a (l1 & l2 & ->)%in_split.
    exists {| zp_left := rev l1 ++ xl; zp_right := a :: l2 |}; intuition.
    (* Is the above a right-shift? *)
    split.
    - cbn.
      now rewrite rev_app_distr, rev_involutive, app_assoc.
    - cbn.
      rewrite app_length.
      cbn.
      omega.
  Qed.

  Lemma right_shift_dichotomy (x y : Zipper A) :
    is_right_shift x y ->
    x = y \/ is_right_shift x (move_right y).
  Proof.
    intros (Hlist & Hlen).
    apply Nat.lt_eq_cases in Hlen.
    destruct Hlen as [Hlen | Hlen].
    - right.
      split.
      + rewrite Hlist.
        apply move_right_list_preserve.
      + pose (move_right_length_r y).
        omega.
    - left.
      apply both_shifts_equality; split; intuition.
      assert (Zipper_length x = Zipper_length y)
        by (rewrite !zipper_list_length; now f_equal).
      destruct x as (xl & xr), y as (yl & yr); cbn in *.
      omega.
  Qed.

  Lemma right_shift_replace (x y : Zipper A) (l : list A) :
    is_right_shift x y ->
    exists l',
    is_right_shift (replace_left x (l' ++ l)) (replace_left y l).
  Proof.
    intros (lxy & Hll & Hrl)%right_shift_left_suffix.
    destruct x as (xl & xr), y as (yl & yr); cbn in *.
    subst.
    unfold replace_left, modify_left, compose, const.
    cbn.
    exists lxy.
    apply right_shift_left_suffix.
    cbn.
    now exists lxy.
  Qed.

  (** Pushing the same item onto the left of two zippers preserves any
      right-shift relationship between them. *)

  Global Instance right_shift_push_left (a : A) :
    Proper (is_right_shift ==> is_right_shift) (fun x => push_left x a).
  Proof.
    intros x y (lxy & Hll & Hrl)%right_shift_left_suffix.
    apply right_shift_left_suffix.
    cbn.
    exists lxy; intuition.
    rewrite Hll.
    now rewrite app_assoc.
  Qed.

  (** As does pushing onto the right, for very similar reasons. *)

  Global Instance right_shift_push_right (a : A) :
    Proper (is_right_shift ==> is_right_shift) (fun x => push_right x a).
  Proof.
    intros x y (lxy & Hll & Hrl)%right_shift_left_suffix.
    apply right_shift_left_suffix.
    cbn.
    exists lxy; intuition.
    rewrite Hrl.
    now rewrite app_assoc.
  Qed.

  (** Inner definition used to build [direct_right_shifts_step]. *)

  Definition direct_right_shifts_step (l r : list A) (z : list (Zipper A)) (c : A) :
    (list A * list A * list (Zipper A)) :=
      (c::l, tl r, {| zp_left := c::l; zp_right := tl r |} :: z).

  (** Inner definition used to build [all_right_shifts]. *)

  Definition direct_right_shifts_inner (l r : list A) (init : list (Zipper A)) : (list A * list A * list (Zipper A)) :=
    List.fold_left (fun '(l', r', z') => direct_right_shifts_step l' r' z')
      r
      (l, r, init).

  (** [direct_right_shifts_inner] returns [init] if [r] is empty. *)

  Lemma direct_right_shifts_inner_init_nil (l : list A) (init : list (Zipper A)) :
    init = snd (direct_right_shifts_inner l [] init).
  Proof.
    intuition.
  Qed.

  Lemma direct_right_shifts_inner_init_cons (l : list A) (r : list A) (x : A) (init : list (Zipper A)) :
    snd (direct_right_shifts_inner l (x::r) init) =
    snd (direct_right_shifts_inner (x::l) r ({| zp_left := x::l; zp_right := r |} :: init)).
  Proof.
    intuition.
  Qed.

  Lemma direct_right_shifts_inner_init_app (l r : list A) (xs ys : list (Zipper A)) :
    snd (direct_right_shifts_inner l r (xs++ys)) =
    snd (direct_right_shifts_inner l r xs) ++ ys.
  Proof.
    revert r l xs ys.
    induction r; intuition.
    rewrite ! direct_right_shifts_inner_init_cons.
    rewrite app_comm_cons.
    apply IHr.
  Qed.

  (** [direct_right_shifts_inner] returns a list that is a super-list
      of its initial list. *)

  Lemma direct_right_shifts_inner_init_incl (l r : list A) (init : list (Zipper A)) :
    incl init (snd (direct_right_shifts_inner l r init)).
  Proof.
    revert init l.
    induction r; try easy.
    intros init l z Hin_z.
    apply IHr.
    now constructor 2.
  Qed.

  (** If [y] is a right shift of [x], it is in the zipper list built by
      [direct_right_shifts_inner] by taking the lists of [x], and providing an
      initial list starting with [x]. *)

  Lemma is_right_shift_in_direct_right_shifts (x y : Zipper A) (z : list (Zipper A)) :
    is_right_shift y x ->
    In y (snd (direct_right_shifts_inner x.(zp_left) x.(zp_right) (x::z))).
  Proof.
    destruct x as (xl & xr).
    unfold direct_right_shifts_inner.
    cbn.
    revert xl z y.
    induction xr; cbn; intuition.
    - left.
      now apply symmetry, right_shift_rightmost.
    - unfold direct_right_shifts_step.
      apply right_shift_dichotomy in H.
      destruct H.
      + apply direct_right_shifts_inner_init_incl.
        constructor 2.
        now constructor 1.
      + apply IHxr, H.
  Qed.

  (** If the initial list is entirely composed of right shifts of [x], then
      so is any result of using [direct_right_shifts_inner]. *)

  Lemma in_direct_right_shifts_is_right_shift (x : Zipper A) (z : list (Zipper A)) :
    Forall (fun y => is_right_shift y x) z ->
    Forall (fun y => is_right_shift y x)
           (snd (direct_right_shifts_inner x.(zp_left) x.(zp_right) z)).
  Proof.
    destruct x as (xl & xr).
    revert xl z.
    induction xr; intuition.
    - apply Forall_forall.
      intros y H0.
      unfold zp_left, zp_right in H0.
      rewrite direct_right_shifts_inner_init_cons in H0.
      assert
          (In y
              (snd (direct_right_shifts_inner (a :: xl) xr [{| zp_left := a :: xl; zp_right := xr |}]) ++
 z)) as [Hsplit|Hsplit]%in_app_iff.
        {
           now rewrite <- direct_right_shifts_inner_init_app.
        }
      + eapply right_shift_transitive.
        2: apply move_right_right_shift.
        cbn.
        refine (Forall_forall_in _ _ _ (IHxr (a :: xl) _ _) Hsplit).
        now constructor.
      + now apply Forall_forall_in with (x := y) in H.
  Qed.

  (** [all_right_shifts z] enumerates all zippers that are right shifts of [z],
      including [z] itself.  *)

  Definition all_right_shifts (z : Zipper A) : list (Zipper A) :=
    rev (snd (direct_right_shifts_inner z.(zp_left) z.(zp_right) [z])).

  (** If [y] is a right shift of [x], it is in [all_right_shifts x]. *)

  Corollary is_right_shift_in_all_right_shifts (x y : Zipper A) :
    is_right_shift y x ->
    In y (all_right_shifts x).
  Proof.
    intro Hrs.
    unfold all_right_shifts.
    apply -> in_rev.
    now apply is_right_shift_in_direct_right_shifts.
  Qed.

  Lemma Forall_rev {X} (P : X -> Prop) (l : list X) :
    Forall P l -> Forall P (rev l).
  Proof.
    intro Hp.
    apply Forall_forall.
    intros x Hin_x%in_rev.
    now apply Forall_forall_in with (x0 := x) in Hp.
  Qed.

  Lemma in_all_right_shifts_is_right_shift (x : Zipper A) :
    Forall (fun y => is_right_shift y x) (all_right_shifts x).
  Proof.
    unfold all_right_shifts.
    apply Forall_rev, in_direct_right_shifts_is_right_shift.
    now constructor.
  Qed.

  Corollary all_right_shifts_iff (x y : Zipper A) :
    is_right_shift y x <-> In y (all_right_shifts x).
  Proof.
    split.
    - apply is_right_shift_in_all_right_shifts.
    - apply (Forall_forall_in _ _ _ (in_all_right_shifts_is_right_shift _)).
  Qed.

  (** If [x] is rightmost, then it is its only right shift. *)

  Lemma all_right_shifts_rightmost (x : Zipper A) :
    rightmost x -> all_right_shifts x = [x].
  Proof.
    now destruct x as (xl & [|]).
  Qed.

  (** If [x] isn't rightmost, then we can decompose [all_right_shifts] into a
      cons of [x] to the right shifts of its first rightward move. *)

  Lemma all_right_shifts_not_rightmost (x : Zipper A) (m : A) :
    cur x = Some m ->
    all_right_shifts x = x :: all_right_shifts (move_right x).
  Proof.
    destruct x as (xl & [|xc xr]); try easy.
    inversion_clear 1.
    cbn.
    unfold all_right_shifts.
    rewrite <- rev_unit.
    f_equal.
    unfold zp_right, zp_left.
    now rewrite direct_right_shifts_inner_init_cons, <- direct_right_shifts_inner_init_app.
  Qed.
End Zipper_shift_props.

Section Zipper_Forall2.
  Variable A : Type.

  Variable R : relation A.

  (** [Forall2Z] over a relation [R] holds for two zippers when
      [Forall2 R] holds for each of the zippers' inner lists. *)

  Definition Forall2Z : relation (Zipper A) :=
    relation_conjunction
      (Forall2 R @@ @zp_left A)
      (Forall2 R @@ @zp_right A).

  (** If we have [Forall2Z] on two zippers, we have [Forall2 R] on their
      lists. *)

  Lemma Forall2Z_Forall2 :
    subrelation Forall2Z (Forall2 R @@ @Zipper_to_list A).
  Proof.
    intros z1 z2 (Hleft & Hright).
    apply Forall2_app; intuition.
    apply Forall2_rev; intuition.
  Qed.

  (** IF both of our zippers are [leftmost], the two notions are equivalent. *)

  Lemma Forall2_Forall2Z_leftmost (z1 z2 : Zipper A) :
    leftmost z1 ->
    leftmost z2 ->
    Forall2Z z1 z2 <-> Forall2 R (Zipper_to_list z1) (Zipper_to_list z2).
  Proof.
    intros Hlz1 Hlz2.
    split.
    - apply Forall2Z_Forall2.
    - destruct z1 as ([|] & z1r), z2 as ([|] & z2r); try easy.
      cbn.
      unfold RelCompFun, zp_left.
      intuition.
  Qed.

  (** Synchronised leftwards moves preserve [Forall2Z]. *)

  Lemma Forall2Z_move_left :
    subrelation Forall2Z (Forall2Z @@ move_left (A:=A)).
  Proof.
    intros ([|z1h z1l] & z1r) ([|z2h z2l] & z2r) (Hzl & Hzr); try easy.
    cbn in *.
    unfold RelCompFun, zp_left, zp_right in *.
    inversion_clear Hzl.
    split; intuition.
  Qed.

  (** Synchronised rewinds preserve [Forall2Z]. *)

  Corollary Forall2Z_rewind :
    subrelation Forall2Z (Forall2Z @@ @rewind A).
  Proof.
    intro z1.
    cbn; unfold RelCompFun.
    cbn.
    intuition.
    now dissolve_Forall2.
  Qed.

  (** Synchronised rightwards moves preserve [Forall2Z]. *)

  Lemma Forall2Z_move_right :
    subrelation Forall2Z (Forall2Z @@ move_right (A:=A)).
  Proof.
    intros (z1l & [|z1c z1r]) (z2l & [|z2c z2r]) (Hzl & Hzr); try easy.
    cbn in *.
    unfold RelCompFun, zp_left, zp_right in *.
    inversion_clear Hzr.
    split; intuition.
  Qed.

  (** If a relation holds pairwise over two zippers, it also holds pairwise
      over every pair of corresponding right shifts. *)

  Lemma all_right_shifts_Proper :
    Proper (Forall2Z ==> Forall2 Forall2Z) (@all_right_shifts A).
  Proof.
      intros (xl & xr) (yl & yr) (Hrl & Hrr).
      cbn in *.
      unfold "@@" in *.
      cbn in *.
      revert xl yl Hrl.
      revert xr yr Hrr.
      refine (Forall2_ind _ _ _).
      - intros xl yl Hforall.
        cbn.
        dissolve_Forall2; now cbn.
      - intros xc yc xr yr Hc Hr Hind xl yl Hl.
        rewrite all_right_shifts_not_rightmost
          with (m := xc),
               all_right_shifts_not_rightmost
          with (x := {| zp_left := yl; zp_right := yc :: yr |}) (m := yc) at 1; try easy.
        dissolve_Forall2; cbn; intuition.
  Qed.

  (** Removals preserve [Forall2Z], as it implies that the cursor is in the
      same position on both zippers. *)

  Lemma Forall2Z_rem :
    subrelation Forall2Z (Forall2Z @@ rem (A:=A)).
  Proof.
    intros (z1l & [|z1c z1r]) (z2l & [|z2c z2r]) (Hzl & Hzr); try easy.
    inversion_clear Hzr.
    split; intuition.
  Qed.
End Zipper_Forall2.
