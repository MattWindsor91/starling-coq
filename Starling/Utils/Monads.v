From Coq Require Import
     Program.Basics.

Local Open Scope program_scope.
Set Universe Polymorphism.

Class Functor@{r d} (V: Type@{r} -> Type@{d}) :=
  mk_Functor
    {
      fmap {A B: Type@{r}} (f: A -> B) (v: V A): V B;

      (* Functor laws over Leibniz equality *)
      fmap_identity {A: Type@{r}} (v: V A): fmap id v = v;
      fmap_compose {A B C: Type@{r}} (f: A -> B) (g: B -> C) (v: V A):
        fmap (g ∘ f) v = (fmap g ∘ fmap f) v
    }.

(** Class [Monad] is the class of Haskell 98-style monads.

    We don't use the modern definition of [Monad] that expects to monads to be
    applicative functors, mostly because we don't need applicative functors,
    and also because it's hard to express them in the dependent version. *)

Class Monad@{r d} (V: Type@{r} -> Type@{d}) {FV: Functor V} :=
  mk_Monad
    {
      mbind {A B: Type@{r}} (f: A -> V B) (v: V A): V B;
      mreturn {A: Type@{r}} (a: A): V A;

      (* Monad laws, over Leibniz equality *)
      monad_identity_left {A B: Type@{r}} (a: A) (f: A -> V B): mbind f (mreturn a) = f a;
      monad_identity_right {A: Type@{r}} (v: V A): mbind mreturn v = v;
      monad_assoc {A B C: Type@{r}} (v: V A) (f: A -> V B) (g: B -> V C): mbind g (mbind f v) = mbind (fun x => mbind g (f x)) v;
      monad_fmap {A B: Type@{r}} (f: A -> B) (m: V A): fmap f m = mbind (mreturn ∘ f) m
    }.

Delimit Scope monad_scope with monad.
Infix "=<<" := mbind (at level 50, left associativity) : monad_scope.
Infix ">>=" := (flip mbind) (at level 60, right associativity) : monad_scope.

Hint Rewrite ->
     @monad_identity_left
     @monad_identity_right
  : monad.

Hint Rewrite <-
     @monad_assoc
     @monad_fmap
  : monad.

Lemma elim_compose {A B C : Type} (f : A -> B) (g : B -> C) :
  (g ∘ f) = fun x => g (f x).
Proof.
  trivial.
Qed.

Hint Rewrite -> @elim_compose : monad.

(** [fmap_compose'] is a version of [fmap_compose] without explicit
    composition in its RHS, which may be easier for rewrites. *)
Lemma fmap_compose'
      {X Y Z : Type} {V : Type -> Type}
      {FV : Functor V}
      (f : X -> Y) (g : Y -> Z) (v : V X) :
  fmap (g ∘ f) v = fmap g (fmap f v).
Proof.
  apply fmap_compose.
Qed.

Section kleisli.

  Context {V  : Type -> Type}
          {FV : Functor V}
          {MV : Monad   V}.

  (** [kcompose] is Kleisli composition. *)

  Definition kcompose {A B C : Type} (f : A -> V B) (g : B -> V C) : A -> V C :=
    mbind g ∘ mbind f ∘ mreturn.

  Hint Unfold kcompose compose : monad.

  (** These are the monad laws, but over Kleisli composition. *)

  Lemma kleisli_identity_left (A B C : Type) (v : B) (g : B -> V C) :
    kcompose mreturn g v = g v.
  Proof.
    autounfold with monad.
    now autorewrite with monad.
  Qed.

  Hint Rewrite @kleisli_identity_left : monad.

  Lemma kleisli_identity_right (A B C : Type) (v : B) (f : B -> V C) :
    kcompose f mreturn v = f v.
  Proof.
    autounfold with monad.
    now autorewrite with monad.
  Qed.

  Hint Rewrite @kleisli_identity_right : monad.

  Lemma kleisli_assoc (A B C D : Type) (v : A) (f : A -> V B) (g : B -> V C) (h : C -> V D) :
    kcompose (kcompose f g) h v = kcompose f (kcompose g h) v.
  Proof.
    autounfold with monad.
    now autorewrite with monad.
  Qed.
End kleisli.

Section id_monad.
  (** ** [id] forms a monad *)

  Global Instance id_Functor : Functor id :=
    {
      fmap A B := apply;
    }.
  Proof.
    all: intuition.
  Defined.

  Global Instance id_Monad : Monad id :=
    {
      mbind   A B := apply;
      mreturn A   := id;
    }.
  Proof.
    all: intuition.
  Defined.
End id_monad.

Infix ">=>" := kcompose (at level 60, right associativity) : monad_scope.