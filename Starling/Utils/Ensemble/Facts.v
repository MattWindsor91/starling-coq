(** * Miscellaneous results about ensembles *)

From Coq Require Import
     Classes.Morphisms
     Classes.RelationClasses
     Sets.Ensembles
     Sets.Constructive_sets.

Section ensemble_facts.
  Context {A : Type}.

  Global Instance Same_set_Equivalence: Equivalence (Same_set A).
  Proof.
    split.
    - split; intros a H; assumption.
    - split; destruct H as [H0 H1]; assumption.
    - intros x y z Hxy Hyz.
      destruct Hxy as [Hxy' Hyx].
      destruct Hyz as [Hyz' Hzy].
      split; intros a H.
      + apply Hyz', Hxy', H.
      + apply Hyx, Hzy, H.
  Defined.

  Lemma Ensemble_dot_assoc (x y z : Ensemble A) :
    Same_set A (Union A (Union A x y) z) (Union A x (Union A y z)).
  Proof.
    split.
    - intros a Hxy_z.
      destruct Hxy_z.
      + destruct H.
        * apply Union_introl, H.
        * apply Union_intror, Union_introl, H.
      + apply Union_intror, Union_intror, H.
    - intros a Hx_yz.
      destruct Hx_yz.
      + apply Union_introl, Union_introl, H.
      + destruct H.
        * apply Union_introl, Union_intror, H.
        * apply Union_intror, H.
  Qed.

  Lemma Ensemble_dot_comm (x y : Ensemble A) :
    Same_set A (Union A x y) (Union A y x).
  Proof.
    split; intros a Hxy; destruct Hxy.
    - apply Union_intror, H.
    - apply Union_introl, H.
    - apply Union_intror, H.
    - apply Union_introl, H.
  Qed.

  Lemma Ensemble_dot_one_l (x : Ensemble A) :
    Same_set A (Union A (Empty_set A) x) x.
  Proof.
    split; intros a Hx.
    - destruct Hx.
      (* Nothing can possibly be in the empty set! *)
      contradiction H.
      exact H.
    - apply Union_intror, Hx.
  Qed.

  Global Instance Included_PreOrder : PreOrder (Included A).
  Proof.
    split.
    - intros a x Ha; exact Ha.
    - intros a b c Hab Hbc x Ha.
      apply Hbc, Hab, Ha.
  Defined.
  
  Lemma Ensemble_inc_dot_left (a b c : Ensemble A) :
    Included A a b -> Included A (Union A a c) (Union A b c).
  Proof.
    intros Hab x [|].
    - apply Union_introl, Hab, H.
    - apply Union_intror, H.
  Qed.

  Lemma Ensemble_inc_sub_congr (a b c : Ensemble A) :
    Included A a b -> Included A (Setminus A a c) (Setminus A b c).
  Proof.
    intros Hab x [Ha Hc].
    split.
    - apply Hab, Ha.
    - apply Hc.
  Qed.

  Global Instance Included_PartialOrder : PartialOrder (Same_set A) (Included A).
  Proof.
    intros x y.
    split.
    - intro Heqv.
      apply Heqv.
    - intro Hinc.
      apply Hinc.
  Defined.

  Lemma Ensemble_sub_adjoint (a b c : Ensemble A) :
    Included A a (Union A b c) -> Included A (Setminus A a b) c.
  Proof.
    intros Ha_bc x Hab_c.
    destruct Hab_c as [Ha Hnb].
    destruct (Ha_bc x).
    - exact Ha.
    - contradiction H.
    - exact H.
  Qed.

  Lemma Ensemble_sub_one_r (a : Ensemble A) :
    Same_set A (Setminus A a (Empty_set A)) a.
  Proof.
    split; intros i Hin.
    - apply Hin.
    - apply Setminus_intro.
      + assumption.
      + apply Noone_in_empty.
  Qed.

  (** ** Other facts *)

  (** If [a] is a subset of [c], and [b] is a subset of [d], then the
      intersection of [a] and [b] is a subset of the intersection of
      [c] and [d]. *)

  Lemma intersection_included_congr (a b c d : Ensemble A) :
    Included A a c ->
    Included A b d ->
    Included A (Intersection A a b) (Intersection A c d).
  Proof.
    intros Hac Hbd x Hin_ab.
    apply Intersection_intro.
    - apply Hac, (Intersection_inv A a b), Hin_ab.
    - apply Hbd, (Intersection_inv A a b), Hin_ab.
  Qed.

  (** [iunion] behaves as an iterated union over a set of sets. *)
  
  Definition iunion (a : Ensemble (Ensemble A)) : Ensemble A :=
    fun x => exists s, In (Ensemble A) a s /\ In A s x.

  (** [set_lift] returns the union of [f s] for each [s] in [ss]. *)
  
  Definition set_lift (f : A -> Ensemble A) (ss : Ensemble A) : Ensemble A :=
    fun x => exists (s: A), In A ss s /\ In A (f s) x.

  Lemma set_lift_iunion (f : A -> Ensemble A) (ss : Ensemble A) :
    Same_set A (set_lift f ss) (iunion (fun a => exists b, In A ss b /\ a = f b)).
  Proof.
    split.
    - intros a (b & Hin_ss_b & Hin_fb_a).
      exists (f b).
      split.
      + exists b.
        repeat split; assumption.
      + assumption.
    - intros a (fb & (b & Hin_ss_b & ->) & Hin_fb_a).
      exists b.
      repeat split; assumption.
  Qed.
  
  (** If one set is included in another, so are their respective
      [set_lift]ed transformations. *)

  Global Instance set_lift_Proper_Included (f : A -> Ensemble A):
    Proper (Included A ==> Included A) (set_lift f).
  Proof.
    intros x y Hxy_inc z Hxz_in.
    unfold set_lift, In in *.
    destruct Hxz_in as (s & Hxs & Hfcsz).
    exists s.
    split.
    - apply Hxy_inc, Hxs.
    - exact Hfcsz.
  Qed.

  (** If one set is equivalent to another, so are their respective
      [set_lift]ed transformations. *)

  Global Instance set_lift_Proper_Same_set (f : A -> Ensemble A):
    Proper (Same_set A ==> Same_set A) (set_lift f).
  Proof.
    (* Trivially from the above. *)
    intros x y (Hxy_inc & Hyx_inc).
    split; apply set_lift_Proper_Included; assumption.
  Qed.

  Lemma set_lift_gen_dist (f : A -> Ensemble A) (x y : Ensemble A) :
    Included A (set_lift f x) y <->
    (forall a, In A x a -> Included A (f a) y).
  Proof.
    split.
    - intros Hinc a Hin_x_a e Hin_fa_e.
      apply Hinc.
      exists a.
      split; assumption.
    - intros Hinc a (b & Hin_x_b & Hin_fb_a).
      apply (Hinc b).
      + apply Hin_x_b.
      + apply Hin_fb_a.
  Qed.

  (** All sets on [A] are included in [Full_set A]. *)

  Lemma Full_set_inc (xs : Ensemble A) :
    Included A xs (Full_set A).
  Proof.
    intros x _.
    constructor.
  Qed.

  (** To show [Same_set] over a [Full_set], we need only show inclusion. *)

  Lemma Full_set_inc_same (xs : Ensemble A) :
    Included A (Full_set A) xs <-> Same_set A xs (Full_set A).
  Proof.
    split.
    - (* -> *)
      intro Hinc.
      split.
      + apply Full_set_inc.
      + apply Hinc.
    - (* <- *)
      intro Hsame.
      apply Hsame.
  Qed.

  (** Any superset of an inhabited set is inhabited. *)
  
  Lemma Included_Inhabited (xs ys : Ensemble A) :
    Included A xs ys -> Inhabited A xs -> Inhabited A ys.
  Proof.
    intros Hinc (x & Hin_x%Hinc).
    exists x.
    exact Hin_x.
  Qed.

  (** If an [Intersection] is inhabited, both of its members must be. *)

  Lemma Intersection_Inhabited (xs ys : Ensemble A) :
    Inhabited A (Intersection A xs ys) -> Inhabited A xs /\ Inhabited A ys.
  Proof.
    intros (x & (Hin_x_xs & Hin_x_ys)%Intersection_inv).
    split; exists x; assumption.
  Qed.
End ensemble_facts.
