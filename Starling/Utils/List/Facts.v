(** * Facts about lists *)

From Coq Require Import
     Arith.PeanoNat
     btauto.Btauto
     Bool.Bool
     Classes.SetoidDec
     Omega
     Relations.Relation_Definitions
     Lists.List
     Program.Basics
     Sets.Ensembles
     Permutation.

From Starling Require Import
     Utils.Monads.

Import ListNotations.
Open Scope program_scope.

Section list_facts_set.
  (** ** Switching from lists to constructive sets *)

  Context {A B : Type}.

  Variable f : A -> Ensemble B.

  (** [flat_map_to_set] maps a function returning a set over a list, and
      unites each resulting set together. *)

  Definition flat_map_to_set (xs: list A) : Ensemble B :=
    fun x => Exists (flip f x) xs.

  (** [flat_map_to_set] distributes across [cons]. *)

  Lemma flat_map_to_set_cons (x: A) (xs: list A):
    Same_set B
             (flat_map_to_set (x::xs))
             (Union B (f x) (flat_map_to_set xs)).
  Proof.
    split; intros s.
    - intros [Hin|Hin]%Exists_cons; intuition.
    - intros [|]; apply Exists_cons; intuition.
  Qed.

  (** [flat_map_to_set] distributes across [++]. *)

  Lemma flat_map_to_set_app (xs ys: list A):
    Same_set B
             (flat_map_to_set (xs ++ ys))
             (Union B (flat_map_to_set xs) (flat_map_to_set ys)).
  Proof.
    induction xs.
    - rewrite app_nil_l.
      split; intros s.
      + intuition.
      + intros [|]; intuition.
        now apply Exists_nil in H.
    - rewrite <- app_comm_cons.
      split; intros s.
      + intros [|]%flat_map_to_set_cons.
        * left.
          apply flat_map_to_set_cons.
          left.
          exact H.
        * apply IHxs in H.
          destruct H as [|].
          -- left.
             apply flat_map_to_set_cons.
             now right.
          -- now right.
      + intros [|].
        * apply flat_map_to_set_cons in H.
          destruct H as [|].
          -- now left.
          -- apply flat_map_to_set_cons.
             right.
             apply IHxs.
             now left.
        * apply flat_map_to_set_cons.
          right.
          apply IHxs.
          now right.
  Qed.
End list_facts_set.

Section list_facts_bool_funs.

  Local Set Implicit Arguments.

  (** ** Miscellaneous functions on decidable lists. *)

  Context {A : Type}.

  Hypothesis Heq_dec : forall (x y : A), { x = y } + { x <> y }.

  Lemma Heq_dec_comm (X : Type) (a b : A) (x y : X) :
    (if Heq_dec a b then x else y) =
    (if Heq_dec b a then x else y).
  Proof.
    destruct (Heq_dec a b), (Heq_dec b a); congruence.
  Qed.

  Lemma Heq_dec_refl (X : Type) (a : A) (x y : X) :
    (if Heq_dec a a then x else y) = x.
  Proof.
    now destruct (Heq_dec a a).
  Qed.

  (** [inb] decides whether [x] is in a list. *)

  Definition inb (x : A) : list A -> bool :=
    existsb (fun y => if Heq_dec x y then true else false).

  (** [inb] is a reflection of [In]. *)

  Lemma inb_In (x : A) (xs : list A) :
    List.In x xs <-> Is_true (inb x xs).
  Proof.
    split.
    - intros Hin.
      apply Is_true_eq_left, existsb_exists.
      exists x.
      split.
      + assumption.
      + apply Heq_dec_refl.
    - intros Hinb%Is_true_eq_true.
      unfold inb in Hinb.
      apply existsb_exists in Hinb.
      destruct Hinb as (x' & Hin & Hdec).
      destruct (Heq_dec x x'); congruence.
  Qed.

  Definition emptyb (l : list A) : bool :=
    if list_eq_dec Heq_dec l [] then true else false.

  Definition inclb (xs ys : list A) : bool :=
    forallb (flip inb ys) xs.

  Lemma forallb_not_existsb (l : list A) (p : A -> bool) :
    negb (forallb p l) = existsb (negb ∘ p) l.
  Proof.
    case_eq (negb (forallb p l)); intros Hfa; symmetry.
    - (* true *)
      apply negb_true_iff in Hfa.
      induction l.
      + discriminate.
      + cbn in *.
        apply orb_true_iff.
        apply andb_false_iff in Hfa.
        destruct Hfa as [Hfa|Hfa].
        * left.
          unfold compose, negb.
          now rewrite Hfa.
        * right.
          apply IHl, Hfa.
    - (* false *)
      apply negb_false_iff in Hfa.
      induction l.
      + reflexivity.
      + cbn in *.
        apply andb_true_iff in Hfa.
        destruct Hfa as (Hfa_pa & Hfa_pl).
        apply orb_false_iff.
        split.
        * apply negb_false_iff, Hfa_pa.
        * apply IHl, Hfa_pl.
  Qed.

  Lemma existsb_not_forallb (l : list A) (p : A -> bool) :
    negb (existsb p l) = forallb (negb ∘ p) l.
  Proof.
    case_eq (negb (existsb p l)); intros Hfa; symmetry.
    - (* true *)
      apply negb_true_iff in Hfa.
      induction l.
      + reflexivity.
      + cbn in *.
        apply orb_false_iff in Hfa.
        destruct Hfa as (Hfa_pa & Hfa_pl).
        apply andb_true_iff.
        split.
        * apply negb_true_iff, Hfa_pa.
        * apply IHl, Hfa_pl.
    - (* false *)
      induction l.
      + discriminate.
      + cbn in *.
        apply andb_false_iff.
        apply negb_false_iff, orb_true_iff in Hfa.
        destruct Hfa as [Hfa|Hfa].
        * left.
          unfold compose, negb.
          now rewrite Hfa.
        * right.
          apply IHl, negb_false_iff, Hfa.
  Qed.

  Lemma existsb_or (l : list A) (p1 p2 : A -> bool) :
    existsb (fun y => p1 y || p2 y) l = existsb p1 l || existsb p2 l.
  Proof.
    induction l.
    - reflexivity.
    - cbn.
      rewrite IHl.
      btauto.
  Qed.

  Corollary inb_comm (x : A) (l : list A) :
    inb x l =
    existsb (fun y => if Heq_dec y x then true else false) l.
  Proof.
    induction l.
    - reflexivity.
    - cbn.
      now rewrite IHl, Heq_dec_comm.
  Qed.
End list_facts_bool_funs.

Section list_facts_ForallAdjPairs.
  Context {A: Type}.
  Variable R : relation A.

  Inductive ForallAdjPairs: list A -> Prop :=
  | ForallAdjPairs_nil: ForallAdjPairs nil
  | ForallAdjPairs_one x: ForallAdjPairs (x::nil)
  | ForallAdjPairs_two x x' xs: R x x' -> ForallAdjPairs (x'::xs) -> ForallAdjPairs (x::x'::xs).

  Definition pair_bridge (xs ys: list A) : Prop :=
    match xs, ys with
    | nil, ys => True
    | xs, nil => True
    | x::xs, y::ys => R (last (x::xs) x) y
    end.

  Lemma ForallAdjPairs_top (x y: A) (xs: list A):
    ForallAdjPairs (x::y::xs) ->
    R x y.
  Proof.
    now inversion 1.
  Qed.

  Lemma last_snoc (a b x y: A) (xs: list A):
    last (x::y::xs) a = last (y::xs) b.
  Proof.
    induction xs; intuition.
    now destruct xs.
  Qed.

  Lemma ForallAdjPairs_snoc (x: A) (xs: list A):
    ForallAdjPairs (x::xs) ->
    ForallAdjPairs xs.
  Proof.
    inversion 1; intuition.
    apply ForallAdjPairs_nil.
  Qed.

  Lemma ForallAdjPairs_app (xs ys: list A):
    pair_bridge xs ys ->
    ForallAdjPairs xs ->
    ForallAdjPairs ys ->
    ForallAdjPairs (xs ++ ys).
  Proof.
    intros Hpb Hxs Hys.
    unfold pair_bridge in Hpb.
    destruct ys.
    - now rewrite app_nil_r.
    - induction xs; intuition.
      rewrite <- app_comm_cons.
      destruct xs.
      + apply ForallAdjPairs_two; intuition.
      + apply ForallAdjPairs_two.
        * now apply ForallAdjPairs_top in Hxs.
        * apply IHxs.
          -- erewrite <- last_snoc.
             apply Hpb.
          -- eapply ForallAdjPairs_snoc, Hxs.
  Qed.
End list_facts_ForallAdjPairs.

Section list_facts_max.
  (** ** Maximum operation *)


  Definition lmax : list nat -> nat :=
    fold_right max 0.

  (** [lmax] is no smaller than any of the items in its given list. *)

  Lemma lmax_le (xs : list nat) (x : nat):
    List.In x xs -> x <= lmax xs.
  Proof.
    induction xs; try easy.
    inversion 1; subst.
    + apply Nat.le_max_l.
    + transitivity (lmax xs); intuition.
      apply Nat.le_max_r.
  Qed.
End list_facts_max.

Section list_facts_sum.
  (** ** Sum operation *)

  Definition sum : list nat -> nat :=
    fold_right plus 0.

  Lemma sum_empty : sum [] = 0.
  Proof.
    intuition.
  Qed.

  Lemma sum_cons (x : nat) (xs : list nat) : sum (x::xs) = x + sum xs.
  Proof.
    reflexivity.
  Qed.

  Lemma sum_app (xs ys : list nat) : sum (xs++ys) = sum xs + sum ys.
  Proof.
    induction xs; cbn; omega.
  Qed.

  (** Permutations have the same sum. *)

  Lemma sum_Permutation (xs ys : list nat) :
    Permutation xs ys ->
    sum xs = sum ys.
  Proof.
    induction 1; intuition.
    - rewrite ! sum_cons.
      omega.
    - rewrite ! sum_cons.
      omega.
  Qed.

  Definition eq_nat
             (A : Type)
             (Heq_dec : forall x y : A, {x = y} + {x <> y}) (x y : A) : nat :=
    if Heq_dec y x then 1 else 0.

  Lemma count_occ_sum (A : Type) (Heq_dec : forall x y : A, {x = y} + {x <> y})
    (x : A) (xs : list A) :
    count_occ Heq_dec xs x = sum (List.map (eq_nat A Heq_dec x) xs).
  Proof.
    induction xs; cbn; unfold eq_nat; intuition.
    rewrite IHxs.
    now destruct (Heq_dec a x).
  Qed.

End list_facts_sum.

Section list_facts_Forall.
  Context {A: Type}.
  Variable (P: A -> Prop).

  (** A slightly easier to use forwards version of [Forall_forall]. *)

  Lemma Forall_forall_in (x : A) (xs : list A) :
    Forall P xs ->
    List.In x xs ->
    P x.
  Proof.
    intros Hforall Hin.
    apply (proj1 (Forall_forall _ _) Hforall x Hin).
  Qed.

  Lemma Forall_snoc (x: A) (xs: list A):
    Forall P (x::xs) ->
    Forall P xs.
  Proof.
    intro Hp.
    apply Forall_forall.
    intros k Hin_k.
    eapply Forall_forall in Hp.
    - exact Hp.
    - apply in_cons, Hin_k.
  Qed.

  Lemma Forall_app (xs ys: list A):
    List.Forall P xs ->
    List.Forall P ys ->
    List.Forall P (xs ++ ys).
  Proof.
    induction xs.
    - intros _ Hys.
      now rewrite List.app_nil_l.
    - rewrite <- List.app_comm_cons.
      intros Haxs Hys.
      apply Forall_cons.
      + eapply Forall_inv, Haxs.
      + apply IHxs.
        * eapply Forall_snoc, Haxs.
        * exact Hys.
  Qed.

  (** [Forall_ppa] is the converse of [Forall_app]. *)

  Lemma Forall_ppa (xs ys: list A):
    List.Forall P (xs ++ ys) ->
    List.Forall P xs /\ List.Forall P ys.
  Proof.
    induction xs; try easy.
    rewrite <- List.app_comm_cons.
    intros Haxsys.
    split.
    - apply Forall_cons.
      + eapply Forall_inv, Haxsys.
      + eapply IHxs, Forall_snoc, Haxsys.
    - eapply IHxs, Forall_snoc, Haxsys.
  Qed.

  (** A conjunction of [Forall]s is a [Forall] of conjunctions, and vice
      versa. *)

  Lemma Forall_conj (Q : A -> Prop) (xs : list A) :
    List.Forall (fun a => P a /\ Q a) xs <->
    List.Forall P xs /\ List.Forall Q xs.
  Proof.
    repeat (try split; intros; try apply Forall_forall);
      match goal with
      | [ H : Forall _ xs /\ Forall _ xs |- _ ] =>
        destruct H
      | _ => idtac
      end;
      match goal with
      | [ H : Forall ?P xs, H1 : List.In ?x xs |- _ ] =>
        apply (proj1 (Forall_forall _ xs) H x H1)
      end.
  Qed.

  (** We can extract a filtering predicate inside a [Forall] into a
      [filter], and vice versa. *)

  Lemma Forall_filter (Q : A -> bool) (xs : list A) :
    List.Forall (fun a => Q a = true -> P a) xs <->
    List.Forall P (filter Q xs).
  Proof.
    repeat (try split; intros; try apply Forall_forall).
    - apply filter_In in H0.
      destruct H0 as (Hin_xs & Hqx).
      revert Hqx.
      apply (proj1 (Forall_forall _ _) H x Hin_xs).
    - pose proof (proj2 (filter_In _ _ _) (conj H0 H1)) as Hin.
      apply (proj1 (Forall_forall _ _) H x Hin).
  Qed.

  (** We can restrict a [Forall] to a sublist. *)

  Lemma Forall_incl (xs ys : list A) :
    incl xs ys ->
    List.Forall P ys ->
    List.Forall P xs.
  Proof.
    intros Hincl Hfa.
    apply Forall_forall.
    intros x Hin%Hincl.
    now apply Forall_forall_in with (xs := ys).
  Qed.
End list_facts_Forall.

(** A specialisation of [Forall_conj] for lists of heterogeneous pairs. *)

Lemma Forall_conj_pairs (A B : Type) (P : A -> Prop) (Q : B -> Prop)
      (xs : list (A * B)) :
  List.Forall (fun '(a, b) => P a /\ Q b) xs <->
  List.Forall (fun '(a, _) => P a) xs /\ List.Forall (fun '(_, b) => Q b) xs.
Proof.
  split; intros Hfa.
  - apply Forall_conj, Forall_forall.
    intros (a & b) Hin_xs.
    apply (proj1 (Forall_forall _ _) Hfa _ Hin_xs).
  - apply Forall_conj in Hfa.
    apply Forall_forall.
    intros (a & b) Hin_xs.
    apply (proj1 (Forall_forall _ _) Hfa _ Hin_xs).
Qed.

(** [flat_map] distributes across [++]. *)

Lemma flat_map_app {A B: Type} (f: A -> list B) (xs ys: list A):
  flat_map f (xs ++ ys) = flat_map f xs ++ flat_map f ys.
Proof.
  now rewrite ! flat_map_concat_map, <- concat_app, map_app.
Qed.

(** [flat_map] over a cons turns into an append. *)

Lemma flat_map_cons {A B: Type} (f: A -> list B) (x: A) (xs: list A):
  flat_map f (x::xs) = f x ++ flat_map f xs.
Proof.
  intuition.
Qed.

(** [flat_map] over a nil turns into a nil. *)

Lemma flat_map_nil {A B: Type} (f: A -> list B):
  flat_map f [] = [].
Proof.
  intuition.
Qed.

(** [concat_length] relates the length of a [concat] to that of its
    constituent lists. *)

Lemma concat_length {A : Type} (xs : list (list A)) :
  length (concat xs) = sum (map (@length _) xs).
Proof.
  induction xs; cbn; eauto.
  now rewrite app_length, IHxs.
Qed.

Section list_facts_map.

  Context { A B : Type }.

  (** If we can show that [xs] is nil, then any mapping over [xs] is
      also nil. *)

  Lemma map_of_nil (f : A -> B) (xs : list A) :
    [] = xs -> [] = map f xs.
  Proof.
    now intros <-.
  Qed.

  (** If we can show that [ys] is a cons [x::xs], then any mapping
      over [ys] distributes as follows. *)

  Lemma map_of_cons (f : A -> B) (x : A) (xs ys : list A) :
    x::xs = ys -> (f x)::(map f xs) = map f ys.
  Proof.
    now intros <-.
  Qed.

  (** If we can split a mapping into two lists, we can derive
      a corresponding split in the original list. *)

  Lemma map_eq_app (f : A -> B) (xs : list A) (ys zs : list B) :
    map f xs = ys++zs ->
    exists ys' zs',
      xs = ys' ++ zs' /\ map f ys' = ys /\ map f zs' = zs.
  Proof.
    revert xs zs.
    induction ys; intros xs zs Hzs.
    - (* base case *)
      now (exists [], xs).
    - (* inductive case *)
      rewrite <- app_comm_cons in Hzs.
      destruct xs as [|x xs]; try easy.
      rewrite map_cons in Hzs.
      inversion Hzs; subst.
      destruct (IHys xs zs H1) as (ys' & zs' & -> & <- & <-).
      now (exists (x::ys'), zs').
  Qed.
End list_facts_map.

Section list_facts_app.
  Lemma app_split_same {A : Type}:
    forall (xs1a xs1b xs2a xs2b: list A),
      xs1a ++ xs1b = xs2a ++ xs2b ->
      length xs1a = length xs2a ->
      xs1a = xs2a /\ xs1b = xs2b.
  Proof.
    induction xs1a, xs2a; try discriminate; intros xs2b Hxs Hlength.
    - (* Both nil *)
      split.
      + reflexivity.
      + now rewrite ! app_nil_l in Hxs.
    - (* Both cons. *)
      rewrite <- ! app_comm_cons in Hxs.
      injection Hxs.
      intros Htail Ha.
      injection Hlength.
      intros Hlength_tail.
      destruct (IHxs1a xs1b xs2a xs2b Htail Hlength_tail) as (Has & Hbs).
      split.
      + now f_equal.
      + exact Hbs.
  Qed.

  Lemma app_split_length {A B : Type} (xs xs1 xs2 : list A) (ys ys1 ys2 : list B):
    xs = xs1 ++ xs2 ->
    ys = ys1 ++ ys2 ->
    length xs = length ys ->
    length xs1 = length ys1 ->
    length xs2 = length ys2.
  Proof.
    intros Hxs Hys Hlen Hlxs1.
    rewrite Hxs, Hys, ! app_length, Hlxs1 in Hlen.
    now apply Nat.add_cancel_l in Hlen.
  Qed.
End list_facts_app.

Hint Rewrite
     @flat_map_cons
     @flat_map_nil
     @flat_map_app
  : flat_map.

Section list_facts_pairwise_induction.

  (** ** Pairwise induction scheme over two parallel lists. *)

  Context
    {X Y : Type}.

  Variables
    (xs : list X)
    (ys : list Y).

  Hypothesis Heq_len : length xs = length ys.

  Variable P : list X -> list Y -> Prop.

  Hypothesis HPnil : P [] [].

  Hypothesis HPcons : forall (x : X) (xs' : list X) (y : Y) (ys' : list Y),
      P xs' ys' -> P (x::xs') (y::ys').

  Lemma list_pairwise_ind : P xs ys.
  Proof.
    pose proof (combine_split xs ys Heq_len) as Hsplit.
    revert Hsplit.
    clear Heq_len.
    remember (combine xs ys) as xys.
    clear Heqxys.
    revert xs ys.
    induction xys.
    - injection 1; now intros <- <-.
    - intros xs ys.
      unfold split.
      destruct a as (x & y).
      fold (split xys).
      destruct (split xys) as (xs' & ys') eqn:Hsplit.
      injection 1; intros <- <-.
      now apply HPcons, IHxys.
  Qed.

End list_facts_pairwise_induction.

Section list_facts_setoid_ops.
  Context
    {A    : Type}
    {S_A  : Setoid A}
    {DS_A : EqDec S_A}.

  Definition equiv_nat (x y : A) : nat :=
    if y == x then 1 else 0.

  Hint Resolve equiv_nat.
  Hint Unfold equiv_nat.

  Lemma equiv_nat_comm (x y : A) :
    equiv_nat x y = equiv_nat y x.
  Proof.
    unfold equiv_nat.
    destruct (equiv_dec x y), (equiv_dec y x); intuition.
  Qed.

  Lemma equiv_nat_equiv (x y : A) :
    x == y -> equiv_nat x y = 1.
  Proof.
    unfold equiv_nat.
    destruct (equiv_dec y x); firstorder.
  Qed.

  Corollary equiv_nat_refl (x : A) :
    equiv_nat x x = 1.
  Proof.
    now apply equiv_nat_equiv.
  Qed.

  Lemma equiv_nat_not (x y : A) :
    x =/= y -> equiv_nat x y = 0.
  Proof.
    unfold equiv_nat.
    destruct (equiv_dec y x); firstorder.
  Qed.

  (** The value of [equiv_nat x y] is equal to the value of [equiv_nat x' y']
      if [x == x'] and [y == y']. *)

  Global Instance equiv_nat_Proper :
    Proper (SetoidClass.equiv ==> SetoidClass.equiv ==> eq) equiv_nat.
  Proof.
    intros x x' Hxe y y' Hye.
    unfold equiv_nat.
    destruct (equiv_dec y x), (equiv_dec y' x'); intuition.
    - rewrite Hye, Hxe in e.
      exfalso.
      now apply c.
    - rewrite Hye, Hxe in c.
      exfalso.
      now apply c.
  Defined.

  Hint Rewrite equiv_nat_refl : list_facts.

  Definition count_occ_equiv (x : A) : list A -> nat :=
    sum ∘ List.map (equiv_nat x).

  Lemma count_occ_count_occ_equiv (Heq_dec : forall x y : A, {x = y} + {x <> y})
    (x : A) (xs : list A) :
    count_occ Heq_dec xs x <= count_occ_equiv x xs.
  Proof.
    induction xs; cbn; unfold equiv_nat; intuition.
    destruct (equiv_dec a x) as [Heqv|Hneqv], (Heq_dec a x) as [->|Hneq]; intuition.
  Qed.

  Lemma count_occ_equiv_nil (x : A) : count_occ_equiv x [] = 0.
  Proof.
    intuition.
  Qed.

  Lemma count_occ_equiv_cons (x y : A) (ys : list A) :
    count_occ_equiv x (y::ys) = equiv_nat x y + count_occ_equiv x ys.
  Proof.
    intuition.
  Qed.

  Hint Rewrite count_occ_equiv_cons : list_facts.

  Corollary count_occ_equiv_cons_eq (x : A) (xs : list A) :
    count_occ_equiv x (x::xs) = S (count_occ_equiv x xs).
  Proof.
    now autorewrite with list_facts.
  Qed.

  Hint Rewrite <- app_comm_cons : list_facts.

  Lemma count_occ_equiv_app (x : A) (xs ys : list A) :
    count_occ_equiv x (xs++ys) = count_occ_equiv x xs + count_occ_equiv x ys.
  Proof.
    induction xs; autorewrite with list list_facts; intuition.
  Qed.

  Hint Rewrite count_occ_equiv_app : list_facts.

  Lemma count_occ_equiv_In (x : A) (xs : list A) :
    0 < count_occ_equiv x xs ->
    Exists
      (SetoidClass.equiv x)
      xs.
  Proof.
    induction xs; try easy.
    intros Heqv_a.
    rewrite count_occ_equiv_cons in Heqv_a.
    unfold equiv_nat in Heqv_a.
    destruct (a == x); intuition.
  Qed.

  Corollary count_occ_equiv_punchout (x : A) (n : nat) (xs : list A) :
    count_occ_equiv x xs = S n ->
    exists (y : A) (xs1 xs2 : list A),
      xs = xs1 ++ (y::xs2) /\
      SetoidClass.equiv x y /\
      count_occ_equiv x (xs1++xs2) = n.
  Proof.
    intros Heqv.
    assert (0 < count_occ_equiv x xs) as Hnz by (rewrite Heqv; apply Nat.lt_0_succ).
    apply count_occ_equiv_In in Hnz.
    destruct (proj1 (Exists_exists _ _) Hnz) as (y & Hy_in & Hy_eqv).
    destruct (In_split _ _ Hy_in) as (xs1 & xs2 & ->).
    exists y, xs1, xs2; intuition.
    (* Now to show that the occurrence count has gone down by one. *)
    autorewrite with list_facts in *.
    unfold equiv_nat in Heqv.
    destruct (equiv_dec y x); intuition.
  Qed.
End list_facts_setoid_ops.

Section list_monad.

  (** ** The list monad

      Lists famously form a monad (and functor), capturing nondeterminism. *)

  Global Instance list_Functor : Functor list :=
    {
      fmap := map
    }.
  Proof.
    - (* identity *)
      apply map_id.
    - (* compose *)
      intros A B C f g v.
      symmetry.
      apply map_map.
  Defined.

  Global Instance list_Monad : Monad list :=
    {
      mbind       := flat_map;
      mreturn _ x := [x];
    }.
  Proof.
    - (* left identity *)
      intros A B a f.
      now autorewrite with flat_map list.
    - (* right identity *)
      intros A v.
      induction v; autorewrite with flat_map list; intuition.
      now rewrite IHv.
    - (* left associativity *)
      intros A B C v f g.
      induction v; autorewrite with flat_map list; intuition.
      now rewrite IHv.
    - (* right associativity. *)
      intros A B f v.
      induction v; autorewrite with flat_map list; intuition.
  Defined.
End list_monad.

Section list_map_accum.

  Set Implicit Arguments.

  Definition map_accum_l
             (Acc X Y : Type)
             (f : Acc -> X -> Acc * Y)
             (init : Acc)
             (xs : list X)
    : (Acc * list Y) :=
    fold_left
      (fun '(acc, ys) x => let (acc', y) := f acc x in (acc', ys++[y]))
      xs
      (init, []).

  Lemma fold_left_step (X Y : Type) (f : Y -> X -> Y) (x : X) (xs : list X) (y : Y) :
    fold_left f (x::xs) y = fold_left f xs (f y x).
  Proof.
    reflexivity.
  Qed.

  Lemma fold_left_map' (X Y : Type) (xs : list X) (ys : list Y) (f : X -> Y) :
    (ys ++ map f xs) = fold_left (fun y x => y ++ [f x]) xs ys.
  Proof.
    revert ys.
    induction xs; cbn; firstorder.
    now rewrite <- (IHxs (ys ++ [f a])), <- app_assoc.
  Qed.

  Corollary fold_left_map (X Y : Type) (xs : list X) (f : X -> Y) :
    map f xs = fold_left (fun y x => y ++ [f x]) xs [].
  Proof.
    now rewrite <- fold_left_map'.
  Qed.

  Lemma fold_left_const_pair (X Y Z : Type) (xs : list X) (y : Y)
        (f : Y -> X -> Y) (z : Z) :
    fold_left (fun '(_, y) x => (z, f y x)) xs (z, y) =
    (z, fold_left f xs y).
  Proof.
    revert y.
    induction xs; cbn; firstorder.
  Qed.

  Lemma map_accum_l_map (X Y : Type) (xs : list X) (f : X -> Y) :
    map f xs = snd (map_accum_l (fun _ x => (tt, f x)) tt xs).
  Proof.
    unfold map_accum_l.
    now rewrite fold_left_map, fold_left_const_pair.
  Qed.
End list_map_accum.

Section Forall2.
  Lemma Forall2_concat {X Y : Type} (P : X -> Y -> Prop)
        (xs : list (list X)) (ys : list (list Y)) :
    Forall2 (Forall2 P) xs ys ->
    Forall2 P (concat xs) (concat ys).
  Proof.
    induction 1; intros; cbn; intuition.
    now apply Forall2_app.
  Qed.

  Section Forall2_basic.
    Variables X Y : Type.

    Variable P : X -> Y -> Prop.

    Lemma forall2_cons_l (x : X) (xs : list X) (y : Y) (ys : list Y) :
      Forall2 P (x::xs) (y::ys) ->
      P x y.
    Proof.
      now inversion 1.
    Qed.

    Lemma forall2_cons_r (x : X) (xs : list X) (y : Y) (ys : list Y) :
      Forall2 P (x::xs) (y::ys) ->
      Forall2 P     xs      ys.
    Proof.
      now inversion 1.
    Qed.

    (** If [Forall2] holds for two lists, it holds for their reverse. *)
    Lemma Forall2_rev (xs : list X) (ys : list Y) :
      Forall2 P xs ys ->
      Forall2 P (rev xs) (rev ys).
    Proof.
      induction 1.
      - now unfold rev.
      - apply Forall2_app; intuition.
    Qed.

    (** If we are mapping over two lists connected by [Forall2 Q], and [Q] on
      two items implies [P] on their mappings, we get [Forall2 P] across the
      mapped lists. *)

    Lemma Forall2_map {X' Y' : Type} (Q : X' -> Y' -> Prop)
          (f : X' -> X) (g : Y' -> Y) (xs : list X') (ys : list Y') :
      Forall2 Q xs ys ->
      (forall x y, Q x y -> P (f x) (g y)) ->
      Forall2 P (map f xs) (map g ys).
    Proof.
      induction 1; intros; constructor; intuition.
    Qed.

    (** As [Forall2_map], but only mapping over the LHS list. *)

    Corollary Forall2_map_l {X' : Type} (Q : X' -> Y -> Prop)
          (f : X' -> X) (xs : list X') (ys : list Y) :
      Forall2 Q xs ys ->
      (forall x y, Q x y -> P (f x) y) ->
      Forall2 P (map f xs) ys.
    Proof.
      replace ys with (map id ys) at 2 by apply map_id.
      apply Forall2_map.
    Qed.

    (** As [Forall2_map], but only mapping over the RHS list. *)

    Corollary Forall2_map_r {Y' : Type} (Q : X -> Y' -> Prop)
          (f : Y' -> Y) (xs : list X) (ys : list Y') :
      Forall2 Q xs ys ->
      (forall x y, Q x y -> P x (f y)) ->
      Forall2 P xs (map f ys).
    Proof.
      replace xs with (map id xs) at 2 by apply map_id.
      apply Forall2_map.
    Qed.
  End Forall2_basic.

  (** As [Forall2_map], but over [flat_map], and where the intermediate result
      derives [Forall2 P] from [Q]. *)

  Corollary Forall2_flat_map {X Y X' Y' : Type}
            (P : X -> Y -> Prop)
            (Q : X' -> Y' -> Prop)
        (f : X' -> list X) (g : Y' -> list Y) (xs : list X') (ys : list Y') :
    Forall2 Q xs ys ->
    (forall x y, Q x y -> Forall2 P (f x) (g y)) ->
    Forall2 P (flat_map f xs) (flat_map g ys).
  Proof.
    intros Hq Hxy.
    rewrite ! flat_map_concat_map.
    now apply Forall2_concat, (Forall2_map _ (Q := Q)).
  Qed.
End Forall2.

(** If a cons-cell is included in another list, so is its tail. *)

Lemma incl_snoc (X : Type) (x : X) (xs ys : list X) :
  incl (x::xs) ys ->
  incl     xs  ys.
Proof.
  intros Hincl y Hin_xs.
  apply Hincl, in_cons, Hin_xs.
Qed.

(** [ni] is a flipped version of In.
    It's useful in some partial application scenarios. *)

Definition ni {A : Type} (xs : list A) (x : A) : Prop :=
  List.In x xs.

Lemma rev_cons {A : Type} (x : A) (xs : list A) :
  rev (x::xs) = rev xs ++ [x].
Proof.
  firstorder.
Qed.

(** Reverse list equality entails equality. *)

Lemma rev_inj {A : Type} (xs ys : list A) :
  rev xs = rev ys -> xs = ys.
Proof.
  revert ys.
  induction xs; cbn; intros [|y ys] Hrev.
  - reflexivity.
  - change (y :: ys) with ([y] ++ ys) in *.
    rewrite rev_app_distr in Hrev.
    now destruct (rev ys).
  - now destruct (rev xs).
  - rewrite rev_cons in Hrev.
    apply app_inj_tail in Hrev.
    destruct Hrev as (Hrev & ->).
    f_equal.
    firstorder.
Qed.

(** [dissolve_Forall2] is a simple tactic for reducing compound [Forall2]s in goal
    and hypothesis position. *)

Ltac dissolve_Forall2 :=
  repeat
    match goal with
    | H: Forall2 _ (?a::?b) (?c::?d) |- _ =>
      inversion_clear H
    | [ |- Forall2 _ (?a::?b) (?c::?d) ] =>
      apply Forall2_cons
    | [ |- Forall2 _ (?a::?b) ([?c] ++ ?d) ] =>
      apply Forall2_cons
    | [ |- Forall2 _ (?a ++ ?b) (?c ++ ?d) ] =>
      apply Forall2_app
    | [ |- Forall2 _ (rev ?a) (rev ?b) ] =>
      apply Forall2_rev
    end.