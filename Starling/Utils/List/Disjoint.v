(** * Disjointness for decidable lists *)

From Coq Require Import
     Bool.Bool
     Lists.List
     Program.Basics.     

From Starling Require Import
     Utils.List.Facts.

Set Implicit Arguments.

Section list_disjoint.

  Local Open Scope program_scope.

  Variable (A : Type).

  (** To decide list disjointness, we need equality decidability. *)
  
  Hypothesis Heq_dec : forall (x y : A), { x = y } + { x <> y }.

  (** [ldisjb] is a decision process for list disjointness. *)
  
  Definition ldisjb (l l' : list A) : bool :=
    forallb (negb ∘ flip (inb Heq_dec) l) l'.

  (** [ldisjb] is commutative. *)
  
  Lemma ldisjb_comm (l l' : list A) :
    ldisjb l l' = ldisjb l' l.
  Proof.
    unfold ldisjb.
    induction l, l'.
    - reflexivity.
    - apply forallb_forall.
      reflexivity.
    - symmetry.
      apply forallb_forall.
      reflexivity.
    - cbn.
      rewrite <- IHl.
      clear IHl.
      rewrite <- ! existsb_not_forallb.        
      unfold compose, flip.
      cbn.
      destruct (Heq_dec a0 a) as [Hva|Hnva], (Heq_dec a a0) as [Hav|Hnav]; subst;
        try contradiction.
      + reflexivity.
      + cbn.
        (* Drop the (negb (inb v l)) bit. *)
        rewrite negb_orb, andb_assoc.
        symmetry.
        rewrite andb_comm, andb_assoc, andb_comm.         
        f_equal.
        (* Now show that these two exists are equivalent...! *)
        rewrite <- negb_orb.
        f_equal.
        rewrite existsb_or, inb_comm.
        apply orb_comm.
  Qed.

  (** [ldisj] is a proof form of [ldisjb]. *)
  
  Definition ldisj (l l' : list A) : Prop := Is_true (ldisjb l l').

  (** It is also commutative. *)
  
  Corollary ldisj_comm (l l' : list A) :
    ldisj l l' -> ldisj l' l.
  Proof.
    apply eq_bool_prop_elim, ldisjb_comm.
  Qed.
  
  (** If we have disjointness between two lists [l] and [l'],
      and we know that [a] is in [l],
      it can't be in [l'].

      Since disjointness is commutative, we don't need the other direction. *)
  
  Definition ldisj_excl (l l' : list A) (a : A) :
    ldisj l l' -> inb Heq_dec a l = true -> inb Heq_dec a l' = false.
  Proof.
    intros Hldisj%Is_true_eq_true Hinl%Is_true_eq_left%inb_In.
    unfold ldisjb in Hldisj.
    rewrite <- existsb_not_forallb in Hldisj.
    apply negb_true_iff in Hldisj.
    rewrite <- Hldisj.
    symmetry.
    case_eq (inb Heq_dec a l'); intro Hinb.
    - apply existsb_exists.
      exists a.
      split.
      + eapply inb_In, Is_true_eq_left, Hinb.
      + eapply Is_true_eq_true, inb_In, Hinl.
    - exact Hldisj.
  Qed.

  (** We can't be in both lists at the same time. *)
  
  Corollary ldisj_not_both (l l' : list A) (a : A) :
    ldisj l l' -> inb Heq_dec a l = true -> inb Heq_dec a l' = true -> False.
  Proof.
    intros Hdisj Hin_l Hin_l'.
    eapply eq_true_false_abs.
    - exact Hin_l'.
    - now apply ldisj_excl with (l := l).
  Qed.
End list_disjoint.