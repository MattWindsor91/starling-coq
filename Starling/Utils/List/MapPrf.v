(** * Mapping of functions dependent on membership proofs

    This file contains [map_prf] and [map_prfA], which behave like
    [List.map], but accept functions that attach [List.In] and
    [SetoidList.InA] proofs to each element (respectively) before
    mapping. *)

From Coq Require Import
     Lists.List
     Lists.SetoidList
     Lists.SetoidPermutation
     Relations
     RelationPairs
     Classes.SetoidClass
     Classes.SetoidDec
.

Import EqNotations.
Import ListNotations.
Local Open Scope signature_scope.
Set Implicit Arguments.


Section widen.

  Variables
    A : Type.

  (** [widen] widens the scope of a membership proof attached to a
      value. *)

  Definition widen {ls1 ls2 : list A}
             (Hnarr : incl ls1 ls2)
             (a : { x : A | In x ls1 }) :
    { x : A | In x ls2 } :=
    exist _ (proj1_sig a) (Hnarr (proj1_sig a) (proj2_sig a)).

End widen.

Section map_prf.

  Variables
    A B : Type.

  Definition cons_lift
             {x : A}
             {xs ys : list A}
             (Hxs : ys = x::xs)
             (f : {x : A | List.In x ys} -> B)
             (y : {x : A | List.In x xs}) :
    B :=
    f (exist _
             (proj1_sig y)
             (rew <- Hxs in in_cons x (proj1_sig y) xs (proj2_sig y))).

  Lemma cons_lift_ext
        {x : A}
        {xs ys : list A}
        (Hxs : ys = x::xs)
        (f : {x : A | List.In x ys} -> B)
        (a : A)
        (Hin_cons : In a ys)
        (Hin_norm : In a xs) :
    (forall x y, proj1_sig x = proj1_sig y -> f x = f y) ->
    f (exist _ a Hin_cons) = cons_lift Hxs f (exist _ a Hin_norm).
  Proof.
    firstorder.
  Qed.

  Fixpoint map_prf
           (xs  : list A)
           (f   : {x : A | List.In x xs} -> B) :
    list B :=
    match xs as o return (xs = o -> list B) with
    | [] => fun _ => []
    | x::xs' =>
      fun Hxs =>
        f (exist _ x (rew <- Hxs in (in_eq x xs')))
          :: (map_prf xs' (cons_lift Hxs f))
    end (reflexivity _).

  Local Open Scope program_scope.

  Lemma map_prf_map (xs : list A) (f : A -> B) :
    map f xs = map_prf xs (f ∘ @proj1_sig _ _).
  Proof.
    induction xs; cbn; unfold compose, proj1_sig; firstorder.
    now rewrite IHxs.
  Qed.

  Lemma map_prf_nil (f : {x : A | List.In x []} -> B) :
    map_prf [] f = [].
  Proof.
    trivial.
  Qed.

  Lemma map_prf_cons (x : A) (xs : list A) (f : {y : A | List.In y (x::xs)} -> B) :
    map_prf (x::xs) f =
    (f (exist _ x (in_eq x xs)))
      ::
      map_prf xs (fun y => f (exist _ (proj1_sig y) (in_cons x (proj1_sig y) xs (proj2_sig y)))).
  Proof.
    trivial.
  Qed.

  Lemma map_prf_eql {SA : Setoid A} {SB : Setoid B}
        (xs ys : list A) (f : {x : A | List.In x xs} -> B) (g : {y : A | List.In y ys} -> B) :
    (forall x y, proj1_sig x == proj1_sig y -> f x == g y) ->
    eqlistA equiv xs ys ->
    eqlistA equiv (map_prf xs f) (map_prf ys g).
  Proof.
    intro Hpermut.
    induction 1; try easy.
    rewrite ! map_prf_cons.
    constructor; intuition.
  Qed.

  Lemma eqlistA_eq {X : Type} (x y : list X) :
    x = y <-> eqlistA (equiv (Setoid := eq_setoid X)) x y.
  Proof.
    split.
    - now intros ->.
    - induction 1; cbn in *; subst; intuition.
  Qed.

  Lemma map_prf_eq
        (xs ys : list A) (f : {x : A | List.In x xs} -> B) (g : {y : A | List.In y ys} -> B) :
    (forall x y, proj1_sig x = proj1_sig y -> f x = g y) ->
    xs = ys ->
    map_prf xs f = map_prf ys g.
  Proof.
    rewrite ! eqlistA_eq.
    apply (map_prf_eql (SA := eq_setoid A) (SB := eq_setoid B)).
  Qed.

  (** We get a result similar to [in_map], but only if the function has
      proof irrelevance. *)

  Lemma in_map_prf (xs : list A) (f : { x : A | List.In x xs } -> B)
        (a : { x : A | List.In x xs}) :
    (forall x y, proj1_sig x = proj1_sig y -> f x = f y) ->
    In (f a) (map_prf xs f).
  Proof.
    revert f.
    destruct a as (a & Ha).
    induction xs; intros f Hprfirr; intuition.
    rewrite map_prf_cons.
    inversion Ha; subst.
    - rewrite (Hprfirr (exist _ a Ha) (exist _ a (in_eq a xs)) (reflexivity _)).
      apply in_eq.
    - apply in_cons.
      rewrite cons_lift_ext with (Hin_norm := H) (Hxs := reflexivity _);
        try easy.
      apply IHxs.
      (* Now show that the proof irrelevance result carries along. *)
      intros (x & Hx) (y & Hy).
      cbn.
      intros ->.
      now apply Hprfirr.
  Qed.

  Lemma in_map_prf_iff (xs : list A) (f : { x : A | List.In x xs } -> B)
        (b : B) :
    (forall x y, proj1_sig x = proj1_sig y -> f x = f y) ->
    In b (map_prf xs f) <->
    exists a, b = f a.
  Proof.
    split.
    - induction xs; try easy.
      intros [Hbl|Hbr].
      + subst.
        now eexists.
      + apply IHxs in Hbr; firstorder.
    - intros (a & ->).
      now apply in_map_prf.
  Qed.
End map_prf.

Section map_prfA.

  Variables
    (A B : Type)
    (RA  : relation A).

  Context
    {ERA : Equivalence RA}.

  Fixpoint map_prfA
           (xs  : list A)
           (f   : {x : A | InA RA x xs} -> B) :
    list B :=
    match xs as o return (xs = o -> list B) with
    | [] => fun _ => []
    | x::xs' =>
      fun Hxs =>
        f (exist _ x (rew <- [InA RA x] Hxs in (In_InA ERA (in_eq x xs'))))
          :: (map_prfA (fun y => f (exist _
                                       (proj1_sig y)
                                       (rew <- [InA RA (proj1_sig y)] Hxs in InA_cons_tl x (proj2_sig y)))))
    end (reflexivity _).

  Lemma map_prfA_nil
        (f : {x : A | InA RA x []} -> B) :
    map_prfA f = [].
  Proof.
    trivial.
  Qed.

  Lemma map_prfA_cons
        (x : A)
        (xs : list A)
        (f : {a : A | InA RA a (x::xs)} -> B) :
    map_prfA f =
    (f (exist _ x (In_InA ERA (in_eq x xs))))
      ::
      map_prfA (fun y => f (exist _ (proj1_sig y) (InA_cons_tl x (proj2_sig y)))).
  Proof.
    trivial.
  Qed.

  Lemma map_prfA_eql
        {RB  : relation B}
        {ERB : Equivalence RB}
        (xs ys : list A) (f : {x : A | InA RA x xs} -> B) (g : {y : A | InA RA y ys} -> B) :
    (forall x y, RA (proj1_sig x) (proj1_sig y) -> RB (f x) (g y)) ->
    eqlistA RA xs ys ->
    eqlistA RB (map_prfA f) (map_prfA g).
  Proof.
    intro Hpermut.
    induction 1; try easy.
    rewrite ! map_prfA_cons.
    constructor; intuition.
  Qed.

  Lemma map_prfA_permut
        {RB  : relation B}
        {ERB : Equivalence RB}
        (xs ys : list A) (f : {x : A | InA RA x xs} -> B) (g : {y : A | InA RA y ys} -> B) :
    (forall x y, RA (proj1_sig x) (proj1_sig y) -> RB (f x) (f y)) ->
    (forall x y, RA (proj1_sig x) (proj1_sig y) -> RB (f x) (g y)) ->
    PermutationA RA xs ys ->
    PermutationA RB (map_prfA f) (map_prfA g).
  Proof.
    intros Hproper_ff Hproper_fg Hpermut.
    induction Hpermut; try easy.
    - (* Skip *)
      rewrite ! map_prfA_cons.
      constructor; intuition.
    - (* Swap *)
      rewrite ! map_prfA_cons.
      cbn.
      etransitivity.
      {
        apply permA_swap.
      }
      repeat constructor; try (now apply Hproper_fg).
      apply eqlistA_PermutationA, map_prfA_eql; intuition.
    - (* Transitivity *)
      transitivity (map_prfA (fun z => f (exist _ (proj1_sig z) (proj2 (PermutationA_equivlistA ERA Hpermut1 (proj1_sig z)) (proj2_sig z))))).
      + apply IHHpermut1; intuition.
      + apply IHHpermut2; intuition.
  Qed.

  (** [in_map_prfA] is the closest we get to [in_map_prf] for [map_prfA]. *)

  Lemma in_map_prfA
        {RB  : relation B}
        {ERB : Equivalence RB}
        (xs : list A) (f : { x : A | InA RA x xs } -> B)
        (a : { x : A | InA RA x xs}) :
    (forall x y, RA (proj1_sig x) (proj1_sig y) -> RB (f x) (f y)) ->
    InA RB (f a) (map_prfA f).
  Proof.
    revert f.
    destruct a as (a & Ha).
    induction xs; intros f Hproper_ff; try easy.
    rewrite map_prfA_cons.
    inversion Ha; subst; apply InA_cons.
    - firstorder.
    - right.
      remember
        (fun y =>
           f
             (exist (fun y0 : A => InA RA y0 (a0 :: xs)) (proj1_sig y)
                    (InA_cons_tl a0 (proj2_sig y))))  as f'.
      assert
        (RB (f (exist (fun x : A => InA RA x (a0 :: xs)) a Ha))
            (f' (exist (fun x : A => InA RA x xs) a H0))) as Hrb.
      {
        rewrite Heqf'.
        cbn.
        now apply Hproper_ff.
      }
      apply (InA_eqA ERB (symmetry Hrb)), IHxs.
      (* Now show that the proof irrelevance result carries along. *)
      rewrite Heqf'.
      intros (x & Hx) (y & Hy).
      cbn.
      intro Hr.
      now apply Hproper_ff.
  Qed.

  Lemma in_map_prfA_iff
        {RB  : relation B}
        {ERB : Equivalence RB}
        (xs : list A) (f : { x : A | InA RA x xs } -> B)
        (b : B) :
    Proper (RA @@ @proj1_sig _ _ ==> RB) f ->
    InA RB b (map_prfA f) <->
    exists a, RB b (f a).
  Proof.
    intro Hproper.
    split.
    - induction xs; try easy.
      cbn.
      intros [Hbl|Hbr]%InA_cons.
      + firstorder.
      + apply IHxs in Hbr; firstorder.
    - intros (a & ->).
      now apply in_map_prfA.
  Qed.
End map_prfA.