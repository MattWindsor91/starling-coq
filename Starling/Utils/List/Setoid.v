(** * List facts: Setoids *)

From Coq Require Import
     Lists.List
     Lists.SetoidList
     Lists.SetoidPermutation
     Classes.SetoidClass
     Classes.SetoidDec
     Permutation
.

From Starling Require Import
     Utils.List.Facts
.

Include ListNotations.
Local Open Scope list_scope.

Set Implicit Arguments.

Section list_facts_Setoid_PermutationA.
  (** Inverse of [Permutation_PermutationA], but only for the equality
    setoid. *)

  Lemma PermutationA_Permutation (T : Type) (xs ys : list T) :
    PermutationA eq xs ys -> Permutation xs ys.
  Proof.
    induction 1; subst; intuition; try constructor.
    econstructor.
    apply IHPermutationA1.
    trivial.
  Qed.

  Lemma PermutationA_length (T : Type) (R : relation T) (xs ys : list T) :
    PermutationA R xs ys ->
    length xs = length ys.
  Proof.
    induction 1; cbn; intuition.
  Qed.

  Global Instance map_PermutationA_Proper
         {A B : Type}
         {RA  : relation A}
         {ERA : Equivalence RA}
         {RB  : relation B}
         {ERB : Equivalence RB}
         (f : A -> B)
         {P : Proper (RA ==> RB) f} :
    Proper (PermutationA RA ==> PermutationA RB) (map f).
  Proof.
    intros xs ys Hpermut.
    induction Hpermut; try easy.
    - (* Skip *)
      rewrite ! map_cons.
      constructor; intuition.
    - (* Swap *)
      rewrite ! map_cons.
      cbn.
      etransitivity.
      {
        apply permA_swap.
      }
      repeat constructor; try (now apply P).
      reflexivity.
    - (* Transitivity *)
      transitivity (map f l₂).
      + apply IHHpermut1; intuition.
      + apply IHHpermut2; intuition.
  Defined.
End list_facts_Setoid_PermutationA.

Section list_facts_Setoid_Decidable.
  Variable A : Type.

  Context
    {SA : Setoid A}
    {DA : EqDec SA}.

  Import ListNotations.

  Definition equivb (x y : A) : bool :=
    if x == y then true else false.

  Lemma equivb_equiv (x y : A) :
    Bool.Is_true (equivb x y) <-> x == y.
  Proof.
    unfold equivb.
    destruct (x == y); intuition.
  Qed.

  Corollary equivb_equiv_reflect (x y : A):
    Bool.reflect (x == y) (equivb x y).
  Proof.
    apply Bool.iff_reflect.
    symmetry.
    transitivity (Bool.Is_true (equivb x y)).
    - split.
      + apply Bool.Is_true_eq_left.
      + apply Bool.Is_true_eq_true.
    - apply equivb_equiv.
  Qed.

  Definition inbA (x : A) : list A -> bool :=
    existsb (equivb x).

  Lemma inbA_InA (x : A) (xs : list A) :
    Bool.Is_true (inbA x xs) <-> InA SetoidClass.equiv x xs.
  Proof.
    split; intros Hin.
    - unfold inbA in Hin.
      apply Bool.Is_true_eq_true, existsb_exists in Hin.
      destruct Hin as (y & Hin_y & Hsd%Bool.Is_true_eq_left%equivb_equiv).
      apply InA_alt.
      now (exists y).
    - apply Bool.Is_true_eq_left, existsb_exists.
      apply InA_alt in Hin.
      destruct Hin as (y & Heq_y & Hin_y).
      exists y; intuition.
      now apply Bool.Is_true_eq_true, equivb_equiv.
  Qed.

  (** [dedupeA RB] is a procedure for removing duplicates, given an
      equivalence decision procedure [RB]. *)

  Definition dedupeA (RB : A -> A -> bool) : list A -> list A :=
    fold_right
      (fun x xs => if (existsb (RB x) xs) then xs else x::xs)
      [].

  (** If [RB] reflects a relation [R], then [dedupeA] eliminates
      duplicates modulo [R]. *)

  Lemma dedupeA_NoDupA_general (R : relation A) (RB : A -> A -> bool) (xs : list A) :
    (forall x y, Bool.reflect (R x y) (RB x y)) ->
    NoDupA R (dedupeA RB xs).
  Proof.
    induction xs; cbn; try easy.
    intro Hreflect.
    destruct (existsb (RB a) (dedupeA RB xs)) eqn:Hinb; intuition.
    constructor; intuition.
    contradict Hinb.
    apply Bool.not_false_iff_true, existsb_exists.
    apply InA_alt in H0.
    destruct H0 as (y & Hrel_y & Hin_y).
    exists y; intuition.
    specialize (Hreflect a y).
    apply Bool.reflect_iff in Hreflect; intuition.
  Qed.

  (** A specific example is using [equivb] to dedupe according to
  [equiv]. *)

  Corollary dedupeA_NoDupA (xs : list A) :
    NoDupA SetoidClass.equiv (dedupeA equivb xs).
  Proof.
    apply dedupeA_NoDupA_general, equivb_equiv_reflect.
  Qed.

  (** If [R] is an equivalence relation, and [RB] reflects it, then
      deduping on [RB] produces an [equivlistA] over [R]. *)

  Lemma dedupeA_equivlistA_general (R : relation A) {ER : Equivalence R} (RB : A -> A -> bool) (xs : list A) :
    (forall x y, Bool.reflect (R x y) (RB x y)) ->
    equivlistA R xs (dedupeA RB xs).
  Proof.
    intros Hreflect x.
    induction xs.
    - intuition.
    - split; intros Hin.
      + apply InA_cons in Hin.
        cbn.
        destruct (existsb (RB a) (dedupeA RB xs)) eqn:HinbA; intuition.
        apply existsb_exists in HinbA.
        destruct HinbA as (z & Hin_z & Hrefl_z).
        apply (Bool.reflect_iff _ _ (Hreflect a z)) in Hrefl_z.
        apply InA_eqA with (x := z); intuition.
        * symmetry.
          now transitivity a.
        * apply In_InA; intuition.
      + apply InA_cons.
        cbn in Hin.
        destruct (existsb (RB a) (dedupeA RB xs)) eqn:HinbA; intuition.
        apply InA_cons in Hin; intuition.
  Qed.

  Corollary dedupeA_inclA_general (R : relation A) {ER : Equivalence R} (RB : A -> A -> bool) (xs : list A) :
    (forall x y, Bool.reflect (R x y) (RB x y)) ->
    inclA R (dedupeA RB xs) xs.
  Proof.
    intros Hreflect x.
    apply dedupeA_equivlistA_general; intuition.
  Qed.

  Lemma dedupeA_equivlistA (xs : list A) :
    equivlistA SetoidClass.equiv xs (dedupeA equivb xs).
  Proof.
    apply dedupeA_equivlistA_general, equivb_equiv_reflect.
    intuition.
  Qed.

  Corollary dedupeA_inclA (xs : list A) :
    inclA SetoidClass.equiv (dedupeA equivb xs) xs.
  Proof.
    intro x.
    apply dedupeA_equivlistA.
  Qed.

  (** Deduplication never adds any new elements. *)

  Lemma dedupeA_incl_general (RB : A -> A -> bool) (xs : list A) :
    incl (dedupeA RB xs) xs.
  Proof.
    induction xs; intuition.
    cbn.
    destruct (existsb (RB a) (dedupeA RB xs)) eqn:HinbA; intuition.
  Qed.

  Lemma dedupeA_incl (xs : list A) :
    incl (dedupeA equivb xs) xs.
  Proof.
    apply dedupeA_incl_general.
  Qed.
End list_facts_Setoid_Decidable.

Section list_facts_Setoid_subrelations.
  Lemma eqlistA_subrel {T : Type} {F G : relation T} (xs ys : list T) :
    subrelation F G -> eqlistA F xs ys -> eqlistA G xs ys.
  Proof.
    intro Hsub.
    induction 1; intuition.
  Qed.

  (** If we've removed duplicates according to a larger equivalence
      relation, we've removed them according to a smaller one. *)

  Lemma NoDupA_subrel {T : Type} {F G : relation T} (xs : list T) :
    subrelation F G -> NoDupA G xs -> NoDupA F xs.
  Proof.
    intros Hsub HndG.
    induction xs; intuition.
    inversion HndG; subst.
    constructor; intuition.
    apply H1, InA_alt.
    apply InA_alt in H.
    destruct H as (y & Hrel%Hsub & Hin).
    now (exists y).
  Qed.
End list_facts_Setoid_subrelations.

Section list_facts_Setoid_eqlistA.
  Context {A : Type}.

  Variable RA : relation A.

  Lemma eqlistA_len (xs ys : list A) :
    eqlistA RA xs ys -> length xs = length ys.
  Proof.
    induction 1; cbn; intuition.
  Qed.

  Lemma map_eqlistA_eql
        {B}
        {ERA : Equivalence RA}
        {RB  : relation B}
        {ERB : Equivalence RB}
        (f g : A -> B)
        (xs ys : list A) :
    (forall x y, RA x y -> RB (f x) (g y)) ->
    eqlistA RA xs ys ->
    eqlistA RB (map f xs) (map g ys).
  Proof.
    intro Hprop.
    induction 1; try easy.
    constructor; intuition.
  Qed.

  Global Instance map_eqlistA_Proper
         {B}
         {ERA : Equivalence RA}
         {RB  : relation B}
         {ERB : Equivalence RB}
         (f : A -> B)
         {P : Proper (RA ==> RB) f} :
    Proper (eqlistA RA ==> eqlistA RB) (map f).
  Proof.
    intros xs ys Hprop.
    now apply map_eqlistA_eql.
  Qed.

  Lemma eqlistA_nil_l (xs : list A) :
    eqlistA RA [] xs <-> [] = xs.
  Proof.
    split.
    - now inversion 1.
    - now intros <-.
  Qed.

  Lemma eqlistA_cons_l (x : A) (xs ys : list A) :
    eqlistA RA (x::xs) ys <-> exists y ys', ys = y::ys' /\ RA x y /\ eqlistA RA xs ys'.
  Proof.
    split.
    - inversion 1; subst.
      now (exists x', l').
    - intros (y & ys' & -> & Hr & Hrs).
      now constructor.
  Qed.

  Section decision_process.
    Variable RBool : A -> A -> bool.

    (** [eqlistbA] is a decision process for [eqlistA]. *)

    Fixpoint eqlistbA (xs ys : list A) : bool :=
      match xs, ys with
      | [], []       => true
      | x::xs, y::ys => RBool x y && eqlistbA xs ys
      | _, _         => false
      end.

    Lemma eqlistbA_len (xs ys : list A) :
      eqlistbA xs ys = true -> length xs = length ys.
    Proof.
      revert ys.
      induction xs.
      - destruct ys; intuition.
      - intros ys Heql.
        destruct ys; intuition.
        apply Bool.andb_true_iff in Heql.
        destruct Heql as (_ & Heqlb).
        cbn.
        f_equal.
        now apply IHxs.
    Qed.

    Lemma eqlistbA_eqlistA_reflect :
      (forall x y, Bool.reflect (RA x y) (RBool x y)) ->
      (forall xs ys, Bool.reflect (eqlistA RA xs ys) (eqlistbA xs ys)).
    Proof.
      intros Hreflect xs ys.
      apply Bool.iff_reflect.
      split; intro Heql.
      - assert (length xs = length ys) as Hlen by now apply eqlistA_len in Heql.
        revert Heql.
        apply (list_pairwise_ind xs ys); intuition.
        inversion Heql; subst.
        apply Bool.andb_true_iff.
        split; intuition.
        now apply (Bool.reflect_iff _ _ (Hreflect x y)).
      - assert (length xs = length ys) as Hlen by now apply eqlistbA_len in Heql.
        revert Heql.
        apply (list_pairwise_ind xs ys); intuition.
        inversion Heql; subst.
        apply Bool.andb_true_iff in H1.
        constructor; intuition.
        now apply (Bool.reflect_iff _ _ (Hreflect x y)).
    Qed.
  End decision_process.
End list_facts_Setoid_eqlistA.