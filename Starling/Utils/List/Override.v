(** * List override

    The list override operator replaces index [i] of a list [xs] with
    [x], if [i] is in-bounds.  Otherwise, it is the identity. *)

From Coq Require Import
     Arith.PeanoNat
     Lists.List
     Omega.

From Coq Require Import ssreflect ssrfun ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


From Starling Require Import
     List.Facts.

Section list_override.

  Context
    {A : Type}. (* Type of elements *)

  Fixpoint list_override (xs : list A) (i : nat) (x : A) : list A :=
    match (i, xs) with
    | (_, nil) => nil
    | (0, _::xs') => x::xs'
    | (S n, x'::xs') => x'::(list_override xs' n x)
    end.

  Lemma list_override_nil (i : nat) (x : A) :
    list_override nil i x = nil.
  Proof.
    by case: i.
  Qed.

  Lemma list_override_cons_zero (x : A) (xs : list A) (v : A) :
    list_override (x::xs) 0 v = v::xs.
  Proof.
    by done.
  Qed.

  Lemma list_override_cons_succ (n : nat) (x : A) (xs : list A) (v : A) :
    list_override (x::xs) (S n) v = x::(list_override xs n v).
  Proof.
    by done.
  Qed.

  Lemma list_override_length (i : nat) (x : A) (xs : list A) :
    length (list_override xs i x) = length xs.
  Proof.
    move: i.
    elim: xs.
    - (* base case: xs is nil *) by move=>i; rewrite list_override_nil.
    - (* ind. case: xs is a::l *) move=> a l H.
      (* now induction on index *) elim.
      + (* base case *) by rewrite list_override_cons_zero.
      + (* ind. case *) by move=> n Hbase; rewrite list_override_cons_succ /= H.
  Qed.

  Lemma list_override_nth (n : nat) (xs : list A) (d v : A) :
    n < length xs ->
    nth n (list_override xs n v) d = v.
  Proof.
    move: n.
    elim: xs; first by move => Hlen; move: (Nat.nlt_0_r Hlen).
    (* inductive case. *)
    move=> a l Hind.
    elim; first by done.
    move=> n Hbase /= Hlen.
      by apply: Hind (proj2 (Nat.succ_lt_mono n (length l)) Hlen).
  Qed.

  Lemma list_override_app_r (n : nat) (xs ys : list A) (v : A) :
    n >= length xs ->
    list_override (xs ++ ys) n v = xs ++ list_override ys (n - length xs) v.
  Proof.
    move: n.
    elim: xs;
      (* Base case: xs is nil. *)
      first by move=> n _; rewrite app_nil_l Nat.sub_0_r.
    (* Ind. case: xs is x::xs *)
    move=> x xs Hind n Hlen.
    (* n must be [S k] for some k. *)
    elim: {Hlen}(Nat.lt_exists_pred (length xs) n Hlen) => {n}k [-> Hlen].
      by rewrite <- ! app_comm_cons, list_override_cons_succ, Hind.
  Qed.

  Lemma list_override_split (n : nat) (xs : list A) (d v : A) :
    n < length xs ->
    exists (xs1 xs2 : list A),
      xs = xs1 ++ (nth n xs d :: xs2) /\
      list_override xs n v = xs1 ++ (v :: xs2) /\
      length xs1 = n.
  Proof.
    move=> Hlen.
    move: {Hlen}(nth_split xs d Hlen) => [xs1] [xs2] [->] {n}<-.
    exists xs1, xs2.
    repeat split; last by rewrite list_override_app_r // Nat.sub_diag.
    have <- : nth (length xs1) xs d = nth (length xs1) (xs1 ++ nth (length xs1) xs d :: xs2) d; last by done.
      by rewrite app_nth2 // Nat.sub_diag.
  Qed.

  Lemma list_override_app_l (n : nat) (xs ys : list A) (v : A) :
    n < length xs ->
    list_override (xs ++ ys) n v = list_override xs n v ++ ys.
  Proof.
    move: n.
    elim: xs;
      (* Base case: xs is nil (contradiction) *)
      first by move=> n Hboom; move: (Nat.nlt_0_r _ Hboom).
    move => x xs Hind n.
    rewrite <- app_comm_cons.
    case: n; first by done.
    move=> n /= /(iffRL (Nat.succ_lt_mono _ _)) Hlen.
      by rewrite (Hind _ Hlen).
  Qed.

  Lemma list_override_nth_inv (n k : nat) (xs ys : list A) (d v : A) :
    n <> k ->
    nth k (list_override xs n v) d = nth k xs d.
  Proof.
    (* Is [k] in bounds?  If not, both sides become [d]. *)
    case: (Nat.le_gt_cases (length xs) k) => Hlen.
    - (* No. *)
      have Hlen': (length (list_override xs n v) <= k)
        by rewrite list_override_length.
        by rewrite ! nth_overflow.
    - (* Yes, so split the list accordingly, and case split on which side
         we're indexing with [k]. *)
      case:
        (nth_split xs d Hlen) (Nat.lt_trichotomy k n)
      => [lefts] [rights] [Hxs] <- [Hleft | [<- | Hright]];
          rewrite Hxs in Hlen; rewrite Hxs //; move {Hxs}.
      + (* Left side. *)
        rewrite list_override_app_r ; first by apply Nat.lt_le_incl.
        rewrite ! app_nth2 // Nat.sub_diag.
        (* n - length lefts must be S j for some j. *)
        have Hnsk: (0 < n - length lefts) by rewrite <- Nat.lt_add_lt_sub_r, Nat.add_0_l.
        by move: (Nat.lt_exists_pred 0 _ Hnsk) => [j] [->] {Hnsk}_.
      + (* Right side. *)
        rewrite list_override_app_l // app_nth2;
          first by rewrite list_override_length.
        move => _.
        by rewrite app_nth2 // list_override_length.
  Qed.

  Lemma list_override_unsplit (n : nat) (xs xs1 xs2 : list A) (d v : A) :
    list_override xs n v = xs1 ++ (v :: xs2) ->
    length xs1 = n ->
    xs = xs1 ++ (nth n xs d :: xs2).
  Proof.
    move=> Hsplit Hlxs1.
    have Hlxs: n < length (list_override xs n v)
      by rewrite Hsplit app_length Hlxs1 Nat.add_succ_r; omega.
    have {Hlxs}Hlxs: n < length xs
      by rewrite list_override_length in Hlxs.
    elim: (nth_split xs d Hlxs) => [xs3] [xs4] [Hxs] [Hlxs3].
    rewrite {1}Hxs.
    rewrite Hxs list_override_app_r in Hsplit; first by rewrite Hlxs3.
    rewrite Hlxs3 Nat.sub_diag list_override_cons_zero in Hsplit.
    rewrite <- Hlxs1 in Hlxs3.
    case: (app_split_same _ _ _ _ Hsplit Hlxs3) => [->] Hxvs.
    by case: Hxvs => [->].
  Qed.

  Lemma list_override_overflow (n : nat) (xs : list A) (x : A) :
    n >= length xs ->
    xs = list_override xs n x.
  Proof.
    move=> Hn.
    rewrite <- app_nil_r at 2.
    rewrite list_override_app_r //.
    by rewrite list_override_nil app_nil_r.
  Qed.

  Lemma list_override_idem (n : nat) (d : A) (xs : list A) :
    xs = list_override xs n (nth n xs d).
  Proof.
    (* Is [n] in bounds? *)
    case: (Nat.lt_ge_cases n (length xs)) => Hbound.
    - (* Yes. *)
      case: (list_override_split d (nth n xs d) Hbound)
      => [xs1] [xs2] [->] [->] [Hln].
      f_equal.
      f_equal.
      rewrite app_nth2 Hln ?(Nat.sub_diag) //.
    - (* No. *)
      by apply list_override_overflow.
  Qed.
End list_override.