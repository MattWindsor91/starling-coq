
(** * View expressions: typeclass instances *)

From Coq Require Import
     Program.Basics (* compose *)
     Relations.Relation_Definitions
     Classes.DecidableClass
     Classes.RelationPairs
     Classes.SetoidClass.

From Starling Require Import
     Utils.Monads
     Views.Expr.Type
     Views.Expr.AtomLanguage
     Views.Classes
.

Set Implicit Arguments.

Section views_expr_instances.
  Variables
    (A V : Type).

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.  

  Local Open Scope signature_scope.

  Global Instance interpret_sm_Measure : Measure (interpret_sm (al_inject A V)).
  
  Section views_expr_instances_Setoid.
    (** [vequiv] is the equivalence on view expressions induced by
          their respective interpretations. *)
    
    Definition vequiv : relation (VExpr A) := equiv@@(interpret_sm (al_inject A V)).

    Global Instance vequiv_Equivalence: Equivalence vequiv := _.
    Global Instance VExpr_Setoid: Setoid (VExpr A) :=
      { equiv := vequiv }.
  End views_expr_instances_Setoid.
  
  Section views_expr_instances_ViewsSemigroup.
    Lemma Join_assoc (x y z: VExpr A):
      Join (Join x y) z == Join x (Join y z).
    Proof.
      apply dot_assoc.
    Qed.
    
    Lemma Join_comm (x y: VExpr A):
      Join x y == Join y x.
    Proof.
      apply dot_comm.
    Qed.

    Lemma vequiv_Join_left (x y z: VExpr A):
      x == y -> Join x z == Join y z.
    Proof.
      intro Hxy.
      apply equiv_dot_left, Hxy.
    Qed.
    
    Global Instance VExpr_ViewsSemigroup: ViewsSemigroup (VExpr A) :=
      {
        dot := Join (A := A);

        dot_assoc := Join_assoc;
        dot_comm := Join_comm;
        equiv_dot_left := vequiv_Join_left
      }.
  End views_expr_instances_ViewsSemigroup.

  Section views_expr_instances_ViewsMonoid.
    Lemma Join_Unit_l (x: VExpr A):
      (Unit A * x)%views == x.
    Proof.
      apply dot_one_l.
    Qed.
    
    Global Instance VExpr_ViewsMonoid: ViewsMonoid (VExpr A) :=
      {
        one := Unit A;

        dot_one_l := Join_Unit_l
      }.
  End views_expr_instances_ViewsMonoid.

  Section views_expr_instances_OrderedViewsSemigroup.      
    (** [vinc] is the ordering on view expressions induced by
          their respective interpretations. *)

    Definition vinc : relation (VExpr A) := inc@@(interpret_sm (al_inject A V)).
    
    Global Instance vinc_PreOrder : PreOrder vinc := {}.
    Global Instance vinc_PartialOrder: PartialOrder vequiv vinc.
    Proof.
      intros x y.
      split.
      - (* Equivalence implies inclusion *)
        intro Heq.
        split; apply equiv_inc.
        + apply Heq.
        + apply symmetry, Heq.
      - (* Inclusion implies equivalence *)
        intros (Hinc_xy & Hinc_yx).
        apply inc_antisymm; assumption.
    Defined.

    Lemma vinc_Join_left (x y z: VExpr A):
      vinc x y -> vinc (x * z)%views (y * z)%views.
    Proof.
      intros Hinc_xy.
      apply inc_dot_left, Hinc_xy.
    Qed.

    Lemma vinc_Join_mono_left (x y: VExpr A):
      vinc x (x * y)%views.
    Proof.
      apply inc_dot_mono_left.
    Qed.
    
    Global Instance VExpr_OrderedViewsSemigroup:
      OrderedViewsSemigroup (VExpr A) :=
      {
        inc := vinc;

        inc_dot_left := vinc_Join_left;
        inc_dot_mono_left := vinc_Join_mono_left
      }.      
  End views_expr_instances_OrderedViewsSemigroup.

  Section views_expr_instances_DecOrderedViewsSemigroup.

    (** Decidable order isn't assumed for views expressions in general. *)
    
    Context {DV : DecOrderedViewsSemigroup V}.

    Instance vinc_Decidable (x y : VExpr A) :
      Decidable (vinc x y) :=
      {
        Decidable_witness :=
          decide (interpret_sm (al_inject A V) x <<=
                               (interpret_sm (al_inject A V) y))%views;
      }.
    Proof.
      now decide (interpret_sm (al_inject A V) x <<= interpret_sm (al_inject A V) y)%views.
    Defined.

    Global Instance VExpr_DecOrderedViewsSemigroup :
      DecOrderedViewsSemigroup (VExpr A) := {}.

  End views_expr_instances_DecOrderedViewsSemigroup.
  
  Section views_expr_instances_SubtractiveViewsSemigroup.
    Lemma vinc_Part_congr (x y z: VExpr A):
      (x <<= y)%views -> ((Part x z) <<= (Part y z))%views.
    Proof.
      intro Hinc_xy.
      apply inc_sub_congr, Hinc_xy.
    Qed.

    Lemma Part_adjoint (x y z: VExpr A):
      (x <<= (y * z))%views -> ((Part x y) <<= z)%views.
    Proof.
      intro Hinc_x_yz.
      apply sub_adjoint, Hinc_x_yz.
    Qed.
    
    Global Instance VExpr_SubtractiveViewsSemigroup
      : AdjView (VExpr A) :=
      {
        sub := Part (A := A);

        inc_sub_congr := vinc_Part_congr;
        sub_adjoint := Part_adjoint
      }.
  End views_expr_instances_SubtractiveViewsSemigroup.

  Local Open Scope views_scope.
  
  Section views_expr_instances_FullViewsSemigroup.
    Context {FV : FullView V}.
    
    Lemma Part_adjoint_back (a b c : VExpr A) :
      a \ b <<= c -> a <<= b * c.
    Proof.
      simpl.
      apply sub_adjoint_back.
    Qed.

    Lemma Join_Part_cancel_inc_back (a b : VExpr A) :
      a <<= (a * b) \ b.
    Proof.
      simpl.
      apply dot_sub_cancel_inc_back.
    Qed.

    Lemma Part_of_Join (a b c : VExpr A) :
      ((a * b) \ c) == ((a \ c) * (b \ (c \ a))).
    Proof.
      simpl.
      apply sub_of_dot.
    Qed.
    
    Global Instance VExpr_FullViewsSemigroup
      : FullView (VExpr A) :=
      {
        sub_adjoint_back := Part_adjoint_back;
        dot_sub_cancel_inc_back := Join_Part_cancel_inc_back;
        sub_of_dot := Part_of_Join;
      }.
  End views_expr_instances_FullViewsSemigroup.

  Local Open Scope program_scope.

  (** [trivial_vexpr_induction] solves a proof by induction on a single
      [VExpr] where the base cases can be solved by reflexivity and
      the inductive cases by rewriting the previous steps. *)

  Ltac trivial_vexpr_induction :=
    match goal with
    | [ v : VExpr _ |- _ ] =>
      let id1 := fresh in
      let id2 := fresh in
      let id3 := fresh in
      let id4 := fresh in      
      induction v as [ | | id1 id2 id3 id4 | id1 id2 id3 id4 ] ;
      ((* Base cases *) reflexivity
       || (* Inductive cases *) cbn; now rewrite id2, id4)
    end.
  
  Section views_expr_instances_Functor.
    (** We define monadic bind first, because we can implement [fmap]
        directly in terms of it. *)
    
    Fixpoint vbind {T U : Type} (f : T -> VExpr U) (e : VExpr T) : VExpr U :=
      match e with
      | Unit _   => Unit U
      | Atom a   => f a
      | Join x y => Join (vbind f x) (vbind f y)
      | Part x y => Part (vbind f x) (vbind f y)
      end.

    Definition vmap {T U : Type} (f : T -> U) : VExpr T -> VExpr U :=
      vbind (@Atom U ∘ f).
      
    Lemma vmap_identity (T : Type) (v : VExpr T) :
      vmap id v = v.
    Proof.
      trivial_vexpr_induction.
    Qed.

    Lemma vmap_compose (T U W : Type) (f : T -> U) (g : U -> W) (v : VExpr T) :
      vmap (g ∘ f) v = (vmap g ∘ vmap f) v.
    Proof.
      trivial_vexpr_induction.
    Qed.
    
    Global Instance VExpr_Functor : Functor VExpr :=
      {
        fmap          := @vmap;
        fmap_identity := vmap_identity;
        fmap_compose  := vmap_compose;
      }.

  End views_expr_instances_Functor.

  Section views_expr_instances_Monad.

    (** We defined [mbind] as [vbind] above.
        [mreturn] will just be [Atom]. *)

    Lemma vexpr_monad_identity_left (X Y : Type) (x : X) (f: X -> VExpr Y) :
      vbind f (Atom x) = f x.
    Proof.
      (* Trivially, by construction. *)
      reflexivity.
    Qed.

    Lemma vexpr_monad_identity_right (X : Type) (v : VExpr X) :
      vbind (@Atom X) v = v.
    Proof.
      trivial_vexpr_induction.
    Qed.

    Lemma vexpr_monad_assoc (X Y Z : Type) (v : VExpr X) (f : X -> VExpr Y) (g : Y -> VExpr Z) :
      vbind g (vbind f v) = vbind (fun x => vbind g (f x)) v.
    Proof.
      trivial_vexpr_induction.
    Qed.

    Lemma vexpr_monad_fmap (X Y : Type) (f : X -> Y) (m : VExpr X) :
      fmap f m = vbind (@Atom Y ∘ f) m.
    Proof.
      reflexivity.
    Qed.
    
    Global Instance VExpr_Monad : Monad VExpr :=
      {
        mbind                := @vbind;
        mreturn              := @Atom;
        monad_identity_left  := vexpr_monad_identity_left;
        monad_identity_right := vexpr_monad_identity_right;
        monad_assoc          := vexpr_monad_assoc;
        monad_fmap           := vexpr_monad_fmap;
      }.
  End views_expr_instances_Monad.

  (** If the mapped function preserves equivalence at the view level,
      so does [vbind]. *)

  Lemma vbind_equiv (X Y : Type) (f : X -> VExpr Y)
        {FV : FullView V}
        {AL_X : AtomLanguage X V}
        {AL_Y : AtomLanguage Y V} :
    (forall x : X, al_inject X V x == interpret_sm (al_inject Y V) (f x)) <->
    (forall e : VExpr X,
        interpret_sm (al_inject X V) e == interpret_sm (al_inject Y V) (vbind f e)).
  Proof.
    split.
    - (* -> *)
      intros Heq_atom e.
      induction e.
      + (* Unit *)
        reflexivity.
      + (* Atom *)
        apply Heq_atom.
      + (* Join *)
        cbn.
        now rewrite IHe1, IHe2.
      + (* Part *)
        cbn.
        now rewrite IHe1, IHe2.
    - (* <- *)
      intros Heq_expr x.
      apply (Heq_expr (Atom x)).
  Qed.

  (** If the mapped function preserves equivalence at the view level,
        so does [vmap]. *)
  
  Corollary vmap_equiv (X Y : Type) (f : X -> Y)
            {FV : FullView V}
            {AL_X : AtomLanguage X V}
            {AL_Y : AtomLanguage Y V} :
    (forall x : X, al_inject X V x == al_inject Y V (f x)) <->
    (forall e : VExpr X,
        interpret_sm (al_inject X V) e == interpret_sm (al_inject Y V) (vmap f e)).
  Proof.
    split.
    - intros Heq_atom e.
      apply vbind_equiv.
      + exact FV.
      + apply Heq_atom.
    - intros Heq_expr a.
      apply (Heq_expr (Atom a)).
  Qed.
End views_expr_instances.

