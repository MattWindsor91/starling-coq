(** * View expressions: Multiset atom language *)

From Coq Require Import
     Arith.PeanoNat
     Sets.Multiset
     Program.Basics
     Relations.Relation_Definitions
     Classes.RelationPairs
     Classes.SetoidClass.

From Starling Require Import
     Utils.Product
     Utils.Option.Facts
     Utils.Multiset.Minus
     Views.Classes
     Views.Instances.Multiset
     Views.Transformers.Product (* used for [==] on products *)
     Views.Expr.AtomLanguage
     Views.Expr.Deminus     
     Views.Expr.Instances
     Views.Expr.List     
     Views.Expr.Type.

Set Implicit Arguments.

Section views_expr_multiset.
  Variables
    (A : Type).

  Hypothesis HeqA : forall (x y : A), {x = y} + {x <> y}.

  Definition mset_proj (a : A) : multiset A := SingletonBag eq HeqA a.

  Global Instance multiset_AtomLanguage_Direct : AtomLanguage A (multiset A) :=
    {
      al_inject := mset_proj;
    }.
  
  Definition mset_pairwise (q d : A) : (option A * option A) :=
    if HeqA q d then (None, None) else (Some q, Some d).

  Lemma mset_pairwise_equiv :
    min_equiv _ _ mset_proj mset_pairwise.
  Proof.
    intros q [d|].
    - (* We have a [d] *)
      unfold min_lift, maybe, compose, amaybe, mset_proj, mset_pairwise.
      destruct HeqA as [->|HneqA].      
      + (* q = d *)
        split.
        * (* fst *)
          apply symmetry, sub_self.
        * (* snd *)
          intro x.
          apply symmetry, Nat.sub_diag.
      + (* q <> d *)
        split.
        * (* fst *)
          intro x.
          simpl.
          destruct (HeqA q x), (HeqA d x).
          -- exfalso.
             apply HneqA.
             rewrite e0.
             exact e.
          -- apply symmetry, Nat.sub_0_r.
          -- apply symmetry, Nat.sub_0_l.
          -- apply symmetry, Nat.sub_0_l.
        * (* snd *)
          intro x.
          simpl.
          destruct (HeqA q x), (HeqA d x).
          -- exfalso.
             apply HneqA.
             rewrite e0.
             exact e.
          -- apply symmetry, Nat.sub_0_l.
          -- apply symmetry, Nat.sub_0_r.
          -- apply symmetry, Nat.sub_0_l.
    - (* We don't have a [d] *)
      apply symmetry.
      split.
      + (* fst *) apply sub_one_r.
      + (* snd *) apply sub_one_l.
  Qed.

  Global Instance multiset_MinusAtomLanguage_Direct : MinusAtomLanguage A (multiset A) :=
    {
      al_minus := mset_pairwise;
    }.
  Proof.
    - apply min_equiv_to_fst, mset_pairwise_equiv.
    - apply min_equiv_to_snd, mset_pairwise_equiv.
  Defined.

  Definition rmpart_multiset : VExpr A -> LNormVExpr A :=
    al_rmpart (ML := multiset_MinusAtomLanguage_Direct).

  Corollary rmpart_multiset_equiv (v : VExpr A) :
    vequiv (proj1_sig (rmpart_multiset v)) v.
  Proof.
    apply al_rmpart_equiv, multiset_FullView.
  Qed.
End views_expr_multiset.