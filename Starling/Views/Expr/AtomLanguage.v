(** * View expressions: Atom languages *)

From Coq Require Import
     Arith.PeanoNat
     Sets.Multiset
     Program.Basics
     Relations.Relation_Definitions
     Classes.RelationPairs
     Classes.SetoidClass.

From Starling Require Import
     Utils.Monads
     Utils.Option.Facts
     Utils.Product
     Views.Classes
     Views.Transformers.Product
     Views.Expr.Type.

Chapter views_expr_atomlanguage.
Local Open Scope program_scope.

Variables
  A
  V : Type.

Class AtomLanguage :=
  mk_AtomLanguage
    {
      al_inject (atom     : A) : V;
    }.

Section minus.
  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}    
    {AV : AdjView V}.
  
  Definition amaybe : option A -> VExpr A :=
    maybe (Unit _) (Atom (A := A)).

  Lemma amaybe_no_parts (a : option A) : no_parts (amaybe a).
  Proof.
    destruct a; exact I.
  Qed.

  Section min_functions.
    Variables
      (r : A -> V)
      (min : A -> A -> (option A * option A)).
    
    Definition min_lift (q : A) (od : option A) : (VExpr A * option A) :=
      maybe (Atom q, None) (prodmap amaybe id ∘ min q) od.

    Definition min_equiv_fst : Prop :=
      (forall (q : A) (od : option A),
          interpret_sm r (fst (min_lift q od)) ==
          interpret_sm r (Part (Atom q) (amaybe od))).
    
    Definition min_equiv_snd : Prop :=
      forall (q : A) (od : option A),
        interpret_sm r (amaybe (snd (min_lift q od))) ==
        interpret_sm r (Part (amaybe od) (Atom q)).

    (** [min_equiv] is a more compact way of expressing [min_equiv_fst] and
      [min_equiv_snd]. *)

    Definition min_equiv : Prop :=
      forall (q : A) (od : option A),
        (prodmap (interpret_sm r) (interpret_sm r ∘ amaybe) (min_lift q od)) ==
        (interpret_sm r (Part (Atom q) (amaybe od)),
         interpret_sm r (Part (amaybe od) (Atom q))).

    (** We can pull [min_equiv_fst] out of [min_equiv]. *)

    Lemma min_equiv_to_fst : min_equiv -> min_equiv_fst.
    Proof.
      intros Heqv q od.
      destruct (Heqv q od) as (Hfst & _).
      unfold "@@" in Hfst.
      rewrite prodmap_fst in Hfst.
      apply Hfst.
    Qed.

    (** We can also pull [min_equiv_snd] out of [min_equiv]. *)

    Lemma min_equiv_to_snd : min_equiv -> min_equiv_snd.
    Proof.
      intros Heqv q od.
      destruct (Heqv q od) as (_ & Hsnd).
      unfold "@@" in Hsnd.
      rewrite prodmap_snd in Hsnd.
      apply Hsnd.
    Qed.
    
    Lemma min_lift_no_parts (q : A) (od : option A) :
      no_parts (fst (min_lift q od)).
    Proof.
      destruct od; simpl.
      - unfold compose.
        rewrite prodmap_fst.
        apply amaybe_no_parts.
      - exact I.
    Qed.
  End min_functions.
    
  Class MinusAtomLanguage {AL : AtomLanguage} :=
    mk_MinusAtomLanguage
      {
        (* Operations *)
        
        al_minus  (quot div : A) : (option A * option A);

        (* Laws *)

        al_equiv_fst : min_equiv_fst al_inject al_minus;
        al_equiv_snd : min_equiv_snd al_inject al_minus;
      }.
  End minus.
End views_expr_atomlanguage.

(** Any [Monad] forms an [AtomLanguage] with [mreturn] as injection. *)

Global Instance AtomLanguage_Monad
       {A : Type} {M : Type -> Type} {FV : Functor M} {MV : Monad M}
  : AtomLanguage A (M A) :=
  {
    al_inject := mreturn
  }.