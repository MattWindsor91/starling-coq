(** * View expressions: Part elimination *)

From Coq Require Import
     FunInd
     Program.Basics
     Classes.SetoidClass
     Arith.PeanoNat.

From Starling Require Import
     Utils.Option.Facts
     Utils.Product
     Views.Classes
     Views.Expr.AtomLanguage
     Views.Expr.Instances
     Views.Expr.Type
     Views.Expr.List.

Set Implicit Arguments.
Set Universe Polymorphism.

Section views_expr_deminus_atomwise.
  Variables
    A (* Atoms *)
    V (* Views *)
  : Type.

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {FV : FullView V}
    {AL : AtomLanguage A V}
    {ML : MinusAtomLanguage A V}.

  Local Open Scope program_scope.

  Definition rmpartA_general_step
             {D : Type}
             (min : A -> D -> (LNormVExpr A * D))
             '(v', d') a :=
    match min a d' with
    | (r, d'') => (lapp r v', d'')
    end.

  Lemma rmpartA_general_step_exec
             {D : Type}
             (min : A -> D -> (LNormVExpr A * D))
             v' d' r d'' a :
    min a d' = (r, d'') -> rmpartA_general_step min (v', d') a = (lapp r v', d'').
  Proof.
    unfold rmpartA_general_step.
    now intros ->.
  Qed.

  Lemma rmpartA_general_step_fold_emlp
             {D : Type}
             (min : A -> D -> (LNormVExpr A * D))
             v r r' d:
    eq_modulo_lnorm_proof r r' ->
    eq_modulo_lnorm_proof
      (fst
         (lnorm_fold_left (rmpartA_general_step min)
                          v
                          (r, d)))
      (fst
         (lnorm_fold_left (rmpartA_general_step min)
                          v
                          (r', d))).
  Proof.
    revert r r' d.
    pattern v.
    apply LNormVExpr_ind_strong; try easy.
    intros a u Hind r r' d Heq_r.
    rewrite ! lnorm_fold_left_cons.
    destruct (min a d) as (adr & add) eqn:Hmad.
    rewrite ! rmpartA_general_step_exec with (r0 := adr) (d'' := add); try easy.
    now apply Hind, lapp_prfirr.
  Qed.

  Definition rmpartA_general
             {D : Type}
             (min : A -> D -> (LNormVExpr A * D))
             (d : D)
             (v : LNormVExpr A) : LNormVExpr A :=
    fst
      (lnorm_fold_left (rmpartA_general_step min)
                       v
                       (lunit A, d)
      ).

  Definition norm_min_lift (min : A -> A -> (option A * option A)) (q : A) (od : option A)
    : (LNormVExpr A * option A) :=
    maybe (latom q, None) (prodmap (list_normalise_maybe_atom (A := A)) id ∘ min q) od.

  Lemma norm_min_lift_equiv_fst (min : A -> A -> (option A * option A)) (q : A) (od : option A) :
    forget_lnorm (fst (norm_min_lift min q od)) == fst (min_lift A min q od).
  Proof.
    unfold norm_min_lift, min_lift, prodmap, compose.
    destruct od as [d|]; cbn.
    - destruct (min q d) as (a & b).
      apply symmetry, list_normalise_maybe_atom_amaybe.
    - apply latom_Atom.
  Qed.

  Lemma norm_min_lift_equiv_snd (min : A -> A -> (option A * option A)) (q : A) (od : option A) :
    snd (norm_min_lift min q od) = snd (min_lift A min q od).
  Proof.
    unfold norm_min_lift, min_lift, prodmap, compose.
    destruct od as [d|]; cbn.
    - destruct (min q d) as (a & b).
      reflexivity.
    - reflexivity.
  Qed.

  Definition rmpartA (m : LNormVExpr A) (os : option A) :
    LNormVExpr A :=
    rmpartA_general (norm_min_lift (al_minus A V)) os m.

  Local Open Scope vexpr_scope.


  Lemma rmpartA_general_nil
        {D : Type}
        (f : A -> D -> (LNormVExpr A * D))
        (d : D) :
    rmpartA_general f d (lunit A) = lunit A.
  Proof.
    intuition.
  Qed.

  Corollary rmpartA_general_nil_gen
        {D : Type}
        (f : A -> D -> (LNormVExpr A * D))
        (d : D)
        (v : LNormVExpr A) :
    list_normalise_destruct_forget v = None ->
    rmpartA_general f d v = lunit A.
  Proof.
    rewrite <- rmpartA_general_nil with (f0 := f) (d0 := d).
    intro Hnone.
    elim_destruct_forget.
    unfold rmpartA_general.
    f_equal.
    now apply lnorm_fold_left_prfirr.
  Qed.

  Lemma rmpartA_general_inner_cons
        {D : Type}
        (min : A -> D -> (LNormVExpr A * D))
        (a : A)
        (v init : LNormVExpr A)
        (d : D) :
    forall r d',
      min a d = (r, d') ->
      lnorm_fold_left (rmpartA_general_step min)
                      (lcons a v)
                      (init, d) =
      lnorm_fold_left (rmpartA_general_step min)
                      v
                      (lapp r init, d').
  Proof.
    intros r d' Hmin.
    rewrite lnorm_fold_left_cons.
    simpl.
    now rewrite Hmin.
  Qed.

  Lemma rmpartA_general_inner_lapp
        {D : Type}
        (min : A -> D -> (LNormVExpr A * D))
        (v x y : LNormVExpr A)
        (d : D) :
    fst (
        lnorm_fold_left (rmpartA_general_step min)
                        v
                        (lapp x y, d)
      ) =
    lapp
      (fst (
           lnorm_fold_left (rmpartA_general_step min)
                           v
                           (x, d)
         )
      )
      y.
  Proof.
    revert x y d.
    pattern v.
    apply LNormVExpr_ind_strong; try easy.
    intros a u Hind x y d.
    transitivity
      (fst
         (lnorm_fold_left
            (rmpartA_general_step min)
            (lcons a u)
            (lapp x y, d))).
    {
        f_equal.
    }
    destruct (min a d) as (r & d') eqn:Hmin.
    rewrite (rmpartA_general_inner_cons min _ _ _ _ Hmin).
    rewrite Hind.
    symmetry.
    transitivity
      (lapp
         (fst
            (lnorm_fold_left
               (rmpartA_general_step min)
               (lcons a u)
               (x, d)))
         y
      ).
      {
        now rewrite lnorm_fold_left_prfirr with (ys := (lcons a u)).
      }
      rewrite rmpartA_general_inner_cons with (r0 := r) (d'0 := d'); try easy.
      rewrite Hind.
      apply symmetry, lapp_assoc.
  Qed.

  Lemma rmpartA_general_cons
        {D : Type}
        (min : A -> D -> (LNormVExpr A * D))
        (v : LNormVExpr A)
        (d d' : D)
        (a : A)
        (r : LNormVExpr A) :
    min a d = (r, d') ->
    eq_modulo_lnorm_proof
      (rmpartA_general min d (lcons a v))
      (lapp (rmpartA_general min d' v) r).
  Proof.
    intro Hmin.
    unfold rmpartA_general.
    rewrite lnorm_fold_left_cons.
    rewrite (rmpartA_general_step_exec min _ _ _ Hmin).
    rewrite <- rmpartA_general_inner_lapp.
    rewrite lapp_unit_r.
    replace (r, d') with (lapp r (lunit A), d') by f_equal.
    rewrite rmpartA_general_inner_lapp.
    rewrite lapp_unit_r.
    apply rmpartA_general_step_fold_emlp.
    apply symmetry, lapp_unit_l.
  Qed.

  Corollary rmpartA_general_cons_equiv
            {D : Type}
            (min : A -> D -> (LNormVExpr A * D))
            (v : LNormVExpr A)
            (d d' : D)
            (a : A)
            (r : LNormVExpr A) :
    min a d = (r, d') ->
    forget_lnorm
      (rmpartA_general min d (lcons a v))
    ==
    (∙ (forget_lnorm r)
       (forget_lnorm (rmpartA_general min d' v))).
  Proof.
    intro Hmin.
    rewrite rmpartA_general_cons with (d'0 := d') (r0 := r); try easy.
    apply lapp_equiv.
  Qed.

  Lemma rmpartA_equiv (m : LNormVExpr A) (os : option A) :
    forget_lnorm (rmpartA m os) == Part (forget_lnorm m) (amaybe _ os).
  Proof.
    unfold rmpartA.
    revert os.
    pattern m.
    apply LNormVExpr_ind_strong.
    - (* Unit *)
      intro os.
      apply symmetry, sub_one_l.
    - (* Cons cell *)
      intros a v Hind os.
      destruct (norm_min_lift (al_minus A V) a os) as (r & d') eqn:Hlift.
      etransitivity.
      {
        apply rmpartA_general_cons_equiv, Hlift.
      }
      (* Rearrange the RHS with sub-of-dot. *)
      symmetry.
      transitivity (∙ (\ (&a) (amaybe _ os))
                      (\ (forget_lnorm v) (\ (amaybe _ os) (&a)))).
      {
        apply sub_of_dot.
      }
      (* Now work out what the various parts of [Hlift] actually are. *)
      pose proof (norm_min_lift_equiv_fst (al_minus A V) a os) as Hnml_fst.
      pose proof (norm_min_lift_equiv_snd (al_minus A V) a os) as Hnml_snd.
      rewrite Hlift in Hnml_fst, Hnml_snd.
      cbn in Hnml_fst, Hnml_snd.
      subst.
      symmetry.
      (* Push through the r equivalence and inductive hypothesis. *)
      transitivity
        (∙(fst (min_lift A (al_minus A V) a os))
          (\ (forget_lnorm v) (amaybe A  (snd (min_lift A (al_minus A V) a os))))).
      {
        apply equiv_dot_proper.
        - apply Hnml_fst.
        - apply Hind.
      }
      (* Now use the atom language. *)
      apply equiv_dot_proper.
      + apply al_equiv_fst.
      + apply sub_equiv_invert, al_equiv_snd.
  Qed.
End views_expr_deminus_atomwise.

Section views_expr_deminus.
  Variables
    A (* Atoms *)
    V (* Views *)
  : Type.

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {FV : FullView V}
    {AL : AtomLanguage A V}.

  Variables
    (minus_atom : LNormVExpr A -> A -> LNormVExpr A).

  Definition minus_atom_equiv : Prop :=
    forall (e : LNormVExpr A) (a : A), (forget_lnorm (minus_atom e a)) == (Part (forget_lnorm e) (Atom a)).

  Local Open Scope vexpr_scope.

  Definition rmpartB_general (m s : LNormVExpr A) : LNormVExpr A :=
    lnorm_fold_left minus_atom s m.

  Lemma rmpartB_general_nil (m : LNormVExpr A) :
    rmpartB_general m (lunit A) = m.
  Proof.
    intuition.
  Qed.

  Lemma rmpartB_general_cons (a : A) (m s : LNormVExpr A) :
    rmpartB_general m (lcons a s) = rmpartB_general (minus_atom m a) s.
  Proof.
    apply lnorm_fold_left_cons.
  Qed.

  (** If the per-atom subtraction preserves equivalence, so does
        [rmpartB_general]. *)

  Lemma rmpartB_general_equiv (m s : LNormVExpr A) :
    minus_atom_equiv ->
    forget_lnorm (rmpartB_general m s) == (\ (forget_lnorm m) (forget_lnorm s)).
  Proof.
    intro Hminus_atom_equiv.
    (* Induction on the subtrahend, leaving the minuend open. *)
    revert m.
    pattern s.
    apply LNormVExpr_ind_strong.
    - (* Unit *)
      intro m.
      apply symmetry, sub_one_r.
    - (* Cons (cons a v = s) *)
      intros a v Hind m.
      rewrite rmpartB_general_cons, Hind.
      transitivity (\ (\ (forget_lnorm m) (&a)) (forget_lnorm v)).
      {
        apply equiv_sub_Proper; try easy.
        apply Hminus_atom_equiv.
      }
      apply symmetry, sub_by_dot.
  Qed.

  (** [rmpart] list-normalises a [VExpr], using [minus_atom] to remove parts. *)

  Fixpoint rmpart (e : VExpr A) : LNormVExpr A :=
    match e with
    | Unit _ => lunit _
    | Atom a => latom a
    | Join x y => lapp (rmpart y) (rmpart x)
    | Part x y => rmpartB_general (rmpart x) (rmpart y)
    end.

  (** [deminus]ing a [VExpr] preserves equivalence if the per-atom minus is
        sound. *)

  Lemma rmpart_equiv (e : VExpr A) :
    minus_atom_equiv ->
    forget_lnorm (rmpart e) == e.
  Proof.
    intro Hminus_atom_equiv.
    induction e.
    - (* Unit *) reflexivity.
    - (* Atom *) apply latom_Atom.
    - (* Join *)
      cbn.
      rewrite lapp_equiv.
      now apply equiv_dot_proper.
    - (* Part *)
      cbn.
      rewrite rmpartB_general_equiv.
      + now apply equiv_sub_Proper.
      + apply Hminus_atom_equiv.
  Qed.
End views_expr_deminus.

Section views_expr_deminus_ML.
  Variables
    A
    V : Type.

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {FV : FullView V}
    {AL : AtomLanguage A V}
    {ML : MinusAtomLanguage A V}.

  (** We can use a [MinusAtomLanguage] to deminus an expression. *)

  Definition al_rmpart : VExpr A -> LNormVExpr A :=
    rmpart (fun m s => (rmpartA m (Some s))).

  Corollary al_rmpart_equiv (v : VExpr A) :
    forget_lnorm (al_rmpart v) == v.
  Proof.
    apply rmpart_equiv.
    - apply FV.
    - intros q os.
      apply rmpartA_equiv, FV.
  Qed.
End views_expr_deminus_ML.