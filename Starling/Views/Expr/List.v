(** * View expressions: Converting [PartlessVExpr]s to/from lists *)

From Coq Require Import
     Relations.Relation_Definitions
     Classes.SetoidClass
     Lists.List
     Arith.PeanoNat
     Program.Basics
     Sorting.Permutation
     RelationPairs.

From Starling Require Import
     Utils.List.Facts
     Utils.Monads
     Utils.Option.Facts
     Views.Classes
     Views.Expr.AtomLanguage
     Views.Expr.Instances
     Views.Expr.Type
     Views.Transformers.Function
.

Set Implicit Arguments.

Import ListNotations.
Local Open Scope program_scope.
Local Open Scope vexpr_scope.

Set Universe Polymorphism.

Section views_expr_list.


  Variables
    (A V : Type).

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.

  Fixpoint listify_partial (v : VExpr A) : list A :=
    match v with
    | Unit _   => []
    | Atom a   => [a]
    | Join x y => listify_partial x ++ listify_partial y
    | Part _ _ => []
    end.

  Definition acons (x : A) (xs : VExpr A) : VExpr A :=
    Join (Atom x) xs.

  Lemma acons_no_parts (x : A) (xs : VExpr A) :
    no_parts xs -> no_parts (acons x xs).
  Proof.
    intro Hnp.
    destruct xs; split; exact I || exact Hnp.
  Qed.

  Lemma acons_equiv (x : A) (y y' : VExpr A) :
    y == y' -> acons x y == acons x y'.
  Proof.
    refine (equiv_dot_right y y' _).
  Qed.

  Definition acat (y : VExpr A) : list A -> VExpr A :=
    fold_right acons y.

  Lemma acat_no_parts (xs : list A) (y : VExpr A) :
    no_parts y -> no_parts (acat y xs).
  Proof.
    intro Hnp.
    induction xs.
    - exact Hnp.
    - apply acons_no_parts, IHxs.
  Qed.

  Lemma acat_app (xs ys : list A) (y : VExpr A) :
    acat y (xs ++ ys) = acat (acat y ys) xs.
  Proof.
    apply fold_right_app.
  Qed.

  Lemma acat_equiv (xs : list A) (y y' : VExpr A) :
    y == y' -> acat y xs == acat y' xs.
  Proof.
    intro Heqv.
    induction xs.
    - apply Heqv.
    - apply acons_equiv, IHxs.
  Qed.

  (** [listify] extracts all of the atoms in a [PartlessVExpr] as a list. *)

  Definition listify : PartlessVExpr A -> list A := listify_partial ∘ (@forget_partless A).

  Lemma acat_listify_equiv (x : VExpr A) (y : PartlessVExpr A) :
    acat x (listify y) == (∙ x (forget_partless y)).
  Proof.
    revert x.
    destruct y as (y & Hy).
    induction y; intros x; cbn; try easy.
    - apply dot_one_r.
    - apply dot_comm.
    - rewrite acat_app.
      specialize (IHy1 (proj1 Hy)).
      specialize (IHy2 (proj2 Hy)).
      cbn in *.
      etransitivity.
      { apply IHy1. }
      transitivity (∙ (∙ (x) (y2)) (y1)).
      { apply equiv_dot_left, IHy2. }
      red.
      unfold "@@".
      cbn.
      rewrite dot_assoc.
      apply equiv_dot_right, dot_comm.
  Qed.

  (** [linterp] interprets a list of atoms directly as a view. *)

  Definition linterp (l : list A) : V :=
    fold_right (fun (x : A) (y : V) => dot (al_inject _ _ x) y) one l.

  (** [listify] is correct with respect to [lproject]. *)

  Lemma linterp_correct' (l1 l2 : list A) :
    dot (linterp l1) (linterp l2) == linterp (l1++l2).
  Proof.
    induction l1; cbn.
    - now rewrite dot_one_l.
    - rewrite <- IHl1.
      apply dot_assoc.
  Qed.

  Theorem linterp_correct :
    interpret_m (al_inject A V) == (linterp ∘ listify).
  Proof.
    intros (v & Hv).
    unfold compose.
    induction v; try easy.
    - apply dot_one_r.
    - destruct Hv as (Hvl & Hvr).
      unfold listify, compose, listify_partial, forget_partless, proj1_sig.
      rewrite <- linterp_correct', <- IHv1, <- IHv2.
      instantiate (1 := Hvr).
      instantiate (1 := Hvl).
      easy.
  Qed.

  Theorem linterp_reflect (v1 v2 : PartlessVExpr A) :
    linterp (listify v1) == linterp (listify v2) ->
    interpret_m (al_inject A V) v1 == interpret_m (al_inject A V) v2.
  Proof.
    now rewrite (linterp_correct v1), (linterp_correct v2).
  Qed.

  Ltac m_to_vexpr m :=
    match m with
    | one => PUnit
    | dot ?m1 ?m2 =>
      let r1 := m_to_vexpr m1 in
      let r2 := m_to_vexpr m2 in
      constr:(PJoin m1 m2)
    | _ => constr:(PAtom m)
    end.

  (** [unlistify_inner] produces a normal-form [VExpr] from an atom list. *)

  Definition unlistify_inner : list A -> VExpr A := acat (Unit A).

  Lemma unlistify_inner_acat (xs : list A) (y : VExpr A) :
    acat y xs == Join (unlistify_inner xs) y.
  Proof.
    induction xs.
    - apply symmetry, dot_one_l.
    - simpl.
      transitivity (Join (Atom a) (Join (unlistify_inner xs) y)).
      {
        apply equiv_dot_right, IHxs.
      }
      apply symmetry, dot_assoc.
  Qed.

  Lemma listify_unlistify_partial_equiv (v : VExpr A) :
    no_parts v -> v == (unlistify_inner (listify_partial v)).
  Proof.
    revert v.
    induction v; intro Hno_parts; try reflexivity.
    - (* Atom *) apply dot_one_r.
    - (* Join *)
      unfold unlistify_inner, listify_partial, compose.
      rewrite acat_app.
      transitivity (acat v2 (listify_partial v1)).
      {
        rewrite unlistify_inner_acat.
        apply equiv_dot_left, IHv1, Hno_parts.
      }
      apply acat_equiv.
      rewrite unlistify_inner_acat.
      transitivity (unlistify_inner (listify_partial v2)).
      {
        apply IHv2, Hno_parts.
      }
      apply dot_one_r.
    - (* Part *)
      contradiction.
  Qed.

  (** Unlistifying, then immediately re-listifying, a list is the
      identity. *)

  Lemma unlistify_listify_partial_eq (xs : list A) :
    listify_partial (unlistify_inner xs) = xs.
  Proof.
    induction xs; cbn; congruence.
  Qed.

  (** [unlistify] produces a normal-form [PartlessVExpr] from an atom list. *)

  Definition unlistify (xs : list A) : PartlessVExpr A :=
    exist (no_parts (A := A)) (unlistify_inner xs) (acat_no_parts xs (Unit _) I).

  Inductive is_lnorm : VExpr A -> Prop :=
    | lnorm_unit : is_lnorm (Unit A)
    | lnorm_join (a : A) (y : VExpr A) : is_lnorm y -> is_lnorm (Join (Atom a) y).

  Definition LNormVExpr := { x : VExpr A | is_lnorm x }.

  Lemma is_lnorm_partless (xs : VExpr A) :
    is_lnorm xs ->
    no_parts xs.
  Proof.
    induction 1; easy.
  Qed.

  Lemma is_lnorm_acons (x : A) (xs : VExpr A) :
    is_lnorm xs ->
    is_lnorm (acons x xs).
  Proof.
    now constructor.
  Defined.

  Corollary is_lnorm_acat (xs : VExpr A) (ys : list A) :
    is_lnorm xs ->
    is_lnorm (acat xs ys).
  Proof.
    intro Hnorm.
    induction ys.
    - apply Hnorm.
    - apply is_lnorm_acons, IHys.
  Qed.

  (** If we have list normalisation on a view expression,
      the equivalence we discussed above strengthens to equality.
      This is because we eliminate the possibility of the view
      being a lone atom. *)

  Lemma listify_unlistify_partial_lnorm_eq (v : VExpr A) :
    is_lnorm v -> v = (unlistify_inner (listify_partial v)).
  Proof.
    induction 1; try reflexivity; try contradiction.
    (* Join *)
    unfold unlistify_inner.
    cbn.
    fold unlistify_inner.
    now rewrite <- IHis_lnorm.
  Qed.

  (** We can forget that an expression is list normalised. *)

  Definition forget_lnorm : LNormVExpr -> VExpr A := proj1_sig (P := is_lnorm).

  Open Scope signature_scope.

  Definition eq_modulo_lnorm_proof : relation LNormVExpr :=
    eq @@ forget_lnorm.

  (** By declaring [forget_lnorm] as a measure, we can get equivalence
      properties on [eq_modulo_lnorm_proof] for free. *)

  Global Instance forget_lnorm_Measure : Measure (forget_lnorm).

  Lemma lnorm_unit_same_proof (xs ys : LNormVExpr) :
    forget_lnorm xs = Unit A ->
    forget_lnorm ys = Unit A ->
    xs = ys.
  Proof.
    destruct xs as (xs & []), ys as (ys & []); easy.
  Qed.

  Definition forget_lnorm_partless (v : LNormVExpr) : PartlessVExpr A :=
    exist _ (forget_lnorm v) (is_lnorm_partless (proj2_sig v)).

  Lemma forget_lnorm_partless_comm (xs : LNormVExpr) :
    forget_lnorm xs = forget_partless (forget_lnorm_partless xs).
  Proof.
    reflexivity.
  Qed.

  Definition lunit : LNormVExpr :=
    exist _ (Unit A) lnorm_unit.

  Corollary lnorm_lunit (xs : LNormVExpr) :
    forget_lnorm xs = Unit A ->
    xs = lunit.
  Proof.
    intro Hxs.
    now apply lnorm_unit_same_proof.
  Qed.

  Definition lcons (v : A) (vs : LNormVExpr) : LNormVExpr :=
    exist _ (acons v (proj1_sig vs)) (is_lnorm_acons _ (proj2_sig vs)).

  Lemma lcons_forget (v : A) (vs : LNormVExpr) :
    forget_lnorm (lcons v vs) = acons v (forget_lnorm vs).
  Proof.
    reflexivity.
  Qed.

  (** Normal [Atom]s aren't list-normalised, so [latom] generates an
    equivalent expression. *)

  Definition latom (x : A) : LNormVExpr := lcons x lunit.

  Lemma latom_Atom (x : A) : forget_lnorm (latom x) == Atom x.
  Proof.
    apply symmetry, dot_one_r.
  Qed.
End views_expr_list.

Section views_expr_list_destruct.

  Variables
    (A V : Type).

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.

  (** A list-normalised view expression is either a cons cell, or unit. *)

  Definition list_normalise_destruct (x : {x : VExpr A | is_lnorm x}) :
    { '(a, y) | forget_lnorm x = (∙ (&a) (forget_lnorm y)) }
    +
    { forget_lnorm x = %1 }.
  Proof.
    destruct x as ([|a|[|a| |] z|] & Hx_l); cbn in *; try (exfalso; now inversion Hx_l).
    - (* Atom *)
      now right.
    - (* Join *)
      left.
      assert (is_lnorm z) as Hz by now inversion Hx_l.
      now refine (exist _ (a, (exist _ z Hz)) _).
  Defined.

  Definition list_normalise_destruct_forget (xs : {x : VExpr A | is_lnorm x}) :
    option (A * {x : VExpr A | is_lnorm x}) :=
    match list_normalise_destruct xs with
    | inleft (exist _ (a, xs') _) => Some (a, xs')
    | inright _ => None
    end.

  Lemma lnorm_snoc (a : A) (v : VExpr A) :
    is_lnorm (acons a v) -> is_lnorm v.
  Proof.
    now inversion 1.
  Defined.

  Definition LNormVExpr_rect (P : VExpr A -> Type) :
    P %1 ->
    (forall a v, P v -> P (acons a v)) ->
    (forall v : LNormVExpr A, P (forget_lnorm v)).
  Proof.
    intros Hunit Hcons (v & Hv).
    induction v; try (exfalso; now inversion Hv).
    - now apply Hunit.
    - destruct v1; try (exfalso; now inversion Hv).
      assert (is_lnorm v2) as Hv' by now inversion Hv.
      apply Hcons, (IHv2 Hv').
  Defined.

  Lemma LNormVExpr_ind (P : VExpr A -> Prop) :
    P %1 ->
    (forall a v, P v -> P (acons a v)) ->
    (forall v : LNormVExpr A, P (forget_lnorm v)).
  Proof.
    apply LNormVExpr_rect.
  Qed.

  (** Stronger version of [LNormVExpr_ind] that doesn't require
      throwing away the proof. *)

  Lemma LNormVExpr_ind_strong (P : LNormVExpr A -> Prop) :
    P (lunit A) ->
    (forall a v, P v -> P (lcons a v)) ->
    (forall v : LNormVExpr A, P v).
  Proof.
    intros Hunit Hcons (v & Hv).
    induction v; try (exfalso; now inversion Hv).
    - now rewrite lnorm_lunit.
    - destruct v1; try (exfalso; now inversion Hv).
      remember (∙ ((&a)) (v2)) as v.
      destruct Hv; try easy.
      inversion Heqv; subst.
      now apply (Hcons a (exist _ v2 Hv)).
  Qed.

  (** Right-fold on list-normalised view expressions. *)

  Definition lnorm_fold_right
             (B : Type) (f : A -> B -> B) (init : B) : LNormVExpr A -> B :=
    LNormVExpr_rect
      (fun _ => B) init (fun x _ xs => f x xs).

  Lemma lnorm_fold_right_prfirr (xs ys : LNormVExpr A)
        (B : Type) (f : A -> B -> B) (init : B) :
    forget_lnorm xs = forget_lnorm ys -> lnorm_fold_right f init xs = lnorm_fold_right f init ys.
  Proof.
    destruct xs as (xs & Hxs), ys as (ys & Hys).
    unfold forget_lnorm, proj1_sig.
    intros ->.
    induction ys; try easy.
    destruct ys1; try easy.
    cbn in *.
    now f_equal.
  Qed.

  Lemma lnorm_fold_right_cons
        (B : Type) (f : A -> B -> B) (init : B) (x : A) (xs : LNormVExpr A) :
    lnorm_fold_right f init (lcons x xs) = f x (lnorm_fold_right f init xs).
  Proof.
    simpl.
    f_equal.
    now apply (lnorm_fold_right_prfirr (exist _ (proj1_sig xs) _) xs f init).
  Qed.

  (** Left-fold on list-normalised view expressions.

      This uses the encoding discussed in
      https://wiki.haskell.org/Foldl_as_foldr. *)

  Definition lnorm_fold_left
             (B : Type) (f : B -> A -> B) : LNormVExpr A -> B -> B :=
    lnorm_fold_right (fun b g x => g (f x b)) (@id B).

  Lemma lnorm_fold_left_prfirr (xs ys : LNormVExpr A)
        (B : Type) (f : B -> A -> B) (init : B) :
    forget_lnorm xs = forget_lnorm ys -> lnorm_fold_left f xs init = lnorm_fold_left f ys init.
  Proof.
    intro Hlnorm.
    unfold lnorm_fold_left.
    now rewrite (lnorm_fold_right_prfirr xs ys _ _ Hlnorm).
  Qed.

  Lemma lnorm_fold_left_cons
        (B : Type) (f : B -> A -> B) (init : B) (x : A) (xs : LNormVExpr A) :
    lnorm_fold_left f (lcons x xs) init = lnorm_fold_left f xs (f init x).
  Proof.
    unfold lnorm_fold_left.
    now rewrite lnorm_fold_right_cons.
  Qed.
End views_expr_list_destruct.

Ltac elim_destruct_forget :=
  match goal with
  | [ H : list_normalise_destruct_forget ?v = ?x |- _ ] =>
    unfold list_normalise_destruct_forget in H;
    let id1 := fresh "a" in
    let id2 := fresh "v" in
    let id3 := fresh "H" in
    destruct (list_normalise_destruct v) as [((id1 & id2) & id3)|];
    try discriminate;
    match goal with
    | [ G : ?y = x |- _ ] => inversion G; subst; clear G
    | _ => idtac
    end
  end.

Section views_expr_unlistify_lnorm.
  Variables
    (A V : Type).

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.

  Definition unlistify_lnorm (xs : list A) : LNormVExpr A :=
    fold_right (lcons (A := A)) (lunit A) xs.

  (** If we're immediately throwing away the list-normalisation proof of an
      [unlistify_lnorm], we can just use [unlistify_inner] instead. *)
  Lemma unlistify_inner_unlistify_lnorm (xs : list A) :
    forget_lnorm (unlistify_lnorm xs) = unlistify_inner xs.
  Proof.
    induction xs; cbn; auto.
    now rewrite <- IHxs.
  Qed.

  Lemma unlistify_lnorm_cons (x : A) (xs : list A) :
    unlistify_lnorm (x::xs) = lcons x (unlistify_lnorm xs).
  Proof.
    reflexivity.
  Qed.

  (** If two lists give the same unlistify result up to proof irrelevance,
      then they are equal. *)

  Lemma unlistify_lnorm_inj (xs ys : list A) :
    forget_lnorm (unlistify_lnorm xs) = forget_lnorm (unlistify_lnorm ys) ->
    xs = ys.
  Proof.
    revert ys.
    induction xs; intros ys Hfnorm.
    - cbn in *.
      now destruct ys.
    - rewrite unlistify_lnorm_cons in Hfnorm.
      destruct ys; try easy.
      inversion Hfnorm; subst.
      f_equal.
      now apply IHxs.
  Qed.
End views_expr_unlistify_lnorm.

Section views_expr_listify_lnorm.
  Variables
    (A V : Type).

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.

  (** [listify_lnorm] is a form of listifying on list-normalised
      expressions that directly uses the right-fold operator. *)

  Definition listify_lnorm : LNormVExpr A -> list A :=
    lnorm_fold_right cons [].

  Lemma listify_lnorm_prfirr (xs ys : LNormVExpr A) :
    forget_lnorm xs = forget_lnorm ys -> listify_lnorm xs = listify_lnorm ys.
  Proof.
    destruct xs as (xs & Hxs), ys as (ys & Hys).
    unfold forget_lnorm, proj1_sig.
    intros ->.
    induction ys; try easy.
    destruct ys1; try easy.
    cbn in *.
    now f_equal.
  Qed.

  Lemma listify_listify_lnorm (xs : LNormVExpr A) :
    listify_lnorm xs = listify (forget_lnorm_partless xs).
  Proof.
    revert xs.
    cbn.
    apply LNormVExpr_ind_strong; try easy.
    intros a (v & Hv) Hind.
    cbn in *.
    f_equal.
    now rewrite <- Hind.
  Qed.

  (** Many of the below properties just use the above lemma, combined
      with the results we already have for the partless versions. *)

  (** We can unroll the listification of a [lcons] into a list-cons. *)

  Corollary listify_lnorm_cons (x : A) (xs : {x : VExpr A | is_lnorm x}) :
    listify_lnorm (lcons x xs) = x::(listify_lnorm xs).
  Proof.
    now rewrite ! listify_listify_lnorm.
  Qed.

  Lemma listify_unlistify_lnorm (v : LNormVExpr A) :
    forget_lnorm (unlistify_lnorm (listify_lnorm v)) = forget_lnorm v.
  Proof.
    rewrite listify_listify_lnorm, unlistify_inner_unlistify_lnorm.
    rewrite listify_unlistify_partial_lnorm_eq; try easy.
    apply (proj2_sig v).
  Qed.

  Lemma unlistify_listify_lnorm (xs : list A) :
    listify_lnorm (unlistify_lnorm xs) = xs.
  Proof.
    rewrite listify_listify_lnorm.
    cbn.
    rewrite unlistify_inner_unlistify_lnorm.
    apply unlistify_listify_partial_eq.
  Qed.

  Import ListNotations.

  Lemma listify_lnorm_eq_nil (v : {x : VExpr A | is_lnorm x}) :
    listify_lnorm v = [] <-> forget_lnorm v = Unit A.
  Proof.
    split.
    - intros Hv.
      now rewrite <- listify_unlistify_lnorm, Hv.
    - rewrite <- listify_unlistify_lnorm.
      now destruct (listify_lnorm v).
  Qed.

  Lemma listify_lnorm_eq_cons (v : {x : VExpr A | is_lnorm x}) (x : A) (xs : list A) :
    listify_lnorm v = x::xs <-> forget_lnorm v = acons x (forget_lnorm (unlistify_lnorm xs)).
  Proof.
    split.
    - intro Hln_v.
      now rewrite <- listify_unlistify_lnorm, Hln_v.
    - rewrite <- listify_unlistify_lnorm.
      destruct (listify_lnorm v); try easy.
      rewrite unlistify_lnorm_cons.
      inversion 1; subst.
      f_equal.
      now apply unlistify_lnorm_inj.
  Qed.
End views_expr_listify_lnorm.

Hint Unfold eq_modulo_lnorm_proof RelCompFun : lnorm.

Section views_expr_list_lapp.
  Variables
    (A V : Type).

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.

  (** [lapp ys xs] appends [ys] onto the end of [xs]. *)

  Definition lapp (ys xs : LNormVExpr A) : LNormVExpr A :=
    lnorm_fold_right (lcons (A := A)) ys xs.

  Lemma lapp_cons_step_r (a : A) (xs ys : LNormVExpr A) :
    lapp xs (lcons a ys) =
    lcons a (lapp xs ys).
  Proof.
    apply lnorm_fold_right_cons.
  Qed.

  Lemma lapp_prfirr_r (xs xs' ys : LNormVExpr A) :
    proj1_sig xs = proj1_sig xs' ->
    lapp ys xs = lapp ys xs'.
  Proof.
    apply lnorm_fold_right_prfirr.
  Qed.

  Lemma lapp_cons_step_r_gen (a : A) (xs xs' ys : LNormVExpr A) :
    list_normalise_destruct_forget xs = Some (a, xs') ->
    lapp ys xs =
    lcons a (lapp ys xs').
  Proof.
    intro H.
    elim_destruct_forget.
    transitivity (lapp ys (lcons a xs')).
    {
      now apply lnorm_fold_right_prfirr.
    }
    apply lapp_cons_step_r.
  Qed.

  Lemma lapp_prfirr_l' :
  forall ys'
    (Hys' : is_lnorm (proj1_sig ys'))
    (xs : LNormVExpr A),
    eq_modulo_lnorm_proof
      (lapp ys' xs)
      (lapp (exist _ (proj1_sig ys') Hys') xs).
  Proof.
    intro ys'.
    pattern ys'.
    apply LNormVExpr_ind_strong.
    - intros Hys' (xs & Hxs).
      symmetry.
      unfold eq_modulo_lnorm_proof, "@@".
      repeat f_equal.
      now apply lnorm_lunit.
    - intros a v Hind Hys' (xs & Hxs).
      induction xs; try easy.
      destruct xs1; try easy.
      rewrite lapp_cons_step_r_gen with (a := a0) (xs' := (exist _ xs2 (lnorm_snoc Hxs))); try easy.
      symmetry.
      unfold eq_modulo_lnorm_proof, "@@".
      rewrite lapp_cons_step_r_gen with (a := a0) (xs' := (exist (@is_lnorm _) xs2 (lnorm_snoc Hxs))); try easy.
      rewrite ! lcons_forget.
      f_equal.
      apply symmetry, IHxs2.
  Qed.

  Lemma lapp_prfirr_l (xs ys ys' : LNormVExpr A) :
    eq_modulo_lnorm_proof ys ys' ->
    eq_modulo_lnorm_proof (lapp ys xs) (lapp ys' xs).
  Proof.
    destruct ys as (ys & Hys), ys' as (ys' & Hys').
    unfold eq_modulo_lnorm_proof, "@@", forget_lnorm at 1 2, proj1_sig at 1 2.
    intros ->.
    apply lapp_prfirr_l'.
  Qed.

  Lemma lapp_prfirr :
    Proper (@eq_modulo_lnorm_proof A ==> @eq_modulo_lnorm_proof A ==> @eq_modulo_lnorm_proof A)
           lapp.
  Proof.
    intros ys ys' Hys_ys' xs xs' Hxs_xs'.
    unfold eq_modulo_lnorm_proof, "@@" in *.
    rewrite lapp_prfirr_l with (ys' := ys'); eauto.
    rewrite lapp_prfirr_r with (xs' := xs'); eauto.
  Qed.

  Lemma lapp_unit_l' (u xs : LNormVExpr A) :
    forget_lnorm u = Unit _ ->
    eq_modulo_lnorm_proof (lapp u xs) xs.
  Proof.
    intro Hu.
    destruct u as (u & Hu').
    autounfold with lnorm.
    unfold "@@".
    cbn in *.
    subst.
    cbn in *.
    revert xs.
    apply LNormVExpr_ind_strong; try easy.
    (* cons case *)
    intros a (v & Hv) Hind.
    rewrite lapp_cons_step_r_gen with (a := a) (xs' := (exist _ v Hv)); try easy.
    now rewrite ! lcons_forget, <- Hind.
  Qed.

  Corollary lapp_unit_l (xs : LNormVExpr A) :
    eq_modulo_lnorm_proof (lapp (lunit A) xs) xs.
  Proof.
    now apply lapp_unit_l'.
  Qed.

  Lemma lapp_unit_r' (u xs : LNormVExpr A) :
    forget_lnorm u = Unit _ ->
    lapp xs u = xs.
  Proof.
    destruct u as (u & Hu').
    unfold forget_lnorm, proj1_sig at 1.
    now intros ->.
  Qed.

  Lemma lapp_unit_r (xs : LNormVExpr A) :
    lapp xs (lunit A) = xs.
  Proof.
    intuition.
  Qed.

  (** [lapp] is associative over Leibniz equality, including proofs. *)

  Corollary lapp_assoc (xs ys zs : LNormVExpr A) :
    lapp xs (lapp ys zs) =
    lapp (lapp xs ys) zs.
  Proof.
    revert xs ys.
    pattern zs.
    apply LNormVExpr_ind_strong; try easy.
    (* cons *)
    intros a v Hind xs ys.
    transitivity (lapp xs (lapp ys (lcons a v))).
    {
      apply lapp_prfirr_r.
      f_equal.
    }
    transitivity (lapp (lapp xs ys) (lcons a v)).
    {
      now rewrite ! lapp_cons_step_r, Hind.
    }
    now apply lapp_prfirr_r.
  Qed.

  Lemma lapp_acat (xs ys : LNormVExpr A) :
    forget_lnorm (lapp ys xs) = acat (forget_lnorm ys) (listify_lnorm xs).
  Proof.
    revert xs.
    apply LNormVExpr_ind_strong; try easy.
    (* inductive case: xy is cons *)
    intros a v Hind.
    transitivity (forget_lnorm (lapp ys (lcons a v))).
    {
      now apply lapp_prfirr.
    }
    rewrite lapp_cons_step_r, lcons_forget, Hind.
    destruct ys as (ys & Hys); subst.
    now rewrite ! listify_listify_lnorm.
  Qed.

  Corollary lapp_equiv (xs ys : {x : VExpr A | is_lnorm x}) :
    forget_lnorm (lapp ys xs) == Join (forget_lnorm xs) (forget_lnorm ys).
  Proof.
    rewrite lapp_acat, listify_listify_lnorm, acat_listify_equiv.
    apply dot_comm.
  Qed.
End views_expr_list_lapp.

Section views_expr_list_length.
  Variables
    (A V : Type).

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.

  Definition lnorm_length : LNormVExpr A -> nat :=
    length (A := A) ∘ listify (A := A) ∘ forget_lnorm_partless (A := A).

  (** If [list_normalise_destruct_forget] is reporting [v] destructs into
    some ([a], [v']), [v']'s length is smaller than [v]. *)
  Lemma lnorm_length_destruct (v v' : LNormVExpr A) (a : A) :
    list_normalise_destruct_forget v = Some (a, v') ->
    lnorm_length v' < lnorm_length v.
  Proof.
    intro Helim.
    elim_destruct_forget.
    cbn.
    rewrite H.
    apply Nat.lt_succ_diag_r.
  Qed.
End views_expr_list_length.

Section views_expr_list_rest.
  (* TODO: clean this up. *)

  Variables
    (A V : Type).

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.

  (** We can list-normalise a partless view expression. *)

  Definition list_normalise (xs : PartlessVExpr A) : { x : VExpr A | is_lnorm x }.
    destruct xs as (xs & Hxs).
    induction xs.
    - (* Unit *)
      exact (lunit A).
    - (* Atom *)
      exact (lcons a (lunit A)).
    - (* Join *)
      specialize (IHxs1 (proj1 Hxs)).
      specialize (IHxs2 (proj2 Hxs)).
      exact (lapp IHxs2 IHxs1).
    - (* Part *)
      contradiction.
  Defined.

  (** List normalisation is semantics-preserving. *)

  Lemma list_normalise_equiv (xs : PartlessVExpr A) :
    forget_partless xs == forget_lnorm (list_normalise xs).
  Proof.
    destruct xs as (xs & Hxs).
    induction xs.
    - (* Unit *)
      reflexivity.
    - (* Atom *)
      apply dot_one_r.
    - (* Join *)
      unfold forget_partless, proj1_sig in *.
      specialize (IHxs1 (proj1 Hxs)).
      specialize (IHxs2 (proj2 Hxs)).
      simpl. (* cbn here is slow! *)
      rewrite lapp_equiv.
      apply equiv_dot_proper; assumption.
    - (* Part *)
      contradiction.
  Qed.

  (** We can list-normalise an optional atom. *)

  Definition list_normalise_maybe_atom : option A -> {x : VExpr A | is_lnorm x} :=
    maybe (lunit A) (latom (A := A)).

  Lemma list_normalise_maybe_atom_amaybe (oa : option A) :
    (amaybe A oa) == forget_lnorm (list_normalise_maybe_atom oa).
  Proof.
    destruct oa.
    - apply symmetry, latom_Atom.
    - reflexivity.
  Qed.

End views_expr_list_rest.

Section views_expr_list_facts.

  Variable A : Type.

  Lemma unlistify_lnorm_app (xs ys : list A) :
    forget_lnorm (unlistify_lnorm (xs++ys)) =
    forget_lnorm (lapp (unlistify_lnorm ys) (unlistify_lnorm xs)).
  Proof.
    revert ys.
    induction xs; intro ys.
    - now rewrite app_nil_l.
    - now rewrite <- app_comm_cons, ! unlistify_lnorm_cons, lapp_cons_step_r,
      !lcons_forget, <- IHxs.
  Qed.

  Lemma forall2_lnorm_lists_inner (xs ys : list A) (R : A -> A -> Prop) :
    Forall2 R xs ys <->
    ForallAtoms2 R (unlistify_inner xs) (unlistify_inner ys).
  Proof.
    split.
    - induction 1; repeat constructor; intuition.
    - intro Hfar.
      assert (length xs = length ys).
      {
        generalize dependent ys.
        induction xs; destruct ys; cbn in *; inversion 1; auto.
      }
      revert Hfar.
      apply (list_pairwise_ind xs ys); intuition.
      inversion Hfar; subst.
      inversion H5; subst.
      auto.
  Qed.

  Corollary forall2_lnorm_lists (xs ys : list A) (R : A -> A -> Prop) :
    Forall2 R xs ys <->
    ForallAtoms2 R
                 (forget_lnorm (unlistify_lnorm xs))
                 (forget_lnorm (unlistify_lnorm ys)).
  Proof.
    rewrite ! unlistify_inner_unlistify_lnorm.
    apply forall2_lnorm_lists_inner.
  Qed.

  Lemma forall2_lnorm (xs ys : {x : VExpr A | is_lnorm x}) (R : A -> A -> Prop) :
    Forall2 R (listify_lnorm xs) (listify_lnorm ys) <->
    ForallAtoms2 R
                 (forget_lnorm xs)
                 (forget_lnorm ys).
  Proof.
    replace (forget_lnorm xs) with (forget_lnorm (unlistify_lnorm (listify_lnorm xs)))
      by apply listify_unlistify_lnorm.
    replace (forget_lnorm ys) with (forget_lnorm (unlistify_lnorm (listify_lnorm ys)))
      by apply listify_unlistify_lnorm.
    apply forall2_lnorm_lists.
  Qed.

  Lemma forall2_lapp (xs1 ys1 xs2 ys2 : {x : VExpr A | is_lnorm x}) (R : A -> A -> Prop) :
    ForallAtoms2 R (forget_lnorm xs1) (forget_lnorm xs2) ->
    ForallAtoms2 R (forget_lnorm ys1) (forget_lnorm ys2) ->
    ForallAtoms2 R (forget_lnorm (lapp ys1 xs1))
                 (forget_lnorm (lapp ys2 xs2)).
  Proof.
    intros H1 H2.
    apply forall2_lnorm in H1.
    apply forall2_lnorm in H2.
    pose proof (Forall2_app H1 H2) as Hcomb%forall2_lnorm_lists.
    now rewrite ! unlistify_lnorm_app,
    ! (lapp_prfirr (listify_unlistify_lnorm _) (listify_unlistify_lnorm _)) in Hcomb.
  Qed.

End views_expr_list_facts.

Section views_expr_list_mapping.

  (** [LNormVExpr]s are almost [Functor]s, using the same operations as
      normal [VExpr]s.  This is because [fmap] doesn't change the
      list-normalised structure.

      We can't write an _actual_ instance for [Functor], because the functor
      laws don't quite hold: the proofs of list normalisation aren't the
      same! *)

  Lemma list_normalised_fmap_preserve (A B : Type) (f : A -> B) (v : VExpr A) :
    is_lnorm v -> is_lnorm (fmap f v).
  Proof.
    induction 1; now constructor.
  Qed.

  Definition lvmap (A B : Type) (f : A -> B)
             (v : {x : VExpr A | is_lnorm x}) : {x : VExpr B | is_lnorm x} :=
    exist _
          (fmap f (forget_lnorm v))
          (list_normalised_fmap_preserve f (proj2_sig v)).

  Lemma lvmap_fmap (A B : Type) (f : A -> B) (v : {x : VExpr A | is_lnorm x}) :
    forget_lnorm (lvmap f v) = fmap f (forget_lnorm v).
  Proof.
    trivial.
  Qed.

  Lemma lvmap_map_inner
        (A B : Type) (f : A -> B) (v : LNormVExpr A) :
    forget_lnorm (lvmap f v) = unlistify_inner (map f (listify_lnorm v)).
  Proof.
    pattern v.
    apply LNormVExpr_ind_strong; try easy.
    intros a v' Hmap.
    rewrite ! lvmap_fmap in *.
    rewrite lcons_forget, listify_lnorm_cons.
    simpl.
    now rewrite <- Hmap.
  Qed.

  (** Modulo the normalisation proof, [lvmap] is equivalent to listifying,
      mapping, and unlistifying. *)

  Corollary lvmap_map
        (A B : Type) (f : A -> B) (v : {x : VExpr A | is_lnorm x}) :
    eq_modulo_lnorm_proof (lvmap f v) (unlistify_lnorm (map f (listify_lnorm v))).
  Proof.
    autounfold with lnorm.
    unfold "@@".
    now rewrite unlistify_inner_unlistify_lnorm, <- lvmap_map_inner.
  Qed.

  (** A similar result for if we're already listifying the view. *)

  Corollary lvmap_map_listify
        (A B : Type) (f : A -> B) (v : {x : VExpr A | is_lnorm x}) :
    listify_lnorm (lvmap f v) = map f (listify_lnorm v).
  Proof.
    rewrite <- unlistify_listify_lnorm.
    apply unlistify_lnorm_inj.
    rewrite ! listify_unlistify_lnorm.
    apply lvmap_map.
  Qed.
End views_expr_list_mapping.

Section views_expr_list_equiv.
  Variables A V : Type.

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {AL : AtomLanguage A V}.

  (** If [xs] is a permutation of [ys], their respective
  [unlistify_lnorm] views are equivalent. *)

  Global Instance unlistify_Permutation_Proper :
    Proper (@Permutation A ==> (equiv @@ @forget_lnorm _)) (@unlistify_lnorm A).
  Proof.
    intros xs ys.
    induction 1; intuition; unfold "@@" in *.
    - rewrite !unlistify_lnorm_cons, !lcons_forget.
      apply equiv_dot_right, IHPermutation.
    - rewrite !unlistify_lnorm_cons, !lcons_forget.
      unfold acons.
      rewrite <-! Join_assoc.
      apply equiv_dot_left, dot_comm.
    - now rewrite IHPermutation1.
  Defined.

  (** As above, but over [listify_lnorm] instead. *)

  Corollary listify_Permutation (v1 v2 : { x : VExpr A | is_lnorm x }) :
    Permutation (listify_lnorm v1) (listify_lnorm v2) ->
    forget_lnorm v1 == forget_lnorm v2.
  Proof.
    setoid_rewrite <- listify_unlistify_lnorm.
    apply unlistify_Permutation_Proper.
  Qed.
End views_expr_list_equiv.
