(** * View expressions: Type definition

    [VExpr]s represent an abstract syntax tree in which views are
    constructed from [Unit], [Atom], [Join], and [Part]. *)

From Coq Require Import
     Classes.SetoidClass.

From Starling Require Import
     Views.Classes.

Set Implicit Arguments.

Section views_expr.
  (** [VExpr] is the grammar of view expressions. *)

  Inductive VExpr (A : Type) :=
  | Unit
  | Atom (a   : A)
  | Join (x y : VExpr A)
  | Part (x y : VExpr A).

  Section views_expr_interpret.
    Variables
      (A V: Type).

    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}.

    (** [no_units] holds when there are no units in the [VExpr]. *)

    Fixpoint no_units (v : VExpr A) : Prop :=
      match v with
      | Unit _ => False
      | Atom _ => True
      | Join x y | Part x y => no_units x /\ no_units y
      end.

    (** A [UnitlessVExpr] is a [VExpr] without units. *)

    Definition UnitlessVExpr : Type := { v | no_units v }.

    (** [no_parts] holds when there are no parts in the [VExpr]. *)

    Fixpoint no_parts (v : VExpr A) : Prop :=
      match v with
      | Unit _ | Atom  _ => True
      | Part _ _ => False
      | Join x y => no_parts x /\ no_parts y
      end.

    (** A [PartlessVExpr] is a [VExpr] without parts. *)

    Definition PartlessVExpr : Type := { v | no_parts v }.

    (** [forget_partless] turns a [PartlessVExpr] into an [Expr]. *)

    Definition forget_partless : PartlessVExpr -> VExpr A := @proj1_sig (VExpr A) no_parts.

    (** [Unit] is a [PartlessVExpr]. *)

    Definition PUnit : PartlessVExpr := exist _ (Unit A) I.

    (** [Atom] is a [PartlessVExpr]. *)

    Definition PAtom (a : A) : PartlessVExpr := exist _ (Atom a) I.

    Definition PJoin (x y : PartlessVExpr) : PartlessVExpr :=
      exist _ (Join (proj1_sig x) (proj1_sig y)) (conj (proj2_sig x) (proj2_sig y)).

    (** A [SimpleVExpr] is a [VExpr] with neither parts nor units. *)

    Definition SimpleVExpr : Type := { v | no_parts v & no_units v }.

    Variable r : A -> V.

    (** [interpret_sm] interprets a [VExpr] as a subtractive monoid. *)

    Fixpoint interpret_sm
             {MV : ViewsMonoid V}
             {OV : OrderedViewsSemigroup V}
             {AV : AdjView V}
             (v: VExpr A) : V :=
      match v with
      | Unit _     => 1%views
      | Atom a   => r a
      | Join x y => (interpret_sm x * interpret_sm y)%views
      | Part x y => (interpret_sm x \ interpret_sm y)%views
      end.

    Lemma interpret_sm_Join (a b : VExpr A)
          {MV : ViewsMonoid V}
          {OV : OrderedViewsSemigroup V}
          {AV : AdjView V} :
      interpret_sm (Join a b) ==
      (interpret_sm a * interpret_sm b)%views.
    Proof.
      reflexivity.
    Qed.

    Lemma interpret_sm_Part (a b : VExpr A)
          {MV : ViewsMonoid V}
          {OV : OrderedViewsSemigroup V}
          {AV : AdjView V} :
      interpret_sm (Part a b) ==
      (interpret_sm a \ interpret_sm b)%views.
    Proof.
      reflexivity.
    Qed.

    (** [interpret_s] interprets a unitless [VExpr] as a subtractive semigroup. *)

    Definition interpret_s
               {OV : OrderedViewsSemigroup V}
               {AV : AdjView V}
               (v : UnitlessVExpr) : V.
    Proof.
      destruct v as (v & Hv).
      induction v.
      - (* No units, so [v] can't be [Unit]. *)
        contradiction.
      - exact (r a).
      - destruct Hv as (Hv1 & Hv2).
        exact (IHv1 Hv1 * IHv2 Hv2)%views.
      - destruct Hv as (Hv1 & Hv2).
        exact (IHv1 Hv1 \ IHv2 Hv2)%views.
    Defined.

    (** [interpret_m] interprets a partless [VExpr] as a monoid. *)

    Definition interpret_m
               {MV : ViewsMonoid V}
               (v: PartlessVExpr) : V.
    Proof.
      destruct v as (v & Hv).
      induction v.
      - exact 1%views.
      - exact (r a).
      - destruct Hv as (Hv1 & Hv2).
        exact (IHv1 Hv1 * IHv2 Hv2)%views.
      - (* No parts, so [v] can't be a Part. *)
        contradiction.
    Defined.

    Lemma interpret_m_Join {MV : ViewsMonoid V} (x y : VExpr A) (Hx : no_parts x) (Hy : no_parts y) :
      interpret_m (exist _ (Join x y) (conj Hx Hy)) =
      (interpret_m (exist _ x Hx) * interpret_m (exist _ y Hy))%views.
    Proof.
      reflexivity.
    Qed.

    Lemma interpret_m_PJoin {MV : ViewsMonoid V} (x y : PartlessVExpr) :
      interpret_m (PJoin x y) = (interpret_m x * interpret_m y)%views.
    Proof.
      destruct x as (x & Hx), y as (y & Hy).
      reflexivity.
    Qed.

    (** [interpret] interprets a simple [VExpr] as a semigroup. *)

    Definition interpret
               (v: SimpleVExpr) : V.
    Proof.
      destruct v as [v Hvp Hvu].
      induction v; try contradiction.
      - (* Atom *)
        exact (r a).
      - (* Join *)
        destruct Hvp as (Hvp1 & Hvp2), Hvu as (Hvu1 & Hvu2).
        exact (IHv1 Hvp1 Hvu1 * IHv2 Hvp2 Hvu2)%views.
    Defined.
  End views_expr_interpret.

  Section views_expr_forall.

    Variables
      (A V: Type).

    Context
      {SV : Setoid V}
      {VV : ViewsSemigroup V}
      {MV : ViewsMonoid V}
      {OV : OrderedViewsSemigroup V}
      {AV : AdjView V}
    .

    (** [AtomIn] is an inductive proof of the membership of a given atom in
        a view expression. *)

    Inductive AtomIn : A -> VExpr A -> Prop :=
    | AtomIn_atom (a : A) :
        AtomIn a (Atom a)
    | AtomIn_join_l (a : A) (x y : VExpr A) :
        AtomIn a x -> AtomIn a (Join x y)
    | AtomIn_join_r (a : A) (x y : VExpr A) :
        AtomIn a y -> AtomIn a (Join x y)
    | AtomIn_part_l (a : A) (x y : VExpr A) :
        AtomIn a x -> AtomIn a (Part x y)
    | AtomIn_part_r (a : A) (x y : VExpr A) :
        AtomIn a y -> AtomIn a (Part x y).

    (** [ForallAtoms] is an inductive proof of a proposition over all atoms of
        a view expression. *)

    Inductive ForallAtoms : (A -> Prop) -> VExpr A -> Prop :=
    | ForallAtoms_unit (P : A -> Prop) :
        ForallAtoms P (Unit A)
    | ForallAtoms_atom (P : A -> Prop) (a : A) :
        P a -> ForallAtoms P (Atom a)
    | ForallAtoms_join (P : A -> Prop) (x y : VExpr A) :
        ForallAtoms P x -> ForallAtoms P y -> ForallAtoms P (Join x y)
    | ForallAtoms_part (P : A -> Prop) (x y : VExpr A) :
        ForallAtoms P x -> ForallAtoms P y -> ForallAtoms P (Part x y).

    Lemma ForallAtoms_forall (P : A -> Prop) (x : VExpr A) :
      ForallAtoms P x <->
      forall (a : A), AtomIn a x -> P a.
    Proof.
      split.
      - induction 1; intro b; inversion 1; intuition.
      - intro Hin.
        induction x; constructor; try easy.
        + apply Hin.
          constructor.
        + apply IHx1.
          intros a Hin'.
          apply Hin, AtomIn_join_l, Hin'.
        + apply IHx2.
          intros a Hin'.
          apply Hin, AtomIn_join_r, Hin'.
        + apply IHx1.
          intros a Hin'.
          apply Hin, AtomIn_part_l, Hin'.
        + apply IHx2.
          intros a Hin'.
          apply Hin, AtomIn_part_r, Hin'.
    Qed.

    (** [ForallAtoms2] is an inductive proof that two view expressions have the
        same structure, and a given relation holds pairwise over their atoms. *)

    Inductive ForallAtoms2 : (A -> A -> Prop) -> VExpr A -> VExpr A -> Prop :=
    | ForallAtoms2_unit (R : A -> A -> Prop) :
        ForallAtoms2 R (Unit A) (Unit A)
    | ForallAtoms2_atom (R : A -> A -> Prop) (a : A) (b : A) :
        R a b ->
        ForallAtoms2 R (Atom a) (Atom b)
    | ForallAtoms2_join (R : A -> A -> Prop) (x1 y1 x2 y2 : VExpr A) :
        ForallAtoms2 R x1 x2 ->
        ForallAtoms2 R y1 y2 ->
        ForallAtoms2 R (Join x1 y1) (Join x2 y2)
    | ForallAtoms2_part (R : A -> A -> Prop) (x1 y1 x2 y2 : VExpr A) :
        ForallAtoms2 R x1 x2 ->
        ForallAtoms2 R y1 y2 ->
        ForallAtoms2 R (Part x1 y1) (Part x2 y2).

    Global Instance ForallAtoms2_Reflexive {R : A -> A -> Prop}
           {R_R : Reflexive R} :
      Reflexive (ForallAtoms2 R).
    Proof.
      intro x.
      induction x; constructor; intuition.
    Defined.

    Global Instance ForallAtoms2_Symmetric {R : A -> A -> Prop}
           {S_R : Symmetric R} :
      Symmetric (ForallAtoms2 R).
    Proof.
      intros x y.
      induction 1; constructor; intuition.
    Defined.

    Global Instance ForallAtoms2_Transitive {R : A -> A -> Prop}
           {T_R : Transitive R} :
      Transitive (ForallAtoms2 R).
    Proof.
      intros x y z Hz.
      generalize dependent z.
      induction Hz; inversion 1; subst; constructor; firstorder.
    Defined.

    Global Instance ForallAtoms2_Equivalence {R : A -> A -> Prop}
           {E_R : Equivalence R} :
      Equivalence (ForallAtoms2 R) := {}.
  End views_expr_forall.
End views_expr.

(** S-expression-style notations for vexprs *)

Notation "%1"            := (Unit _) : vexpr_scope.
Notation "(& x )"        := (Atom x) (x at next level) : vexpr_scope.
Notation "(\ x y )"      := (Part x y) (x, y at next level) : vexpr_scope.
Notation "(∙ )"          := (Unit _) (only parsing) : vexpr_scope.
Notation "(∙ x )"        := x (x at next level, only parsing) : vexpr_scope.
Notation "(∙ x .. y z )" := (Join x .. (Join y z) ..) (x, y, z at next level) : vexpr_scope.
