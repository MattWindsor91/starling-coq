(** * Views expressions: Guarded views *)

From Coq Require Import
     Classes.SetoidClass
     Lists.List
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     Utils.Monads
     Option.Facts
     Backend.Classes
     Views.Classes
     Views.Expr.Type
     Views.Expr.AtomLanguage
     Views.Expr.Deminus
     Views.Expr.Instances
     Views.Expr.List
     Views.Transformers.Function.

Set Implicit Arguments.
Set Universe Polymorphism.

Section views_expr_guarded.
  Polymorphic Universe v.
  Variable V : Type@{v}.  (* Views *)

  Polymorphic Universe ep.
  Variable Ep : Type@{ep}.  (* Expressions *)


  Variables
    LSt (* Local states *)
    : Type.

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}
    {MV : ViewsMonoid V}
    {OV : OrderedViewsSemigroup V}
    {AV : AdjView V}
    {FV : FullView V}
    {S_LSt : Setoid LSt}
    {P_Ep  : PredEx Ep unit unit LSt}.

  Hypothesis HEp_dec :
    forall (e : Ep) (l : LSt),
      {pe_interp tt tt e l} + {~pe_interp tt tt e l}.

  (** A [Guarded] atom is an atom with a guard expression. *)

  Record Guarded A :=
    mk_Guarded
      { g_guard : list Ep;
        g_item  : A;
      }.

  Infix "|>" := mk_Guarded (at level 99) : guard_scope.

  Local Open Scope guard_scope.

  Definition gmap A B (f : A -> B) '(g |> a) : Guarded B := g |> f a.

  Global Instance Guarded_Functor : Functor Guarded :=
    {|
      fmap := gmap;
    |}.
  Proof with reflexivity.
    - intros A (guar & a)...
    - intros A B C f g (guar & a)...
  Defined.

  Local Open Scope backend_scope.

  Definition gbind A B (f : A -> Guarded B) '(g |> a) : Guarded B :=
    let '(g' |> b) := f a in (g ++ g' |> b).

  Definition greturn A (a : A) : Guarded A := (nil |> a).

  Global Instance Guarded_Monad : Monad Guarded :=
    {|
      mbind   := gbind;
      mreturn := greturn
    |}.
  Proof with reflexivity.
    - (* Left identity *)
      intros A B a f.
      simpl.
      destruct (f a) as (guar & b)...
    - (* Right identity *)
      intros A (guar & b).
      simpl.
      rewrite List.app_nil_r...
    - (* Associativity *)
      intros A B C (guar & a) f g.
      simpl.
      destruct (f a) as (guar' & b).
      simpl.
      destruct (g b) as (guar'' & c).
      rewrite List.app_assoc...
    - (* Fmap *)
      intros A B f (guar & a).
      simpl.
      rewrite List.app_nil_r...
  Defined.

  Definition guard_inject {A} {AL : AtomLanguage A (LSt -> V)} '(g |> a) (l : LSt) : V :=
    if HEp_dec (iconj g) l then al_inject A _ a l else 1%views.

  Global Instance guard_AtomLanguage {A} {AL : AtomLanguage A (LSt -> V)}
    : AtomLanguage (Guarded A) (LSt -> V) :=
    {|
      al_inject := guard_inject
    |}.

  (* TODO: tidy this up. *)

  Section gmin_functions.
    Variable
      A (* Primitive atoms *) : Type.

    Context
      {AL : AtomLanguage A (LSt -> V)}.

    Variable gmin :
      A (* quotient *) ->
      A (* divisor *) ->
      list Ep. (* the guard for when the minus succeeds *)

(*

    Definition gmin_lift (q : A) (d : Guarded A) : (VExpr A * Guarded A) :=
      (prodmap amaybe id ∘ gmin q) od.

    Definition gmin_equiv_fst : Prop :=
      (forall (q : A) (od : option A),
          interpret_sm r (fst (gmin_lift q od)) ==
          interpret_sm r (Part (Atom q) (amaybe od))).

    Definition gmin_equiv_snd : Prop :=
      forall (q : A) (od : option A),
        interpret_sm r (amaybe (snd (gmin_lift q od))) ==
        interpret_sm r (Part (amaybe od) (Atom q)).

    (** [gmin_equiv] is a more compact way of expressing [gmin_equiv_fst] and
      [gmin_equiv_snd]. *)

    Definition gmin_equiv : Prop :=
      forall (q : A) (od : option A),
        (prodmap (interpret_sm r) (interpret_sm r ∘ amaybe) (gmin_lift q od)) ==
        (interpret_sm r (Part (Atom q) (amaybe od)),
         interpret_sm r (Part (amaybe od) (Atom q))).

    (** We can pull [gmin_equiv_fst] out of [gmin_equiv]. *)

    Lemma gmin_equiv_to_fst : gmin_equiv -> gmin_equiv_fst.
    Proof.
      intros Heqv q od.
      destruct (Heqv q od) as (Hfst & _).
      unfold "@@" in Hfst.
      rewrite prodmap_fst in Hfst.
      apply Hfst.
    Qed.

    (** We can also pull [gmin_equiv_snd] out of [gmin_equiv]. *)

    Lemma gmin_equiv_to_snd : gmin_equiv -> gmin_equiv_snd.
    Proof.
      intros Heqv q od.
      destruct (Heqv q od) as (_ & Hsnd).
      unfold "@@" in Hsnd.
      rewrite prodmap_snd in Hsnd.
      apply Hsnd.
    Qed.

    Lemma gmin_lift_no_parts (q : A) (od : option A) :
      no_parts (fst (gmin_lift q od)).
    Proof.
      destruct od; simpl.
      - unfold compose.
        rewrite prodmap_fst.
        apply amaybe_no_parts.
      - exact I.
    Qed.
  End gmin_functions.

  Class MinusAtomLanguage {AL : AtomLanguage} :=
    mk_MinusAtomLanguage
      {
        (* Operations *)

        al_minus  (quot div : A) : (option A * option A);

        (* Laws *)

        al_equiv_fst : min_equiv_fst al_inject al_minus;
        al_equiv_snd : min_equiv_snd al_inject al_minus;
      }.
  End minus. *)
  End gmin_functions.

  Section minus.
    Polymorphic Universe a.
    Variable
      A (* Primitive atoms *) : Type@{a}.

    Context
      {AL : AtomLanguage A (LSt -> V)}
      {I_Ep : ImplPredEx Ep unit unit LSt}.

    Local Open Scope backend_scope.
    Local Open Scope program_scope.
    Import ListNotations.

    (** Decidability gives us De Morgan's law. *)

    Lemma pe_de_morgan (ep1 ep2 : Ep) :
      Same_set LSt
               (pe_interp tt tt (¬ (ep1 & ep2)))
               (Union LSt
                      (pe_interp tt tt (¬ep1))
                      (pe_interp tt tt (¬ep2))).
    Proof.
      split.
      - (* -> *)
        intros ls Hin%pe_not_neg.
        destruct (HEp_dec ep1 ls) as [Hep1|Hnep1],
                 (HEp_dec ep2 ls) as [Hep2|Hnep2].
        + (* Both active: contradiction. *)
          contradict Hin.
          intros Hin_cont.
          apply Hin_cont, pe_conj_distr, Intersection_intro; assumption.
        + (* Right inactive. *)
          right.
          apply pe_not_neg, Hnep2.
        + (* Left inactive. *)
          left.
          apply pe_not_neg, Hnep1.
        + (* Both inactive. *)
          left.
          apply pe_not_neg, Hnep1.
      - (* <- *)
        intros ls [ls' Hnep|ls' Hnep];
          apply pe_not_neg;
          apply pe_not_neg in Hnep;
          now intros Hnconj%pe_conj_distr%Intersection_inv.
    Qed.

    (** [ga_minus_success] tells us that if the guard generated by
        [ga_minus] holds, the result of subtraction is unit. *)

    Definition ga_minus_success (ga_minus : A -> A -> list Ep) : Prop :=
      (forall (q d : A) (ls : LSt),
          pe_interp tt tt (iconj (ga_minus q d)) ls ->
          (al_inject _ _ q ls \ al_inject _ _ d ls) == 1)%views.

    (** [ga_minus_failure] tells us that if the guard generated by
        [ga_minus] doesn't hold, subtraction has no effect. *)

    Definition ga_minus_failure (ga_minus : A -> A -> list Ep) : Prop :=
      (forall (q d : A) (ls : LSt),
          pe_interp tt tt (¬ iconj (ga_minus q d)) ls ->
          (al_inject _ _ q ls \ al_inject _ _ d ls) == al_inject _ _ q ls)%views.

    (** [ga_minus_swap] tells us that if we can minus two atoms one
        way, we can minus them the other way too. *)

    Definition ga_minus_swap (ga_minus : A -> A -> list Ep) : Prop :=
      (forall (q d : A) (ls : LSt),
          pe_interp tt tt (iconj (ga_minus q d)) ls ->
          pe_interp tt tt (iconj (ga_minus d q)) ls).

    Section minus_dec.
      Hypothesis Heq_dec : forall x y : A, {x = y} + {x <> y}.
      Hypothesis Heq_proj : forall (x y : A) (ls : LSt),
          x <> y -> (al_inject _ _ x ls \ al_inject _ _ y ls)%views == al_inject _ _ x ls.

      Definition ga_minus_dec  (q d : A) : list Ep :=
        [if Heq_dec q d then TT else FF].

      Lemma ga_minus_dec_success : ga_minus_success ga_minus_dec.
      Proof.
        intros x y ls.
        cbn.
        destruct Heq_dec as [->|Hneq]; intro H.
        - (* equal *)
          apply sub_self.
        - (* not equal: contradiction *)
          apply pe_conj_distr, Intersection_inv in H.
          now destruct H as (Hfalse%pe_false_False & _).
      Qed.

      Lemma ga_minus_dec_failure : ga_minus_failure ga_minus_dec.
      Proof.
        intros x y ls.
        cbn.
        destruct Heq_dec as [->|Hneq]; intro H.
        - (* equal; contradiction *)
          exfalso.
          apply pe_not_neg in H.
          apply H.
          apply pe_conj_distr, Intersection_intro; apply pe_true_True.
        - (* not equal *)
          now rewrite (Heq_proj ls Hneq).
      Qed.
    End minus_dec.

    Variable ga_minus :
      A (* quotient *) ->
      A (* divisor *) ->
      list Ep. (* the guard for when the minus succeeds *)

    Section fail_guard.
      Variables
         min_guar sub_guar match_guar : list Ep.

      Definition fail_guard : list Ep :=
        ¬ iconj (sub_guar ++ match_guar) :: min_guar.

      Variables
        (ls : LSt).

      Lemma fail_guard_min_off :
        ~ pe_interp tt tt (iconj min_guar) ls ->
        ~ pe_interp tt tt (iconj fail_guard) ls.
      Proof.
        intros Hmin_off (_ & Hmin_on)%iconj_cons_interp%pe_conj_distr%Intersection_inv.
        apply Hmin_off, Hmin_on.
      Qed.

      Lemma fail_guard_minus_success :
        pe_interp tt tt (iconj sub_guar) ls ->
        pe_interp tt tt (iconj match_guar) ls ->
        ~ pe_interp tt tt (iconj fail_guard) ls.
      Proof.
        intros Hsub_on Hmatch_on (Hrest_fail%pe_not_neg & _)%pe_conj_distr%Intersection_inv.
        now apply Hrest_fail, iconj_app_interp, pe_conj_distr, Intersection_intro.
      Qed.

      Lemma fail_guard_sub_fail :
        pe_interp tt tt (iconj min_guar) ls ->
        ~ pe_interp tt tt (iconj sub_guar) ls ->
        pe_interp tt tt (iconj fail_guard) ls.
      Proof.
        intros Hmin Hnot_sub.
        apply pe_conj_distr, Intersection_intro.
        - apply pe_not_neg.
          intros (Hin_sub & _)%iconj_app_interp%pe_conj_distr%Intersection_inv.
          apply Hnot_sub, Hin_sub.
        - apply Hmin.
      Qed.

      Lemma fail_guard_match_fail :
        pe_interp tt tt (iconj min_guar) ls ->
        ~ pe_interp tt tt (iconj match_guar) ls ->
        pe_interp tt tt (iconj fail_guard) ls.
      Proof.
        intros Hmin Hnot_match.
        apply pe_conj_distr, Intersection_intro.
        - apply pe_not_neg.
          intros (_ & Hin_match)%iconj_app_interp%pe_conj_distr%Intersection_inv.
          apply Hnot_match, Hin_match.
        - apply Hmin.
      Qed.

      Lemma fail_guard_on_dichotomy :
        pe_interp tt tt (iconj fail_guard) ls ->
        pe_interp tt tt (iconj min_guar) ls /\
        (pe_interp tt tt (¬ iconj sub_guar) ls \/ pe_interp tt tt (¬ iconj match_guar) ls).
      Proof.
        intros (Hsub_match_on & Hmin_on)%pe_conj_distr%Intersection_inv.
        split.
        - (* Show that [min_guar] is on. *)
          apply Hmin_on.
        - (* Show that either [match_guar] or [sub_guar] is off. *)
          apply Union_inv, pe_de_morgan, pe_not_neg.
          apply pe_not_neg in Hsub_match_on.
          now intros Hin_match_sub%iconj_app_interp.
      Qed.
    End fail_guard.

    Arguments fail_guard min_guar sub_guar match_guar : simpl never.

    Lemma fail_guard_match_equiv (min_guar sub_guar match_guar1 match_guar2 : list Ep) :
      Same_set LSt
               (pe_interp tt tt (iconj match_guar1))
               (pe_interp tt tt (iconj match_guar2)) ->
      Same_set LSt
               (pe_interp tt tt (iconj (fail_guard min_guar sub_guar match_guar1)))
               (pe_interp tt tt (iconj (fail_guard min_guar sub_guar match_guar2))).
    Proof.
      intros (Hinc_12 & Hinc_21).
      split; intros ls; specialize (Hinc_12 ls); specialize (Hinc_21 ls).
      - (* -> *)
        intro Hin_1.
        apply fail_guard_on_dichotomy in Hin_1.
        destruct Hin_1 as (Hmin_on & [Hsub_off|Hmatch1_off]).
        + apply fail_guard_sub_fail.
          * apply Hmin_on.
          * eapply pe_not_neg, Hsub_off.
        + apply fail_guard_match_fail.
          * apply Hmin_on.
          * intros Hin_2.
            apply pe_not_neg in Hmatch1_off.
            intuition.
      - (* <- *)
        intro Hin_2.
        apply fail_guard_on_dichotomy in Hin_2.
        destruct Hin_2 as (Hmin_on & [Hsub_off|Hmatch2_off]).
        + apply fail_guard_sub_fail.
          * apply Hmin_on.
          * eapply pe_not_neg, Hsub_off.
        + apply fail_guard_match_fail.
          * apply Hmin_on.
          * intros Hin_1.
            apply pe_not_neg in Hmatch2_off.
            intuition.
    Qed.

    Definition ga_minus_lift '(q_guar |> q_atom) (d : Guarded A) : LNormVExpr (Guarded A) * Guarded A :=
      let '(d_guar |> d_atom) := d in
      let succ_guar := ga_minus q_atom d_atom in
      ( (* q appears if its guard holds, and d's doesn't OR we failed. *)
        latom (fail_guard q_guar d_guar succ_guar |> q_atom),
        (* d appears if its guard holds, and q's doesn't OR we failed. *)
        (fail_guard d_guar q_guar succ_guar |> d_atom)
      ).

    Lemma ga_minus_equiv (q_guar d_guar : list Ep) (q_atom d_atom : A) (ls : LSt):
      ga_minus_success ga_minus ->
      ga_minus_failure ga_minus ->
      ga_minus_swap    ga_minus ->
      guard_inject (fail_guard q_guar d_guar (ga_minus q_atom d_atom) |> q_atom) ls
      ==
      (guard_inject (q_guar |> q_atom) ls \ guard_inject (d_guar |> d_atom) ls)%views.
    Proof.
      intros Hsucc Hfail Hswap.
      cbn.
      unfold DProd.sub_DProd, DProd.lift_op.
      destruct (HEp_dec (iconj q_guar) ls) as [Hq_on|Hq_off].
      - (* q on *)
        destruct (HEp_dec (iconj d_guar) ls) as [Hd_on|Hd_off].
        + (* d on *)
          destruct (HEp_dec (iconj (ga_minus q_atom d_atom)) ls) as [Hm_on|Hm_off],
                   (HEp_dec (iconj (fail_guard q_guar d_guar (ga_minus q_atom d_atom))) ls) as [Hfail_on|Hfail_off].
          * (* match on, fail guard on: contradiction *)
            now pose (fail_guard_minus_success q_guar d_guar (ga_minus q_atom d_atom) ls Hd_on Hm_on).
          * (* match on, fail guard off: valid *)
            symmetry.
            apply (Hsucc q_atom d_atom ls Hm_on).
          * (* match off, fail guard on: valid *)
            symmetry.
            eapply pe_not_neg in Hm_off.
            apply (Hfail q_atom d_atom ls Hm_off).
          * (* match off, fail guard off: contradiction *)
            now pose (fail_guard_match_fail q_guar d_guar (ga_minus q_atom d_atom) ls Hq_on Hm_off).
        + (* d off *)
          destruct (HEp_dec (iconj (fail_guard q_guar d_guar (ga_minus q_atom d_atom))) ls) as [Hfail_on|Hfail_off].
          * (* fail guard on : valid *)
            now apply symmetry, sub_one_r.
          * (* fail guard off: contradiction *)
            now pose (fail_guard_sub_fail q_guar d_guar (ga_minus q_atom d_atom) ls Hq_on Hd_off).
      - (* q off *)
        rewrite sub_one_l.
        destruct (HEp_dec (iconj (fail_guard q_guar d_guar (ga_minus q_atom d_atom))) ls) as [Hfail_on|Hfail_off].
        + (* fail guard on: contradiction *)
          now pose (fail_guard_min_off q_guar d_guar (ga_minus q_atom d_atom) ls Hq_off).
        + (* fail guard off: valid *)
          reflexivity.
    Qed.

    Lemma ga_minus_lift_remainder (q d : Guarded A) :
      ga_minus_success ga_minus ->
      ga_minus_failure ga_minus ->
      ga_minus_swap    ga_minus ->
      vequiv (Atom (snd (ga_minus_lift q d))) (Part (Atom d) (Atom q)).
    Proof.
      intros Hsucc Hfail Hswap.
      unfold ga_minus_lift.
      destruct q as (q_guar & q_atom), d as (d_guar & d_atom).
      unfold snd.
      transitivity (Atom (fail_guard d_guar q_guar (ga_minus d_atom q_atom) |> d_atom)).
      - intros ls.
        cbn.
        destruct
          (HEp_dec (iconj (fail_guard d_guar q_guar (ga_minus q_atom d_atom))))
          as [Hqd_on|Hqd_off],
             (HEp_dec (iconj (fail_guard d_guar q_guar (ga_minus d_atom q_atom))))
            as [Hdq_on|Hdq_off];
          try reflexivity.
        + contradict Hqd_on.
          intros Hqd_on.
          apply Hdq_off.
          eapply fail_guard_match_equiv.
          * split; intros ls'; apply Hswap.
          * assumption.
        + contradict Hdq_on.
          intros Hdq_on.
          apply Hqd_off.
          eapply fail_guard_match_equiv.
          * split; intros ls'; apply Hswap.
          * assumption.
      - intro ls.
        apply ga_minus_equiv; assumption.
    Qed.

    Lemma ga_minus_lift_equiv (q d : Guarded A) :
      ga_minus_success ga_minus ->
      ga_minus_failure ga_minus ->
      ga_minus_swap    ga_minus ->
      vequiv (forget_lnorm (fst (ga_minus_lift q d))) (Part (Atom q) (Atom d)).
    Proof.
      intros Hsucc Hfail Hswap.
      unfold ga_minus_lift.
      destruct q as (q_guar & q_atom), d as (d_guar & d_atom).
      unfold fst.
      rewrite latom_Atom.
      intros ls.
      apply ga_minus_equiv; assumption.
    Qed.

    Definition rmpartA_guard : Guarded A -> LNormVExpr (Guarded A) -> LNormVExpr (Guarded A) :=
      rmpartA_general ga_minus_lift.

    Local Open Scope vexpr_scope.

    (** The result from [rmpartA_guard] is correct. *)

    Lemma rmpartA_guard_equiv (m : LNormVExpr (Guarded A)) (s : Guarded A) :
      ga_minus_success ga_minus ->
      ga_minus_failure ga_minus ->
      ga_minus_swap    ga_minus ->
      vequiv (forget_lnorm (rmpartA_guard s m)) (Part (forget_lnorm m) (Atom s)).
    Proof.
      intros Hsucc Hfail Hswap.
      revert s.
      pattern m.
      apply LNormVExpr_ind_strong.
      - (* Unit *)
        intro s.
        apply symmetry, sub_one_l.
      - (* Cons *)
        intros a v Hind s.
        destruct (ga_minus_lift a s) as (r & d') eqn:Hlift.
        etransitivity.
        {
          apply rmpartA_general_cons_equiv, Hlift.
        }
        (* Rearrange the RHS with sub-of-dot. *)
        symmetry.
        transitivity (∙ (\ (&a) (&s))
                        (\ (forget_lnorm v) (\ (&s) (&a)))).
        {
          apply sub_of_dot.
        }
        unfold ga_minus_lift in Hlift.
        destruct a as (q_guar & q_atom), s as (d_guar & d_atom).
        inversion_clear Hlift.
        (* Push through inductive hypothesis.*)
        symmetry.
        unfold rmpartA_guard in Hind.
        transitivity
          (∙(forget_lnorm
               (latom (fail_guard q_guar d_guar (ga_minus q_atom d_atom) |> q_atom)))
            (\ (forget_lnorm v)
               (& (fail_guard d_guar q_guar (ga_minus q_atom d_atom) |> d_atom)))).
        {
          apply equiv_dot_right, Hind.
        }
        apply equiv_dot_proper.
        + apply (ga_minus_lift_equiv (q_guar |> q_atom) (d_guar |> d_atom) Hsucc Hfail Hswap).
        + apply sub_equiv_invert,
          (ga_minus_lift_remainder (q_guar |> q_atom) (d_guar |> d_atom) Hsucc Hfail Hswap).
    Qed.

    Definition rmpartB_guard
      : LNormVExpr (Guarded A) -> LNormVExpr (Guarded A) -> LNormVExpr (Guarded A) :=
      rmpartB_general (flip rmpartA_guard).

    Lemma rmpartB_guard_equiv (m s : LNormVExpr (Guarded A)) :
      ga_minus_success ga_minus ->
      ga_minus_failure ga_minus ->
      ga_minus_swap    ga_minus ->
      vequiv (forget_lnorm (rmpartB_guard m s)) (Part (forget_lnorm m) (forget_lnorm s)).
    Proof.
      intros Hsucc Hfail Hswap.
      apply rmpartB_general_equiv.
      - apply fun_FullView.
      - intros m' s'.
        now apply rmpartA_guard_equiv.
    Qed.

    Definition rmpart_guard
      : VExpr (Guarded A) -> LNormVExpr (Guarded A) :=
      rmpart (flip rmpartA_guard).

    Lemma rmpart_guard_equiv (e : VExpr (Guarded A)) :
      ga_minus_success ga_minus ->
      ga_minus_failure ga_minus ->
      ga_minus_swap    ga_minus ->
      vequiv (forget_lnorm (rmpart_guard e)) e.
    Proof.
      intros Hsucc Hfail Hswap.
      apply rmpart_equiv.
      - apply fun_FullView.
      - intros m' s'.
        now apply rmpartA_guard_equiv.
    Qed.
  End minus.
End views_expr_guarded.