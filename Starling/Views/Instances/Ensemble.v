(** * View class instances for ensembles *)

From Coq.Classes Require Import SetoidClass.
From Coq.Sets Require Import Constructive_sets Ensembles.

From Starling Require Import
     Views.Classes
     Utils.Ensemble.Facts.

Section instances_set.
  Context {A: Type}.

  Global Instance Ensemble_Setoid: Setoid (Ensemble A) :=
    {
      equiv := Same_set A;

      setoid_equiv := Same_set_Equivalence;
    }.
  
  Global Instance Ensemble_ViewsSemigroup : ViewsSemigroup (Ensemble A) :=
    {
      dot := Union A;

      dot_assoc := Ensemble_dot_assoc;
      dot_comm := Ensemble_dot_comm
    }.
  Proof.
    intros a b c [Hab_inc Hba_inc].
    split.
    - intros x Hac_in.
      destruct (Union_inv A a c x Hac_in).
      + left.
        apply Hab_inc, H.
      + right.
        apply H.
    - intros x Hbc_in.
      destruct (Union_inv A b c x Hbc_in).
      + left.
        apply Hba_inc, H.
      + right.
        apply H.
  Defined.
  
  Global Instance Ensemble_ViewsMonoid : ViewsMonoid (Ensemble A) :=
    {
      one       := Empty_set A;

      dot_one_l := Ensemble_dot_one_l
    }.

  Global Instance Ensemble_OrderedViewsSemigroup : OrderedViewsSemigroup (Ensemble A) :=
    {
      inc := Included A;

      inc_dot_left := Ensemble_inc_dot_left;
      inc_dot_mono_left := Union_introl A
    }.

  Global Instance Ensemble_AdjView : AdjView (Ensemble A) :=
    {
      sub             := Setminus A;

      inc_sub_congr   := Ensemble_inc_sub_congr;
      sub_adjoint     := Ensemble_sub_adjoint;
    }.
End instances_set.
