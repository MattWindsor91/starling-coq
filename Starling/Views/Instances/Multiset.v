(** * View typeclass instances for multisets
    
    Multisets are setoids, views semigroups, views monoids, and adjoint views,
    by appeal to various multiset properties. *)

From Coq Require Import
     Classes.SetoidClass
     Sets.Multiset.

From Starling Require Import
     Views.Classes
     Views.Transformers.Function
     Views.Instances.Nat
     Utils.Multiset.Facts
     Utils.Multiset.Minus
     Utils.Multiset.Subset.     

Section instances_multiset.
  Context {A : Type}.

  (** Below, we define instances for [multiset] based on the existing multiset
      abstractions ([meq], [munion]) and our own extensions
      ([subset], [mminus]).

      Another way to show that [multiset] is a certain class of views algebra
      is to treat it as a function [A -> nat], and use the function lifting of
      the views instances for [nat].  Though we don't do that here, we show that
      the operations in both types of instance are equivalent, which lets us
      get away with describing multiset instances as such in prose. *)
  
  Global Instance multiset_Setoid: Setoid (multiset A) :=
    {
      equiv := @meq A;

      setoid_equiv := meq_Equivalence
    }.

  (** The above definition of [equiv] is equivalent to the functional lifting
      of [equiv] on natural numbers. *)
  
  Lemma equiv_functional (a b : A -> nat) :
    a == b <-> Bag a == Bag b.
  Proof.
    now split.
  Qed.  
  
  Global Instance multiset_ViewsSemigroup: ViewsSemigroup (multiset A) :=
    {
      dot := @munion A;

      dot_assoc := @munion_ass A;
      dot_comm := @munion_comm A;
      equiv_dot_left := @meq_left A;
    }.

  (** The above definition of [dot] is equivalent to the functional lifting
      of [dot] on natural numbers. *)
  
  Lemma dot_functional (a b : A -> nat):
    (meq (Bag (a * b)) (Bag a * Bag b))%views.
  Proof.
    now intro x.
  Qed.
  
  Global Instance multiset_ViewsMonoid: ViewsMonoid (multiset A) :=
    {
      one := @EmptyBag A;

      dot_one_l := @munion_empty_left A
    }.

  (** The above definition of [one] is equivalent to the functional lifting
      of [one] on natural numbers. *)
  
  Lemma one_functional :
    (meq one (Bag one)).
  Proof.
    now intro x.
  Qed.
  
  Global Instance multiset_OrderedViewsSemigroup:
    OrderedViewsSemigroup (multiset A) :=
    {
      inc := @subset A;      

      inc_dot_left := subset_munion_left;
      inc_dot_mono_left := subset_munion_mono_left
    }.

  (** The above definition of [inc] is equivalent to the functional lifting
      of [inc] on natural numbers. *)
  
  Lemma inc_functional (a b : A -> nat) :
    (a <<= b <-> Bag a <<= Bag b)%views.
  Proof.
    now split.
  Qed.
    
  Global Instance multiset_AdjView : AdjView (multiset A) :=
    {
      sub := @mminus A;

      inc_sub_congr := @subset_mminus_congr A;
      sub_adjoint p q r := proj1 (subset_adjoint p q r);
    }.

  Global Instance multiset_FullView : FullView (multiset A) :=
    {
      sub_adjoint_back p q r := proj2 (subset_adjoint p q r);
      dot_sub_cancel_inc_back p q := equiv_inc _ _ (mminus_munion p q);
      sub_of_dot := @mminus_of_munion A;
    }.
End instances_multiset.
