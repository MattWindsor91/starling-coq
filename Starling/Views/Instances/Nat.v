(** * View typeclass instances for list-natural numbers. *)

From Coq Require Import
     Arith.PeanoNat
     Classes.SetoidClass.


From Starling Require Import
     Nat.Facts
     Views.Classes
     Utils.Multiset.LMultiset.

Set Implicit Arguments.

Section instances_nat.
  Global Instance nat_Setoid: Setoid nat := {}.
  
  Global Instance nat_ViewsSemigroup: ViewsSemigroup nat :=
    {
      dot := Nat.add;
    }.
  Proof.
    - (* associativity *) symmetry. apply Nat.add_assoc.
    - (* commutativity *) apply Nat.add_comm.
    - (* monotone *) apply Nat.add_cancel_r.
  Defined.
  
  Global Instance nat_ViewsMonoid: ViewsMonoid nat :=
    {
      one := 0;
      dot_one_l := Nat.add_0_l;
    }.

  Global Instance nat_OrderedViewsSemigroup:
    OrderedViewsSemigroup nat :=
    {
      inc := Nat.le;
    }.
  Proof.
    - (* inc_dot_left *)
      apply Nat.add_le_mono_r.
    - (* inc_dot_mono_left *)
      apply Nat.le_add_r.
  Defined.

  Global Instance nat_DecOrderedViewsSemigroup:
    DecOrderedViewsSemigroup nat :=
    {}.
    
  Global Instance nat_AdjView : AdjView nat :=
    {
      sub := Nat.sub;

      inc_sub_congr := Nat.sub_le_mono_r;
    }.
  Proof.
    - (* sub_adjoint *)
      apply Nat.le_sub_le_add_l.
  Defined.

  Global Instance nat_FullView : FullView nat := {}.
  Proof.
    - (* sub_adjoint_back *)
      apply Nat.le_sub_le_add_l.
    - (* dot_sub_cancel_inc_back *)
      intros a b.
      apply equiv_inc, symmetry, Nat.add_sub.
    - (* sub_of_dot *)
      apply minus_of_plus.
  Defined.
End instances_nat.
