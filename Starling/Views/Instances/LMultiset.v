(** * View typeclass instances for list-based multisets.
    
    List-based multisets are setoids, views semigroups, views monoids,
    and subtractive views, by appeal to various multiset
    properties. *)

From Coq Require Import
     Classes.SetoidClass
     Classes.SetoidDec
     Sets.Multiset.

From Starling Require Import
     Views.Classes
     Utils.Multiset.LMultiset.

(* TODO *)

Set Implicit Arguments.

Section instances_lmultiset.
  Variable (A : Type).

  Context
    {SA : Setoid A}
    {DA : EqDec SA}.

  Global Instance LMultiset_Setoid: Setoid (LMultiset A) :=
    {
      equiv := lmeq (SA := SA);
      setoid_equiv := lmeq_Equivalence (SA := SA)
    }.
  
  Global Instance LMultiset_ViewsSemigroup: ViewsSemigroup (LMultiset A) :=
    {
      dot := @lmunion A;

      dot_assoc := lmunion_ass (SA := SA);
      dot_comm := lmunion_comm (SA := SA);
      equiv_dot_left := lmeq_lmunion_left (SA := SA);
    }.
  
  Global Instance LMultiset_ViewsMonoid: ViewsMonoid (LMultiset A) :=
    {
      one := EmptyLBag A;

      dot_one_l := lmunion_empty_left (SA := SA);
    }.

  Global Instance LMultiset_OrderedViewsSemigroup:
    OrderedViewsSemigroup (LMultiset A) :=
    {
      inc := lsubset (SA := SA);

      inc_dot_left := lsubset_lmunion_left (SA := SA);
      inc_dot_mono_left := lsubset_lmunion_mono_left (SA := SA);
    }.

  Global Instance LMultiset_DecOrderedViewsSemigroup:
    DecOrderedViewsSemigroup (LMultiset A) :=
    {}.
    
  Global Instance LMultiset_AdjView : AdjView (LMultiset A) :=
    {
      sub := lmminus (SA := SA);

      inc_sub_congr := lsubset_lmminus_congr (SA := SA);
      sub_adjoint p q r := proj1 (lsubset_adjoint (SA := SA) p q r);
    }.

  Global Instance LMultiset_FullView : FullView (LMultiset A) :=
    {
      sub_adjoint_back p q r := proj2 (lsubset_adjoint (SA := SA) p q r);
      dot_sub_cancel_inc_back p q := equiv_inc _ _ (lmminus_lmunion (SA := SA) p q);
      sub_of_dot := lmminus_of_lmunion (SA := SA);
    }.      
End instances_lmultiset.
