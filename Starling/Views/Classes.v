(** * Typeclasses for views *)

From Coq Require Import
     Relations.Relation_Definitions
     Classes.DecidableClass
     Classes.RelationClasses
     Classes.SetoidClass
     Classes.SetoidDec
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     Utils.Ensemble.Facts.

(* Based on
   http://www.labri.fr/perso/casteran/CoqArt/TypeClassesTut/typeclassestut.pdf
 *)

(** ** Notation

    Throughout this file, we build a notation scope [views] containing
    notation for the operators used in these typeclasses.  The
    notation we introduce is:

    - [x * y]: semigroup operator (join);
    - [x \ y]: adjoint operator (part);
    - [x <<= y]: view ordering.

    We also use [x == y], from the [Setoid] class, to refer to view
    equivalence. *)

Delimit Scope views_scope with views.
Local Open Scope views_scope.

(** ** Views semigroups

    [ViewsSemigroup] is the class of views semigroups with carrier
    [V].  Views semigroups are commutative semigroups where we define
    associativity, commutativity, and reflexivity in terms of a
    [Setoid] instance for [V].

    [ViewsSemigroup] contains one method: [dot], which is the views
    semigroup's associative, commutative operator.  It contains three
    laws each [ViewsSemigroup] must uphold:

    - [dot_assoc]: [dot] is associative;
    - [dot_comm]: [dot] is commutative;
    - [equiv_dot_left]: [dot] is compatible with equivalence. *)

Class ViewsSemigroup (V: Type) {S: Setoid V} :=
  mkViewsSemigroup
    {
      dot: V -> V -> V;

      dot_assoc x y z: dot (dot x y) z == dot x (dot y z);
      dot_comm x y: dot x y == dot y x;
      equiv_dot_left a b c: a == b -> dot a c == dot b c
    }.

(** In most places, we use [x * y] to refer to [dot x y]. *)

Infix "*" := dot : views_scope.

Section ViewsSemigroup_facts.
  Context {V : Type}.
  Context `{ViewsSemigroup V}.

  (** The flip side of [equiv_dot_left]. *)

  Corollary equiv_dot_right (a b c : V) :
    a == b -> c * a == c * b.
  Proof.
    (* By commutativity of [*] and [equiv_dot_left]. *)
    intros Hab.
    setoid_rewrite dot_comm.
    apply equiv_dot_left, Hab.
  Qed.

  (** [equiv] is proper (compatible) with respect to [dot].
      This means that, if [a == b] and [c == d], [a * b == c * d]. *)

  Global Instance equiv_dot_proper : Proper (equiv ==> equiv ==> equiv) dot.
  Proof.
    intros a b Hab c d Hcd.
    (* By applying the dot rule to replace [a] with [b], then again in
       the other direction to replace [c] with [d]. *)
    transitivity (b * c).
    - apply equiv_dot_left, Hab.
    - apply equiv_dot_right, Hcd.
  Qed.
End ViewsSemigroup_facts.

(** ** General views

    In the Concurrent Views Framework, a view is a commutative monoid.

    All of the types of view used in the Starling mechanisation are
    commutative monoids under some equivalence relation: we capture that fact
    with the type class [GenView]. *)

Class ViewsMonoid (V: Type) {S: Setoid V} {VS: ViewsSemigroup V} :=
  mkViewsMonoid
    {
      one: V;
      dot_one_l x: one * x == x
    }.

Notation "1" := one : views_scope.

Section ViewsMonoid_facts.
  Context {V : Type}.
  Context `{ViewsMonoid V}.

  (** [dot_one_r] is the flipped version of [dot_one_l]. *)

  Corollary dot_one_r (a : V) :
    a == a * 1.
  Proof.
    (* Reduce to [dot_one_l]. *)
    symmetry.
    rewrite dot_comm.
    apply dot_one_l.
  Qed.
End ViewsMonoid_facts.

(** [ZeroViewsSemigroup] is a views semigroup with a zero, usually
    representing inconsistency. *)

Class ZeroViewsSemigroup (V: Type) {S: Setoid V} {VS: ViewsSemigroup V} :=
  mkZeroViewsSemigroup
    {
      zero: V;
      dot_zero_left x: zero * x == zero
    }.

Notation "0" := zero : views_scope.

Section ZeroViewsSemigroup_facts.
  Context {V : Type}.
  Context `{ZeroViewsSemigroup V}.

  (** [dot_zero_right] is the flipped version of [dot_zero_left]. *)

  Corollary dot_zero_right (a : V) :
    0 == a * 0.
  Proof.
    (* Reduce to [dot_zero_left]. *)
    symmetry.
    rewrite dot_comm.
    apply dot_zero_left.
  Qed.
End ZeroViewsSemigroup_facts.

(** [OrderedViewsSemigroup] is a views semigroup with a pre-ordering,
    and equivalence induced by its ordering relation. *)

Class OrderedViewsSemigroup (V: Type)
      {S: Setoid V}
      {VS: ViewsSemigroup V} :=
  mkOrderedViewsSemigroup
    {
      (** [inc] is the preorder on [V]. *)
      inc: V -> V -> Prop;
      (** [inc_preorder] tells us that [inc] is a preorder. *)
      inc_preorder:> PreOrder inc;
      (** [inc_order] tells us that [inc] is a partial order up to equivalence. *)
      inc_order:> PartialOrder equiv inc;
      (** The ordering is compatible with the semigroup.
          (As the semigroup is commutative, we don't need to show the
          other direction.) *)
      inc_dot_left a b c: inc a b -> inc (a * c) (b * c);
      (** The preordering is monotone over [*].
          (Again, we don't need to show the other direction.) *)
      inc_dot_mono_left a b: inc a (a * b)
    }.
Infix "<<= " := inc (at level 85, no associativity): views_scope.
Infix "=>> " := (flip inc) (at level 85, no associativity): views_scope.

Section OrderedViewsSemigroup_facts.
  Context
    {V: Type}
    {VS: Setoid V}
    {VV: ViewsSemigroup V}
    {VO: OrderedViewsSemigroup V}.

  (** [inc_antisymm] tells us that [==] is implied by mutual ordering. *)

  Lemma inc_antisymm (a b: V):
    a <<= b -> a =>> b -> a == b.
  Proof.
    intros Hinc_ab Hinc_ba.
    apply inc_order.
    split; assumption.
  Qed.

  (** [equiv_inc] tells us that ordering is implied by [==]. *)

  Lemma equiv_inc (a b: V):
    a == b -> a <<= b.
  Proof.
    intro Heqv.
    apply inc_order, symmetry, Heqv.
  Qed.

  (** If [a == b] and [c == d], then [a <<= b] implies that [b <<= d]. *)

  Global Instance inc_equiv_impl_Proper : Proper (equiv ==> equiv ==> impl) inc.
  Proof.
    intros a b Hab_e c d Hcd_e Hac_o.
    transitivity a.
    - apply equiv_inc, symmetry, Hab_e.
    - transitivity c.
      + apply Hac_o.
      + apply equiv_inc, Hcd_e.
  Qed.

  (** Through commutativity, we can strengthen the above to iff. *)

  Corollary inc_equiv_iff_Proper : Proper (equiv ==> equiv ==> iff) inc.
  Proof.
    intros a b Hab_e c d Hcd_e.
    split; setoid_rewrite Hab_e; setoid_rewrite Hcd_e; intro Ho; assumption.
  Qed.

  (** The flip side of [inc_dot_left]. *)
  Corollary inc_dot_right (a b c : V) :
    a <<= b -> c * a <<= c * b.
  Proof.
    (* By commutativity of [*] and [inc_dot_left]. *)
    intros Hab.
    transitivity (a * c).
    - apply equiv_inc, dot_comm.
    - transitivity (b * c).
      + apply inc_dot_left, Hab.
      + apply equiv_inc, dot_comm.
  Qed.

  (** The flip side of [inc_dot_mono_left]. *)

  Corollary inc_dot_mono_right (a b: V):
    a <<= b * a.
  Proof.
    (* By commutativity of [*] and [inc_dot_mono_left]. *)
    transitivity (a * b).
    - apply inc_dot_mono_left.
    - apply equiv_inc, dot_comm.
  Qed.

  (** [inc] is proper (compatible) with respect to [dot].
      This means that, if [a == b] and [c == d], [a * b == c * d]. *)

  Global Instance inc_dot_proper : Proper (inc ==> inc ==> inc) dot.
  Proof.
    intros a b Hab c d Hcd.
    (* By applying the dot rule to replace [a] with [b], then again in
       the other direction to replace [c] with [d]. *)
    transitivity (b * c).
    - apply inc_dot_left, Hab.
    - apply inc_dot_right, Hcd.
  Qed.

  (** If an [OrderedViewsSemigroup] is also a monoid, then
      [1] is the minimum element of the monoid. *)
  Lemma one_inc {VM: ViewsMonoid V} (a: V):
    1 <<= a.
  Proof.
    setoid_rewrite <- dot_one_l at 2.
    apply inc_dot_mono_left.
  Qed.

  (** If a [PreOrderedViewsSemigroup] is also a monoid, then
      anything that is ordered below [1] must be [1]. *)
  Corollary inc_one {VM: ViewsMonoid V} (a: V):
    a <<= 1 -> a == 1.
  Proof.
    intro Ha1_inc.
    apply inc_antisymm.
    - exact Ha1_inc.
    - apply one_inc.
  Qed.

  (** If an [OrderedViewsSemigroup] has a zero, then it is the maximum element
      of the semigroup. *)

  Lemma zero_inc {ZS: ZeroViewsSemigroup V} (a : V) :
    a <<= 0.
  Proof.
    setoid_rewrite <- dot_zero_left.
    apply inc_dot_mono_right.
  Qed.

  (** If an [OrderedViewsSemigroup] has a zero, then anything ordered above
      [0] must be [0]. *)

  Corollary inc_zero {ZS: ZeroViewsSemigroup V} (a : V) :
    0 <<= a -> a == 0.
  Proof.
    intro Ha0_inc.
    apply inc_antisymm; intuition.
    apply zero_inc.
  Qed.

  (** We can lift a congruence on inclusion to one on equivalence. *)

  Lemma inc_equiv_lift (f : V -> V):
    (forall a b, a <<= b -> f a <<= f b) ->
    forall a b, a == b -> f a == f b.
  Proof.
    intros Hinc a b Heqv.
    apply inc_antisymm; apply Hinc.
    - apply equiv_inc, Heqv.
    - apply equiv_inc, symmetry, Heqv.
  Qed.

  (** We can also lift a congruence where inclusion is flipped. *)

  Lemma inc_equiv_lift_contra (f : V -> V):
    (forall a b, a <<= b -> f b <<= f a) ->
    forall a b, a == b -> f a == f b.
  Proof.
    intros Hinc a b Heqv.
    apply inc_antisymm; apply Hinc.
    - apply equiv_inc, symmetry, Heqv.
    - apply equiv_inc, Heqv.
  Qed.

  (** If we can add [c] to [a] to get [b], [a] can't be larger than [b]. *)

  Lemma inc_consume (a b c : V) : a * c == b -> a <<= b.
  Proof.
    intro Hacb.
    rewrite <- Hacb.
    apply inc_dot_mono_left.
  Qed.
End OrderedViewsSemigroup_facts.

(** We extend the idea of a [ViewsMonoid] to one that has a monus [sub]
    operator and a subset ordering. *)

Class AdjView (V: Type)
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {PS: OrderedViewsSemigroup V} :=
  mkAdjView
    {
      sub                   : V -> V -> V;
      (* This is [<<=], rather than [==], to admit [AdjView]s with no
         excluded middle on membership, for example [Ensemble]s. *)
      inc_sub_congr   a b c : a <<= b -> (sub a c) <<= (sub b c);
      sub_adjoint     a b c : a <<= (b * c) -> (sub a b) <<= c
    }.

(** Magic wand notations for subtract *)

Infix "*-" := sub (at level 80, no associativity): views_scope.
Infix "-*" := (flip sub) (at level 80, no associativity): views_scope.

(** Traditional notation for subtract *)

Infix "\" := sub (at level 80, no associativity): views_scope.

(** [AdjView] excludes some properties, such as the opposite of
    [sub_adjoint], that are not required for the Starling adjoint proof
    rule but are needed for other results such as modularity.

    [FullView] includes these missing properties.  These properties
    exclude some types, such as sets, from being views. *)

Class FullView (V : Type)
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V} :=
  mkFullView
    {
      sub_adjoint_back (a b c: V): a \ b <<= c -> a <<= b * c;

      (* This complements [dot_sub_cancel_inc], which is a
         derived property of [AdjView]s. *)

      dot_sub_cancel_inc_back (a b : V): a <<= (a * b) \ b;

      (** We can model subtracting from a dot as subtracting from one side,
          then subtracting the remainder from the other. *)

      sub_of_dot (a b c : V) : ((a * b) \ c) == (a \ c) * (b \ (c \ a));
    }.

(** The normal separating views semigroup laws don't work properly when we
    have a zero. *)

Class ZeroSeparatingViewsSemigroup (V : Type)
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}
      {ZV: ZeroViewsSemigroup V}
      {AV: AdjView V} :=
  mk_ZeroSeparatingViewsSemigroup
    {
      z_sub_adjoint_back (a b c: V): a \ b <<= c -> a <<= b * c;

      (* This complements [dot_sub_cancel_inc], which is a
         derived property of [AdjView]s. *)

      z_dot_sub_cancel_inc_back (a b : V): (~ b == 0) -> a <<= (a * b) \ b;

      (** We can model subtracting from a dot as subtracting from one side,
          then subtracting the remainder from the other, so long as
          we're not subtracting [0]. *)

      z_sub_of_dot (a b c : V) : (~ c == 0) -> ((a * b) \ c) == (a \ c) * (b \ (c \ a));

    }.

Section AdjView_facts.
  Context {V : Type}.
  Context `{AdjView V}.

  (** [eqv_sub_congr] is the analogue of [inc_sub_congr] for equivalence. *)

  Corollary eqv_sub_congr (a b c : V) :
    a == b -> (a \ c) == (b \ c).
  Proof.
    (* By reducing to an inclusion problem. *)
    apply inc_equiv_lift with (f := flip sub c).
    intros x y.
    apply inc_sub_congr.
  Qed.

  (** [sub_inc] tells us that [a] always includes a subtraction of itself. *)

  Corollary sub_inc (a b : V):
    a \ b <<= a.
  Proof.
    apply sub_adjoint, inc_dot_mono_right.
  Qed.

  (** [dot a (sub b c)] is a included in [dot a b]. *)

  Corollary sub_dot_inc_r (a b c : V):
    a * (b \ c) <<= a * b.
  Proof.
    apply inc_dot_right, sub_inc.
  Qed.

  (** Subtracting a view from [one] yields [one]. *)

  Corollary sub_one_l {MV: ViewsMonoid V} (a : V):
    (1 \ a) == 1.
  Proof.
    (* By reduction to inclusion, and [sub_inc]. *)
    apply inc_one, sub_inc.
  Qed.

  (** Subtracting a view from itself yields [one]. *)

  Corollary sub_self {MV: ViewsMonoid V} (a : V):
    (a \ a) == 1.
  Proof.
    (* By reduction to inclusion, adjoint, and dot inclusion. *)
    apply inc_one, sub_adjoint, inc_dot_mono_left.
  Qed.

  (** If we add, then remove, a view, we can only make the view smaller.

      This is similar to 'WandQ-frame-intro' from
      https://www.cs.princeton.edu/~appel/papers/wand-frame.pdf. *)

  Lemma dot_sub_cancel_inc (a b : V) :
    (a * b) \ b <<= a.
  Proof.
    apply sub_adjoint, equiv_inc, dot_comm.
  Qed.

  (** Adding a view that subtracts itself is a no-operation. *)

  Corollary sub_self_ast {MV : ViewsMonoid V} (a b : V) :
    a * (b \ b) == a.
  Proof.
    setoid_rewrite dot_one_r at 6.
    apply equiv_dot_right, sub_self.
  Qed.

  (** If a view [a] is no bigger than a view [b], then the subtraction of [a]
      from [b] is always [1].

      The converse, [sub_unit_inc], needs [FullView]. *)

  Lemma inc_sub_unit {MV: ViewsMonoid V} (a b: V):
    a <<= b -> (a \ b) == 1.
  Proof.
    intro Hinc.
    apply inc_one, sub_adjoint.
    setoid_rewrite <- dot_one_r.
    apply Hinc.
  Qed.

  (** Subtracting [0] from a view yields [1]. *)

  Lemma sub_zero_r {MV: ViewsMonoid V} {ZV: ZeroViewsSemigroup V} (a : V):
    (a \ 0) == 1.
  Proof.
    apply inc_one, sub_adjoint.
    rewrite dot_zero_left.
    apply zero_inc.
  Qed.
End AdjView_facts.

Section FullView_facts.
  Context {V: Type}
          {SV: Setoid V}
          {VV: ViewsSemigroup V}
          {OV: OrderedViewsSemigroup V}
          {AV: AdjView V}
          {FV: FullView V}.

  (** The inclusion form of cancellativity is equivalent to
      [dot_sub_cancel_inc_back]. *)

  Lemma dot_cancel_inc_equiv :
    (forall a b c, a * b <<= a * c -> b <<= c) <->
    (forall a b, a <<= (a * b) \ b).
  Proof.
    split.
    - intros Hinc a b.
      eapply Hinc.
      apply sub_adjoint_back.
      apply inc_sub_congr, equiv_inc, dot_comm.
    - intros Hinc a b c Hcanc.
      apply sub_adjoint in Hcanc.
      transitivity (a * b \ a).
      + transitivity (b * a \ a).
        * apply Hinc.
        * apply inc_sub_congr, equiv_inc, dot_comm.
      + apply Hcanc.
  Qed.

  (** This means we get it for free for [FullView]s
      (and implementing it directly is pointless). *)

  Corollary dot_cancel_inc (a b c : V) :
    a * b <<= a * c -> b <<= c.
  Proof.
    apply dot_cancel_inc_equiv, dot_sub_cancel_inc_back.
  Qed.

  (** [FullView]s are thus cancellative. *)

  Corollary dot_cancel (a b c : V) :
    a * b == a * c -> b == c.
  Proof.
    intro Hequiv.
    apply inc_antisymm.
    - (* <<= *)
      eapply dot_cancel_inc, equiv_inc, Hequiv.
    - (* =>> *)
      eapply dot_cancel_inc, equiv_inc, symmetry, Hequiv.
  Qed.

  (** Any view is equivalent to the result of adding, then removing, another
      view to itself. *)

  Lemma dot_sub_cancel (a b: V):
    a == ((a * b) \ b).
  Proof.
    apply inc_antisymm.
    - apply dot_sub_cancel_inc_back.
    - apply dot_sub_cancel_inc.
  Qed.

  (** If a view [a] is smaller than a view [b], the opposite relationship
      holds if we remove [a] and [b] from some other view [c]. *)

  Lemma sub_inc_invert (a b c: V):
    a <<= b -> (c \ b) <<= (c \ a).
  Proof.
    intro Hinc.
    apply sub_adjoint.
    transitivity (a * (c \ a)).
    - apply sub_adjoint_back.
      reflexivity.
    - apply inc_dot_left, Hinc.
  Qed.

  (** We can lift the above to an equivalence. *)

  Corollary sub_equiv_invert (a b c : V) :
    a == b -> (c \ a) == (c \ b).
  Proof.
    apply inc_equiv_lift_contra with (f := sub c).
    intros x y.
    apply sub_inc_invert.
  Qed.

  (** [sub] is proper with respect to equivalences for [FullView]s. *)

  Global Instance equiv_sub_Proper : Proper (equiv ==> equiv ==> equiv) sub.
  Proof.
    intros a b Hab c d Hcd.
    (* By applying the dot rule to replace [a] with [b], then again in
       the other direction to replace [c] with [d]. *)
    transitivity (b \ c).
    - apply eqv_sub_congr, Hab.
    - apply sub_equiv_invert, Hcd.
  Qed.

  (** If the subtraction of [a] from [b] is 1, [a] is included in [b].

      The converse, [inc_sub_unit], only needs [AdjView]. *)

  Lemma sub_unit_inc {MV: ViewsMonoid V} (a b: V):
    (a \ b) == 1 -> a <<= b.
  Proof.
    intro Hinc.
    apply equiv_inc in Hinc.
    transitivity (b * 1).
    + apply sub_adjoint_back, Hinc.
    + apply equiv_inc, symmetry, dot_one_r.
  Qed.

  (** Subtracting [1] does nothing. *)

  Lemma sub_one_r {MV: ViewsMonoid V} (a: V):
    (a \ 1) == a.
  Proof.
    transitivity ((a * 1) \ 1).
    - apply eqv_sub_congr, dot_one_r.
    - apply symmetry, dot_sub_cancel.
  Qed.

  (** Subtracting a view from [0] yields [0]. *)

  Lemma sub_zero_l {MV: ViewsMonoid V} {ZV: ZeroViewsSemigroup V} (a : V):
    (0 \ a) == 0.
  Proof.
    apply inc_zero.
    transitivity ((a * 0) \ a).
    - rewrite sub_of_dot.
      rewrite sub_self, sub_one_r.
      apply equiv_inc, dot_zero_right.
    - apply equiv_inc, equiv_sub_Proper.
      + apply symmetry, dot_zero_right.
      + reflexivity.
  Qed.

  (** This means that [0] is [1].
      In practice, this tells us that any separating zero views semigroup is
      a singleton semigroup, and thus useless. *)

  Corollary zero_is_one {MV: ViewsMonoid V} {ZV: ZeroViewsSemigroup V} :
    0 == 1.
  Proof.
    rewrite <- sub_zero_l with (a := 0).
    now rewrite sub_zero_r.
  Qed.

  Corollary full_view_zero_explode {MV: ViewsMonoid V} {ZV: ZeroViewsSemigroup V}
            (a b : V) :
    a == b.
  Proof.
    apply inc_antisymm.
    - transitivity 0.
      + apply zero_inc.
      + rewrite zero_is_one.
        apply one_inc.
    - transitivity 1.
      + apply one_inc.
      + rewrite <- zero_is_one.
        apply zero_inc.
  Qed.

  (** A view can only grow if we subtract another view, then immediately
      re-add it.

      This is similar to 'wandQ-frame-elim' from
      https://www.cs.princeton.edu/~appel/papers/wand-frame.pdf. *)
  Lemma inc_sub_dot (a b : V) :
    a <<= b * (a \ b).
  Proof.
    apply sub_adjoint_back, reflexivity.
  Qed.

  (** If a view [a] includes a view [b], it can be re-expressed as the
      join of [a] and the difference between [b] and [a].

      This is a fairly fundamental property of many similar algebras;
      in our case it derives from the [sub_of_dot] and [inc_sub_dot]
      properties. *)

  Lemma inc_join (a b : V) {MV : ViewsMonoid V} :
    a =>> b ->
    a == (b * (a \ b)).
  Proof.
    intro Hinc.
    apply inc_antisymm.
    - apply inc_sub_dot.
    - unfold flip.
      transitivity ((b \ (b \ a)) * (a \ b)).
      + apply inc_dot_left.
        apply equiv_inc.
        transitivity (b \ 1).
        * apply symmetry, sub_one_r.
        * apply equiv_sub_Proper; try easy.
          apply symmetry, inc_sub_unit, Hinc.
      + rewrite dot_comm, <- sub_of_dot.
        apply dot_sub_cancel_inc.
  Qed.

  (** Subtracting by a dot is the same as subtracting by each side
      individually. *)

  Lemma sub_by_dot (a b c : V) :
    (a \ (b * c)) == ((a \ b) \ c).
  Proof.
    apply inc_antisymm.
    - apply sub_adjoint.
      transitivity (b * (a \ b)).
      + apply inc_sub_dot.
      + rewrite dot_assoc.
        apply inc_dot_right.
        apply inc_sub_dot.
    - apply sub_adjoint.
      apply sub_adjoint.
      rewrite <- dot_assoc.
      apply inc_sub_dot.
  Qed.

  (** Subtraction half-distributes over dot. *)

  Lemma inc_sub_hor (a1 b1 a2 b2 : V) :
    ((a1 * a2) \ (b1 * b2)) <<= (a1 \ b1) * (a2 \ b2).
  Proof.
    apply sub_adjoint.
    setoid_rewrite dot_comm at 4.
    setoid_rewrite <- dot_assoc.
    setoid_rewrite dot_assoc at 2.
    setoid_rewrite dot_comm at 3.
    setoid_rewrite dot_assoc at 1.
    setoid_rewrite dot_comm at 1.
    apply inc_dot_proper; apply inc_sub_dot.
  Qed.

  (** If we have a property stating that subviews of [a] are subviews of
      [b] if they obey [filter], and [filter] is closed under inclusion,
      then we can glue on any context [c] to [a] and [b] and still have
      the property hold. *)

  Lemma filtered_subset_extend (a b c : V) (filter : V -> Prop) :
    (forall u : V, u <<= a -> filter u -> u <<= b) ->
    (forall u v : V, u <<= v -> filter v -> filter u) ->
    (forall u : V, u <<= c * a -> filter u -> u <<= c * b).
  Proof.
    intros Horig Hfilter_mono u Hinc_a Hfilter_u.
    (* By the adjoint property and the fact that if the filter holds in [u],
       it must also hold in [u\c]. *)
    apply sub_adjoint in Hinc_a.
    apply Hfilter_mono with (u \ c) u in Hfilter_u.
    apply sub_adjoint_back.
    - apply (Horig (u \ c) Hinc_a Hfilter_u).
    - apply sub_inc.
  Qed.

  (** We can weaken the inclusion property to allow [filter] to fail to hold
      if [u] is [one]. *)

  Corollary filtered_subset_extend_one
            {MV: ViewsMonoid V}
            (a b c : V) (filter : V -> Prop) :
    (forall u : V, u <<= a -> filter u -> u <<= b) ->
    (forall u v : V, u <<= v -> filter v -> filter u \/ u == one) ->
    (forall u : V, u <<= c * a -> filter u -> u <<= c * b).
  Proof.
    intros Horig Hfilter_mono u Hinc_a Hfilter_u.
    apply filtered_subset_extend with a (fun u => filter u \/ u == one);
      try assumption.
    - intros w Hinc_w_a [Hfilter_w|Hw_one].
      + apply Horig; assumption.
      + transitivity (1 : V).
        * apply equiv_inc, Hw_one.
        * apply one_inc.
    - intros w v Hinc_w_v [Hfilter_v|Hv_one].
      + apply Hfilter_mono with v; assumption.
      + right.
        apply inc_one.
        transitivity v.
        * exact Hinc_w_v.
        * apply equiv_inc, Hv_one.
    - left; assumption.
  Qed.


  Section induced_one.

    (** This section assumes that [V] is inhabited. *)
    Variable witness : V.

    (** If [V] is inhabited by [witness], we can induce a unit as
        [witness \ witness]. *)

    Definition induced_one : V := witness \ witness.

    Lemma induced_one_sub (x : V) :
      induced_one <<= x.
    Proof.
      unfold induced_one.
      apply sub_adjoint, inc_dot_mono_left.
    Qed.

    Lemma induced_one_inc_sub (x y : V) :
      x <<= y -> induced_one == (x \ y).
    Proof.
      intro Hxy.
      apply inc_antisymm.
      - apply induced_one_sub.
      - apply sub_adjoint.
        rewrite dot_comm.
        transitivity y; try easy.
        apply inc_dot_mono_right.
    Qed.

    Lemma blah (x : V) :
      (induced_one \ x) == induced_one.
    Proof.
      apply inc_antisymm.
      - apply sub_inc.
      - apply sub_adjoint, inc_dot_mono_left.
    Qed.

(*
    Lemma induced_one_dot :
      induced_one * induced_one == induced_one.
    Proof.
      unfold induced_one.
      transitivity ((witness * witness) \ (witness * witness)).
      - apply inc_antisymm.
        + 
induced_one_inc_sub. inc_dot_mono_right.
      - rewrite <- sub_of_dot.
      - apply inc_sub_
*)
    Corollary induced_one_not_unique (x : V) :
      induced_one == (x \ x).
    Proof.
      apply induced_one_inc_sub, reflexivity.
    Qed.
(*
    Lemma induced_one_dot (x : V) :
      induced_one * x <<= x.
    Proof.
      apply dot_cancel_inc with (a := induced_one).
      apply sub_adjoint_back.
      
      rewrite (induced_one_inc_sub x (x * x)).
      - rewrite sub_by_dot.
        


      transitivity (x \ x) y * (x * (x \ x)).
      rewrite <- sub_of_dot.
      apply sub_adjoint.
      rewrite inc_sub_dot.
      transitivity (induced_one * (x \ induced_one)).
      - unfold induced_one.

    Qed.

    Lemma induced_one_div_self :
      (induced_one \ induced_one) == induced_one.
    Proof.
      apply inc_antisymm.
      - apply sub_adjoint, inc_dot_mono_right.
      - apply sub_adjoint.

    Instance induced_ViewsMonoid : ViewsMonoid V :=
      { one := induced_one }.
    Proof.
      intro x.
      unfold induced_one.
      apply inc_antisymm.
      - transitivity ((induced_one \ x) * (x \ (induced_one \ induced_one)).
        + apply inc_dot_left, induced_one_sub.

*)
  End induced_one.
End FullView_facts.

Section ZeroSeparatingViewsSemigroup_facts.
  Context {V: Type}
          {SV: Setoid V}
          {VV: ViewsSemigroup V}
          {OV: OrderedViewsSemigroup V}
          {AV: AdjView V}
          {ZV: ZeroViewsSemigroup V}
          {FV: ZeroSeparatingViewsSemigroup V}.

  (** The inclusion form of cancellativity is equivalent to
      [z_dot_sub_cancel_inc_back]. *)

  Lemma z_dot_cancel_inc_equiv :
    (forall a b c, ~ a == 0 -> a * b <<= a * c -> b <<= c) <->
    (forall a b, ~ b == 0 -> a <<= (a * b) \ b).
  Proof.
    split.
    - intros Hinc a b Hnb.
      eapply Hinc.
      + exact Hnb.
      + apply z_sub_adjoint_back, inc_sub_congr, equiv_inc, dot_comm.
    - intros Hinc a b c Hna Hcanc.
      apply sub_adjoint in Hcanc.
      transitivity (a * b \ a).
      + transitivity (b * a \ a).
        * apply Hinc, Hna.
        * apply inc_sub_congr, equiv_inc, dot_comm.
      + apply Hcanc.
  Qed.

  (** This means we get it for free for [FullView]s
      (and implementing it directly is pointless). *)

  Corollary z_dot_cancel_inc (a b c : V) :
    ~ a == 0 -> a * b <<= a * c -> b <<= c.
  Proof.
    apply z_dot_cancel_inc_equiv, z_dot_sub_cancel_inc_back.
  Qed.

  (** [ZeroSeparatingViewsSemigroup]s are thus cancellative up to [0]. *)

  Corollary z_dot_cancel (a b c : V) :
    ~ a == 0 -> a * b == a * c -> b == c.
  Proof.
    intros Hna Hequiv.
    apply inc_antisymm.
    - (* <<= *)
      now eapply z_dot_cancel_inc, equiv_inc, Hequiv.
    - (* =>> *)
      now eapply z_dot_cancel_inc, equiv_inc, symmetry, Hequiv.
  Qed.

  (** Any view is equivalent to the result of adding, then removing, another
      nonzero view to itself. *)

  Lemma z_dot_sub_cancel (a b: V):
    ~ b == 0 -> a == ((a * b) \ b).
  Proof.
    intro Hnb.
    apply inc_antisymm.
    - apply z_dot_sub_cancel_inc_back, Hnb.
    - apply dot_sub_cancel_inc.
  Qed.

  (** The equivalent of [sub_inc_invert] for separating semigroups with zeroes. *)

  Lemma z_sub_inc_invert (a b c: V):
    a <<= b -> (c \ b) <<= (c \ a).
  Proof.
    intro Hinc.
    apply sub_adjoint.
    transitivity (a * (c \ a)).
    - apply z_sub_adjoint_back.
      reflexivity.
    - apply inc_dot_left, Hinc.
  Qed.

  (** We can lift the above to an equivalence. *)

  Corollary z_sub_equiv_invert (a b c : V) :
    a == b -> (c \ a) == (c \ b).
  Proof.
    apply inc_equiv_lift_contra with (f := sub c).
    intros x y.
    apply z_sub_inc_invert.
  Qed.

  (** [sub] is proper with respect to equivalences for [ZeroSeparatingViewsSemigroup]s. *)

  Global Instance z_equiv_sub_Proper : Proper (equiv ==> equiv ==> equiv) sub.
  Proof.
    intros a b Hab c d Hcd.
    (* By applying the dot rule to replace [a] with [b], then again in
       the other direction to replace [c] with [d]. *)
    transitivity (b \ c).
    - apply eqv_sub_congr, Hab.
    - apply z_sub_equiv_invert, Hcd.
  Qed.

  (** If the subtraction of [a] from [b] is 1, [a] is included in [b].

      The converse, [inc_sub_unit], only needs [AdjView]. *)

  Lemma z_sub_unit_inc {MV: ViewsMonoid V} (a b: V):
    (a \ b) == 1 -> a <<= b.
  Proof.
    intro Hinc.
    apply equiv_inc in Hinc.
    transitivity (b * 1).
    + apply z_sub_adjoint_back, Hinc.
    + apply equiv_inc, symmetry, dot_one_r.
  Qed.

  (** Subtracting [1] does nothing, so long as [1] isn't actually [0]! *)

  Lemma z_sub_one_r {MV: ViewsMonoid V} (a: V):
    ~ 1 == 0 -> (a \ 1) == a.
  Proof.
    transitivity ((a * 1) \ 1).
    - apply eqv_sub_congr, dot_one_r.
    - now apply symmetry, z_dot_sub_cancel.
  Qed.

  (** Subtracting a nonzero view from [0] yields [0], as long as [1] isn't
      actually [0]!. *)

  Lemma z_sub_zero_l {MV: ViewsMonoid V} (a : V):
    ~ 1 == 0 -> ~ a == 0 -> (0 \ a) == 0.
  Proof.
    intros H1 Ha.
    apply inc_zero.
    transitivity ((a * 0) \ a).
    - rewrite z_sub_of_dot; try easy.
      rewrite sub_self, z_sub_one_r; try easy.
      apply equiv_inc, dot_zero_right.
    - apply equiv_inc, z_equiv_sub_Proper.
      + apply symmetry, dot_zero_right.
      + reflexivity.
  Qed.

  (** A view can only grow if we subtract another view, then immediately
      re-add it.

      This is similar to 'wandQ-frame-elim' from
      https://www.cs.princeton.edu/~appel/papers/wand-frame.pdf. *)
  Lemma z_inc_sub_dot (a b : V) :
    a <<= b * (a \ b).
  Proof.
    now apply z_sub_adjoint_back.
  Qed.

  (** Subtracting by a dot is the same as subtracting by each side
      individually. *)

  Lemma z_sub_by_dot (a b c : V) :
    (a \ (b * c)) == ((a \ b) \ c).
  Proof.
    apply inc_antisymm.
    - apply sub_adjoint.
      transitivity (b * (a \ b)).
      + apply z_inc_sub_dot.
      + rewrite dot_assoc.
        apply inc_dot_right.
        apply z_inc_sub_dot.
    - apply sub_adjoint.
      apply sub_adjoint.
      rewrite <- dot_assoc.
      apply z_inc_sub_dot.
  Qed.

  (** Subtraction half-distributes over dot. *)

  Lemma z_inc_sub_hor (a1 b1 a2 b2 : V) :
    ((a1 * a2) \ (b1 * b2)) <<= (a1 \ b1) * (a2 \ b2).
  Proof.
    apply sub_adjoint.
    setoid_rewrite dot_comm at 4.
    setoid_rewrite <- dot_assoc.
    setoid_rewrite dot_assoc at 2.
    setoid_rewrite dot_comm at 3.
    setoid_rewrite dot_assoc at 1.
    setoid_rewrite dot_comm at 1.
    apply inc_dot_proper; apply z_inc_sub_dot.
  Qed.

  (** If we have a property stating that subviews of [a] are subviews of
      [b] if they obey [filter], and [filter] is closed under inclusion,
      then we can glue on any context [c] to [a] and [b] and still have
      the property hold. *)

  Lemma z_filtered_subset_extend (a b c : V) (filter : V -> Prop) :
    (forall u : V, u <<= a -> filter u -> u <<= b) ->
    (forall u v : V, u <<= v -> filter v -> filter u) ->
    (forall u : V, u <<= c * a -> filter u -> u <<= c * b).
  Proof.
    intros Horig Hfilter_mono u Hinc_a Hfilter_u.
    (* By the adjoint property and the fact that if the filter holds in [u],
       it must also hold in [u\c]. *)
    apply sub_adjoint in Hinc_a.
    apply Hfilter_mono with (u \ c) u in Hfilter_u.
    apply z_sub_adjoint_back.
    - apply (Horig (u \ c) Hinc_a Hfilter_u).
    - apply sub_inc.
  Qed.

  (** We can weaken the inclusion property to allow [filter] to fail to hold
      if [u] is [one]. *)

  Corollary z_filtered_subset_extend_one
            {MV: ViewsMonoid V}
            (a b c : V) (filter : V -> Prop) :
    (forall u : V, u <<= a -> filter u -> u <<= b) ->
    (forall u v : V, u <<= v -> filter v -> filter u \/ u == one) ->
    (forall u : V, u <<= c * a -> filter u -> u <<= c * b).
  Proof.
    intros Horig Hfilter_mono u Hinc_a Hfilter_u.
    apply z_filtered_subset_extend with a (fun u => filter u \/ u == one);
      try assumption.
    - intros w Hinc_w_a [Hfilter_w|Hw_one].
      + apply Horig; assumption.
      + transitivity (1 : V).
        * apply equiv_inc, Hw_one.
        * apply one_inc.
    - intros w v Hinc_w_v [Hfilter_v|Hv_one].
      + apply Hfilter_mono with v; assumption.
      + right.
        apply inc_one.
        transitivity v.
        * exact Hinc_w_v.
        * apply equiv_inc, Hv_one.
    - left; assumption.
  Qed.
End ZeroSeparatingViewsSemigroup_facts.

(** [DecOrderedViewsSemigroup] is an ordered views semigroup whose
    ordering is decidable. *)

Class DecOrderedViewsSemigroup (V: Type)
      {S: Setoid V}
      {VS: ViewsSemigroup V}
      {OS: OrderedViewsSemigroup V} :=
  mkDecOrderedViewsSemigroup
    {
      (** [inc] is decidable. *)
      inc_Decidable x y :> Decidable (inc x y)
    }.

Section DecOrderedViewsSemigroup_facts.

  Variable V: Type.

  Context
    {S: Setoid V}
    {VS: ViewsSemigroup V}
    {OS: OrderedViewsSemigroup V}
    {DS: DecOrderedViewsSemigroup V}.

  (** We can restate decidability as a sumbool. *)

  Lemma inc_dec (x y: V): {x <<= y} + {~(x <<= y)}.
  Proof.
    decide (inc x y) as Hinc; firstorder.
  Qed.

  (** Decidability of inclusion gives us decidability of equality. *)

  Global Instance inc_Decidable_equiv_Decidable (x y: V): Decidable (equiv x y) :=
    {
      Decidable_witness := andb (decide (inc x y)) (decide (inc y x))
    }.
  Proof.
    split.
    - intros (Hxy%Decidable_sound & Hyx%Decidable_sound)%Bool.andb_true_iff.
      apply inc_antisymm; assumption.
    - intro Hequiv.
      apply Bool.andb_true_iff.
      split; apply Decidable_complete, equiv_inc; firstorder.
  Defined.

  Corollary equiv_dec (x y: V): {x == y} + {~(x == y)}.
  Proof.
    decide (equiv x y) as Hequiv; firstorder.
  Qed.

  (** This gives us [EqDec] on the setoid for [V]. *)

  Global Instance DecOrderedViewsSemigroup_EqDec : EqDec S := equiv_dec.

End DecOrderedViewsSemigroup_facts.
Local Close Scope views_scope.
