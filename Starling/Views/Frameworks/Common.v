(** * Common framework infrastructure

    Here, we implement the core parts of both the CVF and the LVF:
    labels, axioms, labelled semantics, and Views-Hoare judgements.

    We don't prove soundness of views instances here: instead, we rely
    on a translation from this mini-framework to the original Views
    development in a separate file. *)

From Coq Require Import
     Program.Basics
     Classes.SetoidTactics
     Classes.SetoidClass
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     Utils.Ensemble.Facts
     Views.Classes.

From Starling Require Export
     Views.Frameworks.Common.Language
     Views.Frameworks.Common.Signatures
     Views.Frameworks.Common.Reifier.

Set Implicit Arguments.

Chapter framework_common.

Variables
  (V   : Type)
  (C S : Set).

Context
  {SV : Setoid V}
  {VV : ViewsSemigroup V}.

(** [label_sem] lifts semantic functions to serve the id label. *)

Definition label_sem (sem : Semantics C S) : LabelC C -> S -> Ensemble S :=
  label_maybe (Singleton S) sem.

Lemma id_semantics (sem: Semantics C S) (s: S):
  label_sem sem (Id C) s = Singleton S s.
Proof.
  reflexivity.
Qed.

(** Lifting [Id] over a state set [ss] is the identity. *)

Lemma lift_id_semantics (sem : Semantics C S) (ss: Ensemble S):
  Same_set S (set_lift (label_sem sem (Id C)) ss) ss.
Proof.
  split.
  - apply set_lift_gen_dist.
    intros s Hin_ss s' <-%Singleton_inv.
    exact Hin_ss.
  - intros s Hss_s_in.
    exists s.
    split.
    + exact Hss_s_in.
    + apply In_singleton.
Qed.

Record ViewsAxiom :=
  mk_ViewsAxiom
    {
      pre: V;
      cmd: LabelC C;
      post: V
    }.

Section views_hoare_judgements.
  Definition shoare (sig : Signature C S) (l : LabelC C) (hp hq : Ensemble S) : Prop :=
    Included S
             (set_lift (label_sem sig.(sig_sem) l) hp)
             hq.

  (** We can rewrite the precondition and postcondition of a
      [vhoare] to equivalent sets. *)
  Global Instance shoare_Same_set_Proper (sig : Signature C S) (c : LabelC C) :
    Proper (Same_set S ==> Same_set S ==> iff) (shoare sig c).
  Proof.
    intros w x Hwx y z Hyz.
    split; intros Heqhoare s Hin_trans.
    - apply Hyz, Heqhoare.
      eapply set_lift_Proper_Included.
      + apply Hwx.
      + exact Hin_trans.
    - apply Hyz, Heqhoare.
      eapply set_lift_Proper_Included.
      + apply Hwx.
      + exact Hin_trans.
  Qed.

  (** Rule of consequence: we can strengthen the precondition and
      weaken the precondition of a [shoare]. *)

  Lemma shoare_cons (sig : Signature C S) (hp hp' hq hq' : Ensemble S) (c : LabelC C):
    Included S hp hp' ->
    Included S  hq' hq ->
    shoare sig c hp' hq' ->
    shoare sig c hp hq.
  Proof.
    intros Hp Hq Hp'q' s' (s & Hs & Htrans).
    apply Hq.
    apply Hp'q'.
    exists s.
    split.
    - apply Hp, Hs.
    - apply Htrans.
  Qed.

  Corollary shoare_cons_p (sig : Signature C S) (hp hp' hq : Ensemble S) (c : LabelC C):
    Included S hp hp' ->
    shoare sig c hp' hq ->
    shoare sig c hp hq.
  Proof.
    intros Hp.
    apply shoare_cons.
    - exact Hp.
    - reflexivity.
  Qed.

  Corollary shoare_cons_q (sig : Signature C S) (hp hq hq' : Ensemble S) (c : LabelC C):
    Included S hq' hq ->
    shoare sig c hp hq' ->
    shoare sig c hp hq.
  Proof.
    intros Hq.
    apply shoare_cons.
    - reflexivity.
    - exact Hq.
  Qed.

  (** [vhoare] is the Views-Hoare judgement. *)

  Definition vhoare (sig : Signature C S) (c : LabelC C) (hp hq : V) : Prop :=
    shoare sig c (reify (sig_reifier sig) hp) (reify (sig_reifier sig) hq).

  Definition rinc (sig: Signature C S) (x y: V) :=
    Included S
             (reify (sig_reifier sig) x)
             (reify (sig_reifier sig) y).

  Lemma vhoare_rinc_id (sig : Signature C S) (x y : V) :
    rinc sig x y <-> vhoare sig (Id C) x y.
  Proof.
    split.
    - intros Hrinc s Hcmd%lift_id_semantics.
      apply Hrinc, Hcmd.
    - intros Hvhoare s Hin_x.
      apply Hvhoare, lift_id_semantics, Hin_x.
  Qed.


  (** Views-Hoare, with the identity command, is reflexive. *)

  Lemma vhoare_id_refl (sig : Signature C S) (p : V) :
    vhoare sig (Id C) p p.
  Proof.
    now intros s Htrans%lift_id_semantics.
  Qed.

  (** Rule of consequence: we can strengthen the precondition and
      weaken the precondition of a [vhoare]. *)

  Lemma vhoare_cons (sig : Signature C S) (c : LabelC C) (hp hp' hq hq' : V) :
    rinc sig hp  hp' ->
    rinc sig hq' hq  ->
    vhoare sig c hp' hq' ->
    vhoare sig c hp  hq.
  Proof.
    apply shoare_cons.
  Qed.

  Corollary vhoare_cons_p (sig : Signature C S) (c : LabelC C) (hp hp' hq : V) :
    rinc sig hp hp' ->
    vhoare sig c hp' hq ->
    vhoare sig c hp  hq.
  Proof.
    apply shoare_cons_p.
  Qed.

  Corollary vhoare_cons_q (sig : Signature C S) (c : LabelC C) (hp hq hq' : V) :
    rinc sig hq' hq ->
    vhoare sig c hp hq' ->
    vhoare sig c hp hq .
  Proof.
    apply shoare_cons_q.
  Qed.

  (** We can rewrite the precondition and postcondition of a
      [vhoare] to equivalent views, since we're using a [Reifier]
      to back it. *)
  Global Instance vhoare_equiv_Proper (sig : Signature C S) (c : LabelC C) :
    Proper (equiv ==> equiv ==> iff) (vhoare sig c).
  Proof.
    intros w x Hwx y z Hyz.
    apply shoare_Same_set_Proper; apply (proj2_sig sig.(sig_reifier)); assumption.
  Qed.
End views_hoare_judgements.

Section action_judgements.
  Variable
    (PrimV : Type).

  Context
    {PrimVS : Setoid PrimV}
    {PrimVV : ViewsSemigroup PrimV}.

  Variables
    (hoare : LabelC C -> V -> V -> Prop)
    (plift : PrimV -> V).

  (** [actionj] is the base skeleton of an action judgement. *)

  Definition actionj (c : LabelC C) (p q : V) : Prop :=
    hoare c p q /\ (forall v : PrimV, hoare c (p * plift v) (q * plift v))%views.

  (** If the Hoare judgement is proper wrt equivalence, so is the action
      judgement. *)

  Global Instance actionj_equiv_Proper (c : LabelC C) :
    Proper (equiv ==> equiv ==> iff) (hoare c) ->
    Proper (equiv ==> equiv ==> iff) (actionj c).
  Proof.
    intros Hhoare_Proper w x Hwx y z Hyz.
    split; intros (Hpc & Hni).
    - split.
      + eapply Hhoare_Proper.
        * symmetry; exact Hwx.
        * symmetry; exact Hyz.
        * exact Hpc.
      + intro r.
        eapply Hhoare_Proper.
        * apply equiv_dot_left.
          symmetry; exact Hwx.
        * apply equiv_dot_left.
          symmetry; exact Hyz.
        * apply Hni.
    - split.
      + eapply Hhoare_Proper.
        * exact Hwx.
        * exact Hyz.
        * exact Hpc.
      + intro r.
        eapply Hhoare_Proper.
        * apply equiv_dot_left.
          exact Hwx.
        * apply equiv_dot_left.
          exact Hyz.
        * apply Hni.
  Defined.

  (** If the Hoare judgement is reflexive on entailment, so is the action
      judgement. *)

  Definition actionj_id_refl (c : LabelC C) :
    (forall v : V, hoare c v v) ->
    (forall v : V, actionj c v v).
  Proof.
    intros Hhoare_refl v.
    split.
    - apply Hhoare_refl.
    - intro v'.
      apply Hhoare_refl.
  Qed.
End action_judgements.

Section standard_action_judgements.
  Definition vactionj (sig : Signature C S) (c : LabelC C) (p q : V) : Prop :=
    actionj (vhoare sig) id c p q.

  Global Instance vactionj_equiv_Proper (sig : Signature C S) (c : LabelC C) :
    Proper (equiv ==> equiv ==> iff) (vactionj sig c).
  Proof.
    apply actionj_equiv_Proper, vhoare_equiv_Proper.
  Defined.

  (** Views--Hoare is commutative. *)

  Lemma vhoare_comm (sig : Signature C S) (p1 p2 q1 q2 : V) (c : LabelC C):
    vhoare sig c (p1 * p2)%views (q1 * q2)%views ->
    vhoare sig c (p2 * p1)%views (q2 * q1)%views.
  Proof.
    intros Hflipped s (s' & Hin_p & Hcmd).
    (* Flip postcondition first *)
    eapply (proj2_sig sig.(sig_reifier)).
    - apply dot_comm.
    - refine (Hflipped s _).
      exists s'.
      split.
      + (* Flip precondition now. *)
        eapply (proj2_sig sig.(sig_reifier)).
        * apply dot_comm.
        * exact Hin_p.
      + exact Hcmd.
  Qed.

  (** Semantic entailment is reflexive. *)

  Lemma vactionj_id_refl (sig : Signature C S) (p : V) :
    vactionj sig (Id C) p p.
  Proof.
    apply actionj_id_refl, vhoare_id_refl.
  Qed.

  (** Rule of consequence: we can strengthen the precondition and
      weaken the precondition of a [vactionj]. *)

  Lemma vactionj_cons (sig : Signature C S) (c : LabelC C) (hp hp' hq hq' : V) :
    vactionj sig (Id C) hp  hp' ->
    vactionj sig (Id C) hq' hq  ->
    vactionj sig c      hp' hq' ->
    vactionj sig c      hp  hq.
  Proof.
    intros (Hrinc_p_pc & Hrinc_p_ni) (Hrinc_q_pc & Hrinc_q_ni) (Hvactionj_pc & Hvactionj_ni).
    split.
    - apply vhoare_rinc_id in Hrinc_p_pc.
      apply vhoare_rinc_id in Hrinc_q_pc.
      apply vhoare_cons with (hp' := hp') (hq' := hq'); assumption.
    - intros v.
      specialize (Hrinc_p_ni v).
      specialize (Hrinc_q_ni v).
      specialize (Hvactionj_ni v).
      apply vhoare_rinc_id in Hrinc_p_ni.
      apply vhoare_rinc_id in Hrinc_q_ni.
      apply vhoare_cons with (hp' := (hp' * v)%views) (hq' := (hq' * v)%views); assumption.
  Qed.

  Corollary vactionj_cons_p (sig : Signature C S) (c : LabelC C) (hp hp' hq : V) :
    vactionj sig (Id C) hp hp' ->
    vactionj sig c  hp' hq ->
    vactionj sig c  hp  hq.
  Proof.
    intros Hpp' Horig.
    pose (vactionj_id_refl sig hq) as Hid.
    now apply vactionj_cons with (hp' := hp') (hq' := hq).
  Qed.

  Corollary vactionj_cons_q (sig : Signature C S) (c : LabelC C) (hp hq hq' : V) :
    vactionj sig (Id C) hq' hq ->
    vactionj sig c  hp hq' ->
    vactionj sig c  hp hq .
  Proof.
    intros Hq'q Horig.
    pose (vactionj_id_refl sig hp) as Hid.
    now apply vactionj_cons with (hp' := hp) (hq' := hq').
  Qed.

  Lemma vactionj_closed (sig : Signature C S) (c : LabelC C) (p q r : V) :
    vactionj sig c p q ->
    vactionj sig c (p * r)%views (q * r)%views.
  Proof.
    intros (_ & Hsl_ni).
    split.
    - apply Hsl_ni.
    - intros r2.
      setoid_rewrite dot_assoc.
      apply Hsl_ni.
  Qed.

  Corollary vactionj_closed_l (sig : Signature C S) (c : LabelC C) (p q r : V) :
    vactionj sig c p q ->
    vactionj sig c (r * p)%views (r * q)%views.
  Proof.
    intros Hpq.
    setoid_rewrite dot_comm.
    apply vactionj_closed, Hpq.
  Qed.

  Lemma vactionj_pair (sig : Signature C S) (p1 p2 q1 q2 : V) :
    vactionj sig (Id C) p1 q1 ->
    vactionj sig (Id C) p2 q2 ->
    vactionj sig (Id C) (p1 * p2)%views (q1 * q2)%views.
  Proof.
    intros H1 H2.
    apply vactionj_cons_p with (hp' := (q1 * p2)%views).
    - apply vactionj_closed, H1.
    - apply vactionj_closed_l, H2.
  Qed.
End standard_action_judgements.

End framework_common.

Notation "<| p |> c <| q |>" :=
  (mk_ViewsAxiom p c q) (at level 0, c, p, q at next level): views_scope.
