(** * The Local Views Framework

The Local Views Framework is an adaptation of Khyzha et al.'s generalized
linearisability logic (http://software.imdea.org/~artem/papers/fm16-extended.pdf)
to partial correctness, with various other changes to make it fit in Starling. *)

From Starling Require Export
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.Instances
     Views.Frameworks.LVF.Language
     Views.Frameworks.LVF.SafetyRules
     Views.Frameworks.LVF.SemJudgements
     Views.Frameworks.LVF.Signatures
     Views.Frameworks.LVF.Soundness
     Views.Frameworks.LVF.ThreadLists
     Views.Frameworks.LVF.Threads
.
