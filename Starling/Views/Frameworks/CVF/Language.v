(** * Views Framework: Programming language *)

From Coq Require Import
     Arith.PeanoNat
     Bool.BoolEq
     Lists.List
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles
     Relations.Relation_Definitions
     Classes.SetoidClass.

From Starling Require Import
     Utils.List.Facts     
     Utils.List.Override
     Views.Classes
     Views.Transformers.Function     
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core.

Set Implicit Arguments.
  
Section views_language.
  Variables
    (V    : Type)  (* Shared views *)
    (C    : Set)   (* Atomic actions *)
    (SSt  : Set).  (* Shared state *)

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}.
  
  Variable
    (sig : Signature C SSt).

  (** [Prog] is the type of CVF programs. *)
  
  Inductive Prog : Set :=
  | PSkip               (* Skip *)
  | PAtom (c   : C)     (* Atomic action *)
  | PIter (p   : Prog)  (* Iteration *)
  | PSeq  (l r : Prog)  (* Sequential composition *)
  | PNdt  (l r : Prog)  (* Nondeterministic choice *)
  | PPar  (l r : Prog). (* Parallel composition *)

  (** [Transition] is the CVF labelled transition system. *)
  
  Inductive Transition : Prog -> LabelC C -> Prog -> Prop :=
  | TAtom (c : C):
      Transition (PAtom c) (Cmd c) PSkip
  | TSeqStep (p p' q : Prog) (lc: LabelC C) :
      Transition p lc p' ->
      Transition (PSeq p q) lc (PSeq p' q)
  | TSeqSkip (p : Prog) :
      Transition (PSeq PSkip p) (Id C) p
  | TIterStep (p : Prog) :
      Transition (PIter p) (Id C) (PSeq p (PIter p))
  | TIterSkip (p : Prog) :
      Transition (PIter p) (Id C) PSkip
  | TNdtLeft (p q : Prog) :
      Transition (PNdt p q) (Id C) p
  | TNdtRight (p q : Prog) :
      Transition (PNdt p q) (Id C) q
  | TParLeftStep (p p' q : Prog) (lc : LabelC C) :
      Transition p lc p' ->
      Transition (PPar p q) lc (PPar p' q)                 
  | TParLeftSkip (q : Prog) :
      Transition (PPar PSkip q) (Id C) q
  | TParRightStep (p q q' : Prog) (lc : LabelC C) :
      Transition q lc q' ->
      Transition (PPar p q) lc (PPar p q')
  | TParRightSkip (p : Prog) :
      Transition (PPar p PSkip) (Id C) p.

  (** [PSkip] can't transition out. *)
  
  Lemma transition_skip (pr: Prog) (lc: LabelC C):
    ~ Transition PSkip lc pr.
  Proof.
    inversion 1.
  Qed.

  CoInductive Safe : V -> Prog -> V -> Prop :=
  | mk_Safe p c q :
      (c = PSkip -> vactionj sig (Id C) p q) ->
      (forall lc c',
          Transition c lc c' ->
          (exists (p' : V), vactionj sig lc p p' /\ Safe p' c' q)) ->
      Safe p c q.  
    
  Inductive MTransition : Prog -> SSt -> Prog -> SSt -> Prop :=
    | MT_empty (c : Prog) (s : SSt) : MTransition c s c s
    | MT_step (c1 c2 c3 : Prog) (s1 s2 s3 : SSt) (lc : LabelC C):
        Transition c1 lc c2 ->
        label_sem sig.(sig_sem) lc s1 s2 ->
        MTransition c2 s2 c3 s3 ->
        MTransition c1 s1 c3 s3.
  
  Definition sound (p q : V) (c : Prog) (s s': SSt) :=
      In _ (sig.(sig_reify) p) s ->
      MTransition c s PSkip s' ->
      In _ (sig.(sig_reify) q) s'.

  (** If a program is safe starting from its initial local state,
      it is sound. *)
  
  Lemma safe_sound (p q : V) (c : Prog) (s s' : SSt) :
    Safe p c q -> sound p q c s s'.
  Proof.
    (* This is very similar to the original Coq development,
       by necessity. *)
    intros Hsafe Hin_p Htrans.
    remember PSkip as c'.
    generalize dependent p.
    induction Htrans; intros p Hsafe Hin_p.
    - (* Skip case *)
      inversion Hsafe; subst.
      specialize (H (reflexivity _)).
      apply H, lift_id_semantics, Hin_p.
    - (* Inductive case: at least one transition.

         This time, we use the step case of [Safe]: we'll run it
         forwards to obtain the intermediate view, then do our inductive
         step *)
      inversion Hsafe; subst.
      destruct (H2 lc c2 H) as (p' & Hjudge & Hsafe_ind).
      apply (IHHtrans (reflexivity _) p' Hsafe_ind).
      apply Hjudge.
      exists s1.
      split; assumption.
  Qed.
End views_language.

(** [destruct_cvf_safe] implements the following decision procedure on
    CVF safety goals:

    - Destruct the safety property;
    - If the command is [PSkip], solve the second leg of the property by
      the contradiction that no transition can be made from [PSkip], and
      present the skip action judgement to the user;
    - Otherwise, try to solve the first leg by the contradiction that the
      command is not [PSkip]. *)

Ltac destruct_cvf_safe :=
  match goal with
  | [ |- Safe ?s ?p (PSkip ?C) ?q ] =>
    apply mk_Safe;
    match goal with
    | [ |- (PSkip ?C = PSkip ?C) -> _ ] => intros _
    | _ => intros *; intros Hboom%transition_skip; exfalso; assumption
    end      
  | [ |- Safe ?s ?p ?c ?q ] =>
    apply mk_Safe;
    match goal with
    | [ |- (?c = PSkip ?C) -> _ ] => (discriminate 1) || (intros ->)
    | _ => idtac
    end
  | _ => fail "not a CVF safety"
  end.

(** As [destruct_cvf_safe], but fully introduce, and then invert on any
      resulting transition. *)

Ltac destruct_cvf_safe_and_transition :=
  destruct_cvf_safe;
  intros;
  match goal with
  | [ H : Transition ?c ?p ?c' |- _ ] =>
    inversion H; subst
  | _ => idtac
  end.