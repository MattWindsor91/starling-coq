(** * Views instances

    Here, we implement the core parts of the Views Framework:
    instances, action judgements, and axiom soundness.

    We don't prove soundness of views instances here: instead, we rely
    on a translation from this mini-framework to the original Views
    development in a separate file. *)

From Coq Require Import
     Classes.SetoidTactics
     Classes.SetoidClass
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     Views.Frameworks.Common
     Views.Classes.

Set Implicit Arguments.

Chapter views_framework.

Variable V: Type.
Variables C S: Set.

Section action_judgements.
  Context
    {SO: Setoid V}
    {VS: ViewsSemigroup V}.
  
  Variable
    (sig : Signature C S).
  
  Local Open Scope views_scope.
  
  (** Semigroup axiom judgement. *)

  Definition s_axiom_judgement (a : ViewsAxiom V C) : Prop :=
    vactionj sig a.(cmd) a.(pre) a.(post).

  (** Monoidal axiom judgement. *)
  
  Definition m_axiom_judgement (a : ViewsAxiom V C) :=
    forall v: V, vhoare sig a.(cmd) a.(pre) a.(post).
End action_judgements.

Section instances.
  Context
    {SV: Setoid V}
    {VV: ViewsSemigroup V}.
  
  (** A [PreInstance] is a views instance that may or may not be axiom
      sound. *)

  Record PreInstance : Type :=
    mk_PreInstance
      {
        v_sig    :> Signature C S;
        v_axioms :  Ensemble (ViewsAxiom V C)
      }.

  (** [ViewsAxiomOf] is the dependent type of views axioms in
      pre-instance [i]. *)
  
  Definition ViewsAxiomOf (i: PreInstance): Type :=
    {x: ViewsAxiom V C | In (ViewsAxiom V C) i.(v_axioms) x}.

  (** [axiom_sound] is (semigroup) axiom soundness. *)
  
  Definition axiom_sound (i: PreInstance): Prop :=
    forall a: ViewsAxiomOf i,
      s_axiom_judgement i.(v_sig) (proj1_sig a).

  (** A [ViewsInstance] is a [PreInstance] with a proof of
      axiom soundness. *)
  
  Definition ViewsInstance: Type := {p: PreInstance | axiom_sound p}.

  (** [SViewsAxiomOf] is the dependent type of views axioms in
      sound instance [i]. *)
  
  Definition SViewsAxiomOf (i: ViewsInstance): Type :=
    ViewsAxiomOf (proj1_sig i).

End instances.

End views_framework.
