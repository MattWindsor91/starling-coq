(** * Concurrent Views Framework: Safety inference rules *)

From Coq Require Import
     Arith.PeanoNat
     Bool.BoolEq
     Lists.List
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles
     Relations.Relation_Definitions
     Classes.SetoidClass.

From Starling Require Import
     Views.Classes
     Views.Transformers.Function     
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core     
     Views.Frameworks.CVF.Language.

Set Implicit Arguments.

Section CVF_safety_rules.
  Variables
    (V    : Type)  (* Shared views *)
    (C    : Set)   (* Atomic actions *)
    (SSt  : Set).  (* Shared state *)

  Context
    {SV     : Setoid V}
    {VV     : ViewsSemigroup V}.
  
  Variable
    sig : Signature C SSt.
  
  Lemma cvf_id_transition (p q : V) (c : Prog C) :
    Safe sig p c q ->
    exists p', vactionj sig (Id C) p p' /\ Safe sig p' c q.
  Proof.
    intro Hsafe.
    exists p.
    split.
    - apply vactionj_id_refl.
    - apply Hsafe.
  Qed.
  
  Lemma cvf_safe_skip (p q : V):
    vactionj sig (Id C) p q <->
    Safe sig p (PSkip C) q.
  Proof.
    split.
    - intro Hadj.
      now destruct_cvf_safe.
    - inversion 1 as [p' c' q' Hskip _ Hp Hc Hq].
      now apply Hskip.
  Qed.

  Corollary cvf_safe_skip_strong (p : V) :
    Safe sig p (PSkip C) p.
  Proof.
    apply cvf_safe_skip, vactionj_id_refl.
  Qed.


  Instance cvf_safe_equiv_Proper_impl (c : Prog C) :
    Proper (equiv ==> equiv ==> impl) (fun p q => Safe sig p c q).
  Proof.
    revert c.
    cofix Hco.
    intros c p p' Hp q q' Hq Hsafe_pcq.
    destruct_cvf_safe.
    - apply (vactionj_equiv_Proper sig _ p p' Hp q q' Hq).
      apply cvf_safe_skip, Hsafe_pcq.
    - intros lc c' Htrans.
      inversion Hsafe_pcq; subst.
      destruct (H0 lc c' Htrans) as (r & Haction & Hsafe_rest).
      exists r.
      split.
      + apply (vactionj_equiv_Proper sig _ p p' Hp r r (reflexivity _)), Haction.
      + apply (Hco c' r r (reflexivity _) q q' Hq Hsafe_rest).
  Defined.

  Global Instance cvf_safe_equiv_Proper (c : Prog C) :
    Proper (equiv ==> equiv ==> iff) (fun p q => Safe sig p c q).
  Proof with (apply cvf_safe_equiv_Proper_impl; assumption).
    intros p p' Hp q q' Hq.
    split.
    - simpl...
    - symmetry in Hp, Hq ...
  Defined.  

  Lemma cvf_safe_atomic (p q : V) (atom : C):
    vactionj sig (Cmd atom) p q <->
    Safe sig p (PAtom atom) q.  
  Proof.
    split.
    - (* Action judgement implies safety. *)
      intros Hadj.
      destruct_cvf_safe_and_transition.
      exists q.
      split.
      + apply Hadj.
      + apply cvf_safe_skip_strong.
    - (* Safety implies action judgement. *)
      inversion 1 as [p' c' q' _ Hstep Hp Hc Hq].
      (* Take the atomic step *)
      destruct
        (Hstep (Cmd atom) (PSkip C) (TAtom atom))
        as (r & Hpr & Hsafe_end).
      apply vactionj_cons_q with (hq' := r).
      + apply cvf_safe_skip, Hsafe_end.
      + exact Hpr.
  Qed.
  
  Lemma cvf_safe_cons_p (p p' q : V) (c : Prog C) :
    vactionj sig (Id C) p p' ->
    Safe sig p' c q ->
    Safe sig p c q.  
  Proof.
    intros Hpp' Hsafe.
    destruct Hsafe.
    destruct_cvf_safe.
    - (* Skip case *)
      specialize (H (reflexivity _)).
      apply vactionj_cons_p with (hp' := p0); assumption.
    - (* Step case *)
      intros lc c' Htrans.
      destruct (H0 lc c' Htrans) as (p' & Hrest).
      exists p'.
      split.
      + apply vactionj_cons_p with (hp' := p0).
        * assumption.
        * apply Hrest.
      + apply Hrest.
  Qed.

  Lemma cvf_safe_cons_q (p q' q : V) (c : Prog C) :
    vactionj sig (Id C) q' q ->
    Safe sig p c q' ->
    Safe sig p c q.  
  Proof.
    revert p c.
    cofix Hco.
    intros p c Hpp' Hsafe.
    destruct_cvf_safe.
    - (* Skip case *)
      apply vactionj_cons_q with (hq' := q'); try assumption.
      apply cvf_safe_skip, Hsafe.
    - (* Step case *)
      intros lc c' Htrans.
      inversion Hsafe; subst.
      destruct (H0 lc c' Htrans) as (p' & Hrest).
      exists p'.
      split.
      + apply Hrest.
      + apply Hco.
        * apply Hpp'.
        * apply Hrest.
  Qed.

  Lemma cvf_safe_cons (p p' q' q : V) (c : Prog C) :
    vactionj sig (Id C) p  p' ->
    vactionj sig (Id C) q' q  ->
    Safe sig p' c q' ->
    Safe sig p  c q.
  Proof.
    intros Hp Hq Hcons.
    apply cvf_safe_cons_p with (p' := p').
    - exact Hp.
    - apply cvf_safe_cons_q with (q' := q'); assumption.
  Qed.

  Lemma cvf_safe_seq (p q r : V) (c1 c2 : Prog C):
    Safe sig p c1 q ->
    Safe sig q c2 r ->
    Safe sig p (PSeq c1 c2) r.  
  Proof.
    revert p q r c1 c2.
    cofix Hco.
    intros p q r c1 c2 Hsafe_c1 Hsafe_c2.
    destruct_cvf_safe_and_transition.
    - (* Step *)
      inversion Hsafe_c1; subst.
      destruct (H1 lc p' H4) as (v & Hv).
      exists v.
      split.
      + apply Hv.
      + refine (Hco v q r p' c2 _ Hsafe_c2).
        apply Hv.
    - (* Skip *)
      exists q.
      split.
      + apply cvf_safe_skip, Hsafe_c1.
      + exact Hsafe_c2.
  Qed.

  Lemma cvf_safe_ndt (p q : V) (c1 c2 : Prog C) :
    Safe sig p c1 q ->
    Safe sig p c2 q ->
    Safe sig p (PNdt c1 c2) q.    
  Proof.
    intros Hsafe_1 Hsafe_2.
    destruct_cvf_safe_and_transition;
      (* Splits into two cases, whose solutions are practically identical. *)
      apply cvf_id_transition; assumption.
  Qed.

  (* As per Safe_preloop in the original Views development *)
  
  Lemma cvf_safe_loop_step (p : V) (c1 : Prog C) :
    Safe sig p c1 p ->
    (forall (r : V) (c2 : Prog C),
        Safe sig r c2 p ->
        Safe sig r (PSeq c2 (PIter c1)) p).
  Proof.
    intros Hsafe_c1.
    cofix Hco.
    intros r c2 Hsafe_c2.
    destruct_cvf_safe_and_transition.
    - (* Step *)
      inversion Hsafe_c2; subst.
      destruct (H1 lc p' H4) as (v & Hrest).
      exists v.
      split.
      + apply Hrest.
      + apply Hco, Hrest.
    - (* Skip *)
      exists p.
      apply cvf_safe_skip in Hsafe_c2.
      refine (conj Hsafe_c2 _).
      destruct_cvf_safe_and_transition.
      * exists p.
        refine (conj (vactionj_id_refl _ _) _).
        apply Hco, Hsafe_c1.
      * apply cvf_id_transition, cvf_safe_skip_strong.
  Qed.

  Lemma cvf_safe_loop (p : V) (c1 : Prog C) :
    Safe sig p c1 p ->
    Safe sig p (PIter c1) p.
  Proof.
    intros Hsafe.
    destruct_cvf_safe.
    intros lc c' Htrans.
    inversion Htrans; subst; apply cvf_id_transition.
    - apply cvf_safe_loop_step; apply Hsafe.
    - apply cvf_safe_skip_strong.
  Qed.

  Lemma cvf_safe_frame (p q : V) (f : V) (c : Prog C):
    Safe sig p             c  q ->
    Safe sig (p * f)%views c (q * f)%views.
  Proof.
    revert p q c f.
    cofix Hco.
    intros p q c f Hsafe.
    destruct_cvf_safe.
    - apply vactionj_closed, cvf_safe_skip, Hsafe.
    - intros lc c' Htrans.
      inversion Hsafe; subst.
      destruct (H0 lc c' Htrans) as (r & Hr).
      exists (r * f)%views.
      split.
      + apply vactionj_closed, Hr.
      + apply Hco, Hr.
  Qed.

  Corollary cvf_safe_frame_l (p q f : V) (c : Prog C) :
    Safe sig p             c  q ->
    Safe sig (f * p)%views c (f * q)%views.
  Proof.
    intros Hsafe.
    eapply cvf_safe_equiv_Proper.
    * apply dot_comm.
    * apply dot_comm.
    * apply cvf_safe_frame, Hsafe.
  Qed.

  Lemma cvf_safe_par (p1 p2 q1 q2 : V) (c1 c2 : Prog C) :
    Safe sig p1 c1 q1 ->
    Safe sig p2 c2 q2 ->
    Safe sig (p1 * p2)%views (PPar c1 c2) (q1 * q2)%views.
  Proof.
    revert p1 p2 q1 q2 c1 c2.    
    cofix Hco.
    intros p1 p2 q1 q2 c1 c2 Hsafe_1 Hsafe_2.
    destruct_cvf_safe_and_transition.
    - (* Left step *)
      inversion Hsafe_1; subst.
      destruct (H1 lc p' H4) as (r & Haction & Hsafe_co).
      exists (r * p2)%views.
      split.
      + apply vactionj_closed, Haction.
      + apply Hco; assumption.
    - (* Left skip *)
      apply cvf_safe_skip in Hsafe_1.
      exists (q1 * p2)%views.
      split.
      + apply vactionj_closed, Hsafe_1.
      + apply cvf_safe_frame_l, Hsafe_2.
    - (* Right step *)
      inversion Hsafe_2; subst.
      destruct (H1 lc q' H4) as (r & Haction & Hsafe_co).
      exists (p1 * r)%views.
      split.
      + apply vactionj_closed_l, Haction.
      + apply Hco; assumption.      
    - (* Right skip *)
      apply cvf_safe_skip in Hsafe_2.
      exists (p1 * q2)%views.
      split.
      + apply vactionj_closed_l, Hsafe_2.
      + apply cvf_safe_frame, Hsafe_1.
  Qed.
End CVF_safety_rules.