(** * Local Views Framework: Local Views instances *)

From Coq Require Import
     Program.Basics
     Relations.Relation_Definitions
     Sets.Constructive_sets     
     Sets.Ensembles     
     Classes.SetoidClass.

From Starling Require Import
     Views.Classes
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core
     Views.Frameworks.LVF.ActionJudgements     
     Views.Frameworks.LVF.Signatures.

Set Implicit Arguments.

Section LVF_instances.
  Variables
    (V    : Type)  (* Shared views *)
    (LSig : Type)  (* Local signature *)
    (C    : Set)   (* Atomic actions *)
    (LSt  : Set)   (* Local state *)
    (SSt  : Set).  (* Shared state *)

  Context
    {SV     : Setoid V}
    {VV     : ViewsSemigroup V}.
  

  Definition LVFAxiomatisation : Type := Ensemble (ViewsAxiom (LSt -> V) C).

  Record LVFInstance :=
    mk_LVFInstance
      {
        lvfi_sig    :> LocalSignature C LSt SSt;
        lvfi_axioms :  LVFAxiomatisation;
      }.

  Definition local_axiom_sound (i : LVFInstance) : Prop :=
    forall (x : ViewsAxiom (LSt -> V) C),
      In _ i.(lvfi_axioms) x ->
      slactionj i.(lvfi_sig) x.(cmd) x.(pre) x.(post).

  Definition axioms_lvf_to_cvf (xs : LVFAxiomatisation)
    : Ensemble (ViewsAxiom V (LabelC C * LSt * LSt)) :=
    fun '(mk_ViewsAxiom p lc q) =>
      label_maybe False
                  (fun '(c, l, l') =>
                     exists (pp qq : LSt -> V),
                       pp l = p /\
                       qq l' = q /\
                       In _ xs (mk_ViewsAxiom pp c qq)
                  )
                  lc.
  
  Definition instance_lvf_to_cvf (i : LVFInstance)
    : PreInstance (LabelC C * LSt * LSt) SSt :=
    {|
      v_sig := lsig_erase (i.(lvfi_sig));
      v_axioms := axioms_lvf_to_cvf i.(lvfi_axioms)
    |}.

  Lemma instance_lvf_to_cvf_asound (i : LVFInstance) :
    local_axiom_sound i <->
    axiom_sound (instance_lvf_to_cvf i).
  Proof.
    split.
    - intros Hlas ([p c q] & Hx).
      destruct c as [|[[c l] l']].
      + contradiction.
      + destruct Hx as (fp & fq & <- & <- & Hin_ax).
        simpl in *.
        apply Hlas in Hin_ax.
        apply slactionj_lactionj, Hin_ax.
    - intros Has [p c q] Hin_ax.
      apply slactionj_lactionj.
      intros l l'.
      simpl in *.
      remember (mk_ViewsAxiom (p l) (Cmd (c, l, l')) (q l')) as a.
      assert (In _ (instance_lvf_to_cvf i).(v_axioms) a) as Ha.
      {
        rewrite Heqa.
        exists p, q.
        repeat split; try reflexivity.
        apply Hin_ax.
      }
      specialize (Has (exist _ a Ha)).
      rewrite Heqa in Has.
      apply Has.
  Qed.

  Definition axioms_cvf_to_lvf (xs : Ensemble (ViewsAxiom V (LabelC C * LSt * LSt)))
    : LVFAxiomatisation :=
    fun '(mk_ViewsAxiom p lc q) =>
      forall l l' : LSt,
      exists (pp qq : V),
        pp = p l /\
        qq = q l' /\
        In _ xs (mk_ViewsAxiom pp (Cmd (lc, l, l')) qq).
  
  Definition SoundLVFInstance : Type :=
    { x : LVFInstance | local_axiom_sound x }.
End LVF_instances.