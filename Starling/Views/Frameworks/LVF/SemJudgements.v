(** * Local Views Framework: semantic judgements *)

From Coq Require Import
     Arith.PeanoNat
     Bool.BoolEq
     Lists.List
     Program
     Sets.Constructive_sets
     Sets.Ensembles
     Relations.Relation_Definitions
     Classes.SetoidClass.

From Starling Require Import
     Utils.List.Facts
     Utils.List.Override
     Views.Classes
     Views.Transformers.Function
     Views.Frameworks.Common
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.Language
     Views.Frameworks.LVF.Signatures
     Views.Frameworks.LVF.ThreadLists
.

Set Implicit Arguments.

Open Scope program_scope.

Chapter LVF_semantic_judgements.

Variables
  (V    : Type)  (* Shared views *)
  (C    : Set)   (* Atomic actions *)
  (LSt  : Set)   (* Local state *)
  (SSt  : Set).  (* Shared state *)

Context
  {SV : Setoid V}
  {VV : ViewsSemigroup V}.

Variable
  (lsig : LocalSignature C LSt SSt).

Section single_thread.

  (** The explicit single-thread semantic judgement. *)

  CoInductive SSafeEx: LSt -> (LSt -> V) -> LProg C -> (LSt -> V) -> Prop :=
  | mk_SSafeEx l p c q:
      (`c = PSkip C -> forall l', lactionj lsig (Id C) l l' p q) ->
      (forall lc (c' : LProg C),
          Transition (`c) lc (`c') ->
          (exists (p' : LSt -> V),
              (forall l', lactionj lsig lc l l' p p' /\
                     (label_has_sem lsig l l' lc -> SSafeEx l' p' c' q)))) ->
      SSafeEx l p c q.

  Section SSafeEx_coind.

    (** ** Co-induction scheme for [SSafeEx]

        Based on http://adam.chlipala.net/cpdt/html/Cpdt.Coinductive.html. *)

    Variable R : LSt -> (LSt -> V) -> LProg C -> (LSt -> V) -> Prop.

    Hypothesis Hbase : forall l p q c,
        R l p c q -> `c = PSkip C -> forall l', lactionj lsig (Id C) l l' p q.

    Hypothesis Hind : forall l p c lc c' q,
        R l p c q ->
        Transition (`c) lc (`c') ->
        exists (p' : LSt -> V),
          (forall l', lactionj lsig lc l l' p p' /\
                 (label_has_sem lsig l l' lc -> R l' p' c' q)).

    Lemma SSafeEx_coind (l : LSt) (p q : LSt -> V) (c : LProg C) :
      R l p c q ->
      SSafeEx l p c q.
    Proof.
      revert p l c.
      cofix Hco.
      intros p l (c & Hc) HR.
      apply mk_SSafeEx.
      - (* Skip *)
        now apply Hbase.
      - (* Step *)
        intros lc c' Htrans.
        destruct (Hind _ HR Htrans) as (p' & Hrest).
        exists p'.
        intros l'.
        split.
        + apply Hrest.
        + intros Hhas_sem.
          apply Hco, Hrest, Hhas_sem.
    Qed.
  End SSafeEx_coind.

  (** The proof of [NoPar] used in an [LProg] doesn't matter to [SSafeEx]. *)

  Lemma SSafeEx_prf_irr (p q : LSt -> V) (c1 c2 : LProg C) (l : LSt) :
    `c1 = `c2 -> SSafeEx l p c1 q -> SSafeEx l p c2 q.
  Proof.
    intros Heq Hsafe.
    generalize (conj Heq Hsafe); clear Heq Hsafe.
    intro Hc.
    pose proof (ex_intro (fun c' => `c' = `c2 /\ SSafeEx l p c' q) c1 Hc) as Hcg.
    clear Hc c1.
    revert l p q c2 Hcg.
    apply SSafeEx_coind.
    - intros l p q (c & Hc) ((cw & Hcw) & <- & Hsafe).
      cbn.
      intros ->.
      inversion Hsafe.
      now apply H.
    - intros l p (c & Hc) lc c' q (cw & <- & Hsafe) Htrans.
      cbn in *.
      inversion Hsafe; subst.
      destruct (H0 lc c' Htrans) as (p' & Hstep).
      exists p'.
      intros l'.
      destruct (Hstep l') as (Haction & Hco).
      intuition.
      now exists c'.
  Qed.

  (** If the transition on a [SSafeEx] step is an id-transition, then
      we can prove the step recursion by assuming the view doesn't change
      and just proving [SSafeEx] for the remainder. *)

  Lemma ltsafe_id_transition (p q : LSt -> V) (c : LProg C) (l : LSt) :
    SSafeEx l p c q ->
    exists (p' : LSt -> V),
      (forall l', lactionj lsig (Id C) l l' p p' /\
             (label_has_sem lsig l l' (Id C) -> SSafeEx l' p' c q)).
  Proof.
    intros Hsafe.
    exists p.
    intros l'.
    split.
    + apply lactionj_id_refl.
    + intros <-.
      exact Hsafe.
  Qed.

  (** The implicit single-thread semantic judgement. *)

  Definition SSafe (p : LSt -> V) (c : LProg C) (q : LSt -> V) : Prop :=
    forall (l : LSt), SSafeEx l p c q.

  (** The proof of [NoPar] used in an [LProg] doesn't matter to [SSafe]. *)

  Corollary SSafe_prf_irr (p q : LSt -> V) (c1 c2 : LProg C) :
    `c1 = `c2 -> SSafe p c1 q -> SSafe p c2 q.
  Proof.
    intros Heq Hsafe l'.
    now apply SSafeEx_prf_irr with (c1 := c1).
  Qed.

End single_thread.

Section multi_thread.
  (** ** Multithread Local Views programs *)

  Context
    {MV : ViewsMonoid V}.

  Variable
    (df   : LSt).

  CoInductive MSafeEx: list LSt -> list (LSt -> V) -> list (LSt -> V) -> list (LProg C) -> Prop :=
  | mk_MSafeEx ls ps qs cs:
      length ps = length ls ->
      length ps = length qs ->
      length ps = length cs ->
      ((forall (t : nat), (` (csel cs t)) = PSkip C) ->
       (forall (t : nat) (l' : LSt),
           lactionj lsig (Id C) (lsel df ls t) l' (vsel ps t) (vsel qs t))) ->
      (forall (t : nat) lc (c' : LProg C),
          Transition (` (csel cs t)) lc (`c') ->
          (exists (p' : (LSt -> V)),
              (forall (l' : LSt),
                  lactionj lsig lc (lsel df ls t) l' (vsel ps t) p' /\
                  (label_has_sem lsig (lsel df ls t) l' lc ->
                   MSafeEx (list_override ls t l')
                           (list_override ps t p')
                           qs
                           (list_override cs t c'))))) ->

      MSafeEx ls ps qs cs.

  Definition MSafe (ps qs : list (LSt -> V)) (cs : list (LProg C)) : Prop :=
    forall (ls : list LSt), length ps = length ls -> MSafeEx ls ps qs cs.

  (** If we have local safety on all threads, we have global safety. *)

  Theorem all_SSafeEx_MSafeEx:
    forall (ls : list LSt) (ps qs : list (LSt -> V)) (cs : list (LProg C)),
      length ps = length ls ->
      length ps = length qs ->
      length ps = length cs ->
      (forall t, SSafeEx (lsel df ls t) (vsel ps t) (csel cs t) (vsel qs t)) ->
      MSafeEx ls ps qs cs.
  Proof.
    cofix all_LSafe_MSafeEx.
    intros ls ps qs cs Hlen_pl Hlen_pq Hlen_pc Hall_safe.
    (* Split [MSafe] into its two nontrivial obligations. *)
    constructor; try assumption.
    - (* Base case: when each thread is [Skip]. *)
      intros Hall_skip t.
      intros l'.
      specialize (Hall_safe t).
      inversion Hall_safe.
      apply H, Hall_skip.
    - (* Co-inductive case *)
      intros t lc c' Htrans.
      (* Is 't' a valid thread? *)
      destruct (Nat.le_gt_cases (length cs) t) as [Htc|Htc].
      + (* No, in which case the transition is invalid. *)
        unfold csel in Htrans.
        rewrite (nth_overflow cs _ Htc) in Htrans.
        now apply transition_skip in Htrans.
      + (* Yes. *)
        pose (Hall_safe t) as Ht_safe.
        inversion Ht_safe as [l p c q _ Hstep Hl Hp Hc Hq].
        destruct (Hstep lc c' Htrans) as (p' & Hstep_judgement).
        exists p'.
        intros l'.
        split.
        * apply Hstep_judgement.
        * intros Hhas_sem.
          (* Actually apply the co-induction now. *)
          apply all_LSafe_MSafeEx; try (rewrite ! list_override_length; assumption).
          intros u.
          (* Are we checking safety of the thread we just modified? *)
          destruct (Nat.eq_dec t u) as [<-|Hnu].
          -- (* Yes. *)
            unfold lsel, vsel, csel.
            assert (t < length ps) as Htp by now rewrite Hlen_pc.
            assert (t < length qs) as Htq by now (rewrite <- Hlen_pq, Hlen_pc).
            assert (t < length ls) as Htl by now rewrite <- Hlen_pl.
            rewrite ! list_override_nth; try assumption.
            apply Hstep_judgement, Hhas_sem.
          -- (* No. *)
            unfold lsel, vsel, csel.
            rewrite ! list_override_nth_inv; try assumption.
            apply Hall_safe.
  Qed.

  Corollary all_SSafe_MSafe:
    forall (ps qs : list (LSt -> V)) (cs : list (LProg C)),
      length ps = length qs ->
      length ps = length cs ->
      (forall t, SSafe (vsel ps t) (csel cs t) (vsel qs t)) ->
      MSafe ps qs cs.
  Proof.
    intros ps qs cs Hlen_pq Hlen_pc HSSafe ls Hlen_pl.
    apply all_SSafeEx_MSafeEx; try assumption.
    intro t.
    apply HSSafe.
  Qed.

  Corollary all_SSafe_MSafe_bounded :
    forall (ps qs : list (LSt -> V)) (cs : list (LProg C)),
      length ps = length qs ->
      length ps = length cs ->
      (forall t, t < length ps -> SSafe (vsel ps t) (csel cs t) (vsel qs t)) ->
      MSafe ps qs cs.
  Proof.
    intros ps qs cs Hlen_qs Hlen_cs Hbnd.
    refine (all_SSafe_MSafe ps qs cs Hlen_qs Hlen_cs _).
    intro t.
    destruct (Nat.le_gt_cases (length ps) t) as [Hover|Hin].
    - (* [t] is out of bounds *)
      unfold vsel, csel.
      rewrite ! nth_overflow.
      + constructor.
        * intros _ l'.
          apply lactionj_id_refl.
        * intros lc c' Htrans%transition_skip.
          contradiction.
      + now rewrite <- Hlen_qs.
      + now rewrite <- Hlen_cs.
      + exact Hover.
    - (* [t] is in bounds *)
      apply Hbnd, Hin.
  Qed.
End multi_thread.

End LVF_semantic_judgements.

(** [destruct_safe] implements the following decision procedure on
    explicit local safety goals:

    - Destruct the safety property;
    - If the command is [PSkip], solve the second leg of the property by
      the contradiction that no transition can be made from [PSkip], and
      present the skip action judgement to the user;
    - Otherwise, try to solve the first leg by the contradiction that the
      command is not [PSkip]. *)

Ltac destruct_safe :=
  match goal with
  | [ |- SSafeEx ?s ?l ?p (PSkip ?C) ?q ] =>
    apply mk_SSafeEx;
    match goal with
    | [ |- (PSkip ?C = PSkip ?C) -> _ ] => intros _
    | _ => intros *; intros Hboom%transition_skip; exfalso; assumption
    end
  | [ |- SSafeEx ?s ?l ?p ?c ?q ] =>
    apply mk_SSafeEx;
    match goal with
    | [ |- (?c = PSkip ?C) -> _ ] => (discriminate 1) || (intros ->)
    | _ => idtac
    end
  | _ => fail "not an explicit local safety"
  end.

(** As [destruct_safe], but fully introduce, and then invert on any
      resulting transition. *)

Ltac destruct_safe_and_transition :=
  destruct_safe;
  intros;
  match goal with
  | [ H : Transition ?c ?p ?c' |- _ ] =>
    inversion H; subst
  | _ => idtac
  end.
