(** * Local Views Framework: soundness proofs *)

From Coq Require Import
     Arith.PeanoNat
     Bool.BoolEq
     Lists.List
     Program
     Sets.Constructive_sets
     Sets.Ensembles
     Relations.Relation_Definitions
     Classes.SetoidClass
.

From Starling Require Import
     Utils.List.Facts
     Utils.List.Override
     Views.Classes
     Views.Transformers.Function
     Views.Frameworks.Common
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.Language
     Views.Frameworks.LVF.SemJudgements
     Views.Frameworks.LVF.Signatures
     Views.Frameworks.LVF.ThreadLists
.

Set Implicit Arguments.

Open Scope program_scope.

Chapter LVF_soundness.

Variables
  (V    : Type)  (* Shared views *)
  (C    : Set)   (* Atomic actions *)
  (LSt  : Set)   (* Local state *)
  (SSt  : Set).  (* Shared state *)

Context
  {SV : Setoid V}
  {VV : ViewsSemigroup V}
  {MV : ViewsMonoid V}.

Variables
  (lsig : LocalSignature C LSt SSt)
  (df   : LSt).


(* These definitions were suggested by Matt Parkinson *)

Inductive TTransition: (list (LProg C)) -> nat -> LabelC C -> (list (LProg C)) -> Prop :=
| TT (t: nat) (cs: list (LProg C)) (c' : LProg C) (lc: LabelC C):
    Transition (` (csel cs t)) lc (`c') ->
    TTransition cs t lc (list_override cs t c').

Lemma lnstar_update (ps qs: list (LSt -> V)) (ls: list LSt) (s: SSt):
  length ps = length qs ->
  length ps = length ls ->
  (forall t : nat, lactionj lsig (Id C) (lsel df ls t) (lsel df ls t) (vsel ps t) (vsel qs t)) ->
  slactionj lsig (Id C) (const (lnstar ps ls)) (const (lnstar qs ls)).
Proof.
  revert ps qs ls.
  induction ps, qs, ls; try discriminate.
  - (* All nil *)
    intros _ _ _.
    apply slactionj_id_refl.
  - intros Hlen_pq' Hlen_pl' Hsjudge.
    assert (lactionj lsig (Id C) l l a v) as Hsjudge_hd by apply (Hsjudge 0).
    assert (forall t: nat, lactionj lsig (Id C) (lsel df ls t) (lsel df ls t) (vsel ps t) (vsel qs t)) as Hsjudge_rest.
    {
      intro t.
      apply (Hsjudge (S t)).
    }
    injection Hlen_pq'; intro Hlen_pq.
    injection Hlen_pl'; intro Hlen_pl.
    specialize (IHps qs ls Hlen_pq Hlen_pl Hsjudge_rest).
    (* Start rewriting the views so we can frame. *)
    eapply slactionj_equiv_Proper.
    + symmetry.
      apply fview_dot_const_dist.
    + symmetry.
      apply fview_dot_const_dist.
    + (* Now we can use the pair rule. *)
      apply slactionj_lactionj.
      intros la lb.
      apply lactionj_pair.
      * apply lactionj_const, Hsjudge_hd.
      * apply slactionj_lactionj, IHps.
Qed.

(** [LTState] is a machine state for a multithread program. *)

Record LTState :=
  mk_LTState
    {
      lt_shared : SSt;
      lt_local : list LSt;
    }.

Definition LTStateN (n: nat) := { s : LTState | length (s.(lt_local)) = n }.

Definition ltsem (t : nat) : Semantics C LTState :=
  fun c s s' =>
    exists l',
      (lsig.(lsig_lsem) c
                        (lsel df s.(lt_local)  t, s .(lt_shared))
                        (lsel df s'.(lt_local) t, s'.(lt_shared))) /\
      s'.(lt_local) = (list_override s.(lt_local) t l').

Lemma ltsem_label_override (n t : nat) (lc : LabelC C) (s s' : LTStateN n) :
  In LTState (label_sem (ltsem t) lc (proj1_sig s)) (proj1_sig s') ->
  exists l', (proj1_sig s').(lt_local) = list_override (proj1_sig s).(lt_local) t l'.
Proof.
  (* Case split as to whether [lc] is [id] or not. *)
  intros Htsem.
  unfold label_sem in *.
  destruct lc; simpl in *.
  - (* [id]: we override with the original item. *)
    apply Singleton_inv in Htsem.
    rewrite <- Htsem.
    exists (lsel df (proj1_sig s).(lt_local) t).
    apply list_override_idem.
  - (* Command: do the override as normal. *)
    destruct Htsem as (l' & _ & Hl').
    exists l'.
    apply Hl'.
Qed.

Lemma ltsem_lsem_label (n t : nat) (lc : LabelC C) (s s': LTStateN n):
  In LTState (label_sem (ltsem t) lc (proj1_sig s)) (proj1_sig s') ->
  In (LSt * SSt)
     (label_sem lsig.(lsig_lsem) lc (lsel df (proj1_sig s).(lt_local) t, (proj1_sig s).(lt_shared)))
     (lsel df (proj1_sig s').(lt_local) t, (proj1_sig s').(lt_shared)).
Proof.
  (* Case split as to whether [lc] is [id] or not. *)
  intros Htsem.
  unfold label_sem in *.
  destruct lc; simpl in *.
  - apply Singleton_intro.
    now rewrite <- (Singleton_inv _ _ _ Htsem).
  - destruct Htsem as (l' & Hlsem & Hl').
    unfold lsel in *.
    destruct (Nat.lt_ge_cases t n) as [Htn|Htn].
    + rewrite Hl', list_override_nth in *; try (rewrite (proj2_sig s); apply Htn).
      apply Hlsem.
    + rewrite Hl', ! nth_overflow in *;
        try (rewrite (proj2_sig s); apply Htn);
        try (rewrite list_override_length, (proj2_sig s); apply Htn).
      apply Hlsem.
Qed.

Inductive LTMTransition (n: nat) :
  (list (LProg C)) -> LTStateN n -> (list (LProg C)) -> LTStateN n -> Prop :=
| LMT_empty cs s: LTMTransition cs s cs s
| LMT_step (df: LSt) (t: nat) (cs1 cs2 cs3 : list (LProg C)) (s1 s2 s3 : LTStateN n) (lc: LabelC C):
    length cs1 = length cs2 /\ length cs1 = n ->
    t < n ->
    TTransition cs1 t lc cs2 ->
    label_sem (ltsem t) lc (proj1_sig s1) (proj1_sig s2) ->
    LTMTransition cs2 s2 cs3 s3 ->
    LTMTransition cs1 s1 cs3 s3.

(** Soundness (partial correctness) for a multithread program. *)

Definition ltsound
           (ps qs : list (LSt -> V))
           (cs : list (LProg C))
           (Hlen : length ps = length qs /\ length ps = length cs)
           (s s': LTStateN (length qs)) :=
  In _
     (proj1_sig lsig.(lsig_reifier) (lnstar ps (proj1_sig s).(lt_local)))
     (proj1_sig s).(lt_shared) ->
  (exists cs',
      (forall t, (` (csel cs' t)) = PSkip C) /\
      LTMTransition cs s cs' s') ->
  In _
     (proj1_sig lsig.(lsig_reifier) (lnstar qs (proj1_sig s').(lt_local)))
     (proj1_sig s').(lt_shared).

(** If a program is multithread safe starting from its initial local state,
      it is multithread sound. *)

Theorem msafe_ex_ltsound
        (ps qs : list (LSt -> V))
        (cs : list (LProg C))
        (Hlen: length ps = length qs /\ length ps = length cs)
        (s s' : LTStateN (length qs)):
  MSafeEx lsig df ((proj1_sig s).(lt_local)) ps qs cs ->
  ltsound ps qs cs Hlen s s'.
Proof.
  intros Hsafe Hin_p (cs' & Hcs & Htrans).
  simpl in *.
  remember (csel cs') as fsc'.
  generalize dependent ps.
  induction Htrans; intros ps (Hlen_ps_qs & Hlen_pp) Hsafe Hin_p.
  - (* Skip case *)
    destruct s as (s & Hs).
    assert (length ps = length s.(lt_local)) as Hlen_pl.
    {
      rewrite Hlen_ps_qs.
      symmetry.
      exact Hs.
    }
    inversion Hsafe as [lss pss qss css Hlen_pl' Hlen_ps_qs' Hlen_ps_p Hskip _ Hlss Hpss Hqss Hps].
    rewrite Heqfsc' in Hcs.
    specialize (Hskip Hcs).
    assert (forall (t : nat),
               lactionj lsig (Id C) (lsel df s.(lt_local) t)
                        (lsel df s.(lt_local) t)
                        (vsel ps t) (vsel qs t)) as Hskip' by (intro t; apply Hskip).
    destruct (lnstar_update ps qs s.(lt_local) s.(lt_shared) Hlen_ps_qs Hlen_pl Hskip')
      as (Hupd_pc & Hupd_ni).
    refine (Hupd_pc s.(lt_shared) s.(lt_shared) df df Hin_p (In_singleton _ _)).
  - (* Inductive case: at least one transition.

           This time, we use the step case of [MSafeEx]: we'll run it
           forwards to obtain the intermediate view, then do our inductive
           step *)
    assert (length ps = length (proj1_sig s1).(lt_local)) as Hlen_pl.
    {
      rewrite Hlen_ps_qs.
      symmetry.
      exact (proj2_sig s1).
    }
    inversion Hsafe as [lss pss qss css Hlen_pl' Hlen_ps_qs' Hlen_ps_p1 _ Hstep Hlss Hpss Hqss Hcss].
    destruct H as (Hlen_p1_p2 & Hlen_p1_qs).
    inversion H1 as [tt cst c' lct Htrans_step Hcst Htt Hlct Hcs_over].
    destruct (Hstep t lc c' Htrans_step) as (r & Hrest).
    destruct (Hrest (lsel df (proj1_sig s2).(lt_local) t)) as (Hactionj & Hsafe_ind).
    destruct (ltsem_label_override t lc s1 s2 H2) as (l' & Hl').
    refine (IHHtrans Heqfsc' (list_override ps t r) _ _ _).
    + (* Are the lengths compatible? *)
      subst cs2.
      rewrite ! list_override_length.
      split; assumption.
    + (* Is the rest of the program safe? *)
      rewrite <- Hcs_over.
      rewrite Hl'.
      apply Hrest.
      (* Now, we must show that the transition to [l'] is valid per the
           semantics of [lc]. *)
      destruct lc.
      * apply Singleton_inv in H2.
        rewrite H2 in *.
        rewrite Hl'.
        unfold lsel.
        rewrite list_override_nth.
        -- reflexivity.
        -- now rewrite (proj2_sig s2).
      * apply ltsem_lsem_label in H2.
        exists (proj1_sig s1).(lt_shared), (proj1_sig s2).(lt_shared).
        rewrite Hl' in H2.
        unfold lsel in H2.
        rewrite list_override_nth in H2.
        -- apply H2.
        -- now rewrite (proj2_sig s1).
    + (* Does the intermediate state satisfy the intermediate view? *)
      assert (length ps = length (proj1_sig s1).(lt_local)) as Hlen_local_ps.
      {
        transitivity (length qs).
        - exact Hlen_ps_qs.
        - apply symmetry, (proj2_sig s1).
      }
      assert (t < length ps) as Hlen_t_ps by (rewrite Hlen_ps_qs; exact H0).
      assert (length ps = length (list_override (lt_local (proj1_sig s1)) t l')) as Hlen_local_rs_1.
      {
        rewrite ! list_override_length, (proj2_sig s1).
        exact Hlen_ps_qs.
      }

      assert (length (list_override ps t r) = length (proj1_sig s2).(lt_local)) as Hlen_local_rs_2.
      {
        rewrite list_override_length, (proj2_sig s2).
        exact Hlen_ps_qs.
      }
      assert (t < length (list_override ps t r)) as Hlen_t_rs.
      {
        rewrite list_override_length.
        exact Hlen_t_ps.
      }
      assert (t < length (proj1_sig s1).(lt_local)) as Hlen_t_ls1
          by now rewrite (proj2_sig s1).
      assert (t < length (proj1_sig s2).(lt_local)) as Hlen_t_ls2
          by now rewrite (proj2_sig s2).
      destruct (lnstar_nth df ps (proj1_sig s1).(lt_local) Hlen_local_ps Hlen_t_ps)
        as (ps1 & ps2 & ls1 & ls2 & Hps & Hlps & Hls & Hlls & Hlstar_ps).
      destruct (lnstar_nth df (list_override ps t r) (proj1_sig s2).(lt_local) Hlen_local_rs_2 Hlen_t_rs)
        as (rs1 & rs2 & ls1' & ls2' & Hrs & Hlrs & Hls' & Hlls' & Hlstar_rs).
      (* Push through the list override in the exploded [lnstar]. *)
      unfold vsel, lsel in *.
      rewrite Hl' in *.
      rewrite ! list_override_nth in Hlstar_rs; try assumption.
      rewrite list_override_nth in Hrs; try assumption.
      unfold lsel in Hactionj.
      rewrite list_override_nth in Hactionj; try assumption.
      unfold lsel in Hls'.
      rewrite list_override_nth in Hls'; try assumption.
      (* Now marry up the various list chunks.
           First, ps to rs... *)
      pose (list_override_unsplit (const 1)%views Hrs Hlrs) as Hps_rs.
      rewrite Hps in Hps_rs.
      rewrite <- Hlrs in Hlps.
      destruct (app_split_same _ _ _ _ Hps_rs Hlps) as (<- & Hvrs).
      injection Hvrs.
      intros <- _.
      (* Then, ls to ls'... *)
      pose (list_override_unsplit l' Hls' Hlls') as Hls_ls'.
      rewrite Hls in Hls_ls'.
      rewrite <- Hlls' in Hlls.
      destruct (app_split_same _ _ _ _ Hls_ls' Hlls) as (<- & Hvls).
      injection Hvls.
      intros <- _.
      (* Do the view preservation rewrites, stepwise, to frame the views outside thread t. *)
      eapply (proj2_sig lsig.(lsig_reifier)).
      {
        apply Hlstar_rs.
      }
      eapply (proj2_sig lsig.(lsig_reifier)).
      {
        apply equiv_dot_right.
        apply lnstar_app.
        + rewrite Hlls'.
          exact Hlrs.
        + epose (app_split_length _ _ _ _ _ _ Hrs Hls').
          rewrite <- Hlls' in Hlrs.
          now injection (app_split_length _ _ _ _ _ _ Hrs Hls' Hlen_local_rs_2 Hlrs).
      }
      destruct Hactionj as (Hactionj_pc & Hactionj_ni).
      refine (Hactionj_ni _

                          ((proj1_sig s2).(lt_shared))
                          _

             ).
      exists (proj1_sig s1).(lt_shared).
      split.
      * (* Re-arrange back to the precondition. *)
        eapply (proj2_sig lsig.(lsig_reifier)).
        {
          apply equiv_dot_right.
          symmetry.
          apply lnstar_app.
          - rewrite Hlls'.
            exact Hlrs.
          - rewrite <- Hlls' in Hlrs.
            now injection (app_split_length _ _ _ _ _ _ Hps Hls' Hlen_local_rs_1 Hlrs).
        }
        eapply (proj2_sig lsig.(lsig_reifier)).
        {
          symmetry.
          apply Hlstar_ps.
        }
        apply Hin_p.
      * (* Now show that the semantics relates the two states. *)
        apply ltsem_lsem_label in H2.
        rewrite Hl' in H2.
        unfold lsel in H2.
        rewrite ! list_override_nth in H2; assumption.
Qed.

Corollary msafe_ltsound
          (ps qs : list (LSt -> V))
          (cs : list (LProg C))
          (Hlen: length ps = length qs /\ length ps = length cs)
          (s s' : LTStateN (length qs)):
  MSafe lsig df ps qs cs ->
  ltsound ps qs cs Hlen s s'.
Proof.
  intro Hms.
  apply msafe_ex_ltsound, Hms.
  rewrite (proj1 Hlen).
  symmetry.
  apply (proj2_sig s).
Qed.

End LVF_soundness.