(** * Local Views Framework: Local signatures *)

From Coq Require Import
     Program.Basics
     Relations.Relation_Definitions
     Sets.Constructive_sets     
     Sets.Ensembles     
     Classes.SetoidClass.

From Starling Require Import
     Utils.Ensemble.Facts
     Views.Classes
     Views.Frameworks.Common.

Set Implicit Arguments.

Section LVF_signatures.
  Variables
    (V   : Type)  (* Views *)
    (C   : Set)   (* Atomic actions *)
    (LSt : Set)   (* Local state *)
    (SSt : Set).  (* Shared state *)

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}.

  Record LocalSignature :=
    mk_LocalSignature
      {
        lsig_reifier : Reifier SSt (V := V);
        lsig_lsem    : C -> relation (LSt * SSt)
      }.

  Definition lsem_erase (lsem : C -> relation (LSt * SSt)) : Semantics (LabelC C * LSt * LSt) SSt :=
    fun '(c, l, l') s s' => label_sem lsem c (l, s) (l', s').
  
  Definition lsig_erase (lsig : LocalSignature) :
    Signature (LabelC C * LSt * LSt) SSt :=
    {|
      sig_reifier := lsig.(lsig_reifier);
      sig_sem     := lsem_erase lsig.(lsig_lsem)
    |}.

  Lemma local_id_semantics (lsig : LocalSignature) (l l' : LSt) (s s' : SSt):
    label_sem (lsig_erase lsig).(sig_sem) (Cmd (Id C, l, l')) s s' <-> l = l' /\ s = s'.
  Proof.
    unfold label_sem, label_maybe.
    split.
    - intros Hsig%Singleton_inv.
      now injection Hsig.
    - now intros (-> & ->).
  Qed.

  Lemma local_lift_id_semantics (lsig : LocalSignature) (l l' : LSt) (ss: Ensemble SSt):
    Same_set SSt
             (set_lift (label_sem (lsig_erase lsig).(sig_sem) (Cmd (Id C, l, l'))) ss)
             (fun s => ss s /\ l = l').
  Proof.
    split.
    - apply set_lift_gen_dist.
      intros s Hin_s s' (-> & ->)%local_id_semantics.
      + repeat split.
        exact Hin_s.
      + exact lsig.
    - intros s (Hin_ss & ->).
      exists s.
      now split.
  Qed.

  Definition has_sem (lsig : LocalSignature) (l l' : LSt) (c : C) : Prop :=
    exists s s' : SSt, lsig.(lsig_lsem) c (l, s) (l', s').
  
  Definition label_has_sem (lsig : LocalSignature) (l l' : LSt) (lc : LabelC C) : Prop :=
    label_maybe (l = l') (has_sem lsig l l') lc.
End LVF_signatures.