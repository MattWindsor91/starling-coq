(** * Local Views Framework: action judgements *)

From Coq Require Import
     Program.Basics
     Relations.Relation_Definitions
     Sets.Constructive_sets
     Sets.Ensembles
     Classes.SetoidClass.

From Starling Require Import
     Views.Classes
     Views.Frameworks.Common
     Views.Frameworks.LVF.Signatures
     Views.Transformers.Function.

Set Implicit Arguments.

Section local_action_judgements.
  Variables
    (V    : Type)  (* Views *)
    (C    : Set)   (* Atomic actions *)
    (LSt  : Set)   (* Local state *)
    (SSt  : Set).  (* Shared state *)

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}.

  Variable
    (lsig : LocalSignature C LSt SSt).

  Let reif := lsig.(lsig_reifier).
  Let lsem := lsig.(lsig_lsem).

  (** This just pulls out the existing [Proper] instance in [reif]. *)

  Instance reif_Proper: Proper (equiv ==> Same_set _) (proj1_sig reif)
    := proj2_sig reif.

  Instance reif_Proper_incl: Proper (equiv ==> Included _) (proj1_sig reif).
  Proof.
    intros x y Hxy%symmetry.
    now apply reif_Proper.
  Defined.

  (** [lactionj] is shorthand for the CVF embedding of the LVF action
      judgement. *)

  Definition lactionj (c : LabelC C) (l l' : LSt) (p q : LSt -> V) : Prop :=
    vactionj (lsig_erase lsig) (Cmd (c, l, l')) (p l) (q l').

  Global Instance lactionj_equiv_Proper (c : LabelC C) (l l' : LSt) :
    Proper (equiv ==> equiv ==> iff) (lactionj c l l').
  Proof.
    intros w x Hwx y z Hyz.
    now apply vactionj_equiv_Proper.
  Qed.

  (** Semantic entailment is reflexive. *)

  Corollary lactionj_id_refl (l l' : LSt) (p : LSt -> V) :
    lactionj (Id C) l l' p p.
  Proof.
    unfold lactionj.
    simpl.
    split.
    - intros s (Hin_s & ->)%local_lift_id_semantics.
      exact Hin_s.
    - intros r s (Hin_s & ->)%local_lift_id_semantics.
      exact Hin_s.
  Qed.

  (** We can unbind, or re-bind, the local pre-state in the precondition
      to a [lactionj]. *)

  Lemma lactionj_const_p (l l' : LSt) (p q : LSt -> V) (c : LabelC C) :
    lactionj c l l' p q <-> lactionj c l l' (const (p l)) q.
  Proof.
    split.
    - (* Unbind *)
      intro Hbound.
      apply Hbound.
    - (* Bind *)
      intro Hunbound.
      apply Hunbound.
  Qed.

  (** In a local entailment, we can unbind the local state in both views. *)

  Lemma lactionj_const (p q : LSt -> V) (l1 l2 l2' : LSt):
    lactionj (Id C) l1 l1 p q ->
    lactionj (Id C) l2 l2' (const (p l1)) (const (q l1)).
  Proof.
    (* By splitting into PC and NI, and specialising the Hoare judgements. *)
    intros (Hpc & Hni).
    split.
    - (* PC *)
      intros s (Hin_p & ->)%local_lift_id_semantics.
      apply Hpc, local_lift_id_semantics.
      now split.
    - (* NI *)
      intros r s (Hin_p & ->)%local_lift_id_semantics.
      apply Hni, local_lift_id_semantics.
      now split.
  Qed.

  Lemma lactionj_id_vactionj (l : LSt) (p q : LSt -> V) :
    lactionj (Id C) l l p q <->
    vactionj (lsig_erase lsig) (Id (LabelC C * LSt * LSt)) (p l) (q l).
  Proof.
    split.
    - intros (Hl_pc & Hl_ni).
      split.
      + intros s Hs%lift_id_semantics.
        apply Hl_pc, local_lift_id_semantics.
        now split.
      + intros r s Hs%lift_id_semantics.
        apply Hl_ni, local_lift_id_semantics.
        now split.
    - intros (Hv_pc & Hv_ni).
      split.
      + intros s (Hs & _)%local_lift_id_semantics.
        apply Hv_pc, lift_id_semantics, Hs.
      + intros r s (Hs & _)%local_lift_id_semantics.
        apply Hv_ni, lift_id_semantics, Hs.
  Qed.

  (** The local action judgement has left/right consequence. *)

  Lemma lactionj_cons (l l' : LSt) (p p' q q' :  LSt -> V) (c : LabelC C):
    lactionj (Id C) l  l  p  p' ->
    lactionj (Id C) l' l' q' q  ->
    lactionj c l l' p' q' ->
    lactionj c l l' p  q.
  Proof.
    intros Hp Hq.
    apply vactionj_cons; apply lactionj_id_vactionj; assumption.
  Qed.

  (** The local action judgement has left consequence. *)

  Corollary lactionj_cons_p (l l' : LSt) (p p' q :  LSt -> V) (c : LabelC C):
    lactionj (Id C) l l p p' ->
    lactionj c l l' p' q ->
    lactionj c l l' p q.
  Proof.
    intro Hp.
    apply vactionj_cons_p, lactionj_id_vactionj, Hp.
  Qed.

  (** The local action judgement has right consequence. *)

  Lemma lactionj_cons_q (l l' : LSt) (p q' q : LSt -> V) (c : LabelC C) :
    lactionj (Id C) l' l' q' q ->
    lactionj c l l' p q' ->
    lactionj c l l' p q.
  Proof.
    intro Hq.
    apply vactionj_cons_q, lactionj_id_vactionj, Hq.
  Qed.

  (** [lactionj] is closed over constant frames. *)

  Lemma lactionj_closed (l l' : LSt) (c : LabelC C) (p q : LSt -> V) (r : V):
    lactionj c l l' p q ->
    lactionj c l l' (p * const r)%views (q * const r)%views.
  Proof.
    apply vactionj_closed.
  Qed.

  Lemma label_has_sem_intro (l l' : LSt) (p q : LSt -> V) (c : LabelC C) :
    (label_has_sem lsig l l' c -> lactionj c l l' p q) ->
    lactionj c l l' p q.
  Proof.
    intros Hsem_intro.
    assert ((exists s s', In SSt (label_sem (lsig_erase lsig).(sig_sem) (Cmd (c, l, l')) s) s') ->
            label_has_sem lsig l l' c) as Hget_sem.
    {
      intros (s & s' & Hsem).
      destruct c.
      - apply Singleton_inv in Hsem.
        now injection Hsem.
      - exists s, s'.
        apply Hsem.
    }
    split.
    - intros s' (s & Hin_p & Htrans).
      apply Hsem_intro.
      + apply Hget_sem.
        exists s, s'.
        apply Htrans.
      + exists s.
        split; assumption.
    - intros v s' (s & Hin_p & Htrans).
      apply Hsem_intro.
      + apply Hget_sem.
        exists s, s'.
        apply Htrans.
      + exists s.
        split; assumption.
  Qed.

  (** A version of [lactionj_cons_q] that only requires us to prove
      the entailment over local states that arise from [c]. *)

  Corollary lactionj_cons_q_sem (l l' : LSt) (p q' q : LSt -> V) (c : LabelC C) :
    (label_has_sem lsig l l' c -> lactionj (Id C) l' l' q' q) ->
    lactionj c l l' p q' ->
    lactionj c l l' p q.
  Proof.
    intros Hupd Hactj.
    apply label_has_sem_intro.
    intros Hhas_sem.
    apply lactionj_cons_q with (q' := q').
    - apply Hupd, Hhas_sem.
    - apply Hactj.
  Qed.

  (** We now define equivalents of [vhoare] and [lactionj] that quantify
      over all possible local states. *)

  Definition slhoare (c: LabelC C) (hp hq : LSt -> V) : Prop :=
    forall (s s' : SSt) (l l' : LSt),
      In _ (proj1_sig reif (hp l)) s ->
      In _ (label_sem lsem c (l, s)) (l', s') ->
      In _ (proj1_sig reif (hq l')) s'.

  (** We can rewrite the precondition and postcondition of a
      [slhoare] to equivalent views, since we're using a [Reifier]
      to back it. *)
  Global Instance slhoare_equiv_Proper (c: LabelC C):
    Proper (equiv ==> equiv ==> iff) (slhoare c).
  Proof.
    intros w x Hwx y z Hyz.
    (* We need to peel off and use the reifier equivalence property for this. *)
    unfold slhoare in *.
    destruct reif as (reif' & Hreif).
    unfold proj1_sig in *.
    (* Both directions of [iff] are structurally equivalent, so we do many of
       their proof steps in lockstep. *)
    split.
    1,2: intros Heqhoare s s' l l' Hpre Htrans.
    1,2: specialize (Heqhoare s s' l l').
    (* Cross over between [y] and [z] *)
    1,2: eapply Hreif.
    1: symmetry; apply Hyz.
    2: apply Hyz.
    (* Walk backwards through Hoare judgement. *)
    1,2: apply Heqhoare.
    2,4: apply Htrans.
    (* Cross over between [w] and [x] *)
    1,2: eapply Hreif.
    1: apply Hwx.
    2: symmetry; apply Hwx.
    (* We now have the original precondition back. *)
    1,2: apply Hpre.
  Qed.

  Definition slactionj : LabelC C -> (LSt -> V) -> (LSt -> V) -> Prop :=
    actionj slhoare const.

  Lemma slhoare_vhoare (c : LabelC C) (p q : LSt -> V) :
    slhoare c p q <->
    forall l l' : LSt, vhoare (lsig_erase lsig) (Cmd (c, l, l')) (p l) (q l').
  Proof.
    split.
    - intros Hslh l l' s (s' & Hin_p & Hcmd).
      eapply Hslh.
      + exact Hin_p.
      + exact Hcmd.
    - intros Hlh s s' l l' Hin_s Hcmd.
      apply Hlh with (l := l).
      now exists s.
  Qed.

  Global Instance slactionj_equiv_Proper (c: LabelC C):
    Proper (equiv ==> equiv ==> iff) (slactionj c).
  Proof.
    apply actionj_equiv_Proper, slhoare_equiv_Proper.
  Qed.

  Lemma slhoare_ni_slactionj {MV : ViewsMonoid V} (c : LabelC C) (p q : LSt -> V) :
    (forall r : V, slhoare c (p * const r) (q * const r))%views
    <->
    slactionj c p q.
  Proof.
    split.
    - intros Hslh.
      split.
      + apply (slhoare_equiv_Proper c (dot_one_r _) (dot_one_r _)).
        rewrite <- fview_const_one.
        apply Hslh.
      + apply Hslh.
    - (* This is just the RHS of the action judgement. *)
      now intros (_ & Hslj_ni).
  Qed.

  Lemma slactionj_lactionj (c : LabelC C) (p q : LSt -> V) :
    slactionj c p q <-> forall l l' : LSt, lactionj c l l' p q.
  Proof.
    split.
    - intros (Hpc & Hni) l l'.
      split.
      + apply slhoare_vhoare, Hpc.
      + intro r.
        assert (p l * id r == (p * const r) l)%views as Hp by reflexivity.
        assert (q l' * id r == (q * const r) l')%views as Hq by reflexivity.
        eapply vhoare_equiv_Proper.
        * exact Hp.
        * exact Hq.
        * apply slhoare_vhoare, (Hni r).
    - intro Hloc.
      split.
      + apply slhoare_vhoare.
        intros l l'.
        apply Hloc.
      + intro r.
        apply slhoare_vhoare.
        intros l l'.
        apply Hloc.
  Qed.

  Corollary slhoare_ni_lactionj {MV : ViewsMonoid V} (c : LabelC C) (p q : LSt -> V) :
    (forall r : V, slhoare c (p * const r) (q * const r))%views
    <->
    forall l l' : LSt, lactionj c l l' p q.
  Proof.
    etransitivity.
    - apply slhoare_ni_slactionj.
    - apply slactionj_lactionj.
  Qed.

  Lemma slactionj_closed (c: LabelC C) (p q : LSt -> V) (r : V):
    slactionj c p q ->
    slactionj c (p * const r)%views (q * const r)%views.
  Proof.
    intros (_ & Hsl_ni).
    split.
    - apply Hsl_ni.
    - intros r2.
      setoid_rewrite dot_assoc.
      setoid_rewrite fview_dot_const_dist.
      apply Hsl_ni.
  Qed.

  (** Local Hoare, with the identity command, is reflexive. *)

  Lemma slhoare_id_refl (p : LSt -> V) :
    slhoare (Id C) p p.
  Proof.
    intros s s' l l' Hin_p Htrans%Singleton_inv.
    injection Htrans.
    now intros -> ->.
  Qed.

  (** Semantic entailment is reflexive. *)

  Corollary slactionj_id_refl (p : LSt -> V) :
    slactionj (Id C) p p.
  Proof.
    apply actionj_id_refl, slhoare_id_refl.
  Qed.

  (** Local Hoare triples have left consequence. *)

  Lemma slhoare_cons_p (p p' q : LSt -> V) (c: LabelC C):
    slhoare (Id C) p p' ->
    slhoare c p' q ->
    slhoare c p q.
  Proof.
    intros Hcons Hrest s s' l l' Hin_p Htrans.
    refine (Hrest s s' l l' _ Htrans).
    refine (Hcons s s l l Hin_p (In_singleton _ _)).
  Qed.

  (** The local action judgement has left consequence. *)

  Corollary slactionj_cons_p (p p' q : LSt -> V) (c : LabelC C):
    slactionj (Id C) p p' ->
    slactionj c p' q ->
    slactionj c p q.
  Proof.
    intros (Hcons_pc & Hcons_ni) Hrest.
    split.
    - (* Partial correctness *)
      refine (slhoare_cons_p Hcons_pc _).
      apply Hrest.
    - (* Non-interference *)
      intro r.
      specialize (Hcons_ni r).
      refine (slhoare_cons_p Hcons_ni _).
      apply Hrest.
  Qed.

  (** Local Hoare triples have right consequence. *)

  Corollary slhoare_cons_q (p q' q : LSt -> V) (c : LabelC C) :
    slhoare (Id C) q' q ->
    slhoare c p q' ->
    slhoare c p q.
  Proof.
    intros Hcons Hrest s s' l l' Hin_p Htrans.
    refine (Hcons s' s' l' l' _ (In_singleton _ _)).
    refine (Hrest s s' l l' Hin_p Htrans).
  Qed.

  (** The local action judgement has right consequence. *)

  Corollary slactionj_cons_q (p q' q : LSt -> V) (c : LabelC C) :
    slactionj (Id C) q' q ->
    slactionj c p q' ->
    slactionj c p q.
  Proof.
    intros Hcons (Hrest_pc & Hrest_ni).
    split.
    - (* Partial correctness *)
      refine (slhoare_cons_q _ Hrest_pc).
      apply Hcons.
    - intro r.
      refine (slhoare_cons_q _ (Hrest_ni r)).
      apply Hcons.
  Qed.

  Lemma lactionj_pair (l l' : LSt) (p1 p2 q1 q2 : LSt -> V):
    lactionj (Id C) l l' p1 q1 ->
    lactionj (Id C) l l' p2 q2 ->
    (lactionj (Id C) l l' (p1 * p2) (q1 * q2))%views.
  Proof.
    intros H1 H2.
    split.
    - (* PC *)
      intros s (Hin_p & <-)%local_lift_id_semantics.
      apply lactionj_id_vactionj in H1.
      apply lactionj_id_vactionj in H2.
      apply (vactionj_pair H1 H2), lift_id_semantics, Hin_p.
    - (* NI *)
      intros r s (Hin_p & <-)%local_lift_id_semantics.
      apply lactionj_id_vactionj in H1.
      apply lactionj_id_vactionj in H2.
      apply (vactionj_pair H1 H2), lift_id_semantics, Hin_p.
  Qed.
End local_action_judgements.
