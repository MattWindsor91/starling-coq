(** * Local Views Framework: utilities for dealing with lists of thread components. *)

From Coq Require Import
     Arith.PeanoNat
     Bool.BoolEq
     Lists.List
     Program
     Sets.Constructive_sets
     Sets.Ensembles
     Relations.Relation_Definitions
     Classes.SetoidClass
.

From Starling Require Import
     Utils.List.Facts
     Utils.List.Override
     Views.Classes
     Views.Transformers.Function
     Views.Frameworks.Common
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.Language
     Views.Frameworks.LVF.Signatures
.

Set Implicit Arguments.

Open Scope program_scope.

Chapter LVF_thread_lists.

Variables
  (V    : Type)  (* Shared views *)
  (C    : Set)   (* Atomic actions *)
  (LSt  : Set)   (* Local state *)
  (SSt  : Set).  (* Shared state *)

Context
  {SV : Setoid V}
  {VV : ViewsSemigroup V}
  {MV : ViewsMonoid V}.

Variables
  (lsig : LocalSignature C LSt SSt)
  (df   : LSt).

Section selectors.

  (** Selects the [n]th view from a view list [vs].
      Defaults to [const 1]. *)

  Definition vsel (vs: list (LSt -> V)) (n: nat): LSt -> V :=
    nth n vs (const 1%views).

  (** Selects the [n]th command from a command list [cs].
      Defaults to [PSkip]. *)

  Definition csel (cs: list (LProg C)) (n: nat): LProg C :=
    nth n cs (exist _ (PSkip C) I).

  (** Selects the [n]th command from a local list [ls].
      Defaults to [df]. *)

  Definition lsel (ls: list LSt) (n: nat): LSt :=
    nth n ls df.

End selectors.

Section lnstar.
  (** ** Folding a list of thread local views into a single non-local view *)

  (** [lnstar_combine] models folding a list of thread views and local
    state pairs into a single view. *)

  Definition lnstar_combine (pls: list ((LSt -> V) * LSt)): V :=
    fold_right
      (fun pl (v: V) => (pl.(fst) pl.(snd)) * v)%views
      1%views
      pls.

  (** [lnstar] models folding parallel lists of thread views and local
    states into a single view. *)

  Definition lnstar (ps: list (LSt -> V)) (ls: list LSt): V :=
    lnstar_combine (combine ps ls).

  Lemma lnstar_nil:
    lnstar nil nil = 1%views.
  Proof.
    reflexivity.
  Qed.

  Lemma combine_cons {A B: Type} (x: A) (y: B) (xs: list A) (ys: list B):
    length xs = length ys ->
    combine (x::xs) (y::ys) = (x, y)::(combine xs ys).
  Proof.
    intro Hlen.
    revert x y.
    revert Hlen.
    revert xs ys.
    induction xs, ys; try discriminate.
    - (* Both are nil *)
      reflexivity.
    - (* Both are cons. *)
      intros Hlen x y.
      injection Hlen.
      intro Hlen_xy.
      simpl.
      now rewrite <- (IHxs ys Hlen_xy a b).
  Qed.

  Lemma lnstar_cons (p: LSt -> V) (ps: list (LSt -> V)) (l: LSt) (ls: list LSt):
    length ps = length ls ->
    lnstar (p::ps) (l::ls) = (p l * lnstar ps ls)%views.
  Proof.
    intro Hlen.
    unfold lnstar, lnstar_combine.
    now rewrite combine_cons.
  Qed.

  Lemma lnstar_app (ps1 ps2: list (LSt -> V)) (ls1 ls2: list LSt):
    length ps1 = length ls1 ->
    length ps2 = length ls2 ->
    lnstar (ps1++ps2) (ls1++ls2) == (lnstar ps1 ls1 * lnstar ps2 ls2)%views.
  Proof.
    revert ps1 ls1.
    induction ps1, ls1; try discriminate.
    - (* Both nil *)
      rewrite lnstar_nil.
      simpl.
      symmetry.
      apply dot_one_l.
    - (* Both cons *)
      rewrite <- ! app_comm_cons.
      intros Hlen1a Hlen2.
      injection Hlen1a.
      intro Hlen1.
      rewrite ! lnstar_cons; try assumption.
      + rewrite dot_assoc.
        apply equiv_dot_right, IHps1; assumption.
      + now rewrite ! app_length, Hlen2, Hlen1.
  Qed.

  Corollary lnstar_shuf (p: LSt -> V) (ps1 ps2: list (LSt -> V)) (l: LSt) (ls1 ls2: list LSt):
    length ps1 = length ls1 ->
    length ps2 = length ls2 ->
    lnstar (ps1 ++ (p::ps2)) (ls1 ++ (l::ls2)) == lnstar ((p::ps1)++ps2) ((l::ls1)++ls2).
  Proof.
    intros Hlen1 Hlen2.
    rewrite ! lnstar_app, ! lnstar_cons; try assumption.
    - rewrite dot_comm.
      rewrite ! dot_assoc.
      apply equiv_dot_right.
      apply dot_comm.
    - simpl.
      now f_equal.
    - simpl.
      now f_equal.
  Qed.

  (** For any valid index [n] into a list of local views [ps], if we have a
      parallel locals list [ls], then we can rearrange [lnstar ps ls] into the
      application of the [n]th [ls] to the [n]th [ps] and the [lnstar] of the
      remainder. *)

  Lemma lnstar_nth (ps: list (LSt -> V)) (ls: list LSt) (n: nat):
    length ps = length ls ->
    n < length ps ->
    exists ps1 ps2 ls1 ls2,
      ps = ps1 ++ (vsel ps n)::ps2 /\
      length ps1 = n /\
      ls = ls1 ++ (lsel ls n)::ls2 /\
      length ls1 = n /\
      lnstar ps ls == ((vsel ps n) (lsel ls n) * lnstar (ps1++ps2) (ls1++ls2))%views.
  Proof.
    intros Hlen Hnps.
    assert (n < length ls) as Hnls by (rewrite <- Hlen; exact Hnps).
    destruct (nth_split ps 1%views Hnps) as (ps1 & ps2 & Hps_split & Hps_len).
    destruct (nth_split ls df Hnls) as (ls1 & ls2 & Hls_split & Hls_len).
    exists ps1, ps2, ls1, ls2.
    repeat split; try assumption.
    (* Just need to show the lnstar equivalence now. *)
    (* First, expand out ps and ls. *)
    rewrite Hps_split, Hls_split in *.
    (* We'll first need length results for ps1/ls1 and ps1/ls2 *)
    assert (length ps1 = length ls1) as Hlen1 by (rewrite Hps_len, Hls_len; reflexivity).
    rewrite ! app_length in Hlen.
    rewrite Hps_len, Hls_len in Hlen.
    apply Nat.add_cancel_l in Hlen.
    injection Hlen.
    intro Hlen2.
    (* Now, we can use the shuffle rules for nstar. *)
    rewrite lnstar_shuf; try assumption.
    rewrite ! lnstar_app; try assumption.
    - rewrite lnstar_cons; try assumption.
      rewrite <- Hps_split, <- Hls_split.
      rewrite dot_assoc.
      reflexivity.
    - simpl.
      f_equal.
      exact Hlen1.
  Qed.
End lnstar.

End LVF_thread_lists.