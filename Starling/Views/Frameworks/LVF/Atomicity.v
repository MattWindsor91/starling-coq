(** * Local Views Framework: Atomicity

This development contains general rules for treating a composition of
multiple local atomic actions as a single atomic action in the LVF.
These rules are fairly weak, but true for all LVF instances.

 *)

From Coq Require Import
     Program
     Sets.Ensembles
     Classes.SetoidClass
.

From Starling Require Import
     Views.Classes
     Views.Frameworks.Common
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.Language
     Views.Frameworks.LVF.SemJudgements
     Views.Frameworks.LVF.SafetyRules
     Views.Frameworks.LVF.Signatures
.

Set Implicit Arguments.

Chapter LVF_atomicity.

Variables
  (V    : Type)  (* Shared views *)
  (C    : Set)   (* Atomic actions *)
  (LSt  : Set)   (* Local state *)
  (SSt  : Set).  (* Shared state *)

Context
  {SV : Setoid V}
  {VV : ViewsSemigroup V}.

Variable
  lsig: LocalSignature C LSt SSt.

(** A command is 'local' if:

      1) it is the identity on shared states;
      2) for any pair of local states, if it is defined on one shared state,
         it is defined on all others. *)

Definition is_local_cmd (c : C) : Prop :=
  forall (l l' : LSt) (s s' : SSt),
    lsig.(lsig_lsem) c (l, s) (l', s') ->
    s = s' /\ (forall s'' : SSt, lsig.(lsig_lsem) c (l, s'') (l', s'')).

(** Local commands [c] always preserve a shared view in [vhoare]
      judgements. *)

Lemma local_cmd_view_stability_hoare (c : C) (v : V) (l l' : LSt):
  is_local_cmd c ->
  vhoare (lsig_erase lsig) (Cmd (Cmd c, l, l')) v v.
Proof.
  intros Hlocal s' (s & Hin_p & Htrans).
  rewrite <- (proj1 (Hlocal l l' s s' Htrans)).
  apply Hin_p.
Qed.

(** Local commands [c] always satisfy the action judgement
        {[const (p l)]} [c] {[const (p l)]}, where [l] is the initial local
        variable set. *)

Lemma local_cmd_view_stability (c : C) (p : LSt -> V) (l l' : LSt):
  is_local_cmd c ->
  lactionj lsig (Cmd c) l l' p (const (p l)).
Proof.
  intro Hlocal.
  split.
  - (* Local *)
    apply local_cmd_view_stability_hoare, Hlocal.
  - (* Non-interference *)
    intros r.
    apply local_cmd_view_stability_hoare, Hlocal.
Qed.

Lemma is_local_cmd_id (c : C) (p q : LSt -> V) (l l' : LSt) :
  is_local_cmd c ->
  vactionj (lsig_erase lsig) (Id _) (p l) (q l') ->
  lactionj lsig (Cmd c) l l' p q.
Proof.
  intros Hlocal Haction.
  apply vactionj_cons_q with (hq' := (p l)).
  - exact Haction.
  - apply local_cmd_view_stability, Hlocal.
Qed.

(** A command [cc] is an atomic composition of two commands [c1] and
        [c2] iff [cc] updates a state X to Z if there exists some state Y where
        [c1] updates X to Y and [c2] updates Y to Z. *)

Definition atomic_comp (c1 c2 cc : C) : Prop :=
  forall (l1 l3 : LSt) (s1 s3 : SSt),
    lsig.(lsig_lsem) cc (l1, s1) (l3, s3) <->
    (exists (l2 : LSt) (s2 : SSt),
        lsig.(lsig_lsem) c1 (l1, s1) (l2, s2) /\
        lsig.(lsig_lsem) c2 (l2, s2) (l3, s3)).

(** The atomic composition of two local commands is local. *)

Lemma atomic_comp_local (c1 c2 cc : C) :
  is_local_cmd c1 ->
  is_local_cmd c2 ->
  atomic_comp c1 c2 cc ->
  is_local_cmd cc.
Proof.
  intros Hlocal_c1 Hlocal_c2 Hcomp l1 l3 s1 s3 (l2 & s2 & Hsem_12 & Hsem_23)%Hcomp.
  destruct (Hlocal_c1 l1 l2 s1 s2 Hsem_12) as (-> & Hsem_12_rest).
  destruct (Hlocal_c2 l2 l3 s2 s3 Hsem_23) as (-> & Hsem_23_rest).
  repeat split.
  intros s''.
  apply Hcomp.
  exists l2, s''.
  split.
  - apply Hsem_12_rest.
  - apply Hsem_23_rest.
Qed.

(** The action judgement of two commands entails the action judgement of
        their atomic composition, but only if the final view doesn't depend on
        local pre-state. *)

Lemma atomic_comp_lactionj (c1 c2 cc : C) (lp lq : LSt) (p q r : LSt -> V) :
  atomic_comp c1 c2 cc ->
  (forall lr, lactionj lsig (Cmd c1) lp lr p r /\
         ((exists s s', lsig.(lsig_lsem) c1 (lp, s) (lr, s')) -> lactionj lsig (Cmd c2) lr lq r q)) ->
  lactionj lsig (Cmd cc) lp lq p q.
Proof.
  intros Hcomp Hindiv.
  split.
  - (* Local *)
    intros sq (sp & Hin_p & Htrans_pq).
    destruct (proj1 (Hcomp lp lq sp sq) Htrans_pq) as (lr & sr & Htrans_pr & Htrans_rq).
    destruct (Hindiv lr) as ((Hpr_pc & _) & Hrq).
    eapply Hrq.
    + now exists sp, sr.
    + exists sr.
      split.
      * apply Hpr_pc.
        now (exists sp).
      * apply Htrans_rq.
  - (* Non-interference *)
    intros v sq (sp & Hin_p & Htrans_pq).
    destruct (proj1 (Hcomp lp lq sp sq) Htrans_pq) as (lr & sr & Htrans_pr & Htrans_rq).
    destruct (Hindiv lr) as ((_ & Hpr_ni) & Hrq).
    eapply Hrq.
    + now exists sp, sr.
    + exists sr.
      split.
      * apply Hpr_ni.
        now (exists sp).
      * apply Htrans_rq.
Qed.

Lemma sc_implies_ac_ex (a b c : C) (l : LSt) (p q : LSt -> V):
  atomic_comp a b c ->
  SSafeEx lsig l p (LSeq (LAtom a) (LAtom b)) q ->
  SSafeEx lsig l p (LAtom c) q.
Proof.
  intros Hcomp Hsafe_na.
  inversion Hsafe_na as [l0 p' c' q' _ Hstep Hp Hc Hq].
  apply safe_atomic_ex.
  intros l'.
  destruct (Hstep (Cmd a)
                  (LSeq (LSkip C) (LAtom b))
                  (TSeqStep (PAtom b) (TAtom a)))
    as (p1 & Hp1).
  eapply (atomic_comp_lactionj Hcomp).
  intro lr.
  split.
  - (* P to R *)
    split.
    + (* Local, P to R *)
      intros s' (s & Hin_p & Hcmd).
      destruct (Hp1 lr) as ((Hp1_pc & _) & _).
      apply Hp1_pc.
      exists s.
      now split.
    + (* Non-interference, P to R *)
      intros r s' (s & Hin_p & Hcmd).
      destruct (Hp1 lr) as ((_ & Hp1_ni) & _).
      apply Hp1_ni.
      exists s.
      now split.
  - (* R to Q *)
    intro Hsem_lc.
    destruct (Hp1 lr) as (_ & Hsafe_skip).
    specialize (Hsafe_skip Hsem_lc).
    inversion Hsafe_skip as [lr0 ps cs qs _ Hstep_skip Hps Hcs Hqs].
    destruct (Hstep_skip
                (Id C)
                (LAtom b)
                (TSeqSkip (PAtom b)))
      as (p2 & Hsj2 & Hsafe_rest).
    refine (lactionj_cons_p Hsj2 _).
    now apply safe_atomic_ex, Hsafe_rest.
Qed.

Corollary sc_implies_ac (a b c : C) (p q : LSt -> V):
  atomic_comp a b c ->
  SSafe lsig p (LSeq (LAtom a) (LAtom b)) q ->
  SSafe lsig p (LAtom c) q.
Proof.
  intro Hcomp.
  apply safe_ex_safe.
  intro l.
  apply sc_implies_ac_ex, Hcomp.
Qed.

(** We can rephrase a [lactionj] to make it easier to prove. *)

Lemma lactionj_simpl (c : LabelC C) (p q : LSt -> V) (l l' : LSt) :
  (forall s s',
      In _ (label_sem lsig.(lsig_lsem) c (l, s)) (l', s') ->
      (reify lsig.(lsig_reifier) (p l) s ->
       reify lsig.(lsig_reifier) (q l') s') /\
      (forall v : V,
          reify lsig.(lsig_reifier) (p l * v) s ->
          reify lsig.(lsig_reifier) (q l' * v) s'))%views <->
  lactionj lsig c l l' p q.
Proof.
  split.
  - intro Hsimpl.
    split.
    + (* Local *)
      intros s' (s & Hin_p & Htrans).
      apply (Hsimpl s s' Htrans), Hin_p.
    + (* Non-interference *)
      intros v s' (s & Hin_pv & Htrans).
      apply (Hsimpl s s' Htrans), Hin_pv.
  - intros Haj s s' Hsem.
    split.
    + (* Local *)
      intro Hin_p.
      apply Haj.
      exists s.
      split; assumption.
    + (* Non-interference *)
      intros v Hin_pv.
      apply Haj.
      exists s.
      split; assumption.
Qed.

(** An atomic composition, where the first command is local, is safe if,
        and only if, the non-atomic sequential composition is safe. *)

Lemma local_pre_cmd_ex (lc ac cc : C) (l : LSt) (p q : LSt -> V):
  is_local_cmd lc ->
  atomic_comp lc ac cc ->
  SSafeEx lsig l p (LSeq (LAtom lc) (LAtom ac)) q <->
  SSafeEx lsig l p (LAtom cc) q.
Proof.
  intros Hlocal Hcomp.
  split.
  - (* Non-atomic implies atomic. *)
    apply sc_implies_ac_ex, Hcomp.
  - (* Atomic implies non-atomic. *)
    intro Hsafe_cc.
    apply safe_seq_atom_ex with (q := const (p l)).
    + apply safe_atomic_ex.
      intros l'.
      apply local_cmd_view_stability, Hlocal.
    + intros l' (_ & _ & (_ & Hsem_lc)%Hlocal).
      apply safe_atomic_ex.
      intros l''.
      apply (proj2 (safe_atomic_ex _ _ _ _ cc)) with (l'0 := l'') in Hsafe_cc.
      apply lactionj_simpl.
      intros s s' Hsem_ac.
      eapply lactionj_simpl in Hsafe_cc.
      -- apply Hsafe_cc.
      -- apply Hcomp.
         exists l', s.
         split.
         ++ apply Hsem_lc.
         ++ apply Hsem_ac.
Qed.

Corollary local_pre_cmd (lc ac cc : C) (p q : LSt -> V):
  is_local_cmd lc ->
  atomic_comp lc ac cc ->
  SSafe lsig p (LSeq (LAtom lc) (LAtom ac)) q <->
  SSafe lsig p (LAtom cc) q.
Proof.
  intros Hlocal Hcomp.
  split; apply safe_ex_safe; intro l; apply local_pre_cmd_ex; assumption.
Qed.

Definition is_deterministic (c : C) : Prop :=
  forall (s s'1 s'2 : LSt * SSt),
    lsig.(lsig_lsem) c s s'1 ->
    lsig.(lsig_lsem) c s s'2 ->
    s'1 = s'2.

Definition is_fun (c : C) (f : LSt -> LSt) (g : SSt -> SSt) : Prop :=
  forall (l : LSt) (s : SSt),
    lsig.(lsig_lsem) c (l, s) (f l, g s).

Lemma is_det_fun_local (c : C) (f : LSt -> LSt) :
  is_deterministic c ->
  is_fun c f id ->
  is_local_cmd c.
Proof.
  intros Hdet Hfun l l' s s' Htrans.
  injection (Hdet (l, s) (f l, id s) (l', s') (Hfun l s) Htrans).
  intros <- <-.
  split.
  - reflexivity.
  - intro s''.
    apply Hfun.
Qed.

(** An atomic composition, where the second command is a deterministic
      local function, is safe if,
        and only if, the non-atomic sequential composition is safe. *)

Lemma local_post_cmd_ex (lc ac cc : C) (l : LSt) (p q : LSt -> V) (f : LSt -> LSt) :
  is_deterministic lc ->
  is_fun lc f id ->
  atomic_comp ac lc cc ->
  SSafeEx lsig l p (LSeq (LAtom ac) (LAtom lc)) q <->
  SSafeEx lsig l p (LAtom cc) q.
Proof.
  intros Hdet Hfun Hcomp.
  split.
  - (* Non-atomic implies atomic. *)
    apply sc_implies_ac_ex, Hcomp.
  - (* Atomic implies non-atomic. *)
    intros Hsafe_cc.
    apply safe_seq_atom_ex with (q := q ∘ f).
    + (* First step. *)
      apply safe_atomic_ex.
      intro l'.
      apply (proj2 (safe_atomic_ex _ _ _ _ cc)) with (l'0 := f l') in Hsafe_cc.
      apply lactionj_simpl.
      intros s s' Hsem_ac.
      eapply lactionj_simpl in Hsafe_cc.
      * apply Hsafe_cc.
      * (* Showing the semantics is appropriate. *)
        apply Hcomp.
        exists l', s'.
        split.
        -- apply Hsem_ac.
        -- apply Hfun.
    + (* Second step (local). *)
      intros l' Hsem.
      apply safe_atomic_ex.
      intros l''.
      apply lactionj_simpl.
      intros s s' Hsem_lc.
      injection (Hdet (l', s) (l'', s') (f(l'), s) Hsem_lc (Hfun l' s)).
      now intros -> ->.
Qed.

Corollary local_post_cmd (lc ac cc : C) (p q : LSt -> V) (f : LSt -> LSt) :
  is_deterministic lc ->
  is_fun lc f id ->
  atomic_comp ac lc cc ->
  SSafe lsig p (LSeq (LAtom ac) (LAtom lc)) q <->
  SSafe lsig p (LAtom cc) q.
Proof.
  intros Hdet Hfun Hcomp.
  split; apply safe_ex_safe; intro l; apply (local_post_cmd_ex l _ _ Hdet Hfun); assumption.
Qed.

End LVF_atomicity.
