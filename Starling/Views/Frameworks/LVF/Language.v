(** * Local Views Framework: language *)

From Coq Require Import
     Arith.PeanoNat
     Bool.BoolEq
     Lists.List
     Program
     Sets.Constructive_sets
     Sets.Ensembles
     Relations.Relation_Definitions
     Classes.SetoidClass.

From Starling Require Import
     Utils.List.Facts
     Utils.List.Override
     Views.Classes
     Views.Transformers.Function
     Views.Frameworks.Common
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.Signatures.

Set Implicit Arguments.

Open Scope program_scope.

Section LVF_programs.
  Variables
    (V    : Type)  (* Shared views *)
    (C    : Set)   (* Atomic actions *)
    (LSt  : Set)   (* Local state *)
    (SSt  : Set).  (* Shared state *)

  Context
    {SV : Setoid V}
    {VV : ViewsSemigroup V}.

  Variable
    (lsig : LocalSignature C LSt SSt).

  (** LVF programs are just Views programs without parallel composition.
      Throughout the following, we use [`c] to mean 'the underlying Views
      program of [c]---this is just the shorthand for [proj1_sig] that
      the Program module gives us. *)

  Definition LProg : Set := { x : Prog C | NoPar x }.

  Definition LSkip : LProg := exist _ (PSkip C) I.

  Definition LAtom (c : C) : LProg := exist _ (PAtom c) I.

  Definition LIter (p : LProg) : LProg := exist _ (PIter (proj1_sig p)) (proj2_sig p).

  Definition LSeq (c1 c2 : LProg) : LProg
    := exist _ (PSeq (proj1_sig c1) (proj1_sig c2)) (conj (proj2_sig c1) (proj2_sig c2)).

  Definition LNdt (c1 c2 : LProg) : LProg
    := exist _ (PNdt (proj1_sig c1) (proj1_sig c2)) (conj (proj2_sig c1) (proj2_sig c2)).
End LVF_programs.
