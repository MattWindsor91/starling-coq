(** * Local Views Framework: Threads

This file just packages up the relationship between single-thread safety
[SSafe] and multi-thread safety [MSafe] in a slightly more understandable
way.

 *)

From Coq Require Import
     Arith.PeanoNat
     Lists.List
     Program
     Classes.SetoidClass
.

From Starling Require Import
     Views.Classes
     Views.Frameworks.Common
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.SemJudgements
     Views.Frameworks.LVF.Language
     Views.Frameworks.LVF.ThreadLists
     Views.Frameworks.LVF.Signatures
     Views.Frameworks.LVF.SafetyRules
.

Set Implicit Arguments.

Local Open Scope program_scope.

(* TODO (@MattWindsor91): move this elsewhere. *)

Chapter LVF_threads.
Variables
  (V    : Type)  (* Shared views *)
  (C    : Set)   (* Atomic actions *)
  (LSt  : Set)   (* Local state *)
  (SSt  : Set).  (* Shared state *)

Context
  {SV     : Setoid V}
  {VV     : ViewsSemigroup V}
  {MV     : ViewsMonoid V}.

Variables
  (df   : LSt) (* Default local state *)
  (lsig : LocalSignature C LSt SSt).

Record Thread : Type :=
  mk_Thread
    {
      thread_p : LSt -> V;
      thread_c : LProg C;
      thread_q : LSt -> V;
    }.

Definition ThreadSafe (th : Thread) : Prop :=
  SSafe lsig th.(thread_p) th.(thread_c) th.(thread_q).

(** If all [Thread]s are safe, then the system created by separating out
      the threads' components is [MSafe]. *)

Theorem Thread_Safe_MSafe (ts : list Thread) :
  Forall ThreadSafe ts ->
  MSafe lsig
        df
        (map thread_p ts)
        (map thread_q ts)
        (map thread_c ts).
Proof.
  intros Hall_safe.
  apply all_SSafe_MSafe; try (rewrite ! map_length; reflexivity).
  intros t.
  destruct (Nat.le_gt_cases (length ts) t) as [Ht|Ht].
  - unfold vsel, csel.
    rewrite ! nth_overflow; try (rewrite ! map_length; exact Ht).
    apply safe_skip, slactionj_id_refl.
  - (* In bounds *)
    pose (mk_Thread (const 1) (LSkip C) (const 1))%views as dt.
    unfold vsel, csel.
    fold (LSkip C).
    (* Re-interpret the defaults in SSafe as the parts of the default thread. *)
    change (const 1%views) with (dt.(thread_p)) at 1.
    change (const 1%views) with (dt.(thread_q)) at 1.
    change (LSkip C)       with (dt.(thread_c)).
    rewrite ! map_nth.
    apply Forall_forall with (x := nth t ts dt) in Hall_safe.
    + apply Hall_safe.
    + apply nth_In, Ht.
Qed.
End LVF_threads.