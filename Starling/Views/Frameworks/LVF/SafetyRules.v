(** * Local Views Framework: Safety inference rules *)

From Coq Require Import
     Arith.PeanoNat
     Bool.BoolEq
     Lists.List
     Program
     Sets.Constructive_sets
     Sets.Ensembles
     Relations.Relation_Definitions
     Classes.SetoidClass.

From Starling Require Import
     Views.Classes
     Views.Transformers.Function
     Views.Frameworks.Common
     Views.Frameworks.LVF.ActionJudgements
     Views.Frameworks.LVF.Language
     Views.Frameworks.LVF.SemJudgements
     Views.Frameworks.LVF.Signatures.

Set Implicit Arguments.

Local Open Scope program_scope.

Section LVF_safety_rules.
  Variables
    (V    : Type)  (* Shared views *)
    (C    : Set)   (* Atomic actions *)
    (LSt  : Set)   (* Local state *)
    (SSt  : Set).  (* Shared state *)

  Context
    {SV     : Setoid V}
    {VV     : ViewsSemigroup V}.

  Variable
    lsig: LocalSignature C LSt SSt.

  (** We can promote an explicit rule to an implicit one. *)

  Lemma safe_ex_safe (p1 p2 q1 q2 : LSt -> V) (c1 c2 : LProg C) :
    (forall l, SSafeEx lsig l p1 c1 q1 -> SSafeEx lsig l p2 c2 q2) ->
    SSafe lsig p1 c1 q1 ->
    SSafe lsig p2 c2 q2.
  Proof.
    intros Hsafe_ex Hsafe_1 l; intuition.
  Qed.

  Lemma safe_skip_ex (l : LSt) (p q : LSt -> V):
    (forall l', lactionj lsig (Id C) l l' p q) <-> SSafeEx lsig l p (LSkip C) q.
  Proof.
    cbn.
    split.
    - intros Hadj.
      destruct_safe_and_transition.
      apply Hadj.
    - intros Hsafe l'.
      inversion Hsafe.
      apply (H (reflexivity _)).
  Qed.

  Corollary safe_skip (p q : LSt -> V):
    slactionj lsig (Id C) p q <-> SSafe lsig p (LSkip C) q.
  Proof.
    split.
    - intros Hadj l.
      now apply safe_skip_ex, slactionj_lactionj, Hadj.
    - intro Hsafe.
      apply slactionj_lactionj.
      intros l l'.
      now eapply safe_skip_ex, Hsafe.
  Qed.

  Corollary safe_skip_strong (p : LSt -> V) :
    SSafe lsig p (LSkip C) p.
  Proof.
    apply safe_skip, slactionj_id_refl.
  Qed.

  Lemma safe_atomic_ex (l : LSt) (p q : LSt -> V) (atom: C):
    (forall l', lactionj lsig (Cmd atom) l l' p q) <->
    SSafeEx lsig l p (LAtom atom) q.
  Proof.
    split.
    - (* Action judgement implies safety. *)
      intros Hadj.
      destruct_safe_and_transition.
      exists q.
      intros l'.
      split.
      + apply Hadj.
      + intros _.
        apply SSafeEx_prf_irr with (c1 := LSkip C); intuition.
        now apply safe_skip_strong.
    - (* Safety implies action judgement. *)
      intros Hsafe l'.
      inversion Hsafe as [l0 p' c' q' _ Hstep Hl Hp Hc Hq].
      (* Take the atomic step *)
      destruct
        (Hstep (Cmd atom) (exist _ (PSkip C) I) (TAtom atom))
        as (r & Hr).
      destruct (Hr l') as (Hpr & Hsafe_end).
      apply lactionj_cons_q_sem with (q' := r).
      + intros Hhas_sem.
        now eapply safe_skip_ex, Hr, Hhas_sem.
      + exact Hpr.
  Qed.

  Corollary safe_atomic (p q : LSt -> V) (atom: C):
    slactionj lsig (Cmd atom) p q <-> SSafe lsig p (LAtom atom) q.
  Proof.
    split.
    - (* Action judgement implies safety. *)
      intros Hadj l.
      now eapply safe_atomic_ex, slactionj_lactionj, Hadj.
    - (* Safety implies action judgement. *)
      intro Hsafe.
      apply slactionj_lactionj.
      intros l l'.
      now eapply safe_atomic_ex, Hsafe.
  Qed.

  Lemma safe_cons_p_ex (l : LSt) (p p' q : LSt -> V) (c : LProg C) :
    lactionj lsig (Id C) l l p p' ->
    SSafeEx lsig l p' c q ->
    SSafeEx lsig l p c q.
  Proof.
    intros Hpp' Hsafe.
    destruct Hsafe.
    destruct_safe.
    - (* Skip case *)
      intros Hskip l'.
      specialize (H Hskip l').
      apply lactionj_cons_p with (p' := p0); assumption.
    - (* Step case *)
      intros lc c' Htrans.
      destruct (H0 lc c' Htrans) as (p' & Hrest).
      exists p'.
      intros l'.
      split.
      + apply lactionj_cons_p with (p' := p0).
        * assumption.
        * apply Hrest.
      + apply Hrest.
  Qed.

  Corollary safe_cons_p (p p' q : LSt -> V) (c : LProg C) :
    slactionj lsig (Id C) p p' ->
    SSafe lsig p' c q ->
    SSafe lsig p c q.
  Proof.
    intros Hcons.
    apply safe_ex_safe.
    intro l.
    apply safe_cons_p_ex, slactionj_lactionj, Hcons.
  Qed.

  Lemma safe_cons_q_ex (l : LSt) (p q' q : LSt -> V) (c : LProg C) :
    (forall l', lactionj lsig (Id C) l' l' q' q) ->
    SSafeEx lsig l p c q' ->
    SSafeEx lsig l p c q.
  Proof.
    intros Hcons Hsafe_prev.
    apply SSafeEx_coind with (R := fun l0 p0 c0 q0 => q0 = q /\ SSafeEx lsig l0 p0 c0 q').
    - intros l0 p0 q0 c0 (-> & Hsafe_prv) Hc l'.
      inversion_clear Hsafe_prv.
      apply lactionj_cons_q with (q' := q'); intuition.
    - intros l0 p0 c0 lc c1 q0 (-> & Hsafe_prv) Htrans.
      inversion_clear Hsafe_prv.
      destruct (H0 lc c1 Htrans) as (p' & Hp').
      exists p'.
      intro l'.
      specialize (Hp' l').
      intuition.
    - intuition.
  Qed.

  Corollary safe_cons_q (p q' q : LSt -> V) (c : LProg C) :
    slactionj lsig (Id C) q' q ->
    SSafe lsig p c q' ->
    SSafe lsig p c q.
  Proof.
    intros Hcons.
    apply safe_ex_safe.
    intro l.
    apply safe_cons_q_ex.
    intro l'.
    apply slactionj_lactionj, Hcons.
  Qed.

  Lemma safe_cons_ex (l : LSt) (p p' q' q : LSt -> V) (c : LProg C) :
    lactionj lsig (Id C) l l p p' ->
    (forall l', lactionj lsig (Id C) l' l' q' q) ->
    SSafeEx lsig l p' c q' ->
    SSafeEx lsig l p  c q.
  Proof.
    intros Hp Hq Hcons.
    apply safe_cons_p_ex with (p' := p').
    - exact Hp.
    - apply safe_cons_q_ex with (q' := q'); assumption.
  Qed.

  Lemma safe_cons (p p' q' q : LSt -> V) (c : LProg C) :
    slactionj lsig (Id C) p  p' ->
    slactionj lsig (Id C) q' q  ->
    SSafe lsig p' c q' ->
    SSafe lsig p  c q.
  Proof.
    intros Hp Hq Hcons.
    apply safe_cons_p with (p' := p').
    - exact Hp.
    - apply safe_cons_q with (q' := q'); assumption.
  Qed.

  Lemma safe_seq_ex_weak (l: LSt) (p q r : LSt -> V) (c1 c2 c : LProg C) :
    (` c) = PSeq (`c1) (`c2) ->
    SSafeEx lsig l p c1 r ->
    SSafe   lsig   r c2 q ->
    SSafeEx lsig l p c q.
  Proof.
    revert p q r c1 c2 l c.
    cofix Hco.
    intros p q r c1 c2 l (c & Hc) Hseq Hsafe_c1 Hsafe_c2.
    cbn in *.
    subst.
    destruct_safe_and_transition.
    - (* Step *)
      destruct c' as (c' & Hc').
      cbn in *.
      inversion Hsafe_c1; subst.
      cbn in *.
      destruct (H1 lc (exist _ p' (proj1 Hc')) H3) as (v & Hv).
      exists v.
      intros l'.
      split.
      + apply Hv.
      + intro Hsem.
        refine (Hco v q r (exist _ p' (proj1 Hc')) c2 _ _ _ _ Hsafe_c2); intuition.
        apply Hv, Hsem.
    - (* Skip *)
      apply ltsafe_id_transition.
      apply SSafeEx_prf_irr with (c1 := c2); intuition.
      refine (safe_cons_p_ex _ (Hsafe_c2 l)).
      now apply safe_skip_ex, SSafeEx_prf_irr with (c1 := c1).
  Qed.

  Corollary safe_seq_ex (l: LSt) (p q r : LSt -> V) (c1 c2 : LProg C) :
   SSafeEx lsig l p c1 r ->
   SSafe   lsig   r c2 q ->
   SSafeEx lsig l p (LSeq c1 c2) q.
  Proof.
    now apply safe_seq_ex_weak.
  Qed.

  Corollary safe_seq (p q r : LSt -> V) (c1 c2 : LProg C):
    SSafe lsig p c1 q ->
    SSafe lsig q c2 r ->
    SSafe lsig p (LSeq c1 c2) r.
  Proof.
    intros Hsafe_c1 Hsafe_c2 l.
    apply (safe_seq_ex (Hsafe_c1 l) Hsafe_c2).
  Qed.

  Lemma safe_seq_skip_ex (l : LSt) (p q : LSt -> V) (c : LProg C) :
    SSafeEx lsig l p c q ->
    SSafeEx lsig l p (LSeq (LSkip C) c) q.
  Proof.
    intros Hsafe.
    cbn in *.
    subst.
    destruct_safe_and_transition.
    - now apply transition_skip in H3.
    - apply ltsafe_id_transition.
      now apply SSafeEx_prf_irr with (c1 := c).
  Qed.

  Lemma safe_seq_atom_ex (l : LSt) (p q r : LSt -> V) (a : C) (c : LProg C) :
    SSafeEx lsig l p (LAtom a) q ->
    (forall (l' : LSt), has_sem lsig l l' a -> SSafeEx lsig l' q c r) ->
    SSafeEx lsig l p (LSeq (LAtom a) c) r.
  Proof.
    intros Hsafe_a Hsafe_c.
    destruct_safe_and_transition.
    exists q.
    intros l'.
    (* Show that lc must be PAtom a. *)
    inversion H3.
    subst.
    split.
    - apply safe_atomic_ex, Hsafe_a.
    - intros (s & s' & Hhas_sem).
      apply SSafeEx_prf_irr with (c1 := LSeq (LSkip C) c); intuition.
      apply safe_seq_skip_ex, Hsafe_c.
      exists s, s'.
      apply Hhas_sem.
  Qed.

  Lemma safe_ndt_ex (l : LSt) (p q : LSt -> V) (c1 c2 : LProg C) :
    SSafeEx lsig l p c1 q ->
    SSafeEx lsig l p c2 q ->
    SSafeEx lsig l p (LNdt c1 c2) q.
  Proof.
    intros Hsafe_1 Hsafe_2.
    destruct_safe_and_transition;
      (* Splits into two cases, whose solutions are practically identical. *)
      apply ltsafe_id_transition; subst; eapply SSafeEx_prf_irr; eauto.
  Qed.

  Corollary safe_ndt (p q : LSt -> V) (c1 c2 : LProg C) :
    SSafe lsig p c1 q ->
    SSafe lsig p c2 q ->
    SSafe lsig p (LNdt c1 c2) q.
  Proof.
    intros Hl Hr l.
    now apply safe_ndt_ex.
  Qed.

  (* As per Safe_preloop in the original Views development *)

  Lemma safe_loop_step (p : LSt -> V) (c1 : LProg C) :
    SSafe lsig p c1 p ->
    (forall (l : LSt) (r : LSt -> V) (cl c2 : LProg C),
        ` cl = PSeq (` c2) (PIter (` c1)) ->
        SSafeEx lsig l r c2 p ->
        SSafeEx lsig l r cl p).
  Proof.
    intros Hsafe_c1.
    cofix Hco.
    intros l r (c2 & Hc2) cl Hloop Hsafe_c2.
    cbn in *.
    subst.
    destruct_safe_and_transition.
    - (* Step *)
      destruct c' as (c' & Hc').
      inversion Hsafe_c2; cbn in *; subst.
      destruct (H1 lc (exist _ p' (NoPar_transition H3 (proj2_sig cl))) H3) as (v & Hrest).
      exists v.
      intros l'.
      split.
      + apply Hrest.
      + intros Hsem.
        apply Hco with (c2 := (exist _ p' (NoPar_transition H3 (proj2_sig cl)))); intuition.
        apply Hrest, Hsem.
    - (* Skip *)
      exists p.
      intros l'.
      apply SSafeEx_prf_irr with (c2 := LSkip C) in Hsafe_c2; try easy.
      apply safe_skip_ex with (l' := l') in Hsafe_c2.
      refine (conj Hsafe_c2 _).
      intros Hhas_sem.
      destruct c' as (c' & Hc'); cbn in *; subst.
      destruct_safe_and_transition.
      * exists p.
        intros l''.
        refine (conj (lactionj_id_refl _ _ _ _) _).
        intros Hhas_sem'.
        now apply Hco with (c2 := c1), Hsafe_c1.
      * apply ltsafe_id_transition.
        apply SSafeEx_prf_irr with (c1 := LSkip C); try easy.
        apply safe_skip_strong.
  Qed.

  Lemma safe_loop (p : LSt -> V) (c1 : LProg C) :
    SSafe lsig p c1 p ->
    SSafe lsig p (LIter c1) p.
  Proof.
    intros Hsafe l.
    destruct_safe.
    intros lc c' Htrans.
    inversion Htrans; subst; apply ltsafe_id_transition.
    - apply safe_loop_step with (c1 := c1) (c2 := c1); try easy; apply Hsafe.
    - apply SSafeEx_prf_irr with (c1 := LSkip C); try easy.
      apply safe_skip_strong.
  Qed.

  (** [lvf_fstep] is the LVF equivalent of f-step preservation
      (Property M in the Views paper). *)

  Definition lvf_fstep (f : (LSt -> V) -> (LSt -> V)) :=
    forall (l l' : LSt) (p q : LSt -> V) (c : LabelC C),
    lactionj lsig c l l' p q ->
    lactionj lsig c l l' (f p) (f q).

  (** Framing with a constant frame is f-step preserving. *)

  Lemma lvf_fstep_cframe (f : V) :
    lvf_fstep (fun v => v * const f)%views.
  Proof.
    (* By the framing closure property. *)
    intros l l' p q c.
    apply lactionj_closed.
  Qed.

  (** Explicit generalised frame rule *)

  Lemma safe_genframe_ex (l : LSt) (p q : LSt -> V) (f : (LSt -> V) -> (LSt -> V)) (c : LProg C) :
    lvf_fstep f ->
    SSafeEx lsig l p     c  q ->
    SSafeEx lsig l (f p) c (f q).
  Proof.
    revert p q c l f.
    cofix Hco.
    intros p q c l f Hf Hsafe.
    destruct_safe.
    - intros Hc l'.
      apply Hf, safe_skip_ex.
      now apply SSafeEx_prf_irr with (c1 := c).
    - intros lc c' Htrans.
      inversion Hsafe; subst.
      destruct (H0 lc c' Htrans) as (r & Hr).
      exists (f r).
      intros l'.
      split.
      + apply Hf, Hr.
      + intro Hlsem.
        apply Hco, Hr, Hlsem.
        apply Hf.
  Qed.

  (** Implicit generalised frame rule *)

  Corollary safe_genframe (p q : LSt -> V) (f : (LSt -> V) -> (LSt -> V)) (c : LProg C) :
    lvf_fstep f ->
    SSafe lsig p     c q ->
    SSafe lsig (f p) c (f q).
  Proof.
    intro Hf.
    apply safe_ex_safe.
    intro l.
    apply safe_genframe_ex, Hf.
  Qed.

  (** Explicit constant frame rule *)

  Lemma safe_frame_ex (l : LSt) (p q : LSt -> V) (f : V) (c : LProg C):
    SSafeEx lsig l p                   c  q ->
    SSafeEx lsig l (p * const f)%views c (q * const f)%views.
  Proof.
    apply safe_genframe_ex with (f := fun v => (v * const f)%views), lvf_fstep_cframe.
  Qed.

  Corollary safe_frame (p q : LSt -> V) (f : V) (c : LProg C):
    SSafe lsig p                   c  q ->
    SSafe lsig (p * const f)%views c (q * const f)%views.
  Proof.
    apply safe_ex_safe.
    intro l.
    apply safe_frame_ex.
  Qed.
End LVF_safety_rules.