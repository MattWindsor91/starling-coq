From Coq Require Import ssreflect ssrfun ssrbool.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Chapter views_language.

Variable (C : Set).  (* Atomic actions *)

(** [LabelC] lifts [C] to include the id label. *)

Inductive LabelC : Set :=
| Id
| Cmd of C.

(** [label_maybe] lifts a command transformer [f] over [LabelC], given a
    default [d]. *)

Definition label_maybe {A: Type} (d: A) (f: C -> A) (lc: LabelC) : A :=
  if lc is Cmd c then f c else d.

Inductive Prog: Set :=
| PSkip
| PAtom of C
| PIter of Prog
| PSeq of Prog & Prog
| PNdt of Prog & Prog
| PPar of Prog & Prog.

Inductive Transition: Prog -> LabelC -> Prog -> Prop :=
| TAtom (c: C):
    Transition (PAtom c) (Cmd c) PSkip
| TSeqStep (p p' q: Prog) (lc: LabelC):
    Transition p lc p' ->
    Transition (PSeq p q) lc (PSeq p' q)
| TSeqSkip (p: Prog):
    Transition (PSeq PSkip p) Id p
| TParL (p p' q: Prog) (lc: LabelC):
    Transition p lc p' ->
    Transition (PPar p q) lc (PPar p' q)
| TParR (p q q': Prog) (lc: LabelC):
    Transition q lc q' ->
    Transition (PPar p q) lc (PPar p q')
| TParSync:
    Transition (PPar PSkip PSkip) Id PSkip
| TIterStep (p: Prog):
    Transition (PIter p) Id (PSeq p (PIter p))
| TIterSkip (p: Prog):
    Transition (PIter p) Id PSkip
| TNdtLeft (p q: Prog):
    Transition (PNdt p q) Id p
| TNdtRight (p q: Prog):
    Transition (PNdt p q) Id q.

Derive Inversion Transition_inv with (forall (p p': Prog) (lc : LabelC), Transition p lc p') Sort Prop.

(** [PSkip] can't transition out. *)

Lemma transition_skip (pr: Prog) (lc: LabelC):
  ~ Transition PSkip lc pr.
Proof.
  inversion 1.
Qed.

Lemma transition_seq (l r f: Prog) (lc: LabelC):
  Transition (PSeq l r) lc f ->
  (exists l', f = PSeq l' r /\ Transition l lc l') \/ (lc = Id /\ f = r /\ l = PSkip).
Proof.
  elim/Transition_inv => _ //; last by move=> r' [<-] -> _ ->; right.
    by move=> x y z lc0 Htrans [<-] -> _ _; left; exists y.
Qed.

(** [NoPar] states that a program has no parallel compositions.
    This is important for the LVF. *)

Fixpoint NoPar (pr : Prog) : Prop :=
  match pr with
  | PSkip | PAtom _ => True
  | PPar _ _ => False
  | PIter x => NoPar x
  | PSeq x y => NoPar x /\ NoPar y
  | PNdt x y => NoPar x /\ NoPar y
  end.

(** [NoPar] is closed under transition. *)

Lemma NoPar_transition (p1 p2 : Prog) (lc : LabelC) :
  Transition p1 lc p2 ->
  NoPar p1 ->
  NoPar p2.
Proof.
  elim=> //.
  - (* Sequence-step. *)
    move=> p p' q {lc}lc _ Hnp_p_step [Hnp_p Hnp_q] /=.
    move/Hnp_p_step in Hnp_p.
    exact: (conj Hnp_p Hnp_q).
  - (* Sequence-skip. *)
    by move=> p [_ Hnp_p].
  - (* Ndt-left. *)
    by move=> p q [Hnp_p _].
  - (* Ndt-right. *)
      by move=> p q [_ Hnp_q].
Qed.

Fixpoint noparb (pr : Prog) : bool :=
  match pr with
  | PSkip | PAtom _ => true
  | PPar _ _ => false
  | PIter x => noparb x
  | PSeq x y => noparb x && noparb y
  | PNdt x y => noparb x && noparb y
  end.

(** [NoPar] reflects [noparb]. *)

Lemma NoParP (pr : Prog): reflect (NoPar pr) (noparb pr).
Proof.
  apply/introP; (elim: pr => //= p Hp q Hq);
    by [ move/andP => [Hnp Hnq]; split; [apply/Hp | apply/Hq]
       | move/nandP => [Hnp|Hnq] [Hap Haq]; [apply/Hp/Hap/Hnp | apply/Hq/Haq/Hnq]
       | move=> *
       ].
Qed.
End views_language.