(** * Common infrastructure: Signatures

    While signatures are primarily a CVF construct, we put them in the
    common area because of their usefulness to the LVF-to-CVF embedding. *)

From Coq Require Import
     Program.Basics
     Classes.SetoidTactics
     Classes.SetoidClass
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     Views.Classes
     Views.Frameworks.Common.Reifier.

Set Implicit Arguments.

Chapter framework_common_signatures.

Local Open Scope program_scope.

Variables
  (V   : Type)
  (C S : Set).

Context
  {SV : Setoid V}
  {VV : ViewsSemigroup V}.

Definition Semantics : Type :=
  C -> S -> Ensemble S.

Record Signature :=
  mk_Signature
    {
      sig_reifier : Reifier S (V := V);
      sig_sem     : Semantics
    }.

(** [sig_reify] is shorthand for the function part of [sig_reifier]. *)

Definition sig_reify : Signature -> V -> Ensemble S :=
  reify (SSt := S) ∘ sig_reifier.

End framework_common_signatures.