(** * Reifiers *)

From Coq Require Import
     Classes.RelationClasses
     Relations.Relation_Definitions
     Classes.SetoidClass     
     Sets.Ensembles.

From Starling Require Import
     Views.Classes.

Local Open Scope views_scope.

Set Implicit Arguments.

Chapter reifier.
(** ** Reification functions

    A reification function [Reifier] is some function from views
    to sets of states, such that equivalent views map to the same states. *)

Variables
  (V   : Type) (* Views carrier *)
  (SSt : Set). (* Shared state *)

Context
  {SV : Setoid V}
  {VV : ViewsSemigroup V}.  

Definition Reifier : Type :=
  { f: V -> Ensemble SSt | Proper (equiv ==> Same_set SSt) f }.

Definition reify (r : Reifier) : V -> SSt -> Prop :=
  proj1_sig r.

Section reifier_inc.
  Variable reifier : Reifier.

  Context
    {OV : OrderedViewsSemigroup V}.

  Definition reifier_inc := Proper (inc --> Included SSt) (proj1_sig reifier).

  (** If a reifier is closed under inclusion, [p * r] entails [p] and [r]. *)
    
  Definition reifier_conj :=
    forall (p q : V) (s : SSt),
      reify reifier (p * q) s ->
      reify reifier p s /\ reify reifier q s.
    
  Lemma reifier_inc_conj:
    reifier_inc -> reifier_conj.
  Proof.
    intros Hinc a b e Hab.
    split; eapply Hinc; try exact Hab.
    - apply inc_dot_mono_left.
    - apply inc_dot_mono_right.
  Qed.

  (** [reifier_inc] can be generalised under an arbitrary frame. *)

  Corollary reifier_inc_frame_l (p q v : V) (s : SSt) :
    reifier_inc -> p <<= q -> reify reifier (q * v) s -> reify reifier (p * v) s.
  Proof.
    (* If [inc p q], then the same holds after framing with [v]. *)
    intros Hreifier_inc Hinc_pq Hreify_qv.
    refine (Hreifier_inc (q * v) _ _ _ Hreify_qv).
    apply inc_dot_left, Hinc_pq.
  Qed.
  
  (** Inclusion-closed [Reifier]s are closed under [dot]... *)

  Lemma reify_dot_l :
    reifier_inc ->
    forall (a b : V) (s : SSt), reify reifier (a * b) s -> reify reifier a s.
  Proof.
    intros Hinc a b.
    apply (Hinc (a * b) a (inc_dot_mono_left a b)).
  Qed.

  Lemma reify_dot_r :
    reifier_inc ->
    forall (a b : V) (s : SSt), reify reifier (a * b) s -> reify reifier b s.
  Proof.
    intros Hinc a b.
    apply (Hinc (a * b) b (inc_dot_mono_right b a)).
  Qed.

  (** ...and, in the other direction, minus. *)

  Lemma reify_sub {AV : AdjView V} :
    reifier_inc ->
    forall (a b : V) (s : SSt), reify reifier a s -> reify reifier (a \ b) s.
  Proof.
    intros Hinc a b.
    apply (Hinc a (a \ b) (sub_inc a b)).
  Qed.
End reifier_inc.

End reifier.