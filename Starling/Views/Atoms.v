(** * View atoms *)

Require Import
        Coq.Strings.String.

Require Import
        Starling.Expr.


(** ** Abstract predicates *)

Record APred (V : Type) : Type := mkAPred
  { apName : string;
    apParams : list V }.

Delimit Scope apred_scope with apred.
Local Open Scope apred_scope.

Notation "A {*  xs }" := (mkAPred _ A xs) (at level 0) : apred_scope.

Notation "A { x , .. , y }" := A {*(cons x .. (cons y nil) ..)}
          (at level 0) : apred.


(* TODO: Surely there must be some easier way of stating these! *)

Definition APred_eq_dec {V : Type} :
  (forall xv yv : V, {xv = yv} + {xv <> yv})
  -> forall x y : APred V, {x = y} + {x <> y}.
Proof.
  decide equality.
  - apply (List.list_eq_dec X).
  - apply string_dec.
Qed.

(** An abstract predicate constructed from the parts of another abstract
    predicate is equal to said abstract predicate. *)

Lemma APred_constr {A : Type} (a : APred A) :
  a = (apName A a){* apParams A a }.
Proof.
  case_eq a.
  intros name params.
  unfold apName, apParams.
  reflexivity.
Qed.

(** We can [Eval] inside abstract predicates, so long as their internal
    values are [Eval]able. *)

Definition APred_intr {A C E : Type} `{ EA: Eval (list A) (list C) E }
           (c : APred C) : APred A :=
  (apName C c){* intr (apParams C c)}.

Definition APred_eval {A C E : Type} `{ EA : Eval (list A) (list C) E }
  (e : E)
  (a : APred A)
  : option (APred C) :=
  option_map (mkAPred C (apName A a))
             (eval e (apParams A a)).

Instance APred_Eval (A C E : Type) `{ EA : Eval (list A) (list C) E } :
  Eval (APred A) (APred C) E :=
  { intr := APred_intr ;
    eval := APred_eval }.
Proof.
  intros e v.
  unfold APred_eval, APred_intr.
  (* Carefully peel off some of the record to get to the params. *)
  unfold apName at 1, apParams at 1.
  rewrite (intr_eval e (apParams C v)).
  unfold option_map.
  f_equal.
  rewrite APred_constr.
  reflexivity.
Qed.
