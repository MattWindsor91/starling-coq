(** * Lifting views to functions

    Function views are a special case of dependent-product views, so this
    development re-exports the [DProd] instances with a more intuitive
    type. *)

From Coq Require Import
     Relations.Relation_Definitions
     Classes.RelationClasses
     Classes.SetoidClass
     Program.Basics.

From Starling Require Import
     Views.Classes
     Views.Transformers.DProd.

Section instances_fun.
  Local Open Scope views_scope.
  
  Context {I V: Type}.

  Section instances_fun_Setoid.
    Context {SV: Setoid V}.

    (** If [V] is a Setoid, then the function is a Setoid. *)
    
    Global Instance fun_Setoid : Setoid (I -> V) := DProd_Setoid.
  End instances_fun_Setoid.

  Section instances_fun_ViewsSemigroup.
    Context
      {S: Setoid V}
      {VS: ViewsSemigroup V}.

    Global Instance fun_ViewsSemigroup: ViewsSemigroup (I -> V) := DProd_ViewsSemigroup.
  End instances_fun_ViewsSemigroup.
  
  Section instances_fun_ViewsMonoid.
    Context
      {S: Setoid V}
      {VS: ViewsSemigroup V}
      {VM: ViewsMonoid V}.
    
    Global Instance fun_ViewsMonoid : ViewsMonoid (I -> V) := DProd_ViewsMonoid.
  End instances_fun_ViewsMonoid.

  Section instances_fun_ZeroViewsSemigroup.
    Context
      {S: Setoid V}
      {VS: ViewsSemigroup V}
      {VZ: ZeroViewsSemigroup V}.
    
    Global Instance fun_ZeroViewsSemigroup : ZeroViewsSemigroup (I -> V) := DProd_ZeroViewsSemigroup.
  End instances_fun_ZeroViewsSemigroup.
  
  Section instances_fun_OrderedViewsSemigroup.
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}.
    
    Global Instance fun_OrderedViewsSemigroup: OrderedViewsSemigroup (I -> V) :=
      DProd_OrderedViewsSemigroup.
  End instances_fun_OrderedViewsSemigroup.

  Section instances_fun_AdjView.
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V}.
    
    Global Instance fun_AdjView : AdjView (I -> V) := DProd_AdjView.
  End instances_fun_AdjView.

  Section instances_fun_FullView.
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V}
      {FV: FullView V}.

    Global Instance fun_FullView : FullView (I -> V) := DProd_FullView.
  End instances_fun_FullView.
End instances_fun.

Section fview_facts.

  Local Open Scope views_scope.

  (** ** Facts about functional views

   This section contains useful facts about functional views. *)
  
  Variable (V : Type).

  Context
    {I : Type}
    {SV : Setoid V}
    {VV : ViewsSemigroup V}.

  (** We can express the dependent product [1] as [const 1] when using
      functional views. *)

  Lemma fview_const_one {VM : ViewsMonoid V} :
    (const 1 : I -> V) == 1.
  Proof.
    reflexivity.
  Qed.
  
  (** [const] distributes across [*]. *)

  Lemma fview_dot_const_dist (p q : V):
    ((@const V I p * const q) == const (p * q))%views.
  Proof.
    reflexivity.
  Qed.

  Lemma fview_dot_const_intr (p q : V) (i : I):
    (p * q == (@const V I p * const q) i)%views.
  Proof.
    reflexivity.
  Qed.

  Lemma fview_dot_const_intr_left (p q : I -> V) (i : I):
    ((p * q) i == (const (p i) * q) i)%views.
  Proof.
    reflexivity.
  Qed.

  Lemma fview_dot_const_intr_right (p q : I -> V) (i : I):
    ((p * q) i == (p * const q i) i)%views.
  Proof.
    reflexivity.
  Qed.
End fview_facts.
