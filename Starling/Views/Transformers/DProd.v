(** * Combining views through dependent products *)

From Coq Require Import
     Relations.Relations
     Classes.Morphisms
     Classes.RelationClasses
     Classes.RelationPairs
     Classes.SetoidClass.

From Starling Require Import
     Views.Classes.

Section instances_DProd.
  Local Open Scope views_scope.

  Context {I: Type} {V: I -> Type}.

  Section lift_rel.
    Variable (r : forall i: I, relation (V i)).

    (** If [r] is reflexive for all [i], so is the lifting. *)

    Global Instance lift_rel_DProd_Reflexive
      {R: forall i: I, Reflexive (r i)}:
      Reflexive (forall_relation r).
    Proof.
      intros x i.
      reflexivity.
    Defined.

    (** If [r] is symmetric for all [i], so is the lifting. *)

    Global Instance lift_rel_DProd_Symmetric
      {S: forall i: I, Symmetric (r i)}:
      Symmetric (forall_relation r).
    Proof.
      intros x y Hxy i.
      symmetry.
      apply Hxy.
    Defined.

    (** If [r] is transitive for all [i], so is the lifting. *)

    Global Instance lift_rel_DProd_Transitive
      {T: forall i: I, Transitive (r i)}:
      Transitive (forall_relation r).
    Proof.
      intros x y z Hxy Hyz k.
      unfold forall_relation in *.
      specialize Hxy with k.
      specialize Hyz with k.
      transitivity (y k); assumption.
    Defined.
  End lift_rel.

  Section instances_DProd_Setoid.

    (** If [V i] is a Setoid for all [i], then the dependent product is a
        Setoid. *)

    Context {SV: forall i: I, Setoid (V i)}.

    Definition equiv_DProd: relation (forall i: I, V i) :=
      forall_relation (fun i => equiv).

    (** Since [equiv_DProd] is a [lift_rel_DProd], this is trivial. *)

    Global Instance equiv_DProd_Equivalence : Equivalence equiv_DProd := {}.

    Global Instance DProd_Setoid : Setoid (forall i: I, V i) :=
      {
        equiv := equiv_DProd
      }.
  End instances_DProd_Setoid.

  Section instances_DProd_PreOrder.

    (** If [V i] is a pre-ordered setoid for all [i], then so is the dependent
        product. *)

    Context
      {SV: forall i: I, Setoid (V i)}
      {VV: forall i: I, ViewsSemigroup (V i)}
      {OV: forall i: I, OrderedViewsSemigroup (V i)}.    

    Definition inc_DProd: relation (forall i: I, V i) :=
      forall_relation (fun i => inc).

    (** Since [inc_DProd] is a [lift_rel_DProd], this is trivial. *)

    Global Instance inc_DProd_PreOrder : PreOrder inc_DProd := {}.

    Global Instance inc_fun_PartialOrder : PartialOrder equiv_DProd inc_DProd.
    Proof.
      intros i1 i2.
      split.
      - intro Heqv.
        split.
        + intro x.
          apply equiv_inc, Heqv.
        + intro x.
          apply equiv_inc, symmetry, Heqv.
      - intros (Hinc_12 & Hinc_21) x.
        specialize (Hinc_12 x).
        specialize (Hinc_21 x).        
        apply inc_antisymm; assumption.
    Defined.
  End instances_DProd_PreOrder.

  Section lift_op.

    (** ** Lifting closed operators over dependent products.

        Doing so gives us an array of interesting facts about how the
        lifted operators behave over setoids.

        We define this after [Setoid] and [PreOrdered] instances, because
        we make use of them. *)
    
    Variable op: forall i, V i -> V i -> V i.
    
    Definition lift_op (x y: forall i: I, V i): forall i: I, V i :=
      fun i => op i (x i) (y i).

    Section lift_op_Setoid.
      (** Facts for op lifting when dealing with setoids. *)
      
      Context {S: forall i, Setoid (V i)}.
      Let lop := lift_op.
    
      (** If a liftable operation is associative, so is the lifting. *)
      
      Lemma DProd_op_assoc:
        (forall i, forall a b c: V i, op i (op i a b) c == op i a (op i b c)) ->
        forall a b c: forall i: I, V i, lop (lop a b) c == lop a (lop b c).
      Proof.
        intros Hop a b c i.
        apply Hop.
      Qed.

      (** If a liftable operation is commutative, so is the lifting. *)

      Lemma DProd_op_comm:
        (forall i, forall a b: V i, op i a b == op i b a) ->
        forall a b: forall i: I, V i, lop a b == lop b a.
      Proof.
        intros Hop a b i.
        apply Hop.
      Qed.

      (** If a liftable operation is compatible, so is the lifting. *)

      Lemma DProd_equiv_op_left:
        (forall i, forall a b c: V i, a == b -> op i a c == op i b c) ->
        forall a b c: forall i: I, V i, a == b -> lop a c == lop b c.
      Proof.
        intros Hop a b c Hab_equiv i.
        apply Hop, Hab_equiv.
      Qed.      
    End lift_op_Setoid.

    Section lift_op_PreOrdered.
      (** Facts for op lifting when dealing with preordered setoids. *)
      
      Context
        {SV: forall i, Setoid (V i)}
        {VV: forall i, ViewsSemigroup (V i)}
        {OV: forall i, OrderedViewsSemigroup (V i)}.
      
      Let lop := lift_op.
    
      (** If a liftable operation is compatible, so is the lifting. *)

      Lemma DProd_inc_op_left:
        (forall i, forall a b c: V i, a <<= b -> op i a c <<= op i b c) ->
        forall a b c: forall i: I, V i, inc_DProd a b -> inc_DProd (lop a c) (lop b c).
      Proof.
        intros Hop a b c Hab_inc i.
        apply Hop, Hab_inc.
      Qed.      
    End lift_op_PreOrdered.
  End lift_op.

  Section instances_DProd_ViewsSemigroup.
    Context
      {S: forall i: I, Setoid (V i)}
      {VS: forall i: I, ViewsSemigroup (V i)}.

    Let idot : forall i: I, V i -> V i -> V i := fun i => dot.
    Definition dot_DProd := lift_op idot.

    Global Instance DProd_ViewsSemigroup: ViewsSemigroup (forall i: I, V i) :=
      {
        dot := dot_DProd;

        dot_assoc := DProd_op_assoc idot (fun i => dot_assoc);
        dot_comm := DProd_op_comm idot (fun i => dot_comm);
        equiv_dot_left := DProd_equiv_op_left idot (fun i => equiv_dot_left)
      }.
  End instances_DProd_ViewsSemigroup.
  
  Section instances_DProd_ViewsMonoid.
    Context
      {S: forall i: I, Setoid (V i)}
      {VS: forall i: I, ViewsSemigroup (V i)}
      {VM: forall i: I, ViewsMonoid (V i)}.

    (** [one_DProd] is the unit for dependent products.

        (Ideally, this would be [const one], but that would restrict
        [V i] to be the same type for all [i].  When we use DProd for function
        views, we show that the two are equivalent. *)
    
    Definition one_DProd : forall i: I, V i :=
      fun i => one.

    Lemma DProd_dot_one_l (x : forall i: I, V i) :
      (one_DProd * x) == x.
    Proof.
      intro i.
      apply dot_one_l.
    Qed.
    
    Global Instance DProd_ViewsMonoid : ViewsMonoid (forall i: I, V i) :=
      {
        one       := one_DProd;
        
        dot_one_l := DProd_dot_one_l
      }.
  End instances_DProd_ViewsMonoid.

  Section instances_DProd_ZeroViewsSemigroup.
    Context
      {S: forall i: I, Setoid (V i)}
      {VS: forall i: I, ViewsSemigroup (V i)}
      {VZ: forall i: I, ZeroViewsSemigroup (V i)}.

    (** [zero_DProd] is empty for dependent products.

        (Ideally, this would be [const zero], but that would restrict
        [V i] to be the same type for all [i].  When we use DProd for function
        views, we show that the two are equivalent. *)
    
    Definition zero_DProd : forall i: I, V i :=
      fun i => zero.

    Lemma DProd_dot_zero_l (x : forall i: I, V i) :
      (zero_DProd * x) == zero_DProd.
    Proof.
      intro i.
      apply dot_zero_left.
    Qed.
    
    Global Instance DProd_ZeroViewsSemigroup : ZeroViewsSemigroup (forall i: I, V i) :=
      {
        zero          := zero_DProd;
        
        dot_zero_left := DProd_dot_zero_l
      }.
  End instances_DProd_ZeroViewsSemigroup.
  
  Section instances_DProd_OrderedViewsSemigroup.
    Context
      {SV: forall i: I, Setoid (V i)}
      {VV: forall i: I, ViewsSemigroup (V i)}
      {OV: forall i: I, OrderedViewsSemigroup (V i)}.

    Let idot : forall i: I, V i -> V i -> V i := fun i => dot.

    Lemma DProd_inc_dot_mono_left (a b: forall i: I, V i):
      inc_DProd a (a * b).
    Proof.
      intro i.
      apply inc_dot_mono_left.
    Qed.
    
    Global Instance DProd_OrderedViewsSemigroup: OrderedViewsSemigroup (forall i: I, V i) :=
      {
        inc := inc_DProd;

        inc_dot_left := DProd_inc_op_left idot (fun i => inc_dot_left);
        inc_dot_mono_left := DProd_inc_dot_mono_left
      }.
  End instances_DProd_OrderedViewsSemigroup.

  Section instances_DProd_AdjView.
    Context
      {SV: forall i: I, Setoid (V i)}
      {VV: forall i: I, ViewsSemigroup (V i)}
      {OV: forall i: I, OrderedViewsSemigroup (V i)}
      {AV: forall i: I, AdjView (V i)}.

    Let isub : forall i: I, V i -> V i -> V i := fun i => sub.
    Definition sub_DProd := lift_op isub.
    
    Lemma DProd_sub_adjoint (x y z: forall i: I, V i):
      x <<= (y * z) -> (sub_DProd x y) <<= z.
    Proof.
      intros Hxyz_inc i.
      apply sub_adjoint, Hxyz_inc.
    Qed.

    Global Instance DProd_AdjView : AdjView (forall i: I, V i) :=
      {
        sub := sub_DProd;

        inc_sub_congr := DProd_inc_op_left isub (fun i => inc_sub_congr);
        sub_adjoint := DProd_sub_adjoint;
      }.
  End instances_DProd_AdjView.

  Section instances_DProd_FullView.
    Context
      {SV: forall i: I, Setoid (V i)}
      {VV: forall i: I, ViewsSemigroup (V i)}
      {OV: forall i: I, OrderedViewsSemigroup (V i)}
      {AV: forall i: I, AdjView (V i)}
      {FV: forall i: I, FullView (V i)}.      
    
    Lemma DProd_sub_adjoint_back (a b c: forall i : I, V i) :
      a \ b <<= c -> a <<= b * c.
    Proof.
      intros Hxyz_inc i.
      apply sub_adjoint_back, Hxyz_inc.
    Qed.

    Lemma DProd_dot_sub_cancel_inc_back (a b : forall i : I, V i) :
      a <<= (a * b) \ b.
    Proof.
      intro i.
      apply dot_sub_cancel_inc_back.
    Qed.

    Lemma DProd_sub_of_dot (a b c : forall i : I, V i) :
      ((a * b) \ c) == (a \ c) * (b \ (c \ a)).
    Proof.
      intro i.
      apply sub_of_dot.
    Qed.

    Global Instance DProd_FullView : FullView (forall i: I, V i) :=
      {
        sub_adjoint_back := DProd_sub_adjoint_back;
        dot_sub_cancel_inc_back := DProd_dot_sub_cancel_inc_back;
        sub_of_dot := DProd_sub_of_dot;
      }.
  End instances_DProd_FullView.   
End instances_DProd.
