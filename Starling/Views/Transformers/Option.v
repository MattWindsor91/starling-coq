(** * Views algebra instances for [option] *)

From Coq Require Import
     Classes.SetoidClass.

From Starling Require Import
     Utils.Option.Facts
     Views.Classes.

Section option_instances.

  Context
    { V : Type }.

  (** Setoid instances are in [Utils.Option.Facts]. *)

  Section views_semigroup.

    Context
      {SV : Setoid V}
      {VV : ViewsSemigroup V}.

    Global Instance option_ViewsSemigroup : ViewsSemigroup (option V) :=
      {
        dot := map2 dot
      }.
    Proof.
      - (* Associativity *)
        destruct x, y, z; try easy.
        apply dot_assoc.
      - (* Commutativity *)
        destruct x, y; try easy.
        apply dot_comm.
      - (* equiv_dot_left *)
        destruct a, b, c; try easy.
        cbn.
        apply equiv_dot_left.
    Defined.
    
  End views_semigroup.

  Section views_monoid.

    Context
      {SV : Setoid V}
      {VV : ViewsSemigroup V}
      {MV : ViewsMonoid V}.

    Global Instance option_ViewsMonoid : ViewsMonoid (option V) :=
      {
        one := Some one
      }.
    Proof.
      - (* dot_one_l *)
        destruct x; try easy.
        cbn.
        apply dot_one_l.
    Defined.
    
  End views_monoid.

  Section zero.

    Context
      {SV : Setoid V}
      {VV : ViewsSemigroup V}.

    Global Instance option_ZeroViewsSemigroup : ZeroViewsSemigroup (option V) :=
      {
        zero := None
      }.
    Proof.
      - (* dot_zero_l *)
        firstorder.
    Defined.
    
  End zero.
  
  Section ordered.

    Context
      {SV : Setoid V}
      {VV : ViewsSemigroup V}
      {OV : OrderedViewsSemigroup V}.      

    Definition option_inc (x y : option V) : Prop :=
      match x, y with
      | Some x', Some y' => inc x' y'
      | (* [None] is always included in another view. *)
        _, None => True
      | _, _ => False
      end.

    Global Instance option_inc_Reflexive : Reflexive option_inc.
    Proof.
      intros [|]; now cbn.
    Defined.

    Global Instance option_inc_Transitive : Transitive option_inc.
    Proof.
      intros [|] [|] [|]; cbn; intuition.
      now rewrite H.
    Defined.    

    Global Instance option_equiv_PreOrder : PreOrder option_inc := {}.

    Global Instance option_equiv_PartialOrder : PartialOrder option_equiv option_inc.
    Proof.
      intros [|] [|]; cbn; intuition.
      - now apply equiv_inc.
      - now apply equiv_inc.
      - now apply inc_antisymm.
    Defined.
    
    Global Instance option_OrderedViewsSemigroup : OrderedViewsSemigroup (option V) :=
      {
        inc := option_inc
      }.
    Proof.
      - (* inc_dot_left *)
        destruct a, b, c; try easy.
        apply inc_dot_left.
      - (* inc_dot_mono_left *)
        destruct a, b; try easy.
        apply inc_dot_mono_left.
    Defined.
    
  End ordered.

  Section subtractive.
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {MV: ViewsMonoid V}      
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V}.

    Definition msub (a b : option V) :=
      match a, b with
      | Some a, Some b => Some (sub a b)
      | _, None => one
      | None, Some a => None
      end.
    
    Global Instance option_AdjView : AdjView (option V) :=
      {
        sub := msub
      }.
    Proof.
      - (* inc_sub_congr *)
        destruct a, b, c; try easy.
        cbn.
        apply inc_sub_congr.
      - (* sub_adjoint *)
        destruct a, b, c; try easy; cbn.
        + apply sub_adjoint.
        + intros _.
          apply one_inc.
        + intros _.
          apply one_inc.
    Defined.
  End subtractive.

  Section separating.
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {MV: ViewsMonoid V}      
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V}
      {FV: FullView V}.

    Local Open Scope views_scope.

    Lemma option_sub_adjoint_back (a b c: option V) :
      a \ b <<= c -> a <<= b * c.
    Proof.
      destruct a, b, c; try easy; cbn.
      apply sub_adjoint_back.
    Qed.

    Lemma option_dot_sub_cancel_inc_back (a b : option V) :
      b <> None -> a <<= (a * b) \ b.
    Proof.
      intro Hb.
      destruct a, b; try easy; cbn.
      apply dot_sub_cancel_inc_back.
    Qed.

    Lemma option_sub_of_dot (a b c : option V) :
      c <> None -> ((a * b) \ c) == (a \ c) * (b \ (c \ a)).
    Proof.
      intro Hc.
      destruct a, b, c; try easy; cbn.
      apply sub_of_dot.
    Qed.
  End separating.  
End option_instances.