(** * Combining views through Cartesian products *)

From Coq Require Import
     Relations.Relation_Definitions
     Classes.RelationClasses
     Classes.RelationPairs     
     Classes.SetoidClass.

From Starling Require Import
     Utils.Product
     Views.Classes.

Section instances_prod.
  Local Open Scope views_scope.

  Context {A B : Type}.

  (** First, some helpers. *)

  Section instances_prod_Setoid.
    Context
      {SA: Setoid A} {SB: Setoid B}.
    
    Global Instance prod_Setoid : Setoid (A * B) :=
      {
        equiv := RelProd equiv equiv
      }.
  End instances_prod_Setoid.
  
  Section instances_prod_ViewsSemigroup.
    Context
      {SA: Setoid A} {SB: Setoid B}
      {VSA: ViewsSemigroup A} {VSB: ViewsSemigroup B}.
    
    Definition dot_prod : (A * B) -> (A * B) -> (A * B) := prodmap2 dot dot.
    
    Lemma prod_dot_assoc (x y z : A * B) :
      (dot_prod (dot_prod x y) z) == (dot_prod x (dot_prod y z)).
    Proof.
      destruct x as (xa, xb), y as (ya, yb), z as (za, zb).
      split; apply dot_assoc.
    Qed.

    Lemma prod_dot_comm (x y : A * B) :
      dot_prod x y == dot_prod y x.
    Proof.
      destruct x as (xa, xb), y as (ya, yb).
      split; apply dot_comm.
    Qed.

    Lemma prod_equiv_dot_left (x y z : A * B) :
      x == y -> dot_prod x z == dot_prod y z.
    Proof.
      intro Hinc.
      destruct x as (xa, xb), y as (ya, yb), z as (za, zb).
      split; apply equiv_dot_left, Hinc.
    Qed.
    
    Global Instance prod_ViewsSemigroup : ViewsSemigroup (A * B) :=
      {
        dot := dot_prod;

        dot_assoc := prod_dot_assoc;
        dot_comm := prod_dot_comm;
        equiv_dot_left := prod_equiv_dot_left
      }.
  End instances_prod_ViewsSemigroup.

  Section instances_prod_ViewsMonoid.
    Context
      {SA: Setoid A} {SB: Setoid B}
      {VSA: ViewsSemigroup A} {VSB: ViewsSemigroup B}
      {VMA: ViewsMonoid A} {VMB: ViewsMonoid B}.

    Definition one_prod : A * B :=
      (one, one).
    
    Lemma prod_dot_one_l (x : A * B) :
      (dot_prod (one_prod) x) == x.
    Proof.
      destruct x as (xa, xb).
      split; apply dot_one_l.
    Qed.

    Global Instance prod_ViewsMonoid : ViewsMonoid (A * B) :=
      {
        one       := one_prod;
        
        dot_one_l := prod_dot_one_l
      }.
  End instances_prod_ViewsMonoid.

  Section instances_prod_OrderedViewsSemigroup.
    Context
      {SA: Setoid A} {SB: Setoid B}
      {VA: ViewsSemigroup A} {VB: ViewsSemigroup B}
      {OA: OrderedViewsSemigroup A} {OB: OrderedViewsSemigroup B}.

    Definition inc_prod : relation (A * B) :=
      RelProd inc inc.

    Global Instance inc_prod_PartialOrder : PartialOrder equiv inc_prod.
    Proof.
      intros (xa, xb) (ya, yb).
      split.
      - (* Equivalence implies inclusion *)
        intros (Heqv_a & Heqv_b).
        repeat split; apply inc_order; try assumption.
        + apply symmetry, Heqv_a.
        + apply symmetry, Heqv_b.
      - (* Inclusion implies equivalence *)
        intros ((Hinc_axy & Hinc_bxy) & (Hinc_ayx & Hinc_byx)).
        split; apply inc_antisymm; assumption.
    Qed.     
        
    Lemma prod_inc_dot_left (x y z: A * B):
      inc_prod x y -> inc_prod (x * z) (y * z).
    Proof.
      intro Hinc.
      destruct x as (xa, xb), y as (ya, yb), z as (za, zb).
      split; apply inc_dot_left, Hinc.
    Qed.

    Lemma prod_inc_dot_mono_left (x y: A * B):
      inc_prod x (x * y).
    Proof.
      destruct x as (xa, xb), y as (ya, yb).
      split; apply inc_dot_mono_left.
    Qed.

    Global Instance prod_OrderedViewsSemigroup: OrderedViewsSemigroup (A * B) :=
      {
        inc := inc_prod;
        
        inc_dot_left := prod_inc_dot_left;
        inc_dot_mono_left := prod_inc_dot_mono_left
      }.
  End instances_prod_OrderedViewsSemigroup.

  Section instances_prod_AdjView.
    Context
      {SA: Setoid A} {SB: Setoid B}
      {VA: ViewsSemigroup A} {VB: ViewsSemigroup B}
      {OA: OrderedViewsSemigroup A} {OB: OrderedViewsSemigroup B}      
      {AA: AdjView A} {AB: AdjView B}.

    (** We define pairwise [sub] and [inc] here. *)

    Definition sub_prod : (A * B) -> (A * B) -> (A * B) :=
      prodmap2 sub sub.
  
    Lemma prod_inc_sub_congr (x y z : A * B) :
      inc_prod x y -> inc_prod (sub_prod x z) (sub_prod y z).
    Proof.
      intro Hinc.
      destruct x as (xa, xb), y as (ya, yb), z as (za, zb).
      split; apply inc_sub_congr, Hinc.
    Qed.
        
    Lemma prod_sub_adjoint (x y z : A * B) :
      inc_prod x (dot_prod y z) -> inc_prod (sub_prod x y) z.
    Proof.
      intro Hinc.
      destruct x as (xa, xb), y as (ya, yb), z as (za, zb).
      split; apply sub_adjoint, Hinc.
    Qed.
    
    Global Instance prod_AdjView : AdjView (A * B) :=
      {
        sub := sub_prod;

        inc_sub_congr := prod_inc_sub_congr;
        sub_adjoint := prod_sub_adjoint;
      }.
  End instances_prod_AdjView.
End instances_prod.
