(* * Restricting views through subtyping *)

From Coq Require Import
     Relations.Relation_Definitions
     Classes.SetoidClass
     Classes.SetoidDec
     Sets.Ensembles.

From Starling Require Import
     Views.Classes.

Section instances_subtype.
  Local Open Scope views_scope.
  
  (** If we have an instance for a view [V], then we can derive an
      instance for the subtype [{ x : V | P x }], so long as several
      closure properties hold over [P]. *)
  
  Context
    {V : Type}
    {P : V -> Prop}.

  Section lift_rel.
    (** We can lift any relation on [V] to [{x: V | P x}], and properties
        of the original relation trivially carry over. *)

    Variable (r: relation V).
      
    Definition lift_rel_subtype: relation {x: V | P x} :=
      fun x y => r (proj1_sig x) (proj1_sig y).
      
    (** If [r] is reflexive, so is the lifting. *)

    Global Instance lift_rel_subtype_Reflexive {R: Reflexive r}:
      Reflexive lift_rel_subtype.
    Proof.
      intros x.
      unfold lift_rel_subtype.
      reflexivity.
    Qed.

    (** If [r] is symmetric, so is the lifting. *)
    
    Global Instance lift_rel_subtype_Symmetric {S: Symmetric r}:
      Symmetric lift_rel_subtype.
    Proof.
      intros x y Hxy_f.
      unfold lift_rel_subtype in *.
      apply symmetry, Hxy_f.
    Qed.

    (** If [r] is transitive, so is the lifting. *)
      
    Global Instance lift_rel_subtype_Transitive {T: Transitive r}:
      Transitive lift_rel_subtype.
    Proof.
      intros x y z Hxy_f Hyz_f.
      unfold lift_rel_subtype in *.
      transitivity (proj1_sig y); assumption.
    Qed.      
  End lift_rel.
  
  Section instances_subtype_Setoid.

    (** If [V] is a Setoid, so is any restriction of [V]. *)

    Context {S: Setoid V}.
    
    Definition equiv_subtype: relation {x: V | P x} :=
      lift_rel_subtype equiv.

    Global Instance equiv_subtype_Equivalence: Equivalence equiv_subtype := {}.

    Global Instance subtype_Setoid : Setoid { x : V | P x } :=
      {
        equiv := equiv_subtype
      }.

  End instances_subtype_Setoid.

  Section instances_subtype_EqDec.

    (** If [V] is a decidable Setoid, so is any restriction of [V].

        While this is implied by [V] being a [DecOrderedViewsSemigroup], we
        define it separately here to make it available to [V] when it isn't
        (but is [EqDec]). *)

    Context {S : Setoid V}
            {D : EqDec S}.
    
    Global Instance subtype_EqDec : EqDec subtype_Setoid := {}.
    Proof.
      intros (x & Hx) (y & Hy).
      cbn.
      apply SetoidDec.equiv_dec.
    Defined.
    
  End instances_subtype_EqDec.

  
  Section instances_subtype_PreOrder.

    (** Here, we define the lifted pre-order for subtypes. *)
        
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}.

    Definition inc_subtype: relation {x: V | P x} :=
      lift_rel_subtype inc.

    (** Since [inc_subtype] is a [lift_rel_subtype], this is trivial. *)

    Global Instance inc_subtype_PreOrder : PreOrder inc_subtype := {}.
  End instances_subtype_PreOrder.
  
  Section lift_op.

    (** ** Lifting closed operators over subtyping.

        We can lift operators over subtyping, so long as the result of
        applying the operator is still within the subtype (closure).
        Doing so gives us an array of interesting facts about how the
        lifted operators behave over setoids.

        We define this after [Setoid], as we make use of it. *)
    
    Variable op: V -> V -> V.
    Hypothesis Hop_closed: forall x y: V, P x -> P y -> P (op x y).
    
    Definition lift_op (x y: {x: V | P x}): {x: V | P x} :=
      let xV := proj1_sig x in
      let yV := proj1_sig y in
      exist P (op xV yV) (Hop_closed xV yV (proj2_sig x) (proj2_sig y)).

    Section lift_op_Setoid.
      (** Facts for op lifting when dealing with setoids. *)
      
      Context {S: Setoid V}.
      Let lop := lift_op.
    
      (** If a liftable operation is associative, so is the lifting. *)
      
      Lemma subtype_op_assoc:
        (forall (a b c: V), op (op a b) c == op a (op b c)) ->
        forall (a b c: {x: V | P x}), lop (lop a b) c == lop a (lop b c).
      Proof.
        intros Hop a b c.
        apply Hop.
      Qed.

      (** If a liftable operation is commutative, so is the lifting. *)

      Lemma subtype_op_comm:
        (forall a b: V, op a b == op b a) ->
        forall a b: {x: V | P x}, lop a b == lop b a.
      Proof.
        intros Hop a b.
        apply Hop.
      Qed.

      (** If a liftable operation is compatible, so is the lifting. *)

      Lemma subtype_equiv_op_left:
        (forall a b c: V, a == b -> op a c == op b c) ->
        forall a b c: {x: V | P x}, a == b -> lop a c == lop b c.
      Proof.
        intros Hop a b c.
        apply Hop.
      Qed.      
    End lift_op_Setoid.

    Section lift_op_Ordered.
      (** Facts for op lifting when dealing with ordered views semigroups. *)
      
      Context
        {SV: Setoid V}
        {VV: ViewsSemigroup V}
        {OV: OrderedViewsSemigroup V}.
      
      Let lop := lift_op.
    
      (** If a liftable operation is compatible, so is the lifting. *)

      Lemma subtype_inc_op_left:
        (forall a b c: V, a <<= b -> op a c <<= op b c) ->
        forall a b c: {x: V | P x}, inc_subtype a b -> inc_subtype (lop a c) (lop b c).
      Proof.
        intros Hop a b c Hab_inc.
        apply Hop, Hab_inc.
      Qed.      
    End lift_op_Ordered.    
  End lift_op.
  
  Section instances_subtype_ViewsSemigroup.
    Context
      {S: Setoid V}
      {VS: ViewsSemigroup V}.
    
    (** We must show that the subtype is closed under [dot]. *)
    Hypothesis Hdot_closed : (forall x y : V, P x -> P y -> P (x * y)).
    
    Definition dot_subtype := lift_op dot Hdot_closed.

    Global Instance subtype_ViewsSemigroup : ViewsSemigroup {x: V | P x} :=
      {
        dot := dot_subtype;

        dot_assoc := subtype_op_assoc dot Hdot_closed dot_assoc;
        dot_comm := subtype_op_comm dot Hdot_closed dot_comm;
        equiv_dot_left := subtype_equiv_op_left dot Hdot_closed equiv_dot_left
      }.
  End instances_subtype_ViewsSemigroup.

  Section instances_subtype_ViewsMonoid.
    Context
      {S: Setoid V}
      {VS: ViewsSemigroup V}
      {VM: ViewsMonoid V}.

    Hypothesis Hdot_closed: (forall x y : V, P x -> P y -> P (x * y)).
    Instance VSs: ViewsSemigroup {x : V | P x} := subtype_ViewsSemigroup Hdot_closed.
    
    (** We must show that [one] is part of the subtype. *)
    Hypothesis Hone: P one.

    Definition one_subtype : {x: V | P x} :=
      exist P one Hone.    

    Lemma subtype_dot_one_l (x : {x: V | P x}) :
      (one_subtype * x) == x.
    Proof.
      apply dot_one_l.
    Qed.
    
    Global Instance subtype_ViewsMonoid : ViewsMonoid {x: V | P x} :=
      {
        one       := one_subtype;
        
        dot_one_l := subtype_dot_one_l
      }.
  End instances_subtype_ViewsMonoid.

  Section instances_subtype_OrderedViewsSemigroup.
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}.

    Hypothesis Hdot_closed: (forall x y : V, P x -> P y -> P (x * y)).
    Instance VSps: ViewsSemigroup {x: V | P x} := subtype_ViewsSemigroup Hdot_closed.

    Lemma subtype_inc_dot_mono_left (a b: {x: V | P x}):
      inc_subtype a (a * b).
    Proof.
      simpl.
      apply inc_dot_mono_left.
    Qed.

    Instance subtype_inc_antisymm: PartialOrder equiv_subtype inc_subtype := {}.
    Proof.
      intros (x & Hx) (y & Hy).
      apply inc_order.
    Defined.

    Global Instance subtype_OrderedViewsSemigroup : OrderedViewsSemigroup {x: V | P x} :=
      {
        inc := inc_subtype;

        inc_dot_left := subtype_inc_op_left dot Hdot_closed inc_dot_left;
        inc_dot_mono_left := subtype_inc_dot_mono_left
      }.    
  End instances_subtype_OrderedViewsSemigroup.
  
  Section instances_subtype_AdjView.
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V}.

    (** We require [P] to be closed under subtraction.
        We also require the previous hypotheses from [ViewsMonoid]. *)

    Hypothesis Hsub_closed : forall x y : V, P x -> P y -> P (x \ y).
    
    Hypothesis Hdot_closed : (forall x y : V, P x -> P y -> P (x * y)).

    Instance ViewsSemigroup_Vsub : ViewsSemigroup {x: V | P x} :=
      subtype_ViewsSemigroup Hdot_closed.
    
    Definition sub_subtype (x y : {x: V | P x}) : {x: V | P x} :=
      let xV := proj1_sig x in
      let yV := proj1_sig y in
      exist P (xV \ yV) (Hsub_closed xV yV (proj2_sig x) (proj2_sig y)).
              
    Lemma subtype_sub_adjoint (x y z: {x: V | P x}) :
      x <<= y * z -> (sub_subtype x y) <<= z.
    Proof.
      simpl.
      apply sub_adjoint.
    Qed.
    
    Global Instance subtype_AdjView : AdjView ({x: V | P x}) :=
      {
        sub := sub_subtype;

        inc_sub_congr := subtype_inc_op_left sub Hsub_closed inc_sub_congr;
        sub_adjoint := subtype_sub_adjoint;
      }.
  End instances_subtype_AdjView.

  Section instances_subtype_FullView.
    Context
      {SV: Setoid V}
      {VV: ViewsSemigroup V}
      {OV: OrderedViewsSemigroup V}
      {AV: AdjView V}
      {FV: FullView V}.

    Hypothesis Hsub_closed : forall x y : V, P x -> P y -> P (x \ y).
    
    Hypothesis Hdot_closed : (forall x y : V, P x -> P y -> P (x * y)).

    Instance ViewsSemigroup_Fsub : ViewsSemigroup {x: V | P x} :=
      subtype_ViewsSemigroup Hdot_closed.

    Instance AdjView_Fsub : AdjView {x: V | P x} :=
      subtype_AdjView Hsub_closed Hdot_closed. 
    
    Global Instance subtype_FullView : FullView { x : V | P x } := {}.
    Proof.
      - (* sub_adjoint_back *)
        destruct a, b, c; cbn; apply sub_adjoint_back.
      - (* dot_sub_cancel_inc_back *)
        destruct a, b; cbn; apply dot_sub_cancel_inc_back.
      - (* sub_of_dot *)
        destruct a, b, c; cbn; apply sub_of_dot.
    Defined.
  End instances_subtype_FullView.
End instances_subtype.

Section zsep_to_sep.
  Variable V : Type.
  
  Context
    {SV: Setoid V}
    {VV: ViewsSemigroup V}
    {OV: OrderedViewsSemigroup V}
    {AV: AdjView V}
    {ZV: ZeroViewsSemigroup V}      
    {FV: ZeroSeparatingViewsSemigroup V}.

  Local Open Scope views_scope.

  Hypothesis vnz_sub_closed : forall x y : V, ~ x == 0 -> ~ y == 0 -> ~ (x \ y) == 0.
  Hypothesis vnz_dot_closed : forall x y : V, ~ x == 0 -> ~ y == 0 -> ~ (x * y) == 0.

  (** If we can prove that [v] is never [0], and that property is closed over
      [sub] and [dot], we can promote a zero-separating algebra to a separating
      one. *)
  
  Global Instance ZeroSeparatingViewsSemigroup_FullView :
    FullView { v : V | ~ v == 0 }
             (AV := subtype_AdjView vnz_sub_closed vnz_dot_closed) := {}.
  Proof.
    - (* sub_adjoint_back *)
      destruct a, b, c; cbn; apply z_sub_adjoint_back.
    - (* dot_sub_cancel_inc_back *)
      destruct a, b; cbn; now apply z_dot_sub_cancel_inc_back.
    - (* sub_of_dot *)
      destruct a, b, c; cbn; now apply z_sub_of_dot.
  Defined.
End zsep_to_sep.

Section dotclose.
  Variables
    (V: Type)
    (P: V -> Prop).

  Context
    {S: Setoid V}
    {VS: ViewsSemigroup V}.
  
  Inductive dotclose (v: V): Prop :=
  | pv: P v -> dotclose v
  | pd: (exists a b, v = (a * b)%views /\ dotclose a /\ dotclose b) -> dotclose v.

  Lemma dotclose_closed (a b: V): dotclose a -> dotclose b -> dotclose (a * b)%views.
  Proof.
    intros Hx Hy.
    apply pd.
    exists a.
    exists b.
    split.
    - reflexivity.
    - split; assumption.
  Qed.
  
  Global Instance dotclose_ViewsSemigroup: ViewsSemigroup {v: V | dotclose v} :=
    subtype_ViewsSemigroup dotclose_closed.
End dotclose.
