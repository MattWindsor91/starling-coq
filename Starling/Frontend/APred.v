(** * Abstract predicates *)

From Starling Require Export
     Frontend.APred.Core
     Frontend.APred.Flatten
     Frontend.APred.GDefiner
     Frontend.APred.GMatch
     Frontend.APred.Match
     Frontend.APred.MatchPermut
     Frontend.APred.Taggable
.