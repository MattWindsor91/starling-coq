(** * Syntactic definers *)

From Coq Require Import
     Bool.Bool
     Classes.DecidableClass
     Classes.SetoidClass
     Lists.List
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     ProofRules (* Temporary, until we move dreify elsewhere? *)     
     Utils.List.Facts
     Views.Classes
     Views.Frameworks.Common     
     Views.Frameworks.CVF.Core
     Backend.Classes.

Set Implicit Arguments.

(** A syntactic definition matches a view pattern to its predicate
    expression. *)

Record SynDefinition (VPat Ep: Type) :=
  mk_SynDefinition
    {
      sd_view: VPat;
      sd_expr: Ep
    }.

Definition SynDefiner (VPat Ep: Type) := list (SynDefinition VPat Ep).

(** [DefinitionMatch] is the class of view patterns, views, and predicate
    expressions that have a [matcher] function.

    [matcher] matches a view against a definition, returning [None] if the
    definition doesn't match, and [Some e], where [e] is the condition for the
    match, if it does. *)

Class DefinitionMatch (VPat V Ep : Type) :=
  mk_DefinitionMatch
    {
      matcher : V -> VPat -> option Ep;
    }.

Chapter starling_logic_theoryexprs_syndefiner.

Variables
  (V    : Type)
  (Ep   : Type)
  (GCtx : Type)
  (S    : Set)
.

Context
  {SS  : Setoid S}
  {SV  : Setoid V}
  {VV  : ViewsSemigroup V}
  {OV  : OrderedViewsSemigroup V}
  {DV  : DecOrderedViewsSemigroup V}  
  {P_Ep: PredEx Ep GCtx unit S}. (* TODO: generalise to LCtx *)

(** A syntactic definer [SynDefiner] is a list of syntactic definitions. *)

Definition def_matches (v: V) (n: SynDefinition V Ep): Prop :=
  n.(sd_view) == v.

Lemma def_matches_dec (v: V) (n: SynDefinition V Ep):
  {def_matches v n} + {~ def_matches v n}.
Proof.
  eapply equiv_dec.
  exact DV.
Qed.

Definition has_syn_def (v: V): SynDefiner V Ep -> Prop :=
  Exists (def_matches v).

Definition SynDefiningViews (d: SynDefiner V Ep) :=
  {v: V | has_syn_def v d}.

Definition sd_collect (f: V -> bool): SynDefiner V Ep -> Ep :=
  fold_right
    (fun n e =>
       if (f (n.(sd_view)))
       then pe_conj e n.(sd_expr)
       else e)
    pe_true.

Lemma sd_collect_cons (f: V -> bool) (d: SynDefinition V Ep) (ds: SynDefiner V Ep):
  sd_collect f (d::ds) =
  if (f (d.(sd_view)))
  then pe_conj (sd_collect f ds) d.(sd_expr)
  else sd_collect f ds.
Proof.
  reflexivity.
Qed.

Lemma sd_collect_strengthen (f g: V -> bool) (d: SynDefiner V Ep) (gc : GCtx) :
  (forall v, Bool.Is_true (f v) -> Bool.Is_true (g v)) ->
  Included S
           (pe_interp gc tt (sd_collect g d))
           (pe_interp gc tt (sd_collect f d)).
Proof.
  intros Hstr s.
  induction d.
  - intros _.
    apply pe_true_True.
  - unfold sd_collect, fold_right.
    destruct (f (sd_view a)) eqn:Hf.
    + rewrite (Is_true_eq_true _ (Hstr (sd_view a) (Is_true_eq_left _ Hf))).
      intros (Hin_d & Hin_a)%pe_conj_distr%Intersection_inv.
      apply pe_conj_distr.
      intuition.
    + destruct (g (sd_view a)) eqn:Hg.
      * intros (Hin_d & _)%pe_conj_distr%Intersection_inv.
        apply IHd, Hin_d.
      * apply IHd.
Qed.

Section syn_reify.
  Section syn_reify_defs.
    Variable v: V.
    
    Definition sd_syn_reify_expr: SynDefiner V Ep -> Ep :=
      sd_collect (fun u => decide (u <<= v)%views).
    
    Definition sd_syn_reify (gc : GCtx) : SynDefiner V Ep -> Ensemble S :=
      compose (pe_interp gc tt) sd_syn_reify_expr.
  End syn_reify_defs.
    
  Lemma sd_syn_reify_cons (v: V) (gc : GCtx) (d: SynDefinition V Ep) (ds: SynDefiner V Ep):
    Same_set S
             (sd_syn_reify v gc (d::ds))
             (if decide (d.(sd_view) <<= v)%views
              then Intersection S (sd_syn_reify v gc ds) (pe_interp gc tt d.(sd_expr))
              else sd_syn_reify v gc ds).
  Proof.
    unfold sd_syn_reify, compose, sd_syn_reify_expr.
    rewrite sd_collect_cons.
    decide (sd_view d <<= v)%views as Hdv.
    - apply pe_conj_distr.
    - reflexivity.
  Qed.

  Corollary sd_syn_reify_cons_inc (v: V) (gc : GCtx) (d: SynDefinition V Ep) (ds: SynDefiner V Ep):
    Included S
             (sd_syn_reify v gc (d::ds))
             (sd_syn_reify v gc ds).             
  Proof.
    intros s Hcons%sd_syn_reify_cons.
    decide (sd_view d <<= v)%views as Hdv.
    - destruct Hcons as [s Hds Hd].
      apply Hds.
    - apply Hcons.
  Qed.

  (** The reification of a view [v] is included inside the reification
      of any subview [u]. *)

  Lemma sd_syn_reify_view_inc (u v: V) (gc : GCtx) (ds: SynDefiner V Ep):
    (u <<= v)%views ->
    Included S
             (sd_syn_reify v gc ds)
             (sd_syn_reify u gc ds).
  Proof.
    intro Huv.
    apply sd_collect_strengthen.
    unfold decide.
    intros w Hfu%Is_true_eq_true%(Decidable_sound _ _).
    apply Is_true_eq_left, Decidable_complete.
    now transitivity u.
  Qed.
End syn_reify.

Section syn_define.
  Variable v: V.
  
  Definition sd_syn_define_expr: SynDefiner V Ep -> Ep :=
    sd_collect (fun u => decide (u == v)%views).
  
  Definition sd_syn_define (gc : GCtx) : SynDefiner V Ep -> Ensemble S :=
    compose (pe_interp gc tt) sd_syn_define_expr.

  Lemma sd_reify_define (gc : GCtx) (d: SynDefiner V Ep):
    Included S
             (sd_syn_reify v gc d)
             (sd_syn_define gc d).
  Proof.
    apply sd_collect_strengthen.
    intros u Heq%Is_true_eq_true.
    apply Decidable_sound in Heq.
    apply Is_true_eq_left, Decidable_complete, equiv_inc, Heq.
  Qed.

  Lemma sd_syn_define_cons (gc : GCtx) (d: SynDefinition V Ep) (ds: SynDefiner V Ep):
    Same_set S
             (sd_syn_define gc (d::ds))
             (if decide (d.(sd_view) == v)
              then Intersection S (sd_syn_define gc ds) (pe_interp gc tt d.(sd_expr))
              else sd_syn_define gc ds).
  Proof.
    unfold sd_syn_define, compose, sd_syn_define_expr.
    rewrite sd_collect_cons.
    decide (sd_view d == v) as Hdv.
    - apply pe_conj_distr.
    - reflexivity.
  Qed.

  Corollary sd_syn_define_cons_inc (gc : GCtx) (d: SynDefinition V Ep) (ds: SynDefiner V Ep):
    Included S
             (sd_syn_define gc (d::ds))
             (sd_syn_define gc ds).             
  Proof.
    intros s Hcons%sd_syn_define_cons.
    decide (sd_view d == v) as Hdv; try apply Intersection_inv in Hcons; intuition.
  Qed.
End syn_define.
  
(** The syntactic defining-views proof rule template *)

Section starling_logic_theoryexprs_syndefiner_template.
  Variable C: Set.

  Context {AV: AdjView V}.
  
  (** [sd_define] is a variant of [sd_syn_define] that is compatible with the
      semantic defining-views operators. *)

  Definition sd_define (gc : GCtx) (d: SynDefiner V Ep) (v: V): option (Ensemble S) :=
    if (Exists_dec (def_matches v) d (def_matches_dec v))
    then Some (sd_syn_define v gc d)
    else None.
      
  (** If a view [v] is a defining view for [sd_define d], it's a
      syntactic defining view for [d] too. *)

  Lemma has_def_has_syn_def (gc : GCtx) (d: SynDefiner V Ep) (v: V):
    has_def V S (sd_define gc d) v ->
    has_syn_def v d.
  Proof.
    intros (def & Hv).
    unfold sd_define in Hv.
    now destruct (Exists_dec (def_matches v) d (def_matches_dec v)).
  Qed.

  Corollary has_def_exists_syn_def (gc : GCtx) (d: SynDefiner V Ep) (v: V):
    has_def V S (sd_define gc d) v ->
    exists (df : SynDefinition _ _), List.In df d /\ def_matches v df.
  Proof.
    now intros Hd%has_def_has_syn_def%Exists_exists.
  Qed.
  
  Definition DefiningViews_SynDefiningViews
             (gc : GCtx)
             (d: SynDefiner V Ep) (v: DefiningViews V S (sd_define gc d)): SynDefiningViews d.
  Proof.
    destruct v as (v & Hv).
    exists v.
    apply has_def_has_syn_def with (gc := gc), Hv.
  Defined.

  Lemma sd_define_nil (gc : GCtx) (v: V):
    sd_define gc nil v = None.
  Proof.
    unfold sd_define.
    destruct (Exists_dec (def_matches v) nil (def_matches_dec v)).
    - apply Exists_nil in e.
      contradiction.
    - reflexivity.
  Qed.
    
  (** If a view is not in [sd_define], its [sd_syn_define] trivially holds. *)
    
  Lemma sd_define_none_syn_define (gc : GCtx) (d: SynDefiner V Ep) (v: V) (s: S):
    sd_define gc d v = None ->
    In S (sd_syn_define v gc d) s.
  Proof.
    intro Hnone.
    unfold sd_define in Hnone.
    destruct (Exists_dec (def_matches v) d (def_matches_dec v)).
    - discriminate.
    - apply Forall_Exists_neg in n.
      induction d.
      + apply pe_true_True.
      + unfold sd_syn_define, sd_syn_define_expr, compose.
        rewrite sd_collect_cons.
        pose (Hna0 := Forall_inv n).
        cbv beta in Hna0.
        unfold def_matches in Hna0.
        apply (Decidable_sound_alt _ _) in Hna0.
        unfold decide.
        rewrite Hna0.
        apply IHd.
        apply Forall_snoc in n.
        exact n.
  Qed.
    
  (** A state has a definition in [sd_define] if, and only if, it is in
      [sd_syn_define]. *)
  Lemma sd_define_syn_define (gc : GCtx) (d: SynDefiner V Ep) (v: V) (ss: Ensemble S):
    sd_define gc d v = Some ss ->
    Same_set S ss (sd_syn_define v gc d).
  Proof.
    intro Hsome.
    unfold sd_define in Hsome.
    destruct (Exists_dec (def_matches v) d (def_matches_dec v)).
    - inversion Hsome as [].
      reflexivity.
    - discriminate.
  Qed.

  (** [sd_syn_define] is equivalent to [dfn_lift]ing [sd_define]. *)

  Corollary sd_define_lift_syn_define (gc : GCtx) (d: SynDefiner V Ep) (v: V):
    Same_set S (dfn_lift V S (sd_define gc d) v) (sd_syn_define v gc d).
  Proof.
    unfold dfn_lift.
    destruct (sd_define gc d v) as [ss|] eqn:Hdef.
    - apply sd_define_syn_define, Hdef.
    - split; intros s Hin.
      + apply sd_define_none_syn_define, Hdef.
      + easy.
  Qed.

  Lemma sd_reify_cons_r (gc : GCtx) (d: SynDefinition V Ep) (ds: SynDefiner V Ep) (v: V):
    Included S
             (dreify V S (sd_define gc (d :: ds)) v)
             (dreify V S (sd_define gc ds) v).
  Proof.
    intros s Hdds u.
    eapply sd_define_lift_syn_define, sd_syn_define_cons_inc, sd_define_lift_syn_define, Hdds.
  Qed.

  Global Instance sd_syn_define_expr_eq_Proper
         (d: SynDefiner V Ep):
    Proper (equiv ==> eq) (fun v => sd_syn_define_expr v d).
  Proof.
    intros x y Hxy.
    unfold sd_syn_define_expr.
    induction d.
    - reflexivity.
    - rewrite ! sd_collect_cons.
      rewrite IHd.
      decide (sd_view a == x)%views; decide (sd_view a == y)%views;
        try reflexivity.
      + (* Contradictory parts *)
        setoid_rewrite Hxy in H.
        contradiction.
      + (* Contradictory parts *)
        setoid_rewrite Hxy in H.
        contradiction.
  Defined.  
  
  Global Instance sd_syn_define_equiv_Proper (gc : GCtx) (d : SynDefiner V Ep):
    Proper (equiv ==> Same_set S) (fun v => sd_syn_define v gc d).
  Proof.
    intros x y Hxy.
    unfold sd_syn_define, compose.
    rewrite (sd_syn_define_expr_eq_Proper d x) with (y := y).
    - reflexivity.
    - exact Hxy.
  Defined.
  
    (** A state is in [sd_reify] if, and only if, it is in [sd_syn_reify]. *)
    
    Lemma sd_reify_syn_reify (gc : GCtx) (ds: SynDefiner V Ep) (v: V):
      Same_set S
               (dreify V S (sd_define gc ds) v)
               (sd_syn_reify v gc ds).
    Proof.
      split; intros s Hin.
      - induction ds as [|d].
        + apply pe_true_True.
        + unfold sd_syn_reify, sd_syn_reify_expr, compose.
          rewrite sd_collect_cons.
          decide (sd_view d <<= v)%views as Ha0_v.
          * apply pe_conj_distr.
            split.
            -- eapply IHds, sd_reify_cons_r, Hin.
            -- specialize (Hin (exist _ (sd_view d) Ha0_v)).
               apply sd_define_lift_syn_define, sd_syn_define_cons in Hin.
               unfold proj1_sig in Hin.
               decide (sd_view d == sd_view d) as Hdd;
                 try apply Intersection_inv in Hin;
                 intuition.
          * eapply IHds, sd_reify_cons_r, Hin.
      - intros (u & Huv).
        apply (sd_syn_reify_view_inc u v gc ds Huv), sd_reify_define in Hin.
        apply sd_define_lift_syn_define, Hin.
    Qed.

    Global Instance sd_syn_reify_expr_eq_Proper
           (d: SynDefiner V Ep):
      Proper (equiv ==> eq) (fun v => sd_syn_reify_expr v d).
    Proof.
      intros x y Hxy.
      unfold sd_syn_reify_expr.
      induction d.
      - reflexivity.
      - rewrite ! sd_collect_cons.
        rewrite IHd.
        decide (sd_view a <<= x)%views; decide (sd_view a <<= y)%views;
          try reflexivity.
        + (* Contradictory parts *)
          setoid_rewrite Hxy in H.
          contradiction.
        + (* Contradictory parts *)
          setoid_rewrite Hxy in H.
          contradiction.
    Defined.
    
    Global Instance sd_syn_reify_equiv_Proper (gc : GCtx) (d: SynDefiner V Ep):
      Proper (equiv ==> Same_set S) (fun v => sd_syn_reify v gc d).
    Proof.
      intros x y Hxy.
      transitivity (dreify V S (sd_define gc d) x).
      {
        symmetry.
        apply sd_reify_syn_reify.
      }
      transitivity (dreify V S (sd_define gc d) y).
      {
        apply dreify_eqv, Hxy.
      }
      apply sd_reify_syn_reify.
    Qed.

    Definition sd_syn_reifier (gc : GCtx) (d: SynDefiner V Ep): Reifier S :=
      exist _ (fun v => sd_syn_reify v gc d) (sd_syn_reify_equiv_Proper gc d).
End starling_logic_theoryexprs_syndefiner_template.
End starling_logic_theoryexprs_syndefiner.

Infix "~>" := mk_SynDefinition (at level 99): syndefiner_scope.
