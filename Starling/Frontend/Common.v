(** * Class and results for Starling logic frontends

    A frontend, in the Starling world, is a function that decomposes
    atomic Hoare triples (eg from an outline decomposition) into
    backend verification conditions.  It has the property that there
    exists a Concurrent Views Framework instance and a decomposition
    into instance axioms such that the frontend is 'covering'.
 *)

From Coq Require Import
     Classes.SetoidClass
     Lists.List
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     ProgramProof
     ProofRules
     Backend.Classes
     Utils.List.Facts
     Views.Classes
     Views.Frameworks.Common     
     Views.Frameworks.CVF.Core.

Local Open Scope views_scope.
Local Open Scope outline_scope.
Local Open Scope program_scope.   

Section frontend_class.
  Definition list_to_set {T: Type}: list T -> Ensemble T :=
    flip (List.In (A := _)).

  Class Setlike (T : Type -> Type) :=
    mk_Setlike
    {
      to_set {A : Type}     : T A -> Ensemble A;
      in_setlike {A : Type} : T A -> A -> Prop;
      
      to_set_incl (A : Type) (ta : T A) (a : A) :
        in_setlike ta a <-> In _ (to_set ta) a;
    }.

  Global Instance Setlike_Ensemble : Setlike Ensemble :=
    {
      to_set _ := id;
      in_setlike := In;
    }.
  Proof.
    reflexivity.
  Defined.

  Global Instance Setlike_List : Setlike list :=
    {
      to_set _ := list_to_set;
      in_setlike A := flip (List.In (A := A));
    }.
  Proof.
    reflexivity.
  Defined.

  Lemma in_flat_map_set {T U : Type} (xs : list T) (f : T -> list U) (u : U) :
    List.In u (flat_map f xs) <->
    In _ (flat_map_to_set (list_to_set ∘ f) xs) u.
  Proof.
    split.
    - rewrite in_flat_map.
      intros (x & Hin_x & Hin_u).
      apply Exists_exists.
      exists x.
      split; assumption.
    - intro Hin.
      apply Exists_exists in Hin.
      destruct Hin as (x & Hin_x & Hin_u).
      rewrite in_flat_map.
      exists x.
      split; assumption.
  Qed.  

  Corollary in_flat_map_set_same {T U : Type} (xs : list T) (f : T -> list U) :
    Same_set _ (list_to_set (flat_map f xs))
               (flat_map_to_set (list_to_set ∘ f) xs).
  Proof.
    split; intro x; apply in_flat_map_set.
  Qed.  
  
  (** A backend decomposition [d] is a [covering] decomposition with
      respect to the Views instance [i] if the Hoare judgement over
      all of the backend VCs produced by decomposing a single frontend
      VC is sufficient to show that that all of the CVF axioms coming out
      of the VC are in the axiomatisation. *)

  Definition covering
             (Va    : Type) (* Assertion views *)
             (Vf    : Type) (* Framework views *)
             (Ep    : Type) (* Predicate expressions *)
             (Er    : Type) (* Relation expressions *)
             (LCtx  : Type) (* Local context *)
             (GCtx  : Type) (* Global context *)
             (Ca    : Set)  (* Assertion commands *)
             (Cf    : Set)  (* Framework commands *)
             (SSt   : Set)  (* Shared states *)
             {SSSt  : Setoid SSt}
             {SVf   : Setoid Vf}
             {P_Ep  : PredEx Ep GCtx LCtx SSt}
             {R_Er  : RelEx Er GCtx LCtx SSt}
             (i     : GCtx -> PreInstance Cf SSt)
             (b     : ViewsAxiom Va Ca -> Ensemble (BVCond Ep Er))
             (f     : ViewsAxiom Va Ca -> Ensemble (ViewsAxiom Vf Cf)) : Prop :=
    forall (ax : ViewsAxiom Va Ca) (g : GCtx),
      Included _ (b ax) (bvhoare g) ->
      Included _ (f ax) (i g).(v_axioms).

  (** If the view conditions map directly to CVF axioms, the covering
      requirement is simpler. *)

  Lemma covering_direct
        (V     : Type) (* Views *)
        (Ep    : Type) (* Predicate expressions *)
        (Er    : Type) (* Relation expressions *)
        (LCtx  : Type) (* Local context *)
        (GCtx  : Type) (* Global context *)
        (C     : Set)  (* Commands *)
        (SSt   : Set)  (* Shared states *)
        {SSSt  : Setoid SSt}        
        {SV    : Setoid V}
        {P_Ep  : PredEx Ep GCtx LCtx SSt}
        {R_Er  : RelEx Er GCtx LCtx SSt}
        (i     : GCtx -> PreInstance C SSt)
        (d     : ViewsAxiom V C -> Ensemble (BVCond Ep Er)) :
    (forall (ax : ViewsAxiom V C) (g : GCtx),
        (forall (b: BVCond Ep Er), In _ (d ax) b -> bvhoare g b) ->
        In _ (i g).(v_axioms) ax) ->
    covering _ _ _ _ _ _ _ _ _ i d (Singleton _).
  Proof.
    intros Hcovering_direct ax g Hin_decomp x <-%Singleton_inv.
    apply Hcovering_direct, Hin_decomp.
  Qed.

  Definition builder_to_template
             {Ep Er GCtx LCtx Va Vf : Type}
             {Ca Cf SSt : Set}
             {SSSt  : Setoid SSt}
             {SVf : Setoid Vf}             
             {P_Ep : PredEx Ep GCtx LCtx SSt}
             {R_Er : RelEx Er GCtx LCtx SSt}
             (b : ViewsAxiom Va Ca -> Ensemble (BVCond Ep Er))             
             (f : ViewsAxiom Va Ca -> Ensemble (ViewsAxiom Vf Cf))
             (g : GCtx) :
    Template Vf Cf SSt :=
    fun (s : Signature Cf SSt) (af : ViewsAxiom Vf Cf) =>
      exists (aa : ViewsAxiom Va Ca),
        In _ (f aa) af /\
        Included _ (b aa) (bvhoare g (LCtx := LCtx) (S := SSt)).
  
  Definition builder_to_instance
             {Ep Er GCtx LCtx Va Vf : Type}
             {Ca Cf SSt : Set}
             {SSSt  : Setoid SSt}
             {SVf : Setoid Vf}
             {P_Ep : PredEx Ep GCtx LCtx SSt}
             {R_Er : RelEx Er GCtx LCtx SSt}
             (b : ViewsAxiom Va Ca -> Ensemble (BVCond Ep Er))
             (f : ViewsAxiom Va Ca -> Ensemble (ViewsAxiom Vf Cf))
             (sig : GCtx -> Signature Cf SSt)
             (g : GCtx) :
    PreInstance Cf SSt :=
    instantiate Vf Cf SSt (sig g) (builder_to_template b f g).

  Lemma builder_to_template_covering
        {Ep Er GCtx LCtx Va Vf : Type}
        {Ca Cf SSt : Set}
        {SSSt  : Setoid SSt}
        {SVf : Setoid Vf}             
        {P_Ep : PredEx Ep GCtx LCtx SSt}
        {R_Er : RelEx Er GCtx LCtx SSt}             
        (b : ViewsAxiom Va Ca -> Ensemble (BVCond Ep Er))
        (f : ViewsAxiom Va Ca -> Ensemble (ViewsAxiom Vf Cf))
        (sig : GCtx -> Signature Cf SSt) :
    covering Va Vf Ep Er LCtx GCtx Ca Cf SSt
             (builder_to_instance b f sig)
             b
             f.
  Proof.
    intros ax g Hbh af Hin_f.
    exists ax.
    split; assumption.
  Qed.
    
  (** Class [Frontend] encapsulates a sound frontend for a given set of
      predicate and relation expressions. *)
  
  Class Frontend
        (Va    : Type)        (* Assertion views *)
        (Vf    : Type)        (* Framework views *)
        (Ep    : Type)        (* Predicate expressions *)
        (Er    : Type)        (* Relation expressions *)
        (LCtx  : Type)        (* Local context *)
        (GCtx  : Type)        (* Global context *)
        (Ca    : Set)         (* Assertion commands *)
        (Cf    : Set)         (* Framework commands *)
        (SSt   : Set)         (* Shared states *)
        (T     : Type -> Type) (* Carrier type *)
        {SSSt  : Setoid SSt}
        {SVf   : Setoid Vf}             
        {P_Ep  : PredEx Ep GCtx LCtx SSt}
        {R_Er  : RelEx Er GCtx LCtx SSt}
        {T_SLk : Setlike T} :=
    mk_Frontend
      {
        fr_instance       : GCtx -> PreInstance Cf SSt;
        fr_views_decomp   : ViewsAxiom Va Ca -> Ensemble (ViewsAxiom Vf Cf);
        fr_backend_decomp : ViewsAxiom Va Ca -> T (BVCond Ep Er);

        fr_covering       : covering _ _ _ _ _ _ _ _ _ fr_instance (to_set (A := BVCond Ep Er) ∘ fr_backend_decomp) fr_views_decomp
      }.

End frontend_class.

Section logic_helpers.
  Context
    {Va Vf Ep Er LCtx GCtx : Type}
    {Ca Cf SSt : Set}
    {T     : Type -> Type} (* Carrier type *)    
    {SSSt  : Setoid SSt}
    {SVa   : Setoid Va}
    {VVa   : ViewsSemigroup Va}
    {SVf   : Setoid Vf}
    {VVf   : ViewsSemigroup Vf}
    {P_Ep  : PredEx Ep GCtx LCtx SSt}
    {R_Er  : RelEx Er GCtx LCtx SSt}    
    {T_SLk : Setlike T}.

  Local Open Scope program_scope.  

  Definition fr_backend_decomp_set
             {B : Backend Ep Er GCtx LCtx SSt}
             {F : Frontend Va Vf Ep Er LCtx GCtx Ca Cf SSt T} :
    ViewsAxiom Va Ca -> Ensemble (BVCond Ep Er) :=
    to_set ∘ fr_backend_decomp.

  Definition fr_top_axioms
             {F : Frontend Va Vf Ep Er LCtx GCtx Ca Cf SSt T}
             (g : GCtx) :
    Ensemble (ViewsAxiom Va Ca) :=
    fun (ax : ViewsAxiom Va Ca) =>
      Included (ViewsAxiom Vf Cf)
               (fr_views_decomp ax)
               (fr_instance g).(v_axioms).
  
  Definition lg_action
             {B : Backend Ep Er GCtx LCtx SSt}
             {F : Frontend Va Vf Ep Er LCtx GCtx Ca Cf SSt T} : ViewsAxiom Va Ca -> Prop :=
    bi_solve ∘ fr_backend_decomp_set.

  
  Definition lg_safe
             {B : Backend Ep Er GCtx LCtx SSt}
             {F : Frontend Va Vf Ep Er LCtx GCtx Ca Cf SSt T} : Outline Va Ca -> Prop :=
    bi_solve ∘ flat_map_to_set fr_backend_decomp_set ∘ vcs.
End logic_helpers.

(* TODO(@MattWindsor91): tidy this up. *)

Global Instance direct_frontend
       {V Ep Er LCtx GCtx : Type}
       {C SSt : Set}
       {T     : Type -> Type} (* Carrier type *)    
       {SSSt  : Setoid SSt}
       {SV    : Setoid V}
       {VV    : ViewsSemigroup V}
       {P_Ep     : PredEx Ep GCtx LCtx SSt}
       {R_Er     : RelEx Er GCtx LCtx SSt}    
       {T_SLk    : Setlike T}
       (instance : GCtx -> PreInstance C SSt)
       (decomp   : ViewsAxiom V C -> T (BVCond Ep Er))
       (Hcover :
          forall (ax : ViewsAxiom V C) (g : GCtx),
            (forall (b: BVCond Ep Er), In _ (to_set (decomp ax)) b -> bvhoare g b) ->
            In _ (instance g).(v_axioms) ax) :
  Frontend V V Ep Er LCtx GCtx C C SSt T :=
  {|
    fr_instance := instance;
    fr_views_decomp := Singleton _;
    fr_backend_decomp := decomp;
    fr_covering := covering_direct V Ep Er LCtx GCtx C SSt instance
                                   (to_set ∘ decomp) Hcover
  |}.

Global Instance builder_frontend
       {Ep Er GCtx LCtx Va Vf : Type}
       {Ca Cf SSt : Set}
       {T : Type -> Type}
       {SSSt  : Setoid SSt}
       {SVf   : Setoid Vf}
       {P_Ep  : PredEx Ep GCtx LCtx SSt}
       {R_Er  : RelEx Er GCtx LCtx SSt}
       {T_SLk : Setlike T}
       (b : ViewsAxiom Va Ca -> T (BVCond Ep Er))
       (f : ViewsAxiom Va Ca -> Ensemble (ViewsAxiom Vf Cf))
       (sig : GCtx -> Signature Cf SSt) :
  Frontend Va Vf Ep Er LCtx GCtx Ca Cf SSt T :=
  {|
    fr_instance := builder_to_instance (to_set ∘ b) f sig;
    fr_views_decomp := f;
    fr_backend_decomp := b;
    fr_covering := builder_to_template_covering (to_set ∘ b) f sig
  |}.
       
Lemma fr_top_axioms_direct
      {V Ep Er LCtx GCtx : Type}
      {C SSt : Set}
      {T     : Type -> Type} (* Carrier type *)    
      {SSSt  : Setoid SSt}
      {SV    : Setoid V}
      {VV    : ViewsSemigroup V}
      {P_Ep     : PredEx Ep GCtx LCtx SSt}
      {R_Er     : RelEx Er GCtx LCtx SSt}
      {T_SLk    : Setlike T}
      (instance : GCtx -> PreInstance C SSt)
      (decomp   : ViewsAxiom V C -> T (BVCond Ep Er))
      (Hcover :
         forall (ax : ViewsAxiom V C) (g : GCtx),
           (forall (b: BVCond Ep Er), In _ (to_set (decomp ax)) b -> bvhoare g b) ->
           In _ (instance g).(v_axioms) ax)
      (g : GCtx) :
  let F := direct_frontend instance decomp Hcover in
  Same_set _ (fr_top_axioms g) (fr_instance g).(v_axioms).
Proof.
  intros F.
  unfold fr_top_axioms.
  split.
  - intros ax Hin.
    apply Hin.
    reflexivity.
  - intros ax Hin ax' <-%Singleton_inv.
    exact Hin.
Qed.

(** These are the turnstile notations for solving using a set solver
    and frontend. *)

Notation ">>~" := lg_action (at level 99): frontend_scope.
Notation "|~" := lg_safe (at level 99): frontend_scope.

Local Open Scope frontend_scope.

Section frontend_soundness.

  (** Soundness of a Starling logic frontend depends on the relationship 
      between the way it decomposes frontend VCs into backend VCs, and the
      axiomatisation of its underlying Views instance.

      If the decomposition is [covering]---the Hoare judgement on all
      of the decomposed backend VCs entails the presence of the
      frontend VC in the instance's axiomatisation---then if the
      backend solver claims safety, then so does the underlying Views
      construction. *)
  
  Variables
    (Va Vf Ep Er GCtx LCtx : Type)
    (Ca Cf SSt : Set)
    (T : Type -> Type).

  Context
    {SVa   : Setoid Va}
    {VVa   : ViewsSemigroup Va}
    {SVf   : Setoid Vf}
    {VVf   : ViewsSemigroup Vf}
    {T_SLk : Setlike T}.

  (** If a decomposition is [covering], then the backend solution of 
      the entire decomposition of the VCs of an outline entails its
      safety in [i]. *)

  Lemma frontend_outline_safe
        {B : Backend Ep Er GCtx LCtx SSt}
        {F : Frontend Va Vf Ep Er LCtx GCtx Ca Cf SSt T}        
        (o: Outline Va Ca) :
    lg_safe o -> exists (g : GCtx), (outline_sound (fr_top_axioms g) o).
  Proof.
    intros (gc & Hsolved)%bi_solve_ideal.
    exists gc.
    intros a Ha.
    apply fr_covering.
    intros x Hin.
    apply Hsolved, Exists_exists.
    exists a.
    split; assumption.
  Qed.
End frontend_soundness.

