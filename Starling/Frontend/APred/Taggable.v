(** * Abstract predicates: Taggable class *)

From Coq Require Import
     Program.Basics
.

From Starling Require Import
     Frontend.APred.Core
     Views.Expr.Guarded
.

Local Open Scope program_scope.

Set Universe Polymorphism.

(** [Tagger] is the class of all things that can be a tag. *)

Class Tagger Tag :=
  mk_Tagger
    {
      tag_dec (t1 t2 : Tag) : { t1 = t2 } + { t1 <> t2 }
    }.

(** [Taggable] is the class of all things that have a particular tag. *)

Class Taggable Tag T {Tagger_Tag : Tagger Tag} :=
  mk_Taggable
    {
      tag_of (t : T) : Tag;
    }.

Global Instance Taggable_Tag {Tag} {Tagger_Tag : Tagger Tag} : Taggable Tag Tag :=
  {|
    tag_of := id
  |}.

Global Instance Taggable_LooseAPred {Tag Arg} {Tagger_Tag : Tagger Tag} :
  Taggable Tag (LooseAPred Tag Arg) :=
  {|
    tag_of := @lap_tag Tag Arg
  |}.

Global Instance Taggable_Subtype {Tag T} {P : T -> Prop} `{TT : Taggable Tag T} :
  Taggable Tag { x : T | P x } :=
  {|
    tag_of := tag_of ∘ (@proj1_sig _ _)
  |}.

Global Instance Taggable_Guarded {Tag G T} `{TT : Taggable Tag T} :
  Taggable Tag (Guarded G T) :=
  {|
    tag_of := tag_of ∘ (@g_item G T)
  |}.