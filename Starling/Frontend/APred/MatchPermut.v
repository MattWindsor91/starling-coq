(** * Abstract predicates: Pattern matching permutation results *)

From Coq Require Import
     Arith
     FunInd
     Lists.List
     Program.Basics
     Recdef
     Relations
     RelationPairs
     Classes.SetoidDec
     Classes.SetoidClass
     Lists.SetoidList
     Lists.SetoidPermutation
     Sorting.Permutation
.

From Starling Require Import
     Frontend.APred.Core
     Frontend.APred.Match
     Frontend.APred.Taggable
     Utils.List.Facts
     Utils.List.Setoid
     Utils.Option.Facts
     Utils.Zipper
.

Set Universe Polymorphism.

Section APred_match_permut.
  Import ListNotations.
  Local Open Scope program_scope.
  Local Open Scope views_scope.

  Variables Tag : Type.

  Context
    {T  : Type}
    {TR : Tagger Tag}
    {TT : Taggable Tag T}
  .

  Hypothesis Htag_dec : forall (x y : Tag), { x = y } + { x <> y }.

  Section forwards.

    (** We assume the existence of a setoid-esque relation [R], and some
      properties about it. *)

    Variable R : relation T.

    Context
      {ER : Equivalence R}.

    Hypothesis Htag_of_R : forall t1 t2, R t1 t2 -> tag_of t1 = tag_of t2.

    (* TODO: this *should* be a general (or sort-of general) result for
     permutations, but proving it in the general case seems
     excruciatingly difficult, so we assume it. *)
    Hypothesis Hperm_cons :
      forall x xs ys,
        PermutationA R (x::xs) (x::ys) -> PermutationA R xs ys.

    Lemma tag_matches_permut (t : Tag) (z1 z2 : Zipper T) :
      PermutationA R z1.(zp_right) z2.(zp_right) ->
      Forall
        (fun zp1 =>
           Exists
             (fun zp2 => (bindR R) (cur zp1) (cur zp2))
             (tag_matches Tag t z2))
        (tag_matches Tag t z1).
    Proof.
      destruct z1 as (zl1 & zr1), z2 as (zl2 & zr2).
      cbn in *.
      intro Hperm.
      revert zl1 zl2.
      induction Hperm; intros zl1 zl2.
      - (* Nil *)
        rewrite (tag_matches_rightmost _ t (mk_Zipper T zl1 []) (reflexivity _)).
        now cbn in *.
      - (* Skip *)
        rewrite (tag_matches_maybe_head_rec _
                                            t (mk_Zipper T zl1 (x₁ :: l₁)) x₁
                                            (reflexivity _)),
        (tag_matches_maybe_head_rec _
                                    t (mk_Zipper T zl2 (x₂ :: l₂)) x₂
                                    (reflexivity _)).
        apply Forall_app.
        + (* The part that was just added to the permutation *)
          destruct (tag_dec t (tag_of x₁)), (tag_dec t (tag_of x₂));
            constructor;
            cbn; try easy.
          * apply Exists_exists.
            exists {| zp_left := zl2; zp_right := x₂ :: l₂ |}; intuition.
          * (* The remaining case is a contradiction given [Htag_of_R]. *)
            now rewrite (Htag_of_R x₁ x₂ H) in *.
        + (* The existing (inductive) part *)
          cbn.
          specialize (IHHperm (x₁::zl1) (x₂::zl2)).
          apply Forall_forall.
          intros x Hin_x.
          pose proof (Forall_forall_in _ _ _ IHHperm Hin_x) as (zz & Hzz_in & Hzz_R)%Exists_exists.
          apply Exists_exists.
          firstorder.
      - (* Transpose *)
        rewrite (tag_matches_maybe_head_rec _
                                            t (mk_Zipper T zl1 (y :: x :: l)) y
                                            (reflexivity _)),
        (tag_matches_maybe_head_rec _
                                    t (mk_Zipper T zl2 (x :: y :: l)) x
                                    (reflexivity _)),
        (tag_matches_maybe_head_rec _
                                    t (mk_Zipper T (x :: zl2) (y :: l)) y
                                    (reflexivity _)),
        (tag_matches_maybe_head_rec _
                                    t (mk_Zipper T (y :: zl1) (x :: l)) x
                                    (reflexivity _)).
        cbn.
        destruct (tag_dec t (tag_of x)), (tag_dec t (tag_of y));
          (repeat apply Forall_app); cbn; try easy;
            apply Forall_forall; intros; apply Exists_exists;
              repeat match goal with
                     | [ H : In ?x [ ?y ] |- _ ] =>
                       inversion H; subst; intuition; clear H
                     | [ |- exists x,
                           In x
                              ({| zp_left := ?l; zp_right := ?c :: ?r |} :: _) /\
                           bindR R (cur ?y) (cur x) ] =>
                       exists {| zp_left := l; zp_right := c :: r |}; intuition; now cbv
                     | [ |- exists x,
                           In x
                              (_ :: {| zp_left := ?l; zp_right := ?c :: ?r |} :: _) /\
                           bindR R (cur _) (cur x) ] =>
                       exists {| zp_left := l; zp_right := c :: r |}; intuition; now cbv
                     end;
              (* All of the remaining cases here have virtually the same proof. *)
              destruct
                (Forall_forall_in _ _ _ (tag_matches_left_swap _ _ _ (y::x::zl2)) H) as
                  (prefix & Hin);
              exists (replace_left x0 (prefix ++ y :: x :: zl2)); intuition;
                autorewrite with zipper;
                pose proof (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _ _) H) as (m & -> & Htag);
                now cbv.
      - (* Transitivity *)
        apply Forall_forall.
        intros x Hin_1.
        pose proof (Forall_forall_in _ _ _ (IHHperm1 zl1 zl1) Hin_1) as
            (y & Hin_2 & Hbind_xy)%Exists_exists.
        pose proof (Forall_forall_in _ _ _ (IHHperm2 zl1 zl2) Hin_2) as
            (z & Hin_3 & Hbind_yz)%Exists_exists.
        apply Exists_exists.
        exists z; intuition.
        (* Now to show transitivity. *)
        pose proof (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _ _) Hin_1)
          as (cx & Hcx & _).
        pose proof (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _ _) Hin_2)
          as (cy & Hcy & _).
        pose proof (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _ _) Hin_3)
          as (cz & Hcz & _).
        rewrite Hcx, Hcy, Hcz in *.
        apply -> bindR_Some in Hbind_xy.
        apply -> bindR_Some in Hbind_yz.
        apply bindR_Some.
        now transitivity cy.
    Qed.

    (** If we apply [proc_tag_matches] using two zips whose right lists
      are setoid permutations of one another, then at least one of the
      matches in one application starts with the head of a match from
      the other application. *)

    Lemma proc_tag_matches_permut
          (t       : Tag)
          (pm1 pm2 : list T)
          (pz1 pz2 : Zipper T) :
      PermutationA R pz1.(zp_right) pz2.(zp_right) ->
      Forall
        (fun '(m1, z1) =>
           Exists (fun '(m2, z2) =>
                     bindR R (hd_error m1) (hd_error m2))
                  (proc_tag_matches Tag t pm2 pz2))
        (proc_tag_matches Tag t pm1 pz1).
    Proof.
      intro Hperm_zip.
      apply Forall_forall.
      unfold proc_tag_matches.
      intros (m1 & z1) (xm & Hmatch & Hin)%in_map_iff.
      pose proof (Forall_forall_in _ _ _ (tag_matches_permut t _ _ Hperm_zip) Hin)
        as (xm' & Hin' & Hbind)%Exists_exists.
      (* Eliminate bindR by showing that [xm] and [xm'] have [cur]s. *)
      pose proof (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _ _) Hin)
        as (m & Hcxm & _).
      pose proof (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _ _) Hin')
        as (m' & Hcxm' & _).
      rewrite Hcxm, Hcxm' in Hbind.
      apply -> bindR_Some in Hbind.
      (* Now build the exist witness. *)
      apply Exists_exists.
      exists (proc_tag_match pm2 xm').
      split.
      - apply in_map, Hin'.
      - destruct (proc_tag_match pm2 xm') as (m2, z2) eqn:Hmatch'.
        unfold proc_tag_match in *.
        rewrite Hcxm in Hmatch.
        rewrite Hcxm' in Hmatch'.
        cbn in *.
        inversion Hmatch; subst.
        inversion Hmatch'; subst.
        intuition.
    Qed.

    Definition iter_permut_invariant
               (pmatches1 pmatches2 : list (list T * Zipper T)) : Prop :=
      Forall
        (fun '(pm1, pz1) =>
           leftmost pz1 /\
           Exists
             (fun '(pm2, pz2) =>
                leftmost pz2 /\
                eqlistA R pm1 pm2 /\
                PermutationA R pz1.(zp_right) pz2.(zp_right))
             pmatches2)
        pmatches1.

    Lemma pattern_matches_iter_permut
          (t : Tag)
          (pmatches1 pmatches2 : list (list T * Zipper T)) :
      iter_permut_invariant pmatches1 pmatches2 ->
      iter_permut_invariant (pattern_matches_iter Tag t pmatches1)
                            (pattern_matches_iter Tag t pmatches2).
    Proof.
      intro Hperm_zip.
      apply Forall_forall.
      intros (m1 & z1) Hin.
      pose proof (Forall_forall_in _ _ _ (pattern_matches_iter_leftmost_zips _ _ _) Hin)
        as Hlm; intuition.
      apply Exists_exists.
      unfold pattern_matches_iter in *.
      apply in_flat_map in Hin.
      destruct Hin as ((pm1 & pz1) & Hin_pmatches1 & Hmatch1).
      apply Forall_forall_in with (x := (pm1, pz1)) in Hperm_zip; try easy.
      destruct Hperm_zip as
          (Hleft_pz1 &
           ((pm2 & pz2) &
            Hin_pmatches2 & Hleft_pz2 & Hperm_match & Hperm_zip)%Exists_exists).
      cbn in *.
      pose proof (Forall_forall_in _ _ _ (proc_tag_matches_permut t _ pm2 _ _ Hperm_zip) Hmatch1)
        as ((m2 & z2) & Hmatch2 & Hhd_R)%Exists_exists.
      (* To show [m1] is a permutation of [m2], we need to show that each
         side is an extension of [pm1] or [pm2] respectively. *)
      destruct (Forall_forall_in _ _ _ (proc_tag_matches_split _ _ _ _) Hmatch1) as
          (mh1 & ->).
      destruct (Forall_forall_in _ _ _ (proc_tag_matches_split _ _ _ _) Hmatch2) as
          (mh2 & ->).
      exists ((mh2 :: pm2), z2); intuition.
      - apply in_flat_map.
        now (exists (pm2, pz2)).
      - pose proof (Forall_forall_in _ _ _ (proc_tag_matches_leftmost_zips _ _ _ _) Hmatch2); intuition.

      - cbv in Hhd_R.
        (* TODO: this needs factoring out a bit. *)
        unfold proc_tag_matches in *.
        apply in_map_iff in Hmatch1.
        apply in_map_iff in Hmatch2.
        destruct Hmatch1 as (zp1 & Hptm1 & Hinz1),
                            Hmatch2 as (zp2 & Hptm2 & Hinz2).
        unfold proc_tag_match in *.
        destruct zp1 as (zp1l & [|zp1c zp1r]); try easy.
        destruct zp2 as (zp2l & [|zp2c zp2r]); try easy.
        cbn in *.
        inversion Hptm1; subst.
        inversion Hptm2; subst.
        rewrite <-! to_list_rewind.
        (* [tag_matches] preserves zipper listifications, so we can
           eventually salvage the original permutation. *)
        pose proof (Forall_forall_in _ _ _ (tag_matches_lists _ _ _) Hinz1) as Hzl1.
        pose proof (Forall_forall_in _ _ _ (tag_matches_lists _ _ _) Hinz2) as Hzl2.
        unfold Zipper_lists_eq, "@@" in *.
        cbn in *.
        rewrite (leftmost_list Hleft_pz1) in Hzl1.
        rewrite (leftmost_list Hleft_pz2) in Hzl2.
        rewrite Hzl1, Hzl2 in Hperm_zip.
        (* Now we can break up the various permutations. *)
        assert (PermutationA R
                             (mh1 :: (rev zp1l ++ zp1r))
                             (mh2 :: (rev zp2l ++ zp2r))) as Hperm_zip'.
        {
          transitivity (rev zp1l ++ mh1 :: zp1r).
          {
            apply PermutationA_middle, ER.
          }
          transitivity (rev zp2l ++ mh2 :: zp2r); try easy.
          symmetry.
          apply PermutationA_middle, ER.
        }
        rewrite Hhd_R in Hperm_zip'.
        now apply Hperm_cons in Hperm_zip'.
    Qed.

    Lemma pattern_matches_inner_permut
          (ts : list Tag)
          (input1 input2 : list T) :
      PermutationA R input1 input2 ->
      iter_permut_invariant (pattern_matches_inner Tag ts input1)
                            (pattern_matches_inner Tag ts input2).
    Proof.
      intro Hperm_input.
      induction ts.
      - (* Base case *)
        rewrite ! pattern_matches_inner_empty_tag.
        constructor; intuition.
        + apply list_leftmost.
        + apply Exists_exists.
          exists ([], list_to_Zipper input2); intuition.
          apply list_leftmost.
      - rewrite ! pattern_matches_inner_cons_unfold.
        assert (iter_permut_invariant (pattern_matches_inner Tag ts input1)
                                      (pattern_matches_inner Tag ts input2)) as Hinv.
        {
          apply Forall_forall.
          intros (m & z) Hin_input1.
          pose proof (Forall_forall_in _ _ _
                                       (pattern_matches_inner_leftmost_zips _ _ _)
                                       Hin_input1); intuition.
          apply Forall_forall_in with (x := (m, z)) in IHts; intuition.
        }
        apply pattern_matches_iter_permut with (t := a) in Hinv; intuition.
    Qed.

    Corollary pattern_matches_inner_match_permut
              (ts : list Tag)
              (input1 input2 : list T) :
      PermutationA R input1 input2 ->
      Forall (fun '(m1, _) =>
                Exists (fun '(m2, _) => eqlistA R m1 m2)
                       (pattern_matches_inner Tag ts input2))
             (pattern_matches_inner Tag ts input1).
    Proof.
      intro Hperm_input.
      apply Forall_forall.
      intros (m1 & z1) Hin1.
      pose proof
           (Forall_forall_in _ _ _
                             (pattern_matches_inner_permut _ _ _ Hperm_input) Hin1) as (_ & ((m2 & z2) & Hin2 & _ & Hperm2 & _)%Exists_exists).
      apply Exists_exists.
      now exists (m2, z2).
    Qed.

    Corollary pattern_matches_match_permut
              (ts : list Tag)
              (input1 input2 : list T) :
      PermutationA R input1 input2 ->
      Forall (fun m1 =>
                Exists (eqlistA R m1)
                       (pattern_matches Tag ts input2))
             (pattern_matches Tag ts input1).
    Proof.
      intro Hperm_input.
      unfold pattern_matches.
      apply Forall_forall.
      intros m1 ((m & z) & <- & Hin)%in_map_iff.
      apply (Forall_forall_in _ _ _
                              (pattern_matches_inner_match_permut _ _ _ Hperm_input)),
      Exists_exists
        in Hin.
      destruct Hin as ((m1 & z1) & Hin & Heql).
      apply Exists_exists.
      exists m1; intuition.
      apply in_map_iff.
      now exists (m1, z1).
    Qed.

    Corollary pattern_matches_exact_match_permut
              (ts : list Tag)
              (input1 input2 : list T) :
      PermutationA R input1 input2 ->
      Forall (fun m1 =>
                Exists (eqlistA R m1)
                       (pattern_matches_exact Tag ts input2))
             (pattern_matches_exact Tag ts input1).
    Proof.
      intro Hperm_input.
      unfold pattern_matches_exact.
      apply Forall_forall.
      intros m1 (Hin & Hlen)%filter_In.
      apply (Forall_forall_in _ _ _
                              (pattern_matches_match_permut _ _ _ Hperm_input)),
      Exists_exists
        in Hin.
      destruct Hin as (m2 & Hin & Heql).
      apply Exists_exists.
      exists m2; intuition.
      apply filter_In; intuition.
      (* Show that all of the lengths are equal. *)
      apply Nat.eqb_eq.
      apply Nat.eqb_eq in Hlen.
      apply PermutationA_length in Hperm_input.
      apply eqlistA_length in Heql.
      congruence.
    Qed.
  End forwards.

  Section backwards.
    (** Every time we run a zipper through [proc_tag_matches], the element that
      moves into the new match is the one that we remove from the zipper. *)

    Lemma proc_tag_matches_zipper_rem
          (t : Tag)
          (prev_match : list T)
          (prev_zip : Zipper T) :
      Forall
        (fun '(new_match, new_zip) =>
           exists a : T,
             hd_error new_match = Some a /\
             Permutation (a::(Zipper_to_list new_zip))
                         (Zipper_to_list prev_zip))
        (proc_tag_matches _ t prev_match prev_zip).
    Proof.
      apply Forall_forall.
      intros (new_match & new_zip) Hin.
      destruct (proj1 (Forall_forall _ _) (proc_tag_matches_split _ _ _ _) _ Hin)
        as (a & ->).
      exists a; intuition.
      unfold proc_tag_matches, proc_tag_match in Hin.
      apply in_map_iff in Hin.
      destruct Hin as (xzip & Hxcur & Hxin), (cur xzip) as [c|] eqn:Hxzip; try easy.
      inversion Hxcur; subst.
      rewrite <- to_list_rewind_equal.
      destruct xzip as (xl & [|xr]); try easy.
      inversion Hxzip; subst.
      rewrite (proj1 (Forall_forall _ _) (tag_matches_lists _ _ _) _ Hxin).
      apply Permutation_middle.
    Qed.

    Definition SubPermutation (xs ys : list T) : Prop :=
      exists (xs' : list T), Permutation (xs++xs') ys.

    (** As we apply [proc_tag_matches], the join of each match and each
      zipper remains invariant, and the match grows monotonically. *)

    Corollary proc_tag_matches_dist
              (t : Tag)
              (prev_match : list T)
              (prev_zip : Zipper T) :
      Forall
        (fun '(new_match, new_zip) =>
           Permutation
             (new_match ++ Zipper_to_list new_zip)
             (prev_match ++ Zipper_to_list prev_zip)
        )
        (proc_tag_matches _ t prev_match prev_zip).
    Proof.
      apply Forall_forall.
      intros (new_match & new_zip) Hin.
      (* Use the above lemma to eliminate the zipper parts. *)
      destruct (proj1 (Forall_forall _ _) (proc_tag_matches_zipper_rem _ _ _) _ Hin)
        as (hd & Hhd & Hpermut).
      pose proof (proj1 (Forall_forall _ _) (proc_tag_matches_prev _ _ _ _) _ Hin)
        as (z & -> & Hpz & Hhd' & <-).
      rewrite <- Hpz in *.
      rewrite Hhd in Hhd'.
      destruct new_match as [|x tl_match]; try easy.
      rewrite <- app_comm_cons, Permutation_middle.
      apply Permutation_app_head.
      now inversion_clear Hhd.
    Qed.

    (** This property carries on to [pattern_matches_iter]... *)

    Lemma pattern_matches_iter_dist
          (t : Tag)
          (prev : list (list T * Zipper T)) :
      Forall
        (fun '(new_match, new_zip) =>
           Exists
             (fun '(prev_match, prev_zip) =>
                Permutation
                  (new_match ++ Zipper_to_list new_zip)
                  (prev_match ++ Zipper_to_list prev_zip)
             )
             prev)
        (pattern_matches_iter _ t prev).
    Proof.
      apply Forall_forall.
      unfold pattern_matches_iter.
      intros (view, vzip) ((p_match & p_zip) & Hin_p & Hmatch)%in_flat_map.
      pose proof (proj1 (Forall_forall _ _) (proc_tag_matches_dist _ _ _) _ Hmatch) as Heqv.
      apply Exists_exists.
      now (exists (p_match, p_zip)).
    Qed.

    (** ... and [pattern_matches_inner]. *)

    Lemma pattern_matches_inner_dist
          (ts : list Tag)
          (xs : list T) :
      Forall
        (fun '(new_match, new_zip) =>
           Permutation
             (new_match ++ Zipper_to_list new_zip)
             xs)
        (pattern_matches_inner _ ts xs).
    Proof.
      apply Forall_forall.
      revert xs.
      induction ts; intros xs matches Hmatches.
      - rewrite pattern_matches_inner_empty_tag in Hmatches.
        now destruct Hmatches as [<- | Hct].
      - destruct matches as (new_match & new_zip).
        pose proof (proj1 (Forall_forall _ _) (pattern_matches_iter_dist a _) _ Hmatches)
          as ((xs'' & zip') & Hfr & Hperm)%Exists_exists.
        now rewrite <- (IHts xs (xs'', zip') Hfr).
    Qed.

    (** As a result of the above, we know that the matches put out by
      [pattern_matches_inner] are always sub-permutations of the input view. *)

    Corollary pattern_matches_inner_subpermut
              (ts : list Tag)
              (xs : list T) :
      Forall
        (fun '(new_match, _) => SubPermutation new_match xs)
        (pattern_matches_inner _ ts xs).
    Proof.
      apply Forall_forall.
      intros (new_match & new_zip) Hin.
      exists (Zipper_to_list new_zip).
      now rewrite <- (proj1 (Forall_forall _ _) (pattern_matches_inner_dist _ _) _ Hin).
    Qed.

    (** If we only consider matches with exactly the same length as the
      original list, such matches are direct permutations of said list. *)

    Lemma pattern_matches_inner_exact_permut
          (ts : list Tag)
          (xs : list T) :
      Forall
        (* We arrange this in a slightly different way from usual to
             make stating the final result easier. *)
        (fun new_match =>
           length (fst new_match) =? length xs = true ->
           Permutation (fst new_match) xs)
        (pattern_matches_inner _ ts xs).
    Proof.
      apply Forall_forall.
      intros (new_match & new_zip) Hin.
      (* Effectively, what we need to do here is show that the zipper is empty
       at the end of pattern matching. *)
      rewrite <- (proj1 (Forall_forall _ _) (pattern_matches_inner_combined_length _ _ _) _ Hin).
      rewrite <- (proj1 (Forall_forall _ _) (pattern_matches_inner_dist _ _) _ Hin).
      intros Hlnmatch%Nat.eqb_eq.
      symmetry in Hlnmatch.
      rewrite <- Nat.add_0_r in Hlnmatch.
      apply Nat.add_cancel_l in Hlnmatch.
      unfold Zipper_length in Hlnmatch.
      destruct new_zip as ([|] & [|]); cbn; try easy.
      now rewrite app_nil_r.
    Qed.

    (** The above observation carries over to [pattern_matches_exact]. *)

    Lemma pattern_matches_exact_permut
          (ts : list Tag)
          (xs : list T) :
      Forall
        (* We arrange this in a slightly different way from usual to
             make stating the final result easier. *)
        (Permutation xs)
        (pattern_matches_exact _ ts xs).
    Proof.
      apply Forall_forall.
      unfold pattern_matches_exact, pattern_matches.
      intros ys (((m & z) & <- & Hin)%in_map_iff & Hlen)%filter_In.
      symmetry.
      now apply (Forall_forall_in _ _ _ (pattern_matches_inner_exact_permut ts _)).
    Qed.
  End backwards.
End APred_match_permut.