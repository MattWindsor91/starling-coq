(** * Abstract predicates: flattening *)

From Coq Require Import
     Arith.PeanoNat
     Bool.Bool
     Lists.List
     Lists.SetoidList
     Program.Basics
     Relations.Relation_Operators
     Sets.Constructive_sets
     Classes.RelationPairs
     Classes.SetoidClass
     Classes.SetoidDec
     Recdef
.

From Starling Require Import
     Backend.Alpha.Classes
     Backend.Classes
     Frontend.APred.Core
     Frontend.APred.Taggable
     Utils.List.Facts
     Utils.Option.Facts
     Utils.Monads
     Utils.Multiset.LMultiset
     Views.Classes
     Views.Expr
     Views.Instances.LMultiset
     Views.Transformers.Function
     Views.Transformers.Subtype.

Set Universe Polymorphism.

Section APred_flatten.

  Local Open Scope program_scope.
  Local Open Scope signature_scope.
  Import ListNotations.

  Polymorphic Universe t.
  Variable Tag : Type@{t}.

  Polymorphic Universe a.
  Variable Arg : Type@{a}.

  Variables
    Val

    GCtx
    LCtx : Type.

  Variable
    protos : Tag -> option TagProto.

  Context
    {T_Tag : Tagger Tag}
    {S_Val : Setoid Val}.

  Definition collect_protos : list Tag -> option (list TagProto) :=
    collect ∘ List.map protos.

  Definition unify_proto (xs : list TagProto) : TagProto :=
    {|
      sp_argc := sum (List.map sp_argc xs);
    |}.

  Definition flattened_protos : list Tag -> option TagProto :=
    fmap unify_proto ∘ collect_protos.

  (** The empty tag is always valid.

      This is necessary to represent the monoidal unit! *)

  Lemma flatten_nil_legal : has_TagProto _ flattened_protos [].
  Proof.
    easy.
  Qed.

  (** If [x] was a valid single tag, and [xs] a valid tag list, then
      [x::xs] is valid; and vice versa. *)

  Lemma flatten_cons_legal (x : Tag) (xs : list Tag):
    (has_TagProto _ protos x /\
     has_TagProto _ flattened_protos xs) <->
    has_TagProto _ flattened_protos (x::xs).
  Proof.
    split.
    - intros ((xdef & Hx)%not_none_is_some & (xsdef & Hxs)%not_none_is_some).
      unfold has_TagProto, flattened_protos, collect_protos, compose in *.
      rewrite List.map_cons, Hx.
      unfold collect.
      fold (collect (List.map protos xs)).
      now destruct (collect (List.map protos xs)) as [xs'|].
    - intros (xsdef & Hhas_xs)%not_none_is_some.
      unfold has_TagProto, flattened_protos, collect_protos, compose in *.
      rewrite List.map_cons in Hhas_xs.
      unfold collect in Hhas_xs.
      fold (collect (List.map protos xs)) in Hhas_xs.
      cbn in Hhas_xs.
      now destruct (protos x) as [x'|], (collect (List.map protos xs)) as [xs'|].
  Qed.

  (** [xs] is in [flattened_protos] provided that each element in [xs]
      is in [protos]. *)

  Corollary flatten_legal_legal (xs : list Tag) :
    Forall (has_TagProto _ protos) xs <->
    has_TagProto _ flattened_protos xs.
  Proof.
    induction xs; try easy.
    split.
    - intro Hlegal.
      apply flatten_cons_legal.
      inversion Hlegal; intuition.
    - intros (Ha & Hxs)%flatten_cons_legal; intuition.
  Qed.

  (** We can lift a single tag into the [flattened_protos] world. *)

  Corollary flatten_single_legal (x : Tag) :
    has_TagProto _ protos x <->
    has_TagProto _ flattened_protos [x].
  Proof.
    split.
    - intro Hx.
      now apply flatten_cons_legal.
    - now intros Hx%flatten_cons_legal.
  Qed.

  (** If two tag lists appear in [flattened_protos], so does their append. *)

  Lemma flatten_app_legal (xs ys : list Tag) :
    has_TagProto _ flattened_protos xs ->
    has_TagProto _ flattened_protos ys ->
    has_TagProto _ flattened_protos (xs++ys).
  Proof.
    intros Hxs Hys.
    induction xs; try easy.
    rewrite <- app_comm_cons.
    apply flatten_cons_legal in Hxs.
    apply flatten_cons_legal.
    tauto.
  Qed.

  Lemma flatten_ATag_legal (ts : list (ATag protos)) :
    has_TagProto _ flattened_protos (map (@proj1_sig _ _) ts).
  Proof.
    apply flatten_legal_legal.
    unfold has_TagProto.
    induction ts; constructor; intuition.
    now apply (proj2_sig a).
  Qed.

  Lemma unflatten_ATag_legal (ts : ATag flattened_protos) :
    Forall (has_TagProto _ protos) (proj1_sig ts).
  Proof.
    apply flatten_legal_legal, (proj2_sig ts).
  Qed.

  Definition flatten_tags (ts : list (ATag protos)) :
    ATag flattened_protos :=
    exist _ (map (@proj1_sig _ _) ts) (flatten_ATag_legal _).

  Import EqNotations.

  Fixpoint unflatten_tags_inner
           (ts : list Tag)
           (Hts : Forall (has_TagProto _ protos) ts) :
    list (ATag protos) :=
    match ts return (_ -> list (ATag protos)) with
    | [] => fun _ => []
    | t::ts' =>
      fun Hxs =>
        (exist _ t (Forall_inv (rew <- Hxs in Hts)))
          :: unflatten_tags_inner ts' (Forall_snoc _ t ts' (rew <- Hxs in Hts))
    end (reflexivity _).

  Lemma unflatten_tags_inner_cons
        (p : Tag)
        (ps : list Tag)
        (Hts : Forall (has_TagProto _ protos) (p::ps)) :
      unflatten_tags_inner (p::ps) Hts =
      (exist _ p (Forall_inv Hts))::(unflatten_tags_inner ps (Forall_snoc _ p ps Hts)).
  Proof.
    intuition.
  Qed.

  Lemma unflatten_inner_flatten_map {A} (ts : list (ATag protos)) (f : ATag protos -> A)
        (Hts : Forall (has_TagProto _ protos) (proj1_sig (flatten_tags ts))) :
    (forall x y, proj1_sig x = proj1_sig y -> f x = f y) ->
    map f (unflatten_tags_inner (proj1_sig (flatten_tags ts)) Hts) = map f ts.
  Proof.
    intro Hprfirr.
    induction ts; intuition.
    rewrite map_cons.
    cbn.
    f_equal.
    - now apply Hprfirr.
    - apply IHts.
  Qed.

  Definition unflatten_tags (ts : ATag flattened_protos) :
    list (ATag protos) :=
    unflatten_tags_inner (proj1_sig ts) (unflatten_ATag_legal ts).

  (** Iff [unflatten_tags] gives us an empty list, we fed in an empty tag. *)

  Lemma unflatten_tags_nil (ts : ATag flattened_protos) :
    unflatten_tags ts = [] <-> proj1_sig ts = [].
  Proof.
    split; intros Hresult; now destruct ts as ([|] & Hts).
  Qed.

  Lemma unflatten_tags_inner_length
        (ts  : list Tag)
        (Hts : Forall (has_TagProto _ protos) ts) :
    length ts = length (unflatten_tags_inner ts Hts).
  Proof.
    induction ts; cbn; try easy.
    now f_equal.
  Qed.

  Corollary unflatten_tags_length (ts : ATag flattened_protos) :
    length (proj1_sig ts) = length (unflatten_tags ts).
  Proof.
    apply unflatten_tags_inner_length.
  Qed.

  Lemma unflatten_flatten_map {A} (ts : list (ATag protos)) (f : ATag protos -> A) :
    (forall x y, proj1_sig x = proj1_sig y -> f x = f y) ->
    map f (unflatten_tags (flatten_tags ts)) = map f ts.
  Proof.
    apply unflatten_inner_flatten_map.
  Qed.

  Lemma unflatten_tags_inner_proj' {T: Type} {TT : Taggable Tag T}
        (ts : list Tag)
        (xs : list T)
        (Hts : Forall (has_TagProto _ protos) ts) :
    Forall2 (fun t (a : T) => t = tag_of a) ts xs ->
    Forall2 (fun t a => proj1_sig t = tag_of a) (unflatten_tags_inner ts Hts)
            xs.
  Proof.
    induction 1; constructor.
    - apply H.
    - apply IHForall2.
  Qed.

  Corollary unflatten_tags_inner_proj
        (ts : list Tag)
        (Hts : Forall (has_TagProto _ protos) ts) :
    Forall2
      (fun x y => proj1_sig x = y)
      (unflatten_tags_inner ts Hts)
      ts.
  Proof.
    pose proof (unflatten_tags_inner_proj' (T := Tag)) as Hproj.
    apply Hproj.
    clear Hts Hproj.
    induction ts; constructor; intuition.
  Qed.

  Corollary unflatten_tags_proj (ts : ATag flattened_protos) :
    Forall2
      (fun x y => proj1_sig x = y)
      (unflatten_tags ts)
      (proj1_sig ts).
  Proof.
    apply unflatten_tags_inner_proj.
  Qed.

  (** Given the same tag list with different proofs of validity, the result of
      [unflatten_tags_inner] is equal modulo carried validity proofs. *)

  Lemma unflatten_tags_inner_prfirr
        (ps : list Tag)
        (Hts Hts' : Forall (has_TagProto _ protos) ps) :
    eqlistA (eq @@ (@proj1_sig _ _))
            (unflatten_tags_inner ps Hts)
            (unflatten_tags_inner ps Hts').
  Proof.
    induction ps; cbn; try easy.
    now constructor.
  Qed.

  (** Given the same tag list with different proofs of validity, the result of
      [unflatten_tags] is equal modulo carried validity proofs. *)

  Global Instance unflatten_tags_prfirr_Proper :
    Proper (eq @@ (@proj1_sig _ _) ==> eqlistA (eq @@ (@proj1_sig _ _)))
            unflatten_tags.
  Proof.
    intros (ps & Hps) (ps' & Hps').
    unfold "@@".
    simpl.
    intros ->.
    apply unflatten_tags_inner_prfirr.
  Defined.

  Lemma unflatten_tags_inner_proj_prfirr
        (A : Type)
        (ts : list Tag)
        (Hts1 Hts2 : Forall (has_TagProto _ protos) ts)
        (xs : list A) (f : A -> Tag) :
    Forall2 (fun x y => proj1_sig x = f y) (unflatten_tags_inner ts Hts1) xs ->
    Forall2 (fun x y => proj1_sig x = f y) (unflatten_tags_inner ts Hts2) xs.
  Proof.
    revert xs.
    induction ts; try easy.
    inversion 1; subst.
    constructor; try easy.
    eapply IHts, H4.
  Qed.

  Corollary unflatten_tags_proj_prfirr
        (A : Type)
        (ts : list Tag)
        (Hts1 Hts2 : has_TagProto _ flattened_protos ts)
        (xs : list A) (f : A -> Tag) :
    Forall2 (fun x y => proj1_sig x = f y) (unflatten_tags (exist _ ts Hts1)) xs ->
    Forall2 (fun x y => proj1_sig x = f y) (unflatten_tags (exist _ ts Hts2)) xs.
  Proof.
    apply unflatten_tags_inner_proj_prfirr.
  Qed.

  (** The argc of a flattened tag is the sum of the argcs of its unflattening. *)

  Lemma unflatten_tags_inner_argc
        (ts : ATag flattened_protos)
        (Hvalid : Forall (has_TagProto _ protos) (proj1_sig ts)) :
    get_argc ts = sum (map (@get_argc _ _) (unflatten_tags_inner (proj1_sig ts) Hvalid)).
  Proof.
    destruct ts as (ts & Hts).
    unfold unflatten_tags, proj1_sig.
    rewrite (same_tag_same_argc
               (exist (fun x => flattened_protos x <> None) ts Hts)
               (exist _ ts (proj1 (flatten_legal_legal _) Hvalid)));
      try easy.
    unfold proj1_sig in *.
    clear Hts.
    induction ts; try easy.
    cbn.
    rewrite <- (IHts _).
    unfold get_argc, compose, get_ATagProto, proj1_sig.
    inversion Hvalid; subst.
    destruct (option_sumor (protos a))
      as [(x & Hx)|Hx],
         (option_sumor (flattened_protos ts))
        as [(y & Hy)|Hy],
           (option_sumor (flattened_protos (a::ts)))
             as [(z & Hz)|Hz];
      cbn in *; try easy.
    {
      rewrite Hx in Hz.
      unfold collect_protos, compose in Hy.
      destruct (collect (map protos ts)) as [cts|]; cbn in *; try easy.
      inversion Hz; subst.
      now inversion Hy.
    }
    all: exfalso;
      apply flatten_legal_legal, not_none_is_some in H2;
      destruct H2 as (e & H2);
      rewrite Hx in Hz;
      cbn in *;
      unfold collect_protos, compose in *;
      destruct (collect (map protos ts)) as [cts|]; cbn in *; try easy.
  Qed.

  Corollary unflatten_tags_argc (ts : ATag flattened_protos) :
    get_argc ts = sum (map (@get_argc _ _) (unflatten_tags ts)).
  Proof.
    apply unflatten_tags_inner_argc.
  Qed.

  (** [flatten_LooseAPred] converts a list of [LooseAPred]s into a single
      flat [LooseAPred]. *)

  Definition flatten_LooseAPred (aps : list (LooseAPred Tag Arg)) :
    LooseAPred (list Tag) Arg :=
    mk_LooseAPred
      (list Tag)
      Arg
      (map (@lap_tag _ _) aps)
      (flat_map (@lap_argv _ _) aps).

  Lemma flatten_APred_valid (aps : list (APred Arg protos)) :
    valid (flatten_LooseAPred (map (@proj1_sig _ _) aps)) flattened_protos.
  Proof.
    induction aps.
    - now (exists (unify_proto [])).
    - destruct a as (a & atp & Ha_def & Ha_len).
      destruct IHaps as (tp & Haps_def & Haps_len).
      unfold valid in *.
      cbn in *.
      rewrite Ha_def, app_length, Ha_len, Haps_len.
      destruct (collect (map protos (map (lap_tag (Arg:=Arg)) (map (@proj1_sig _ _) aps))))
        as [l|] eqn:Hl; unfold valid in Hl; rewrite Hl in *; cbn in *; try easy.
      exists (unify_proto (atp :: l)).
      now inversion Haps_def; subst.
  Qed.

  (** Flattening a list of valid abstract predicates produces an
      abstract predicate that is valid in the flattened world. *)

  Definition flatten_APred (aps : list (APred Arg protos)) :
    APred Arg flattened_protos :=
    exist _ (flatten_LooseAPred (map (@proj1_sig _ _) aps))
          (flatten_APred_valid aps).

  Lemma flatten_LooseAPred_nil :
    flatten_LooseAPred [] = mk_LooseAPred _ _ [] [].
  Proof.
    intuition.
  Qed.

  (** If two lists of [APred]s are pairwise equivalent, then the resulting
      flattenings are equivalent. *)

  Lemma flatten_APred_Proper {S_Arg : Setoid Arg} :
    Proper (eqlistA equiv  ==> equiv) flatten_APred.
  Proof.
    intros x y Hxy.
    induction Hxy; intuition; cbn in *.
    unfold APred_equiv, flatten_LooseAPred in *.
    cbn in *.
    split.
    - f_equal; intuition.
    - destruct H as (_ & Hargv1%eqlistA_altdef).
      destruct IHHxy as (_ & Hargv2%eqlistA_altdef).
      apply eqlistA_altdef.
      now apply Forall2_app.
  Qed.
End APred_flatten.