(** * Abstract predicates: Guarded pattern matching *)

From Coq Require Import
     Arith
     Omega
     FunInd
     Lists.List
     Program.Basics
     Recdef
     Relations
     Classes.RelationClasses
     Classes.SetoidDec
     Classes.SetoidClass
     RelationPairs
.

From Starling Require Import
     Backend.Alpha.Classes
     Backend.Classes
     Frontend.APred.Core
     Frontend.APred.Match
     Frontend.APred.MatchPermut
     Frontend.APred.Taggable
     Utils.List.Facts
     Utils.Option.Facts
     Utils.Monads
     Utils.Multiset.LMultiset
     Utils.Zipper
     Views.Classes
     Views.Expr
     Views.Expr.Instances
     Views.Instances.LMultiset
     Views.Transformers.Function
     Views.Transformers.Subtype
.

Set Universe Polymorphism.

Section APred_gmatch.
  Import ListNotations.
  Local Open Scope program_scope.
  Local Open Scope views_scope.
  Local Open Scope signature_scope.

  Polymorphic Universe t.
  Variable Tag : Type@{t}.

  Polymorphic Universe l.
  Variable Val : Type@{l}.

  Polymorphic Universe v.
  Variable Var : Type@{v}.

  Polymorphic Universe ep.
  Variable Ep : Type@{v} -> Type@{ep}.

  Polymorphic Universe ev.
  Variable Ev : Type@{v} -> Type@{ev}.

  Variables
    (protos : Tag -> option TagProto)
  .

  Context
    {S_Val : Setoid Val}
    {D_Val : EqDec S_Val}
    {V_Ev  : ValEx Ev (Val := Val)}
    {T_Tag : Tagger Tag}
    {P_EEp : EqPredEx unit unit Ep (Val := Val)}.

  Definition merge_guards
             (atoms : list (GAPred Ep (Ev Var) protos)) :
    Guarded (Ep (Ev Var)) (list (APred (Ev Var) protos)) :=
    match split (map (fun '(mk_Guarded g a) => (g, a)) atoms) with
    | (g, v) => mk_Guarded (concat g) v
    end.

  Lemma merge_guards_length
        (atoms : list (GAPred Ep (Ev Var) protos)) :
    length atoms = length (g_item (merge_guards atoms)).
  Proof.
    induction atoms; intuition.
    cbn.
    rewrite IHatoms.
    unfold merge_guards.
    cbn.
    destruct (split (map (fun '(mk_Guarded g a) => (g, a)) atoms)) as (g & v) eqn:Hsplit.
    now destruct a as (a_guar & a_item).
  Qed.

  Lemma merge_guards_cons
        (atoms : list (GAPred Ep (Ev Var) protos))
        (gs    : list (Ep (Ev Var)))
        (x     : APred (Ev Var) protos)
        (xs    : list (APred (Ev Var) protos)) :
    merge_guards atoms = (mk_Guarded gs (x::xs)) ->
    exists gs1 gs2 atoms',
      gs = gs1 ++ gs2 /\
      merge_guards atoms' = mk_Guarded gs2 xs.
  Proof.
    revert gs x xs.
    destruct atoms; try easy; intros.
    unfold merge_guards in *.
    cbn in H.
    destruct g as (g & a).
    destruct (split (map (fun '(mk_Guarded g a) => (g, a)) atoms)) as (g' & v') eqn:Hsplit.
    exists g, (concat g'), atoms.
    inversion H; subst.
    now rewrite Hsplit.
  Qed.

  Lemma merge_guards_atom_cons
        (a : GAPred Ep (Ev Var) protos)
        (atoms : list (GAPred Ep (Ev Var) protos)) :
    merge_guards (a::atoms) =
    {| g_guard := a.(g_guard) ++ (merge_guards atoms).(g_guard);
       g_item  := a.(g_item)  :: (merge_guards atoms).(g_item) |}.
  Proof.
    unfold merge_guards.
    cbn.
    now destruct (split (map (fun '(mk_Guarded g a0) => (g, a0)) atoms)) as (xs & ys), a as (ag & ai).
  Qed.

  Definition g_pattern_matches_list
             (ts   : list Tag)
             (view : list (GAPred Ep (Ev Var) protos))
    : list (Guarded (Ep (Ev Var)) (list (APred (Ev Var) protos))) :=
    map merge_guards (pattern_matches _ ts view).

  Definition g_pattern_matches
             (ts   : list Tag)
             (view : LNormVExpr (GAPred Ep (Ev Var) protos))
    : list (Guarded (Ep (Ev Var)) (list (APred (Ev Var) protos))) :=
    g_pattern_matches_list ts (listify_lnorm view).

  Definition g_pattern_matches_exact_list
             (ts   : list Tag)
             (view : list (GAPred Ep (Ev Var) protos))
    : list (Guarded (Ep (Ev Var)) (list (APred (Ev Var) protos))) :=
    map merge_guards (pattern_matches_exact _ ts view).

  (** [g_pattern_matches_exact] collects only the pattern matches with the
      same number of atoms as the original view. *)

  Definition g_pattern_matches_exact
             (ts   : list Tag)
             (view : LNormVExpr (GAPred Ep (Ev Var) protos))
    : list (Guarded (Ep (Ev Var)) (list (APred (Ev Var) protos))) :=
    g_pattern_matches_exact_list ts (listify_lnorm view).

  (** As such, the exact pattern matches are included in the normal
      pattern matches. *)

  Lemma g_pattern_matches_exact_incl
        (ts   : list Tag)
        (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    incl (g_pattern_matches_exact ts view)
         (g_pattern_matches       ts view).
  Proof.
    unfold g_pattern_matches, g_pattern_matches_exact, g_pattern_matches_exact_list.
    intros v (x & <- & Hin_exact%pattern_matches_exact_incl)%in_map_iff.
    apply in_map_iff.
    now (exists x).
  Qed.

  Lemma g_pattern_matches_empty_empty
        (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    g_pattern_matches [] view = [mk_Guarded [] []].
  Proof.
    reflexivity.
  Qed.

  (** Any non-empty pattern fails to match on an empty view.
      This version of the lemma is proof-irrelevant on the proof of
      list-normality for the empty view. *)

  Lemma g_pattern_matches_tags_empty'
        (view : LNormVExpr (GAPred Ep (Ev Var) protos))
        (t    : Tag)
        (ts   : list Tag):
    proj1_sig view = Unit _ ->
    g_pattern_matches (t::ts) view = [].
  Proof.
    unfold g_pattern_matches, g_pattern_matches_list, pattern_matches.
    intro Hview.
    destruct view as (v & Hv); unfold proj1_sig in Hview; subst.
    revert t.
    induction ts; try easy.
    specialize (IHts a).
    simpl in *.
    apply map_eq_nil in IHts.
    apply map_eq_nil in IHts.
    now rewrite IHts.
  Qed.

  (** This version assumes the proof used in [LUnit]. *)

  Corollary g_pattern_matches_tags_empty
            (t    : Tag)
            (ts   : list Tag) :
    g_pattern_matches (t::ts) (lunit _) = [].
  Proof.
    now apply g_pattern_matches_tags_empty'.
  Qed.

  (** The length of each pattern match is equal to the length of the tag
      list. *)

  Lemma g_pattern_matches_merged_length
        (ts   : list Tag)
        (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun '(mk_Guarded _ pmatch) => length pmatch = length ts)
      (g_pattern_matches ts view).
  Proof.
    apply Forall_forall.
    intros (g & xs) Hin.
    unfold g_pattern_matches, g_pattern_matches_list in Hin.
    apply in_map_iff in Hin.
    destruct Hin as (gs & Hmg & Hin).
    change xs with (g_item {| g_guard := g; g_item := xs |}).
    rewrite <- Hmg, <- merge_guards_length.
    apply (Forall_forall_in _ _ _ (pattern_matches_merged_length _ _ _) Hin).
  Qed.

  (** If we have a match for a pattern [t::ts], we can infer that there
      is a match for a pattern [ts] that is a sub-match of the original. *)

  Lemma g_pattern_matches_cons
        (t      : Tag)
        (ts     : list Tag)
        (view   : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun '(mk_Guarded guar pmatch) =>
         exists guar_1,
           Exists
             (fun '(mk_Guarded guar_2 ps) =>
                guar = guar_1 ++ guar_2 /\ tl pmatch = ps)
             (g_pattern_matches ts view))
      (g_pattern_matches (t::ts) view).
  Proof.
    apply Forall_forall.
    unfold g_pattern_matches, g_pattern_matches_list.
    intros (guar & pmatch) (mg & Hmg & Hin)%in_map_iff.
    pose proof (Forall_forall_in _ _ _ (pattern_matches_cons _ t ts _) Hin).
    cbn in H.
    generalize dependent pmatch.
    generalize dependent guar.
    induction mg.
    - exists [].
      inversion Hmg; subst.
      apply Exists_exists.
      exists (mk_Guarded [] []); intuition.
      apply in_map_iff.
      now (exists []).
    - intros guar pmatch Hmg.
      rewrite merge_guards_atom_cons in Hmg.
      inversion Hmg; subst.
      exists (g_guard a).
      apply Exists_exists.
      exists (merge_guards mg); intuition.
      + apply in_map, H.
      + now destruct (merge_guards mg).
  Qed.

  (** The tag of every atom in every match lines up with the tag at the same
      position in the pattern. *)

  Theorem g_pattern_matches_tags
          (ts   : list Tag)
          (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun '(mk_Guarded _ pmatch) =>
         Forall2
           (fun t a => t = proj1_sig (ap_tag a))
           ts
           pmatch)
      (g_pattern_matches ts view).
  Proof.
    apply Forall_forall.
    unfold g_pattern_matches, g_pattern_matches_list.
    intros (guar & pmatch) (mg & Hmg & Hin)%in_map_iff.
    pose proof (Forall_forall_in _ _ _ (pattern_matches_tags _ ts _) Hin) as Htags.
    simpl in *.
    assert (length ts = length mg) as Hlen'
        by now rewrite (Forall_forall_in _ _ _ (pattern_matches_merged_length _ _ _) Hin).
    assert (length ts = length pmatch) as Hlen.
    {
      change pmatch with (g_item {| g_guard := guar; g_item := pmatch |}).
      now rewrite <- Hmg, <- merge_guards_length.
    }
    clear Hlen'.
    clear Hin.
    generalize dependent mg.
    generalize dependent guar.
    apply list_pairwise_ind with (xs := ts) (ys := pmatch).
    - exact Hlen.
    - (* Base case is trivial. *)
      constructor.
    - intros t' ts' v' vs' Hind guar mg Hmg Htags.
      inversion Htags; subst.
      rewrite merge_guards_atom_cons in Hmg.
      destruct y as (yg & ya).
      cbn in Hmg.
      destruct (split (map (fun '(mk_Guarded g a) => (g, a)) l')) as (sg & sa) eqn:Hsplit.
      constructor.
      + now inversion Hmg; subst.
      + inversion Hmg; subst.
        inversion Htags; subst.
        apply (Hind (concat sg) l'); intuition.
        ++ unfold merge_guards.
           now rewrite Hsplit.
  Qed.

  Definition lists_mostly_equal : relation (list (GAPred Ep (Ev Var) protos)) :=
    Forall2 (gapred_mostly_equal _ _ (protos := protos)).

  Definition zippers_mostly_equal : relation (Zipper (GAPred Ep (Ev Var) protos)) :=
    Forall2Z (gapred_mostly_equal _ _ (protos := protos)).

  Global Instance tag_matches_Forall2Z_mostly_equal_Proper (t : Tag) :
    Proper (zippers_mostly_equal ==> Forall2 zippers_mostly_equal)
           (tag_matches Tag t (T := GAPred Ep (Ev Var) protos)).
  Proof.
    intros z1 z2 Hzmeq.
    apply tag_matches_Proper; intuition.
    destruct x as (gx & ix & Hix), y as (gy & iy & Hiy).
    destruct H as (Hg & Hi).
    cbn in *.
    congruence.
  Qed.

  Global Instance proc_tag_match_mostly_equal_Proper :
    Proper
      (lists_mostly_equal ==> zippers_mostly_equal
                          ==> (lists_mostly_equal * zippers_mostly_equal)%signature)
      proc_tag_match.
  Proof.
    intros pm1 pm2 Hpm [zl1 [|zc1 zr1]] [zl2 [|zc2 zr2]] (Hzl & Hzr); try easy;
      repeat split; unfold "@@", proc_tag_match in *; cbn; auto.
    - now dissolve_Forall2.
    - cbn in *.
      unfold lists_mostly_equal.
      now dissolve_Forall2.
    - cbn in *.
      now dissolve_Forall2.
  Qed.

  (** If two previous matches [pm1] and [pm2] are related by
        almost-equality across each atom, and the analogous zipper
        result holds for [z1] and [z2], then the same relations carry forward
        after [proc_tag_matches]. *)

  Global Instance proc_tag_matches_mostly_equal_Proper (t : Tag) :
    Proper
      (lists_mostly_equal ==> zippers_mostly_equal
                          ==> (Forall2 (lists_mostly_equal * zippers_mostly_equal)%signature))
      (proc_tag_matches Tag t).
  Proof.
    intros pm1 pm2 Hpm z1 z2 Hz.
    eapply Forall2_map.
    - apply tag_matches_Forall2Z_mostly_equal_Proper, Hz.
    - intros x y.
      apply proc_tag_match_mostly_equal_Proper, Hpm.
  Qed.

  Global Instance pattern_matches_iter_mostly_equal_Proper (t : Tag) :
    Proper
      ((Forall2 (lists_mostly_equal * zippers_mostly_equal)%signature)
         ==> (Forall2 (lists_mostly_equal * zippers_mostly_equal)%signature))
      (pattern_matches_iter Tag t).
  Proof.
    intros l1 l2 Hl.
    eapply Forall2_flat_map.
    - apply Hl.
    - intros (xl & xz) (yl & yz) (Hxyl & Hxyz).
      now apply proc_tag_matches_mostly_equal_Proper.
  Qed.

  Global Instance pattern_matches_inner_mostly_equal_Proper (ts : list Tag) :
    Proper
      (lists_mostly_equal
         ==> (Forall2 (lists_mostly_equal * zippers_mostly_equal)%signature))
      (pattern_matches_inner Tag ts).
  Proof.
    intros xs xs' Hxs.
    induction ts.
    - rewrite ! pattern_matches_inner_empty_tag.
      constructor; intuition.
      split.
      + constructor.
      + unfold "@@", snd.
        apply Forall2_Forall2Z_leftmost; intuition; apply list_leftmost.
    - rewrite pattern_matches_inner_cons_unfold.
      apply pattern_matches_iter_mostly_equal_Proper, IHts.
  Qed.

  Global Instance pattern_matches_mostly_equal_Proper (ts : list Tag) :
    Proper
      (lists_mostly_equal ==> Forall2 lists_mostly_equal)
      (pattern_matches Tag ts).
  Proof.
    intros xs xs' Hxs.
    eapply Forall2_map.
    - now apply pattern_matches_inner_mostly_equal_Proper.
    - now intros (x & xz) (y & yz) (Hxy & _).
  Qed.

  Definition merged_mostly_equal : relation (Guarded (Ep (Ev Var)) (list (APred (Ev Var) protos))) :=
    relation_conjunction (eq @@ @g_guard _ _)
                         (Forall2 (eq @@ @proj1_sig _ _) @@ @g_item _ _).

  Global Instance g_pattern_matches_mostly_equal_Proper (ts : list Tag) :
    Proper
      ((ForallAtoms2 (gapred_mostly_equal _ _ (protos := protos)) @@ forget_lnorm (A := _))
         ==>
         Forall2 merged_mostly_equal
      )
      (g_pattern_matches ts).
  Proof.
    intros (v1 & Hv1) (v2 & Hv2) Hequal%forall2_lnorm.
    apply (pattern_matches_mostly_equal_Proper ts) in Hequal.
    unfold "@@", g_pattern_matches.
    eapply Forall2_map.
    - apply Hequal.
    - intros x y.
      induction 1; cbn.
      + repeat constructor.
      + cbn in IHForall2.
        unfold "@@" in *.
        destruct H as (Hg & Hi).
        rewrite ! merge_guards_atom_cons; split; try constructor; try easy.
        cbn.
        now f_equal.
  Qed.

  Hypothesis Hep_dec : forall (e : Ep (Ev Var)) (s : Var -> EVal Ev), { pe_interp tt tt e s } + { ~ pe_interp tt tt e s }.

  (* Once again, we need to give Coq some help with the instances.
     (Fixing this would be smashing.) *)

  Instance gap_AtomLanguage
    : AtomLanguage (GAPred Ep (Ev Var) protos)
                   ((Var -> EVal Ev) -> LMultiset (APred (EVal Ev) protos)) :=
    guard_AtomLanguage
      Hep_dec
      (SV := lms_Setoid protos tag_dec).

  Instance lmse_Setoid : Setoid (VExpr (GAPred Ep (Ev Var) protos)) :=
    VExpr_Setoid
      (AL := gap_AtomLanguage)
      (SV := fun_Setoid (SV := lms_Setoid protos tag_dec)).

  Instance lmse_ViewsSemigroup : ViewsSemigroup (VExpr (GAPred Ep (Ev Var) protos)) :=
    VExpr_ViewsSemigroup (AL := gap_AtomLanguage).

  Instance lmse_ViewsMonoid : ViewsMonoid (VExpr (GAPred Ep (Ev Var) protos)) :=
    VExpr_ViewsMonoid (AL := gap_AtomLanguage).

  Instance lmse_OrderedViewsSemigroup : OrderedViewsSemigroup (VExpr (GAPred Ep (Ev Var) protos)) :=
    VExpr_OrderedViewsSemigroup (AL := gap_AtomLanguage).

  Instance lmse_SubtractiveViewsSemigroup : AdjView (VExpr (GAPred Ep (Ev Var) protos)) :=
    VExpr_SubtractiveViewsSemigroup (AL := gap_AtomLanguage).

  Instance lmse_FullViewsSemigroup : FullView (VExpr (GAPred Ep (Ev Var) protos)) :=
    VExpr_FullViewsSemigroup (AL := gap_AtomLanguage).

  Definition view_of_zipper :
    Zipper (GAPred Ep (Ev Var) protos) ->
    VExpr (GAPred Ep (Ev Var) protos) :=
    forget_lnorm (A := _) ∘ unlistify_lnorm (A := _) ∘ Zipper_to_list (A := _).

  (** Every time we run a zipper through [proc_tag_matches], the atom that
      moves into the new match is the one that we remove from the zipper. *)

  Lemma proc_tag_matches_view_zipper_rem
        (t : Tag)
        (prev_match : list (GAPred Ep (Ev Var) protos))
        (prev_zip : Zipper (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun '(new_match, new_zip) =>
         exists a : GAPred Ep (Ev Var) protos,
           hd_error new_match = Some a /\
           equiv (acons a (view_of_zipper new_zip))
                 (view_of_zipper prev_zip))
      (proc_tag_matches _ t prev_match prev_zip).
  Proof.
    apply Forall_forall.
    intros (m & z) Hin.
    (* By appeal to the general/permutation version. *)
    destruct
      (Forall_forall_in _ _ _ (proc_tag_matches_zipper_rem _ _ _ _) Hin) as (a & Hhd & Hperm).
    exists a; intuition.
    unfold view_of_zipper, compose.
    rewrite <- lcons_forget.
    apply listify_Permutation.
    now rewrite listify_lnorm_cons, !unlistify_listify_lnorm.
  Qed.

  (** As we apply [proc_tag_matches], the join of each match and each
      zipper remains invariant, and the match grows monotonically. *)

  Lemma proc_tag_matches_view_dist
        (t : Tag)
        (prev_match : list (GAPred Ep (Ev Var) protos))
        (prev_zip : Zipper (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun '(new_match, new_zip) =>
         equiv
           (forget_lnorm (unlistify_lnorm new_match) *
            view_of_zipper new_zip)
           (forget_lnorm (unlistify_lnorm prev_match) *
            view_of_zipper prev_zip)
      )%views
      (proc_tag_matches _ t prev_match prev_zip).
  Proof.
    apply Forall_forall.
    intros (new_match & new_zip) Hin.
    setoid_rewrite <- lapp_equiv.
    rewrite <- !unlistify_lnorm_app.
    apply listify_Permutation.
    rewrite !unlistify_listify_lnorm.
    apply (Forall_forall_in _ _ _ (proc_tag_matches_dist _ _ _ _) Hin).
  Qed.

  (** This property carries on to [pattern_matches_iter]... *)

  Lemma pattern_matches_iter_view_dist
        (t : Tag)
        (prev : list (list (GAPred Ep (Ev Var) protos) * Zipper (GAPred Ep (Ev Var) protos))) :
    Forall
      (fun '(new_match, new_zip) =>
         Exists
           (fun '(prev_match, prev_zip) =>
              equiv
                (forget_lnorm (unlistify_lnorm new_match) *
                 view_of_zipper new_zip)
                (forget_lnorm (unlistify_lnorm prev_match) *
                 view_of_zipper prev_zip)
           )%views
           prev)
      (pattern_matches_iter _ t prev).
  Proof.
    apply Forall_forall.
    intros (new_match, new_zip) Hin.
    pose proof (Forall_forall_in _ _ _ (pattern_matches_iter_dist _ _ _) Hin) as
        ((ematch & ezip) & Hein & Hperm)%Exists_exists.
    apply Exists_exists.
    exists (ematch, ezip); intuition.
    setoid_rewrite <- lapp_equiv.
    rewrite <- !unlistify_lnorm_app.
    apply listify_Permutation.
    now rewrite !unlistify_listify_lnorm.
  Qed.

  (** ... and [pattern_matches_inner]. *)

  Lemma pattern_matches_inner_view_dist
        (ts   : list Tag)
        (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun '(new_match, new_zip) =>
         equiv
           (forget_lnorm (unlistify_lnorm new_match) * view_of_zipper new_zip)%views
           (forget_lnorm view))
      (pattern_matches_inner _ ts (listify_lnorm view)).
  Proof.
    apply Forall_forall.
    intros (new_match, new_zip) Hin.
    setoid_rewrite <- lapp_equiv.
    rewrite <- !unlistify_lnorm_app.
    apply listify_Permutation.
    rewrite !unlistify_listify_lnorm.
    now apply (Forall_forall_in _ _ _ (pattern_matches_inner_dist _ _ _) Hin).
  Qed.

  (** As a result of the above, we know that the matches put out by
      [pattern_matches_inner] are always subviews of the input view. *)

  Corollary pattern_matches_inner_subview
            (ts   : list Tag)
            (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun '(new_match, _) =>
         forget_lnorm (unlistify_lnorm new_match) <<=
                      forget_lnorm view)
      (pattern_matches_inner _ ts (listify_lnorm view)).
  Proof.
    apply Forall_forall.
    intros (new_match & new_zip) Hin.
    rewrite <- (proj1 (Forall_forall _ _) (pattern_matches_inner_view_dist _ _) _ Hin).
    apply inc_dot_mono_left.
  Qed.

  (** Finally, we get that the pattern matches coming out of
      [pattern_matches] are merged subviews of the input. *)

  Theorem g_pattern_matches_subview
          (ts   : list Tag)
          (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun result =>
         exists result_unmerged,
           result = merge_guards result_unmerged /\
           (forget_lnorm (unlistify_lnorm result_unmerged)
                         <<= forget_lnorm view))
      (g_pattern_matches ts view).
  Proof.
    apply Forall_forall.
    unfold g_pattern_matches, g_pattern_matches_list, pattern_matches.
    intros result (lg & <- & ((result_unmerged & zip) & <- & Hin)%in_map_iff)%in_map_iff.
    pose proof (proj1 (Forall_forall _ _) (pattern_matches_inner_subview _ _) _ Hin).
    now (exists result_unmerged).
  Qed.

  (** If we only consider matches with exactly the same length as the
      original view, such matches are equivalent to said view.
      This lets us use a guarded syntactic definer to define views,
      not reify them. *)

  Lemma pattern_matches_inner_subview_define
        (ts   : list Tag)
        (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    Forall
      (* We arrange this in a slightly different way from usual to
         make stating the final result easier. *)
      (fun new_match =>
         length (fst new_match) =? length (listify_lnorm view) = true ->
         forget_lnorm (unlistify_lnorm (fst new_match)) ==
         forget_lnorm view)
      (pattern_matches_inner _ ts (listify_lnorm view)).
  Proof.
    apply Forall_forall.
    intros (new_match & new_zip) Hin.
    (* Effectively, what we need to do here is show that the zipper is empty
       at the end of pattern matching. *)
    rewrite <- (proj1 (Forall_forall _ _) (pattern_matches_inner_view_dist _ _) _ Hin).
    pose proof (proj1 (Forall_forall _ _) (pattern_matches_inner_combined_length _ _ _) _ Hin) as <-.
    intros Hlnmatch%Nat.eqb_eq.
    symmetry in Hlnmatch.
    rewrite <- Nat.add_0_r in Hlnmatch.
    apply Nat.add_cancel_l in Hlnmatch.
    unfold Zipper_length in Hlnmatch.
    destruct new_zip as ([|] & [|]); try easy.
    apply dot_one_r.
  Qed.

  Theorem g_pattern_matches_exact_subview_define
          (ts   : list Tag)
          (view : LNormVExpr (GAPred Ep (Ev Var) protos)) :
    Forall
      (fun result =>
         exists result_unmerged,
           result = merge_guards result_unmerged /\
           (forget_lnorm (unlistify_lnorm result_unmerged)
            == forget_lnorm view))
      (g_pattern_matches_exact ts view).
  Proof.
    apply Forall_forall.
    unfold g_pattern_matches_exact, g_pattern_matches_exact_list, pattern_matches_exact, pattern_matches.
    intros result (rm & <- & (((result_unmerged & zip) & <- & Hin)%in_map_iff & Hfilter)%filter_In)%in_map_iff.
    exists result_unmerged; intuition.
    now apply
        (Forall_forall_in _ _ _ (pattern_matches_inner_subview_define _ _) Hin).
  Qed.
End APred_gmatch.

Section APred_gmatch_mapping.

  (** Results about guarded pattern matches and tag-preserving maps.

        These extend the analogous section from [Match]. *)

  Variables
    Tag
    Val
    Var_1 Var_2 : Type.

  Variable
    Ev_1 Ev_2
    Ep_1 Ep_2 : Type -> Type.

  Variables
    (protos : Tag -> option TagProto)
  .

  Context
    {S_Val  : Setoid Val}
    {D_Val  : EqDec S_Val}
    {V_Ev1  : ValEx Ev_1 (Val := Val)}
    {V_Ev2  : ValEx Ev_2 (Val := Val)}
    {T_Tag  : Tagger Tag}
    {P_EEp1 : EqPredEx unit unit Ep_1 (Val := Val)}
    {P_EEp2 : EqPredEx unit unit Ep_2 (Val := Val)}.

  Variable f
    : (Ev_1 Var_1) ->
      (Ev_2 Var_2).

  Variable g : Ep_1 (Ev_2 Var_2) -> Ep_2 (Ev_2 Var_2).

  Local Definition fog : Ep_1 (Ev_1 Var_1) -> Ep_2 (Ev_2 Var_2) :=
    g ∘ fmap f.

  Definition map_matches
             (x : Guarded (Ep_1 (Ev_1 Var_1)) (list (APred (Ev_1 Var_1) protos))) :
             Guarded (Ep_2 (Ev_2 Var_2)) (list (APred (Ev_2 Var_2) protos)) :=
    {| g_guard := map fog x.(g_guard);
       g_item := map (apred_map f) x.(g_item)
    |}.

  (** TODO(@MattWindsor91): propagate these to GStarling. *)

  Definition map_gapred
             (x : GAPred Ep_1 (Ev_1 Var_1) protos) :
             (GAPred Ep_2 (Ev_2 Var_2) protos) :=
    {| g_guard := map fog x.(g_guard);
       g_item := apred_map f x.(g_item)
    |}.

  Lemma map_matches_gapred_merge (xs : list (GAPred Ep_1 (Ev_1 Var_1) protos)) :
    merge_guards _ _ _ _ _ (map map_gapred xs) =
    map_matches (merge_guards _ _ _ _ _ xs).
  Proof.
    induction xs; eauto.
    cbn.
    rewrite ! merge_guards_atom_cons, ! IHxs.
    unfold map_matches.
    cbn.
    now rewrite map_app.
  Qed.

  Lemma map_gapred_tag_preserve (x : GAPred Ep_1 (Ev_1 Var_1) protos) :
    tag_of x = tag_of (map_gapred x).
  Proof.
    intuition.
  Qed.

  Lemma g_pattern_matches_list_map_back (ts : list Tag) (input : list (GAPred Ep_1 (Ev_1 Var_1) protos)) :
    Forall
      (fun m1 =>
         Exists
           (fun m2 => m1 = map_matches m2)
           (g_pattern_matches_list _ _ _ _ protos ts input)
      )
      (g_pattern_matches_list _ _ _ _ protos ts (map map_gapred input)).
  Proof.
    apply Forall_forall.
    intros (xg & xr) (x2 & <- & Hin_x2)%in_map_iff.
    apply Exists_exists.
    pose proof (Forall_forall_in _ _ _ (pattern_matches_map_back _ _ map_gapred_tag_preserve _ _) Hin_x2)
      as (x1 & Hin_x1 & ->)%Exists_exists.
    exists (merge_guards _ _ _ _ _ x1); split.
    - now apply in_map.
    - apply map_matches_gapred_merge.
  Qed.

  Corollary g_pattern_matches_map_back (ts : list Tag) (input : LNormVExpr (GAPred Ep_1 (Ev_1 Var_1) protos)) :
    Forall
      (fun m1 =>
         Exists
           (fun m2 => m1 = map_matches m2)
           (g_pattern_matches _ _ _ _ protos ts input)
      )
      (g_pattern_matches _ _ _ _ protos ts (lvmap map_gapred input)).
  Proof.
    unfold g_pattern_matches.
    rewrite lvmap_map_listify.
    apply g_pattern_matches_list_map_back.
  Qed.
End APred_gmatch_mapping.