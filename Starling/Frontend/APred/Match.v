(** * Abstract predicates: Core pattern matching

    This contains the bits of pattern matching that aren't dependent on
    a particular abstract predicate format. *)

From Coq Require Import
     Arith
     FunInd
     Lists.List
     Program.Basics
     Recdef
     Relations
     RelationPairs
     Classes.SetoidDec
     Classes.SetoidClass
     Omega
.

From Starling Require Import
     Backend.Alpha.Classes
     Backend.Classes
     Views.Expr.Guarded
     Frontend.APred.Core
     Frontend.APred.Taggable
     Utils.List.Facts
     Utils.Option.Facts
     Utils.Zipper
.

Set Universe Polymorphism.

Section APred_match.
  Import ListNotations.
  Local Open Scope program_scope.
  Local Open Scope views_scope.

  Polymorphic Universe g.
  Variable Tag : Type@{g}.

  Section tag_matches.
    Polymorphic Universe t.

    Context
      {T : Type@{t}}
      {TR : Tagger Tag}
      {TT : Taggable Tag T}.

    (** [has_tag_match t] is a decision process for working out if the
        cursor of [vzip] contains an atom whose tag matches [t]. *)

    Definition has_tag_match (t : Tag) (vzip : Zipper T) : bool :=
      match vzip.(cur) with
      | Some m =>
        if tag_dec t (tag_of m) then true else false
      | None => false
      end.

    (** [tag_matches] calculates all of the positions at which a tag appears in
        a view. *)

    Definition tag_matches (t : Tag) (vzip : Zipper T) : list (Zipper T) :=
      filter (has_tag_match t) (all_right_shifts vzip).

    Section tag_matches_facts_same_zip.
      Variables
        (t    : Tag)
        (vzip : Zipper T).

      Lemma tag_matches_maybe_head_rec (m : T) :
        vzip.(cur) = Some m ->
        tag_matches t vzip =
        (if (tag_dec t (tag_of m)) then [vzip] else [])
          ++ tag_matches t (move_right vzip).
        destruct vzip as (vl & [|vc vr]); cbn; try easy.
        inversion_clear 1.
        unfold tag_matches.
        rewrite all_right_shifts_not_rightmost with (m := m); try easy.
        cbn.
        now destruct (tag_dec t (tag_of m)).
      Qed.

      (** If the head of the input zipper has the expected tag,
          the output is as follows: *)

      Corollary tag_matches_head_rec (m : T) :
        vzip.(cur) = Some m ->
        tag_of m = t ->
        tag_matches t vzip = vzip::tag_matches t (move_right vzip).
      Proof.
        intros Hrec%tag_matches_maybe_head_rec.
        rewrite Hrec.
        intros ->.
        now destruct (tag_dec t t).
      Qed.

      (** If the head of the input zipper has the expected tag,
          it appears directly in the match output. *)

      Corollary tag_matches_head (m : T) :
        vzip.(cur) = Some m ->
        tag_of m = t ->
        List.In vzip (tag_matches t vzip).
      Proof.
        intros Hcur Htag.
        rewrite (tag_matches_head_rec m Hcur Htag).
        apply in_eq.
      Qed.

      (** If the zipper is rightmost, there is no output. *)

      Lemma tag_matches_rightmost :
        rightmost vzip ->
        tag_matches t vzip = [].
      Proof.
        intro Hrm.
        destruct vzip as (vl & vr) eqn:Hz.
        unfold rightmost in Hrm.
        cbn in Hrm.
        now subst vr.
      Qed.

      (** Every match returned for the rightwards move of the
          zipper appears in the output for the zipper itself. *)

      Lemma tag_matches_move_right :
        Forall
          (ni (tag_matches t vzip))
          (tag_matches t (move_right vzip)).
      Proof.
        apply Forall_forall.
        red.
        unfold tag_matches.
        intros x (Hin & Htag)%filter_In.
        apply filter_In.
        intuition.
        (* Show that [vzip] can't be rightmost. *)
        destruct vzip as (zl & [|zc zr]); intuition.
        rewrite all_right_shifts_not_rightmost with (m := zc); intuition.
      Qed.

      (** Every zipper returned by [tag_matches] has a cursor pointing at
          an abstract predicate, and that predicate has the given tag. *)

      Lemma tag_matches_zip_cur :
        Forall
          (fun z => exists m, z.(cur) = Some m /\ tag_of m = t)
          (tag_matches t vzip).
      Proof.
        unfold tag_matches.
        apply Forall_filter, Forall_forall.
        intros x _ Htag_match.
        unfold has_tag_match in Htag_match.
        destruct (cur x); intuition.
        destruct (tag_dec t (tag_of t0)); intuition.
        now exists t0.
      Qed.

      (** All of the matches produced by [tag_matches] are right
          shifts of the input. *)

      Lemma tag_matches_right_shift :
        Forall
          (fun z' => is_right_shift z' vzip)
          (tag_matches t vzip).
      Proof.
        apply Forall_filter.
        apply Forall_impl with (P := fun a => is_right_shift a vzip); intuition.
        apply in_all_right_shifts_is_right_shift.
      Qed.

      (** All possible right shifts of [vzip] with the appropriate tag
          appear in [tag_matches]. *)

      Lemma tag_matches_all_right_shifts (rs : Zipper T) (i : T) :
        is_right_shift rs vzip ->
        cur rs = Some i ->
        tag_of i = t ->
        In rs (tag_matches t vzip).
      Proof.
        intros Hrs Hcur Htag.
        apply filter_In.
        split.
        - now apply is_right_shift_in_all_right_shifts.
        - unfold has_tag_match.
          subst.
          rewrite Hcur.
          now destruct (tag_dec (tag_of i) (tag_of i)).
      Qed.

      (** Every item in [vzip]'s right list that has the correct tag is
          pointed to by a cursor in one of the returned zippers. *)

      Lemma tag_matches_all_zips :
        Forall
          (fun a => tag_of a = t ->
                 Exists
                   (fun z => z.(cur) = Some a)
                   (tag_matches t vzip))
          (vzip.(zp_right)).
      Proof.
        apply Forall_impl with (P := (fun a => exists y, is_right_shift y vzip /\ cur y = Some a)).
        - intros a (y & Hrs & Hcur) Htag.
          apply Exists_exists.
          exists y; intuition.
          now apply tag_matches_all_right_shifts with (i := a).
        - apply in_right_right_shift.
      Qed.

      (** All of the matches produced by [tag_matches] have the same
          list extraction. *)

      Lemma tag_matches_lists :
        Forall (Zipper_lists_eq vzip) (tag_matches t vzip).
      Proof.
        (* All right shifts have the same zipper list. *)
        apply Forall_impl with (P := fun z => is_right_shift z vzip).
        - now intros z (Hrz_lists & Hrz_lengths).
        - apply tag_matches_right_shift.
      Qed.

      (** Every zipper returned by [tag_matches] has the same length as
          the input zip. *)

      Corollary tag_matches_zip_length :
        Forall
          (fun z => z.(Zipper_length) = vzip.(Zipper_length))
          (tag_matches t vzip).
      Proof.
        (* All right shifts have the same zipper length. *)
        apply Forall_impl with (P := Zipper_lists_eq vzip).
        - intros z.
          rewrite ! zipper_list_length.
          congruence.
        - apply tag_matches_lists.
      Qed.
    End tag_matches_facts_same_zip.

    (** As a result, we can swap out the left part of an input zipper,
        and the resulting changes will only affect said left part. *)

    Lemma tag_matches_left_swap (t : Tag) (vzip : Zipper T) (zl' : list T) :
      Forall
        (fun m =>
           exists prefix,
             In (replace_left m (prefix ++ zl'))
                (tag_matches t (replace_left vzip zl')))
        (tag_matches t vzip).
    Proof.
      apply Forall_forall.
      intros x Hin_x.
      (* First, show that [x] is a right shift of [vzip]. *)
      pose proof (Forall_forall_in _ _ _ (tag_matches_right_shift _ _) Hin_x) as Hrs.
      (* This, in turn, tells us that there exists a [prefix] such that
         the replacement we're looking for in [tag_matches] is a right shift of
         [replace_left vzip zl']. *)
      destruct (right_shift_replace zl' Hrs) as (prefix & Hprefix_rs).
      (* Since [x] is a match, it'll have a [cur] with the correct tag. *)
      destruct (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _) Hin_x) as (i & Hcur & Htag).
      exists prefix.
      (* Now we just need to show that this particular right shift is in
         the matches. *)
      now apply tag_matches_all_right_shifts with (i := i).
    Qed.

    (** If we add a new atom onto the start, it doesn't affect the
        existing matches if we've already walked past it. *)

    Lemma tag_matches_push_left
          (t : Tag)
          (a : T)
          (z : Zipper T) :
      Forall
        (fun z' => List.In (push_left z' a) (tag_matches t (push_left z a)))
        (tag_matches t z).
    Proof.
      apply Forall_forall.
      intros x (Hrs%all_right_shifts_iff & Htag)%filter_In.
      apply filter_In; intuition.
      now apply all_right_shifts_iff, right_shift_push_left.
    Qed.

    (** If we add a new atom onto the end of a view, it doesn't affect the
        existing matches. *)

    Lemma tag_matches_push_right
          (t : Tag)
          (a : T)
          (z : Zipper T) :
      Forall
        (fun z' => List.In (push_right z' a) (tag_matches t (push_right z a)))
        (tag_matches t z).
    Proof.
      apply Forall_forall.
      intros x (Hrs%all_right_shifts_iff & Htag)%filter_In.
      apply filter_In; intuition.
      - now apply all_right_shifts_iff, right_shift_push_right.
      - now destruct x as (xl & [|]).
    Qed.

    Global Instance Forall2_filter_Proper {A} (R : relation A) (P : A -> bool):
      (forall x y, R x y -> P x = P y) ->
      Proper (Forall2 R ==> Forall2 R) (filter P).
    Proof.
      intros Hrp.
      red.
      unfold "==>"%signature.
      apply Forall2_ind; cbn; try easy.
      intros x y xs ys Hxy Hxys Hind.
      rewrite (Hrp x y Hxy).
      destruct (P y); intuition.
    Qed.

    Lemma tag_of_eq_Forall2Z (R : relation T) (t : Tag) (xs ys : Zipper T) :
      (forall x y : T, R x y -> tag_of x = tag_of y) ->
      Forall2Z R xs ys ->
      has_tag_match t xs = has_tag_match t ys.
    Proof.
      intros Htags (Hxs & Hys).
      destruct xs as (xl & xr), ys as (yl & yr).
      unfold "@@" in *.
      unfold has_tag_match, cur.
      cbn in *.
      clear Hxs xl yl.
      induction Hys; cbn; intuition.
      now rewrite (Htags x y H).
    Qed.

    Lemma tag_matches_Proper (R : relation T) (t : Tag) :
      (forall x y, R x y -> tag_of x = tag_of y) ->
      Proper (Forall2Z R ==> Forall2 (Forall2Z R)) (tag_matches t).
    Proof.
      intros Htags (xl & xr) (yl & yr) (Hrl & Hrr).
      apply Forall2_filter_Proper.
      - intros xs ys.
        now apply tag_of_eq_Forall2Z.
      - now apply all_right_shifts_Proper.
    Qed.
  End tag_matches.

  Section proc_tag_matches.
    Context
      {T : Type}
      {TR : Tagger Tag}
      {TT : Taggable Tag T}.

    (** [proc_tag_match] processes a single match [z] from a run of
        [tag_matches], shuffling the matched atom onto the existing match
        [prev_match] and removing it from [z] ready for the next run. *)

    Definition proc_tag_match
               (prev_match : list T)
               (z : Zipper T) :
      (list T * Zipper T) :=
      maybe
        ([], rewind z) (* This won't happen in practice -- see above. *)
        (fun c => (c :: prev_match, rewind (rem z)))
        z.(cur).

    (** The returned zipper from [proc_tag_matches] is always the result of
        [rem]ing and [rewind]ing the original zipper.
        (Recall that if the original zipper is at the end, [rem] is
        idempotent.) *)

    Lemma proc_tag_match_zipper_rem_rewind
          (prev_match : list T)
          (z : Zipper T) :
      snd (proc_tag_match prev_match z) = rewind (rem z).
    Proof.
      unfold proc_tag_match, maybe.
      destruct (cur z) eqn:H; try easy.
      cbn.
      f_equal.
      unfold rem, cur in *.
      now destruct (zp_right z).
    Qed.

    Definition proc_tag_matches
               (t : Tag)
               (prev_match : list T)
               (vzip : Zipper T)
      : list (list T * Zipper T) :=
      map (proc_tag_match prev_match) (tag_matches t vzip).

    (** The head of each [GAPred] list returned by [proc_tag_matches]
        exists and has tag [t] (soundness). *)

    Lemma proc_tag_matches_head_tag
          (t : Tag)
          (prev_match : list T)
          (vzip : Zipper T) :
      Forall
        (fun '(v, z) =>
           exists a, hd_error v = Some a /\ tag_of a = t)
        (proc_tag_matches t prev_match vzip).
    Proof.
      apply Forall_forall.
      intros (v & nzip) (x & Hx & Hin)%in_map_iff.
      unfold proc_tag_matches, proc_tag_match in *.
      pose proof (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _) Hin) as (c & Hcur & Htag).
      rewrite Hcur in Hx.
      inversion_clear Hx.
      now exists c.
    Qed.

    (** Each [GAPred] list returned by [proc_tag_matches]
        represents [prev_match] with the cursor of [vzip] moved onto
        it. *)
    Lemma proc_tag_matches_prev
          (t : Tag)
          (prev_match : list T)
          (vzip : Zipper T) :
      Forall
        (fun '(v, z) =>
           exists z',
             z = rewind (rem z') /\ Zipper_to_list z' = Zipper_to_list vzip /\
             hd_error v = cur z' /\ tl v = prev_match
        )
        (proc_tag_matches t prev_match vzip).
    Proof.
      apply Forall_forall.
      intros (v & nzip) (x & Hx & Hin)%in_map_iff.
      unfold proc_tag_matches, proc_tag_match in *.
      pose proof (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _) Hin) as (c & Hcur & Htag).
      rewrite Hcur in Hx.
      inversion_clear Hx.
      exists x; firstorder.
      pose proof (Forall_forall_in _ _ _ (tag_matches_lists _ _) Hin).
      congruence.
    Qed.

    Corollary proc_tag_matches_split
              (t : Tag)
              (prev_match : list T)
              (vzip : Zipper T) :
      Forall
        (fun '(v, _) => exists a, v = a :: prev_match)
        (proc_tag_matches t prev_match vzip).
    Proof.
      apply Forall_forall.
      intros (v, nzip) Hin.
      destruct (proj1 (Forall_forall _ _) (proc_tag_matches_head_tag t prev_match vzip) (v, nzip) Hin)
        as (a & Hhd & _).
      pose proof (proj1 (Forall_forall _ _) (proc_tag_matches_prev t prev_match vzip) (v, nzip) Hin) as (z & -> & _ & _ & Htl).
      exists a.
      now rewrite (proj1 (hd_error_tl_repr v _ _) (conj Hhd Htl)).
    Qed.

    (** Each returned list thus has a length one higher than the previous
        match. *)

    Lemma proc_tag_matches_length_mono
          (t : Tag)
          (prev_match : list T)
          (vzip : Zipper T) :
      Forall
        (fun '(v, _) => length v = S (length prev_match))
        (proc_tag_matches t prev_match vzip).
    Proof.
      apply Forall_forall.
      intros (v & nzip) Hin.
      now destruct (proj1 (Forall_forall _ _) (proc_tag_matches_split _ _ _) _ Hin)
        as (a & ->).
    Qed.

    Lemma proc_tag_matches_all_right_shifts
          (t : Tag)
          (a : T)
          (prev_match : list T)
          (rs vzip : Zipper T) :
      is_right_shift rs vzip ->
      cur rs = Some a ->
      tag_of a = t ->
      In (a::prev_match, rewind (rem rs))
         (proc_tag_matches t prev_match vzip).
    Proof.
      intros Hrs Hcur Htag.
      pose proof (tag_matches_all_right_shifts t vzip rs a Hrs Hcur Htag).
      unfold proc_tag_matches.
      apply in_map_iff.
      exists rs; intuition.
      unfold proc_tag_match.
      now rewrite Hcur.
    Qed.

    (** Each atom in [vzip]'s right list that has the correct tag appears in the
        head of a [proc_tag_matches] match list (completeness). *)

    Lemma proc_tag_matches_all_atoms
          (t : Tag)
          (prev_match : list T)
          (vzip : Zipper T) :
      Forall
        (fun a =>
           tag_of a = t ->
           (exists z,
             In (a::prev_match, rewind (rem z))
                (proc_tag_matches t prev_match vzip) /\
             cur z = Some a /\
             Zipper_lists_eq vzip z))
        (vzip.(zp_right)).
    Proof.
      apply Forall_forall.
      intros x Hin_right Htag.
      pose proof (Forall_forall_in _ _ _ (in_right_right_shift _) Hin_right)
        as (rs & His_rs & Hcur).
      exists rs.
      split; intuition.
      - now apply proc_tag_matches_all_right_shifts.
      - apply (symmetry (proj1 His_rs)).
    Qed.

    (** Each returned zipper has a length one lower than the previous zipper
        (or zero, whichever is higher). *)

    Lemma proc_tag_matches_length_zip_mono
          (t : Tag)
          (prev_match : list T)
          (vzip : Zipper T) :
      Forall
        (fun '(_, z) => Zipper_length z = pred (Zipper_length vzip))
        (proc_tag_matches t prev_match vzip).
    Proof.
      apply Forall_forall.
      intros (v & nzip) Hin.
      (* Now work out that the zipper is a remove-rewind, and, as such, has
       one less length. *)
      unfold proc_tag_matches in Hin.
      apply in_map_iff in Hin.
      destruct Hin as (z & Hz & Hin_z).
      pose proof (proc_tag_match_zipper_rem_rewind prev_match z) as Hrem.
      rewrite Hz in Hrem.
      unfold snd in Hrem.
      rewrite Hrem, <- rewind_length_preserve.
      pose proof (proj1 (Forall_forall _ _) (tag_matches_zip_length _ _) _ Hin_z) as Hlen.
      rewrite rem_length, ! Nat.add_pred_r, <- Hlen.
      - easy.
      - (* Since we know that [z] came from [tag_matches], we know that it must have
         at least one item in its right list---the matched atom! *)
        destruct (proj1 (Forall_forall _ _ ) (tag_matches_zip_cur _ _) _ Hin_z) as
            (m & Hm_z & _).
        unfold cur in Hm_z.
        unfold Zipper_length_r.
        now destruct z as (zl & [|]).
    Qed.

    Lemma proc_tag_matches_leftmost_zips
          (t : Tag) (prev_match : list T) (pzip : Zipper T) :
      Forall
        (@leftmost _ ∘ snd)
        (proc_tag_matches t prev_match pzip).
    Proof.
      unfold proc_tag_matches.
      induction (tag_matches t pzip); constructor.
      - unfold compose.
        rewrite proc_tag_match_zipper_rem_rewind.
        apply rewind_leftmost.
      - apply IHl.
    Qed.
  End proc_tag_matches.

  Section pattern_matches_iter.
    Context
      {T  : Type}
      {TR : Tagger Tag}
      {TT : Taggable Tag T}.

    Definition pattern_matches_iter (t : Tag) :
      list (list T * Zipper T) ->
      list (list T * Zipper T) :=
      flat_map (prod_curry (proc_tag_matches t)).

    Section pattern_matches_iter_facts.
      Variables
        (t    : Tag)
        (prev : list (list T * Zipper T))
      .

      (** The head of each [GAPred] list returned by [pattern_matches_iter]
        exists and has tag [t]. *)

      Lemma pattern_matches_iter_head_tag :
        Forall
          (fun '(v, _) => exists a, hd_error v = Some a /\ tag_of  a = t)
          (pattern_matches_iter t prev).
      Proof.
        apply Forall_forall.
        intros (v & nzip) Hin.
        unfold pattern_matches_iter in *.
        induction prev; try easy.
        apply in_app_or in Hin.
        destruct Hin as [Hin_a_matches|Hin_rec]; intuition.
        destruct a as (prev_match & vzip).
        now apply (proj1 (Forall_forall _ _) (proc_tag_matches_head_tag t prev_match vzip) (v, nzip)).
      Qed.

      (** Every match-in-progress and zipper in a round of
          [pattern_matches_iter] is the result of shifting an element
          of a zipper in [prev] onto its respective match-in-progress. *)

      Lemma pattern_matches_iter_prev :
        Forall
          (fun '(v, z) =>
             Exists
               (fun '(v', z') =>
                  exists z'',
                  z = rewind (rem z'') /\ Zipper_lists_eq z'' z' /\
                  hd_error v = cur z'' /\ tl v = v') prev)
               (pattern_matches_iter t prev).
      Proof.
        apply Forall_forall.
        intros (v & nzip) Hin.
        apply Exists_exists.
        induction prev; try easy.
        apply in_app_or in Hin.
        destruct Hin as [Hin_a_matches|Hin_rec].
        - exists a; intuition.
          destruct a as (prev_match & vzip).
          unfold prod_curry in Hin_a_matches.
          apply (proj1 (Forall_forall _ _) (proc_tag_matches_prev t prev_match vzip) (v, nzip) Hin_a_matches).
        - destruct (IHl Hin_rec) as (x & Hx_in & Hx_tl).
          exists x; intuition.
      Qed.

      Corollary pattern_matches_iter_split :
        Forall
          (fun '(v, z) =>
             Exists
               (fun '(v', z') =>
                  exists a z'',
                    z = rewind (rem z'') /\ Zipper_lists_eq z'' z' /\
                    Some a = cur z'' /\
                    v = a :: v')
               prev
          )
          (pattern_matches_iter t prev).
      Proof.
        apply Forall_forall.
        intros (v, nzip) Hin.
        destruct (proj1 (Forall_forall _ _) pattern_matches_iter_head_tag (v, nzip) Hin)
          as (a & Hhd & _).
        pose (proj1 (Forall_forall _ _) pattern_matches_iter_prev (v, nzip) Hin) as Htl.
        apply Exists_exists in Htl.
        destruct Htl as ((preds & zips) & in_prev & (z & -> & Heq & Hcz & Htl)).
        apply Exists_exists.
        exists (preds, zips); intuition.
        rewrite (proj1 (hd_error_tl_repr v _ _) (conj Hhd Htl)).
        exists a, z; repeat split; congruence.
      Qed.

      Lemma pattern_matches_iter_empty :
        pattern_matches_iter t [] = [].
      Proof.
        reflexivity.
      Qed.

      (** If the length of all pattern matches entering
      [pattern_matches_iter] is the same, all pattern matches
      exiting [pattern_matches_iter] have a length one higher. *)

      Lemma pattern_matches_iter_length (n : nat) :
        Forall
          (fun '(v, _) => length v = n)
          prev ->
        Forall
          (fun '(v, _) => length v = S n)
          (pattern_matches_iter t prev).
      Proof.
        induction prev; intros Henter.
        - (* There won't be any pattern matches, so the condition is
         vacuously true. *)
          now rewrite pattern_matches_iter_empty.
        - apply Forall_app.
          + destruct a as (v & nzip).
            apply Forall_inv in Henter; subst.
            apply proc_tag_matches_length_mono.
          + eapply IHl.
            now inversion Henter.
      Qed.

      (** The opposite can be said for the zippers. *)

      Lemma pattern_matches_iter_zip_length (n : nat) :
        Forall
          (fun '(_, z) => Zipper_length z = n)
          prev ->
        Forall
          (fun '(_, z) => Zipper_length z = pred n)
          (pattern_matches_iter t prev).
      Proof.
        induction prev; intros Henter.
        - (* There won't be any pattern matches, so the condition is
         vacuously true. *)
          now rewrite pattern_matches_iter_empty.
        - apply Forall_app.
          + destruct a as (v & nzip).
            apply Forall_inv in Henter; subst.
            apply proc_tag_matches_length_zip_mono.
          + eapply IHl.
            now inversion Henter.
      Qed.

      Corollary pattern_matches_iter_combined_length (n k : nat) :
        Forall
          (fun '(v, z) => length v = n /\ Zipper_length z = k)
          prev ->
        Forall
          (fun '(v, z) => length v + Zipper_length z = pred k + S n)
          (pattern_matches_iter t prev).
      Proof.
        intro Hprev.
        apply Forall_forall.
        intros (v & z) Hin_iter.
        (* Extract the antecedents for the above lemmas from [Hprev]. *)
        destruct (proj1 (Forall_conj_pairs _ _ _ _ prev) Hprev) as (Forall_v & Forall_z).
        rewrite
          (Forall_forall_in _ _ _
                            (pattern_matches_iter_zip_length k Forall_z)
                            Hin_iter),
        (Forall_forall_in _ _ _
                          (pattern_matches_iter_length n Forall_v)
                          Hin_iter).
        apply Nat.add_comm.
      Qed.

      Lemma pattern_matches_iter_all_right_shifts
          (a : T)
          (rs : Zipper T) :
        Forall
          (fun '(prev_match, pzip) =>
             is_right_shift rs pzip ->
             cur rs = Some a ->
             tag_of a = t ->
             In (a::prev_match, rewind (rem rs)) (pattern_matches_iter t prev)
          )
          prev.
      Proof.
        apply Forall_forall.
        intros (prev_match & pzip) Hin Hrs Hcur Htag.
        apply in_flat_map.
        exists (prev_match, pzip); intuition.
        now apply proc_tag_matches_all_right_shifts.
      Qed.

      (** For every in-flight match going into [pattern_matches_iter],
          each atom waiting to be processed in the zipper ends up
          attached to the existing match in at least one of the
          results of the iteration.  (Completeness) *)

      Lemma pattern_matches_iter_all_atoms :
        Forall
          (fun '(prev_match, pzip) =>
             Forall
               (fun a =>
                  tag_of a = t ->
                  exists z,
                    In (a::prev_match, rewind (rem z)) (pattern_matches_iter t prev) /\
                    cur z = Some a /\
                    Zipper_lists_eq pzip z
               )
               pzip.(zp_right))
          prev.
      Proof.
        apply Forall_forall.
        intros (prev_match & pzip) Hin.
        apply Forall_forall.
        intros g Hin_g Htag.
        pose proof (Forall_forall_in _ _ _ (proc_tag_matches_all_atoms t prev_match pzip) Hin_g Htag) as
            (nzip & Hin_matches & <- & Heq).
        exists nzip; intuition.
        apply in_flat_map.
        exists (prev_match, pzip); intuition.
      Qed.
    End pattern_matches_iter_facts.

    Lemma pattern_matches_iter_leftmost_zips
          (t : Tag) (prev : list (list T * Zipper T)) :
      Forall
        (@leftmost _ ∘ snd)
        (pattern_matches_iter t prev).
    Proof.
      apply Forall_forall.
      intros (m & z) Hin_prev.
      unfold pattern_matches_iter in Hin_prev.
      apply in_flat_map in Hin_prev.
      destruct Hin_prev as ((pm & pz) & Hin_prev & Hin_ptm).
      eapply (Forall_forall_in _ _ _ (proc_tag_matches_leftmost_zips _ _ _)), Hin_ptm.
    Qed.
  End pattern_matches_iter.

  Section pattern_matches_inner.
    Context
      {T  : Type}
      {TR : Tagger Tag}
      {TT : Taggable Tag T}
    .

    Definition pattern_matches_inner
               (ts    : list Tag)
               (input : list T)
      : list (list T * Zipper T) :=
      fold_right
        pattern_matches_iter
        [([], list_to_Zipper input)]
        ts.

    Section pattern_matches_inner_facts.
      Section pattern_matches_inner_facts_easy.
        Variables
          (input : list T)
          (ts    : list Tag).

        (** The empty pattern always produces one, empty, match. *)

        Lemma pattern_matches_inner_empty_tag :
          pattern_matches_inner [] input = [([], list_to_Zipper input)].
        Proof.
          reflexivity.
        Qed.

        (** If we have at least one tag, empty input produces no results for
            [pattern_matches_inner]. *)

        Lemma pattern_matches_inner_empty_input (t : Tag) :
          pattern_matches_inner (t::ts) [] = [].
        Proof.
          revert t.
          induction ts; cbn in *; try easy.
          now rewrite (IHl a).
        Qed.

        Lemma pattern_matches_inner_cons_unfold (t : Tag) :
          pattern_matches_inner (t::ts) input =
          pattern_matches_iter t
                               (pattern_matches_inner ts input).
        Proof.
          reflexivity.
        Qed.

        (** The length of each pattern match is equal to the length of the tag
            list. *)

        Lemma pattern_matches_inner_length_match :
          Forall
            (fun '(v, _) => length v = length ts)
            (pattern_matches_inner ts input).
        Proof.
          induction ts.
          - now constructor.
          - apply Forall_forall.
            intros (g & vs) Hin.
            pose proof (Forall_forall_in _ _ _ (pattern_matches_iter_split a _) Hin) as ((tl & tzip) & Htl & (hd & _ & _ & _ & _ & ->))%Exists_exists.
            cbn.
            now rewrite (Forall_forall_in _ _ _ IHl Htl).
        Qed.

        (** The length of each zipper is equal to the length difference of the
            input and tag list. *)

        Lemma pattern_matches_inner_length_zip :
          Forall
            (fun '(_, z) => Zipper_length z = length input - length ts)
            (pattern_matches_inner ts input).
        Proof.
          unfold pattern_matches_inner.
          induction ts.
          - constructor; intuition.
          - apply Forall_forall.
            intros (v & z) Hin.
            unfold pattern_matches_inner, pattern_matches_iter in Hin.
            cbn in Hin.
            apply in_flat_map in Hin.
            destruct Hin as ((v' & z') & Hin_matches & Hin_link).
            cbn.
            rewrite Nat.sub_succ_r, <- (Forall_forall_in _ _ _ IHl Hin_matches).
            apply (Forall_forall_in _ _ _ (proc_tag_matches_length_zip_mono _ _ _) Hin_link).
        Qed.
      End pattern_matches_inner_facts_easy.

      (** Every item in a returned zipper comes from the input list. *)

      Lemma pattern_matches_inner_zip_input_sublist
            (ts : list Tag) (input : list T) :
        Forall
          (fun '(_, z) => incl (Zipper_to_list z) input)
          (pattern_matches_inner ts input).
      Proof.
        induction ts.
        - (* Empty tags *)
          rewrite pattern_matches_inner_empty_tag.
          now constructor.
        - (* Inductive tags. *)
          apply Forall_forall.
          intros (m, z) Hin.
          rewrite pattern_matches_inner_cons_unfold in Hin.
          pose proof
               (Forall_forall_in
                  _ _ _
                  (pattern_matches_iter_prev a _)
                  Hin
               ) as ((m' & z') & Hin_base & (z'' & -> & Heq & Hhd & <-))%Exists_exists.
          pose proof
               (Forall_forall_in
                  _ _ _
                  IHts
                  Hin_base
               ) as Hincl_base.
          cbn in *.
          autorewrite with zipper in *.
          intros x Hin_rz.
          apply Hincl_base.
          rewrite <- Heq.
          now apply rem_incl.
      Qed.

      Lemma pattern_matches_inner_cons
            (t     : Tag)
            (ts    : list Tag)
            (input : list T) :
        Forall
          (fun '(vs, z) =>
             Exists
               (fun '(vs', z') =>
                  exists vh z'',
                    z = rewind (rem z'') /\ Zipper_lists_eq z'' z' /\
                    Some vh = cur z'' /\
                    vs = vh::vs'
               )
               (pattern_matches_inner ts input)
          )
          (pattern_matches_inner (t::ts) input).
      Proof.
        apply Forall_forall.
        intros (vs & z) Hin.
        (* First, we can work out that the pattern match length is the same as
           the length of [t::ts], and, thus, derive [p] and [ps]. *)
        pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_length_match input (t::ts)) Hin).
        destruct vs as [|vh vs']; try easy.
        apply Exists_exists.
        rewrite pattern_matches_inner_cons_unfold in Hin.
        pose proof (Forall_forall_in _ _ _ (pattern_matches_iter_split _ _) Hin)
          as ((tl & tzip) & Htl & (hd & z' & Hz & Htzip & Hcurz & ->))%Exists_exists.
        exists (tl, tzip); intuition.
        exists hd, z'; intuition.
      Qed.

      (** Simplified form of [pattern_matches_inner_cons] that doesn't
          mention zippers. *)

      Corollary pattern_matches_inner_cons_simpl
            (t     : Tag)
            (ts    : list Tag)
            (input : list T) :
        Forall
          (fun '(vs, _) =>
             Exists
               (fun '(vs', _) =>
                  exists vh, vs = vh::vs'
               )
               (pattern_matches_inner ts input)
          )
          (pattern_matches_inner (t::ts) input).
      Proof.
        apply Forall_forall.
        intros (m & z) Hin.
        apply Exists_exists.
        pose proof (Forall_forall_in _ _ _
                                     (pattern_matches_inner_cons _ _ _)
                                     Hin)
          as ((m' & z') & Hin' & (vh & _ & _ & _ & _ & ->))%Exists_exists.
        exists (m', z'); intuition.
        now (exists vh).
      Qed.

      (** The length of each pattern match is no greater than the length of the
          tags. *)

      Import EqNotations.

      Lemma pattern_matches_inner_length_bound (ts : list Tag) (input : list T) :
        length ts > length input ->
        pattern_matches_inner ts input = [].
      Proof.
        revert input.
        induction ts; try easy.
        intros [|n ns].
        - now rewrite pattern_matches_inner_empty_input.
        - intros [Hlen|Hlen]%Nat.le_succ_r.
          + cbn in *.
            unfold pattern_matches_inner in IHts.
            rewrite IHts.
            * apply pattern_matches_iter_empty.
            * apply Hlen.
          + fold (length ts) in Hlen.
            inversion Hlen.
            (** By contradiction. *)
            destruct (destruct_list (pattern_matches_inner (a :: ts) (n :: ns)))
              as [((hv & hz) & (tl & Hin))|Haa]; try easy.
            exfalso.
            cbn in Hin.
            unfold pattern_matches_iter in Hin.
            pose proof (rew <- Hin in in_eq (hv, hz) tl) as Hin'.
            apply in_flat_map in Hin'.
            destruct Hin' as ((nv & nz) & Hin_nv & Hin_matches).
            pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_length_match _ _) Hin_nv) as Hlen_m.
            cbn in Hlen_m.
            pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_length_zip _ _) Hin_nv) as Hlen_z.
            rewrite <- H0, Nat.sub_diag in Hlen_z.
            now destruct nz as ([|]&[|]).
      Qed.

      (** The input length is equal to the sum of the match and zipper
          length for each result of [pattern_matches_inner]. *)

      Corollary pattern_matches_inner_combined_length
                (ts : list Tag) (input : list T) :
        Forall
          (fun '(v, z) => length v + Zipper_length z = length input)
          (pattern_matches_inner ts input).
      Proof.
        apply Forall_forall.
        intros (v & z) Hin.
        rewrite (Forall_forall_in _ _ _ (pattern_matches_inner_length_match _ _) Hin).
        rewrite (Forall_forall_in _ _ _ (pattern_matches_inner_length_zip _ _) Hin).
        destruct (Nat.le_gt_cases (length ts) (length input)).
        - now rewrite Nat.add_comm, Nat.sub_add.
        - now rewrite (pattern_matches_inner_length_bound ts input H) in Hin.
      Qed.

      (** Each match returning from [pattern_matches_inner] has the correct
          tag at the correct place. *)

      Lemma pattern_matches_inner_tags
            (ts : list Tag) (input : list T) :
        Forall (fun '(pmatch, _) => Forall2 (fun t a => t = tag_of a) ts pmatch)
               (pattern_matches_inner ts input).
      Proof.
        induction ts.
        - rewrite pattern_matches_inner_empty_tag.
          now constructor.
        - apply Forall_forall.
          intros (vs & z) Hin.
          (* Work out that [vs] is non-empty, and that its tail is in the
             matches for [ts]. *)
          pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_cons_simpl _ _ _) Hin)
            as ((vs' & z') & Hin' & (v & ->))%Exists_exists.
          (* This lets us break open the [Forall2]. *)
          apply Forall_forall_in with (x := (vs', z')) in IHts; try easy.
          constructor; try easy.
          (* Now we just need to show that a single step of iteration gives us
             the right tag. *)
          destruct (Forall_forall_in _ _ _ (pattern_matches_iter_head_tag _ _) Hin) as
              (b & Hb & <-).
          now inversion Hb.
      Qed.

      Lemma pattern_matches_inner_leftmost_zips
            (ts : list Tag) (ns : list T) :
        Forall
          (fun '(m, z) => leftmost z)
          (pattern_matches_inner ts ns).
      Proof.
        destruct ts.
        - rewrite ! pattern_matches_inner_empty_tag.
          constructor; try easy.
        - apply Forall_forall.
          intros (m & z) Hin.
          rewrite pattern_matches_inner_cons_unfold in Hin.
          apply (Forall_forall_in _ _ _ (pattern_matches_iter_leftmost_zips _ _) Hin).
      Qed.

      (** If we had a match for [ts] in [ns], the same match exists in
          [n::ns].  (The lemma is slightly stronger, adding in information
          about the zipper, to make the induction go through. *)

      Lemma pattern_matches_inner_match_incl
            (ts : list Tag) (n : T) (ns : list T) :
        Forall
          (fun '(m, z) =>
             Exists
               (fun '(m', z') => m = m' /\ z' = insert_cur z n)
               (pattern_matches_inner ts (n::ns)))
          (pattern_matches_inner ts ns).
      Proof.
        induction ts.
        - rewrite ! pattern_matches_inner_empty_tag.
          constructor; try easy.
          apply Exists_exists; try easy.
          exists ([], list_to_Zipper (n::ns)); intuition.
        - apply Forall_forall.
          intros (m & z) Hin.
          apply Exists_exists.
          rewrite pattern_matches_inner_cons_unfold in *.
          unfold pattern_matches_iter in Hin.
          apply in_flat_map in Hin.
          destruct Hin as
              ((m' & z') & Hin' & Hmatch).
          pose proof (Forall_forall_in _ _ _ IHts Hin') as
              ((m'' & z'') & Hin'' & <- & Hzl)%Exists_exists.
          exists (m, insert_cur z n); intuition.
          apply in_flat_map.
          exists (m', z''); intuition.
          subst.
          pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_leftmost_zips _ _) Hin') as Hlm.
          cbn in Hlm.
          destruct z' as ([|] & zr); try easy.
          unfold insert_cur in *.
          cbn in *.
          unfold proc_tag_matches in Hmatch.
          apply in_map_iff in Hmatch.
          destruct Hmatch as ((mm & mz) & Hptm & Hin_tmatch).
          destruct (Forall_forall_in _ _ _ (tag_matches_zip_cur _ _) Hin_tmatch) as (t & Hcur & Ht).
          destruct mz as [|mzh mzt]; try easy.
          change z with (snd (m, z)).
          pose proof (proc_tag_match_zipper_rem_rewind m' (mk_Zipper _ mm (mzh::mzt))) as Hrew.
          rewrite Hptm in Hrew.
          subst.
          rewrite Hrew in *.
          Opaque rewind.
          cbn.
          assert (zp_left (rewind {| zp_left := mm; zp_right := mzt |}) = []) as ->
              by apply rewind_leftmost.
          rewrite <- to_list_rewind.
          pose proof (Forall_forall_in _ _ _ (tag_matches_lists _ _) Hin_tmatch) as Hsame_list.
          cbn in *.
          subst.
          apply in_map_iff.
          exists (mk_Zipper _ (mm ++ [n]) (mzh::mzt)).
          split.
          + inversion Hptm; subst.
            cbn.
            f_equal.
            destruct (rewind {| zp_left := mm ++ [n]; zp_right := mzt |})
              as (rl & rr) eqn:Hrew.
            f_equal.
            * change rl with (zp_left {| zp_left := rl; zp_right := rr |}).
              rewrite <- Hrew.
              apply rewind_leftmost.
            * change rr with (zp_right {| zp_left := rl; zp_right := rr |}).
              rewrite <- Hrew, <- to_list_rewind.
              cbn.
              now autorewrite with list.
          + apply (Forall_forall_in _ _ _ (tag_matches_move_right _ _)).
            cbn.
            apply (Forall_forall_in _ _ _ (tag_matches_push_left _ _ _) Hin_tmatch).
      Qed.

      Corollary pattern_matches_inner_match_app
                (ts : list Tag) (ns1 ns2 : list T) :
        Forall
          (fun '(m, _) =>
             Exists (fun '(m', _) => m = m')
                    (pattern_matches_inner ts (ns1++ns2)))
          (pattern_matches_inner ts ns2).
      Proof.
        induction ns1.
        - (* Base case: no prefix: trivial *)
          rewrite app_nil_l.
          apply Forall_forall.
          intros (m, z) Hin.
          apply Exists_exists.
          now (exists (m, z)).
        - rewrite <- ! List.app_comm_cons.
          apply Forall_forall.
          intros (m, z) Hin.
          pose proof (Forall_forall_in _ _ _ IHns1 Hin) as ((m' & z') & Hin' & ->)%Exists_exists.
          pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_match_incl _ a _) Hin') as ((m'' & z'') & Hin'' & <- & ->)%Exists_exists.
          apply Exists_exists.
          now (exists (m', insert_cur z' a)).
      Qed.
    End pattern_matches_inner_facts.
  End pattern_matches_inner.

  Section pattern_matches.
    Context
      {T  : Type}
      {TR : Tagger Tag}
      {TT : Taggable Tag T}
    .

    Definition pattern_matches (ts : list Tag) (input : list T) :
      list (list T) :=
      map fst (pattern_matches_inner ts input).

    Definition pattern_matches_exact (ts : list Tag) (input : list T) :
      list (list T) :=
      filter (fun m => length m =? length input)
             (pattern_matches ts input).

    Section pattern_matches_facts.
      Lemma pattern_matches_cons_incl
            (ts : list Tag) (n : T) (ns : list T) :
        incl (pattern_matches ts ns)
             (pattern_matches ts (n::ns)).
      Proof.
        intros mat Hmat_ns.
        unfold pattern_matches in Hmat_ns.
        apply in_map_iff in Hmat_ns.
        destruct Hmat_ns as ((mat' & zip) & Hmat & Hmat_ns).
        subst.
        pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_match_incl ts n ns) Hmat_ns) as ((mat & zip') & Hin_cons & -> & _)%Exists_exists.
        apply in_map_iff.
        firstorder.
      Qed.

      Lemma pattern_matches_exact_incl
            (ts : list Tag)
            (ns : list T) :
        incl
          (pattern_matches_exact ts ns)
          (pattern_matches ts ns).
      Proof.
        intros a Hin_ex.
        unfold pattern_matches_exact in Hin_ex.
        now apply filter_In in Hin_ex.
      Qed.
    End pattern_matches_facts.

    (** The length of each pattern match is equal to the length of the tag
      list. *)

    Lemma pattern_matches_merged_length
          (ts    : list Tag)
          (input : list T) :
      Forall
        (fun pmatch => length pmatch = length ts)
        (pattern_matches ts input).
    Proof.
      unfold pattern_matches, pattern_matches_inner.
      induction ts.
      - now constructor.
      - apply Forall_forall.
        intros vs ((gv & nzip) & Hmerge & Hin)%in_map_iff.
        pose proof (Forall_forall_in _ _ _ (pattern_matches_iter_split a _) Hin)
          as ((tl & tzip) & Hin_tl & (hd & z'' & -> & Hzip_eq & Hhd & ->))%Exists_exists.
        subst.
        cbn.
        f_equal.
        rewrite <- (Forall_forall_in _ tl _ IHts); intuition.
        change tl with (fst (tl, tzip)).
        now apply in_map.
    Qed.

    (** If we have a match for a pattern [t::ts], we can infer that there
      is a match for a pattern [ts] that is a sub-match of the original. *)

    Lemma pattern_matches_cons
          (t     : Tag)
          (ts    : list Tag)
          (input : list T) :
      Forall
        (fun pmatch => In (tl pmatch) (pattern_matches ts input))
        (pattern_matches (t::ts) input).
    Proof.
      apply Forall_forall.
      unfold pattern_matches at 1.
      intros pmatch ((vs & z) & Hmg & Hin)%in_map_iff.
      subst.
      pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_cons_simpl t ts _) Hin)
        as ((lv & lz) & Hin_lv & (v & ->))%Exists_exists.
      (* We can now work out that [guar] is a conjunction of multiple guards. *)
      cbn.
      change lv with (fst (lv, lz)).
      now apply in_map.
    Qed.

    (** The tag of every atom in every match lines up with the tag at the same
      position in the pattern. *)

    Theorem pattern_matches_tags
            (ts   : list Tag)
            (input : list T) :
      Forall
        (Forall2 (fun t a => t = tag_of a) ts)
        (pattern_matches ts input).
    Proof.
      apply Forall_forall.
      unfold pattern_matches.
      intros gi ((v & z) & Hmerged & Hin)%in_map_iff.
      subst.
      assert (length ts = length v) as Hlen
          by now rewrite (Forall_forall_in _ _ _ (pattern_matches_inner_length_match _ _) Hin).
      generalize dependent z.
      apply list_pairwise_ind with (xs := ts) (ys := v); intuition.
      - (* Base case is trivial. *)
        constructor.
      - pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_cons_simpl _ _ _) Hin) as
            ((ys'' & zc) & Hin' & (y' & Hinv))%Exists_exists.
        inversion Hinv; subst.
        constructor; try easy.
        + (* Finally, at this stage, we get to the actually interesting case. *)
          pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_tags  _ _) Hin) as Htags.
          now inversion Htags.
        + now apply (H zc).
    Qed.

    Corollary pattern_matches_exact_tags
            (ts   : list Tag)
            (input : list T) :
      Forall
        (Forall2 (fun t a => t = tag_of a) ts)
        (pattern_matches_exact ts input).
    Proof.
      apply Forall_incl with (ys := pattern_matches ts input).
      - apply pattern_matches_exact_incl.
      - apply pattern_matches_tags.
    Qed.
  End pattern_matches.

  Section mapping.

    (** Results about pattern matches and tag-preserving maps. *)

    Context
      {T1 T2 : Type}
      {TR : Tagger Tag}
      {TT1 : Taggable Tag T1}
      {TT2 : Taggable Tag T2}.

    Variable f : T1 -> T2.

    Hypothesis Hf_tag : forall (x : T1), tag_of x = tag_of (f x).

    Lemma pattern_matches_inner_map_back (ts : list Tag) (input : list T1) :
      Forall
        (fun '(m1, z1) =>
           Exists
             (fun '(m2, z2) =>
                m1 = map f m2 /\
                z1.(zp_left) = map f z2.(zp_left) /\
                z1.(zp_right) = map f z2.(zp_right))
             (pattern_matches_inner ts input)
        )
        (pattern_matches_inner ts (map f input)).
    Proof.
      apply Forall_forall.
      induction ts; intuition.
      - cbn.
        inversion H; inversion H0; intuition.
      - destruct x as (m2 & z2).
        apply Exists_exists.
        pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_cons _ _ _) H) as
            ((m2' & z2') & Hts & (x' & z'' & -> & Hzeq & Hcur & ->))%Exists_exists.
        destruct z'' as (zl & [|zc zr]); try easy.
        inversion Hcur; subst; clear Hcur.
        destruct (proj1 (Exists_exists _ _) (IHts (m2', z2') Hts)) as ((m1 & z1) & Hin & -> & Hz2l & Hz2r).
        rewrite pattern_matches_inner_cons_unfold.
        assert (exists zc', zc = f zc') as (zc' & ->).
        {
          pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_zip_input_sublist _ _) Hts) as Hsublist.
          cbn in Hsublist.
          rewrite <- Hzeq in Hsublist.
          cbn in Hsublist.
          assert (In zc (rev zl ++ zc :: zr)) as Hin_zc by intuition.
          specialize (Hsublist _ Hin_zc).
          apply in_map_iff in Hsublist.
          destruct Hsublist as (zc' & Hf & _).
          now (exists zc').
        }
        (* Show that the various zippers in play are all leftmost;
           this simplifies a lot of the qualities hanging about. *)
        pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_leftmost_zips _ _) Hin)
          as Hz1_leftmost.
        pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_leftmost_zips _ _) Hts)
          as Hz2_leftmost.
        cbn in *.
        destruct z1 as ([|] & z1r), z2' as ([|] & z2r); try easy.
        unfold Zipper_lists_eq, "@@" in Hzeq.
        cbn in *.
        subst.
        (* We're now looking for something in z1r that gives 'f zc''
           when applied to f. *)
        destruct (map_eq_app f _ _ _ (symmetry Hz2r)) as (xs & ys & -> & Hxs & Hys).
        destruct ys as [|y ys]; try easy.
        inversion Hys; subst.
        exists (y :: m1, rewind ({| zp_left := rev xs; zp_right := ys |})); repeat split.
        + (* First, justify this particular zipper. *)
          apply
            (Forall_forall_in _ _ _ (pattern_matches_iter_all_right_shifts a _ y {| zp_left := rev xs; zp_right := y::ys |}) Hin);
            try easy.
          * (* Is this actually a right shift? *)
            split; cbn.
            -- now rewrite rev_involutive.
            -- rewrite app_length.
               cbn.
               omega.
          * (* Is [y]'s tag [a]?  Yes, through a large chain of coincidences. *)
            rewrite Hf_tag, H1.
            destruct (Forall_forall_in _ _ _ (pattern_matches_iter_head_tag a _) H)
              as (x & Hx & tag).
            now inversion Hx; subst.
        (* All that remains is to show that [z1] has the right
           relationship with the original zippers. *)
        + cbn.
          now rewrite map_app, map_rev, map_rev, rev_involutive, Hxs.
    Qed.

    Lemma pattern_matches_map_back (ts : list Tag) (input : list T1) :
      Forall
        (fun m1 =>
           Exists
             (fun m2 => m1 = map f m2)
             (pattern_matches ts input)
        )
        (pattern_matches ts (map f input)).
    Proof.
      apply Forall_forall.
      unfold pattern_matches.
      intros m1' ((m1 & (z1l & z1r)) & <- & Hin_m1)%in_map_iff.
      apply Exists_exists.
      pose proof (Forall_forall_in _ _ _ (pattern_matches_inner_map_back ts input) Hin_m1)
        as ((m2 & z2) & Hin_m2 & -> & Hzl & Hzr)%Exists_exists.
      cbn in *.
      subst.
      exists (fst (m2, z2)); intuition.
      now apply in_map.
    Qed.
  End mapping.
End APred_match.