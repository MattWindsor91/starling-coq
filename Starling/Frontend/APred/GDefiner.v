(** * Abstract predicates: Guarded definer *)

From Coq Require Import
     Classes.SetoidClass
     Lists.List
     Program.Program
     RelationPairs
.

From Starling Require Import
     Backend.Classes
     Backend.AbstractExpr
     Backend.Alpha
     Frontend.APred.Core
     Frontend.APred.Flatten
     Frontend.APred.GMatch
     Frontend.APred.Taggable
     Utils.List.Facts
     Utils.List.MapPrf
     Utils.Monads
     Views.Expr
.

Set Universe Polymorphism.


(** An [APredDef] is a definition.  It consists of a tag, and
    an abstract-expression definition. *)

Record APredDef Symb VBop EBop VMop EMop (Arg : Type@{_}) {Tag} (pfun : Tag -> option TagProto) :=
  mk_APredDef
    {
      apd_tag : ATag pfun;
      apd_def : AbstractExpr Symb VBop EBop VMop EMop Arg;
    }.

(** An [APredDefiner] is a finite definer relation.
        We model it as a list of [APredDef]. *)

Definition APredDefiner Symb VBop EBop VMop EMop (Arg : Type@{_}) {Tag}
  (pfun : Tag -> option TagProto) := list (APredDef Symb VBop EBop VMop EMop Arg pfun).

Arguments mk_APredDef [Symb VBop EBop VMop EMop Arg Tag pfun].
Arguments apd_tag [Symb VBop EBop VMop EMop Arg Tag pfun].
Arguments apd_def [Symb VBop EBop VMop EMop Arg Tag pfun].

Section APred_GDefiner.
  Import ListNotations.
  Local Open Scope signature_scope.
  Local Open Scope program_scope.

  Polymorphic Universe v.
  Variable Var : Type@{v}.

  Polymorphic Universe ep.
  Variable Ep : Type@{v} -> Type@{ep}.

  Polymorphic Universe ev.
  Variable Ev : Type@{v} -> Type@{ev}.

  Polymorphic Universe l.
  Variable Val : Type@{l}.

  Polymorphic Universe gc.
  Variable GCtx : Type@{gc}.
  Polymorphic Universe lc.
  Variable LCtx : Type@{lc}.

  Variables
    Symb
    VBop
    EBop
    VMop
    EMop : Set.

  Context
    {S_Val : Setoid Val}
    {V_Ev  : ValEx Ev}
    {E_Ep  : EqPredEx GCtx LCtx Ep}
    {AEC   : AbstractExprContext Symb VBop EBop VMop EMop (AP_Ep := E_Ep) }
  .

  Section instantiate.
    Polymorphic Universe t.

    Variable Tag : Type@{t}.
    Variables
      (pfun : Tag -> option TagProto)
    .
    Hypothesis Htag_eq : forall (x y : Tag), { x = y } + { x <> y }.

    Definition inst_DefVar
               (x : APred (Ev Var) pfun) :
               DefVar Var ->
               Ev Var :=
      destruct_DefVar mreturn (fun k => nth k (proj1_sig (ap_argv x)) (ve_bot Var)).

    (** If two [APred]s are equal modulo validity proof, so are their
        tags. *)

    Lemma eq_ap_tag_eq (x y : APred (Ev Var) pfun) :
      `x = `y ->
      `x.(ap_tag) = `y.(ap_tag).
    Proof.
      destruct x as (x & Hx), y as (y & Hy); cbn.
      now inversion 1.
    Qed.

    Definition inst_APred
               (x : APred (Ev Var) pfun) :
      AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var)) ->
      AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      fmap (mbind (inst_DefVar x))
           (Functor := AbstractExpr_Functor Symb VBop EBop VMop EMop).

    Lemma inst_APred_prf_irr
          (x y : APred (Ev Var) pfun)
          (vx : AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var)))
          (vy : AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var))) :
      `x = `y ->
      Forall2_AE_Vars
        (fun (evx : Ev (DefVar Var))
           (evy : Ev (DefVar Var)) =>
             mbind (inst_DefVar x) evx =
             mbind (inst_DefVar y) evy)
        vx vy ->
      inst_APred x vx = inst_APred y vy.
    Proof.
      intro Hproj.
      cbn.
      induction 1; cbn in *; eauto; try congruence.
      induction H; eauto.
      f_equal; cbn.
      inversion IHForall2; subst.
      f_equal.
      apply H.
    Qed.

    Lemma inst_APred_prf_irr'
          (x y : APred (Ev Var) pfun)
          (vx vy : AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var))) :
      `x = `y ->
      Forall2_AE_Vars
        (fun (evx evy : Ev (DefVar Var)) =>
           forall (f : Var -> Ev Var)
             (g : nat -> Ev Var) g',
             (forall n, g n = g' n) ->
             mbind (destruct_DefVar f g) evx =
             mbind (destruct_DefVar f g') evy)
        vx vy ->
      inst_APred x vx = inst_APred y vy.
    Proof.
      intros Hp Hforall.
      destruct x as (x & Hvalid_x), y as (y & Hvalid_y).
      cbn in Hp.
      subst.
      unfold inst_APred, inst_DefVar.
      apply inst_APred_prf_irr; intuition.
      induction Hforall; constructor; cbn in *; eauto.
      induction H; constructor; eauto.
    Qed.

    Local Open Scope program_scope.

    Definition inst_GAPred
               (x : GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun)
               (def : AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var))) :
      AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      EImpl (iconj x.(g_guard)
                       (P_Ep := PredEx_AbstractExpr (ACtx := AEC)))
            (inst_APred x.(g_item) def).

    Lemma inst_GAPred_prf_irr'
          (x y : GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun)
          (vx vy : AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var))) :
      gapred_mostly_equal _ _ x y ->
      Forall2_AE_Vars
        (fun (evx evy : Ev (DefVar Var)) =>
           forall (f : Var -> Ev Var)
             (g g' : nat -> Ev Var),
             (forall n, g n = g' n) ->
             mbind (destruct_DefVar f g) evx =
             mbind (destruct_DefVar f g') evy)
        vx vy ->
      inst_GAPred x vx = inst_GAPred y vy.
    Proof.
      intros (Heq_guard & Heq_item) Hforall.
      unfold inst_GAPred.
      f_equal.
      - now rewrite Heq_guard.
      - now apply inst_APred_prf_irr'.
    Qed.

  End instantiate.

  Section multi_definer.
    Polymorphic Universe t.
    Variable Tag : Type@{t}.
    Variables
      (pfun : Tag -> option TagProto)
    .

    Context {T_Tag : Tagger Tag}.

    Definition inst_APred_md
      (xs : list (APred (Ev Var) pfun)) :
      AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var)) ->
      AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      inst_APred _ _ (flatten_APred _ _ _ xs).

    Definition inst_GAPreds
               (xs : Guarded (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) (list (APred (Ev Var) pfun)))
               (def : AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var))) :
      AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      inst_GAPred
        _ _
        (mk_Guarded xs.(g_guard) (flatten_APred _ _ _ xs.(g_item)))
        def.

    Lemma inst_GAPreds_prf_irr
          (xs ys : Guarded (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) (list (APred (Ev Var) pfun)))
          (vx vy : AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var))) :
      xs.(g_guard) = ys.(g_guard) ->
      Forall2 (eq @@ @proj1_sig _ _) xs.(g_item) ys.(g_item) ->
      Forall2_AE_Vars
        (fun (evx evy : Ev (DefVar Var)) =>
           forall (f : Var -> Ev Var)
             (g g' : nat -> Ev Var),
             (forall n, g n = g' n) ->
             evx >>= (destruct_DefVar f g) =
             evy >>= (destruct_DefVar f g'))%monad
        vx vy ->
      inst_GAPreds xs vx = inst_GAPreds ys vy.
    Proof.
      intros Heq_guard Heq_items Hforall.
      apply inst_GAPred_prf_irr'; eauto.
      split; eauto.
      cbn.
      induction Heq_items; cbn; eauto.
      unfold flatten_LooseAPred in *.
      inversion IHHeq_items.
      f_equal; cbn; congruence.
    Qed.

    Corollary inst_GAPreds_prf_irr'
          (xs ys : Guarded (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) (list (APred (Ev Var) pfun)))
          (vx vy : AbstractExpr Symb VBop EBop VMop EMop (Ev (DefVar Var))) :
      merged_mostly_equal _ _ _ _ _ xs ys ->
      Forall2_AE_Vars
        (fun (evx evy : Ev (DefVar Var)) =>
           forall (f : Var -> Ev Var)
             (g g' : nat -> Ev Var),
             (forall n, g n = g' n) ->
             evx >>= (destruct_DefVar f g) =
             evy >>= (destruct_DefVar f g'))%monad
        vx vy ->
      inst_GAPreds xs vx = inst_GAPreds ys vy.
    Proof.
      intros (Heq_guard & Heq_items) Hforall.
      apply inst_GAPreds_prf_irr; eauto.
    Qed.

    Import EqNotations.

    Lemma match_flatten_same_tag
          (v      : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun))
          (d      : APredDef Symb VBop EBop VMop EMop (Ev Var) (flattened_protos _ pfun))
          (gmatch : { x : Guarded (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) (list (APred (Ev Var) pfun))
                    | In x (g_pattern_matches _ _ _ _ pfun (proj1_sig d.(apd_tag)) v) }) :
      proj1_sig d.(apd_tag) =
      proj1_sig (flatten_APred Tag (Ev Var) pfun (g_item (proj1_sig gmatch))).(ap_tag).
    Proof.
      destruct gmatch as (gmatch & Hgmatch).
      pose proof
           (Forall_forall_in _ _ _
                             (g_pattern_matches_tags _ _ _ _ pfun _ _) Hgmatch) as Htags.
      pose proof
           (Forall_forall_in _ _ _
                             (g_pattern_matches_merged_length Tag Var _ _ pfun _ _) Hgmatch) as Hlen.
        cbn in *.
        destruct gmatch as (gguard & gitem).
        generalize dependent gguard.
        generalize dependent gitem.
        unfold apd_tag.
        destruct d as ((dt & Hdt) & _).
        induction dt; intros; subst.
        - apply length_zero_iff_nil in Hlen.
          now subst.
        - cbn in Hlen.
          destruct gitem as [|g gitem]; try easy.
          cbn.
          f_equal.
          + apply (forall2_cons_l Htags).
          + destruct
                 (proj1 (Forall_forall _ _) (g_pattern_matches_cons _ _ _ _ pfun _ _ _) _ Hgmatch)
              as (guar1 & ((guar_2 & ps) & Hin_ys & -> & Hgitem)%Exists_exists).
            assert (flattened_protos Tag pfun dt <> None) as Hdt'
                   by eapply flatten_cons_legal, Hdt.
            refine (IHdt Hdt' _ (forall2_cons_r Htags) _ _ _).
            * now inversion Hlen.
            * inversion Hgitem; subst.
              apply Hin_ys.
              (* Phew!! *)
    Qed.

    Definition inst_gmatch
               (v : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun))
               (d : APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var)) (flattened_protos _ pfun))
               (gmatch : Guarded (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) (list (APred (Ev Var) pfun))) :
      AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      inst_GAPreds gmatch d.(apd_def).

    Definition gd_reify_single
               (v : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun))
               (d : APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var)) (flattened_protos _ pfun)) :
      list (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) :=
      map
           (inst_gmatch v d)
           (g_pattern_matches _ _ _ _ pfun (proj1_sig d.(apd_tag)) v).

    Instance setoid_gme :
      Setoid
        (Guarded (AbstractExpr Symb VBop EBop VMop EMop (Ev Var))
                 (list ({x : LooseAPred Tag (Ev Var) | valid x pfun}))) :=
      {
        equiv := merged_mostly_equal _ _ _ _ pfun
      }.
    Proof.
      repeat split; unfold merged_mostly_equal, "@@" in *; cbn in *.
      - induction (g_item x); intuition.
      - intuition.
      - destruct H as (_ & H).
        induction H; intuition.
      - destruct H, H0; congruence.
      - destruct H as (_ & H), H0 as (_ & H0).
        generalize dependent (g_item z).
        induction H; inversion 1.
        + subst; intuition.
        + subst.
          inversion H1; subst.
          constructor; try congruence.
          intuition.
    Defined.

    Global Instance gd_reify_single_Proper
           (d : APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var)) (flattened_protos _ pfun)) :
      Proper
        ((ForallAtoms2 (gapred_mostly_equal _ _ (protos := pfun)) @@ forget_lnorm (A := _))
           ==> eq)
        (flip gd_reify_single d).
    Proof.
      intros (v1 & Hv1) (v2 & Hv2).
      change (fun x => is_lnorm x)
        with
          (is_lnorm
             (A := (Guarded (AbstractExpr Symb VBop EBop VMop EMop (Ev Var))
                            {x : LooseAPred Tag (Ev Var) | valid x pfun}))).
      intro Hfat.
      unfold "@@", forget_lnorm, proj1_sig in Hfat.
      unfold gd_reify_single, flip.
      rewrite ! map_prf_map.
      apply eqlistA_eq, map_prf_eql with (SA := setoid_gme).
      - intros x y Hmeq.
        apply inst_GAPreds_prf_irr'; eauto.
        apply Forall2_AE_Vars_Reflexive.
        intros z f g g' Hpn.
        apply ve_Monad_fun_ext.
        intros [a|a]; try easy.
        now apply Hpn.
      - apply SetoidList.eqlistA_altdef, g_pattern_matches_mostly_equal_Proper, Hfat.
    Qed.

    (** Both reification and definition lift from one definition to an entire
        definer in the same way. *)

    Definition gd_conj_over_definer
               {P_Ep : PredEx (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) GCtx LCtx (Var -> Val) }
               (op : APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun) ->
                     list (AbstractExpr Symb VBop EBop VMop EMop (Ev Var))) :
      list (APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun)) ->
      AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      iconj (P_Ep := P_Ep) ∘ flat_map op.

    Definition gd_conj_view_over_definer
               {P_Ep : PredEx (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) GCtx LCtx (Var -> Val) }
               (op : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun) ->
                     APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun) ->
                     list (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)))
               (v : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun)) :
      list (APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun)) ->
      AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      gd_conj_over_definer (op v) (P_Ep := P_Ep).

    Global Instance gd_conj_view_over_definer_Proper
           (op : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun) ->
                 APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun) ->
                 list (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)))
           {Sing_Proper :
              forall (d : APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun)),
              Proper
                ((ForallAtoms2 (gapred_mostly_equal _ _ (protos := pfun)) @@ forget_lnorm (A := _))
                   ==> eq)
                (flip op d) }
           (d : list (APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun))) :
      Proper
        ((ForallAtoms2 (gapred_mostly_equal _ _ (protos := pfun)) @@ forget_lnorm (A := _))
           ==> eq)
        (flip (gd_conj_view_over_definer op) d).
    Proof.
      red.
      intros x y Hfa_xy.
      unfold flip, compose, gd_conj_view_over_definer, gd_conj_over_definer, compose.
      f_equal.
      induction d; try easy.
      cbn.
      rewrite IHd.
      f_equal.
      now apply Sing_Proper.
    Defined.

    (** [gd_reify] is the syntactic reification over guarded abstract
        predicates, over a whole definer. *)

    Definition gd_reify
      : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun) ->
        list (APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun)) ->
        AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      gd_conj_view_over_definer gd_reify_single.

    Global Instance gd_reify_Proper
           (d : list (APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var))(flattened_protos _ pfun))) :
      Proper
        ((ForallAtoms2 (gapred_mostly_equal _ _ (protos := pfun)) @@ forget_lnorm (A := _))
           ==> eq)
        (flip gd_reify d) :=
      gd_conj_view_over_definer_Proper _ d.

    (** [gd_def_single] is the syntactic definition over guarded abstract
        predicates, for a single definition. *)

    Definition gd_def_single
               (v : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun))
               (d : APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var)) (flattened_protos _ pfun)) :
      list (AbstractExpr Symb VBop EBop VMop EMop (Ev Var)) :=
      map (inst_gmatch v d)
          (g_pattern_matches_exact _ _ _ _ pfun (proj1_sig d.(apd_tag)) v).


    Definition gd_def
      : LNormVExpr (GAPred (AbstractExpr Symb VBop EBop VMop EMop) (Ev Var) pfun) ->
        list (APredDef Symb VBop EBop VMop EMop (Ev (DefVar Var)) (flattened_protos _ pfun)) ->
        AbstractExpr Symb VBop EBop VMop EMop (Ev Var) :=
      gd_conj_view_over_definer gd_def_single.
  End multi_definer.
End APred_GDefiner.

