(** * Abstract predicates: core definitions *)

From Coq Require Import
     Arith.PeanoNat
     Bool.Bool
     Lists.List
     Lists.SetoidList
     Program.Basics
     Program.Utils
     Relations.Relation_Operators
     Sets.Constructive_sets
     Classes.RelationPairs
     Classes.SetoidClass
     Classes.SetoidDec
     Omega
.

From Starling Require Import
     Backend.Alpha.Chained
     Backend.Alpha.Classes
     Backend.Classes
     Utils.List.Facts
     Utils.Option.Facts
     Utils.Monads
     Utils.Multiset.LMultiset
     Views.Classes
     Views.Expr
     Views.Instances.LMultiset
     Views.Transformers.Function
     Views.Transformers.Subtype.

Set Universe Polymorphism.

Record LooseAPred Tag Arg :=
  mk_LooseAPred
    {
      lap_tag  : Tag;
      lap_argv : list Arg
    }.

Arguments lap_tag  [Tag Arg].
Arguments lap_argv [Tag Arg].

Record TagProto :=
  mk_TagProto
    {
      sp_argc : nat;
    }.

Definition valid {Tag} {Arg}
           (x    : LooseAPred Tag Arg)
           (pfun : Tag -> option TagProto)
  : Prop :=
  exists (tp : TagProto),
    pfun x.(lap_tag) = Some tp
    /\ length x.(lap_argv) = tp.(sp_argc).

Definition APred {Tag} Arg (pfun : Tag -> option TagProto) :=
  { x : LooseAPred Tag Arg | valid x pfun }.


Definition GAPred {Tag} (Ep : Type@{_} -> Type@{_}) T (pfun : Tag -> option TagProto) :=
  Guarded (Ep T) (APred T pfun).

Notation ATag :=
  (fun pfun => { x | pfun x <> None }).

Section APred_defs.

  Import ListNotations.
  Local Open Scope program_scope.

  Polymorphic Universe t.
  Variable Tag : Type@{t}.

  Polymorphic Universe a.
  Variable Arg : Type@{a}.

  Hypothesis Htag_eq : forall (x y : Tag), { x = y } + { x <> y }.

  Definition has_TagProto (pfun : Tag -> option TagProto) (s : Tag) : Prop :=
    pfun s <> None.

  Definition get_ATagProto (pfun : Tag -> option TagProto) (s : ATag pfun) : TagProto.
  Proof.
    refine
      (match option_sumor (pfun (proj1_sig s)) with
       | inleft x => proj1_sig x
       | inright H => _
       end).
    now destruct s as (s & Hs).
  Defined.

  Definition get_argc (pfun : Tag -> option TagProto) : ATag pfun -> nat :=
    sp_argc ∘ (get_ATagProto pfun).

  Definition ap_tag {pfun : Tag -> option TagProto} (x : APred Arg pfun) : ATag pfun.
  Proof.
    refine (exist _ x.(proj1_sig).(lap_tag) _).
    destruct x as (x & tp & Htp & Hlen).
    unfold proj1_sig, has_TagProto.
    now rewrite Htp.
  Defined.

  Lemma valid_argv_matches_argc
        {pfun : Tag -> option TagProto} (ap : APred Arg pfun) :
    length (lap_argv (proj1_sig ap)) = get_argc pfun (ap_tag ap).
  Proof.
    destruct ap as (ap & tp & Htp & Hlen).
    unfold proj1_sig, get_argc, ap_tag, proj1_sig, compose.
    rewrite Hlen.
    f_equal.
    unfold get_ATagProto, proj1_sig.
    now rewrite Htp.
  Qed.

  Definition ap_argv {pfun : Tag -> option TagProto} (ap : APred Arg pfun) :
    { x : list Arg | length x = get_argc pfun ap.(ap_tag) } :=
    (exist _ ap.(proj1_sig).(lap_argv) (valid_argv_matches_argc ap)).

  Definition mk_APred {pfun : Tag -> option TagProto}
             (tag : ATag pfun)
             (argv : { x : list Arg | length x = get_argc pfun tag })
    : APred Arg pfun.
  Proof.
    refine (exist _ (mk_LooseAPred _ _ (proj1_sig tag) (proj1_sig argv)) _).
    exists (get_ATagProto pfun tag).
    split.
    - unfold lap_tag, get_ATagProto.
      destruct tag as (tag & Htag).
      unfold proj1_sig.
      destruct (option_sumor (pfun tag)); try easy.
      now destruct s as (s & Hs).
    - now destruct argv.
  Defined.

  Local Open Scope monad_scope.

  Fixpoint get_TagProto_list (dfs : list (Tag * TagProto)) (s : Tag) : option TagProto :=
    match dfs with
    | [] => None
    | (t, d)::dfs' =>
      if Htag_eq t s
      then Some d
      else get_TagProto_list dfs' s
    end.

  Lemma lap_tag_ap_tag {pfun : Tag -> option TagProto} (x : APred Arg pfun) :
    x.(ap_tag).(proj1_sig) = x.(proj1_sig).(lap_tag).
  Proof.
    trivial.
  Qed.

End APred_defs.

Arguments lap_tag  [Tag Arg].
Arguments lap_argv [Tag Arg].
Arguments ap_tag  [Tag Arg pfun].
Arguments ap_argv [Tag Arg pfun].

Arguments get_ATagProto [Tag pfun].
Arguments get_argc [Tag pfun].

Set Implicit Arguments.

Section APred_setoid.
  Polymorphic Universe t.
  Variable Tag : Type@{t}.

  Polymorphic Universe a.
  Variable Arg : Type@{a}.

  Context {S_Ev : Setoid Arg}.

  (* TODO: this will need to be changed to setoids eventually. *)

  Definition APred_equiv (x y : LooseAPred Tag Arg) : Prop :=
    x.(lap_tag) = y.(lap_tag) /\
    eqlistA (SetoidClass.equiv (Setoid := S_Ev)) x.(lap_argv) y.(lap_argv).

  Global Instance APred_equiv_Reflexive : Reflexive APred_equiv := {}.
  Proof.
    now intros (xt & xv).
  Defined.

  Global Instance APred_equiv_Symmetric : Symmetric APred_equiv := {}.
  Proof.
    now intros (xt & xv) (yt & yv) (Htags & Hlists).
  Defined.

  Global Instance APred_equiv_Transitive : Transitive APred_equiv := {}.
  Proof.
    intros
      (xt & xv)
      (yt & yv)
      (zt & zv)
      (Htags_xy & Hlists_xy)
      (Htags_yz & Hlists_yz);
      cbn in *;
      subst;
      split;
      intuition.
    now setoid_rewrite Hlists_xy.
  Defined.

  Global Instance APred_equiv_Equivalence : Equivalence APred_equiv := {}.
  Global Instance APred_Setoid : Setoid (LooseAPred Tag Arg) :=
    { equiv := APred_equiv }.

  Context
    {D_Ev : EqDec S_Ev}.

  Hypothesis Htag_eq : forall (x y : Tag), { x = y } + { x <> y }.

  Definition eqlistB (x y : list Arg) : bool :=
    (Nat.eqb (length x) (length y))
      && forallb (fun '(x, y) => if x == y then true else false) (combine x y).

  (** [eqlistB] is a witness for [eqlistA]. *)

  Lemma eqlistB_eqlist (x y : list Arg) :
    Is_true (eqlistB x y) <-> eqlistA equiv x y.
  Proof.
    split.
    - intros (Hlen%Is_true_eq_true%Nat.eqb_eq & Hrest)%andb_prop_elim.
      revert Hrest.
      apply (list_pairwise_ind x y); intuition.
      cbn in Hrest.
      destruct (SetoidDec.equiv_dec x0 y0); intuition.
    - intro Hequiv.
      (* Do lots of rearranging to make the induction go through. *)
      pose proof (eqlistA_length Hequiv) as Hel.
      generalize Hequiv; clear Hequiv.
      apply (list_pairwise_ind x y); intuition.
      apply andb_prop_intro.
      split.
      * eapply Is_true_eq_left, Nat.eqb_eq, eqlistA_length, Hequiv.
      * inversion_clear Hequiv.
        apply andb_prop_intro.
        destruct (SetoidDec.equiv_dec x0 y0); intuition.
        now apply andb_prop_elim in H2.
  Qed.

  (** This then gives us a decision procedure for list equivalence! *)

  Lemma eqlistA_dec (x y : list Arg) :
    { eqlistA equiv x y } + { ~ eqlistA equiv x y }.
  Proof.
    destruct (eqlistB x y) eqn:Heqb.
    - (* true *)
      left.
      now apply eqlistB_eqlist, Is_true_eq_left.
    - (* false *)
      right.
      intros Heqa%eqlistB_eqlist%Is_true_eq_true; congruence.
  Qed.

  (** From this, we can build a decision procedure for [LooseAPred]s. *)

  Global Instance LooseAPred_EqDec : EqDec APred_Setoid := {}.
  Proof.
    intros (xtag & xargv) (ytag & yargv).
    destruct (Htag_eq xtag ytag) as [Htag|Htag], (eqlistA_dec xargv yargv) as [Hargv|Hargv];
      ((now left) || right; now intros (Hetag & Helist)).
  Defined.

End APred_setoid.

Section APred_mapping.
  Polymorphic Universe t.
  Variable Tag : Type@{t}.

  Polymorphic Universe a.
  Variable A : Type@{a}.

  Polymorphic Universe b.
  Variable B : Type@{b}.

  Context
    {TF : Tag -> option TagProto}.

  Variable
    f : A -> B.

  Definition lapred_map (a : LooseAPred Tag A) : LooseAPred Tag B :=
    mk_LooseAPred _ _ a.(lap_tag) (map f a.(lap_argv)).

  Lemma lapred_map_valid (a : LooseAPred Tag A) :
    valid a TF -> valid (lapred_map a) TF.
  Proof.
    intros (tp & Hatag & Hapred).
    exists tp.
    split; intuition.
    rewrite <- Hapred.
    apply map_length.
  Qed.

  Definition apred_map (a : APred A TF) : APred B TF :=
    exist _ (lapred_map a.(proj1_sig)) (lapred_map_valid a.(proj2_sig)).
End APred_mapping.

Section APred_facts.
  Polymorphic Universe t.
  Variable Tag : Type@{t}.

  Polymorphic Universe v.
  Variable Var : Type@{v}.

  Polymorphic Universe l.
  Variable Val : Type@{l}.

  Polymorphic Universe ep.
  Variable Ep : Type@{v} -> Type@{ep}.

  Polymorphic Universe ev.
  Variable Ev : Type@{v} -> Type@{ev}.

  Variable
    protos : Tag -> option TagProto.

  Context
    {S_Val : Setoid Val}
    {D_Val : EqDec S_Val}
    {V_Ev  : ValEx Ev (Val := Val)}
    {P_EEp : EqPredEx unit unit Ep (Val := Val)}.

  Hypothesis Htag_eq : forall (x y : Tag), { x = y } + { x <> y }.

  Lemma same_tag_same_lookup (x y : ATag protos) :
    proj1_sig x = proj1_sig y ->
    get_ATagProto x = get_ATagProto y.
  Proof.
    intro Heq.
    unfold get_argc, get_ATagProto, compose.
    pose (proj2_sig x) as Hhas_x.
    pose (proj2_sig y) as Hhas_y.
    destruct
      (option_sumor (protos (proj1_sig x)))
      as [(a & Ha)|Ha],
         (option_sumor (protos (proj1_sig y)))
        as [(b & Hb)|Hb];
      cbn; intuition.
    rewrite Heq, Hb in Ha.
    now injection Ha.
  Qed.

  Corollary same_tag_same_argc (x y : ATag protos) :
    proj1_sig x = proj1_sig y ->
    get_argc x = get_argc y.
  Proof.
    intros Hlookup%same_tag_same_lookup.
    unfold get_argc, compose.
    now rewrite Hlookup.
  Qed.

  Lemma get_argc_to_tag {X} (x : APred X protos) (t : ATag protos) :
    (`x).(lap_tag) = `t ->
    get_argc (x.(ap_tag)) = get_argc t.
  Proof.
    destruct x as ((xt & xa) & Hx).
    cbn.
    intros ->.
    now apply same_tag_same_argc.
  Qed.

  Global Instance AtomLanguage_APred_Monadic
         {V} { FV : Functor V } { MV : Monad V }
    : AtomLanguage (APred (Ev Var) protos) (V (APred _ protos)) := AtomLanguage_Monad.

  (** This gives us, for example, a trivial [AtomLanguage] over list
        multisets. *)

  Global Instance AtomLanguage_APred_LMultiset_Direct
    : AtomLanguage (APred (Ev Var) protos) (LMultiset (APred _ protos)) :=
    AtomLanguage_APred_Monadic.

  (** Using this in practice means we'll need to be able to decide equality
      over expressions.  This isn't always possible, so we instead boil
      expressions down to values.  This can fail. *)

  Program Definition nth_prf' {A}
             (xs : list A)
             (arg : nat)
             (Harg : arg < length xs) : A :=
    match nth_error xs arg with
    | Some a => a
    | None => !
    end.
  Next Obligation.
    contradict Harg.
    apply Nat.le_ngt, (proj1 (nth_error_None _ _) (eq_sym Heq_anonymous)).
  Defined.

  Definition nth_prf {A}
             (xs : list A)
             (arg : { x : nat | x < length xs }) : A :=
    nth_prf' xs (proj2_sig arg).

  Lemma eqlistA_nth
        {T : Type}
        {ST : Setoid T}
        (n : nat)
        (def : T) :
    Proper (eqlistA SetoidClass.equiv ==> SetoidClass.equiv) (fun xs => nth n xs def).
  Proof.
    intros x y Hxy.
    revert n.
    induction Hxy; intuition.
    destruct n; cbn; intuition.
  Qed.

  Lemma eqlistA_nth_error
        {T}
        {ST : Setoid T}
        (n : nat) :
    Proper (eqlistA SetoidClass.equiv ==> SetoidClass.equiv) (flip (@nth_error T) n).
  Proof.
    intros x y Hxy.
    revert n.
    induction Hxy; intuition.
    unfold flip in *.
    cbn in *.
    destruct n; cbn; intuition.
  Qed.

  Lemma to_nat_cast_proj (x y : nat) (H : x = y) (k : Fin.t x) :
    ` (Fin.to_nat k) = ` (Fin.to_nat (Fin.cast k H)).
  Proof.
    revert y H.
    induction k; intros; cbn; destruct H; intuition.
    destruct (Fin.to_nat k) as (i & P).
    cbn in *.
    rewrite (IHk n eq_refl).
    now destruct (Fin.to_nat (Fin.cast k eq_refl)) as (i' & P').
  Qed.

  Lemma nth_prf_equiv {A} {SA : Setoid A}
        (xs ys : list A)
        (xa : { x : nat | x < length xs })
        (ya : { x : nat | x < length ys }) :
    `xa = `ya ->
    eqlistA SetoidClass.equiv xs ys ->
    SetoidClass.equiv (nth_prf xs xa) (nth_prf ys ya).
  Proof.
    destruct xa as (a & Hxa), ya as (a' & Hya).
    cbn.
    intros <- Hequiv.
    unfold nth_prf, nth_prf'.
    apply eqlistA_nth_error with (n := a) in Hequiv.
    unfold flip in Hequiv.
    cbn in *.
    generalize (nth_prf'_obligation_1 xs Hxa) as Hx.
    generalize (nth_prf'_obligation_1 ys Hya) as Hy.
    destruct (nth_error xs a) eqn:Hex, (nth_error ys a) eqn:Hey; intuition.
  Qed.

  (** We only use the proof to rule out lookup failure, so
      [nth_prf] has proof irrelevance. *)

  Corollary nth_prf_irr {A}
            (xs : list A)
            (arg1 arg2 : { x : nat | x < length xs }) :
    `arg1 = `arg2 ->
    nth_prf xs arg1 = nth_prf xs arg2.
  Proof.
    intro Hargs.
    now apply (nth_prf_equiv (SA := @eq_setoid A)).
  Qed.

  Definition nth_fin {A}
             (xs : list A) (arg : Fin.t (length xs)) : A :=
    nth_prf xs (Fin.to_nat arg).

  Lemma fin_cast_nat
        (n k : nat)
        (t   : Fin.t n)
        (Hnk : n = k) :
    proj1_sig (Fin.to_nat t) = proj1_sig (Fin.to_nat (Fin.cast t Hnk)).
  Proof.
    generalize dependent k.
    induction t; intros k []; cbn; intuition.
    destruct (Fin.to_nat t) as (nt, Hnt); cbn in *.
    specialize (IHt n eq_refl).
    subst.
    destruct (Fin.to_nat (Fin.cast t eq_refl)); intuition.
  Qed.

  Lemma nth_fin_equiv {A} {SA : Setoid A}
        (xs ys : list A)
        (xa : Fin.t (length xs))
        (ya : Fin.t (length ys)) :
    ` (xa.(Fin.to_nat)) = ` (ya.(Fin.to_nat)) ->
    eqlistA SetoidClass.equiv xs ys ->
    SetoidClass.equiv (nth_fin xs xa) (nth_fin ys ya).
  Proof.
    apply nth_prf_equiv.
  Qed.

  Corollary nth_fin_prf_irr {A}
        (xs    : list A)
        (t1 t2 : Fin.t (length xs)) :
    ` (t1.(Fin.to_nat)) = ` (t2.(Fin.to_nat)) ->
    nth_fin xs t1 =
    nth_fin xs t2.
  Proof.
    apply nth_prf_irr.
  Qed.

  Lemma nth_fin_cast_add' {A}
        (xs  : list A)
        (k   : nat)
        (tn  : Fin.t (length xs))
        (tk  : Fin.t k)
        (Hck : k = length xs) :
    proj1_sig tn.(Fin.to_nat) = proj1_sig tk.(Fin.to_nat) ->
    nth_fin xs tn =
    nth_fin xs (Fin.cast tk Hck).
  Proof.
    intro Hnat.
    rewrite (fin_cast_nat _ Hck) in Hnat.
    now apply nth_fin_prf_irr.
  Qed.

  Corollary nth_fin_cast_add {A}
        (xs : list A)
        (tn : Fin.t (length xs)) :
    nth_fin xs tn =
    nth_fin xs (Fin.cast tn eq_refl).
  Proof.
    now apply nth_fin_cast_add'.
  Qed.


  Definition lookup_APred_arg
             (x : APred (Ev Var) protos)
             (v : Fin.t (get_argc x.(ap_tag)))
    : Ev Var :=
    nth_fin (` (x.(ap_argv)))
            (Fin.cast v (eq_sym (proj2_sig x.(ap_argv)))).

  Lemma lookup_APred_arg_prf_irr
        (x y : APred (Ev Var) protos)
        (tn  : Fin.t (get_argc x.(ap_tag)))
        (tk  : Fin.t (get_argc y.(ap_tag))) :
    proj1_sig x = proj1_sig y ->
    proj1_sig tn.(Fin.to_nat) = proj1_sig tk.(Fin.to_nat) ->
    lookup_APred_arg x tn =
    lookup_APred_arg y tk.
  Proof.
    intros Hxy Hnat.
    unfold lookup_APred_arg.
    destruct x as (x & Hx), y as (y & Hy).
    cbn in *.
    subst.
    apply nth_fin_prf_irr.
    now rewrite <- ! fin_cast_nat.
  Qed.

  Corollary lookup_APred_arg_cast_irr
        (x y : APred (Ev Var) protos)
        (n k : nat)
        (tn  : Fin.t n)
        (Hcn : n = get_argc x.(ap_tag))
        (tk  : Fin.t k)
        (Hck : k = get_argc y.(ap_tag)) :
    proj1_sig x = proj1_sig y ->
    proj1_sig tn.(Fin.to_nat) = proj1_sig tk.(Fin.to_nat) ->
    lookup_APred_arg x (Fin.cast tn Hcn) =
    lookup_APred_arg y (Fin.cast tk Hck).
  Proof.
    rewrite (fin_cast_nat _ Hcn), (fin_cast_nat _ Hck).
    now apply lookup_APred_arg_prf_irr.
  Qed.

  Lemma lookup_APred_arg_equiv
        (x y : APred (Ev Var) protos)
        (n k : nat)
        (tn  : Fin.t n)
        (Hcn : n = get_argc x.(ap_tag))
        (tk  : Fin.t k)
        (Hck : k = get_argc y.(ap_tag)) :
    x == y ->
    proj1_sig tn.(Fin.to_nat) = proj1_sig tk.(Fin.to_nat) ->
    lookup_APred_arg x (Fin.cast tn Hcn) ==
    lookup_APred_arg y (Fin.cast tk Hck).
  Proof.
    intros (Htag & Hargv) s.
    apply nth_fin_equiv; intuition.
    now rewrite <- ! to_nat_cast_proj.
  Qed.

  Import ListNotations.

  (** [APred_arg_list_guard] generates an equality guard for two [APred]
      argument vectors. *)

  Fixpoint APred_arg_list_guard (xs : list (Ev Var)) (ys : list (Ev Var)) : list (Ep (Ev Var)) :=
    match xs, ys with
    | List.nil, List.nil => []
    | (x::xs'), (y::ys') =>
      (ape_eq x y)::(APred_arg_list_guard xs' ys')
    | _, _ => [pe_false (ImplPredEx := ape_ImplPredEx _ (EqPredEx := P_EEp))]
    end.

  (** [APred_eq_guard] generates an equality guard for two [APred]s. *)

  Definition APred_eq_guard (x y : APred (Ev Var) protos) : list (Ep (Ev Var)) :=
    if Htag_eq x.(ap_tag).(proj1_sig) y.(ap_tag).(proj1_sig)
    then APred_arg_list_guard (proj1_sig x.(ap_argv)) (proj1_sig y.(ap_argv))
    else [pe_false (ImplPredEx := ape_ImplPredEx _)].

  Local Open Scope monad_scope.

  Definition lower_argv (f : Var -> EVal Ev) : list (Ev Var) -> list (EVal Ev) :=
    map (mbind f).

  Lemma lower_argv_preserve_length (f : Var -> EVal Ev) (xs : list (Ev Var)) :
    length (lower_argv f xs) = length xs.
  Proof.
    apply map_length.
  Qed.

  Import EqNotations.

  Definition lower_LooseAPred (x : LooseAPred Tag (Ev Var)) (s : Var -> EVal Ev) :
    LooseAPred Tag (EVal Ev) :=
    mk_LooseAPred _ _ x.(lap_tag) (lower_argv s x.(lap_argv)).

  Lemma lower_LooseAPred_same_tag
        (x : LooseAPred Tag (Ev Var)) (t : Tag) (s : Var -> EVal Ev) :
    (lower_LooseAPred x s).(lap_tag) = x.(lap_tag).
  Proof.
    trivial.
  Qed.

  Lemma lower_LooseAPred_argv
        (x : LooseAPred Tag (Ev Var)) (s : Var -> EVal Ev) :
    (lower_LooseAPred x s).(lap_argv) =
    (lower_argv s x.(lap_argv)).
  Proof.
    trivial.
  Qed.

  Corollary lower_LooseAPred_same_argc
        (x : LooseAPred Tag (Ev Var)) (t : Tag) (s : Var -> EVal Ev) :
    x.(lap_argv).(length) = (lower_LooseAPred x s).(lap_argv).(length).
  Proof.
    now rewrite lower_LooseAPred_argv, lower_argv_preserve_length.
  Qed.

  Definition lower_APred (x : APred (Ev Var) protos) (s : Var -> EVal Ev) :
    APred (EVal Ev) protos.
  Proof.
    destruct x as (x & Hrest).
    exists (lower_LooseAPred x s).
    destruct Hrest as (tp & Htp & Hargc).
    unfold lower_LooseAPred in *.
    exists tp.
    split; intuition.
    rewrite <- Hargc.
    apply lower_argv_preserve_length.
  Defined.

  Global Instance AtomLanguage_APred_LMultiset_Lowered
    : AtomLanguage (APred (Ev Var) protos)
                   ((Var -> EVal Ev) -> LMultiset (APred (EVal Ev) protos)) :=
    {
      al_inject x := mreturn ∘ (lower_APred x)
    }.

  Lemma lower_LooseAPred_equiv (x y : LooseAPred Tag (Ev Var)) (s : Var -> EVal Ev) :
    x.(lap_tag) = y.(lap_tag) ->
    eqlistA (SetoidClass.equiv)
            (lower_argv s x.(lap_argv))
            (lower_argv s y.(lap_argv)) ->
    equiv (lower_LooseAPred x s)
          (lower_LooseAPred y s).
  Proof.
    intros Hsame_tag Hsame_interp.
    unfold lower_LooseAPred.
    now destruct (lower_argv s (lap_argv x)), (lower_argv s (lap_argv y)).
  Qed.

  Lemma lower_APred_equiv (x y : APred (Ev Var) protos) (s : Var -> EVal Ev) :
    x.(ap_tag).(proj1_sig) = y.(ap_tag).(proj1_sig) ->
    eqlistA SetoidClass.equiv
            (lower_argv s x.(ap_argv).(proj1_sig))
            (lower_argv s y.(ap_argv).(proj1_sig)) ->
    equiv (lower_APred x s) (lower_APred y s).
  Proof.
    intros Hsame_tag Hsame_interp.
    destruct x as (xtag & (xargs & Hxargs)), y as (ytag & (yargs & Hyargs)).
    now apply lower_LooseAPred_equiv.
  Qed.

  Hint Unfold
       APred_eq_guard
       APred_arg_list_guard
       lower_APred
       maybe
       compose.

  (** If the guard holds, the two predicates are equivalent. *)

  Lemma APred_eq_guard_equiv (x y : APred (Ev Var) protos) (s : Var -> EVal Ev) :
    pe_interp tt tt (iconj (APred_eq_guard x y)
                           (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))
              (eval_lift s) ->
    equiv (lower_APred x s) (lower_APred y s).
  Proof.
    intro Hinterp.
    unfold APred_eq_guard in Hinterp.
    destruct (Htag_eq (proj1_sig (ap_tag x)) (proj1_sig (ap_tag y))).
    - (* Tags equal *)
      apply lower_APred_equiv; try easy.
      destruct x as ((xt & xargv) & xtp & Hxt & Hxargv), y as ((yt & yargv) & ytp & Hyt & Hyargv); cbn in *.
      unfold APred_arg_list_guard in Hinterp.
      revert Hinterp.
      apply list_pairwise_ind with (xs := xargv) (ys := yargv).
      + congruence.
      + now cbn.
      + intros x xs y ys Hind
               (Hxyeq%ape_eq_val_eq & Hinterp)%iconj_cons_interp%pe_conj_distr%Intersection_inv.
        constructor; intuition.
        intros f.
        unfold eval_lift in Hxyeq.
        rewrite <- ! ve_state_mbind in Hxyeq.
        (* [f] must be equivalent to [elim_empty], which is the only thing we
           have left to show. *)
        transitivity (ve_interp (elim_empty Val) (s =<< x)).
        { now apply ve_interp_equiv. }
        transitivity (ve_interp (elim_empty Val) (s =<< y)).
        { now apply Hxyeq. }
        now apply ve_interp_equiv.
    - (* Tags not equal. *)
      apply iconj_cons_interp, pe_conj_distr, Intersection_inv, proj1 in Hinterp.
      contradict Hinterp.
      apply pe_false_False with (P_Ep := ape_PredEx _).
  Qed.

  (** If the two predicates have an equivalent lowering, the equality guard
      holds. *)

  Lemma APred_eq_equiv_guard (x y : APred (Ev Var) protos) (s : Var -> EVal Ev) :
    equiv (lower_APred x s)
          (lower_APred y s) ->
    (pe_interp tt tt (iconj (APred_eq_guard x y)
                            (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))
               (eval_lift s)).
  Proof.
    intros (Hsame_tag & Hsame_argv).
    destruct x as (x & Hx), y as (y & Hy).
    cbn in *.
    unfold proj1_sig, lower_APred, APred_eq_guard in *.
    rewrite ! lap_tag_ap_tag.
    cbn in *.
    unfold lower_LooseAPred in *.
    cbn in *.
    inversion_clear Hsame_tag.
    destruct (Htag_eq (lap_tag x) (lap_tag x)); intuition.
    (* TODO: move this proof out? *)
    pose proof (lower_argv_preserve_length s (lap_argv x)) as Hlx.
    pose proof (lower_argv_preserve_length s (lap_argv y)) as Hly.
    generalize Hsame_argv.
    apply (list_pairwise_ind (lap_argv x) (lap_argv y)).
    + (* Same length. *)
      rewrite <- Hlx, <- Hly.
      eapply eqlistA_length.
      apply Hsame_argv.
    + (* Base case. *)
      intros _.
      apply pe_true_True.
    + (* Inductive case. *)
      intros l ls m ms Hind Heqv.
      cbn.
      inversion_clear Heqv.
      destroy_conj.
      apply ape_eq_val_eq.
      unfold eval_lift.
      now rewrite <- ! ve_state_mbind.
  Qed.

  (* TODO: work out why typeclass inference isn't working here? *)

  Instance lms_Setoid : Setoid (LMultiset (APred (EVal Ev) protos)) :=
    LMultiset_Setoid
      (A := APred (EVal Ev) protos)
      (SA := subtype_Setoid (S := APred_Setoid Tag (S_Ev := ValEx_Setoid (V_Ev := V_Ev))))
      (DA := subtype_EqDec (D := LooseAPred_EqDec Htag_eq (D_Ev := EVal_EqDec (V_Ev := V_Ev)))).

  Instance lms_ViewsSemigroup : ViewsSemigroup (LMultiset (APred (EVal Ev) protos)) :=
    LMultiset_ViewsSemigroup
      (A := APred (EVal Ev) protos).

  Instance lms_ViewsMonoid : ViewsMonoid (LMultiset (APred (EVal Ev) protos)) :=
    LMultiset_ViewsMonoid
      (A := APred (EVal Ev) protos).

  Instance lms_OrderedViewsSemigroup : OrderedViewsSemigroup (LMultiset (APred (EVal Ev) protos)) :=
    LMultiset_OrderedViewsSemigroup
      (A := APred (EVal Ev) protos).

  Instance lms_AdjView : AdjView (LMultiset (APred (EVal Ev) protos)) :=
    LMultiset_AdjView
      (A := APred (EVal Ev) protos).

  Lemma APred_eq_guard_success :
    ga_minus_success
      APred_eq_guard
      (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev))).
  Proof.
    intros p1 p2 s' Hguard_interp%APred_eq_guard_equiv.
    cbn.
    destruct (SetoidDec.equiv_dec (lower_APred p1 s') (lower_APred p2 s'));
      intuition.
    now unfold EmptyLBag.
  Qed.

  Lemma APred_eq_guard_failure :
    ga_minus_failure
      APred_eq_guard
      (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev))).
  Proof.
    intros p1 p2 s Hguard_interp%pe_not_neg.
    cbn.
    unfold compose; cbn.
    destruct (SetoidDec.equiv_dec (lower_APred p1 s) (lower_APred p2 s)) as [Heq|Hneq];
      try easy.
    now apply APred_eq_equiv_guard in Heq.
  Qed.

  Lemma APred_eq_guard_swap :
    ga_minus_swap
      APred_eq_guard
      (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev))).
  Proof.
    intros ((xtag & xargv) & xtp & Hx) ((ytag & yargv) & ytp & Hy) ls Hinterp_xy.
    cbn in *.
    unfold APred_eq_guard in *.
    rewrite ! lap_tag_ap_tag in *.
    cbn in *.
    (* Decide all of the tag decisions. *)
    destruct (Htag_eq xtag ytag) as [Hxyt|], (Htag_eq ytag xtag); intuition.
    subst.
    (* Now show that the tag prototypes are equal. *)
    rewrite H in H1.
    inversion H1; subst.
    (* This then tells us that the lengths are equal. *)
    rewrite <- H2 in H0.
    (* Now we can do pairwise induction. *)
    generalize Hinterp_xy.
    apply (list_pairwise_ind xargv yargv); intuition.
    cbn in *.
    destroy_conj.
    eapply ape_eq_val_eq, symmetry, ape_eq_val_eq, H4.
  Qed.

  (** Two [GAPred]s are _mostly_ equal if their guards are equal and their
      items are equal modulo proof of validity. *)

  Definition gapred_mostly_equal (x y : GAPred Ep (Ev Var) protos) : Prop :=
    g_guard x = g_guard y /\
    proj1_sig (g_item x) = proj1_sig (g_item y).

  Global Instance gapred_mostly_equal_Reflexive :
    Reflexive gapred_mostly_equal.
  Proof.
    easy.
  Defined.

  Global Instance gapred_mostly_equal_Symmetric :
    Symmetric gapred_mostly_equal.
  Proof.
    intros x y (Heqg & Heqi).
    now split.
  Defined.

  Global Instance gapred_mostly_equal_Transitive :
    Transitive gapred_mostly_equal.
  Proof.
    intros x y z (Heqgxy & Heqixy) (Heqgyz & Heqiyz).
    split; congruence.
  Defined.

  Global Instance gapred_mostly_equal_Equivalence :
    Equivalence gapred_mostly_equal := {}.

  Local Open Scope signature_scope.

  Definition lnorm_mostly_equal : relation (LNormVExpr (GAPred Ep (Ev Var) protos)) :=
    ForallAtoms2 gapred_mostly_equal @@ forget_lnorm (A := _).

  Definition rmpart_guard_APred : VExpr (GAPred Ep (Ev Var) protos) -> LNormVExpr (GAPred Ep (Ev Var) protos) :=
    rmpart_guard APred_eq_guard
      (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev))).

  Hypothesis Hep_dec : forall (x : Ep (Ev Var)) (s : Var -> EVal Ev), { pe_interp tt tt x s } + { ~ pe_interp tt tt x s }.

  Lemma rmpart_guard_APred_equiv (e : VExpr (GAPred Ep (Ev Var) protos)) :
    vequiv (forget_lnorm (rmpart_guard_APred e)) e
           (AL := guard_AtomLanguage Hep_dec).
  Proof.
    apply rmpart_guard_equiv.
    - apply LMultiset_FullView.
    - apply APred_eq_guard_success.
    - apply APred_eq_guard_failure.
    - apply APred_eq_guard_swap.
  Qed.

  Local Open Scope signature_scope.

  (** If the minuend and subtrahend of a guarded minus are mostly equal to
      another minuend and subtrahend,
      so are the resulting remainders and carries. *)

  Global Instance mostly_equal_ga_minus_Proper :
    Proper
      (gapred_mostly_equal
         ==> gapred_mostly_equal
         ==> (RelProd (ForallAtoms2 gapred_mostly_equal @@ (@forget_lnorm _))
                    gapred_mostly_equal))
      (ga_minus_lift APred_eq_guard
                     (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))).
  Proof.
    intros (x1g & (x1a & Hx1)) (x2g & (x2a & Hx2)) (Heqgx & Heqax)
           (y1g & (y1a & Hy1)) (y2g & (y2a & Hy2)) (Heqgy & Heqay).
    unfold gapred_mostly_equal, proj1_sig in *; cbn in *; subst.
    split; unfold "@@"; cbn; intuition.
    constructor; intuition; now constructor.
  Qed.

  Lemma mostly_equal_lcons a x y :
    ForallAtoms2 gapred_mostly_equal (forget_lnorm (lcons a x)) (forget_lnorm y) ->
    exists a' x',
      forget_lnorm y = forget_lnorm (lcons a' x')
      /\ gapred_mostly_equal a a'
      /\ (ForallAtoms2 gapred_mostly_equal @@ forget_lnorm (A := _)) x x'.
  Proof.
    destruct y as (y & Hy).
    inversion 1; cbn in *; subst.
    inversion H3; subst.
    inversion Hy; subst.
    exists b, (exist _ y2 H1); intuition.
  Qed.

  Global Instance mostly_equal_rmpartA_Proper :
    Proper
      (gapred_mostly_equal
         ==> (ForallAtoms2 gapred_mostly_equal @@ forget_lnorm (A := _))
         ==> (ForallAtoms2 gapred_mostly_equal @@ forget_lnorm (A := _)))
      (rmpartA_guard APred_eq_guard
                     (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))).
  Proof.
    unfold rmpartA_guard.
    intros a a' Ha' x y.
    generalize dependent a'.
    generalize dependent a.
    generalize dependent x.
    pattern y.
    apply LNormVExpr_ind_strong; unfold "@@".
    - (* unit *)
      intros x a a' Heqa Heqda.
      inversion Heqda; subst.
      rewrite (rmpartA_general_nil _ a').
      symmetry in H1.
      apply lnorm_lunit in H1.
      now subst.
    - (* cons *)
      intros a' v Hind (x & Hx) a a'' Hmeq
             (a''' & x' & Heqx & Heqa%symmetry & Heqv%symmetry)%symmetry%mostly_equal_lcons.
      simpl in *.
      subst.
      replace
        (forget_lnorm
           (rmpartA_general
              (ga_minus_lift APred_eq_guard
                             (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))
              ) a
              (exist _ (acons a''' (` x')) Hx)))
        with
          (forget_lnorm
             (rmpartA_general
                (ga_minus_lift APred_eq_guard (I_Ep := ImplPredEx_EVal (S_Val := S_Val))) a
                (lcons a''' x'))).
      2: {
        unfold rmpartA_general.
        f_equal.
        f_equal.
        now apply lnorm_fold_left_prfirr.
      }
      destruct (ga_minus_lift
                  APred_eq_guard
                  a''' a
                  (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))
                  (I_Ep := ImplPredEx_EVal (S_Val := S_Val))) as
          (r & d) eqn:Hminus.
      rewrite (rmpartA_general_cons _ _ _ _ (r := r) (d' := d)); try easy.
      destruct (ga_minus_lift
                  APred_eq_guard
                  a' a''
                  (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))
                  (I_Ep := ImplPredEx_EVal (S_Val := S_Val))) as
          (r' & d') eqn:Hminus'.
      rewrite (rmpartA_general_cons _ _ _ _ (r := r') (d' := d')); try easy.
      destruct (mostly_equal_ga_minus_Proper Heqa Hmeq)
        as (Hgme_1 & Hgme_2); unfold "@@" in *.
      rewrite Hminus, Hminus' in Hgme_1.
      rewrite Hminus, Hminus' in Hgme_2.
      subst.
      apply forall2_lapp; try easy.
      apply Hind; try easy.
  Qed.

  Global Instance mostly_equal_Symmetric : Symmetric gapred_mostly_equal.
  Proof.
    induction 1; constructor; intuition.
  Defined.

  Global Instance mostly_equal_rmpartB_Proper :
    Proper
      ((ForallAtoms2 gapred_mostly_equal @@ forget_lnorm (A := _))
         ==> (ForallAtoms2 gapred_mostly_equal @@ forget_lnorm (A := _))
         ==> (ForallAtoms2 gapred_mostly_equal @@ forget_lnorm (A := _)))
      (rmpartB_general (flip (rmpartA_guard APred_eq_guard
                                            (P_Ep := PredEx_EVal (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))))).
  Proof.
    intros x x' Heqx y.
    revert x x' Heqx.
    pattern y.
    apply LNormVExpr_ind_strong.
    - (* unit *)
      intros x x' Hx_gme y'.
      inversion 1; subst.
      rewrite rmpartB_general_nil.
      symmetry in H1.
      apply lnorm_lunit in H1.
      now subst.
    - (* cons *)
      intros a v Hind x x' Heqx (y' & Hy') (a''' & v' & Heq_cons & Heqa & Heqv)%mostly_equal_lcons.
      cbn in Heq_cons.
      subst.
      unfold "@@", rmpartA_guard.
      transitivity
        (forget_lnorm (rmpartB_general (flip (rmpartA_general (ga_minus_lift APred_eq_guard                              (P_Ep := PredEx_EVal
                                                                                                                                    (P_Ep := PredEx_Domain_Chained Var (V_Ev := V_Ev)))))) x' (lcons a''' v'))).
      {
        rewrite ! rmpartB_general_cons.
        apply Hind; try easy.
        now apply mostly_equal_rmpartA_Proper.
      }
      unfold rmpartB_general.
      f_equiv.
      now apply lnorm_fold_left_prfirr.
  Qed.

  Global Instance mostly_equal_rmpart_Proper :
    Proper
      ((ForallAtoms2 gapred_mostly_equal)
         ==> (ForallAtoms2 gapred_mostly_equal @@ forget_lnorm (A := _)))
      rmpart_guard_APred.
  Proof.
    intros x.
    induction x; intro y; inversion 1.
    - now constructor.
    - constructor; subst; easy.
    - apply (forall2_lapp _ _ _ _ (IHx1 x3 H3) (IHx2 y2 H5)).
    - apply (mostly_equal_rmpartB_Proper (IHx1 x3 H3) (IHx2 y2 H5)).
Qed.

End APred_facts.

Delimit Scope apred_scope with apred.
Local Open Scope apred_scope.

(*  Notation "A {*  xs }" := (mk_APred _ A xs) (at level 0) : apred_scope.

  Notation "A { x , .. , y }" := A {*(cons x .. (cons y nil) ..)}
                                   (at level 0) : apred. *)