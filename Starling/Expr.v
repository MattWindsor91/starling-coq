(** * Starling abstract and concrete expressions *)

Require Import
        Arith
        Coq.Program.Basics
        Coq.Relations.Relation_Definitions
        Coq.Sets.Multiset
        Coq.Strings.String
        ZArith.

Require Import
        Starling.Utils.Option.Facts
        Starling.Views.Classes.

(** ** Starling values

    Starling values are either integers or Booleans.
    At time of writing, the Starling tool also supports arrays and other
    compound types, but we do not model them here (besides, we can probably
    leave them to the expression language). *)

Local Open Scope Z_scope.

Inductive Val :=
  | VZ      : Z -> Val
  | VBool   : bool -> Val.

(* We have to define value equality manually, rather than through Scheme
   Equality, so that we can reuse the existing results for Z/Bool.

   Any ideas on how to avoid this are welcome. *)

Lemma Val_eq_dec (x y : Val) :
  { x = y } + { x <> y }.
Proof.
  decide equality.
  - apply Z.eq_dec.
  - apply Bool.bool_dec.
Qed.

Definition Val_beq (x y : Val) : bool :=
  if (Val_eq_dec x y) then true else false.

(** [val_beq] entails, and is entailed by, equality. *)

Lemma Val_beq_eq (x y : Val) :
  Val_beq x y = true -> x = y.
Proof.
  (* Appeal to similar facts about the integers and Booleans. *)
  intro H.
  unfold Val_beq in H.
  destruct (Val_eq_dec x y) as [Hxy|].
  - exact Hxy.
  - discriminate.
Qed.

Lemma Val_eq_beq (x y : Val) :
  x = y -> Val_beq x y = true.
Proof.
  (* As above. *)
  intros [].
  unfold Val_beq.
  destruct (Val_eq_dec x x) as [Hxy|].
  - reflexivity.
  - contradiction.
Qed.


Open Scope string_scope.

Definition Env: Set := (string -> option Val).

(** ** The expression type class

    Starling is designed to be meta-theoretically independent of the expression
    language used, and the semantics thereof.  What Starling does need, however,
    is the ability to interpret expressions as integers and Booleans.

    We package this restriction up as the [Eval] typeclass. *)

Class Eval ( A (* Abstract type *)
             C (* Concrete type *)
             E (* Environment *)
             : Type ) :=
  { intr          : C -> A; 
    eval          : E -> A -> option C;
    intr_eval e c : eval e (intr c) = Some c }.

(** Eval is reflexive. *)

Instance Id_Eval {A E} : Eval A A E :=
  { intr   := id   ;
    eval e := Some }.
Proof.
  reflexivity.
Defined.

(** It also distributes through lists... *)

Instance list_Eval {A C E} `{EA: Eval A C E} : Eval (list A) (list C) E :=
  { intr     := List.map intr                 ;
    eval e a := collect (List.map (eval e) a) }.
Proof.
  intros e v.
  (* Proof by induction on the original list. *)
  induction v.
  - reflexivity.
  - simpl.
    (* By Eval A B, we know that eval . intr = id. *)
    rewrite (intr_eval e a), IHv.
    reflexivity.
Qed.

Definition eval_eq {A C E} `{Eval A C E} (x y : A) :=
  forall (e : E), eval e x = eval e y.

(* TODO: partiality *)

Definition sep_mset {A} (x y : multiset A) : Prop :=
  forall a : A,
    multiplicity x a = Nat.zero \/ multiplicity y a = Nat.zero.

(** A specialisation of [Eval] is the class that can be evaluated to Booleans,
    which we call [BEval].
n
    [BEval] objects have a conjunction operator with specific properties. *)
Class BEval (B E : Type) `{Eval B bool E} :=
  { conj                 : B -> B -> B;
    conj_diag   x        : eval_eq x (conj x x);
    conj_comm   x y      : eval_eq (conj x y) (conj y x);
    conj_assoc  x y z    : eval_eq (conj x (conj y z)) (conj (conj x y) z);
    conj_mono   x y e    : eval e (conj x y) = Some true -> eval e x = Some true }.

Infix "&" := conj (at level 80, right associativity).

Corollary conj_mono_flip {E R} `{BEval E R} :
  forall x e,
    eval e x = Some false ->
      forall y, eval e (x & y) = None \/ eval e (x & y) = Some false.
Proof.
  intros x e Heval y.
  case_eq (eval e (x & y)).
  - intros b Hexy.
    right.
    destruct b.
    + contradict Heval.
      rewrite (conj_mono x y e Hexy).
      discriminate.
    + f_equal.
  - left.
    reflexivity.
Qed.

(** If we can generate an integer from an expression, we can generate a natural
    from it, partial on the integer being non-negative. *)

Local Open Scope Z_scope.

Definition Z_to_nat (z : Z) : option nat :=
  if z >=? 0 then Some (Z.to_nat z) else None.

Definition lift_eval_Z_to_nat {A} (f : Env -> A -> option Z) (e : Env) (v : A) : option nat :=
  bind Z_to_nat (f e v).

Instance nat_Z_compose_Eval (A E : Type) `{ EA : Eval A Z E } : Eval A nat E :=
  { intr     := compose intr Z.of_nat    ;
    eval e v := bind Z_to_nat (eval e v) }.
Proof. 
  intros e v.
  unfold compose.
  unfold lift_eval_Z_to_nat.
  rewrite intr_eval.
  unfold bind.
  unfold Z_to_nat.
  rewrite Nat2Z.id.
  destruct v; reflexivity.
Defined.


(** Values are integer and Boolean-producing expressions. *)

Definition eval_Val_Z (v : Val) : option Z :=
  match v with
  | VZ z => Some z
  | _ => None
  end.

Instance Z_Val_Eval {E} : Eval Val Z E :=
  { intr   := VZ         ;
    eval e := eval_Val_Z }.
Proof. 
  reflexivity.
Defined.

Definition eval_Val_bool (v : Val) : option bool :=
  match v with
  | VBool b => Some b
  | _ => None
  end.

Instance Bool_Val_Eval {E} : Eval Val bool E :=
  { intr   := VBool         ;
    eval e := eval_Val_bool }.
Proof. 
  reflexivity.
Defined.

(** ** Concrete expressions

    Though Starling is agnostic as to what the expressions of its underlying
    theory actually are, we provide an encoding of a subset of the expression
    language understood by the tool, as well as a semantics for that language,
    for purposes of demonstration. *)

Inductive IExpr :=
  | IVar   : string         -> IExpr
  | IZ     : Z              -> IExpr
  | IAdd   : IExpr -> IExpr -> IExpr
  | ISub   : IExpr -> IExpr -> IExpr
  | IMul   : IExpr -> IExpr -> IExpr
  | IDiv   : IExpr -> IExpr -> IExpr.

Inductive BExpr :=
  | BVar   : string         -> BExpr
  | BTrue  :                   BExpr
  | BFalse :                   BExpr
  | BAnd   : BExpr -> BExpr -> BExpr
  | BOr    : BExpr -> BExpr -> BExpr
  | BImpl  : BExpr -> BExpr -> BExpr
  | BBEq   : BExpr -> BExpr -> BExpr
  | BIEq   : IExpr -> IExpr -> BExpr
  | BGt    : IExpr -> IExpr -> BExpr
  | BGe    : IExpr -> IExpr -> BExpr
  | BLe    : IExpr -> IExpr -> BExpr
  | BLt    : IExpr -> IExpr -> BExpr
  | BNot   : BExpr          -> BExpr.

Inductive Expr :=
  | EB     : BExpr          -> Expr
  | EI     : IExpr          -> Expr.


Local Open Scope Z_scope.
Fixpoint eval_IExpr_Z (v : Env) (i : IExpr) : option Z :=
  match i with
  | IVar x   => bind (fun vx => match vx with | VZ vz => Some vz | _ => None end) (v x) 
  | IZ x     => Some x
  | IAdd x y => map2 (fun vx vy => vx + vy) (eval_IExpr_Z v x) (eval_IExpr_Z v y)
  | ISub x y => map2 (fun vx vy => vx - vy) (eval_IExpr_Z v x) (eval_IExpr_Z v y)
  | IMul x y => map2 (fun vx vy => vx * vy) (eval_IExpr_Z v x) (eval_IExpr_Z v y)
  | IDiv x y => map2 (fun vx vy => vx / vy) (eval_IExpr_Z v x) (eval_IExpr_Z v y)
  end.

Definition eval_IExpr (v : Env) (i : IExpr) : option Val :=
  option_map VZ (eval_IExpr_Z v i).

Local Open Scope bool_scope.

Section short_circuit_logic.
  (** Short-circuiting, partial [andb]. *)
    
  Definition ando (x y: option bool): option bool :=
    match x with
    | Some false => Some false
    | None =>
      match y with
      | Some false => Some false
      | _ => None
      end
    | Some true => y
    end.
  
  Lemma ando_diag (x: option bool): x = ando x x.
  Proof.
    destruct x as [x|]; try destruct x; reflexivity.
  Qed.

  Lemma ando_comm (x y: option bool): ando x y = ando y x.
  Proof.
    destruct x as [x|], y as [y|]; try destruct x; try destruct y; reflexivity.
  Qed.

  Lemma ando_assoc (x y z: option bool): ando x (ando y z) = ando (ando x y) z.
  Proof.
    destruct x as [x|], y as [y|], z as [z|];
      try destruct x; try destruct y; try destruct z;
        reflexivity.
  Qed.

  Lemma ando_false (x: option bool): ando x (Some false) = Some false.
  Proof.
    destruct x as [x|]; try destruct x; reflexivity.
  Qed.

  Lemma ando_true (x: option bool): ando x (Some true) = x.
  Proof.
    destruct x as [x|]; try destruct x; reflexivity.
  Qed.
  
  Lemma ando_andb (x y: bool): ando (Some x) (Some y) = Some (andb x y).
  Proof.
    (* Case analysis. *)
    destruct x, y; reflexivity.
  Qed.

  (** Short-circuiting, partial [orb]. *)

  Definition oro (x y: option bool): option bool :=
    match x with
    | Some true => Some true
    | None =>
      match y with
      | Some true => Some true
      | _ => None
      end
    | Some false => y
    end.
  
  Lemma oro_diag (x: option bool): x = oro x x.
  Proof.
    destruct x as [x|]; try destruct x; reflexivity.
  Qed.

  Lemma oro_comm (x y: option bool): oro x y = oro y x.
  Proof.
    destruct x as [x|], y as [y|]; try destruct x; try destruct y; reflexivity.
  Qed.

  Lemma oro_assoc (x y z: option bool): oro x (oro y z) = oro (oro x y) z.
  Proof.
    destruct x as [x|], y as [y|], z as [z|];
      try destruct x; try destruct y; try destruct z;
        reflexivity.
  Qed.

  Lemma oro_true (x: option bool): oro x (Some true) = Some true.
  Proof.
    destruct x as [x|]; try destruct x; reflexivity.
  Qed.

  Lemma oro_orb (x y: bool): oro (Some x) (Some y) = Some (orb x y).
  Proof.
    (* Case analysis. *)
    destruct x, y; reflexivity.
  Qed.

  (** Lukasiewicz implication. *)
  
  Definition implo (x y: option bool): option bool :=
    match x with
    | Some true => y
    | None =>
      match y with
      | Some false => None
      | _ => Some true
      end
    | Some false => Some true
    end.

  Lemma implo_implb (x y: bool): implo (Some x) (Some y) = Some (implb x y).
  Proof.
    destruct x, y; reflexivity.
  Qed.
End short_circuit_logic.

Fixpoint eval_BExpr_bool (v : Env) (b : BExpr) : option bool :=
  match b with
  | BVar x    => bind (fun vx => match vx with | VBool vx => Some vx | _ => None end) (v x)
  | BTrue     => Some true
  | BFalse    => Some false
  | BAnd  x y => ando                                (eval_BExpr_bool v x) (eval_BExpr_bool v y)
  | BOr   x y => oro                                 (eval_BExpr_bool v x) (eval_BExpr_bool v y)
  | BImpl x y => implo                               (eval_BExpr_bool v x) (eval_BExpr_bool v y)
  | BBEq x y  => map2 Bool.eqb                       (eval_BExpr_bool v x) (eval_BExpr_bool v y)
  | BIEq x y  => map2 Z.eqb                          (eval_IExpr_Z    v x) (eval_IExpr_Z    v y)
  | BGt  x y  => map2 Z.gtb                          (eval_IExpr_Z    v x) (eval_IExpr_Z    v y)
  | BGe  x y  => map2 Z.geb                          (eval_IExpr_Z    v x) (eval_IExpr_Z    v y)
  | BLe  x y  => map2 Z.leb                          (eval_IExpr_Z    v x) (eval_IExpr_Z    v y)
  | BLt  x y  => map2 Z.ltb                          (eval_IExpr_Z    v x) (eval_IExpr_Z    v y)
  | BNot x    => option_map negb                     (eval_BExpr_bool v x)
  end.

Definition eval_BExpr (v : Env) (b : BExpr) : option Val :=
  option_map VBool (eval_BExpr_bool v b).

Definition eval_Expr (v : Env) (e : Expr) : option Val :=
  match e with
  | EB b => eval_BExpr v b
  | EI i => eval_IExpr v i
  end.

Definition eval_IExpr_nat (v : Env) (i : IExpr) : option nat :=
  match (eval_IExpr_Z v i) with
  | Some vx => if vx >=? 0 then Some (Z.to_nat vx) else None
  | _ => None
  end.

Definition IExpr_dec (x y : IExpr) :
  { x = y } + { x <> y }.
Proof.
  decide equality.
  - apply string_dec.
  - apply Z.eq_dec.
Defined.

Definition BExpr_dec (x y : BExpr) :
  { x = y } + { x <> y }.
Proof.
  repeat decide equality.
Defined.

Definition Expr_dec (x y : Expr) :
  { x = y } + { x <> y }.
Proof.
  decide equality.
  - apply BExpr_dec.
  - apply IExpr_dec.
Defined.

(** We can define [Eval] classes mapping IExprs to integers,
    BExprs to Booleans, and Exprs to integers, Booleans, and Vals. *)

Instance Z_IExpr_Eval : Eval IExpr Z Env :=
  { intr := IZ           ;
    eval := eval_IExpr_Z }.
Proof. 
  reflexivity.
Defined.

Definition intr_bool (b : bool) : BExpr :=
  if b then BTrue else BFalse.

Global Instance bool_BExpr_Eval : Eval BExpr bool Env :=
  { intr := intr_bool       ;
    eval := eval_BExpr_bool }.
Proof. 
  intros e v.
  destruct v; reflexivity.
Defined.

Lemma BAnd_diag (x : BExpr) :
  eval_eq x (BAnd x x).
Proof.
  intro e.
  apply ando_diag.
Qed.  

Lemma BAnd_comm (x y : BExpr) :
  eval_eq (BAnd x y) (BAnd y x).
Proof.
  intro e.
  apply ando_comm.
Qed.

Lemma BAnd_assoc (x y z : BExpr) :
  eval_eq (BAnd x (BAnd y z)) (BAnd (BAnd x y) z).
Proof.
  intro e.
  apply ando_assoc.
Qed.

Lemma BAnd_mono (x y : BExpr) (e : Env) :
  eval_BExpr_bool e (BAnd x y) = Some true ->
  eval_BExpr_bool e x = Some true.
Proof.
  intro Hxy.
  simpl in Hxy.
  unfold ando in Hxy.
  destruct (eval_BExpr_bool e x) as [ex|], (eval_BExpr_bool e y) as [ey|].
  - destruct ex.
    + reflexivity.
    + discriminate.
  - destruct ex; discriminate.
  - destruct ey; discriminate.
  - exact Hxy.
Qed.

Global Instance bool_BExpr_BEval : BEval BExpr Env :=
  { conj := BAnd;
    conj_diag := BAnd_diag;
    conj_comm := BAnd_comm;
    conj_assoc := BAnd_assoc;
    conj_mono := BAnd_mono }.
