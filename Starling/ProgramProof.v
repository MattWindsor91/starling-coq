From Coq Require Import
     Sets.Ensembles
     Program.Basics
     Lists.List
     Classes.SetoidClass
     Relations.Relation_Definitions.

From Starling Require Import
     Views.Classes
     Views.Frameworks.Common
     Views.Frameworks.CVF.Core
     Views.Frameworks.LVF
.

Import ListNotations.

Set Implicit Arguments.
Delimit Scope outline_scope with out.
Local Open Scope outline_scope.

(** * Proofs about whole programs *)

Section ProgramProof_types.

  (** [BinOp] is the set of binary operators:

    - [BPar] is parallel composition;
    - [BSeq] is sequential composition;
    - [BNdt] is nondeterministic choice. *)

  Inductive BinOp: Set := BPar | BSeq | BNdt.

  (** [Outline] is the type of proof outlines:

    - [OAtomic] is an atomic action;
    - [OIter] is iteration;
    - [OBin] is a binary operator over [BinOp]. *)

  Inductive Outline (V: Type) (C: Set) :=
  | OSkip   (p : V)                                 (q : V)
  | OAtomic (p : V) (c    : C)                      (q : V)
  | OIter   (p : V) (o    : Outline V C)            (q : V)
  | OFrame  (p : V) (f : V) (o : Outline V C)       (q : V)
  | OBin    (p : V) (o1 o2: Outline V C) (b: BinOp) (q : V).

End ProgramProof_types.

Arguments OSkip [V C].


Notation "{{ p }} c1 '+' c2 {{ q }}" :=
  (OBin p c1 c2 BNdt q)
    (at level 0, c1, c2 at next level): outline_scope.
Notation "{{ p }} c1 '||' c2 {{ q }}" :=
  (OBin p c1 c2 BPar q)
    (at level 0, c1, c2 at next level): outline_scope.
Notation "{{ p }} c1 ';' c2 {{ q }}" :=
  (OBin p c1 c2 BSeq q)
    (at level 0, c1, c2 at next level): outline_scope.
Notation "{{ p }} 'skip' {{ q }}" :=
  (OSkip p q)
    (at level 0): outline_scope.
Notation "{{ p }} < c > {{ q }}" :=
  (OAtomic p c q)
    (at level 0, c at next level): outline_scope.
Notation "{{ p }} c '*' {{ q }}" :=
  (OIter p c q)
    (at level 0, c at next level): outline_scope.
Notation "{{ p }} 'frame' f 'in' c {{ q }}" :=
  (OFrame p f c q)
    (at level 0, c at next level): outline_scope.

Section Outline.
  (** [V] is the Views semigroup, with setoid instance [SV] and
    semigroup instance [VV]. *)

  Context {V: Type}.

  Context {SV: Setoid V} {VV: ViewsSemigroup V}.

  (** [C] is the atomic command set. *)

  Context {C: Set}.

  (** [S] is the state set. *)

  Context {S: Set}.

  Section Outline_accessors.
    (** ** Accessors for parts of an [Outline]

      In the following, we refer to the outline being accessed as [o]. *)
    
    Variable o: Outline V C.
    
    (** [outpre] fetches the precondition of outline [o]. *)

    Definition outpre: V :=
      match o with
      | {{ p }} _ + _ {{ _ }} => p
      | {{ p }} _ || _ {{ _ }} => p
      | {{ p }} _ ; _ {{ _ }} => p
      | {{ p }} skip {{ _ }} => p
      | {{ p }} frame _ in _ {{ _ }} => p                                 
      | {{ p }} < _ > {{ _ }} => p
      | {{ p }} _ * {{ _ }} => p
      end.

    (** [outpost] fetches the postcondition of outline [o]. *)

    Definition outpost: V :=
      match o with
      | {{ _ }} _ + _ {{ q }} => q
      | {{ _ }} _ || _ {{ q }} => q
      | {{ _ }} _ ; _ {{ q }} => q
      | {{ _ }} skip {{ q }} => q
      | {{ _ }} frame _ in _ {{ q }} => q
      | {{ _ }} < _ > {{ q }} => q
      | {{ _ }} _ * {{ q }} => q        
      end.

    (** [ol_has_conds] is [True] when outline [o] has a precondition
      equivalent to [p], and postcondition equivalent to [q]. *)

    Definition ol_has_conds (p q: V): Prop :=
      outpre == p /\ outpost == q.

  End Outline_accessors.

  Section Outline_vcs.    
    (** [atomic_vcs] is the list of atomic triple verification conditions for
      [o]. *)

    Local Open Scope views_scope.
    
    Fixpoint atomic_vcs (o: Outline V C): list (ViewsAxiom V C) :=
      match o with
      | OSkip   p q       => [ <|p|> (Id C)  <|q|> ]
      | OAtomic p c q     => [ <|p|> (Cmd c) <|q|> ]
      | OIter   _ c _     => atomic_vcs c
      | OFrame  _ _ c _   => atomic_vcs c
      | OBin    _ c d _ _ => atomic_vcs c ++ atomic_vcs d
      end.

    Definition bin_side_vcs (b: BinOp) (p q : V) (c d: Outline V C): list (V * V) :=
      (match b with
       | BPar =>
         [(p, outpre c * outpre d); (outpost c * outpost d, q)]
       | BSeq =>
         [(p, outpre c); (outpost c, outpre d); (outpost d, q)]
       | BNdt =>
         [(p, outpre c); (p, outpre d); (outpost c, q); (outpost d, q)]
       end)%views.
    
    (** [side_vcs] is the list of verification side-conditions for [o]. *)
    
    Fixpoint side_vcs (o: Outline V C): list (V * V) :=
      match o with
      | OSkip   _ _       => []        
      | OAtomic _ _ _     => []
      | OIter   p c q     => [(p, outpre c); (outpre c, q); (outpost c, outpre c)] ++ side_vcs c
      | OFrame  p f c q   => [(p, outpre c * f); (outpost c * f, q)]%views ++ side_vcs c
      | OBin    p c d b q => bin_side_vcs b p q c d ++ side_vcs c ++ side_vcs d
      end.

    (** [to_id_vc] maps a side-condition [s] to an id-transition. *)

    Definition to_id_vc '(p, q) : ViewsAxiom V C :=
      <|p|> (Id _) <|q|>.
    
    (** [in_atomic_vcs] tells us when a verification condition [x] is in
      the atomic conditions of [o]. *)
    
    Definition in_atomic_vcs (o: Outline V C) (x: ViewsAxiom V C): Prop :=
      List.In x (atomic_vcs o).

    Hint Unfold in_atomic_vcs.

    (** [in_side_vcs] tells us when a pair of views [p] and [q] is in
      the side-conditions of [o]. *)
    
    Definition in_side_vcs (o: Outline V C) (p q: V): Prop :=
      List.In (p, q) (side_vcs o).

    Hint Unfold in_side_vcs.

    
    (** [vcs] is the set of all verification conditions for [o]. *)
    
    Definition vcs (o: Outline V C): list (ViewsAxiom V C) :=
      atomic_vcs o ++ map to_id_vc (side_vcs o).

    (** [in_vcs] tells us when a verification condition [x] is in the
      conditions of [o]. *)
    
    Definition in_vcs (o: Outline V C) (x: ViewsAxiom V C): Prop :=
      List.In x (vcs o).

    Lemma in_vcs_dichotomy (o: Outline V C) (p q: V) (lc: LabelC C):
      in_vcs o <|p|> lc <|q|> ->
      in_atomic_vcs o <|p|> lc <|q|> \/
      (lc = Id C /\ in_side_vcs o p q).
    Proof.
      unfold in_vcs, vcs.
      intros [Hina|((p' & q') & Hid & Hinb)%in_map_iff]%in_app_or.
      - left.
        exact Hina.
      - right.
        injection Hid.
        intros -> -> ->.
        split.
        + reflexivity.
        + assumption.
    Qed.

    Lemma in_atomic_vcs_bin (b: BinOp) (p q: V) (x: ViewsAxiom V C) (c d: Outline V C):
      in_atomic_vcs (OBin p c d b q) x <->
      in_atomic_vcs c x \/ in_atomic_vcs d x.
    Proof.
      split.
      - intros Hin_bin.
        unfold in_atomic_vcs, atomic_vcs in Hin_bin.
        apply in_app_or in Hin_bin.
        exact Hin_bin.
      - intros Hin_or.
        unfold in_atomic_vcs, atomic_vcs.
        apply in_or_app, Hin_or.
    Qed.

    Lemma in_side_vcs_bin_trichotomy (b: BinOp) (p q ap aq: V) (c d: Outline V C):
      in_side_vcs (OBin p c d b q) ap aq ->
      In (ap, aq) (bin_side_vcs b p q c d) \/
      in_side_vcs c ap aq \/
      in_side_vcs d ap aq.
    Proof.
      intros Hin_side.
      unfold in_side_vcs, side_vcs in Hin_side.
      destruct (in_app_or _ _ _ Hin_side) as [Hin_bsv|Hin_osv].
      - left.
        assumption.
      - right.
        apply in_app_or, Hin_osv.
    Qed.
    
    Lemma in_atomic_vcs_in_vcs (o: Outline V C) (x: ViewsAxiom V C):
      in_atomic_vcs o x -> in_vcs o x.
    Proof.
      intro Havcs.
      apply in_or_app.
      left.
      apply Havcs.
    Qed.

    Lemma in_side_vcs_in_vcs (o: Outline V C) (p q: V):
      in_side_vcs o p q -> in_vcs o <|p|> (Id C) <|q|>.
    Proof.
      intro Hsvcs.
      apply in_or_app.
      right.
      apply in_map_iff.
      exists (p, q).
      split.
      - reflexivity.
      - exact Hsvcs.
    Qed.
    
    Section Outline_vc_accessors.

      Lemma skip_axiom (p q : V) :
        in_vcs (OSkip p q) <|p|> (Id C) <|q|>.
      Proof.
        apply List.in_or_app.
        left.
        left.
        reflexivity.
      Qed.
      
      Lemma atomic_axiom (p q: V) (c : C):
        in_vcs (OAtomic p c q) <|p|> (Cmd c) <|q|>.
      Proof.
        apply List.in_or_app.
        left.
        left.
        reflexivity.
      Qed.

    End Outline_vc_accessors.

    Section Outline_vc_inclusions.
      Lemma iter_incl (o: Outline V C) (a: ViewsAxiom V C):
        in_vcs o a ->
        forall p q: V, in_vcs (OIter p o q) a.
      Proof.
        intros Hin p q.
        destruct (List.in_app_or _ _ a Hin) as [Hin_a|Hin_s];
          apply List.in_or_app.
        - left.
          apply Hin_a.
        - repeat right.
          apply Hin_s.
      Qed.

      Lemma frame_incl (o : Outline V C) (a : ViewsAxiom V C):
        in_vcs o a ->
        forall p q f: V, in_vcs (OFrame p f o q) a.
      Proof.
        intros Hin p q f.
        destruct (List.in_app_or _ _ a Hin) as [Hin_a|Hin_s];
          apply List.in_or_app.
        - left.
          apply Hin_a.
        - repeat right.
          apply Hin_s.
      Qed.
      
      Lemma bin_incl (o1 o2: Outline V C) (a: ViewsAxiom V C):
        in_vcs o1 a \/ in_vcs o2 a ->
        forall (p q: V) (b: BinOp), in_vcs (OBin p o1 o2 b q) a.
      Proof.
        intros Hin p q b.
        destruct Hin as [Hin|Hin],
                        (List.in_app_or _ _ a Hin) as [Hin_a|Hin_s];
          apply List.in_or_app.
        - left.
          apply List.in_or_app.
          left.
          apply Hin_a.
        - repeat right.
          simpl.
          repeat rewrite List.map_app.
          apply List.in_or_app.
          right.
          apply List.in_or_app.
          left.
          apply Hin_s.
        - left.
          apply List.in_or_app.
          right.
          apply Hin_a.
        - repeat right.
          simpl.
          repeat rewrite List.map_app.
          apply List.in_or_app.
          right.
          apply List.in_or_app.
          right.
          apply Hin_s.      
      Qed.      
    End Outline_vc_inclusions.

    Section Outline_soundness.
      Variable
        (axs: Ensemble (ViewsAxiom V C)).
      
      Definition outline_sound (o: Outline V C) :=
        Included _ (in_vcs o) axs.
      
      Lemma outline_sound_atomic_vcs (o: Outline V C) :
        outline_sound o ->
        Included _ (in_atomic_vcs o) axs.
      Proof.
        intros Hos.
        transitivity (in_vcs o).
        - intros x.
          apply in_atomic_vcs_in_vcs.
        - apply Hos.
      Qed.

      Corollary outline_sound_atomic_vcs_bin (b: BinOp) (c1 c2: Outline V C) (p q : V):
        outline_sound c1 ->
        outline_sound c2 ->
        Included _ (in_atomic_vcs (OBin p c1 c2 b q)) axs.
      Proof.
        intros Hos1 Hos2 ax [Hin_c1|Hin_c2]%in_atomic_vcs_bin.
        - apply outline_sound_atomic_vcs with (o := c1); assumption.
        - apply outline_sound_atomic_vcs with (o := c2); assumption.        
      Qed.

      (** If both sides of a binop outline are sound, then we can show that
        a verification condition is in the side *)
      
      Lemma outline_sound_side_vcs (o: Outline V C) (p q: V):
        outline_sound o ->
        in_side_vcs o p q ->
        Ensembles.In _ axs <|p|> (Id C) <|q|>.
      Proof.
        intros Hos Hin_vcs.
        apply Hos, in_side_vcs_in_vcs, Hin_vcs.
      Qed.

      Lemma outline_sound_skip (p q : V) :
        outline_sound (OSkip p q) -> Ensembles.In _ axs <|p|> (Id C) <|q|>.
      Proof.
        intro Hos.
        apply Hos, skip_axiom.
      Qed.
      
      Lemma outline_sound_atom (p q : V) (c : C) :
        outline_sound (OAtomic p c q) -> Ensembles.In _ axs <|p|> (Cmd c) <|q|>.
      Proof.
        intro Hos.
        apply Hos, atomic_axiom.
      Qed.
      
      Lemma outline_sound_iter (o: Outline V C) (p q: V):
        outline_sound (OIter p o q) -> outline_sound o.
      Proof.
        intros Hos ax Hin_ax.
        apply Hos, iter_incl, Hin_ax.
      Qed.

      (** [in_list] solves a question of whether an item is in a list by
          recursively trying to match the car of the list against it by
          reflexivity. *)
      
      Ltac in_list :=
        repeat
          match goal with
          | |- In _ (_::_) => (left; reflexivity) || right
          | |- _ => fail "Not in list"
          end.

      Lemma outline_sound_iter_enter (o: Outline V C) (p q: V):
        outline_sound (OIter p o q) ->
        Ensembles.In _ axs <|p|> (Id C) <|o.(outpre)|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).        
        apply List.in_or_app.
        left.
        in_list.
      Qed.
      
      Lemma outline_sound_iter_loop (o: Outline V C) (p q: V):
        outline_sound (OIter p o q) ->
        Ensembles.In _ axs <|o.(outpost)|> (Id C) <|o.(outpre)|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).
        apply List.in_or_app.
        left.
        in_list.
      Qed.
      
      Lemma outline_sound_iter_skip (o: Outline V C) (p q: V):
        outline_sound (OIter p o q) ->
        Ensembles.In _ axs <|o.(outpre)|> (Id C) <|q|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).
        apply List.in_or_app.
        left.
        in_list.
      Qed.
      
      Lemma outline_sound_frame (o : Outline V C) (p q f : V):
        outline_sound (OFrame p f o q) ->
        outline_sound o.
      Proof.
        intros Hos a Hin_ax.
        apply Hos, frame_incl, Hin_ax.
      Qed.

      Lemma outline_sound_frame_enter (o : Outline V C) (p q f : V):
        outline_sound (OFrame p f o q) ->
        Ensembles.In _ axs <|p|> (Id C) <|o.(outpre) * f|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).        
        apply List.in_or_app.
        left.
        in_list.
      Qed.

      Lemma outline_sound_frame_exit (o : Outline V C) (p q f : V):
        outline_sound (OFrame p f o q) ->
        Ensembles.In _ axs <|o.(outpost) * f|> (Id C) <|q|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).
        apply List.in_or_app.
        left.
        in_list.
      Qed.
      
      Lemma outline_sound_bin (o1 o2: Outline V C) (p q: V) (b: BinOp):
        outline_sound (OBin p o1 o2 b q) ->
        outline_sound o1 /\ outline_sound o2.
      Proof.
        intros Hsound.
        split; intros a Ha; apply Hsound, bin_incl.
        - now left.
        - now right.
      Qed.

      Lemma outline_sound_seq_enter (o1 o2 : Outline V C) (p q : V):
        outline_sound (OBin p o1 o2 BSeq q) ->
        Ensembles.In _ axs <|p|> (Id C) <|o1.(outpre)|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).        
        apply List.in_or_app.
        left.
        unfold bin_side_vcs.
        in_list.
      Qed.

      Lemma outline_sound_seq_exit (o1 o2 : Outline V C) (p q : V):
        outline_sound (OBin p o1 o2 BSeq q) ->
        Ensembles.In _ axs <|o2.(outpost)|> (Id C) <|q|>.
      Proof.
        intro Hsound.
        refine (outline_sound_side_vcs _ _ Hsound _).        
        apply List.in_or_app.
        left.
        unfold bin_side_vcs.
        in_list.
      Qed.


      Lemma outline_sound_seq_join (o1 o2 : Outline V C) (p q : V):
        outline_sound (OBin p o1 o2 BSeq q) ->
        Ensembles.In _ axs <|o1.(outpost)|> (Id C) <|o2.(outpre)|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).        
        apply List.in_or_app.
        left.
        unfold bin_side_vcs.
        in_list.
      Qed.

      Lemma outline_sound_ndt_enter1 (o1 o2 : Outline V C) (p q : V):
        outline_sound (OBin p o1 o2 BNdt q) ->
        Ensembles.In _ axs <|p|> (Id C) <|o1.(outpre)|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).        
        apply List.in_or_app.
        left.
        unfold bin_side_vcs.
        in_list.
      Qed.

      Lemma outline_sound_ndt_enter2 (o1 o2 : Outline V C) (p q : V):
        outline_sound (OBin p o1 o2 BNdt q) ->
        Ensembles.In _ axs <|p|> (Id C) <|o2.(outpre)|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).        
        apply List.in_or_app.
        left.
        unfold bin_side_vcs.
        in_list.
      Qed.

      Lemma outline_sound_ndt_exit1 (o1 o2 : Outline V C) (p q : V):
        outline_sound (OBin p o1 o2 BNdt q) ->
        Ensembles.In _ axs <|o1.(outpost)|> (Id C) <|q|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).        
        apply List.in_or_app.
        left.
        unfold bin_side_vcs.
        in_list.
      Qed.      
      
      Lemma outline_sound_ndt_exit2 (o1 o2 : Outline V C) (p q : V):
        outline_sound (OBin p o1 o2 BNdt q) ->
        Ensembles.In _ axs <|o2.(outpost)|> (Id C) <|q|>.
      Proof.
        intro Hos.
        refine (outline_sound_side_vcs _ _ Hos _).        
        apply List.in_or_app.
        left.
        unfold bin_side_vcs.
        in_list.
      Qed.
    End Outline_soundness.
  End Outline_vcs.
End Outline.

Definition outline_sound_inst
           (V : Type)
           (C SSt : Set)
           {SV : Setoid V}
           {VV : ViewsSemigroup V}
           (i : PreInstance C SSt)
           (o: Outline V C) := outline_sound i.(v_axioms) o.

Section Outline_local.
  Variables
    (V    : Type)  (* Shared views *)
    (C    : Set)   (* Atomic actions *)
    (LSt  : Set)   (* Local state *)
    (SSt  : Set).  (* Shared state *)

  Context
    {SV   : Setoid V}
    {VV   : ViewsSemigroup V}.
  
  Fixpoint is_local_outline (o : Outline (LSt -> V) C) : Prop :=
    match o with
    | OBin _ _ _ BPar _ => False
    | OBin _ o1 o2 b _ => is_local_outline o1 /\ is_local_outline o2
    | OIter _ o _ => is_local_outline o
    | OFrame _ f o _ => is_local_outline o /\ exists (ff : V), f == const ff                                   
    | OAtomic _ _ _ => True
    | OSkip _ _ => True                        
    end.

  Definition LocalOutline : Type := { x : Outline (LSt -> V) C | is_local_outline x }.

  Definition lo_to_o (l : LocalOutline) : Outline (LSt -> V) C := proj1_sig l.
  
  Definition loskip (p : LSt -> V) (c : C) (q : LSt -> V) : LocalOutline :=
    exist is_local_outline {{ p }} skip {{ q }} I.
  
  Definition loatomic (p : LSt -> V) (c : C) (q : LSt -> V) : LocalOutline :=
    exist is_local_outline {{ p }} < c > {{ q }} I.

  Definition loframe (p : LSt -> V) (f : V) (o : LocalOutline) (q : LSt -> V) : LocalOutline :=
    exist is_local_outline
          ({{ p }} frame (const f) in (proj1_sig o) {{ q }})
          (conj (proj2_sig o)
                (ex_intro (fun ff => const f == const ff) _ (reflexivity _))).

  Definition loiter (p : LSt -> V) (o : LocalOutline) (q : LSt -> V) : LocalOutline :=
    exist is_local_outline
          ({{ p }} (proj1_sig o)* {{ q }})
          (proj2_sig o).
  
  Definition lobin (p : LSt -> V) (b : BinOp) (Hb : b <> BPar) (o1 o2 : LocalOutline) (q : LSt -> V) : LocalOutline.
  Proof.
    refine (exist is_local_outline (OBin p (proj1_sig o1) (proj1_sig o2) b q) _).
    destruct b; try contradiction; apply (conj (proj2_sig o1) (proj2_sig o2)).
  Defined.

  Definition loseq (p : LSt -> V) (o1 o2 : LocalOutline) (q : LSt -> V) : LocalOutline.
  Proof.
    refine (lobin p _ o1 o2 q (b := BSeq)).
    discriminate.
  Qed.

  Definition londt (p : LSt -> V) (o1 o2 : LocalOutline) (q : LSt -> V) : LocalOutline.
  Proof.
    refine (lobin p _ o1 o2 q (b := BNdt)).
    discriminate.
  Qed.

  Definition local_outline_to_prog : LocalOutline -> LProg C.
  Proof.
    intros (o & Ho).
    induction o.
    - exact (LSkip C).
    - exact (LAtom c).
    - apply LIter.
      apply (IHo Ho).
    - apply IHo, Ho.
    - destruct b.
      + contradiction Ho.
      + destruct Ho as (Ho1 & Ho2).
        apply LSeq.
        * apply (IHo1 Ho1).
        * apply (IHo2 Ho2).
      + destruct Ho as (Ho1 & Ho2).
        apply LNdt.
        * apply (IHo1 Ho1).
        * apply (IHo2 Ho2).          
  Defined.
  
  Theorem local_outline_sound_safe
          (li : SoundLVFInstance C LSt SSt)
          (o : LocalOutline) :
    outline_sound li.(proj1_sig).(lvfi_axioms) (proj1_sig o) ->
    SSafe (li.(proj1_sig).(lvfi_sig))
          ((proj1_sig o).(outpre))
          (local_outline_to_prog o)
          ((proj1_sig o).(outpost)).
  Proof.
    intros Hosound.
    destruct li as (li & Hli).
    destruct o as (o & Ho).
    induction o.
    - (* Skip *)
      apply safe_skip.
      apply outline_sound_skip in Hosound.
      apply Hli in Hosound.
      apply Hosound.
    - (* Atomic *)
      apply safe_atomic.
      apply outline_sound_atom in Hosound.
      apply Hli in Hosound.
      apply Hosound.
    - (* Iteration *)
      apply safe_cons with (p' := o.(outpre)) (q' := o.(outpre)).
      + (* Entry: p => precondition of o *)
        apply outline_sound_iter_enter in Hosound.
        apply Hli in Hosound.
        apply Hosound.
      + (* Skip: precondition of o => q *)
        apply outline_sound_iter_skip in Hosound.
        apply Hli in Hosound.
        apply Hosound.
      + (* Now apply the safety rule *)
        apply safe_loop.
        apply safe_cons_q with (q' := o.(outpost)).
        * (* Loop: postcondition of o => precondition of o *)
          apply outline_sound_iter_loop in Hosound.
          apply Hli in Hosound.
          apply Hosound.
        * apply IHo.
          eapply outline_sound_iter, Hosound.
    - (* Frame rule *)
      destruct Ho as (Ho & ff & Hff).
      (* NB: the above enforces that the frame is constant. *)
      apply safe_cons with (p' := (o.(outpre) * const ff)%views)
                           (q' := (o.(outpost) * const ff)%views).
      + (* Entry: p => precondition of o * frame *)
        apply outline_sound_frame_enter in Hosound.
        apply Hli in Hosound.
        unfold proj1_sig.
        rewrite <- Hff.
        apply Hosound.
      + (* Exit: p => precondition of o * frame *)
        apply outline_sound_frame_exit in Hosound.
        apply Hli in Hosound.
        unfold proj1_sig.
        rewrite <- Hff.
        apply Hosound.
      + (* Now apply the safety rule *)
        apply safe_frame.
        apply IHo.
        eapply outline_sound_frame, Hosound.
    - (* Binary operation: case-split on operation. *)
      destruct b; try contradiction.
      + (* Sequential composition *)
        apply safe_cons with (p' := o1.(outpre)) (q' := o2.(outpost)).
        * (* Entry: p => precondition of o1 *)
          apply outline_sound_seq_enter in Hosound.
          apply Hli in Hosound.
          apply Hosound.
        * (* Exit: postcondition of o2 => q *)
          apply outline_sound_seq_exit in Hosound.
          apply Hli in Hosound.
          apply Hosound.
        * destruct Ho as (Ho1 & Ho2).
          apply safe_seq with (q := o1.(outpost)).
          -- (* Left bit *)
            eapply IHo1.
            apply outline_sound_bin in Hosound.
            apply Hosound.
          -- (* Right bit: need to push through a consequence first *)
            apply safe_cons_p with (p' := o2.(outpre)).
            ++ (* Join: postcondition of o1 => precondition of o2 *)
              apply outline_sound_seq_join in Hosound.
              apply Hli in Hosound.
              apply Hosound.
            ++ eapply IHo2.
               apply outline_sound_bin in Hosound.
               apply Hosound.
      + (* Nondeterminism *)
        destruct Ho as (Ho1 & Ho2).
        apply safe_ndt.
        * (* Left possibility. *)
          apply safe_cons with (p' := o1.(outpre)) (q' := o1.(outpost)).
          -- (* Enter-1: p => precondition of o1 *)
              apply outline_sound_ndt_enter1 in Hosound.
              apply Hli in Hosound.
              apply Hosound.
          -- (* Exit-1: postcondition of o1 => q *)
              apply outline_sound_ndt_exit1 in Hosound.
              apply Hli in Hosound.
              apply Hosound.
          -- eapply IHo1.
             apply outline_sound_bin in Hosound.             
             apply Hosound.
        * (* Right possibility. *)
          apply safe_cons with (p' := o2.(outpre)) (q' := o2.(outpost)).
          -- (* Enter-2: p => precondition of o2 *)
              apply outline_sound_ndt_enter2 in Hosound.
              apply Hli in Hosound.
              apply Hosound.
          -- (* Exit-2: postcondition of o2 => q *)
              apply outline_sound_ndt_exit2 in Hosound.
              apply Hli in Hosound.
              apply Hosound.
          -- eapply IHo2.
             apply outline_sound_bin in Hosound.             
             apply Hosound.
  Qed.
End Outline_local.

Infix "|-" := outline_sound_inst (at level 99): outline_scope.

Section Outline_program_logic.
  (** [V] is the Views semigroup, with setoid instance [SV] and
    semigroup instance [VV]. *)

  Variable V: Type.
  Context {SV: Setoid V} {VV: ViewsSemigroup V}.

  (** [C] is the atomic command set. *)

  Variable C: Set.

  (** [S] is the state set. *)

  Variable S: Set.

  (* TODO: make this a formal property of instances. *)

  Local Open Scope views_scope.
  
  Definition has_equiv_axioms (i: PreInstance C S): Prop :=
    forall p q : V, p == q -> Ensembles.In _ i.(v_axioms) <|p|> (Id C) <|q|>.
  
  Local Open Scope views_scope.

  Variable i: PreInstance C S.
  Hypothesis Hequiv: has_equiv_axioms i.
  
  (** We can reduce the proof of a program logic step on a binary
        operation to showing that the side-conditions are valid axioms. *)
  
  Lemma ospl_bin (b: BinOp) (c1 c2: Outline V C) (p q: V):
    i |- c1 ->
         i |- c2 ->
              (forall p' q': V,
                  In (p', q') (bin_side_vcs b p q c1 c2) ->
                  Ensembles.In _ i.(v_axioms) <|p'|> (Id C) <|q'|>) ->
              i |- (OBin p c1 c2 b q).
  Proof.
    intros Hc1 Hc2 Hside_ok [ap ac aq] Ha.
    simpl.
    destruct (in_vcs_dichotomy _  ap aq ac Ha) as [Haa|(-> & Has)].
    - apply (outline_sound_atomic_vcs_bin b p q Hc1 Hc2 _ Haa).
    - destruct (in_side_vcs_bin_trichotomy b p q ap aq c1 c2 Has) as
          [Hside|[Has1|Has2]].
      + apply Hside_ok, Hside.
      + apply outline_sound_side_vcs with (o := c1); assumption.
      + apply outline_sound_side_vcs with (o := c2); assumption.
  Qed.
  
  Theorem ospl_ndt (c1 c2: Outline V C) (p q: V):
    ol_has_conds c1 p q ->
    ol_has_conds c2 p q ->
    i |- c1 ->
         i |- c2 ->
              i |- {{ p }} c1 + c2 {{ q }}.
  Proof.
    intros (Hc1_p & Hc1_q) (Hc2_p & Hc2_q) Hos_c1 Hos_c2.
    apply ospl_bin; try assumption.
    intros p' q' [Hp1|[Hp2|[Hq1|[Hq2|Hnull]]]].
    - injection Hp1.
      intros <- <-.
      apply Hequiv, symmetry, Hc1_p.
    - injection Hp2.
      intros <- <-.
      apply Hequiv, symmetry, Hc2_p.
    - injection Hq1.
      intros <- <-.
      apply Hequiv, Hc1_q.
    - injection Hq2.
      intros <- <-.
      apply Hequiv, Hc2_q.
    - contradiction.
  Qed.

  Theorem ospl_par (c1 c2: Outline V C):
    i |- c1 ->
         i |- c2 ->
              i |- {{ outpre c1 * outpre c2 }} c1 || c2 {{ outpost c1 * outpost c2 }}.
  Proof.
    intros Hos_c1 Hos_c2.
    apply ospl_bin; try assumption.
    intros p' q' [Hp|[Hq|Hnull]].
    - injection Hp.
      intros <- <-.
      apply Hequiv, reflexivity.
    - injection Hq.
      intros <- <-.
      apply Hequiv, reflexivity.
    - contradiction.
  Qed.

  Theorem ospl_seq (c1 c2: Outline V C) (p q r: V):
    ol_has_conds c1 p r ->
    ol_has_conds c2 r q ->
    i |- c1 -> i |- c2 -> i |- {{ p }} c1; c2 {{ q }}.
  Proof.
    intros (Hc1_p & Hc1_r) (Hc2_r & Hc2_q) Hos_c1 Hos_c2.
    apply ospl_bin; try assumption.
    intros p' q' [Hp|[Hr|[Hq|Hnull]]].
    - injection Hp.
      intros <- <-.
      apply Hequiv, symmetry, Hc1_p.
    - injection Hr.
      intros <- <-.
      apply Hequiv.
      transitivity r.
      + exact Hc1_r.
      + symmetry.
        exact Hc2_r.
    - injection Hq.
      intros <- <-.
      apply Hequiv.
      exact Hc2_q.
    - contradiction.
  Qed.      
End Outline_program_logic.

Section Outline_combinators.
  Variables
    (V: Type)
    (C: Set).

  Context
    {VV : Setoid V}
    {SV : ViewsSemigroup V}    
    {MV : ViewsMonoid V}.
  
  (** [seq] sequentially composes a list of outlines. *)

  Fixpoint seq (p: V) (xs: list (Outline V C)) (q: V): Outline V C :=
    match xs with
    | nil         => {{ p }} skip {{ q }}
    | (x::nil)    => {{ p }} frame 1%views in x {{ q }}                                       
    | (x::y::nil) => {{ p }} x ; y {{ q }}
    | (x::xs)     => {{ p }} x ; (seq (outpost x) xs q) {{ q }}
    end.
  
End Outline_combinators.

Local Close Scope outline_scope.
