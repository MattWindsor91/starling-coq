# Starling formalisation in Coq

This is a *highly experimental* and *incomplete* formalisation of
large parts of the
[Starling](https://MattWindsor91.github.io/starling) theory in
[Coq](https://coq.inria.fr/).  It mainly serves to give
machine-checked proofs of many of the results on which Starling
depends, including compatibility with the existing Views Framework
development.

## Contributions

- A hierarchy of typeclasses for views semigroups and monoids,
  including ordered semigroups, subtractive semigroups, and so on.
- Stock instances of those typeclasses for multisets (function-based
  and list-based), sets (constructive), and some transformations of
  semigroups (functions, Cartesian products, indexed products, subtype
  restriction).
- The Local Views Framework, a thin layer on top of the Views Framework to add
  native thread-local state support for programs built as a top-level
  composition of sequential thread subprograms (and based on work by
  [Khyzha et al.](http://software.imdea.org/~artem/papers/fm16-extended.pdf);
- Formulations of Views-based proof rules for concurrency, including
  the rule used in the Starling tool.
- More to come.

## The Starling program logics

The following program logics have been developed (to some degree) on top of the
Starling work:

- _µStarling_: a minimal, shared-state only logic using the Concurrent Views
  Framework.
  Generates a bounded set of verification conditions, but quite restricted
  in terms of expressivity.
- _LoStarling_: a local-state logic using the Local Views Framework.
  Does _not_ generate a bounded set of verification conditions, and thus can't
  be extracted into a program.

The logic used in the Starling tool itself is still under mechanisation.

## A note about branches and development

At time of writing (18 April 2019), the `master` branch is frozen except for
bug fixes.  This branch represents the state of the Starling mechanisation at
the end of my (Matt Windsor's) PhD work.

Active development on this Coq mechanisation is (currently) ongoing in the
`dev` branch, but is presently a side-project.

## Requirements

- Coq 8.9+.
- Dune.
- If making the Views connectivity proof, the [Views Framework Coq development](https://sites.google.com/site/viewsmodel/files/Views.zip).

## Instructions

`starling-coq` uses Dune as its build system.  Assuming that Dune and Coq
are both installed, type `dune build` to build `starling-coq`.

### With Views connectivity proof

- Extract the Views Framework development to `./ViewsCoq`.
- Currently, the Views Framework development needs the following changes to work in 8.7:
  - Change the body of `Language.st_equiv_equiv` to `{}`.

## Organisation

- `Starling/`: the Starling formalisation
  - `Utils/`: assorted lemmas and definitions
    - `Ensemble/`: facts about constructive sets
    - `List/`: facts about lists
    - `Multiset/`: facts about multisets; multiset minus and
      inclusion; list-based multisets
    - `Nat/`: facts about natural numbers
    - `Option/`: facts about option types
    - `Monads.v`: Haskell 98 style monads
  - `Views/`: Starling's version of the Views frameworks, and their associated
    typeclasses and supporting proofs
    - `Instances/`: stock instances for common types (sets, multisets, etc.)
    - `Transformers/`: derived instances for functions of views,
      products of views, etc.
    - `Frameworks/`: the actual Views frameworks (proof rules, semantics,
      safety, and soundness)
      - `Common/`: things common to all Views frameworks (signatures, reifiers,
        etc.)
      - `CVF/`: the Concurrent Views Framework (based on
        [_Views: Compositional Reasoning for Concurrent Programs_](https://dl.acm.org/citation.cfm?id=2429104))
      - `LVF/`: the Local Views Framework (new for Starling, but heavily
        inspired by [_A Generic Logic for Proving Linearizability_](http://software.imdea.org/~artem/papers/fm16-extended.pdf))
  - `Backends/`: formalisation of what Starling expects from backend solvers
  - `Logics/`: logics based on the Starling framework
    - `MicroStarling.v`: the MicroStarling program logic
    - `LoStarling.v`: the LoStarling program logic
  - (Further organisation is underway.)
- `Old/`: bits of mechanisation that have code-rotted and need fixing up to
  target the current state
- `WIP/`: fragments of mechanisation that haven't been finished yet

## Licence

MIT; see `LICENSE`.
