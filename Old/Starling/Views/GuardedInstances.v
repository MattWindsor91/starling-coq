(** * Iterated view instance for [GenView] *)

Require Import views.
Require Import gviews.
Require Import gviews_sem_eq.

Instance GView_GenView : GenView GView sem_eq GUnion GEmp :=
  { eq_equiv   := sem_eq_Equivalence;
    dot_assoc  := gunion_ass;
    dot_comm   := gunion_comm;
    (* TODO: semeq is too strong for these properties
       We need a well-formedness qualifier *)
    unit_left  := gunion_empty_left;
    unit_right := gunion_empty_right }.
