(** * Semantic equivalence for guarded views
    Semantic equivalence is complicated for guarded views: the presence
    of environments and partiality means that there are multiple types of
    semantic equivalence. *)

Require Import Coq.Sets.Multiset.
Require Import ZArith.
Require Import Coq.Classes.RelationClasses.

Require Import views.
Require Import iviews.
Require Import iviews_sem_eq.
Require Import gviews.
Require Import expr.
Require Import option_facts.


(** ** Semantic equivalence under environment
    Two guarded views [i] and [j] are _semantically equivalent under_ an
    environment [v] if, and only if:

    - [i] and [j] are both ill-formed; _or_
    - [i] and [j] are well-formed _and_ [sem i v] and [sem j v] are
      semantically equivalent under the rule for iterated views. *)

Definition sem_eq_under_v (i j : GView) (v : Env) : Prop :=
  match sem i v, sem j v with
    | Some ii, Some ji => iviews_sem_eq.sem_eq ii ji
    | None   , None    => True
    | _      , _       => False
  end.

(** Semantic equivalence under environment is symmetric, reflexive, and
    transitive modulo the environment itself. *)

Theorem sem_eq_under_v_sym (a b : GView) (v : Env) :
  sem_eq_under_v a b v -> sem_eq_under_v b a v.
Proof.
  intro H.
  unfold sem_eq_under_v; unfold sem_eq_under_v in H.
  destruct (sem a v), (sem b v); try assumption.
  symmetry; exact H.
Qed.

Theorem sem_eq_under_v_refl (a : GView) (v : Env):
  sem_eq_under_v a a v.
Proof.
  unfold sem_eq_under_v.
  destruct (sem a v).
  - reflexivity.
  - exact I.
Qed.

Theorem sem_eq_under_v_trans (a b c : GView) (v : Env) :
  sem_eq_under_v a b v -> sem_eq_under_v b c v -> sem_eq_under_v a c v.
Proof.
  unfold sem_eq_under_v in *; intros H1 H2.
  destruct (sem a v), (sem b v), (sem c v); try assumption.
  - transitivity i0; assumption.
  - contradiction H1.
  - exact I.
Qed.


(** ** Semantic equivalence

    Two guarded views [i] and [j] are _semantically equivalent_ (proper)
    if they are semantically equivalent under all environments. *)

Definition sem_eq (i j : GView) : Prop :=
  forall (v : Env), sem_eq_under_v i j v.

(** Semantic equivalence is an equivalence relation.
    We prove this by witnessing the reflexivity, symmetry, and transitivity
    of the semantic function.  Because we have already proven these properties
    for semantic equivalence under environments, this is mostly an exercise in
    lifting what we already have. *)

Theorem sem_eq_refl (a : GView):
  sem_eq a a.
Proof.
  unfold sem_eq; intro v; apply sem_eq_under_v_refl.
Qed.

Instance sem_eq_Reflexive : Reflexive sem_eq :=
  { reflexivity := sem_eq_refl }.

Theorem sem_eq_sym (a b : GView) :
  sem_eq a b -> sem_eq b a.
Proof.
  unfold sem_eq.
  intros H v.
  apply (sem_eq_under_v_sym a b v (H v)).
Qed.

Instance sem_eq_Symmetric : Symmetric sem_eq :=
  { symmetry := sem_eq_sym }.

Theorem sem_eq_trans (a b c : GView) :
  sem_eq a b -> sem_eq b c -> sem_eq a c.
Proof.
  unfold sem_eq.
  intros H1 H2 v.
  apply (sem_eq_under_v_trans a b c v (H1 v) (H2 v)).
Qed.

Instance sem_eq_Transitive : Transitive sem_eq :=
  { transitivity := sem_eq_trans }.

Instance sem_eq_Equivalence : Equivalence sem_eq :=
  { Equivalence_Reflexive  := sem_eq_Reflexive  ;
    Equivalence_Symmetric  := sem_eq_Symmetric  ;
    Equivalence_Transitive := sem_eq_Transitive }.


(** ** Further properties of semantic equivalence
    We now prove some more properties of the semantic equivalence of guarded
    views. *)

Theorem sem_dichotomy (a : GView) (v : Env) :
  sem a v = None \/ exists ii : IView, sem a v = Some ii.
Proof.
  destruct (sem a v).
  - right; exists i; reflexivity.
  - left; reflexivity.
Qed.

Theorem sem_gatom_succeed_guar (n : IExpr) (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (i : IView),
    sem (GAtom (guar, pred, n)) v = Some i
    -> exists b, eval_BExpr_bool v guar = Some b.
Proof.
  intros v i H.
  unfold sem, eval_GAtom, bind in H.
  destruct (eval_BExpr_bool v guar).
  - exists b; reflexivity.
  - discriminate H.
Qed.

Theorem sem_gatom_succeed_nat (n : IExpr) (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (i : IView),
    sem (GAtom (guar, pred, n)) v = Some i
    -> exists k, eval_IExpr_nat v n = Some k.
Proof.
  intros v i H.
  unfold sem, eval_GAtom, bind in H.
  destruct (eval_BExpr_bool v guar),
           (eval_IExpr_nat v n); try discriminate H.
  exists n0; reflexivity.
Qed.

Theorem sem_gatom_succeed_eapred (n : IExpr) (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (i : IView),
    sem (GAtom (guar, pred, n)) v = Some i
    -> exists p, eval_EAPred v pred = Some p.
Proof.
  intros v i H.
  unfold sem, eval_GAtom, bind in H.
  destruct (eval_BExpr_bool v guar),
           (eval_IExpr_nat v n),
           (eval_EAPred v pred); try discriminate H.
  exists a; reflexivity.
Qed.

Theorem sem_gatom_succeed (n : IExpr) (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (i : IView),
    sem (GAtom (guar, pred, n)) v = Some i
    -> exists b k p,
       eval_BExpr_bool v guar = Some b
       /\
       eval_IExpr_nat v n = Some k
       /\
       eval_EAPred v pred = Some p.
Proof.
  intros v i H.
  destruct (sem_gatom_succeed_guar n guar pred v i H) as [b Hgb].
  destruct (sem_gatom_succeed_nat n guar pred v i H) as [k Hnk].
  destruct (sem_gatom_succeed_eapred n guar pred v i H) as [p Hep].
  exists b, k, p.
  repeat split; assumption.
Qed.


Lemma sem_gatom_succeed_back_on (n : IExpr) (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (k : nat) (p : APred Val),
    eval_BExpr_bool v guar = Some true
    -> eval_IExpr_nat v n = Some k
    -> eval_EAPred v pred = Some p
    -> sem (GAtom (guar, pred, n)) v = Some (IAtom (p, k)).
Proof.
  intros v k p Hb Hk Hp.
  unfold sem, eval_GAtom, bind.
  rewrite Hb, Hk, Hp.
  reflexivity.
Qed.

Lemma sem_gatom_succeed_back_off (n : IExpr) (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (k : nat) (p : APred Val),
    eval_BExpr_bool v guar = Some false
    -> eval_IExpr_nat v n = Some k
    -> eval_EAPred v pred = Some p
    -> sem (GAtom (guar, pred, n)) v = Some IEmp.
Proof.
  intros v k p Hb Hk Hp.
  unfold sem, eval_GAtom, bind.
  rewrite Hb, Hk, Hp.
  reflexivity.
Qed.

Theorem sem_gatom_succeed_back (n : IExpr) (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (b : bool) (k : nat) (p : APred Val),
    eval_BExpr_bool v guar = Some b
    -> eval_IExpr_nat v n = Some k
    -> eval_EAPred v pred = Some p
    -> exists i, sem (GAtom (guar, pred, n)) v = Some i.
Proof.
  intros v b k p Hb Hk Hp.
  destruct b.
  - exists (IAtom (p, k)); apply (sem_gatom_succeed_back_on n guar pred v k p Hb Hk Hp).
  - exists IEmp; apply (sem_gatom_succeed_back_off n guar pred v k p Hb Hk Hp).
Qed.


(** [GUnion] and [GMinus] are both based on [map2].
    By specialising the [map2] theorems, we can prove some properties about
    [GUnion] and [GMinus] we can use later. *)

Theorem sem_eq_term_gunion_some (a b : GView) (v : Env) :
  (forall (ad bd : IView),
   sem a v = Some ad
   -> sem b v = Some bd
   -> sem (GUnion a b) v = Some (IUnion ad bd)).
Proof.
  intros ad bd H1 H2.
  unfold sem; fold sem.
  rewrite H1, H2.
  apply map2_some.
Qed.

Theorem sem_eq_term_gminus_some (a b : GView) (v : Env) :
  (forall (ad bd : IView),
   sem a v = Some ad
   -> sem b v = Some bd
   -> sem (GMinus a b) v = Some (IMinus ad bd)).
Proof.
  intros ad bd H1 H2.
  unfold sem; fold sem.
  rewrite H1, H2.
  apply map2_some.
Qed.

Theorem sem_eq_term_gunion_some_back (a b : GView) (v : Env) (abi : IView) :
 sem (GUnion a b) v = Some abi
 -> exists (ad bd : IView), sem a v = Some ad /\ sem b v = Some bd.
Proof.
  apply map2_some_back.
Qed.

Theorem sem_eq_term_gminus_some_back (a b : GView) (v : Env) (abi : IView) :
 sem (GMinus a b) v = Some abi
 -> exists (ad bd : IView), sem a v = Some ad /\ sem b v = Some bd.
Proof.
  apply map2_some_back.
Qed.

Theorem sem_eq_term_gunion_some_both (a b : GView) (c : IView) (v : Env) :
   sem (GUnion a b) v = Some c ->
   exists (ai bi : IView),
     sem a v = Some ai /\
     sem b v = Some bi /\
     c = IUnion ai bi.
Proof.
  apply map2_some_both.
Qed.

Theorem sem_eq_term_gminus_some_both (a b : GView) (c : IView) (v : Env) :
   sem (GMinus a b) v = Some c ->
   exists (ai bi : IView),
     sem a v = Some ai /\
     sem b v = Some bi /\
     c = IMinus ai bi.
Proof.
  apply map2_some_both.
Qed.

Theorem sem_eq_term_gunion_none (a b : GView) (v : Env) :
  sem a v = None \/ sem b v = None
  -> sem (GUnion a b) v = None.
Proof.
  apply map2_none.
Qed.

Theorem sem_eq_term_gminus_none (a b : GView) (v : Env) :
  sem a v = None \/ sem b v = None
  -> sem (GMinus a b) v = None.
Proof.
  apply map2_none.
Qed.

Theorem sem_eq_term_gunion_none_back (a b : GView) (v : Env) :
  sem (GUnion a b) v = None
  -> sem a v = None \/ sem b v = None.
Proof.
  apply map2_none_back.
Qed.

Theorem sem_eq_term_gminus_none_back (a b : GView) (v : Env) :
  sem (GMinus a b) v = None
  -> sem a v = None \/ sem b v = None.
Proof.
  apply map2_none_back.
Qed.


(** ** Congruences
    Here, we prove that guarded view semantic equivalence is congruent with
    respect to union and minus. *)

Theorem sem_eq_from_isem_eq (a b : GView) (ai bi : IView) (v : Env) :
  iviews_sem_eq.sem_eq ai bi
  -> sem a v = Some ai
  -> sem b v = Some bi
  -> sem_eq_under_v a b v.
Proof.
  intros Hi Ha Hb.
  unfold sem_eq_under_v.
  rewrite Ha, Hb.
  exact Hi.
Qed.

Theorem sem_eq_reduce (a b : GView) (v : Env) :
  (forall (ai bi : IView),
       sem a v = Some ai ->
       sem b v = Some bi ->
       iviews_sem_eq.sem_eq ai bi)
  -> (sem a v = None -> sem b v = None)
  -> (sem b v = None -> sem a v = None)
  -> sem_eq_under_v a b v.
Proof.
  intros H1 H2 H3.
  unfold sem_eq, sem_eq_under_v.
  (* We need to know that, if sem a or sem b are not Some, they are None. *)
  specialize sem_dichotomy with (a := a) (v := v); intro HDa.
  specialize sem_dichotomy with (a := b) (v := v); intro HDb.
  (* Now case split on whether sem a or sem b fail. *)
  destruct (sem a v).
  - (* First, sem a v is well-formed.
       HDa contains a contradiction in its first leg, so we need to cleanup. *)
    destruct HDa as [con|HDa]; try discriminate con.
    destruct (sem b v).
    + (* As above, but now for sem b v. *)
      destruct HDb as [con|HDb]; try discriminate con.
      (* Now, we have both a and b well-formed, so we need to use our
         reduction hypothesis. *)
      apply H1; reflexivity.
    + (* Now, we have a case where one is well-formed but the other isn't.
         By H3, this is a contradiction. *)
      absurd (Some i = (None : option IView)).
      * apply some_is_not_none.
        exists i.
        reflexivity.
      * apply H3.
        reflexivity.
 - (* This time, HDa is useless. *)
   destruct (sem b v).
   + (* Now, we have the above case in the other direction. *)
     absurd (Some i = (None : option IView)).
     * apply some_is_not_none.
       exists i.
       reflexivity.
     * apply H2.
       reflexivity.
   + (* Finally, both are ill-formed, and we have no case to answer. *)
     exact I.
Qed.

Theorem sem_eq_reduce_all (a b : GView) :
  (forall (v : Env) (ai bi : IView),
       sem a v = Some ai ->
       sem b v = Some bi ->
       iviews_sem_eq.sem_eq ai bi)
  -> (forall (v : Env), sem a v = None -> sem b v = None)
  -> (forall (v : Env), sem b v = None -> sem a v = None)
  -> sem_eq a b.
Proof.
  intros H1 H2 H3.
  unfold sem_eq; intro v.
  apply (sem_eq_reduce a b v (H1 v) (H2 v) (H3 v)).
Qed.

Lemma weak_sem_eq_congr_isemeq (a b c d : GView) (f : IView -> IView -> IView) (v : Env):
  sem_eq_under_v a b v
  -> sem_eq_under_v c d v
  -> (forall (ai bi ci di : IView), iviews_sem_eq.sem_eq ai bi -> iviews_sem_eq.sem_eq ci di -> iviews_sem_eq.sem_eq (f ai ci) (f bi di))
  -> (forall (ai bi : IView), 
      map2 f (sem a v) (sem c v) = Some ai
      -> map2 f (sem b v) (sem d v) = Some bi
      -> iviews_sem_eq.sem_eq ai bi).
Proof.
  intros Hab Hcd Hcongr ai bi Hac Hbd.
  (* First, realise that a, b, c, and d must be well-formed
     and witness this. *)
  destruct (map2_some_back ai (sem a v) (sem c v) f Hac) as [ad [cd [Ha Hc]]].
  destruct (map2_some_back bi (sem b v) (sem d v) f Hbd) as [bd [dd [Hb Hd]]].
  (* Next, use the above to determine the results of f over a/c and b/d. *) 
  rewrite Ha, Hc, (map2_some ad cd f) in Hac.
  injection Hac; intro HacS.
  rewrite Hb, Hd, (map2_some bd dd f) in Hbd.
  injection Hbd; intro HbdS.
  (* Now we know enough to rewrite the goal as a congruence question. *)
  rewrite <- HacS, <- HbdS.
  apply Hcongr.
  * unfold sem_eq_under_v in Hab; rewrite Ha, Hb in Hab; exact Hab.
  * unfold sem_eq_under_v in Hcd; rewrite Hc, Hd in Hcd; exact Hcd.
Qed.

Lemma weak_sem_eq_congr_liveness (v : Env) (a b c d : GView) (f : IView -> IView -> IView) :
  sem_eq_under_v a b v
  -> sem_eq_under_v c d v
  -> map2 f (sem a v) (sem c v) = None
  -> map2 f (sem b v) (sem d v) = None.
Proof.
  intros Hab Hcd Hacn.
  apply map2_none.
  apply map2_none_back in Hacn.
  unfold sem_eq_under_v in Hab, Hcd.
  destruct (sem a v), (sem b v); try contradiction Hab.
  - (* In this case, A and B are both well-formed, so C must be ill-formed. *)
    destruct (sem c v).
    + destruct (sem d v).
      * destruct Hacn as [Hac1|Hac2].
        -- discriminate Hac1.
        -- discriminate Hac2.
      * contradiction Hcd.
    + destruct (sem d v).
      * contradiction Hcd.
      * right; reflexivity.
  - left; reflexivity.
Qed.

(** We now have enough tools to prove congruence for GUnion and GMinus, given
    the congruence proofs for IUnion and IMinus. *)

Theorem weak_sem_eq_congr_gunion (a b c d : GView) (v : Env) :
  sem_eq_under_v a b v -> sem_eq_under_v c d v -> sem_eq_under_v (GUnion a c) (GUnion b d) v.
Proof.
  intros Hab Hcd.
  apply sem_eq_reduce.
  - apply weak_sem_eq_congr_isemeq with (f := IUnion); try assumption.
    apply iviews_sem_eq.sem_eq_congr_iunion.
  - intro H; apply (weak_sem_eq_congr_liveness v a b c d IUnion Hab Hcd H).
  - pose (Hba := sem_eq_under_v_sym a b v Hab).
    pose (Hdc := sem_eq_under_v_sym c d v Hcd).
    intro H; apply (weak_sem_eq_congr_liveness v b a d c IUnion Hba Hdc H).
Qed.

Theorem sem_eq_congr_gunion (a b c d : GView) :
  sem_eq a b -> sem_eq c d -> sem_eq (GUnion a c) (GUnion b d).
Proof.
  intros Hab Hcd.
  unfold sem_eq in *; intro v.
  apply (weak_sem_eq_congr_gunion a b c d v (Hab v) (Hcd v)).
Qed.

Theorem weak_sem_eq_congr_gminus (a b c d : GView) (v : Env) :
  sem_eq_under_v a b v -> sem_eq_under_v c d v -> sem_eq_under_v (GMinus a c) (GMinus b d) v.
Proof.
  intros Hab Hcd.
  apply sem_eq_reduce.
  - apply weak_sem_eq_congr_isemeq with (f := IMinus); try assumption.
    apply iviews_sem_eq.sem_eq_congr_iminus.
  - intro H; apply (weak_sem_eq_congr_liveness v a b c d IMinus Hab Hcd H).
  - pose (Hba := sem_eq_under_v_sym a b v Hab).
    pose (Hdc := sem_eq_under_v_sym c d v Hcd).
    intro H; apply (weak_sem_eq_congr_liveness v b a d c IMinus Hba Hdc H).
Qed.

Theorem sem_eq_congr_gminus (a b c d : GView) :
  sem_eq a b -> sem_eq c d -> sem_eq (GMinus a c) (GMinus b d).
Proof.
  intros Hab Hcd.
  unfold sem_eq in *; intro v.
  apply (weak_sem_eq_congr_gminus a b c d v (Hab v) (Hcd v)).
Qed.


(** ** GAtom equivalences
    We now prove the same facts about GAtom that we did for IAtom. *)

(** The union of two atoms with the same predicate is equivalent to one atom
    iterated the addition of the two atoms' iterations, if both atoms'
    iterators are well-formed *)

Theorem gatom_gunion_same (a : APred Expr) (b : BExpr) (n m o : IExpr) (v : Env) (nn mn : nat) :
  eval_IExpr_nat v n = Some nn ->
  eval_IExpr_nat v m = Some mn ->
  eval_IExpr_nat v o = Some (nn + mn) ->
  sem_eq_under_v
    (GUnion (GAtom (b, a, n)) (GAtom (b, a, m)))
    (GAtom (b, a, o))
    v.
Proof.
  intros Hnsucc Hmsucc Hosucc.
  unfold sem_eq_under_v, sem, eval_GAtom, map2.
  rewrite Hnsucc, Hmsucc, Hosucc.
  (* In this case we have to split on whether the guard is well-formed, and
     whether it holds if it is; we also need to split on whether the predicate
     is wf.  Whenever one is ill-formed, we reach a tautology. *)
  destruct (eval_BExpr_bool v b), (eval_EAPred v a); try exact I.
  - (* Split on whether the guard is actually holding. *)
    destruct b0.
    + apply iviews_sem_eq.iunion_iatom_same.
    + apply iviews_sem_eq.iunion_empty_left.
Qed.

(** The minus of two atoms with the same predicate is equivalent to one atom
    iterated the subtraction of the two atoms' iterations, if both atoms'
    iterators are well-formed. *)

Theorem gatom_gminus_same (a : APred Expr) (b : BExpr) (n m o : IExpr) (v : Env) (nn mn : nat) :
  eval_IExpr_nat v n = Some nn ->
  eval_IExpr_nat v m = Some mn ->
  eval_IExpr_nat v o = Some (nn - mn) ->
  sem_eq_under_v
    (GMinus (GAtom (b, a, n)) (GAtom (b, a, m)))
    (GAtom (b, a, o))
    v.
Proof.
  (* This is more or less the same as the gunion proof, so perhaps we should
     abstract over the two. *)
  intros Hnsucc Hmsucc Hosucc.
  unfold sem_eq_under_v, sem, eval_GAtom, map2.
  rewrite Hnsucc, Hmsucc, Hosucc.
  destruct (eval_BExpr_bool v b), (eval_EAPred v a); try exact I.
  - destruct b0.
    + apply iviews_sem_eq.iminus_iatom_same.
    + apply iviews_sem_eq.iminus_empty_right.
Qed.

Definition gatom_wf
  (ep : APred Expr) (p : APred Val)
  (eb : BExpr) (b : bool)
  (en : IExpr) (n : nat)
  (v : Env) :=
     eval_EAPred     v ep = Some p
  /\ eval_BExpr_bool v eb = Some b
  /\ eval_IExpr_nat  v en = Some n.

(** In addition, the minus of two well-formed predicates is equivalent to the minuend. *)

Theorem gatom_gminus_diff
  (ep1 ep2 : APred Expr) (p1 p2 : APred Val)
  (eb1 eb2 : BExpr) (b1 b2 : bool)
  (en1 en2 : IExpr) (n1 n2 : nat)
  (v : Env) :
  gatom_wf ep1 p1 eb1 b1 en1 n1 v ->
  gatom_wf ep2 p2 eb2 b2 en2 n2 v ->
  p1 <> p2 ->
  sem_eq_under_v
    (GMinus (GAtom (eb1, ep1, en1)) (GAtom (eb2, ep2, en2)))
    (GAtom (eb1, ep1, en1))
    v.
Proof.
  intros [Hp1S [Hb1S Hn1S]] [Hp2S [Hb2S Hn2S]] Hdisj.
  unfold sem_eq_under_v, sem, eval_GAtom.
  rewrite Hp1S, Hp2S, Hb1S, Hb2S, Hn1S, Hn2S.
  destruct b1, b2; simpl.
  - apply (iminus_iatom_diff p1 p2 n1 n2 Hdisj).
  - apply iminus_empty_right.
  - apply iminus_empty_left.
  - apply iminus_empty_left.
Qed.

(** Subtracting two well-formed atoms on the same predicate whose guards
    are both true (or both false) and where the minuend iterator is greater
    than the subtrahend operator is equivalent to emp. *)

Theorem gatom_gminus_gemp
  (ep1 ep2 : APred Expr) (p : APred Val)
  (eb1 eb2 : BExpr) (b : bool)
  (en1 en2 : IExpr) (n1 n2 : nat)
  (v : Env) :
  gatom_wf ep1 p eb1 b en1 n1 v ->
  gatom_wf ep2 p eb2 b en2 n2 v ->
  n1 <= n2 ->
  sem_eq_under_v
    (GMinus (GAtom (eb1, ep1, en1)) (GAtom (eb2, ep2, en2)))
    GEmp
    v.
Proof.
  intros [Hp1S [Hb1S Hn1S]] [Hp2S [Hb2S Hn2S]] Hless.
  unfold sem_eq_under_v, sem, eval_GAtom, map2.
  rewrite Hp1S, Hp2S, Hb1S, Hb2S, Hn1S, Hn2S.
  simpl.
  destruct b.
  - apply (iminus_iatom_empty p n2 n1 Hless).
  - apply iminus_empty_left.
Qed.

(** Lemma for writing proofs over [GUnion]. *)

Lemma sem_eq_reduce_all_gunion (a b c d : GView) :
  (forall (v : Env) (ai bi ci di : IView),
       sem a v = Some ai ->
       sem b v = Some bi ->
       sem c v = Some ci ->
       sem d v = Some di ->
       iviews_sem_eq.sem_eq (IUnion ai bi) (IUnion ci di))
  -> (forall (v : Env), sem a v = None -> sem c v = None \/ sem d v = None)
  -> (forall (v : Env), sem b v = None -> sem c v = None \/ sem d v = None)
  -> (forall (v : Env), sem c v = None -> sem a v = None \/ sem b v = None)
  -> (forall (v : Env), sem d v = None -> sem a v = None \/ sem b v = None)
  -> sem_eq (GUnion a b) (GUnion c d).
Proof.
  intros Hise Han Hbn Hcn Hdn.
  apply sem_eq_reduce_all.
  - intros v abi cdi Habi Hcdi.
    pose (sem_eq_term_gunion_some_both a b abi v Habi) as Habu.
    destruct Habu as [ai [bi [Hai [Hbi Habe]]]].
    pose (sem_eq_term_gunion_some_both c d cdi v Hcdi) as Hcdu.
    destruct Hcdu as [ci [di [Hci [Hdi Hcde]]]].
    rewrite Habe, Hcde.
    apply (Hise v ai bi ci di Hai Hbi Hci Hdi).
  - intros v Hab.
    apply sem_eq_term_gunion_none.
    pose (sem_eq_term_gunion_none_back a b v Hab) as Habx.
    destruct Habx as [Ha|Hb].
    + apply (Han v Ha).
    + apply (Hbn v Hb).
  - intros v Hcd.
    apply sem_eq_term_gunion_none.
    pose (sem_eq_term_gunion_none_back c d v Hcd) as Hcdx.
    destruct Hcdx as [Hc|Hd].
    + apply (Hcn v Hc).
    + apply (Hdn v Hd).
Qed.

(** [GUnion] is associative under semantic equivalence. *)

Theorem gunion_ass (x y z : GView) :
  sem_eq (GUnion (GUnion x y) z) (GUnion x (GUnion y z)).
Proof.
  apply sem_eq_reduce_all_gunion; intro v.
  - intros xyi zi xi yzi Huxy Hzi Hxi Huyz.
    pose (sem_eq_term_gunion_some_both x y xyi v Huxy) as Hxy.
    destruct Hxy as [xi' [yi [Hxi' [Hyi Hxy]]]].
    pose (sem_eq_term_gunion_some_both y z yzi v Huyz) as Hyz.
    destruct Hyz as [yi' [zi' [Hyi' [Hzi' Hyz]]]].
    (* Now eliminate all the primes. *)
    rewrite Hxi in Hxi'.
    injection Hxi'; intro Hxe.
    rewrite Hyi in Hyi'.
    injection Hyi'; intro Hye.
    rewrite Hzi in Hzi'.
    injection Hzi'; intro Hze.
    rewrite <- Hxe in Hxy.
    rewrite <- Hye, <- Hze in Hyz.
    rewrite Hxy, Hyz.
    apply iunion_ass.
  - intros Hxy.
    pose (sem_eq_term_gunion_none_back x y v Hxy) as Hxyn.
    destruct Hxyn as [Hx|Hy].
    + left; assumption.
    + right.
      apply sem_eq_term_gunion_none.
      left; assumption.
  - right.
    apply sem_eq_term_gunion_none.
    right; assumption.
  - left.
    apply sem_eq_term_gunion_none.
    left; assumption.
  - intros Hyz.
    pose (sem_eq_term_gunion_none_back y z v Hyz) as Hyzn.
    destruct Hyzn as [Hy|Hz].
    + left.
      apply sem_eq_term_gunion_none.
      right; assumption.
    + right; assumption.
Qed.

(** [GUnion] is commutative under semantic equivalence. *)

Theorem gunion_comm (x y : GView) :
  sem_eq (GUnion x y) (GUnion y x).
Proof.
  apply sem_eq_reduce_all_gunion; intro v.
  - intros xi yi yi' xi' Hxi Hyi Hyi' Hxi'.
    (* Now unify both sides' versions of xi and yi. *)
    rewrite Hxi in Hxi'; rewrite Hyi in Hyi'.
    injection Hxi'; intro Hxe.
    injection Hyi'; intro Hye.
    rewrite <- Hxe, <- Hye.
    apply iunion_comm.
  - right; assumption.
  - left; assumption.
  - right; assumption.
  - left; assumption.
Qed.

(** [GUnion] has [GEmp] as a left unit. *)

Theorem gunion_empty_left (x : GView) :
  sem_eq (GUnion GEmp x) x.
Proof.
  apply sem_eq_reduce_all; intro v.
  - intros exi xi Huex Hux.
    pose (sem_eq_term_gunion_some_both GEmp x exi v Huex) as Hex.
    destruct Hex as [ei [xi' [He [Hx Hexi]]]].
    (* First substitute in the semantics of the union. *)
    rewrite Hux in Hx.
    injection Hx; intro Hxe.
    rewrite <- Hxe in Hexi.
    rewrite Hexi.
    (* Now evaluate and substitute [sem GEmp v]. *)
    unfold sem in He.
    injection He; intro Hee.
    rewrite <- Hee.
    (* Now we can use the iterated views fact. *)
    apply iunion_empty_left.
  - intro Hex.
    pose (sem_eq_term_gunion_none_back GEmp x v Hex) as Hexn.
    destruct Hexn as [He|Hx].
    * (* GEmp is always well-formed, so this is false. *)
      discriminate He. 
    * exact Hx.
  - intro Hx.
    apply sem_eq_term_gunion_none.
    right; exact Hx.
Qed.

(** [GUnion] has [GEmp] as a right unit. *)

Theorem gunion_empty_right (x : GView) :
  sem_eq x (GUnion x GEmp).
Proof.
  symmetry.
  transitivity (GUnion GEmp x).
  - apply gunion_comm.
  - apply gunion_empty_left.
Qed.

(** [GUnion] is cancellative if the frame is well-formed. *)

Theorem gunion_cancel_left (a b c : GView) (ao : IView) (v : Env) :
  sem a v = Some ao ->
  sem_eq_under_v (GUnion a b) (GUnion a c) v -> sem_eq_under_v b c v.
Proof.
  intros Hao.
  unfold sem_eq_under_v.
  intro Habc.
  case_eq (sem b v).
  - intros bo Hbo.
    case_eq (sem c v).
    + intros co Hco.
      pose (sem_eq_term_gunion_some a b v ao bo Hao Hbo) as Hsab.
      rewrite Hsab in Habc.
      pose (sem_eq_term_gunion_some a c v ao co Hao Hco) as Hsac.
      rewrite Hsac in Habc.
      apply (iunion_cancel_left ao bo co Habc).
    + intro Hcn.
      pose (sem_eq_term_gunion_none a c v (or_intror (sem a v = None) Hcn)) as Hacn.
      rewrite Hacn in Habc.
      pose (sem_eq_term_gunion_some a b v ao bo Hao Hbo) as Hsab.
      rewrite Hsab in Habc.
      exact Habc.
  - intro Hbn.
    pose (sem_eq_term_gunion_none a b v (or_intror (sem a v = None) Hbn)) as Habn.
    rewrite Habn in Habc.
    case_eq (sem c v).
    + intros co Hco. 
      pose (sem_eq_term_gunion_some a c v ao co Hao Hco) as Hsac.
      rewrite Hsac in Habc.
      exact Habc.
    + intro Hcn; exact I.
Qed.

Theorem gunion_cancel_right (a b c : GView) (ao : IView) (v : Env) :
  sem a v = Some ao ->
  sem_eq_under_v (GUnion b a) (GUnion c a) v -> sem_eq_under_v b c v.
Proof.
  intros Hao Habc.
  apply (gunion_cancel_left a b c ao v Hao).
  pose (gunion_comm a b) as Hcommab.
  pose (gunion_comm c a) as Hcommac.
  unfold sem_eq in *.
  specialize Hcommab with v.
  specialize Hcommac with v.
  apply (sem_eq_under_v_trans
           (GUnion a b) (GUnion b a) (GUnion a c) v
           Hcommab).
  apply (sem_eq_under_v_trans
           (GUnion b a) (GUnion c a) (GUnion a c) v
           Habc
           Hcommac).
Qed.


(** Iteration of guarded views is equivalent to guarding of iterated views. *)

Fixpoint iter_g (n : nat) (guar : BExpr) (pred : APred Expr) :=
  match n with
  | O   => GEmp
  | S k => GUnion (GAtom (guar, pred, IZ 1)) (iter_g k guar pred)
  end.

Lemma iter_g_succeed_back (n : nat) (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (b : bool) (p : APred Val),
    (eval_BExpr_bool v guar = Some b
     /\
     eval_EAPred v pred = Some p) ->
    exists i, sem (iter_g n guar pred) v = Some i.
Proof.
  intros v b p [HA HB].
  induction n.
  - unfold iter_g; exists IEmp; reflexivity.
  - destruct IHn as [i Hn].
    unfold iter_g; fold iter_g.    
    (* Case split on whether the guard is active. *)
    destruct b.
    (* Suppose it is. *)
    + exists (IUnion (IAtom (p, 1)) i).
      rewrite sem_eq_term_gunion_some with (ad := (IAtom (p, 1))) (bd := i).
      * reflexivity.
      * unfold sem, eval_GAtom, bind.
        rewrite HA, HB.
        reflexivity. 
      * assumption.
    (* Suppose it isn't. *)
    + exists (IUnion IEmp i).
      rewrite sem_eq_term_gunion_some with (ad := IEmp) (bd := i).
      * reflexivity.
      * unfold sem, eval_GAtom, bind.
        rewrite HA, HB.
        reflexivity.
      * assumption.
Qed.

Lemma sem_gatom_zero (guar : BExpr) (pred : APred Expr) :
  forall (v : Env) (b : bool) (p : APred Val),
    eval_BExpr_bool v guar = Some b
    -> eval_EAPred v pred = Some p
    -> sem_eq_under_v GEmp (GAtom (guar, pred, IZ (Z.of_nat 0))) v.
Proof.
  intros v b p HbSucc HpSucc.
  unfold sem_eq_under_v, sem, eval_GAtom.
  rewrite HbSucc, HpSucc; simpl.
  destruct b.
  - apply iemp_zero_iatom.
  - reflexivity.
Qed. 

Lemma weak_sem_eq_from_sem_eq (a b : GView) :
  sem_eq a b -> forall v, sem_eq_under_v a b v.
Proof.
  intro Hse.
  unfold sem_eq in Hse.
  exact Hse.
Qed.

Lemma eval_IExpr_nat_Z_id (n : nat) (v : Env) :
  eval_IExpr_nat v (IZ (Z.of_nat n)) = Some n.
Proof.
  unfold eval_IExpr_nat, eval_IExpr_Z.
  rewrite Nat2Z.id.
  destruct n; reflexivity.
Qed.

Theorem iter_g_equiv (guar : BExpr) (pred : APred Expr) (n : nat) :
  forall (v : Env) (b : bool) (p : APred Val),
    eval_BExpr_bool v guar = Some b
    -> eval_EAPred v pred = Some p
    -> sem_eq_under_v (iter_g n guar pred) (GAtom (guar, pred, IZ (Z.of_nat n))) v.
Proof.
  intros v b p.
  induction n.
  - apply sem_gatom_zero.
  - intros HbSucc HpSucc.
    unfold iter_g; fold iter_g.
    (* Try to reduce this to a congruence problem and a standalone, simpler problem. *)
    apply
      (sem_eq_under_v_trans
        (GUnion (GAtom (guar, pred, IZ 1)) (iter_g n guar pred))
        (GUnion (GAtom (guar, pred, IZ 1)) (GAtom (guar, pred, IZ (Z.of_nat n))))
        (GAtom (guar, pred, IZ (Z.of_nat (S n))))
        v).
    + apply weak_sem_eq_congr_gunion.
      (* The congruence problem is quite easily solved with what we know. *)
      * apply sem_eq_under_v_refl.
      * apply (IHn HbSucc HpSucc). 
    + apply gatom_gunion_same with (nn := 1) (mn := n); try apply eval_IExpr_nat_Z_id.
      reflexivity.
Qed.
