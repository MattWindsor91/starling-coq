(** * The free proof rule *)

From Coq Require Import
     Relations.Relation_Definitions
     Classes.SetoidClass (* This comes after Relations because of [equiv] *)
     Sets.Constructive_sets
     Sets.Ensembles.

Require Import Starling.Expr.

From Starling Require Import
     Definer
     Reifier
     ProgramProof
     Views.Classes
     Views.Frameworks.CVF.Core
     Views.Morphisms.

Chapter ProofRules_Free.

Variable V: Type.

Context
  {SV: Setoid V}
  {VV: ViewsSemigroup V}.

Variables S C: Set.

Variables
  (reify: V -> Ensemble S)
  (sem: C -> S -> Ensemble S).

Hypothesis Hreifier_eqv: reifier_eqv reify.
Hypothesis Hreify_proper: Proper (equiv ==> Same_set S) reify.

Section ProofRules_Free_Instance.
  (** The free axiomatisation [free_axiomatisation] is just the set of
      axioms that meet the semigroup action judgement. *)
  
  Definition free_axiomatisation: Ensemble (ViewsAxiom V C) :=
    s_axiom_judgement V C S sem reify.

  Instance ViewsInstance_Free: ViewsInstance V C S :=
    {
      v_reify := reify;
      v_semantics := sem;
      v_axioms := free_axiomatisation;
      v_reify_proper := Hreify_proper
    }.

  (** The free instance is trivially sound. *)
  
  Instance SoundViewsInstance_Free: @SoundViewsInstance V C S SV VV ViewsInstance_Free := {}.
  Proof.
    intros (a & Ha).
    apply Ha.
  Qed.

  (* TODO: need to overhaul views instances first.
  Theorem free_maximal (I: ViewsInstance V C S) (SI: @SoundViewsInstance V C S SV VV I) (a: ViewsAxiom V C):
    In _ (@v_axioms V C S SV I) a -> In _ (@v_axioms V C S SV ViewsInstance_Free) a.
  Proof.
    intro Ha.
    simpl in *.
    unfold free_axiomatisation.
    pose (a' := exist (In _ (@v_axioms V C S SV I)) a Ha).
    pose (s_axiom_sound V C S a').
    simpl in s.
*)
End ProofRules_Free_Instance.

End ProofRules_Free.
