(** * Classes and types for local backend interfaces *)

From Coq Require Import
     Classes.Morphisms
     Lists.List
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     Backend.Classes.

Set Implicit Arguments.

Section starling_logic_backend_local_bvcond.

  (** A [LoBVCond] is a locally parameterised backend verification
        condition.  We parametrise each expression in a [LoBVCond] with
        one or more local contexts. *)
  
  Record LoBVCond (V Ep Er LoP LoQ: Type) (LoG: V -> Type) :=
    mk_LoBVCond
      {
        lbv_g_view: V;
        lbv_w: LoP -> LoQ -> LoG lbv_g_view -> Ep;
        lbv_c: LoP -> LoQ -> Er;
        lbv_g: LoG lbv_g_view -> Ep
      }.

  Variables
    (Ep: Type)
    (Er: Type)
    (GCtx: Type)
    (V: Type)
    (S: Set).
  
  Context
    {P_Ep : PredEx Ep GCtx unit S}
    {P_Er : RelEx  Er GCtx unit S}
    
    {LoP: Type}      (* Local pre-state *)
    {LoQ: Type}      (* Local post-state *)
    {LoG: V -> Type}. (* Local goal-state *)

  (** [lbvhoare] is the local Hoare judgement on a [LoBVCond]. *)
  
  Definition lbvhoare (a: GCtx) (x: LoBVCond Ep Er LoP LoQ LoG): Prop :=
    forall (loP: LoP) (loQ: LoQ) (loG: LoG x.(lbv_g_view)),
      bvhoare a
              {|
                bv_w := x.(lbv_w) loP loQ loG;
                bv_c := x.(lbv_c) loP loQ;
                bv_g := x.(lbv_g) loG
              |}.
  
  (** [ideal_solve_local] is the idealised locally aware solver
        predicate over sets. *)
  
  Definition ideal_solve_local (xs: Ensemble (LoBVCond Ep Er LoP LoQ LoG)) :=
    exists (a: GCtx), forall (x: LoBVCond Ep Er LoP LoQ LoG),
        Ensembles.In _ xs x -> lbvhoare a x.

  (** [ideal_solve_local_list] is the idealised locally aware solver
        predicate over lists. *)
  
  Definition ideal_solve_local_list (xs: list (LoBVCond Ep Er LoP LoQ LoG)): Prop :=
    exists (a: GCtx), Forall (lbvhoare a) xs.
  

Section starling_logic_backend_local_interface.

  (** Class [LoBackend] is the class of all backend interfaces that can handle 
      local contexts. *)

  Class LoBackend (V Ep Er LoP LoQ GCtx : Type) (LoG: V -> Type) (S: Set) :=
    mk_LoBackend
      {
        lbi_predex :> PredEx Ep GCtx unit S;
        lbi_relex :> RelEx Er GCtx unit S;
        lbi_solve: Ensemble (LoBVCond Ep Er LoP LoQ LoG) -> Prop;

        lbi_solve_same xs ys: Same_set _ xs ys -> lbi_solve xs -> lbi_solve ys;
        lbi_solve_ideal xs: lbi_solve xs -> ideal_solve_local xs
      }.

  (** We can use any [Backend] as a [LoBackend], just by erasing local
      variables. *)

  Global Instance Backend_LoBackend (V Ep Er LoP LoQ Aux: Type) (LoG: V -> Type) (S: Set)
         {B: Backend Ep Er Aux S}:
    LoBackend Ep Er LoP LoQ Aux LoG S :=
    {
      lbi_solve xs := bi_solve (local_erased xs)
    }.
  Proof.
    - intros xs ys Hsame.
      apply bi_solve_same, local_erased_Proper, Hsame.
    - intros xs Hbi_solve.
      apply ideal_solve_local_erase, bi_solve_ideal, Hbi_solve.
  Defined.

  (** Class [LoCompBackend] is the class of all compositional local backends. *)

  Class LoCompBackend (V Ep Er LoP LoQ Aux: Type) (LoG: V -> Type) (S: Set)
        {LB: LoBackend Ep Er LoP LoQ Aux LoG S} :=
    mk_LoCompBackend
      {
        lcbi_solve_empty: lbi_solve (Empty_set _);
        lcbi_solve_union xs ys: lbi_solve xs -> lbi_solve ys -> lbi_solve (Union _ xs ys);
        lcbi_solve_noinu xs ys: lbi_solve (Union _ xs ys) -> lbi_solve xs /\ lbi_solve ys;        
      }.

  (** We can use any [CompBackend] as a [LoCompBackend], just by
      erasing local variables. *)

  Global Instance CompBackend_LoCompBackend (V Ep Er LoP LoQ Aux: Type) (LoG: V -> Type) (S: Set)
         {B: Backend Ep Er Aux S}
         {CB: CompBackend}:
    LoCompBackend (V := V) (LoP := LoP) (LoQ := LoQ) (LoG := LoG) := {}.
  Proof.
    - simpl.
      eapply bi_solve_same.
      + symmetry.
        apply local_erased_Empty.
      + apply cbi_solve_empty.
    - intros xs ys Hxs Hys.
      eapply bi_solve_same.
      + symmetry.
        apply local_erased_Union.
      + apply cbi_solve_union; assumption.
    - intros xs ys Hxys.
      eapply bi_solve_same in Hxys.
      + apply cbi_solve_noinu, Hxys.
      + apply local_erased_Union.
  Defined.  
End starling_logic_backend_local_interface.
