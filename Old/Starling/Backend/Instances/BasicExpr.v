From Coq Require Import
     Classes.SetoidClass
     Classes.SetoidDec     
     ZArith
     Program.Basics
     Sets.Constructive_sets
     Sets.Ensembles.

From Starling Require Import
     Utils.Monads
     Utils.Option.Facts
     Backend.Alpha
     Backend.Classes.

Set Implicit Arguments.

(** ** Starling values

    Starling values are either integers or Booleans.
    At time of writing, the Starling tool also supports arrays and other
    compound types, but we do not model them here (besides, we can probably
    leave them to the expression language). *)

Inductive Val :=
  | VZ (Z: Z)
  | VBool (b: bool)
  | VBot.

(* We have to define value equality manually, rather than through Scheme
   Equality, so that we can reuse the existing results for Z/Bool.

   Any ideas on how to avoid this are welcome. *)

Lemma Val_eq_dec (x y : Val) :
  { x = y } + { x <> y }.
Proof.
  decide equality.
  - apply Z.eq_dec.
  - apply Bool.bool_dec.
Qed.

Definition Val_beq (x y : Val) : bool :=
  if (Val_eq_dec x y) then true else false.

(** [val_beq] entails, and is entailed by, equality. *)

Lemma Val_beq_eq (x y : Val) :
  Val_beq x y = true -> x = y.
Proof.
  (* Appeal to similar facts about the integers and Booleans. *)
  intro H.
  unfold Val_beq in H.
  now destruct (Val_eq_dec x y) as [Hxy|].
Qed.

Lemma Val_eq_beq (x y : Val) :
  x = y -> Val_beq x y = true.
Proof.
  (* As above. *)
  intros [].
  unfold Val_beq.
  now destruct (Val_eq_dec x x) as [Hxy|].
Qed.

(** ** Concrete expressions

    Though Starling is agnostic as to what the expressions of its underlying
    theory actually are, we provide an encoding of a subset of the expression
    language understood by the tool, as well as a semantics for that language,
    for purposes of demonstration. *)

Inductive BOp :=
| BBAnd
| BBOr
| BBImpl
| BBEq
| BBGt
| BBGe
| BBLe
| BBLt
| BZAdd
| BZSub
| BZMul
| BZDiv.
  
Inductive Expr (Var: Type) :=
| EVar (v: Var)
| EVal (v: Val)
| EBop (o: BOp) (x y: Expr Var)
| ENot (x: Expr Var).

Fixpoint ebind (Var Var' : Type) (f: Var -> Expr Var') (e: Expr Var): Expr Var' :=
  match e with
  | EVar v => f v
  | EVal _ v => EVal _ v
  | EBop o x y => EBop o (ebind f x) (ebind f y)
  | ENot x => ENot (ebind f x)
  end.

Lemma ebind_bop (Var Var' : Type) (f: Var -> Expr Var') (o: BOp) (x y: Expr Var) :
  ebind f (EBop o x y) = EBop o (ebind f x) (ebind f y).
Proof.
  reflexivity.
Qed.

Lemma ebind_not (Var Var' : Type) (f: Var -> Expr Var') (x: Expr Var) :
  ebind f (ENot x) = ENot (ebind f x).
Proof.
  reflexivity.
Qed.

Local Open Scope program_scope.

Definition efmap (Var Var' : Type) (f: Var -> Var') : Expr Var -> Expr Var' :=
  ebind (@EVar _ ∘ f).

Lemma efmap_id (Var : Type) (x: Expr Var):
  efmap id x = ebind (@EVar _) x.
Proof.
  reflexivity.
Qed.

Lemma efmap_bop (Var Var' : Type) (f: Var -> Var') (o: BOp) (x y: Expr Var):
  efmap f (EBop o x y) = EBop o (efmap f x) (efmap f y).
Proof.
  reflexivity.
Qed.

Lemma efmap_not (Var Var' : Type) (f : Var -> Var') (x : Expr Var):
  efmap f (ENot x) = ENot (efmap f x).
Proof.
  reflexivity.
Qed.

Definition eval_Val_Z (v: Val): option Z :=
  match v with
  | VZ z => Some z
  | _ => None
  end.

Definition eval_Val_B (v: Val): option bool :=
  match v with
  | VBool b => Some b
  | _ => None
  end.


Local Open Scope Z_scope.

Definition opB (op: bool -> bool -> bool) (v1 v2: Val): option Val :=
  map2 (fun b1 b2 => VBool (op b1 b2)) (eval_Val_B v1) (eval_Val_B v2).

Definition opZ (op: Z -> Z -> Z) (v1 v2: Val): option Val :=
  map2 (fun z1 z2 => VZ (op z1 z2)) (eval_Val_Z v1) (eval_Val_Z v2).

Definition opR (op: Z -> Z -> bool) (v1 v2: Val): option Val :=
  map2 (fun z1 z2 => VBool (op z1 z2)) (eval_Val_Z v1) (eval_Val_Z v2).

Definition opEq (v1 v2: Val): option Val :=
  match v1, v2 with
  | VZ z1, VZ z2 => Some (VBool (Z.eqb z1 z2))
  | VBool b1, VBool b2 => Some (VBool (Bool.eqb b1 b2))
  | _, _ => None
  end.

Definition bop (b: BOp): Val -> Val -> option Val :=
  match b with
  | BZAdd  => opZ Z.add
  | BZSub  => opZ Z.sub
  | BZMul  => opZ Z.mul
  | BZDiv  => opZ Z.div
  | BBAnd  => opB andb
  | BBOr   => opB orb
  | BBImpl => opB implb
  | BBEq   => opEq
  | BBGt   => opR Z.gtb
  | BBGe   => opR Z.geb
  | BBLe   => opR Z.leb
  | BBLt   => opR Z.ltb
  end.

Definition opNot (v: Val): option Val :=
  option_map (compose VBool negb) (eval_Val_B v).

Fixpoint eval_Expr (Var: Type) (e: Expr Var) (s : Var -> Val) : Val :=
  match e with
  | EVar v => s v
  | EVal _ v => v
  | EBop b x y => maybe VBot id (bop b (eval_Expr x s) (eval_Expr y s))
  | ENot x => maybe VBot id (opNot (eval_Expr x s))
  end.

Global Instance Val_Setoid : Setoid Val := eq_setoid Val.

Lemma eval_Expr_Proper (Var : Type) (e : Expr Var) :
  Proper (equiv ==> equiv) (eval_Expr e).
Proof.
  induction e; intros s1 s2 Hs; cbn; try easy.
  - apply Hs.
  - specialize (IHe1 s1 s2 Hs).
    specialize (IHe2 s1 s2 Hs).
    congruence.
  - specialize (IHe s1 s2 Hs).
    congruence.
Qed.

Definition eval_Expr_Pred (Var : Type) (e : Expr Var) (s : Var -> Val): Prop :=
  eval_Expr e s = VBool true.

Lemma bop_band_split (v1 v2 : Val):
  bop BBAnd v1 v2 = Some (VBool true) <->
  v1 = VBool true /\ v2 = VBool true.
Proof.
  split.
  - intros (b1 & b2 & Hv1 & Hv2 & Hand)%map2_some_both.
    symmetry in Hand.
    injection Hand.
    intros (Hb1 & Hb2)%Bool.andb_true_iff.
    rewrite Hb1 in Hv1.
    rewrite Hb2 in Hv2.
    unfold eval_Val_B in *.
    destruct v1, v2; try discriminate.
    split; f_equal.
    + injection Hv1; intro; assumption.
    + injection Hv2; intro; assumption.      
  - intros (-> & ->).
    eapply map2_some_gen.
    + reflexivity.
    + reflexivity.
    + f_equal.
Qed.

Corollary eval_bop_band_split (Var : Type) (e1 e2 : Expr Var) (s : Var -> Val):
  eval_Expr (EBop BBAnd e1 e2) s = VBool true <->
  eval_Expr e1 s = VBool true /\ eval_Expr e2 s = VBool true.
Proof.
  destruct (bop_band_split (eval_Expr e1 s) (eval_Expr e2 s)) as (Hsp1 & Hsp2).
  split.
  - intros Hin.
    apply Hsp1.
    cbn in *.
    destruct (opB andb (eval_Expr e1 s) (eval_Expr e2 s)); try easy.
    unfold maybe, id in Hin; congruence.
  - intros Hin%Hsp2.
    cbn in *.
    now rewrite Hin.
Qed.

(*
Global Instance Expr_PredEx (Var : Type) :
  PredEx (Expr Var) unit unit (Var -> Val) :=
  {
    pe_true := EVal _ (VBool true);
    pe_conj := EBop BBAnd;
    pe_interp := fun _ _ => eval_Expr_Pred (Var := Var)
  }.
Proof.
  - easy.
  - intros [] [] ep s1 s2 Hseqv.
    unfold eval_Expr_Pred.
    now rewrite (eval_Expr_Proper ep Hseqv).
  - intros [] [] ep1 ep2.
    split; intros s.
    + intro Hin_and.
      unfold Ensembles.In in *.
      unfold eval_Expr_Pred in *.
      unfold eval_Expr in Hin_and.
      fold (eval_Expr ep1 s) in *.        
      fold (eval_Expr ep2 s) in *.
      (* Blast away all cases where the inner expressions don't yield
         Booleans. *)
      constructor;
        unfold In;
        destruct (eval_Expr ep1 s), (eval_Expr ep2 s); try easy;
          now destruct b, b0.
    + intros (Hin_1 & Hin_2)%Intersection_inv.
      unfold In, eval_Expr_Pred in *.
      cbn.
      destruct (eval_Expr ep1 s), (eval_Expr ep2 s); try easy.
      inversion_clear Hin_1.
      now inversion_clear Hin_2.
Defined.

Global Instance Expr_ImplPredEx (Var : Type) :
  ImplPredEx (Expr Var) unit unit (Var -> Val) :=
  {
    pe_false := EVal _ (VBool false);
    pe_impl := EBop BBImpl;
  }.
Proof.
  - (* false is empty set *)
    easy.
  - (* modus ponens *)
    intros [] [] e1 e2 s (Himpl & Hep1)%eval_bop_band_split.
    cbn in Himpl.
    rewrite Hep1 in Himpl.
    cbn in *.
    unfold eval_Expr_Pred.
    destruct (eval_Expr e2 s); try easy.
    now destruct b.
  - (* conditional proof *)
    intros [] [] e1 e2 s Hcond.
    cbn in *.
    unfold eval_Expr_Pred in *.
    cbn.
    unfold opB, eval_Val_B.
    destruct (eval_Expr e1 s); cbn; try easy.
    

    *)

Global Instance Expr_Functor : Functor Expr :=
  {
    fmap := efmap
  }.
Proof.
  - intros a e.
    induction e; try easy.
    + rewrite efmap_bop; congruence.
    + cbn; congruence.
  - intros A B C f g e.
    induction e; try easy.
    + rewrite efmap_bop.
      unfold compose at 2 in IHe1.
      unfold compose at 2 in IHe2.
      now rewrite IHe1, IHe2.
    + rewrite efmap_not.
      unfold compose at 2 in IHe.
      now rewrite IHe.
Defined.

Global Instance Expr_Monad : Monad Expr :=
  {
    mbind := ebind;
    mreturn := EVar;
  }.
Proof.
  all: try easy.
  - intros a v.
    rewrite <- efmap_id.
    apply (fmap_identity (Functor := Expr_Functor)).
  - intros a b c e f g.
    induction e; try easy.
    + now rewrite ! ebind_bop, IHe1, IHe2.
    + now rewrite ! ebind_not, IHe.
Defined.

Global Instance Expr_ValEx : ValEx Expr :=
  {
    ve_interp V := flip (@eval_Expr V);
  }.
Proof.
  - reduce.
    unfold flip, eval_Expr.
    induction x; try easy.
    * now rewrite <- IHx1, <- IHx2.
    * now rewrite <- IHx.
  - reduce.
    unfold flip.
    reduce_hyp H.
    induction ev; cbn; congruence.
  - easy.
Defined.
  
(* Global Instance Expr_EqPredEx : EqPredEx unit unit Expr := {}. *)




    
